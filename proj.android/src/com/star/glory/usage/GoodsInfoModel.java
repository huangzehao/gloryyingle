package com.star.glory.usage;

public class GoodsInfoModel {
    private String GoodsName = "";
    private String GoodsPrice = "";
    public String getGoodsName() {
        return GoodsName;
    }

    public void setGoodsName(String goodsName) {
        GoodsName = goodsName;
    }

    public String getGoodsPrice() {
        return GoodsPrice;
    }

    public void setGoodsPrice(String goodsPrice) {
        GoodsPrice = goodsPrice;
    }
}
