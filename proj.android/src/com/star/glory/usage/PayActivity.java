package com.star.glory.usage;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.star.glory.R;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.cocos2dx.cpp.PayHelper;

import sdk.pay.PayException;
import sdk.pay.PayExceptionType;
import sdk.pay.PayMD5Util;
import sdk.pay.PayTypeModel;
import sdk.pay.PayUtil;
import sdk.pay.PayUtilCallBack;
import sdk.pay.TokenParam;

public class PayActivity extends Activity implements PayUtilCallBack {
	private Toast mToast;
	private int mCheckedId = -1;
	private ImageView mOldCheckedView;
	private ListView mPayList;
	private TextView mPayName;
	private TextView mPayPrice;
	private Button mPaySubmit;
	private PayUtil mPayUtil;
	
	private String mCustomNmae = "";
	private String mUserID = "";
	protected Dialog mProgressDialog;

	private static final String SYSTEM_NAME = "jft";
	private static final String CODE = "10211943";
	private static final String APPID = "20170414002411383932";
	private static final String COM_KEY = "20E3536EDC62BA233E1149B9D47C09FE";
	private static final String KEY = "2023b5b41a5ead63a6f3637b96dd9541";
	private static final String VECTOR = "5fba55116514603c";

	private static final String RETURN_URL = "http://shangjia.jtpay.com/Form/TestReturn";
	private static final String NOTICE_URL = "http://www.778899yx.com:808/MobileUri.aspx";


	
	private boolean isPaying;

	private final Runnable mRunnable = new Runnable() {
		@Override
		public void run() {
			dismiss();
			List<PayTypeModel> payTypeModelList = mPayUtil.getPayTypeModels();
			if (payTypeModelList.size() > 0) {
				mPayList.setVisibility(View.VISIBLE);
				PayTypeAdapter adapter = new PayTypeAdapter(payTypeModelList);
				mPayList.setAdapter(adapter);
				mPayList.post(new Runnable() {
					// set default choice
					@Override
					public void run() {
						mOldCheckedView = (ImageView) mPayList.getChildAt(0)
								.findViewById(
										R.id.ImageButton_junpay_type_Checked);
						mOldCheckedView
								.setImageResource(R.drawable.image_icon_radiobutton_yes);
						mCheckedId = 0;
					}
				});
			} else {
				mPayList.setVisibility(View.GONE);
				Toast.makeText(PayActivity.this, "Did not get the payment!",
						Toast.LENGTH_SHORT).show();
			}
		}
	};

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		PayException.getInstance().init(getApplicationContext());
		setContentView(R.layout.pay_layout);
		initWidgets();
		Intent intent = getIntent();
		Bundle bundle = intent.getExtras();
		String goodsName = bundle.getString("GoodsName");
		String goodsPrice = bundle.getString("GoodsPrice");
		//String goodsPrice = "2";
		mCustomNmae = bundle.getString("CustomName");
		mUserID = bundle.getString("UserID");
		mPayName.setText(goodsName);
		mPayPrice.setText(goodsPrice);
		mToast = Toast.makeText(this, "", Toast.LENGTH_SHORT);
		mPayUtil = new PayUtil(this, APPID, KEY, VECTOR, SYSTEM_NAME, this,
				true);
		mProgressDialog = ProgressDialog.show(PayActivity.this, "",
				getString(R.string.init_tips), false, true, null);
		mPayUtil.getToken(getTokenParam());
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		if (null != mPayUtil && isPaying) {
			mPayUtil.getPayStatus();

			isPaying = false;
		}
	}

	@Override
	protected void onPause() {
		super.onPause();
		dismiss();
	}

	private TokenParam getTokenParam() {
		TokenParam tokenParam = new TokenParam();
		Date date = new Date();
		DateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmmss");

		tokenParam.setP1_usercode(CODE);
		tokenParam.setP2_order(dateFormat.format(date));
		tokenParam.setP3_money(mPayPrice.getText().toString());
		tokenParam.setP4_returnurl(RETURN_URL); // user define
		tokenParam.setP5_notifyurl(NOTICE_URL); // user define
		tokenParam.setP6_ordertime(dateFormat.format(date));
		tokenParam.setP14_customname(mCustomNmae);// user name
		tokenParam.setP7_sign(PayMD5Util.getMD5(
				CODE + "&" + dateFormat.format(date) + "&"
						+ mPayPrice.getText().toString() + "&" + RETURN_URL
						+ "&" + NOTICE_URL + "&" + dateFormat.format(date)
						+ COM_KEY).toUpperCase());
		tokenParam.setP24_remark(mUserID);// user name
		return tokenParam;
	}

	private void initWidgets() {
		mPayName = (TextView) findViewById(R.id.junpay_TextView_name);
		mPayPrice = (TextView) findViewById(R.id.junpay_TextView_price);
		mPayList = (ListView) findViewById(R.id.junpay_listview);
		mPaySubmit = (Button) findViewById(R.id.junpay_button_submit);
		mPaySubmit.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if (mCheckedId >= 0) {
					mProgressDialog = ProgressDialog.show(PayActivity.this, "",
							getString(R.string.pay_info), false, true, null);
					String typeId = getTypeId(mCheckedId);
					mPayUtil.getPayParam(Integer.parseInt(typeId));
					isPaying = true;
				} else {
					Toast.makeText(PayActivity.this,
							"Payment has not yet chosen", Toast.LENGTH_SHORT)
							.show();
				}
			}
		});
		mPayList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				if (mCheckedId != -1) {
					mOldCheckedView
							.setImageResource(R.drawable.image_icon_radiobutton_no);
				}
				mOldCheckedView = (ImageView) view
						.findViewById(R.id.ImageButton_junpay_type_Checked);
				mOldCheckedView
						.setImageResource(R.drawable.image_icon_radiobutton_yes);
				mCheckedId = position;
			}
		});
	}

	// @Override
	// public void onRequestPermissionsResult(int requestCode, String[]
	// permissions, int[] grantResults) {
	// super.onRequestPermissionsResult(requestCode, permissions, grantResults);
	// mPayUtil.onRequestPermissionsResult(requestCode, permissions,
	// grantResults);
	// }

	private void dismiss() {
		if (null != mProgressDialog && mProgressDialog.isShowing()) {
			mProgressDialog.dismiss();
		}
	}

	private void showToast(String str) {
		mToast.setText(str);
		mToast.show();
	}

	@Override
	protected void onDestroy() {
		if (null != mPayUtil) {
			mPayUtil.destroy();
		}
		super.onDestroy();
	}

	private String getTypeId(int index) {
		List<PayTypeModel> list = mPayUtil.getPayTypeModels();
		PayTypeModel model = list.get(index);
		return model.getTypeid();
	}

	@Override
	public void onPayDataResult() {
		runOnUiThread(mRunnable);
	}

	@Override
	public void onPayException(int exceptionType, String exceptionContent) {
		dismiss();
		if (null != exceptionContent) {
			showToast(exceptionContent);
		} else {
			PayExceptionType type = PayExceptionType.values()[exceptionType];
			String str;
			switch (type) {
			case DATA_EXCEPTION: {
				str = getString(R.string.data_exception);
				break;
			}
			case ENCRYPT_EXCEPTION: {
				str = getString(R.string.encrypt_exception);
				break;
			}
			case GET_PAY_METHOD_FAILED: {
				str = getString(R.string.get_pay_method_failed);
				break;
			}
			case DECRYPT_EXCEPTION: {
				str = getString(R.string.decrypt_exception);
				break;
			}
			case RETURN_ERROR_DATA: {
				str = getString(R.string.return_error_data);
				break;
			}
			case PAY_SYSTEM_ID_EMPTY: {
				str = getString(R.string.pay_system_id_empty);
				break;
			}
			case SERVER_CONNECTION_EXCEPTION: {
				str = getString(R.string.server_connection_exception);
				break;
			}
			case GET_PAY_STATUS_FAILED: {
				str = getString(R.string.get_pay_status_failed);
				break;
			}
			case INVALID_TOKEN: {
				str = getString(R.string.invalid_token);
				break;
			}
			case GET_TOKEN_FAILURE: {
				str = getString(R.string.get_token_failed);
				break;
			}
			case GET_SERVERS_FAILURE: {
				str = getString(R.string.get_servers_failure);
				break;
			}
			case NET_WORK_NOT_AVAILABLE: {
				str = getString(R.string.net_work_not_available);
				break;
			}
			default: {
				str = "";
				break;
			}
			}
			showToast(str);
		}
	}

	@Override
	public void onPayStatus(int payStatus) {
	      if (payStatus == 1) {
              showToast(getString(R.string.pay_success));
          } else {
              showToast(getString(R.string.pay_failure));
          }
	      PayHelper.payJFTResult(payStatus);
	}

	@Override
	public void onGetTokenResult() {
		mPayUtil.getPayType();
	}

	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		String action = data.getAction();
	}
}