package com.star.glory.usage;

import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.star.glory.R;

import java.util.List;

import sdk.pay.PayTypeModel;


/**
 * Created by Joe_PC on 2016/2/16,
 * For MyApplication.
 */
public class PayTypeAdapter extends BaseAdapter {
    private List<PayTypeModel> mArrayList;

    public PayTypeAdapter(List<PayTypeModel> arrayList) {
        mArrayList = arrayList;
    }

    @Override
    public int getCount() {
        return mArrayList.size();
    }

    @Override
    public Object getItem(int position) {
        return mArrayList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        ViewHolder holder;
        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.item_listviewlayout, null);
            holder = new ViewHolder();
            holder.image_junpay_type = (ImageView) convertView.findViewById(R.id.image_junpay_type);
            holder.TextView_junpay_type_name = (TextView) convertView.findViewById(R.id.TextView_junpay_type_name);
            holder.TextView_junpay_type_tips = (TextView) convertView.findViewById(R.id.TextView_junpay_type_tips);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        switch (mArrayList.get(position).getTypeid()) {
            case "1":
                break;
            case "2":
                break;
            case "3":
                holder.image_junpay_type.setImageResource(R.drawable.u43);
                break;
            case "4":
                holder.image_junpay_type.setImageResource(R.drawable.u29);
                break;
            case "5":
                break;
        }
        holder.TextView_junpay_type_name.setText(String.format("%s", mArrayList.get(position).getTypename()));
        String contactWay = mArrayList.get(position).getContactWay();
        String description = "赢乐电玩城";
        if (!TextUtils.isEmpty(contactWay)) {
            description = contactWay;
        }
        holder.TextView_junpay_type_tips.setText(String.format("%s", description));

        return convertView;
    }

    private static final class ViewHolder {
        ImageView image_junpay_type;
        TextView TextView_junpay_type_name;
        TextView TextView_junpay_type_tips;
    }
}
