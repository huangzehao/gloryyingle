package com.star.glory.usage;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.star.glory.R;

import java.util.ArrayList;

/**
 * Created by Joe_PC on 2016/2/19,
 * For MyApplication.
 */
public class GoodsListAdapter extends BaseAdapter {
    private ArrayList<GoodsInfoModel> mArraylist;
    private LayoutInflater mInflater;
    ViewHolder holder;

    public GoodsListAdapter(Context context, ArrayList<GoodsInfoModel> arrayList) {
        mArraylist = arrayList;
        mInflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return mArraylist.size();
    }

    @Override
    public Object getItem(int position) {
        return mArraylist.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.item_goodslist_layout, null);
            holder = new ViewHolder();
            holder.item_ImageView_GoodsList = (ImageView) convertView.findViewById(R.id.item_ImageView_GoodsList);
            holder.item_TextView_GoodsList_Name = (TextView) convertView.findViewById(R.id.item_TextView_GoodsList_Name);
            holder.item_TextView_GoodsList_Price = (TextView) convertView.findViewById(R.id.item_TextView_GoodsList_Price);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        holder.item_ImageView_GoodsList.setImageResource(R.drawable.free_trainer);
        holder.item_TextView_GoodsList_Name.setText(String.format("%s", mArraylist.get(position).getGoodsName()));
        holder.item_TextView_GoodsList_Price.setText(String.format("%s", mArraylist.get(position).getGoodsPrice()));

        return convertView;
    }

    private static final class ViewHolder {
        ImageView item_ImageView_GoodsList;
        TextView item_TextView_GoodsList_Name;
        TextView item_TextView_GoodsList_Price;
    }
}
