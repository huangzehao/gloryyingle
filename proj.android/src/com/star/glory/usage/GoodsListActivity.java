package com.star.glory.usage;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;


import java.util.ArrayList;

import com.star.glory.R;



public class GoodsListActivity extends Activity {

    private GoodsListAdapter listAdapter;
    private ListView listView;
    private ArrayList<GoodsInfoModel> arrayList = new ArrayList<GoodsInfoModel>();
    private static final String PRICE = "0.01";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_goods_list_acitvity);
        listView = (ListView) findViewById(R.id.GoodsListAcitvity_GoodsList);
        for (int i = 0; i < 1; i++) {
            GoodsInfoModel goodsInfoModel = new GoodsInfoModel();
            String name = "耐克";
            goodsInfoModel.setGoodsName(name);
            goodsInfoModel.setGoodsPrice(PRICE);
            arrayList.add(goodsInfoModel);
        }
        listAdapter = new GoodsListAdapter(this, arrayList);
        listView.setAdapter(listAdapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(GoodsListActivity.this, PayActivity.class);
                intent.putExtra("GoodsName", arrayList.get(position).getGoodsName());
                intent.putExtra("GoodsPrice", arrayList.get(position).getGoodsPrice());
                intent.putExtra("position", position);
                startActivity(intent);
            }
        });
    }

}
