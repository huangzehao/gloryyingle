package com.star.glory.usage;

import android.app.Application;
import android.content.Context;


import sdk.pay.PayException;


public class PayApplication extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        PayException.getInstance().init(getApplicationContext());
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
    }
}
