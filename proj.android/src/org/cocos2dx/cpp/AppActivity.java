/****************************************************************************
Copyright (c) 2008-2010 Ricardo Quesada
Copyright (c) 2010-2012 cocos2d-x.org
Copyright (c) 2011      Zynga Inc.
Copyright (c) 2013-2014 Chukong Technologies Inc.
 
http://www.cocos2d-x.org

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
****************************************************************************/
package org.cocos2dx.cpp;

import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.security.MessageDigest;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

import org.apache.http.util.EncodingUtils;
import org.cocos2dx.lib.Cocos2dxActivity;
import org.cocos2dx.lib.Cocos2dxGLSurfaceView;


/*import com.fanwei.sdk.api.PaySdk;*/


import com.star.glory.DialogHelper;
import com.star.glory.NewHelper;
import com.star.glory.R;
import com.star.glory.usage.GoodsListActivity;
import com.star.glory.usage.PayActivity;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.PowerManager;
import android.telephony.TelephonyManager;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import cn.sharesdk.ShareSDKUtils;
import sdk.pay.PayException;

public class AppActivity extends Cocos2dxActivity {
	static AppActivity instance = null;
	//PayActivity payactivity_ = null;
//	WebView m_webView;//WebView控件  
//	FrameLayout m_webLayout;//FrameLayout布局  
//	LinearLayout m_topLayout;//LinearLayout布局  
//	Button m_backButton;//关闭按钮  
	
	PowerManager.WakeLock mWakeLock;
	private UpdateManager updateMan;
	private ProgressDialog updateProgressDialog;
	String uniquid;
	
	public static AppActivity getInstance() {  
	    return instance;  
	}  
	
//	public WebView getWebView() {
//		return m_webView;
//	}
//	
	
	@Override
	@SuppressWarnings("deprecation")
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		instance = this;
		
        //初始化一个空的布局
//	    m_webLayout = new FrameLayout(this);  
//	    FrameLayout.LayoutParams lytp = new FrameLayout.LayoutParams(1200,650);  
//	    lytp .gravity = Gravity.CENTER;  
//	    addContentView(m_webLayout, lytp);
//	       
		PayException.getInstance().init(getApplicationContext());
		
		PowerManager pm = (PowerManager)getSystemService(Context.POWER_SERVICE);
		mWakeLock = pm.newWakeLock(PowerManager.SCREEN_BRIGHT_WAKE_LOCK, "XYTEST");
		mWakeLock.acquire();
		
		///< 检测更新..
        updateMan = new UpdateManager(AppActivity.this, appUpdateCb);
		updateMan.checkUpdate();
		
		///< 获取唯一标示
		TelephonyManager TelephonyMgr = (TelephonyManager)getSystemService(TELEPHONY_SERVICE); 
		uniquid = TelephonyMgr.getDeviceId(); 
		
/*		PaySdk.init(this, PaySdk.PORTRAIT);
		PayHelper.activity = this;*/
		
		ShareSDKUtils.prepare();
	}
	
    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
    }
	
	@Override
	public void onDestroy() {
		super.onDestroy();
		if(mWakeLock != null) 
		  { 
		   mWakeLock.release(); 
		   mWakeLock = null; 
		  } 

	}
	
	@Override
	public void onPause()
	{
		super.onPause();
		mWakeLock = null;
	}
	
	@SuppressWarnings("deprecation")
	@Override
	public void onResume() {
		super.onResume();
		if(mWakeLock == null) 
		  { 
		   PowerManager pm = (PowerManager)getSystemService(Context.POWER_SERVICE); 
		   mWakeLock = pm.newWakeLock(PowerManager.SCREEN_BRIGHT_WAKE_LOCK, "XYTEST"); 
		   mWakeLock.acquire(); 
		  } 
	}
	
	public static int haveNetWork() {

		//这里处理C++传递过来的数据
		boolean havaWifi = NewHelper.isWifiEnabled(getContext());
		return havaWifi ? 1 : 0;	
	}
	
	 // 自动更新回调函数
		UpdateManager.UpdateCallback appUpdateCb = new UpdateManager.UpdateCallback() 
		{

			@Override
			public void downloadProgressChanged(int progress) {
				if (updateProgressDialog != null
						&& updateProgressDialog.isShowing()) {
					updateProgressDialog.setProgress(progress);
				}

			}

			@Override
			public void downloadCompleted(Boolean sucess, CharSequence errorMsg) {
				if (updateProgressDialog != null
						&& updateProgressDialog.isShowing()) {
					updateProgressDialog.dismiss();
				}
				if (sucess) {
					updateMan.update();
				} else {
					DialogHelper.Confirm(AppActivity.this,
							R.string.dialog_error_title,
							R.string.dialog_downfailed_msg,
							R.string.dialog_downfailed_btnnext,
							new DialogInterface.OnClickListener() {

								@Override
								public void onClick(DialogInterface dialog,
										int which) {
									updateMan.downloadPackage();

								}
							}, R.string.dialog_downfailed_btnnext, null);
				}
			}

			@Override
			public void downloadCanceled() 
			{
				// TODO Auto-generated method stub

			}

			@Override
			public void checkUpdateCompleted(Boolean hasUpdate,
					CharSequence updateInfo) {
				
				if (hasUpdate) {
					DialogHelper.Confirm(AppActivity.this,
							getText(R.string.dialog_update_title),
							getText(R.string.dialog_update_msg).toString()
							+updateInfo+
							getText(R.string.dialog_update_msg2).toString(),
									getText(R.string.dialog_update_btnupdate),
							new DialogInterface.OnClickListener() {

								@Override
								public void onClick(DialogInterface dialog,
										int which) {
									updateProgressDialog = new ProgressDialog(
											AppActivity.this);
									updateProgressDialog
											.setMessage(getText(R.string.dialog_downloading_msg));
									updateProgressDialog.setIndeterminate(false);
									updateProgressDialog
											.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
									updateProgressDialog.setMax(100);
									updateProgressDialog.setProgress(0);
									updateProgressDialog.setCancelable(false);
									updateProgressDialog.show();

									updateMan.downloadPackage();
								}
							},getText( R.string.dialog_update_btnnext), null);
				}

			}
		};
		
		@SuppressLint("DefaultLocale")
		public void doJFTpay(String userid, int payNumble) {
			String goodsname = String.format("%d元宝", payNumble);
			float fPayNumble = payNumble;
			String goodsprice = String.format("%.2f", fPayNumble);
			Intent intent=new Intent();   
            intent.putExtra("GoodsName", goodsname);
            intent.putExtra("GoodsPrice", goodsprice);
            intent.putExtra("CustomName", userid);
            intent.putExtra("UserID", userid);
			intent.setClass(AppActivity.this, PayActivity.class); 
			startActivity(intent); 

			
//			Intent intent=new Intent();   
//			intent.setClass(AppActivity.this, GoodsListActivity.class); 
//			startActivity(intent); 
		}

//		public void openWeChatWebview(final String accounts, final int payNumble) {  
//		    //Log.v("TestJacky", "openWebView");  
//		    this.runOnUiThread(new Runnable() {//在主线程里添加别的控件  
//		        public void run() {     
//		            //初始化webView  
//		            m_webView = new WebView(instance);  
//		            //设置webView能够执行javascript脚本  
//		            m_webView.getSettings().setJavaScriptEnabled(true);              
//		            //设置可以支持缩放  
//		            m_webView.getSettings().setSupportZoom(true);//设置出现缩放工具  
//		            m_webView.getSettings().setBuiltInZoomControls(true);  
//		            //载入URL  
//		            //m_webView.loadUrl("http://www.baidu.com");  
//		            
//		            String usercode = "10211943";
//		            
//		            SimpleDateFormat df = new SimpleDateFormat("yyyyMMddHHmmss");//设置日期格式
//		            String timeStr = df.format(new Date());
//		            
//		            //生成uuid作为订单号
//		            int hashCodeV = UUID.randomUUID().toString().hashCode();
//		            if(hashCodeV < 0) {//有可能是负数
//		                hashCodeV = - hashCodeV;
//		            }
//
//		            String orderStr = usercode + "-" + timeStr + String.format("-%05d", hashCodeV);
//		            
//		            float num = payNumble;
//		            String payNumbleStr = String.format("%.2f", num);
//		            
//		            String p4returnurl = "http://www.baidu.com";
//		            
//		            String p5notifyurl = "http://";
//		            
//		            String signStr = usercode + "&" +orderStr+"&"+payNumbleStr+"&"+p4returnurl+"&"+p5notifyurl+"&"+timeStr+"20E3536EDC62BA233E1149B9D47C09FE";
//		            String p7sign = null;
//		            try {
//		            	MessageDigest md5=MessageDigest.getInstance("MD5");
//		            	md5.update(signStr.getBytes());
//		            	p7sign = new BigInteger(1, md5.digest()).toString(16);
//		            } catch (Exception e) {	                
//		            }
//		            
//		            
//		            String ipStr = null;
//		            try {
//						ipStr =  InetAddress.getLocalHost().getHostAddress().toString();
//						ipStr.replace('.', '_');
//					} catch (UnknownHostException e) {
//						// TODO Auto-generated catch block
//						e.printStackTrace();
//					}
//		            StringBuilder builder1 = new StringBuilder();
//		              builder1.append("p1_usercode=").append(usercode).append("&")
//		                      .append("p2_order=").append(orderStr).append("&")
//		                      .append("p3_money=").append(payNumbleStr).append("&")
//		                      .append("p4_returnurl=").append(p4returnurl).append("&")
//		                      .append("p5_notifyurl=").append(p5notifyurl).append("&")
//		                      .append("p6_ordertime=").append(timeStr).append("&")
//		                      .append("p7_sign=").append(p7sign).append("&")
//		                      .append("p9_paymethod=").append("3").append("&")
//		                      .append("p14_customname=").append(accounts).append("&")
//		                      .append("p17_customip=").append(ipStr).append("&")
//		                      .append("p25_terminal=").append("2").append("&")
//		                      .append("p26_iswappay=").append("3");
//
//		            String postData = builder1.toString();
//		            
//		            m_webView.postUrl("http://pay.jtpay.com/form/pay", EncodingUtils.getBytes(postData, "UTF-8"));
//		            //使页面获得焦点  
//		            m_webView.requestFocus();  
//		            //如果页面中链接，如果希望点击链接继续在当前browser中响应  
////		            m_webView.setWebViewClient(new WebViewClient(){         
////		                public boolean shouldOverrideUrlLoading(WebView view, String url) {     
//////		                    if(url.indexOf("tel:")<0){  
//////		                        view.loadUrl(url);   
//////		                    }  
//////		                    return true;      
////		                	return false;
////		                }      
////		            });  
//		              
//		            //初始化线性布局 里面加按钮和webView  
//		            m_topLayout = new LinearLayout(instance);        
//		            m_topLayout.setOrientation(LinearLayout.VERTICAL);  
//		            //初始化返回按钮  
//		            m_backButton = new Button(instance);  
//		            m_backButton.setBackgroundResource(R.drawable.btn_close);  
//		            LinearLayout.LayoutParams lypt=new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);  
//		            lypt.gravity=Gravity.RIGHT;  
//		            m_backButton.setLayoutParams(lypt);              
//		            m_backButton.setOnClickListener(new OnClickListener() {                      
//		                public void onClick(View v) {  
//		                    removeWebView();  
//		                }  
//		            });  
//		            //把image加到主布局里  
//		            //m_webLayout.addView(m_imageView);  
//		            //把webView加入到线性布局  
//		            m_topLayout.addView(m_backButton);  
//		            m_topLayout.addView(m_webView);                  
//		            //再把线性布局加入到主布局  
//		            m_webLayout.addView(m_topLayout);  
//		        }  
//		    });  
//		}  
//		
//		public void removeWebView() {                
//		    //m_webLayout.removeView(m_imageView);  
//		    //m_imageView.destroyDrawingCache();  
//		      
//		    m_webLayout.removeView(m_topLayout);  
//		    m_topLayout.destroyDrawingCache();  
//		              
//		    m_topLayout.removeView(m_webView);  
//		    m_webView.destroy();  
//		              
//		    m_topLayout.removeView(m_backButton);  
//		    m_backButton.destroyDrawingCache();  
//		} 

	
}
