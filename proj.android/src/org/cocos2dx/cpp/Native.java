package org.cocos2dx.cpp;

import java.util.List;

import org.cocos2dx.lib.Cocos2dxActivity;
import org.cocos2dx.lib.Cocos2dxHelper;

import com.star.glory.wxapi.WXEntryActivity;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Environment;
import android.util.Log;
import android.widget.Toast;
import cn.sharesdk.framework.Platform;
import cn.sharesdk.framework.ShareSDK;
import cn.sharesdk.wechat.friends.Wechat;
import cn.sharesdk.framework.Platform.ShareParams;


public class Native extends Cocos2dxHelper
{
	
	
//	public static native void WxLoginGetAccessToken(String kUrl);
//	public static native void WxLoginGetFailToken(String Error);
//	
	
	 public static void LoginWX(String APP_ID,String AppSecret)
	  {
		 Intent intent = new Intent(Cocos2dxActivity.getContext(), WXEntryActivity.class);
		 intent.putExtra(WXEntryActivity.ReqWxLogin,"wxlogin");
		 Cocos2dxActivity.getContext().startActivity(intent);
		 
	  }
	 public static void ShareImageWX(String ImgPath,int nType)
     {
//		 Intent intent = new Intent(Cocos2dxActivity.getContext(), WXEntryActivity.class);
//		 intent.putExtra(WXEntryActivity.ReqWxShareImg,"ReqWxShareImg");
//		 intent.putExtra("ImgPath",ImgPath);
//		 intent.putExtra("ShareType",nType);
//		 Cocos2dxActivity.getContext().startActivity(intent);
		 
		 ShareParams sp = new ShareParams();
		 sp.setImagePath(ImgPath);
		 sp.setShareType(Platform.SHARE_IMAGE);
		 Platform wechat = ShareSDK.getPlatform (Wechat.NAME);
		 wechat.share(sp);		
     } 
	 public static void ShareTextWX(String text,int nType)
     {
//		 Intent intent = new Intent(Cocos2dxActivity.getContext(), WXEntryActivity.class);
//		 intent.putExtra(WXEntryActivity.ReqWxShareTxt,"ReqWxShareTxt");
//		 intent.putExtra("ShareText",text);
//		 intent.putExtra("ShareType",nType);
//		 Cocos2dxActivity.getContext().startActivity(intent);
		 
		 ShareParams sp = new ShareParams();
		 sp.setText(text);
		 sp.setShareType(Platform.SHARE_TEXT);
		 Platform wechat = ShareSDK.getPlatform (Wechat.NAME);
		 wechat.share(sp);
     } 
	 
	 public static void ShareUrlWX(String url,String title,String desc,int nType)
     {
		 Intent intent = new Intent(Cocos2dxActivity.getContext(), WXEntryActivity.class);
		 intent.putExtra(WXEntryActivity.ReqWxShareUrl,"ReqWxShareUrl");
		 intent.putExtra("ShareUrl",url);
		 intent.putExtra("ShareTitle",title);
		 intent.putExtra("ShareDesc",desc);
		 intent.putExtra("ShareType",nType);
		 Cocos2dxActivity.getContext().startActivity(intent);
//		 
//		 ShareParams sp = new ShareParams();
//		 sp.setTitle(title);
//		 sp.setText(desc);
//		 sp.setTitleUrl(url);
//		 sp.setShareType(Platform.SHARE_WEBPAGE);
//		 Platform wechat = ShareSDK.getPlatform (Wechat.NAME);
//		 wechat.share(sp);
     } 
	 
	 public static void showWebView(String kUrl)
     {
		 Log.d("showWebView",kUrl);
		 Uri uri = Uri.parse(kUrl);
		 Intent it = new Intent(Intent.ACTION_VIEW, uri);    
		 Cocos2dxActivity.getContext().startActivity(it);
     }

//	public static void versionUpdate(final String url, final String info,final int size, final int isUpdate) 
//	{
//		 Log.d("versionUpdate","1111");
//		QYFun.getInstance().setContext(Cocos2dxActivity.getContext());
//
//		 Log.d("versionUpdate","2222");
//		new Thread() {
//			@Override
//			public void run() {
//
//				 Log.d("versionUpdate","3333");
//				QYFun.getInstance().showVersionUpdate(url,info,size,isUpdate);
//			}
//		}.start();
//		
//	}

//	 public static void startSoundRecord()
//	 {
//		 String SoundFilePath= Environment.getExternalStorageDirectory().getAbsolutePath();  
//		 String SoundFileName = "soundRecord.wav";
//
//		 Log.d("startSoundRecord",SoundFilePath);
//		 ExtAudioRecorder recorder = ExtAudioRecorder.getInstanse(false);
//		 recorder.recordChat(SoundFilePath+"/",SoundFileName);
//		 
//	 }
	 
//	 public static String stopSoundRecord()
//	 {
//		 return ExtAudioRecorder.stopRecord();
//	 }
	 
	 public static double getLatitude()
	 {
		//获取地理位置管理器  
		 LocationManager locationManager = (LocationManager)Cocos2dxActivity.getContext().getSystemService(Context.LOCATION_SERVICE);  
		 String locationProvider;
		 //获取所有可用的位置提供器  
		 List<String> providers = locationManager.getProviders(true);  
		 if(providers.contains(LocationManager.GPS_PROVIDER)){  
		     //如果是GPS  
		     locationProvider = LocationManager.GPS_PROVIDER;  
		 }else if(providers.contains(LocationManager.NETWORK_PROVIDER)){  
		     //如果是Network  
		     locationProvider = LocationManager.NETWORK_PROVIDER;  
		 }else{  
		     //Toast.makeText(this, "没有可用的位置提供器", Toast.LENGTH_SHORT).show();  
		     return -1;  
		 }  
		 //获取Location  
		 Location location = locationManager.getLastKnownLocation(locationProvider);

		 return location.getLatitude();
	 }
	 
	 public static double getLongitude()
	 {
		//获取地理位置管理器  
		 LocationManager locationManager = (LocationManager)Cocos2dxActivity.getContext().getSystemService(Context.LOCATION_SERVICE);  
		 String locationProvider;
		 //获取所有可用的位置提供器  
		 List<String> providers = locationManager.getProviders(true);  
		 if(providers.contains(LocationManager.GPS_PROVIDER)){  
		     //如果是GPS  
		     locationProvider = LocationManager.GPS_PROVIDER;  
		 }else if(providers.contains(LocationManager.NETWORK_PROVIDER)){  
		     //如果是Network  
		     locationProvider = LocationManager.NETWORK_PROVIDER;  
		 }else{  
		     //Toast.makeText(this, "没有可用的位置提供器", Toast.LENGTH_SHORT).show();  
		     return -1;  
		 }  
		 //获取Location  
		 Location location = locationManager.getLastKnownLocation(locationProvider);

		 return location.getLongitude();
	 }
	 
//	 //获取版本号
//	 public static String getVersionName() {
//		 return Cocos2dxActivity.getContext().getString(R.string.version_name);
//	 }
//	 
//	 //复制到剪切板
//	 public static void copyToClipboard(String str) {
//		 ClipboardManager myClipboard;
//		 myClipboard = (ClipboardManager)Cocos2dxActivity.getContext().getSystemService(Context.CLIPBOARD_SERVICE);
//		 ClipData myClip;
//		 myClip = ClipData.newPlainText("text", str);
//		 myClipboard.setPrimaryClip(myClip);
//		 Toast.makeText(Cocos2dxActivity.getContext(), "复制成功", Toast.LENGTH_LONG).show();
//	 }
}
