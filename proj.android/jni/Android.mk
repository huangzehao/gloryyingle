LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

$(call import-add-path,$(LOCAL_PATH)/../../cocos2d)
$(call import-add-path,$(LOCAL_PATH)/../../cocos2d/external)
# $(call import-add-path,$(LOCAL_PATH)/../../cocos2d/extensions)
$(call import-add-path,$(LOCAL_PATH)/../../cocos2d/cocos)

LOCAL_MODULE := cocos2dcpp_shared

LOCAL_MODULE_FILENAME := libcocos2dcpp

FILE_LIST := hellocpp/main.cpp
FILE_LIST += $(wildcard $(LOCAL_PATH)/../../Classes/*.cpp)  
FILE_LIST += $(wildcard $(LOCAL_PATH)/../../Classes/iconv/*.cpp)
FILE_LIST += $(wildcard $(LOCAL_PATH)/../../Classes/ShlwGame/*.cpp)  
FILE_LIST += $(wildcard $(LOCAL_PATH)/../../Classes/ShlwGame/ShlwDefine/*.cpp)  
FILE_LIST += $(wildcard $(LOCAL_PATH)/../../Classes/ShlwGame/ShlwDefine/Game/*.cpp)  
FILE_LIST += $(wildcard $(LOCAL_PATH)/../../Classes/ShlwGame/ShlwKernel/*.cpp)  
FILE_LIST += $(wildcard $(LOCAL_PATH)/../../Classes/ShlwGame/ShlwView/*.cpp)  
FILE_LIST += $(wildcard $(LOCAL_PATH)/../../Classes/ShlwGame/ShlwView/Node/*.cpp)  
FILE_LIST += $(wildcard $(LOCAL_PATH)/../../Classes/ShlwGame/ShlwView/Scene/*.cpp)  
FILE_LIST += $(wildcard $(LOCAL_PATH)/../../Classes/ShlwGame/ShlwView/ui/*.cpp)  
FILE_LIST += $(wildcard $(LOCAL_PATH)/../../Classes/DntgGame/*.cpp)  
FILE_LIST += $(wildcard $(LOCAL_PATH)/../../Classes/DntgGame/DntgDefine/*.cpp)  
FILE_LIST += $(wildcard $(LOCAL_PATH)/../../Classes/DntgGame/DntgDefine/Game/*.cpp)  
FILE_LIST += $(wildcard $(LOCAL_PATH)/../../Classes/DntgGame/DntgKernel/*.cpp)  
FILE_LIST += $(wildcard $(LOCAL_PATH)/../../Classes/DntgGame/DntgView/*.cpp)  
FILE_LIST += $(wildcard $(LOCAL_PATH)/../../Classes/DntgGame/DntgView/Node/*.cpp)  
FILE_LIST += $(wildcard $(LOCAL_PATH)/../../Classes/DntgGame/DntgView/Scene/*.cpp)  
FILE_LIST += $(wildcard $(LOCAL_PATH)/../../Classes/DntgGame/DntgView/ui/*.cpp)  
FILE_LIST += $(wildcard $(LOCAL_PATH)/../../Classes/LkpyGame/*.cpp)  
FILE_LIST += $(wildcard $(LOCAL_PATH)/../../Classes/LkpyGame/LkpyDefine/*.cpp)  
FILE_LIST += $(wildcard $(LOCAL_PATH)/../../Classes/LkpyGame/LkpyDefine/Game/*.cpp)  
FILE_LIST += $(wildcard $(LOCAL_PATH)/../../Classes/LkpyGame/LkpyKernel/*.cpp)  
FILE_LIST += $(wildcard $(LOCAL_PATH)/../../Classes/LkpyGame/LkpyView/*.cpp)  
FILE_LIST += $(wildcard $(LOCAL_PATH)/../../Classes/LkpyGame/LkpyView/Node/*.cpp)  
FILE_LIST += $(wildcard $(LOCAL_PATH)/../../Classes/LkpyGame/LkpyView/Scene/*.cpp)  
FILE_LIST += $(wildcard $(LOCAL_PATH)/../../Classes/LkpyGame/LkpyView/ui/*.cpp)   

FILE_LIST += $(wildcard $(LOCAL_PATH)/../../Classes/common/*.cpp)  
FILE_LIST += $(wildcard $(LOCAL_PATH)/../../Classes/gamescene_Baccarat/*.cpp)  
FILE_LIST += $(wildcard $(LOCAL_PATH)/../../Classes/gamescene_DZShowHand/*.cpp)  
FILE_LIST += $(wildcard $(LOCAL_PATH)/../../Classes/gamescene_Ox/*.cpp)  
FILE_LIST += $(wildcard $(LOCAL_PATH)/../../Classes/gamescene_Ox100/*.cpp)  
FILE_LIST += $(wildcard $(LOCAL_PATH)/../../Classes/gamescene_ZJH/*.cpp)  
FILE_LIST += $(wildcard $(LOCAL_PATH)/../../Classes/gamescene_DouDiZhu/*.cpp)  
FILE_LIST += $(wildcard $(LOCAL_PATH)/../../Classes/gamescene_SGLY/*.cpp)  
FILE_LIST += $(wildcard $(LOCAL_PATH)/../../Classes/gamescene_SHZ/*.cpp)  
FILE_LIST += $(wildcard $(LOCAL_PATH)/../../Classes/gamescene_JSYS/*.cpp)  
FILE_LIST += $(wildcard $(LOCAL_PATH)/../../Classes/gamescene_BetCar/*.cpp)  
FILE_LIST += $(wildcard $(LOCAL_PATH)/../../Classes/gamescene_FQZS/*.cpp)  
FILE_LIST += $(wildcard $(LOCAL_PATH)/../../Classes/gamescene_Ox4/*.cpp)  
FILE_LIST += $(wildcard $(LOCAL_PATH)/../../Classes/gamescene_LHDB/*.cpp)  
FILE_LIST += $(wildcard $(LOCAL_PATH)/../../Classes/gamescene_TBOx/*.cpp)  
FILE_LIST += $(wildcard $(LOCAL_PATH)/../../Classes/gamescene_SSZP/*.cpp)
FILE_LIST += $(wildcard $(LOCAL_PATH)/../../Classes/gamescene_SaiMa/*.cpp)
FILE_LIST += $(wildcard $(LOCAL_PATH)/../../Classes/gamescene_ShowHand/*.cpp)
FILE_LIST += $(wildcard $(LOCAL_PATH)/../../Classes/gamescene_SLWH/*.cpp)      

FILE_LIST += $(wildcard $(LOCAL_PATH)/../../Classes/Kernel/*.cpp)  
FILE_LIST += $(wildcard $(LOCAL_PATH)/../../Classes/Kernel/kernel/*.cpp)  
FILE_LIST += $(wildcard $(LOCAL_PATH)/../../Classes/Kernel/kernel/game/*.cpp)  
FILE_LIST += $(wildcard $(LOCAL_PATH)/../../Classes/Kernel/kernel/server/*.cpp)  
FILE_LIST += $(wildcard $(LOCAL_PATH)/../../Classes/Kernel/kernel/user/*.cpp)  
FILE_LIST += $(wildcard $(LOCAL_PATH)/../../Classes/Kernel/network/*.cpp)  
FILE_LIST += $(wildcard $(LOCAL_PATH)/../../Classes/Kernel/network/client_net/*.cpp)  
FILE_LIST += $(wildcard $(LOCAL_PATH)/../../Classes/Kernel/socket/*.cpp)  
FILE_LIST += $(wildcard $(LOCAL_PATH)/../../Classes/Platform/*.cpp)  
FILE_LIST += $(wildcard $(LOCAL_PATH)/../../Classes/Platform/PFDefine/*.cpp)  
FILE_LIST += $(wildcard $(LOCAL_PATH)/../../Classes/Platform/PFDefine/data/*.cpp)  
FILE_LIST += $(wildcard $(LOCAL_PATH)/../../Classes/Platform/PFDefine/df/*.cpp)  
FILE_LIST += $(wildcard $(LOCAL_PATH)/../../Classes/Platform/PFDefine/msg/*.cpp)  
FILE_LIST += $(wildcard $(LOCAL_PATH)/../../Classes/Platform/PFKernel/*.cpp)  
FILE_LIST += $(wildcard $(LOCAL_PATH)/../../Classes/Platform/PFView/*.cpp)  
FILE_LIST += $(wildcard $(LOCAL_PATH)/../../Classes/Platform/PFView/LoadScene/*.cpp)  
FILE_LIST += $(wildcard $(LOCAL_PATH)/../../Classes/Platform/PFView/LoginScene/*.cpp)  
FILE_LIST += $(wildcard $(LOCAL_PATH)/../../Classes/Platform/PFView/ModeScene/*.cpp)  
FILE_LIST += $(wildcard $(LOCAL_PATH)/../../Classes/Platform/PFView/ServerListScene/*.cpp)  
FILE_LIST += $(wildcard $(LOCAL_PATH)/../../Classes/Platform/PFView/ServerScene/*.cpp)  
FILE_LIST += $(wildcard $(LOCAL_PATH)/../../Classes/pthread/*.cpp)  
FILE_LIST += $(wildcard $(LOCAL_PATH)/../../Classes/Tools/*.cpp)  
FILE_LIST += $(wildcard $(LOCAL_PATH)/../../Classes/Tools/core/*.cpp)  
FILE_LIST += $(wildcard $(LOCAL_PATH)/../../Classes/Tools/Dialog/*.cpp)  
FILE_LIST += $(wildcard $(LOCAL_PATH)/../../Classes/Tools/Manager/*.cpp)  
FILE_LIST += $(wildcard $(LOCAL_PATH)/../../Classes/Tools/tools/*.cpp)  

LOCAL_SRC_FILES := $(FILE_LIST:$(LOCAL_PATH)/%=%)  

LOCAL_C_INCLUDES := $(LOCAL_PATH)/../../Classes

LOCAL_STATIC_LIBRARIES := cocos2dx_static
# //LOCAL_SHORT_COMMANDS := true

LOCAL_WHOLE_STATIC_LIBRARIES += libiconv_static
# LOCAL_WHOLE_STATIC_LIBRARIES += cocos_extension_static
# LOCAL_WHOLE_STATIC_LIBRARIES += libiconv_static

include $(BUILD_SHARED_LIBRARY)

$(call import-module,.)
# $(call import-module,libiconv)
# $(call import-module,extensions)
$(call import-module,libiconv)
