//
//  IosHelper.cpp
//  kkddz
//
//  Created by macbook110 on 13-6-4.
//
//

#include "IosHelper.h"
#include "Tools/tools/MTNotification.h"

#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)

#import <AVFoundation/AVFoundation.h>
#import <AudioToolbox/AudioToolbox.h>
#import <UIKit/UIDevice.h>
#include <sys/socket.h> // Per msqr
#include <sys/sysctl.h>
#include <net/if.h>
#include <net/if_dl.h>
#import "WXApi.h"
#import "WXApiObject.h"
#import <JftMergeSDK/JftMergeSDK.h>
#import "SDKExport/MD5.h"
#import "PayCallFunc.h"
//#import "SDKExport/MBProgressHUD.h"
#endif//

void IosHelper::sendAuthRequest()
{ 
    //构造SendAuthReq结构体 
    SendAuthReq* req =[[[SendAuthReq alloc ] init ] autorelease ];
    req.scope = @"snsapi_userinfo" ;
    req.state = @"123" ;
    //第三方向微信终端发送一个SendAuthReq消息结构
    [WXApi sendReq:req]; 
}

void IosHelper::shareWithWeixinCircleTxt(const char * pTxt,const char * pUrl){
    NSString *txt = [NSString stringWithCString:pTxt encoding:NSUTF8StringEncoding];
    WXMediaMessage *message = [WXMediaMessage message];
    message.title = txt;
    message.description = txt;
    message.messageExt = txt;
    message.messageAction = txt;
    
    [message setThumbImage:[UIImage imageNamed:@"weixin_share_icon.png"]];
    
    WXWebpageObject *ext = [WXWebpageObject object];
    ext.webpageUrl = [NSString stringWithCString:pUrl encoding:NSUTF8StringEncoding];
    
    message.mediaObject = ext;
    
    
    SendMessageToWXReq* req = [[[SendMessageToWXReq alloc] init]autorelease];
    req.bText = NO;
    req.message = message; 
    req.scene = WXSceneTimeline;
    
    [WXApi sendReq:req];
}
void IosHelper::shareWithWeixinFriendTxt(const char * pTxt,const char * pDes,const char * pUrl){
    
    NSString *txt = [NSString stringWithCString:pTxt encoding:NSUTF8StringEncoding];
    NSString *des = [NSString stringWithCString:pDes encoding:NSUTF8StringEncoding];
    WXMediaMessage *message = [WXMediaMessage message];
    message.title = txt;
    message.description = des;
    message.messageExt = txt;
    message.messageAction = txt;
    
    WXWebpageObject *ext = [WXWebpageObject object];
    ext.webpageUrl = [NSString stringWithCString:pUrl encoding:NSUTF8StringEncoding];
    
    message.mediaObject = ext;
    
    
    SendMessageToWXReq* req = [[[SendMessageToWXReq alloc] init]autorelease];
    req.bText = NO;
    req.message = message; 
    req.scene = WXSceneSession;
    
    [WXApi sendReq:req];

}

void IosHelper::shareWithWeixinFriendImg(const char * pTxt,const char *FileName)
{
    NSString *txt = [NSString stringWithCString:pTxt encoding:NSUTF8StringEncoding];
    NSString *filePath = [NSString stringWithCString:FileName encoding:NSUTF8StringEncoding];
    WXMediaMessage *message = [WXMediaMessage message];
    
    message.title = txt;
    message.description = txt;
    [message setThumbImage:[UIImage imageNamed:@"weixin_share_icon.png"]];
    
    WXImageObject *ext = [WXImageObject object];
    ext.imageData = [NSData dataWithContentsOfFile:filePath];
    
    
    
    message.mediaObject = ext;
    
    SendMessageToWXReq* req = [[[SendMessageToWXReq alloc] init]autorelease];
    req.bText = NO;
    req.message = message;
    req.scene = WXSceneSession;
    
    [WXApi sendReq:req];
}
void IosHelper::shareWithWeixinCircleImg(const char * pTxt,const char *FileName)
{
    NSString *txt = [NSString stringWithCString:pTxt encoding:NSUTF8StringEncoding];
    NSString *filePath = [NSString stringWithCString:FileName encoding:NSUTF8StringEncoding];
    WXMediaMessage *message = [WXMediaMessage message];
    
    message.title = txt;
    message.description = txt;
    [message setThumbImage:[UIImage imageNamed:@"weixin_share_icon.png"]];
    
    WXImageObject *ext = [WXImageObject object];
    ext.imageData = [NSData dataWithContentsOfFile:filePath];
    
    
    
    message.mediaObject = ext;
    
    SendMessageToWXReq* req = [[[SendMessageToWXReq alloc] init]autorelease];
    req.bText = NO;
    req.message = message;
    req.scene = WXSceneTimeline;
    
    [WXApi sendReq:req];
}
void IosHelper::startBrowserJni( const char * url)
{
    NSString *nsFineName = [NSString stringWithCString:url encoding:NSUTF8StringEncoding];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:nsFineName]];
}
 AVAudioRecorder *recorder = NULL;
void IosHelper::beginRecord(const char *_fileName)
{
    if (recorder == nil)
    {
        //设置文件名和录音路径
        NSString *recordFilePath = [NSString stringWithCString:_fileName encoding:NSUTF8StringEncoding];
        
        NSDictionary *recordSetting = [[NSDictionary alloc] initWithObjectsAndKeys:
                                       [NSNumber numberWithFloat: 8000.0],AVSampleRateKey, //采样率
                                       [NSNumber numberWithInt: kAudioFormatLinearPCM],AVFormatIDKey,
                                       [NSNumber numberWithInt:16],AVLinearPCMBitDepthKey,//采样位数 默认 16
                                       [NSNumber numberWithInt: 1], AVNumberOfChannelsKey,//通道的数目
                                       nil];
        //初始化录音
        NSError *error = nil;
        recorder = [[ AVAudioRecorder alloc] initWithURL:[NSURL URLWithString:recordFilePath] settings:recordSetting error:&error];
    }
    recorder.meteringEnabled = YES;
    [recorder prepareToRecord];
    //开始录音
    UInt32 sessionCategory = kAudioSessionCategory_PlayAndRecord;
    AudioSessionSetProperty(kAudioSessionProperty_AudioCategory, sizeof(sessionCategory), &sessionCategory);
    
    // 扬声器播放
    UInt32 audioRouteOverride = kAudioSessionOverrideAudioRoute_Speaker;
    AudioSessionSetProperty (kAudioSessionProperty_OverrideAudioRoute, sizeof(audioRouteOverride), &audioRouteOverride);
    [[AVAudioSession sharedInstance] setCategory: AVAudioSessionCategoryPlayAndRecord error:nil];
    [[AVAudioSession sharedInstance] setActive:YES error:nil];
    [recorder record];
}
const char * IosHelper::endRecord()
{
    if (recorder == nil)
        return "";
    if (recorder.isRecording)
        [recorder stop];
	return "";
}


void IosHelper::payJunFuTongWeChatIOS(const char* userid, const char* gameid, int payNumble)
{
    JFTTokenModel * tokenModel;
    
    NSString *moneyStr = [NSString stringWithFormat:@"%d", payNumble];
    static NSString *SYSTEM_NAME = @"jft";
    static NSString *CODE = @"10211943";
    static NSString *APPID = @"20170414002411383932";
    static NSString *COM_KEY = @"20E3536EDC62BA233E1149B9D47C09FE";
    static NSString *KEY = @"2023b5b41a5ead63a6f3637b96dd9541";
    static NSString *VECTOR = @"5fba55116514603c";

    tokenModel = [JFTTokenModel new];
    tokenModel.p1_user_code =CODE;
    tokenModel.userAppid = APPID;
    tokenModel.keyString = KEY;
    tokenModel.ivString  = VECTOR;
    
    NSDateFormatter *formatter=[[NSDateFormatter alloc]init];
    [formatter setDateFormat:@"yyyyMMddHHmmss"];
    NSDate *p2_orderDate=[NSDate date];
    tokenModel.p2_order=[formatter stringFromDate:p2_orderDate];
    tokenModel.p3_money = moneyStr;
    tokenModel.p4_returnurl = @"http://shangjia.jtpay.com/Form/TestReturn";
    tokenModel.p5_notifyurl = @"http://www.778899yx.com:808/MobileUri.aspx";;
    tokenModel.p6_ordertime = [formatter stringFromDate:p2_orderDate];
    NSString *p7_sign=[NSString stringWithFormat:@"%@&%@&%@&%@&%@&%@%@",tokenModel.p1_user_code,tokenModel.p2_order,tokenModel.p3_money,tokenModel.p4_returnurl,tokenModel.p5_notifyurl,tokenModel.p6_ordertime,COM_KEY];
    tokenModel.p7_sign = [MD5 md532BitUpper:p7_sign];
    tokenModel.serviceType =  SYSTEM_NAME;
    
    
    NSString* userIDStr = [NSString stringWithCString:userid encoding:NSUTF8StringEncoding];
    NSString* gameIDStr = [NSString stringWithCString:gameid encoding:NSUTF8StringEncoding];
    //以下为可空参数
    tokenModel.parameterDic =@{
                               @"p8_signtype":@"",@"p10_paychannelnum":@"",@"p11_cardtype":@"",@"p12_channel":@"",@"p13_orderfailertime":@"",@"p14_customname":gameIDStr,@"p15_customcontacttype":@"",@"p16_customcontact":@"",@"p17_customip":@"",@"p18_product":@"",@"p19_productcat":@"",@"p20_productnum":@"",@"p21_pdesc":@"",@"p22_version":@"",@"p23_charset":@"",@"p24_remark":userIDStr,@"p25_terminal":@"",@"p26_iswappay":@"",@"P27_phonecharacter":@""};
    
    static PayCallFunc* paycallfunc = [PayCallFunc new];
    [JfPay getToken:tokenModel success:^(BOOL tokenSuccess) {
        
//        [JfPay getPayTypeListkey:tokenModel.keyString iv:tokenModel.ivString appId:tokenModel.userAppid delegate:self];
//        dispatch_async(dispatch_get_main_queue(), ^{
//            [MBProgressHUD hideHUDForView:self.view animated:YES];
//        });
        
        JFTPayModel *payModel =[JFTPayModel new];
        
        //NSDictionary<NSString *,NSString *> * typeItem = [self.parArray objectAtIndex:indexPath.item];
        //NSString * typeId   = [typeItem objectForKey:@"typeid"];

        payModel.key        = KEY;
        payModel.iv         = VECTOR;
        payModel.appId      = APPID;
        NSLog(@"%@",payModel.appId);
        payModel.payTypeId  = @"3";
        payModel.controler  = [UIApplication sharedApplication].keyWindow.rootViewController;
        payModel.isReturn = YES;
        payModel.weichatAppid = @"wx7de6c5c43caa2176";
        [JfPay payByJftPayModel:payModel delegate:paycallfunc];
        
    } failer:^(NSString *failerStr) {
        NSLog(@"%@",failerStr);
//        dispatch_async(dispatch_get_main_queue(), ^{
//            [MBProgressHUD hideHUDForView:self.view animated:YES];
//            MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
//            hud.mode = MBProgressHUDModeText;
//            hud.labelText = failerStr;
//            hud.margin = 10.f;
//            //        hud.yOffset = 150.f;
//            hud.removeFromSuperViewOnHide = YES;
//            [hud hide:YES afterDelay:1.5];
//        });
    }];
    
}

void IosHelper::payJunFuTongWeChatSuccess()
{
    int payStatus = 1;
    G_NOTIFY_D("JFT_PAY_RESULT", &payStatus);
}
