//
//  IAPHelper.mm
//  Byds
//
//  Created by  Dima on 14-5-19.
//
//

#import <StoreKit/StoreKit.h>
#include "IAPHelper.h"

using namespace std;

//////////////////////////////////////////////////////////////////////////////
// 查询产品接口
@interface iAPProductsRequestDelegate : NSObject<SKProductsRequestDelegate>
@property (nonatomic, assign) IAPHelper* iap;
@end

@implementation iAPProductsRequestDelegate

// 1
-(void)productsRequest:(SKProductsRequest *)request didReceiveResponse:(SKProductsResponse *)response
{
    // release old
    _iap->cleanup();
    // record new product
    _iap->skProducts = [response.products retain];
    
    //NSLog(@"products count:%d", [response.products count]);
    for (int index = 0; index < [response.products count]; ++index) {
        SKProduct* skProduct = [response.products objectAtIndex:index];
        
        // check is valid
        bool isValid = true;
        
        for (NSString* invalidIdentifier in response.invalidProductIdentifiers) {
            //NSLog(@"invalidIdentifier:%@", invalidIdentifier);
            if ([skProduct.productIdentifier isEqualToString:invalidIdentifier]) {
                isValid = false;
                break;
            }
        }
        
        // locale price to string
        NSNumberFormatter* formater = [[NSNumberFormatter alloc] init];
        [formater setFormatterBehavior:NSNumberFormatterBehavior10_4];
        [formater setNumberStyle:NSNumberFormatterCurrencyStyle];
        [formater setLocale:skProduct.priceLocale];
        
        NSString* sPrice = [formater stringFromNumber:skProduct.price];
        [formater release];
        
        IAPProduct* product = new IAPProduct;
        product->identifier = string([skProduct.productIdentifier UTF8String]);
        product->title      = skProduct.localizedTitle == NULL ? "invalid title" : string([skProduct.localizedTitle UTF8String]);
        product->description= skProduct.localizedDescription == NULL ? "invalid description" : string([skProduct.localizedDescription UTF8String]);
        product->price      = string([sPrice UTF8String]);
        product->index      = index;
        product->isValid    = isValid;
        _iap->IAPProducts.push_back(product);
    }
} // productsRequest

// 2
- (void)requestDidFinish:(SKRequest *)request
{
    _iap->delegate->onRequestProductsFinish();
    [request.delegate release];
    [request release];
} // requestDidFinish

// 3
- (void)request:(SKRequest *)request didFailWithError:(NSError *)error
{
    //NSLog(@"%@", error);
    _iap->delegate->onRequestProductsError([error code]);
} // request

@end

//////////////////////////////////////////////////////////////////////////////
// 查询产品接口
@interface iAPTransactionObserver : NSObject<SKPaymentTransactionObserver>
@property (nonatomic, assign) IAPHelper* iap;
// 验证
-(BOOL)verify:(NSData*)receipt;
@end

@implementation iAPTransactionObserver

// 验证
-(BOOL)verify:(NSData*)receipt
{
    //第一步，创建URL
    NSString *strUrl = [NSString stringWithFormat:@"http://czbuyu.io78.com/IphoneMessage.aspx"];
    //NSString *strUrl = [NSString stringWithFormat:@"http://192.168.10.65:8080/jcby/verify.php"];
    NSURL *url = [NSURL URLWithString:strUrl];
    
    //第二步，通过URL创建网络请求
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc]initWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:30];
    //NSURLRequest初始化方法第一个参数：请求访问路径，第二个参数：缓存协议，第三个参数：网络请求超时时间（秒）
    /*
     其中缓存协议是个枚举类型包含：
     NSURLRequestUseProtocolCachePolicy（基础策略）
     NSURLRequestReloadIgnoringLocalCacheData（忽略本地缓存）
     NSURLRequestReturnCacheDataElseLoad（首先使用缓存，如果没有本地缓存，才从原地址下载）
     NSURLRequestReturnCacheDataDontLoad（使用本地缓存，从不下载，如果本地没有缓存，则请求失败，此策略多用于离线操作）
     NSURLRequestReloadIgnoringLocalAndRemoteCacheData（无视任何缓存策略，无论是本地的还是远程的，总是从原地址重新下载）
     NSURLRequestReloadRevalidatingCacheData（如果本地缓存是有效的则不下载，其他任何情况都从原地址重新下载）
     */
    // 设置请求方式为post, 默认为get
    [request setHTTPMethod:@"POST"];
    // 设置参数
    NSString* sReceipt = [receipt base64Encoding];
    NSString* str = [NSString stringWithFormat:@"order=%s&amount=%d&receipt=%@",
                     _iap->mPayOrder.c_str(), _iap->mAmount, sReceipt];
    //NSLog(@"data:%@", str);
    
    NSData* data = [str dataUsingEncoding:NSUTF8StringEncoding];
    [request setHTTPBody:data];
    
    //第三步，连接服务器
    
    NSError* err = nil;
    NSData *received = [NSURLConnection sendSynchronousRequest:request returningResponse:nil error:&err];
    //NSString* sResult = [[NSString alloc]initWithData:received encoding:NSUTF8StringEncoding];
    ////NSLog(@"rep:%@", sResult);
    
    if (err)
    {
        //NSLog(@"err:%@", err.description);
    }
    else
    {
        NSDictionary* dic = [NSJSONSerialization JSONObjectWithData:received options:NSJSONReadingMutableLeaves error:&err];
        if (err)
        {
            //NSLog(@"err:%@", err.description);
        }
        else
        {
            //NSLog(@"result-> status:%@ info:%@", [dic objectForKey:@"status"], [dic objectForKey:@"info"]);
            if ([[dic objectForKey:@"status"] intValue] == 0)
                return true;
        }
    }
    
    return false;
}

// 1.
- (void)paymentQueue:(SKPaymentQueue *)queue updatedTransactions:(NSArray *)transactions
{
    for (SKPaymentTransaction* transaction in transactions)
    {
        string identifier([transaction.payment.productIdentifier UTF8String]);
        IAPPaymentEvent e;
        
        //NSLog(@"updatedTransactions:%@", transaction.payment.productIdentifier);
        NSString *sNsErr = @"";

        switch (transaction.transactionState) {
            case SKPaymentTransactionStatePurchasing:
                //NSLog(@"updatedTransactions:SKPaymentTransactionStatePurchasing 购买中。。。");
                e = IAP_PAYMENT_PURCHASING;
                _iap->delegate->onPaymentEvent(identifier, transaction.payment.quantity, e, "");
                return;
            case SKPaymentTransactionStatePurchased:
                {
                    //NSLog(@"updatedTransactions:SKPaymentTransactionStatePurchased 进行服务端验证");
                    if ([self verify:transaction.transactionReceipt])
                    {
                        //NSLog(@"updatedTransactions:SKPaymentTransactionStatePurchased 购买完成");
                        e = IAP_PAYMENT_PURCHASED;
                    }
                    else
                    {
                        //NSLog(@"updatedTransactions:SKPaymentTransactionStatePurchased 验证失败");
                        e = IAP_PAYMENT_FAILED;
                        sNsErr = @"购买失败:购买凭据验证失败";
                        
                    }
                }
                break;
            case SKPaymentTransactionStateFailed:
                //NSLog(@"updatedTransactions:SKPaymentTransactionStateFailed 购买失败:[%d]%@", transaction.error.code, transaction.error.localizedDescription);

                switch (transaction.error.code) {
                    case SKErrorUnknown:
                        //NSLog(@"SKErrorUnknown");
                        sNsErr = @"购买失败:未知错误";
                        break;
                    case SKErrorClientInvalid:
                        //NSLog(@"SKErrorClientInvalid");
                        sNsErr = @"购买失败:客户端无法进行此操作";
                        break;
                        
                    case SKErrorPaymentCancelled:
                        //NSLog(@"SKErrorPaymentCancelled");
                        sNsErr = @"购买失败:取消购买";
                        break;
                    case SKErrorPaymentInvalid:
                        //NSLog(@"SKErrorPaymentInvalid");
                        sNsErr = @"购买失败:支付验证错误";
                        break;
                    case SKErrorPaymentNotAllowed:
                        //NSLog(@"SKErrorPaymentNotAllowed");
                        sNsErr = @"购买失败:不允许支付";
                        break;                        
                }
                e = IAP_PAYMENT_FAILED;
                break;
            case SKPaymentTransactionStateRestored:
                // NOTE: consumble payment is not restorable
                //NSLog(@"updatedTransactions:SKPaymentTransactionStateRestored 恢复购买");
                e = IAP_PAYMENT_RESTORED;
                break;
        }
        
        string sErr = [sNsErr UTF8String];
        _iap->delegate->onPaymentEvent(identifier, transaction.payment.quantity, e, sErr);
        
        if (e != IAP_PAYMENT_PURCHASING) {
            [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
        }
    }
} // paymentQueue

// 2
-(void)paymentQueue:(SKPaymentQueue *)queue removedTransactions:(NSArray *)transactions
{
    for (SKPaymentTransaction* transaction in transactions) {
        string identifier([transaction.payment.productIdentifier UTF8String]);
        //NSLog(@"removedTransactions:%@", transaction.payment.productIdentifier);
        _iap->delegate->onPaymentEvent(identifier, transaction.payment.quantity, IAP_PAYMENT_REMOVED, "");
    }
} // paymentQueue

@end

//////////////////////////////////////////////////////////////////////////////
// IAP 辅助类
IAPHelper::IAPHelper()
    : delegate(0)
    , skProducts(0)
    , skTransactionObserver(0)
{
    skTransactionObserver = [[iAPTransactionObserver alloc] init];
    ((iAPTransactionObserver*)skTransactionObserver).iap = this;
    [[SKPaymentQueue defaultQueue] addTransactionObserver:(iAPTransactionObserver*)skTransactionObserver];
}

IAPHelper::~IAPHelper()
{
    cleanup();
    
    [(iAPTransactionObserver*)skTransactionObserver release];
    [[SKPaymentQueue defaultQueue] removeTransactionObserver:(iAPTransactionObserver*)skTransactionObserver];
}

bool IAPHelper::canMakePayments()
{
    return [SKPaymentQueue canMakePayments];
}

void IAPHelper::cleanup()
{
    if (skProducts)
        [(NSArray*)skProducts release];
    skProducts = 0;
    
    vector<IAPProduct*>::iterator it = IAPProducts.begin();
    for (; it != IAPProducts.end(); ++it) {
        delete *it;
    }
    
    IAPProducts.clear();
}

void IAPHelper::requestProducts(std::vector<std::string> &productIdentitfiers)
{
    // 1
    NSMutableSet* set = [NSMutableSet setWithCapacity:productIdentitfiers.size()];
    
    vector<string>::iterator it = productIdentitfiers.begin();
    for (; it != productIdentitfiers.end(); ++it) {
        [set addObject:[NSString stringWithUTF8String:(*it).c_str()]];
    }
    
    // 2
    SKProductsRequest* productsRequest = [[SKProductsRequest alloc] initWithProductIdentifiers:set];
    
    // 3
    iAPProductsRequestDelegate* delegate = [[iAPProductsRequestDelegate alloc] init];
    delegate.iap = this;
    productsRequest.delegate = delegate;
    
    // 4
    [productsRequest start];
}

IAPProduct* IAPHelper::productByIdentifier(const std::string &identifier)
{
    vector<IAPProduct*>::iterator it = IAPProducts.begin();
    
    for (; it != IAPProducts.end(); ++it) {
        IAPProduct* product = *it;
        if (product->identifier == identifier)
            return product;
    }
    
    return 0;
}

void IAPHelper::paymentWithProduct(IAPProduct *product, int ammount, const std::string& sAccount, const std::string& sUserName, const std::string& sUserId, const std::string& payOrder)
{      
    //第一步，创建URL
    NSString *strUrl = [NSString stringWithFormat:@"http://czbuyu.io78.com/IphoneOrderBill.aspx?CrashUser=%s&CrashMoney=%d&p2_Order=%s", sAccount.c_str(), ammount, payOrder.c_str()];
    NSURL *url = [NSURL URLWithString:strUrl];
    
    //第二步，通过URL创建网络请求
    NSURLRequest *request = [[NSURLRequest alloc]initWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:10];
    //NSURLRequest初始化方法第一个参数：请求访问路径，第二个参数：缓存协议，第三个参数：网络请求超时时间（秒）
    /*
      其中缓存协议是个枚举类型包含：
      NSURLRequestUseProtocolCachePolicy（基础策略）
      NSURLRequestReloadIgnoringLocalCacheData（忽略本地缓存）
      NSURLRequestReturnCacheDataElseLoad（首先使用缓存，如果没有本地缓存，才从原地址下载）
      NSURLRequestReturnCacheDataDontLoad（使用本地缓存，从不下载，如果本地没有缓存，则请求失败，此策略多用于离线操作）
      NSURLRequestReloadIgnoringLocalAndRemoteCacheData（无视任何缓存策略，无论是本地的还是远程的，总是从原地址重新下载）
      NSURLRequestReloadRevalidatingCacheData（如果本地缓存是有效的则不下载，其他任何情况都从原地址重新下载）
    */
    //第三步，连接服务器
    NSData *received = [NSURLConnection sendSynchronousRequest:request returningResponse:nil error:nil];
    
    NSString *str = [[NSString alloc]initWithData:received encoding:NSUTF8StringEncoding];
    
    //NSLog(@"%@",str);
    
    if ([str UTF8String] != payOrder)
    {
        //NSLog(@"下订单失败,请稍后访问或联系客服");
        string sErr = [@"下订单失败,请稍后访问或联系客服" UTF8String];
        delegate->onPaymentEvent(product->identifier, 1, IAP_PAYMENT_FAILED, sErr);
        return;
    }
    
    mAccount    = sAccount;
    mUserName   = sUserName;
    mUserId     = sUserId;
    mPayOrder   = payOrder;
    mAmount     = ammount;

    // 发送订单
    paymentWithProduct(product, 1);
}

void IAPHelper::paymentWithProduct(IAPProduct *product, int quantity)
{
    SKProduct* skProduct = [(NSArray*)(skProducts) objectAtIndex:product->index];
    SKMutablePayment* payment = [SKMutablePayment paymentWithProduct:skProduct];
    payment.quantity = quantity;
    
    [[SKPaymentQueue defaultQueue] addPayment:payment];
}


std::string iosPlatformVersion()
{
    // app版本
    NSDictionary *infoDictionary = [[NSBundle mainBundle] infoDictionary];
    NSString *app_Version = [infoDictionary objectForKey:@"CFBundleVersion"];
    if (app_Version == 0)
        return "x.x.x";
    return [app_Version UTF8String];    
}