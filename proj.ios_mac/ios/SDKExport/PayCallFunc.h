//
//  PayViewController.h
//  DemoProject
//
//  Created by zhl on 2017/2/9.
//  Copyright © 2017年 zhl. All rights reserved.
//

#import <UIKit/UIKit.h>
#import<JftMergeSDK/JftMergeSDK.h>
@interface PayCallFunc : NSObject<JftSDKPayDelegate>
@property (nonatomic,copy,nonnull)NSArray *parArray;
@property (nonatomic,strong,nonnull) NSString *token;
@property (nonatomic,strong,nonnull) NSString *key;
@property (nonatomic,strong,nonnull) NSString *iv;
@property (nonatomic,strong,nonnull) NSString *serviceTType;
@property (nonatomic,strong,nonnull) NSString *appid;
@property (nonatomic,strong,nonnull) NSString *userCode;
@property (nonatomic,strong,nonnull) NSString *cmkey;
@end
