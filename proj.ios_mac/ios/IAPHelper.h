//
//  IAPHelper.h
//  Byds
//
//  Created by  Dima on 14-5-19.
//
//
#ifndef _IAPHelper_H_
#define _IAPHelper_H_

#include <vector>
#include <string>

/////////////////////////////////////////////////////////////////////////////////////
// IAP 产品
class IAPProduct
{
public:
    std::string identifier;
    std::string title;
    std::string description;
    std::string price;
    
    bool isValid;
    int  index; // internal use : index of skProductss
};

///////////////////////////////////////////////////////////////////////////////////
//
typedef enum {
    IAP_PAYMENT_PURCHASING, // just notify, ui do nothing
    IAP_PAYMENT_PURCHASED,  // need unlock app functionality
    IAP_PAYMENT_FAILED,     // remove waiting on ui, tall user payment was failed
    IAP_PAYMENT_RESTORED,   // need unlock app functionality, consumble payment no need to core about this
    IAP_PAYMENT_REMOVED,    // remove waiting on ui
}IAPPaymentEvent;

class IAPDelegate
{
public:
    virtual ~IAPDelegate() {}
    /**
     * 请求产品数据成功
     */
    virtual void onRequestProductsFinish() = 0;
    /**
     * 请求产品数据失败
     */
    virtual void onRequestProductsError(int code) = 0;
    /**
     * 购买产品结果
     * @param identitfier   产品标示
     * @param quantity      数量
     * @param e             结果类型
     * @param sErr          失败信息
     */
    virtual void onPaymentEvent(const std::string& identifier, int quantity, IAPPaymentEvent e, const std::string& sErr) = 0;
};

////////////////////////////////////////////////////////////////////////////////////
// IAP 辅助类
class IAPHelper
{
public:
    IAPHelper();
    ~IAPHelper();
    
    // 是否可以使用iap购买
    bool canMakePayments();
    // 请求产品数据
    void requestProducts(std::vector<std::string>& productIdentitfiers);
    // 根据产品标示获取产品信息
    IAPProduct* productByIdentifier(const std::string& identifier);
    // 购买产品
    void paymentWithProduct(IAPProduct* product, int quantity = 1);
    // 购买产品
    void paymentWithProduct(IAPProduct* product, int ammount, const std::string& sAccount, const std::string& sUserName, const std::string& sUserId, const std::string& payOrder);
    
    // 产品结果通知
    IAPDelegate* delegate;
    
    ///////////////////////////////////////////////////////////////
    // 临时数据
    std::string mAccount;
    std::string mUserName;
    std::string mUserId;
    std::string mPayOrder;
    int         mAmount;
    
    // use for oc class
    void cleanup();
    // SKProduct
    void *skProducts;
    // TransactionObserver
    void *skTransactionObserver;
    
    std::vector<IAPProduct*> IAPProducts;
};


std::string iosPlatformVersion();

#endif // _IAPHelper_H_