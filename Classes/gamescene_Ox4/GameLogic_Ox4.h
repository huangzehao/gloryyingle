#ifndef GAME_LOGIC_Ox4_HEAD_FILE
#define GAME_LOGIC_Ox4_HEAD_FILE

#pragma once

#include "Platform/PFDefine/df/types.h"


//////////////////////////////////////////////////////////////////////////
//宏定义

//数值掩码
#define	LOGIC_MASK_COLOR			0xF0								//花色掩码
#define	LOGIC_MASK_VALUE			0x0F								//数值掩码

//扑克类型
#define OX_VALUE0					0									//混合牌型
#define OX_FIVEKING_2				101									//银牛
#define OX_FIVEKING					102									//金牛
#define OX_THREE_SAME				103									//三条牌型
#define OX_FOUR_SAME				104									//四条牌型
//#define OX_FOURKING				104									//天王牌型

#define MAX_COUNT						5									//最大数目
//////////////////////////////////////////////////////////////////////////

//分析结构
struct tagAnalyseResult_ox4
{
	byte 							cbFourCount;						//四张数目
	byte 							cbThreeCount;						//三张数目
	byte 							cbDoubleCount;						//两张数目
	byte							cbSignedCount;						//单张数目
	byte 							cbFourLogicVolue[1];				//四张列表
	byte 							cbThreeLogicVolue[1];				//三张列表
	byte 							cbDoubleLogicVolue[2];				//两张列表
	byte 							cbSignedLogicVolue[5];				//单张列表
	byte							cbFourCardData[MAX_COUNT];			//四张列表
	byte							cbThreeCardData[MAX_COUNT];			//三张列表
	byte							cbDoubleCardData[MAX_COUNT];		//两张列表
	byte							cbSignedCardData[MAX_COUNT];		//单张数目
};



//////////////////////////////////////////////////////////////////////////

//游戏逻辑类
class GameLogic_Ox4
{
	//变量定义
private:
	static byte						g_cbCardData_Ox[52];				//扑克定义

	//函数定义
public:
	//构造函数
	GameLogic_Ox4();
	//析构函数
	virtual ~GameLogic_Ox4();

	//类型函数
public:
	//获取类型
	byte GetCardType(byte cbCardData[], byte cbCardCount, byte *bcOutCadData = NULL);
	//获取数值
	byte GetCardValue(byte cbCardData) { return cbCardData&LOGIC_MASK_VALUE; }
	//获取花色
	byte GetCardColor(byte cbCardData) { return cbCardData&LOGIC_MASK_COLOR; }
	//获取倍数
	byte GetTimes(byte cbCardData[], byte cbCardCount);
	//获取牛牛
	bool GetOxCard(byte cbCardData[], byte cbCardCount);
	//获取整数
	bool IsIntValue(byte cbCardData[], byte cbCardCount);

	//控制函数
public:
	//排列扑克
	void SortCardList(byte cbCardData[], byte cbCardCount);
	//混乱扑克
	void RandCardList(byte cbCardBuffer[], byte cbBufferCount);

	//功能函数
public:
	//逻辑数值
	byte GetCardLogicValue(byte cbCardData);
	//对比扑克
	bool CompareCard(byte cbFirstData[], byte cbNextData[], byte cbCardCount,bool FirstOX,bool NextOX);
};

//////////////////////////////////////////////////////////////////////////

#endif
