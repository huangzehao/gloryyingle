//#include "StdAfx.h"
#include "GameLogic_Ox4.h"
#include "Platform/PFDefine/df/Macro.h"

//////////////////////////////////////////////////////////////////////////

//扑克数据
byte GameLogic_Ox4::g_cbCardData_Ox[52]=
{
	0x01,0x02,0x03,0x04,0x05,0x06,0x07,0x08,0x09,0x0A,0x0B,0x0C,0x0D,	//方块 A - K
	0x11,0x12,0x13,0x14,0x15,0x16,0x17,0x18,0x19,0x1A,0x1B,0x1C,0x1D,	//梅花 A - K
	0x21,0x22,0x23,0x24,0x25,0x26,0x27,0x28,0x29,0x2A,0x2B,0x2C,0x2D,	//红桃 A - K
	0x31,0x32,0x33,0x34,0x35,0x36,0x37,0x38,0x39,0x3A,0x3B,0x3C,0x3D	//黑桃 A - K
};

//////////////////////////////////////////////////////////////////////////

//构造函数
GameLogic_Ox4::GameLogic_Ox4()
{
}

//析构函数
GameLogic_Ox4::~GameLogic_Ox4()
{
}

//获取类型
byte GameLogic_Ox4::GetCardType(byte cbCardData[], byte cbCardCount, byte *bcOutCadData)
{
	assert(cbCardCount==MAX_COUNT);

	//炸弹牌型
	byte bSameCount = 0;
	SortCardList(cbCardData,cbCardCount);
	byte bSecondValue = GetCardValue(cbCardData[MAX_COUNT/2]);
	for(byte i=0;i<cbCardCount;i++)
	{
		if(bSecondValue == GetCardValue(cbCardData[i]))
		{
			bSameCount++;
		}
	}
	if(bSameCount==4)
		return OX_FOUR_SAME;

	bool blBig = true;
	for (int i = 0;i<cbCardCount;i++)
	{
		byte bcGetValue = GetCardLogicValue(cbCardData[i]);
		if(bcGetValue!=10&&bcGetValue!=11)
		{

			blBig = false;
			break;

		}
	}
	if(blBig)
	{
		byte bKingCount1=0,bTenCount=0;
		for(byte i=0;i<cbCardCount;i++)
		{
			if(GetCardValue(cbCardData[i])>10)
			{
				bKingCount1++;
			}
			else if(GetCardValue(cbCardData[i])==10)
			{
				bTenCount++;
			}
		}
		if(bKingCount1==MAX_COUNT) 
		{
			return OX_FIVEKING; //金牛
		}
		else
		{

			if(bKingCount1==4&&bTenCount==1) return OX_FIVEKING_2; //银牛
		}


	}

	byte bTemp[MAX_COUNT];
	byte bSum=0;
	for (byte i=0;i<cbCardCount;i++)
	{
		bTemp[i]=GetCardLogicValue(cbCardData[i]);
		bSum+=bTemp[i];
	}

	for (byte i=0;i<cbCardCount-1;i++)
	{
		for (byte j=i+1;j<cbCardCount;j++)
		{
			if((bSum-bTemp[i]-bTemp[j])%10==0)
			{	
				if (bcOutCadData != NULL)
				{	
					bcOutCadData[4] = cbCardData[i];
					bcOutCadData[3] = cbCardData[j];
					int nCard = 2;
					for (int nn = 0; nn < 5; nn++)
					{
						if (nn != i && nn != j)
						{	
							bcOutCadData[nCard--] = cbCardData[nn];
						}
					}
				}
				return ((bTemp[i]+bTemp[j])>10)?(bTemp[i]+bTemp[j]-10):(bTemp[i]+bTemp[j]);
			}
		}
	}

	return OX_VALUE0;
}

//获取倍数
byte GameLogic_Ox4::GetTimes(byte cbCardData[], byte cbCardCount)
{
	if(cbCardCount!=MAX_COUNT)return 0;

	byte bTimes=GetCardType(cbCardData,MAX_COUNT);
	if(bTimes==OX_FOUR_SAME)return 10;
		else if(bTimes==OX_FIVEKING)return 6;//金牛
	else if(bTimes==OX_FIVEKING_2)return 5;
	else if(bTimes<7)return 1;
	else if(bTimes==7)return 2;
	else if(bTimes==8)return 2;
	else if(bTimes==9)return 3;
	else if(bTimes>=10)return 4;
	

	return 0;
}

//获取牛牛
bool GameLogic_Ox4::GetOxCard(byte cbCardData[], byte cbCardCount)
{
	assert(cbCardCount==MAX_COUNT);

	//设置变量
	byte bTemp[MAX_COUNT],bTempData[MAX_COUNT];
	memcpy(bTempData,cbCardData,sizeof(bTempData));
	byte bSum=0;
	for (byte i=0;i<cbCardCount;i++)
	{
		bTemp[i]=GetCardLogicValue(cbCardData[i]);
		bSum+=bTemp[i];
	}

	//查找牛牛
	for (byte i=0;i<cbCardCount-1;i++)
	{
		for (byte j=i+1;j<cbCardCount;j++)
		{
			if((bSum-bTemp[i]-bTemp[j])%10==0)
			{
				byte bCount=0;
				for (byte k=0;k<cbCardCount;k++)
				{
					if(k!=i && k!=j)
					{
						cbCardData[bCount++] = bTempData[k];
					}
				}assert(bCount==3);

				cbCardData[bCount++] = bTempData[i];
				cbCardData[bCount++] = bTempData[j];

				return true;
			}
		}
	}

	return false;
}

//获取整数
bool GameLogic_Ox4::IsIntValue(byte cbCardData[], byte cbCardCount)
{
	byte sum=0;
	for(byte i=0;i<cbCardCount;i++)
	{
		sum+=GetCardLogicValue(cbCardData[i]);
	}
	assert(sum>0);
	return (sum%10==0);
}

//排列扑克
void GameLogic_Ox4::SortCardList(byte cbCardData[], byte cbCardCount)
{
	//转换数值
	byte cbLogicValue[MAX_COUNT];
	for (byte i=0;i<cbCardCount;i++) cbLogicValue[i]=GetCardValue(cbCardData[i]);	

	//排序操作
	bool bSorted=true;
	byte cbTempData,bLast=cbCardCount-1;
	do
	{
		bSorted=true;
		for (byte i=0;i<bLast;i++)
		{
			if ((cbLogicValue[i]<cbLogicValue[i+1])||
				((cbLogicValue[i]==cbLogicValue[i+1])&&(cbCardData[i]<cbCardData[i+1])))
			{
				//交换位置
				cbTempData=cbCardData[i];
				cbCardData[i]=cbCardData[i+1];
				cbCardData[i+1]=cbTempData;
				cbTempData=cbLogicValue[i];
				cbLogicValue[i]=cbLogicValue[i+1];
				cbLogicValue[i+1]=cbTempData;
				bSorted=false;
			}	
		}
		bLast--;
	} while(bSorted==false);

	return;
}

//混乱扑克
void GameLogic_Ox4::RandCardList(byte cbCardBuffer[], byte cbBufferCount)
{
	//混乱准备
	byte cbCardData[CountArray(g_cbCardData_Ox)];
	memcpy(cbCardData,g_cbCardData_Ox,sizeof(g_cbCardData_Ox));

	//混乱扑克
	byte bRandCount=0,bPosition=0;
	do
	{
		bPosition=rand()%(CountArray(g_cbCardData_Ox)-bRandCount);
		cbCardBuffer[bRandCount++]=cbCardData[bPosition];
		cbCardData[bPosition]=cbCardData[CountArray(g_cbCardData_Ox)-bRandCount];
	} while (bRandCount<cbBufferCount);

	return;
}

//逻辑数值
byte GameLogic_Ox4::GetCardLogicValue(byte cbCardData)
{
	//扑克属性
	byte bCardColor=GetCardColor(cbCardData);
	byte bCardValue=GetCardValue(cbCardData);

	//转换数值
	return (bCardValue>10)?(10):bCardValue;
}

//对比扑克
bool GameLogic_Ox4::CompareCard(byte cbFirstData[], byte cbNextData[], byte cbCardCount,bool FirstOX,bool NextOX)
{
	if(FirstOX!=NextOX)return (FirstOX>NextOX);

	//比较牛大小
	if(FirstOX==TRUE)
	{
		//获取点数
		byte cbNextType=GetCardType(cbNextData,cbCardCount);
		byte cbFirstType=GetCardType(cbFirstData,cbCardCount);

		//点数判断
		if (cbFirstType!=cbNextType) return (cbFirstType>cbNextType);

		switch(cbNextType)
		{
		case OX_FOUR_SAME:		//炸弹牌型	
 		case OX_THREE_SAME:		//葫芦牌型
 			{
 				//排序大小
 				byte bFirstTemp[MAX_COUNT],bNextTemp[MAX_COUNT];
 				memcpy(bFirstTemp,cbFirstData,cbCardCount);
 				memcpy(bNextTemp,cbNextData,cbCardCount);
 				SortCardList(bFirstTemp,cbCardCount);
 				SortCardList(bNextTemp,cbCardCount);
 
 				return GetCardValue(bFirstTemp[MAX_COUNT/2])>GetCardValue(bNextTemp[MAX_COUNT/2]);
 
 				break;
 			}
		}
	}

	//排序大小
	byte bFirstTemp[MAX_COUNT],bNextTemp[MAX_COUNT];
	memcpy(bFirstTemp,cbFirstData,cbCardCount);
	memcpy(bNextTemp,cbNextData,cbCardCount);
	SortCardList(bFirstTemp,cbCardCount);
	SortCardList(bNextTemp,cbCardCount);

	//比较数值
	byte cbNextMaxValue=GetCardValue(bNextTemp[0]);
	byte cbFirstMaxValue=GetCardValue(bFirstTemp[0]);
	if(cbNextMaxValue!=cbFirstMaxValue)return cbFirstMaxValue>cbNextMaxValue;

	//比较颜色
	return GetCardColor(bFirstTemp[0]) > GetCardColor(bNextTemp[0]);

	return false;
}

//////////////////////////////////////////////////////////////////////////
