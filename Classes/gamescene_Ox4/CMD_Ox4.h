#ifndef CMD_OX4_HEAD_FILE
#define CMD_OX4_HEAD_FILE


//////////////////////////////////////////////////////////////////////////
//公共宏定义
#include "platform/PFDefine/df/types.h"
namespace Ox4
{
#pragma pack(push)  
#pragma pack(1)

#define KIND_ID						203										//游戏 I D
#define GAME_PLAYER					4										//游戏人数
#define GAME_NAME					TEXT("四人牛牛")							//游戏名字
#define VERSION_SERVER				PROCESS_VERSION(6,0,3)					//程序版本
#define VERSION_CLIENT				PROCESS_VERSION(6,0,3)					//程序版本
#define SERVER_LEN					32									//房间长度

#define GAME_GENRE					(GAME_GENRE_GOLD|GAME_GENRE_MATCH)		//游戏类型
#define MAXCOUNT					5										//扑克数目

//结束原因
#define GER_NO_PLAYER				0x10									//没有玩家

//游戏状态
#define GS_TK_FREE					GAME_STATUS_FREE                        //等待开始
#define GS_TK_CALL					GAME_STATUS_PLAY						//叫庄状态
#define GS_TK_SCORE					GAME_STATUS_PLAY+1						//下注状态
#define GS_TK_PLAYING				GAME_STATUS_PLAY+2						//游戏进行

//用户状态
#define USEX_NULL                   0                                       //用户状态
#define USEX_PLAYING                1                                       //用户状态
#define USEX_DYNAMIC                2                                       //用户状态   

//////////////////////////////////////////////////////////////////////////
//服务器命令结构

#define SUB_S_GAME_START				100									//游戏开始
#define SUB_S_ADD_SCORE					101									//加注结果
#define SUB_S_PLAYER_EXIT				102									//用户强退
#define SUB_S_SEND_CARD					103									//发牌消息
#define SUB_S_GAME_END					104									//游戏结束
#define SUB_S_OPEN_CARD					105									//用户摊牌
#define SUB_S_CALL_BANKER				106									//用户叫庄
#define SUB_S_ADMIN_COMMDN				107									//用户控制
#define SUB_S_CONTROLLIST				108									//用户控制

//游戏状态
struct CMD_S_StatusFree
{
	int64							lCellScore;							//基础积分
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID || CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
	tchar								szGameRoomName[SERVER_LEN * 2];
#elif (CC_TARGET_PLATFORM == CC_PLATFORM_WIN32)
	tchar								szGameRoomName[SERVER_LEN];
#endif

};

//游戏状态
struct CMD_S_StatusCall
{
	word							    	wCallBanker;						//叫庄用户
	byte							        cbPlayStatus[GAME_PLAYER];          //用户状态
};

//游戏状态
struct CMD_S_StatusScore
{
	//下注信息
	int64								lTurnMaxScore;						//最大下注
	//int64								lTurnLessScore;						//最小下注
	int64								lTableScore[GAME_PLAYER];			//下注数目
	byte								    cbPlayStatus[GAME_PLAYER];          //用户状态
	word							    	wBankerUser;						//庄家用户
};

//游戏状态
struct CMD_S_StatusPlay
{
	//状态信息	
	byte								    cbPlayStatus[GAME_PLAYER];          //用户状态
	int64								lTurnMaxScore;						//最大下注
	//int64								lTurnLessScore;						//最小下注
	int64								lTableScore[GAME_PLAYER];			//下注数目
	word								    wBankerUser;						//庄家用户

	//扑克信息
	byte							    	cbHandCardData[GAME_PLAYER][MAXCOUNT];//桌面扑克
	byte						      		bOxCard[GAME_PLAYER];				//牛牛数据

	//TCHAR							szGameRoomName[SERVER_LEN];			//房间名称
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID || CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
	tchar								szChatMessage[SERVER_LEN * 2];
#elif (CC_TARGET_PLATFORM == CC_PLATFORM_WIN32)
	tchar								szChatMessage[SERVER_LEN];			
#endif
};

//用户叫庄
struct CMD_S_CallBanker
{
	word							     	wCallBanker;						//叫庄用户
	bool							    	bFirstTimes;						//首次叫庄
};

//游戏开始
struct CMD_S_GameStart
{
	//下注信息
	int64								lTurnMaxScore;						//最大下注
	word							     	wBankerUser;						//庄家用户
};

//用户下注
struct CMD_S_AddScore
{
	word							    	wAddScoreUser;						//加注用户
	int64								lAddScoreCount;						//加注数目
};

//游戏结束
struct CMD_S_GameEnd
{
	int64								lGameTax[GAME_PLAYER];				//游戏税收
	int64								lGameScore[GAME_PLAYER];			//游戏得分
	byte							     	cbCardData[GAME_PLAYER];			//用户扑克
};

//发牌数据包
struct CMD_S_SendCard
{
	byte								    cbCardData[GAME_PLAYER][MAXCOUNT];	//用户扑克
};

//用户退出
struct CMD_S_PlayerExit
{
	word						      		wPlayerID;							//退出用户
};

//用户摊牌
struct CMD_S_Open_Card
{
	word							     	wPlayerID;							//摊牌用户
	byte							      	bOpen;								//摊牌标志
};
////机器人信息
//struct tagRobotInfo
//{
//	TCHAR szCfgFileName[MAX_PATH];										//配置文件
//	int	nMaxTime;														//最大赔率
//
//	tagRobotInfo()
//	{
//		TCHAR szTmpCfgFileName[MAX_PATH] = _TEXT("srnn.ini");
//		nMaxTime = 1;
//		memcpy(szCfgFileName, szTmpCfgFileName, sizeof(szCfgFileName));
//	}
//};
//////////////////////////////////////////////////////////////////////////
//客户端命令结构
#define SUB_C_CALL_BANKER				1									//用户叫庄
#define SUB_C_ADD_SCORE					2									//用户加注
#define SUB_C_OPEN_CARD					3									//用户摊牌
#define SUB_C_ADMIN_COMMDN				4									//系统控制

//用户叫庄
struct CMD_C_CallBanker
{
	byte							    	bBanker;							//做庄标志
};

//用户加注
struct CMD_C_AddScore
{
	int64								lScore;								//加注数目
};

//用户摊牌
struct CMD_C_OxCard
{
	byte							    	bOX;								//牛牛标志
};


//////////////////////////////////////////////////////////////////////////
//控制
//服务器控制返回
#define	 S_CR_FAILURE				0		//失败
#define  S_CR_UPDATE_SUCCES			1		//更新成功
#define	 S_CR_ADD_SUCCESS			2		//添加成功
#define  S_CR_ADD_FAILURE			3		//添加失败
#define  S_CR_DELETE_SUCCESS		4		//删除成功
#define  S_CR_DELETE_FAILURE		5		//删除失败
struct CMD_S_ControlReturns
{
	byte cbReturnsType;				//回复类型

	dword dwUserID;

	int StartDate;
	int EndDate;
	int StartTime;
	int EndTime;

	int64 lTargetScore;
	int64 lControledScore;
	int	iUserWinRate;
};


//客户端控制申请
#define  C_CA_UPDATE				1		//更新
#define	 C_CA_ADD					2		//添加
#define  C_CA_DELETE				3		//删除
struct CMD_C_ControlApplication
{
	byte cbControlAppType;			//申请类型

	dword dwUserID;

	int StartDate;
	int EndDate;
	int StartTime;
	int EndTime;

	int64 lTargetScore;
	int	iUserWinRate;

};

#pragma pack(pop)

}
//////////////////////////////////////////////////////////////////////////

#endif
