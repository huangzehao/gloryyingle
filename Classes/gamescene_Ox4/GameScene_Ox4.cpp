#include "ClientKernelSink_Ox4.h"
#include "GameScene_Ox4.h"
#include "PlayerLayer_Ox4.h"
#include "Tools/tools/MTNotification.h"
#include "common/KeybackLayer.h"
#include "Tools/tools/SimpleTools.h"
#include "Tools/ViewHeader.h"
#include "Tools/Manager/SoundManager.h"
#include "Tools/tools/gPlatform.h"
#include "Tools/tools/StringData.h"
#include "Kernel/kernel/game/IClientKernel.h"
#include "Tools/tools/StaticData.h"
#include "Kernel/kernel/server/IServerItemSink.h"
#include "Tools/Dialog/NewDialog.h"
#include "Platform/PFView/ServerScene/ServerScene.h"
#include "Platform/PFView/ModeScene/ModeScene.h"
using namespace Ox4;

#define  P_WIDTH      1420
#define  P_WIDTH_2    P_WIDTH/2

#define  P_HEIGHT     800
#define  P_HEIGHT_2   P_HEIGHT/2

#define TAG_BT_CLOSE			1
#define TAG_BT_ADD_CANNON_MULTI	2
#define TAG_BT_SUB_CANNON_MULTI	3
#define TAG_BT_ADD_CANNON_SCORE	4
#define TAG_BT_SUB_CANNON_SCORE	5
#define TAG_BT_GMCTR			6

bool isOx4ReconnectOnLoss = false;
//////////////////////////////////////////////////////////////////////////
static GameScene_Ox4* __gGameScene_Ox4 = 0;

GameScene_Ox4* GameScene_Ox4::shared()
{
	return __gGameScene_Ox4;
}
//////////////////////////////////////////////////////////////////////////
GameScene_Ox4::GameScene_Ox4()
{
	__gGameScene_Ox4 = this;
	mMsgContentIndex = 0;

	char sSound[32] = { 0 };
	sprintf(sSound, "BGM_NORMAL_1");
	SoundManager::shared()->playMusic(sSound);

}

GameScene_Ox4::~GameScene_Ox4()
{
	__gGameScene_Ox4 = 0;
	if(touch_layer_)
	{
	touch_layer_->setTouchEnabled(false);
	touch_layer_->setSink(0);
	touch_layer_->release();
	}

	char sSound[32] = { 0 };
	sprintf(sSound, "BGM_NORMAL_1");
	SoundManager::shared()->stopMusic();

	G_NOTIFY_UNREG("GAME_CLOSE");
	G_NOTIFY_UNREG("EVENT_GAME_EXIT");

	mFrameLayer4 = nullptr;
	mPlayerLayer4 = nullptr;

	SoundManager::shared()->stopMusic();
	PLAY_PLATFORM_BG_MUSIC
}

//初始化方法
bool GameScene_Ox4::init()
{
	do 
	{	
		CC_BREAK_IF(!Scene::init());
		
		addChild(KeybackLayer::create());

		is_touch_fire_ = false;

		int iZorder = -500;

		m_OxPoint[0] = ccp(703, 680);
		m_OxPoint[1] = ccp(1040, 415);
		m_OxPoint[2] = ccp(680, 160);
		m_OxPoint[3] = ccp(300, 410);

		mPlayerLayer4 = PlayerLayer_Ox4::create();//玩家层
		this->addChild(mPlayerLayer4,1);

		mFrameLayer4 = FrameLayer_Ox4::create();//按钮层
		this->addChild(mFrameLayer4,0);
		mFrameLayer4->setCloseDialogInfo(this, callfuncN_selector(GameScene_Ox4::closeCallback), SSTRING("back_to_room"), SSTRING("back_to_room_content"));

		m_Card.setCardArrayWithRow("ox4GameScene/Resource/game_card-hd.png", CSpriteCard::g_cbCardData, 55);

		SoundManager::shared()->stopMusic();
		SoundManager::shared()->playMusic("Ox4_BACK_MUSIC");

		mMessageLayer = MessageLayer::create();
		mMessageLayer->setName("mMessageLayer");
		addChild(mMessageLayer, 100);

		auto kernel = IClientKernel::get();
		if (kernel)
		{
			kernel->SetChatSink(mMessageLayer);
			kernel->SetStringMessageSink(mMessageLayer);
		}
		
		///< 关闭游戏
		G_NOTIFY_REG("EVENT_GAME_EXIT", GameScene_Ox4::closeGameNotify);
		///< 断线重连
		G_NOTIFY_REG("RECONNECT_ON_LOSS", GameScene_Ox4::func_Reconnect_on_loss);

		this->setName("GameScene");
		this->setTag(KIND_ID);

		return true;
	} while (0);

	return false;
}
void GameScene_Ox4::createButtonUserReday()
{

	for (int i = 0; i < 4; i ++)
	{
		PlayerNode_Ox4 * node = mPlayerLayer4->getPlayerNodeById(i);
		node->clearUser(false);

	}
	mFrameLayer4->showMenuItem(FrameLayer_Ox4::ITEM_TAG_START, true);
	mFrameLayer4->showMenuItem(FrameLayer_Ox4::ITEM_TAG_REQUIRE_BANKSER, false);
	mFrameLayer4->showMenuItem(FrameLayer_Ox4::ITEM_TAG_SHOW_HARD, false);
	mFrameLayer4->showMenuItem(FrameLayer_Ox4::ITEM_TAG_ABORT, false);
	mFrameLayer4->showWaitPanelContent(false);

}

void GameScene_Ox4::freeState()
{

	mFrameLayer4->showMenuItem(FrameLayer_Ox4::ITEM_TAG_START, true);
	mFrameLayer4->showMenuItem(FrameLayer_Ox4::ITEM_TAG_REQUIRE_BANKSER, false);
	mFrameLayer4->showMenuItem(FrameLayer_Ox4::ITEM_TAG_SHOW_HARD, false);
	mFrameLayer4->showMenuItem(FrameLayer_Ox4::ITEM_TAG_ABORT, false);
	mFrameLayer4->showWaitPanelContent(false);

}

void GameScene_Ox4::callBankerState(int chair_id)
{
	IClientKernel * kernel = IClientKernel::get();

	if (kernel->GetMeChairID() == chair_id)
	{
		mFrameLayer4->showMenuItem(FrameLayer_Ox4::ITEM_TAG_START, false);
		mFrameLayer4->showMenuItem(FrameLayer_Ox4::ITEM_TAG_REQUIRE_BANKSER, true);
		mFrameLayer4->showMenuItem(FrameLayer_Ox4::ITEM_TAG_SHOW_HARD, false);
		mFrameLayer4->showMenuItem(FrameLayer_Ox4::ITEM_TAG_ABORT, true);
	}	
	
	mFrameLayer4->showWaitPanelContent(true);
	mFrameLayer4->setWaitPanelContent(2);
}

void GameScene_Ox4::addGoldState(int chair_id, long long maxValue)
{
	IClientKernel * kernel = IClientKernel::get();

	if (kernel->GetMeChairID() == chair_id)
	{
		mFrameLayer4->showMenuItem(FrameLayer_Ox4::ITEM_TAG_START, false);
		mFrameLayer4->showMenuItem(FrameLayer_Ox4::ITEM_TAG_REQUIRE_BANKSER, false);
		mFrameLayer4->showMenuItem(FrameLayer_Ox4::ITEM_TAG_SHOW_HARD, false);
		mFrameLayer4->showMenuItem(FrameLayer_Ox4::ITEM_TAG_ABORT, false);
		mFrameLayer4->setAddScoreMaxValue(maxValue);
	}

	mFrameLayer4->showWaitPanelContent(true);
	mFrameLayer4->setWaitPanelContent(4);
}

void GameScene_Ox4::playingState(int addGoldChair_id, long long addGoldScore)
{
	IClientKernel * kernel = IClientKernel::get();

	if (kernel)
	{
		mFrameLayer4->showMenuItem(FrameLayer_Ox4::ITEM_TAG_START, false);
		mFrameLayer4->showMenuItem(FrameLayer_Ox4::ITEM_TAG_REQUIRE_BANKSER, false);
		mFrameLayer4->showMenuItem(FrameLayer_Ox4::ITEM_TAG_SHOW_HARD, false);
		mFrameLayer4->showMenuItem(FrameLayer_Ox4::ITEM_TAG_SHOW_HARD, true);
		mFrameLayer4->showMenuItem(FrameLayer_Ox4::ITEM_TAG_ABORT, false);

		mFrameLayer4->showWaitPanelContent(true);

		PlayerNode_Ox4 * node = nullptr;
		IClientUserItem* pClientUserItem = kernel->GetTableUserItem(kernel->GetMeChairID());
		int my_usid = kernel->SwitchViewChairID(pClientUserItem->GetChairID());
		node = mPlayerLayer4->getPlayerNodeById(my_usid);
		node->setShowGoldKuang(true);
		node->setBuyMoney(addGoldScore);
	}
}

/// 游戏状态, 自己已经梭哈
void GameScene_Ox4::playingState(bool me_alery_showhard)
{
	mFrameLayer4->showMenuItem(FrameLayer_Ox4::ITEM_TAG_SHOW_HARD, !me_alery_showhard);
}


///< 显示用户信息
void GameScene_Ox4::showUserInfo(IClientUserItem *pIClientUserItem, byte cbUserStatus)
{
	IClientKernel* kernel = IClientKernel::get();

	PlayerNode_Ox4 * node = nullptr;

	node = mPlayerLayer4->getPlayerNodeById(kernel->SwitchViewChairID(pIClientUserItem->GetChairID()));
	node->setNickName(pIClientUserItem->GetNickName());
	node->setMoney(pIClientUserItem->GetUserScore());

	if (pIClientUserItem->GetUserStatus() == US_READY)
	{
		node->setIsPrepare(true);
		showSettlementFrame(pIClientUserItem->GetChairID(), false);
	}	
	else
		node->setIsPrepare(false);
	node->setIsZhuang(false);
	node->setShowGoldKuang(false);
	node->setFaceID(pIClientUserItem->GetFaceID());
	node->setHeadInfo(true);
	///< 清理牌信息
	this->clearCardData(pIClientUserItem->GetChairID());
}

void GameScene_Ox4::showBankerWithIndex(int Index)
{
	Label *pLableUserScore1 = Label::createWithSystemFont("庄家", "Arial", 40);
	pLableUserScore1->setTag(203);
	pLableUserScore1->setAnchorPoint(ccp(0.0f, 0.5f));
	pLableUserScore1->setPosition(m_BankerPoint[Index]);
	addChild(pLableUserScore1);

}
/// 显示加分按钮(只有第一个参数有效)
void GameScene_Ox4::ShowAddButton(longlong	Score)
{
	mFrameLayer4->setAddScoreMaxValue(Score);

}
void GameScene_Ox4::showAddScoreTips(longlong	Score,int userIndex,bool Tybe)
{

	IClientKernel* kernel = IClientKernel::get();
	PlayerNode_Ox4* node = nullptr;

	node = mPlayerLayer4->getPlayerNodeById(kernel->SwitchViewChairID(userIndex));
	node->setShowGoldKuang(true);
	node->setBuyMoney(Score);
}

void GameScene_Ox4::showGameEndScore(int userName1,longlong lScore1, int type,int conunt)
{
	IClientKernel* kernel = IClientKernel::get();
	IClientUserItem* pClientUserItem1= kernel->GetTableUserItem(userName1);
	PlayerNode_Ox4 * node = nullptr;

	node = mPlayerLayer4->getPlayerNodeById(kernel->SwitchViewChairID(pClientUserItem1->GetChairID()));
	if (lScore1 != 0)
		node->setShowGoldKuang(true);
	node->setBuyMoney(lScore1);

	if (userName1 == kernel->GetMeChairID())
	{
		if (lScore1 > 0)
			SoundManager::shared()->playSound("GAME_WIN");
		else
			SoundManager::shared()->playSound("GAME_LOST");
	}
}


void GameScene_Ox4::showScoreTips(int charId, int64 score)
{
	IClientKernel* kernel = IClientKernel::get();

	PlayerNode_Ox4 * node = nullptr;
	node = mPlayerLayer4->getPlayerNodeById(kernel->SwitchViewChairID(charId));

	if (score == 0)
		node->setShowGoldKuang(false);
	else
		node->setShowGoldKuang(true);
	node->setBuyMoney(score);
}

void GameScene_Ox4::ccButtonAdd0(Ref* obj,Control::EventType e)
{
	gClientKernelSink_Ox4.OnMeAddGold(8);
}
void GameScene_Ox4::ccButtonAdd1(Ref* obj,Control::EventType e)
{
	gClientKernelSink_Ox4.OnMeAddGold(4);
}
void GameScene_Ox4::ccButtonAdd2(Ref* obj,Control::EventType e)
{
	gClientKernelSink_Ox4.OnMeAddGold(2);
}
void GameScene_Ox4::ccButtonAdd3(Ref* obj,Control::EventType e)
{
	gClientKernelSink_Ox4.OnMeAddGold(1);
}
void GameScene_Ox4::ccButtonAdd4(Node* sender)
{
	gClientKernelSink_Ox4.OnMeAddGold(4);
}
void GameScene_Ox4::ccButtonAdd5(Node* sender)
{
	gClientKernelSink_Ox4.OnMeGive(0);
}

/// 开始发送手牌
void GameScene_Ox4::sendCardStart()
{
	mFrameLayer4->showWaitPanelContent(false);

		IClientKernel* kernel = IClientKernel::get();
		float delay = 0;
		m_xxx = 0;
		for (byte j = 0; j < 5; j++)
		{
			for (byte i = 0; i < GAME_PLAYER_Ox4; i++)
			{
				IClientUserItem* pClientUserItem = kernel->GetTableUserItem(i);
				int my_usid = kernel->SwitchViewChairID(i);

				if (pClientUserItem != NULL)
				{
					CSpriteCard* xxx = m_Card.getCardWithBack();
					xxx->setPosition(ccp(P_WIDTH_2, P_HEIGHT_2 + 50));
					addChild(xxx, j);
					xxx->setScale(0);

 					if (pClientUserItem->GetUserStatus() == US_PLAYING)
 					{

						if (i == kernel->GetMeChairID())
						{
							SoundManager::shared()->playSound("SEND_CARD");
							xxx->setMark(gClientKernelSink_Ox4.m_cbHandCardData[i][j]);
							xxx->setTag(8520 + my_usid * 5 + j);
						
							xxx->runAction(CCSequence::create(DelayTime::create(delay), ScaleTo::create(0.01, 1, 1), CCMoveTo::create(0.2f, ccp(gClientKernelSink_Ox4.m_CardPoint[my_usid].x + j * 35, gClientKernelSink_Ox4.m_CardPoint[my_usid].y)), CCCallFuncN::create(this, callfuncN_selector(GameScene_Ox4::ShowMeCard)), NULL));

						}
						else
						{
							xxx->setTag(8520 + my_usid * 5 + j);
							xxx->runAction(CCSequence::create(DelayTime::create(delay), ScaleTo::create(0.01, 1, 1), CCMoveTo::create(0.2f, ccp(gClientKernelSink_Ox4.m_CardPoint[my_usid].x + j * 35, gClientKernelSink_Ox4.m_CardPoint[my_usid].y)), NULL));

			     		}
				  }
			}
		}
	}
}


///< 直接让牌在指定的位置
void GameScene_Ox4::sendCardToPoint()
{

	IClientKernel* kernel = IClientKernel::get();
	m_xxx = 0;
	for (byte j = 0; j < 5; j++)
	{
		for (byte i = 0; i < GAME_PLAYER_Ox4; i++)
		{
			IClientUserItem* pClientUserItem = kernel->GetTableUserItem(i);
			int my_usid = kernel->SwitchViewChairID(i);
			if (pClientUserItem != NULL)
			{
				if (pClientUserItem->GetUserStatus() == US_PLAYING)
				{
				
			      if (i == kernel->GetMeChairID())
			        {
			 	         CSpriteCard* xxx = m_Card.getCardWithData(gClientKernelSink_Ox4.m_cbHandCardData[i][j]);
			 	          SoundManager::shared()->playSound("SEND_CARD");
			 	           xxx->setTag(8520 + my_usid * 5 + j);
						  xxx->setPosition(ccp(gClientKernelSink_Ox4.m_CardPoint[my_usid].x + j * 35, gClientKernelSink_Ox4.m_CardPoint[my_usid].y));
			              	addChild(xxx);
			        }
			        else
			        {
			 	             CSpriteCard* xxx = m_Card.getCardWithBack();
			 	           addChild(xxx);
			 	           xxx->setTag(8520 + my_usid * 5 + j);
						   xxx->setPosition(ccp(gClientKernelSink_Ox4.m_CardPoint[my_usid].x + j * 35, gClientKernelSink_Ox4.m_CardPoint[my_usid].y));
		   
			       }
			   }
			}
		}
	}
}

///< 显示自己的手牌
void GameScene_Ox4::ShowMeCard(CCNode* sender)
{
	SoundManager::shared()->playSound("ox4_SEND_CARD");
	CSpriteCard* xxx2 = dynamic_cast<CSpriteCard*>(sender);
	byte CardMark=xxx2->getMark();
	if(CardMark!=0)
	{
		CSpriteCard* xxx1 = m_Card.getCardWithData(CardMark);
		xxx1->setPosition(xxx2->getPosition());
		xxx2->removeFromParent();
		addChild(xxx1, 1);
		xxx1->setTag(xxx2->getTag());

		if(m_xxx==0)
		{
			showTanpaiButton();
			removeChildByTag(204);			
			showTipsWithType(0);
			m_xxx=1;
		}
	}
}
void GameScene_Ox4::showTanpaiButton()
{
	mFrameLayer4->showMenuItem(FrameLayer_Ox4::ITEM_TAG_SHOW_HARD, true);
	mFrameLayer4->showMenuItem(FrameLayer_Ox4::ITEM_TAG_ABORT, false);
	mFrameLayer4->showMenuItem(FrameLayer_Ox4::ITEM_TAG_START, false);
	mFrameLayer4->showMenuItem(FrameLayer_Ox4::ITEM_TAG_REQUIRE_BANKSER, false);
}
void GameScene_Ox4::ccButtonUser(Ref* obj,Control::EventType e)
{
	gClientKernelSink_Ox4.OnMeGive(0);
	removeChildByTag(1313);
	removeChildByTag(1314);
}
void GameScene_Ox4::ccButtonUserNo(Ref* obj,Control::EventType e)
{
	gClientKernelSink_Ox4.OnMeGive(1);
}
//显示牛牛牌类型
void GameScene_Ox4::showOxCardType(int cardtype,int index,int type)
{
	if (getChildByTag(600 + index))
	{
		removeChildByTag(600 + index);
	}
	if (type == 0)
		return;

	Texture2D* pTexture = TextureCache::sharedTextureCache()->addImage("ox4GameScene/Resource/game_cardtype_ox.png");

	//创建牛牛图标
	Sprite* Chip = TouchSprite::spriteImageWithTexture(pTexture, 14, cardtype);

	IClientKernel* kernel = IClientKernel::get();
	if (index == 2)
	{
		Chip->setScale(1.2);
		Chip->setPosition(m_OxPoint[index] + ccp(0, 0));
	}
	else
	{
		Chip->setScale(1);
		Chip->setPosition(m_OxPoint[index] + ccp(0, 0));
	}
	Chip->setTag(600 + index);
	this->addChild(Chip, 2);

	return;
}
void GameScene_Ox4::showTipsWithType(int Index)
{
	if(Index==1)
	{
		mFrameLayer4->setWaitPanelContent(4);
	}
	else
	{
		mFrameLayer4->setWaitPanelContent(2);
	}

}
void GameScene_Ox4::showCallBankerButton()
{
	mFrameLayer4->showMenuItem(FrameLayer_Ox4::ITEM_TAG_START, false);
	mFrameLayer4->showMenuItem(FrameLayer_Ox4::ITEM_TAG_REQUIRE_BANKSER, true);
	mFrameLayer4->showMenuItem(FrameLayer_Ox4::ITEM_TAG_ABORT, true);

}
void GameScene_Ox4::ccButtonUserCall(Ref* obj,Control::EventType e)
{
	gClientKernelSink_Ox4.ButtonWithCallBanker();
	removeChildByTag(201);
	removeChildByTag(202);
}
void GameScene_Ox4::ccButtonUserNoCall(Ref* obj,Control::EventType e)
{
	gClientKernelSink_Ox4.ButtonWithNoCall();
}
void GameScene_Ox4::showWaitCallBankerWithIndex()
{
	mFrameLayer4->showWaitPanelContent(true);
	mFrameLayer4->setWaitPanelContent(2);
}
void GameScene_Ox4::onEnter()
{
	Scene::onEnter();
	if(KeybackLayer::getInstance())
	{
		KeybackLayer::getInstance()->addKeybackListener(this);
	}
}
void GameScene_Ox4::onExit()
{
	if(KeybackLayer::getInstance())
	{
		KeybackLayer::getInstance()->removeKeybackListener(this);
	}
	Scene::onExit();
}

///< 返回大厅
void GameScene_Ox4::onKeybackClicked()
{	
	popup(SSTRING("system_tips_title"),SSTRING("back_to_chair_select"),DLG_MB_OK|DLG_MB_CANCEL, 0, this, callfuncN_selector(GameScene_Ox4::onBackToRoom));
}


void GameScene_Ox4::onBackToRoom(Node* pNode)
{
	switch (pNode->getTag())
	{
	case DLG_MB_OK:
		{
			if (IClientKernel::get())
				IClientKernel::get()->Intermit(GameExitCode_Normal);
			return ;
		}
		break;
	}
}

void GameScene_Ox4::onBtnClick(Ref* obj,Control::EventType e)
{
	Control* ctr = (Control*)obj;
	switch (ctr->getTag())
	{
	case TAG_BT_CLOSE:
		{
			if (IClientKernel::get())
				IClientKernel::get()->Intermit(GameExitCode_Normal);
			return ;
		}
		break;
	default:
		break;
	}
}

void GameScene_Ox4::onEnterTransitionDidFinish()
{
	Scene::onEnterTransitionDidFinish();
	auto ik = IClientKernel::get();
	if (ik)
	{
		ik->SendGameOption();
		startUpdate();
	}
	
	touch_layer_=0;

	EventListenerTouchOneByOne* mListener = EventListenerTouchOneByOne::create();
	mListener->setSwallowTouches(true);
	mListener->onTouchBegan = CC_CALLBACK_2(GameScene_Ox4::onTouchBegan, this);
	mListener->onTouchMoved = CC_CALLBACK_2(GameScene_Ox4::onTouchMoved, this);
	mListener->onTouchEnded = CC_CALLBACK_2(GameScene_Ox4::onTouchEnded, this);
	_eventDispatcher->addEventListenerWithSceneGraphPriority(mListener, this);

}

void GameScene_Ox4::onExitTransitionDidStart()
{	
	stopUpdate();
	Scene::onExitTransitionDidStart();
}

void GameScene_Ox4::startUpdate()
{
	stopUpdate();
	schedule(schedule_selector(GameScene_Ox4::frameUpdate));
}

void GameScene_Ox4::stopUpdate()
{
	unschedule(schedule_selector(GameScene_Ox4::frameUpdate));
}

void GameScene_Ox4::frameUpdate(float dt)
{
	gClientKernelSink_Ox4.OnUpdate(dt);

}

void GameScene_Ox4::closeCallback(cocos2d::Node* obj)
{
	switch (obj->getTag())
	{
	case DLG_MB_OK:
	{
					  if (IClientKernel::get())
						  IClientKernel::get()->Intermit(GameExitCode_Normal);

	}
		break;
	}
}

/////////////////////////////////////////////////////////////////////////////////

void GameScene_Ox4::func_msg_show()
{
	msg_start_next(false);
}

void GameScene_Ox4::func_msg_start_next()
{
	msg_start_next(true);
}
// 添加消息
void GameScene_Ox4::add_msg(const std::string& msg)
{
	mMessageList.push_back(msg);

	if (mMessageList.size() == 1)
	{
		spMessage_bg->setVisible(true);
		spMessage_bg->setOpacity(0);
		spMessage_bg->stopAllActions();
		spMessage_bg->runAction(Sequence::create(
			FadeIn::create(0.3f),
			CallFunc::create(CC_CALLBACK_0(GameScene_Ox4::func_msg_show,this)),
			0));
	}
}


void GameScene_Ox4::msg_start_next(bool bRemoveFront)
{
	if (bRemoveFront && !mMessageList.empty())
		mMessageList.pop_front();

	if (mMessageList.empty())
	{
		spMessage_bg->stopAllActions();
		spMessage_bg->runAction(Sequence::create(
			FadeOut::create(0.3f),
			Hide::create(),
			0));
		return;
	}

	int index = mMsgContentIndex;
	mMsgContentIndex = (mMsgContentIndex+1)%2;
	Label* label = mMsgContents[index];

	label->setString(mMessageList.front().c_str());


	Size cs = spMessage_bg->getContentSize();
	Size csMsg = label->getContentSize();
	if (cs.width >= csMsg.width)
	{
		label->setPosition(Point(cs.width/2, cs.height/2+20));

		label->setVisible(true);
		label->runAction(Sequence::create(
			Spawn::create(FadeIn::create(0.2f), MoveTo::create(0.2f, Point(cs.width/2, cs.height/2)), 0),
			DelayTime::create(3),
			CallFunc::create(CC_CALLBACK_0(GameScene_Ox4::func_msg_start_next,this)),
			Spawn::create(CCFadeOut::create(0.2f), MoveTo::create(0.2f, Point(cs.width/2, cs.height/2-20)), 0),
			Hide::create(), 
			0));
	}
	else
	{
		float lenMoveSpeed = 0.01f;
		float len = (csMsg.width - cs.width) / 2;
		label->setPosition(Point(cs.width/2 + len, cs.height/2+20));

		label->setVisible(true);
		label->runAction(Sequence::create(
			Spawn::create(FadeIn::create(0.2f), MoveTo::create(0.2f, Point(cs.width/2 + len, cs.height/2)), 0),
			DelayTime::create(2),
			MoveTo::create(lenMoveSpeed * len * 2, Point(cs.width/2 - len, cs.height/2)),
			DelayTime::create(2),
			CallFunc::create(CC_CALLBACK_0(GameScene_Ox4::func_msg_start_next,this)),
			Spawn::create(CCFadeOut::create(0.2f), MoveTo::create(0.2f, Point(cs.width/2 - len, cs.height/2-20)), 0),
			Hide::create(), 
			0));
	}
}
//////////////////////////////////////////////////////////////////////////
bool GameScene_Ox4::onTouchBegan(Touch *pTouch, Event *pEvent)
{
	IClientKernel* kernel = IClientKernel::get();

	if (kernel==0)
		return false;

	Point pt = locationFromTouch(pTouch);

	return true;

}

void GameScene_Ox4::onTouchMoved(Touch *pTouch, Event *pEvent)
{
	IClientKernel* kernel = IClientKernel::get();

	if (kernel==0)
		return;

}

void GameScene_Ox4::onTouchEnded(Touch *pTouch, Event *pEvent)
{
	is_touch_fire_ = false;

}


//定时器
void GameScene_Ox4::func_send_time_check(float dt)
{

}

Point GameScene_Ox4::locationFromTouch(Touch* touch)
{
	return Director::getInstance()->convertToGL(touch->getLocationInView());
}


/// 界面显示时间的回调
void GameScene_Ox4::setMeUserClock(int time)
{
	if (mFrameLayer4)
	{
		mFrameLayer4->showTimeOut(time);
	}
}

void GameScene_Ox4::net_user_state_update(int chair_id, byte user_state)
{
	auto ik = IClientKernel::get();
	PlayerNode_Ox4 * node = nullptr;

	int my_userid = ik->SwitchViewChairID(chair_id);
	node = mPlayerLayer4->getPlayerNodeById(my_userid);
	node->setIsZhuang(false);
	if (user_state == US_NULL)
	{
		node->setIsPrepare(false);
		clearCardData(chair_id);
	}
	else if (user_state == US_READY)
	{
		node->setIsPrepare(true);
		///< 清理牌
		clearCardData(chair_id);

		IClientKernel* kernel = IClientKernel::get();

		PlayerNode_Ox4 * node = nullptr;
		node = mPlayerLayer4->getPlayerNodeById(kernel->SwitchViewChairID(chair_id));
		node->setShowGoldKuang(false);
	}
}

void GameScene_Ox4::net_user_level(int chair_id)
{
	IClientKernel* kernel = IClientKernel::get();
	int my_userid = kernel->SwitchViewChairID(chair_id);
	PlayerNode_Ox4 * node = mPlayerLayer4->getPlayerNodeById(my_userid);

	node->clearUser(true);
	
}

void GameScene_Ox4::clearPlayerPrepare()
{
	auto node = mPlayerLayer4->getPlayerNodeById(0);
	node->setIsPrepare(false);
	node = mPlayerLayer4->getPlayerNodeById(1);
	node->setIsPrepare(false);
	node = mPlayerLayer4->getPlayerNodeById(2);
	node->setIsPrepare(false);
	node = mPlayerLayer4->getPlayerNodeById(3);
	node->setIsPrepare(false);
}

void GameScene_Ox4::showBankerIconAtPlayerNode(int chairId)
{
	SoundManager::shared()->playSound("GAME_START");
	PlayerNode_Ox4 * node = nullptr;
	IClientKernel* kernel = IClientKernel::get();

	int my_user = kernel->SwitchViewChairID(chairId);

	node = mPlayerLayer4->getPlayerNodeById(my_user);
	node->setIsZhuang(true);

}

void GameScene_Ox4::clearOtherUser(int chair_id)
{
	IClientKernel * kernel = IClientKernel::get();
	int index = kernel->SwitchViewChairID(chair_id);
	auto node = mPlayerLayer4->getPlayerNodeById(index);
	node->clearUser(false);
}

void GameScene_Ox4::clearCardData(int chair_id)
{
	IClientKernel * kernel = IClientKernel::get();

	int index = kernel->SwitchViewChairID(chair_id);
	for (int j = 0; j < 5; j++)
	{
		this->removeChildByTag(8520 + index * 5 + j);
	}
	removeChildByTag(600 + index);
	
}

void GameScene_Ox4::net_user_show_hard(int chair_id)
{
	IClientKernel* kernel = IClientKernel::get();
	byte bFuckYou[5];
	CCPoint pos[5];
	
	int my_card_id = kernel->SwitchViewChairID(chair_id);
	if (gClientKernelSink_Ox4.m_bUserOxCard[kernel->GetMeChairID()])
	{
		if (gClientKernelSink_Ox4.m_cbUserOxCard[kernel->GetMeChairID()] != 0)
		{
			gClientKernelSink_Ox4.m_pGameLogic->GetOxCard(bFuckYou, MAX_COUNT);
			int index = 0;
			for (int i = 0; i < MAX_COUNT; i++)
			{
				
				CSpriteCard* xxx2 = (CSpriteCard *)this->getChildByTag(8520 + my_card_id * 5 + i);

				pos[i] = xxx2->getPosition();

				xxx2->removeFromParent();

			}
			for (int i = 0; i<MAX_COUNT; i++)
			{
				CSpriteCard* card = this->m_Card.getCardWithData(bFuckYou[i]);
				card->setTag(8520 + my_card_id * 5 + i);

				if (i>2)
				{
					card->setPosition(ccp(pos[i].x, pos[i].y + 30));
					card->m_bSelect = true;
					card->setZOrder(1);
				}
				else
				{
					card->setPosition(ccp(pos[i].x, pos[i].y));
					card->m_bSelect = false;
					card->setZOrder(1);
				}
				this->addChild(card);
			}

		}
		else
		{


		}

	}

	this->showOxCardType(gClientKernelSink_Ox4.m_cbUserOxCard[kernel->GetMeChairID()], my_card_id, 1);

}

void GameScene_Ox4::closeGameNotify(cocos2d::Ref * ref)
{
	if (IClientKernel::get())
		IClientKernel::get()->Intermit(GameExitCode_Normal);
}

void GameScene_Ox4::showSettlementFrame(int chair_id, bool isShow)
{
	IClientKernel* kernel = IClientKernel::get();
	PlayerNode_Ox4 * node = nullptr;

	int my_userid = kernel->SwitchViewChairID(chair_id);
	node = mPlayerLayer4->getPlayerNodeById(my_userid);
	node->setShowGoldKuang(isShow);
}

void GameScene_Ox4::initGameBaseData()
{
	DF::shared()->init(KIND_ID, GAME_PLAYER, VERSION_CLIENT, STATIC_DATA_STRING("appname"), platformGetPlatform());
}

void GameScene_Ox4::func_Reconnect_on_loss(cocos2d::Ref * obj)
{
	IClientKernel* kernel = IClientKernel::get();
	if (kernel == nullptr)
	{
		G_NOTIFY_D("MODE_SELECTED", MTData::create(1));
		return;
	}

	isOx4ReconnectOnLoss = true;
	///< 断线重连
	this->schedule(SEL_SCHEDULE(&GameScene_Ox4::reconnect_on_loss), 1.5f);

}

void GameScene_Ox4::reconnect_on_loss(float dt)
{
	static float total_time = 0;
	total_time += dt;

	bool isHaveNet = false;

#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID || CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
	isHaveNet = SimpleTools::obtainNetWorkState();
#endif

#if (CC_TARGET_PLATFORM == CC_PLATFORM_WIN32)
	if (total_time > 10)
		isHaveNet = true;
#endif
	if (total_time > 15)
	{
		this->unschedule(SEL_SCHEDULE(&GameScene_Ox4::reconnect_on_loss));
		if (!isHaveNet)
		{
			log("mei you wang luo l!");
			///< 没有网络了..
			NewDialog::create(SSTRING("System_Tips_26"), NewDialog::AFFIRM, [=]()
			{
				if (IClientKernel::get())
					IClientKernel::get()->Intermit(GameExitCode_Normal);
			});
			isOx4ReconnectOnLoss = false;
			total_time = 0.0f;
			return;
		}
	}

	if (isHaveNet)
	{
		if (total_time <= 15.0f)
			this->unschedule(SEL_SCHEDULE(&GameScene_Ox4::reconnect_on_loss));
		NewDialog::create(SSTRING("System_Tips_28"), NewDialog::NONEBUTTON, nullptr, nullptr, [=]()
		{

			ServerScene * tServer = dynamic_cast<ServerScene *>(ModeScene::create()->getChildByName("ServerScene"));
			if (tServer)
			{
				tServer->connectServer();
			}
			isOx4ReconnectOnLoss = false;
		});
		total_time = 0.0f;
		isOx4ReconnectOnLoss = true;
	}
	else
	{
		NewDialog::create(SSTRING("System_Tips_29"), NewDialog::NONEBUTTON);
	}

}
bool GameScene_Ox4::is_Reconnect_on_loss()
{
	return isOx4ReconnectOnLoss;
}
void GameScene_Ox4::ExitGameTiming()
{
	NewDialog::create(SSTRING("System_Tips_36"), NewDialog::NONEBUTTON);
	this->scheduleOnce(SEL_SCHEDULE(&GameScene_Ox4::CloseKernelExitGame), 2.5f);
}

void GameScene_Ox4::CloseKernelExitGame(float dt)
{
	IClientKernel::destory();
}

