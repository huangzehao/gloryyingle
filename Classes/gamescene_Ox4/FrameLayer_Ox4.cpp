#include "FrameLayer_Ox4.h"
#include "ClientKernelSink_Ox4.h"
#include "Tools/tools/StringData.h"
#include "Kernel/kernel/game/IClientKernel.h"
#include "Tools/Manager/SpriteHelper.h"
#include "Tools/Manager/SoundManager.h"
#include "common/GameSetLayer.h"
#include "Tools/tools/SimpleTools.h"
#include "Tools/Dialog/NewDialog.h"
#include "Tools/tools/Convert.h"
#include "Tools/ViewHeader.h"
#include "ui/UIWidget.h"

USING_NS_CC_EXT;

FrameLayer_Ox4::FrameLayer_Ox4()
{
}


FrameLayer_Ox4::~FrameLayer_Ox4()
{
	bool isHave = this->isScheduled(SEL_SCHEDULE(&FrameLayer_Ox4::sche_time_out));
	if (isHave)
	{
		this->unschedule(SEL_SCHEDULE(&FrameLayer_Ox4::sche_time_out));
	}
}

bool FrameLayer_Ox4::init()
{
	if (!Layer::init())
	{
		return false;
	}

	//解析主界面cocosstudio json文件
	Widget * mRoot = cocostudio::GUIReader::getInstance()->widgetFromJsonFile("ox4GameScene/Ox4_Main.json");
	mPanelMain = dynamic_cast<Widget *>(mRoot->getChildByName("Panel_Main"));
	this->addChild(mRoot);

	btn_return = dynamic_cast<Button *>(mPanelMain->getChildByName("btn_return"));
	btn_return->addTouchEventListener(this, SEL_TouchEvent(&FrameLayer_Ox4::btnCloseGameCallBack));

	btn_start = dynamic_cast<Button *>(mPanelMain->getChildByName("btn_start"));
	btn_start->addTouchEventListener(this, SEL_TouchEvent(&FrameLayer_Ox4::btnStartGameCallBack));

	btn_require_banker = dynamic_cast<Button *>(mPanelMain->getChildByName("btn_require_banker"));
	btn_require_banker->addTouchEventListener(this, SEL_TouchEvent(&FrameLayer_Ox4::btnRequireBankerGameCallBack));

	btn_abort = dynamic_cast<Button *>(mPanelMain->getChildByName("btn_abort"));
	btn_abort->addTouchEventListener(this, SEL_TouchEvent(&FrameLayer_Ox4::btnAbortGameCallBack));

	btn_tanpai = dynamic_cast<Button *>(mPanelMain->getChildByName("btn_tanpai"));
	btn_tanpai->addTouchEventListener(this, SEL_TouchEvent(&FrameLayer_Ox4::btnTanpaiGameCallBack));

	lab_text_content = dynamic_cast<ImageView *>(mPanelMain->getChildByName("lab_text_content"));
	
	img_clock = dynamic_cast<ImageView *>(mPanelMain->getChildByName("img_clock"));
	img_timesub = dynamic_cast<TextAtlas *>(img_clock->getChildByName("img_timesub"));

	btn_start->setVisible(false);
	btn_require_banker->setVisible(false);
	btn_abort->setVisible(false);
	btn_tanpai->setVisible(false);
	lab_text_content->setVisible(false);

	btn_score_bg = Sprite::create();
	btn_score_bg_1 = ui::Button::create("ox4GameScene/Resource/gui-tniuniu-button-bet-3.png");
	btn_score_bg_1->setTag(1);
	btn_score_bg_1->addTouchEventListener(this, ui::SEL_TouchEvent(&FrameLayer_Ox4::btnAddScoreCallBack));
	btn_score_bg_1->setPosition(Vec2(1025, 280));
	btn_score_bg_2 = ui::Button::create("ox4GameScene/Resource/gui-tniuniu-button-bet-2.png");
	btn_score_bg_2->setTag(2);
	btn_score_bg_2->setPosition(Vec2(815, 280));
	btn_score_bg_2->addTouchEventListener(this, ui::SEL_TouchEvent(&FrameLayer_Ox4::btnAddScoreCallBack));
	btn_score_bg_3 = ui::Button::create("ox4GameScene/Resource/gui-tniuniu-button-bet-1.png");
	btn_score_bg_3->setTag(4);
	btn_score_bg_3->setPosition(Vec2(605, 280));
	btn_score_bg_3->addTouchEventListener(this, ui::SEL_TouchEvent(&FrameLayer_Ox4::btnAddScoreCallBack));
	btn_score_bg_4 = ui::Button::create("ox4GameScene/Resource/gui-tniuniu-button-bet-0.png");
	btn_score_bg_4->setTag(8);
	btn_score_bg_4->setPosition(Vec2(395, 280));
	btn_score_bg_4->addTouchEventListener(this, ui::SEL_TouchEvent(&FrameLayer_Ox4::btnAddScoreCallBack));
	btn_score_bg->addChild(btn_score_bg_1);
	btn_score_bg->addChild(btn_score_bg_2);
	btn_score_bg->addChild(btn_score_bg_3);
	btn_score_bg->addChild(btn_score_bg_4);
	this->addChild(btn_score_bg);
	btn_score_bg->setVisible(false);

	return true;
}

void FrameLayer_Ox4::btnCloseGameCallBack(cocos2d::Ref * ref, ui::TouchEventType eType)
{
	if (eType == ui::TouchEventType::TOUCH_EVENT_BEGAN)
	{
		SoundManager::shared()->playSound("ox4_GAME_READY");
	}
	if (eType == ui::TouchEventType::TOUCH_EVENT_ENDED)
	{
		ui::Button * btn_now = dynamic_cast<ui::Button *>(ref);
		btn_now->setTag(DLG_MB_OK);

		auto kernel = IClientKernel::get();
		int clock_id = kernel->GetClockID();
		if (clock_id != 50)
		{
			NewDialog::create(SSTRING("back_to_room_content"), NewDialog::AFFIRMANDCANCEL, [=]()
			{
				(mTarget->*mCallback)(btn_now);
			});
		}
		else
		{
			if (mCallback)
			{
				(mTarget->*mCallback)(btn_now);
			}
		}
	}
}

void FrameLayer_Ox4::hideBetButton()
{
	btn_score_bg->setVisible(false);
}

void FrameLayer_Ox4::setCloseDialogInfo(cocos2d::Ref* target, cocos2d::SEL_CallFuncN callfun, const std::string& title, const std::string& content)
{
	mTarget = target;
	mCallback = callfun;
	mTitle = title;
	mContent = content;
}

void FrameLayer_Ox4::onBtnClick(cocos2d::Ref* obj, cocos2d::extension::Control::EventType e)
{
	Control* ctr = (Control*)obj;

	popup(mTitle.c_str(), mContent.c_str(), DLG_MB_OK | DLG_MB_CANCEL, 0, mTarget, mCallback);
}

void FrameLayer_Ox4::func_affirm_exit_call_back(cocos2d::Ref *pSender)
{
	ui::Button * btn_now = dynamic_cast<ui::Button *>(pSender);
	btn_now->setTag(DLG_MB_OK);

		if (mCallback)
		{
			(mTarget->*mCallback)(btn_now);
		}

}

//倒计时的回调
void FrameLayer_Ox4::sche_time_out(float dt)
{
	total_time = total_time - (int)dt;
	if (total_time < 3)
	{
		IClientKernel * kernel = IClientKernel::get();
		int clock_id = kernel->GetClockID();
		SoundManager::shared()->playSound("GAME_WARN");
	}
	if (total_time > 0)
	{
		img_timesub->setString(StringUtils::format("%d", total_time));
		return;
	}
	else
	{
		this->unschedule(SEL_SCHEDULE(&FrameLayer_Ox4::sche_time_out));
		IClientKernel* kernel = IClientKernel::get();

		func_affirm_exit_call_back(btn_return);
	}
}

/// 设置音效和音乐
void FrameLayer_Ox4::func_set_call_back(cocos2d::Ref *pSender)
{
	Node* node = dynamic_cast<Node*>(pSender);
	GameSetLayer * layer = GameSetLayer::create();

	this->getParent()->addChild(layer, 0xFFFF);
}

//开始按钮点击的回调
void FrameLayer_Ox4::btnStartGameCallBack(cocos2d::Ref * ref, ui::TouchEventType eType)
{
	if (eType == ui::TouchEventType::TOUCH_EVENT_BEGAN)
	{
		SoundManager::shared()->playSound("ox4_GAME_READY");
	}
	if (eType == ui::TouchEventType::TOUCH_EVENT_ENDED)
	{
		IClientKernel* kernel = IClientKernel::get();

		kernel->SendUserReady(NULL, 0);

		///删除开始计时器
		gClientKernelSink_Ox4.OnStartremove(50);

		showMenuItem(MenuItemTag::ITEM_TAG_START, false);

		kernel->SetGameClock(kernel->GetMeChairID(), 50, 30);

		GameScene_Ox4 * pGame_scene = dynamic_cast<GameScene_Ox4 *>(this->getParent());

		pGame_scene->clearCardData(kernel->GetMeChairID());
		pGame_scene->showSettlementFrame(kernel->GetMeChairID(), false);
	}
}

/// 请求坐庄的回调
void FrameLayer_Ox4::btnRequireBankerGameCallBack(cocos2d::Ref * ref, ui::TouchEventType eType)
{
	if (eType == ui::TouchEventType::TOUCH_EVENT_BEGAN)
	{
		SoundManager::shared()->playSound("follow");
	}
	if (eType == ui::TouchEventType::TOUCH_EVENT_ENDED)
	{
		gClientKernelSink_Ox4.ButtonWithCallBanker();
		showMenuItem(ITEM_TAG_REQUIRE_BANKSER, false);
		showMenuItem(ITEM_TAG_ABORT, false);
	}
}

/// 摊牌按钮的回调
void FrameLayer_Ox4::btnTanpaiGameCallBack(cocos2d::Ref * ref, ui::TouchEventType eType)
{
	if (eType == ui::TouchEventType::TOUCH_EVENT_BEGAN)
	{
		SoundManager::shared()->playSound("follow");
	}
	if (eType == ui::TouchEventType::TOUCH_EVENT_ENDED)
	{
		showMenuItem(ITEM_TAG_SHOW_HARD, false);
		gClientKernelSink_Ox4.OnMeGive(0);
	}
}
/// 放弃按钮的回调
void FrameLayer_Ox4::btnAbortGameCallBack(cocos2d::Ref * ref, ui::TouchEventType eType)
{
	if (eType == ui::TouchEventType::TOUCH_EVENT_BEGAN)
	{
		SoundManager::shared()->playSound("follow");
	}
	if (eType == ui::TouchEventType::TOUCH_EVENT_ENDED)
	{
		gClientKernelSink_Ox4.ButtonWithNoCall();
		showMenuItem(ITEM_TAG_REQUIRE_BANKSER, false);
		showMenuItem(ITEM_TAG_ABORT, false);
	}
}

///< 显示按钮
void FrameLayer_Ox4::showMenuItem(MenuItemTag item_tag, bool isShow)
{
	switch (item_tag)
	{
	case FrameLayer_Ox4::ITEM_TAG_START:
		btn_start->setVisible(isShow);
		break;
	case FrameLayer_Ox4::ITEM_TAG_REQUIRE_BANKSER:
		btn_require_banker->setVisible(isShow);
		break;
	case FrameLayer_Ox4::ITEM_TAG_SHOW_HARD:
		btn_tanpai->setVisible(isShow);
		break;
	case FrameLayer_Ox4::ITEM_TAG_ABORT:
		btn_abort->setVisible(isShow);
		break;
	default:
		break;
	}
}



void FrameLayer_Ox4::showTimeOut(int time)
{
	if (time <= 0) return;
	total_time = time;

	bool isHave = this->isScheduled(SEL_SCHEDULE(&FrameLayer_Ox4::sche_time_out));
	if (total_time > 0 && !isHave)
	{
		this->schedule(SEL_SCHEDULE(&FrameLayer_Ox4::sche_time_out), 1);
	}
}

void FrameLayer_Ox4::showWaitPanelContent(bool isShow)
{
	lab_text_content->setVisible(isShow);
}

void FrameLayer_Ox4::setWaitPanelContent(int type)
{
	lab_text_content->loadTexture(StringUtils::format("oxGameScene/Resource/Game_Prep_Content_%d.png", type));
}

///< 设置分数
void FrameLayer_Ox4::setAddScoreMaxValue(long long score)
{
	if (score < 0) return;
	max_score = score;

	btn_score_bg->setVisible(true);

	std::string str = StringUtils::format("%lld", score / 1);
	btn_score_bg_1->setTitleText(str);
	btn_score_bg_1->setTitleFontSize(30);
	str = StringUtils::format("%lld", score / 2);
	btn_score_bg_2->setTitleText(str);
	btn_score_bg_2->setTitleFontSize(30);
	str = StringUtils::format("%lld", score / 4);
	btn_score_bg_3->setTitleText(str);
	btn_score_bg_3->setTitleFontSize(30);
	str = StringUtils::format("%lld", score / 8);
	btn_score_bg_4->setTitleText(str);
	btn_score_bg_4->setTitleFontSize(30);
}

///< 加分数的回调
void FrameLayer_Ox4::btnAddScoreCallBack(cocos2d::Ref * ref, ui::TouchEventType eType)
{
	if (eType == ui::TouchEventType::TOUCH_EVENT_BEGAN)
	{
		SoundManager::shared()->playSound("ADD_SCORE");
	}
	if (eType == ui::TouchEventType::TOUCH_EVENT_ENDED)
	{
		ui::Button * btn_now = dynamic_cast<ui::Button *>(ref);
		int tag = btn_now->getTag();
		///< 发送下注消息
		gClientKernelSink_Ox4.OnMeAddGold(tag);
		///< 把加分面板隐藏
		btn_score_bg->setVisible(false);
	}
}