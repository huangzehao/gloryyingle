#include "PlayerNode_Ox4.h"
#include "Tools/tools/YRCommon.h"

#define  P_WIDTH      1420
#define  P_WIDTH_2    P_WIDTH/2

#define  P_HEIGHT     800
#define  P_HEIGHT_2   P_HEIGHT/2

const int PLAYER_POSTION[][4] = {
	{ 659, 604 },
	{ 1295, 367 },
	{ 659,  79 },
	{ 120, 367 },
};

PlayerNode_Ox4::PlayerNode_Ox4()
{
}


PlayerNode_Ox4::~PlayerNode_Ox4()
{
}

bool PlayerNode_Ox4::init(int chair_id, Widget * mRoot)
{
	if (!Node::init())
	{
		return false;
	}

	//初始化椅子号
	mChairId = chair_id;
	mPanelMain = dynamic_cast<Widget *>(mRoot->getChildByName("Panel_Main"));

	//初始化玩家节点控制
	img_PlayerNode = dynamic_cast<ImageView *>(mPanelMain->getChildByName(StringUtils::format("PlayerNode_%d", mChairId))) ;
	img_PlayerNode->setVisible(false);

	img_head = dynamic_cast<ImageView *>(img_PlayerNode->getChildByName("img_headBg")->getChildByName("img_head"));
	lab_name = dynamic_cast<Text *>(img_PlayerNode->getChildByName("lab_name"));
	al_score = dynamic_cast<TextAtlas *>(img_PlayerNode->getChildByName("al_score"));
	img_zhuang = dynamic_cast<ImageView *>(img_PlayerNode->getChildByName("img_zhuang"));
	img_prepare = dynamic_cast<ImageView *>(img_PlayerNode->getChildByName("img_prepare"));
	img_jinbi_kuang = dynamic_cast<ImageView *>(img_PlayerNode->getChildByName("img_jinbi_kuang"));
	lable_score = dynamic_cast<Text *>(img_jinbi_kuang->getChildByName(("lable_score")));

	if (mChairId == 2)
	{
		al_mScore = dynamic_cast<TextAtlas *>(img_PlayerNode->getChildByName("img_score_kuang")->getChildByName("al_mScore"));
	}
	return true;
	
}

PlayerNode_Ox4 * PlayerNode_Ox4::create(int chard_id, Widget *mRoot)
{
	PlayerNode_Ox4 * node = new PlayerNode_Ox4;
	if (node && node->init(chard_id, mRoot))
	{
		node->autorelease();
		return node;
	}
	delete node;
	return nullptr;
}

void PlayerNode_Ox4::setFaceID(int wFaceID)
{
	img_head->loadTexture(YRComGetHeadImageById(wFaceID));
}

void PlayerNode_Ox4::setHeadInfo(bool isVisible)
{
	img_PlayerNode->setVisible(isVisible);
}

void PlayerNode_Ox4::setIsPrepare(bool isPre)
{
	img_prepare->setVisible(isPre);
}

void PlayerNode_Ox4::setNickName(const char * name)
{
	lab_name->setString(name);
}

void PlayerNode_Ox4::setMoney(long long money)
{
	total_money = money;
	al_score->setString(StringUtils::format("%lld", total_money));
	if (mChairId == 2)
	{
		al_mScore->setString(StringUtils::format("%lld", total_money));
	}
}

void PlayerNode_Ox4::setBuyMoney(long long money)
{
	std::string str_mon = StringUtils::format("%lld", money);
	lable_score->setString(str_mon);

	if (money < 0)
	{
		lable_score->setTextColor(Color4B::RED);
	}
}

void PlayerNode_Ox4::setIsZhuang(bool isZ)
{
	img_zhuang->setVisible(isZ);
}

void PlayerNode_Ox4::setShowGoldKuang(bool isShow)
{
	img_jinbi_kuang->setVisible(isShow);
	lable_score->setVisible(isShow);
	lable_score->setTextColor(Color4B::YELLOW);
}

void PlayerNode_Ox4::clearUser(bool isLevel)
{
	if (isLevel)
	{
		img_PlayerNode->setVisible(false);
	}
}

void PlayerNode_Ox4::addMoney(long long money)
{
	total_money += money;
	std::string str_mon = StringUtils::format("%lld", total_money);
	al_score->setString(str_mon);
}
