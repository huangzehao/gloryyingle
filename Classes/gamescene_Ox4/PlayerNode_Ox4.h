#ifndef PLAYERNODE_OX4_H
#define PLAYERNODE_OX4_H

#include "cocos2d.h"
#include "cocostudio/CocoStudio.h"
#include "ui/CocosGUI.h"

USING_NS_CC;
using namespace cocos2d::ui;
using namespace cocostudio;

class PlayerNode_Ox4 : public Node
{
public:
	PlayerNode_Ox4();
	~PlayerNode_Ox4();

	bool init(int chair_id,Widget * mRoot);
	static PlayerNode_Ox4 * create(int chard_id, Widget * mRoot);

	//设置头像
	void setFaceID(int wFaceID);

	//设置用户信息
	void setHeadInfo(bool isVisible);
	void setIsZhuang(bool isZ);
	void setIsPrepare(bool isPre);
	void setNickName(const char * name);
	void setMoney(long long money);
	void setBuyMoney(long long money);
	void setShowGoldKuang(bool isShow);
	void addMoney(long long money);

	void clearUser(bool isLevel);

protected:
	///<玩家节点主面板
	Widget * mPanelMain;
	///<玩家节点
	ImageView * img_PlayerNode;
	///<玩家头像
	ImageView * img_head;
	///<玩家名字
	Text * lab_name;
	///<玩家分数
	TextAtlas * al_score;
	///<玩家当庄标识
	ImageView * img_zhuang;
	///<玩家已准备标识
	ImageView * img_prepare;
	///<押注金币框
	ImageView *img_jinbi_kuang;
	///<押注分数
	Text * lable_score;
	///<主角分数
	TextAtlas * al_mScore;

	///<玩家椅子号
	int mChairId;
	long long total_money;
};

#endif

