#include "PlayerLayer_Ox4.h"
#include "ClientKernelSink_Ox4.h"
#include "Tools/tools/MTNotification.h"
#include "Kernel/kernel/game/IClientKernel.h"
#include "CMD_Ox4.h"





using namespace Ox4;
PlayerLayer_Ox4::PlayerLayer_Ox4()
{
	G_NOTIFY_REG("EVENT_USER_SCORE", PlayerLayer_Ox4::updateUserScore);
}


PlayerLayer_Ox4::~PlayerLayer_Ox4()
{
	G_NOTIFY_UNREG("EVENT_USER_SCORE");
}

bool PlayerLayer_Ox4::init()
{
	if (!Layer::init())
	{
		return false;
	}

	Widget * mRoot = cocostudio::GUIReader::getInstance()->widgetFromJsonFile("ox4GameScene/PlayerNode.json");
	this->addChild(mRoot);
	
	for (int i = 0; i < GAME_PLAYER; i++)
	{
		PlayerNode_Ox4 * node = PlayerNode_Ox4::create(i, mRoot);
		this->addChild(node);
		mPlayerList.push_back(node);
	}

	return true;
}

PlayerNode_Ox4 * PlayerLayer_Ox4::getPlayerNodeById(int chair_id)
{
	if (chair_id < mPlayerList.size())
	{
		return mPlayerList[chair_id];
	}

	return nullptr;
	
}

//更新 用户 积分

void PlayerLayer_Ox4::updateUserScore(cocos2d::Ref * ref)
{
	EventCustom *event_ = (EventCustom*)ref;
	if (event_ == 0){
		return;
	}

	MTData* data = (MTData*)event_->getUserData();
	if (data == 0)
		return;

	IClientUserItem * item = (IClientUserItem *)data->mPData;

	IClientKernel * kernel = IClientKernel::get();
	PlayerNode_Ox4 * node = nullptr;
	if (item)
	{
		int chair_id = item->GetChairID();
		node = getPlayerNodeById(kernel->SwitchViewChairID(chair_id));

		if (node)
		{
			node->setMoney(item->GetUserScore());
		}
	}
}
