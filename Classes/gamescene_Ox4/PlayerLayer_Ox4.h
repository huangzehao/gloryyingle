#ifndef PLAYERLAYER_OX4_
#define PLAYERLAYER_OX4_

#include "cocos2d.h"
#include "PlayerNode_Ox4.h"

USING_NS_CC;

class PlayerLayer_Ox4 : public Layer
{
public:
	PlayerLayer_Ox4();
	~PlayerLayer_Ox4();

	CREATE_FUNC(PlayerLayer_Ox4);

	bool init();

	PlayerNode_Ox4 * getPlayerNodeById(int chair_id);

	void updateUserScore(cocos2d::Ref * ref);
protected:
	std::vector<PlayerNode_Ox4 *> mPlayerList;


};

#endif

