#ifndef FRAMELAYER_OX4_H
#define FRAMELAYER_OX4_H

#include "cocos2d.h"
#include "GameScene_Ox4.h"
#include "cocos-ext.h"
#include "ui/UITextAtlas.h"
#include "cocostudio/CocoStudio.h"
#include "ui/CocosGUI.h"
#include "ui/UIButton.h"

using namespace cocos2d::ui;
using namespace cocostudio;
USING_NS_CC;

class FrameLayer_Ox4 : public cocos2d::Layer
{
public:
	FrameLayer_Ox4();
	~FrameLayer_Ox4();

	CREATE_FUNC(FrameLayer_Ox4);
	enum MenuItemTag
	{
		ITEM_TAG_START, ///< 开始按钮
		ITEM_TAG_REQUIRE_BANKSER, ///< 请求庄家
		ITEM_TAG_SHOW_HARD, ///< 摊牌
		ITEM_TAG_ABORT, ///< 放弃
	};

	virtual bool init();

	void setCloseDialogInfo(cocos2d::Ref* target, cocos2d::SEL_CallFuncN callfun, const std::string& title, const std::string& content);


protected:
	cocos2d::Ref*		mTarget;
	cocos2d::SEL_CallFuncN	mCallback;
	std::string				mTitle;
	std::string				mContent;

	///<主面板
	Widget * mPanelMain;
	///<退出按钮
	Button * btn_return;
	///<开始按钮
	Button * btn_start;
	///<叫庄按钮
	Button * btn_require_banker;
	///<不叫按钮
	Button * btn_abort;
	///<摊牌按钮
	Button * btn_tanpai;
	///< 准备时候的内容
	ImageView * lab_text_content;
	///< 闹钟图片
	ImageView * img_clock;
	///<闹钟倒计时
	TextAtlas * img_timesub;

	Sprite * btn_score_bg;
	ui::Button * btn_score_bg_1;
	ui::Button * btn_score_bg_2;
	ui::Button * btn_score_bg_3;
	ui::Button * btn_score_bg_4;

private:
	void onBtnClick(cocos2d::Ref* obj, cocos2d::extension::Control::EventType e);
	/// 退出回调
	void func_affirm_exit_call_back(cocos2d::Ref *pSender);

	/// 关闭游戏按钮回调
	void btnCloseGameCallBack(cocos2d::Ref * ref, ui::TouchEventType eType);
	/// 开始按钮的回调
	void btnStartGameCallBack(cocos2d::Ref * ref, ui::TouchEventType eType);
	/// 请求坐庄的回调
	void btnRequireBankerGameCallBack(cocos2d::Ref * ref, ui::TouchEventType eType);
	/// 摊牌按钮的回调
	void btnTanpaiGameCallBack(cocos2d::Ref * ref, ui::TouchEventType eType);
	/// 放弃按钮的回调
	void btnAbortGameCallBack(cocos2d::Ref * ref, ui::TouchEventType eType);
	/// 设置音效和音乐
	void func_set_call_back(cocos2d::Ref *pSender);

private:
	int total_time; ///<总时间

	long long max_score; ///< 最大投注额
public:
	//创建主面板
	void createMainPanel();
	///< 显示按钮
	void showMenuItem(MenuItemTag item_tag, bool isShow = true);
	///< 显示时间结束
	void showTimeOut(int time);
	///< 显示提示面板
	void showWaitPanelContent(bool isShow);
	///< 提示性的文字
	void setWaitPanelContent(int type);
	///< 设置分数
	void setAddScoreMaxValue(long long score);
	///< 隐藏押注按钮
	void hideBetButton();
private:
	///< 倒计时的回调
	void sche_time_out(float dt);
	///< 加分数的回调
	void btnAddScoreCallBack(cocos2d::Ref * ref, ui::TouchEventType eType);

};




























#endif