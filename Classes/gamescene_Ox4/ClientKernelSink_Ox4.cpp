#include "Tools/tools/MTNotification.h"
#include "Tools/Manager/SoundManager.h"
#include "Tools/Dialog/NewDialog.h"
#include "Kernel/kernel/game/IClientKernel.h"
#include "Kernel/kernel/server/IServerItemSink.h"
#include "Tools/tools/StringData.h"
#include "Tools/Dialog/Timer.h"
#include "Kernel/kernel/server/IServerItem.h"
#include "ClientKernelSink_Ox4.h"


#define  P_WIDTH      kRevolutionWidth
#define  P_WIDTH_2    P_WIDTH/2

#define  P_HEIGHT    kRevolutionHeight
#define  P_HEIGHT_2    P_HEIGHT/2

//时间标识
#define IDI_GAME_TIME				100									//游戏定时器
#define IDI_START_GAME				50									//开始定时器

//游戏时间
#define TIME_START_GAME				30									//开始定时器

#define	SHAKE_FPS					30
#define SHAKE_FRAMES				30
#define KICK_OUT_TIME				60000
#define FIRE_INTERVAL				200
#define HELP_SHOW_TIME				5000

#define MAX_PATHS			260
#define MODE_MEDAL							1					//元宝模式(既平台的奖牌)
USING_NS_CC;
using namespace Ox4;

ClientKernelSink_Ox4* ClientKernelSink_Ox4::s_Instance = NULL;

ClientKernelSink_Ox4 gClientKernelSink_Ox4;

//构造函数
ClientKernelSink_Ox4::ClientKernelSink_Ox4()
{
	szGlobalMessage = "";
	char path[MAX_PATHS]={0};	

	//下注倍数
	m_nMultiples[0] = 1;
	m_nMultiples[1] = 2;
	m_nMultiples[2] = 4;
	m_nMultiples[3] = 0;

	m_GameIsStart=false;
	m_IsZhongTu = false;

	m_nSendCount=0;
	m_SendCard=false;                        //发牌标志
	m_wSendIndex=0;							//发牌索引
	m_nSendChair=0;							//发送座位	

	m_CardPoint[0] = ccp(665,679);
	m_CardPoint[1] = ccp(1000,414);
	m_CardPoint[2] = ccp(628, 160);
	m_CardPoint[3] = ccp(260, 414);
	for (int i=0; i<GAME_PLAYER_Ox4; i++)
	{
		m_cbUserOxCard[i]	= 0;
	}

	return;
}

//析构函数
ClientKernelSink_Ox4::~ClientKernelSink_Ox4()
{
	m_pGameScene = nullptr;
}

ClientKernelSink_Ox4* ClientKernelSink_Ox4::GetInstance()
{
	if(s_Instance == NULL)
		s_Instance = new ClientKernelSink_Ox4();

	return s_Instance;
}

void ClientKernelSink_Ox4::Release()
{

}

//控制接口
//启动游戏场景
bool ClientKernelSink_Ox4::SetupGameClient()
{
	PLAZZ_PRINTF(("flow->ClientKernelSink::SetupGameClient\n"));
	mIsNetworkPrepared = false;

	Director * director = Director::getInstance();
	Scene * pScene = director->getRunningScene();
	if (pScene->getTag() == KIND_ID && pScene->getName() == "GameScene")
	{
		///< 表示是自己!!
		m_pGameScene = dynamic_cast<GameScene_Ox4 *>(pScene);
	}
	else
	{
		if (!m_pGameScene)
			m_pGameScene = GameScene_Ox4::create();
	}

	const tagUserAttribute &userattr_ = IServerItem::get()->GetUserAttribute();

	PLAZZ_PRINTF(("flow->ClientKernelSink::SetupGameClient1\n"));

	if (pScene != m_pGameScene)
	{
		director->pushScene(CCTransitionFade::create(0.5f, m_pGameScene));
	}
	else
	{
		if (IClientKernel::get())
		{
			IClientKernel::get()->SendGameOption();
		}
	}

	return false;
}

//重置游戏
void ClientKernelSink_Ox4::ResetGameClient()
{
	PLAZZ_PRINTF(("flow->ClientKernelSink_Ox::ResetGameClient\n"));
	IClientKernel * kernel = IClientKernel::get();

	Director::getInstance()->getScheduler()->unschedule(schedule_selector(ClientKernelSink_Ox4::OnGameTimer), this);

	kernel->KillGameClock(IDI_START_GAME);

	mIsNetworkPrepared = false;

}

//关闭游戏
void ClientKernelSink_Ox4::CloseGameClient(int exit_tag /*= 0*/)
{
	mIsNetworkPrepared = false;
	m_pGameScene = 0;
	//删除定时器
	IClientKernel* kernel = IClientKernel::get();
	kernel->KillGameClock(IDI_START_GAME);

 	Director::getInstance()->getScheduler()->unschedule(
 		schedule_selector(ClientKernelSink_Ox4::OnGameTimer), this);

	if (!GameScene_Ox4::is_Reconnect_on_loss())
		G_NOTIFY_D("MODE_SELECTED", MTData::create(1));
	
}


//////////////////////////////////////////////////////////////////////////
//框架事件

//系统滚动消息
bool ClientKernelSink_Ox4::OnGFTableMessage(const char* szMessage)
{
	return true;
}


//全局消息
bool ClientKernelSink_Ox4::OnGFGlobalMessage(const char* szMessage)
{
	szGlobalMessage = szMessage;
	m_pGameScene->add_msg(szMessage);
	return true;
}


//获取系统消息
std::string ClientKernelSink_Ox4::GetGlobalMessage()
{
	return szGlobalMessage;
}


//等待提示
bool ClientKernelSink_Ox4::OnGFWaitTips(bool bWait)
{
	return true;
}

//比赛信息
bool ClientKernelSink_Ox4::OnGFMatchInfo(tagMatchInfo* pMatchInfo)
{
	return true;
}

//比赛等待提示
bool ClientKernelSink_Ox4::OnGFMatchWaitTips(tagMatchWaitTip* pMatchWaitTip)
{
	return true;
}

//比赛结果
bool ClientKernelSink_Ox4::OnGFMatchResult(tagMatchResult* pMatchResult)
{
	return true;
}
//////////////////////////////////////////////////////////////////////////
//游戏事件

//旁观消息
bool ClientKernelSink_Ox4::OnEventLookonMode(void* data, int dataSize)
{
	return true;
}

//场景消息
bool ClientKernelSink_Ox4::OnEventSceneMessage(byte cbGameStatus, bool bLookonUser, void* data, int dataSize)
{
	IClientKernel* kernel = IClientKernel::get();
	switch (cbGameStatus)
	{
		///< 等待开始
	case GS_TK_FREE:
		{
				kernel->SetGameClock(kernel->GetMeChairID(),50,30);
				m_pGameScene->freeState();
				return true;
		}
		///< 叫庄状态
	case GS_TK_CALL:
		{
			for(word i=0;i<GAME_PLAYER;i++){
				IClientUserItem* m_UserItem = kernel->GetTableUserItem(i);
				if(m_UserItem != NULL){
					OnEventUserEnter(m_UserItem,bLookonUser);
				}
			}

			m_bGameReady = true;
			onSubRecordConnectBanker(data, dataSize);

			return true;
		}
		///< 下注状态
	case GS_TK_SCORE:
		onSubRecordConnectAddGold(data, dataSize);
		return true;
		///< 游戏状态
	case GS_TK_PLAYING:
		onSubRecordConnectPlaying(data, dataSize);
		return true;
	}

	return false;
}

//游戏消息
bool ClientKernelSink_Ox4::OnEventGameMessage(int sub, void* data, int wSize)
{
 	switch(sub)
 	{
		//用户叫庄
	case SUB_S_CALL_BANKER:
		{
			return OnUserCallBanker(data,wSize);
		}
		//游戏开始
	case SUB_S_GAME_START:
		{
			return OnSubGameStart(data,wSize);
		}
		//加注结果
	case SUB_S_ADD_SCORE:
		{
			return OnSubAddGold(data,wSize);
		}
		//用户强退
	case SUB_S_PLAYER_EXIT:
		{
			return OnSubGiveUp(data,wSize);
		}
		//发牌消息
	case SUB_S_SEND_CARD:
		{
			return OnSubSendCard(data,wSize);
		}
		//游戏结束
	case SUB_S_GAME_END:
		{
			return OnSubGameEnd(data,wSize);
		}
		//用户摊牌
	case SUB_S_OPEN_CARD:
		{
			return OnSubOpenCard(data,wSize);
		}
	default:
		break;
	}

	return true;
}

void ClientKernelSink_Ox4::Showback(Node* pNode)
{
	CSpriteCard* pCard = dynamic_cast<CSpriteCard*>(pNode);
	pCard->setPosition(ccp(pCard->getPosition().x,pCard->getPosition().y-30));
}
//叫庄
bool ClientKernelSink_Ox4::OnUserCallBanker(const void * pBuffer, word wDataSize)
{
	//效验数据
	if (wDataSize != sizeof(CMD_S_CallBanker)) return false;
	CMD_S_CallBanker * callBanker = (CMD_S_CallBanker *)pBuffer;

	m_wCurrentUser=callBanker->wCallBanker;
	m_Istan=false;


	IClientKernel* kernel = IClientKernel::get();

	///< 隐藏准备
	m_pGameScene->clearPlayerPrepare();
	///< 金币框隐藏
	m_pGameScene->showSettlementFrame(0, false);
	m_pGameScene->showSettlementFrame(1, false);
	m_pGameScene->showSettlementFrame(2, false);
	m_pGameScene->showSettlementFrame(3, false);
	m_pGameScene->clearCardData(0);
	m_pGameScene->clearCardData(1);
	m_pGameScene->clearCardData(2);
	m_pGameScene->clearCardData(3);
	//首次叫庄
	if (callBanker->bFirstTimes)
	{
		m_pGameScene->showWaitCallBankerWithIndex();
	}

	//当前叫庄
	if(m_wCurrentUser==kernel->GetMeChairID())
	{
		m_pGameScene->showCallBankerButton();
	}

	//显示空闲倒计时 
	OnStartremove(50);
	OnStartremove(51);

	kernel->SetGameClock(kernel->GetMeChairID(),51,30);
	return true;
}

//叫庄回调(发送)
void ClientKernelSink_Ox4::ButtonWithCallBanker()
{
	//发送叫庄
	CMD_C_CallBanker banker;
	memset(&banker, 0, sizeof(CMD_C_CallBanker));
	banker.bBanker=1;

	IClientKernel* kernel = IClientKernel::get();
	kernel->SendSocketData(SUB_C_CALL_BANKER, &banker, sizeof(CMD_C_CallBanker));
	//叫庄标识
	m_bCallBanker=true;
	OnStartremove(51);

}

//无人叫庄(发送)
void ClientKernelSink_Ox4::ButtonWithNoCall()
{
	//发送叫庄
	CMD_C_CallBanker banker;
	memset(&banker, 0, sizeof(CMD_C_CallBanker));
	banker.bBanker=0;

	IClientKernel* kernel = IClientKernel::get();

	kernel->SendSocketData(SUB_C_CALL_BANKER, &banker, sizeof(CMD_C_CallBanker));
	//叫庄标识
	m_bCallBanker=false;
	OnStartremove(51);
}
//游戏开始
bool ClientKernelSink_Ox4::OnSubGameStart(const void * pBuffer, word wDataSize)
{
	//效验数据
	if (wDataSize!=sizeof(CMD_S_GameStart)) return false;
	CMD_S_GameStart * pGameStart=(CMD_S_GameStart *)pBuffer;

	IClientKernel* kernel = IClientKernel::get();

	//游戏变量
	memset(m_cbHandCardData,0,sizeof(byte)*MAX_COUNT);
	m_wCurrentUser = pGameStart->wBankerUser;
	m_lTurnMaxGold=pGameStart->lTurnMaxScore;

	m_pGameScene->clearCardData(0);
	m_pGameScene->clearCardData(1);
	m_pGameScene->clearCardData(2);
	m_pGameScene->clearCardData(3);

	CSpriteCard::setAutoScaleSize(1.0f);
	m_pGameScene->m_Card.setCardArrayWithRow("ox4GameScene/Resource/game_card-hd.png", CSpriteCard::g_cbCardData, 55);
	//庄家标志
	m_pGameScene->showBankerIconAtPlayerNode(m_wCurrentUser);

	for (int i=0; i<GAME_PLAYER_Ox4; i++)
	{
		m_cbUserOxCard[i]	= 0;
	}

	m_cbSendCardCount=0;
	m_cbSendCard=0;
	memset(m_lTableScore,0,sizeof(m_lTableScore));
	m_bCanShowHand=false;
	m_bShowHand=false;
	m_GameIsStart=true;

	//闲家加注
	if (m_wCurrentUser != kernel->GetMeChairID() && !m_IsZhongTu)
	{
		m_pGameScene->ShowAddButton(m_lTurnMaxGold);
	}
	m_IsZhongTu = false;

	OnStartremove(51);

	kernel->SetGameClock(kernel->GetMeChairID(),52,30);

	//闲家请下注
	m_pGameScene->showTipsWithType(1);

	return true;
}

bool ClientKernelSink_Ox4::onSubRecordConnectBanker(const void * pBuffer, int wDataSize)
{
	//效验数据
	if (wDataSize != sizeof(CMD_S_StatusCall)) return false;
	CMD_S_StatusCall * callBanker = (CMD_S_StatusCall *)pBuffer;

	m_wCurrentUser = callBanker->wCallBanker;
	m_Istan = false;
	m_IsZhongTu = true;

	IClientKernel* kernel = IClientKernel::get();

	///< 隐藏准备
	m_pGameScene->clearPlayerPrepare();

	//当前叫庄
	
	m_pGameScene->callBankerState(m_wCurrentUser);

	//显示空闲倒计时 
	OnStartremove(50);
	OnStartremove(51);

	kernel->SetGameClock(kernel->GetMeChairID(), 51, 30);

	return true;
}

bool ClientKernelSink_Ox4::onSubRecordConnectAddGold(const void * pBuffer, int wDataSize)
{
	//效验数据
	if (wDataSize != sizeof(CMD_S_StatusScore)) return false;
	CMD_S_StatusScore * pGameStart = (CMD_S_StatusScore *)pBuffer;

	IClientKernel* kernel = IClientKernel::get();

	//游戏变量
	memset(m_cbHandCardData, 0, sizeof(byte)*MAX_COUNT);
	m_wCurrentUser = pGameStart->wBankerUser;
	m_lTurnMaxGold = pGameStart->lTurnMaxScore;
	m_pGameScene->clearCardData(0);
	m_pGameScene->clearCardData(1);
	m_pGameScene->clearCardData(2);
	m_pGameScene->clearCardData(3);
	///< 金币框隐藏
	m_pGameScene->showSettlementFrame(0, false);
	m_pGameScene->showSettlementFrame(1, false);
	m_pGameScene->showSettlementFrame(2, false);
	m_pGameScene->showSettlementFrame(3, false);
	m_pGameScene->m_Card.setCardArrayWithRow("ox4GameScene/Resource/game_card-hd.png", CSpriteCard::g_cbCardData, 55);
	//庄家标志
	m_pGameScene->showBankerIconAtPlayerNode(m_wCurrentUser);

	for (int i = 0; i < GAME_PLAYER_Ox4; i++)
	{
		m_cbUserOxCard[i] = 0;
	}

	m_cbSendCardCount = 0;
	m_cbSendCard = 0;
	memset(m_lTableScore, 0, sizeof(m_lTableScore));
	m_bCanShowHand = false;
	m_bShowHand = false;
	m_GameIsStart = true;

	//闲家加注, 在里面在进行判断
	IClientUserItem* pClientUserItem1 = kernel->GetTableUserItem(kernel->GetMeChairID());
	if (pClientUserItem1->GetUserStatus() == US_PLAYING)
	{
		m_pGameScene->addGoldState(!m_wCurrentUser, m_lTurnMaxGold);
	}
	

	
	OnStartremove(51);
	if (pClientUserItem1->GetUserStatus() == US_PLAYING)
	{
		kernel->SetGameClock(kernel->GetMeChairID(), 52, 30);
	}
	

	//闲家请下注
	m_pGameScene->showTipsWithType(1);

	return true;
}

bool ClientKernelSink_Ox4::onSubRecordConnectPlaying(const void * pBuffer, int wDataSize)
{
	if (wDataSize != sizeof(CMD_S_StatusPlay)) return false;
	CMD_S_StatusPlay * sendCard = (CMD_S_StatusPlay *)pBuffer;

	m_wCurrentUser = sendCard->wBankerUser;
	m_lTurnMaxGold = sendCard->lTurnMaxScore;

	for (int i = 0; i < 4; i++)
	{
		m_pGameScene->clearCardData(i);
	}

	CSpriteCard::setAutoScaleSize(1.0f);
	m_pGameScene->m_Card.setCardArrayWithRow("ox4GameScene/Resource/game_card-hd.png", CSpriteCard::g_cbCardData, 55);
	

	IClientKernel* kernel = IClientKernel::get();
	IClientUserItem* pClientUserItem1 = kernel->GetTableUserItem(kernel->GetMeChairID());
	///< 显示庄家
	//庄家标志
	m_pGameScene->showBankerIconAtPlayerNode(m_wCurrentUser);
	///< 显示下注信息
	int64 addScore = 0;

	for (int i = 0; i < GAME_PLAYER_Ox4; i++)
	{
		if (sendCard->lTableScore[i] != 0 && pClientUserItem1->GetUserStatus() == US_PLAYING)
		{
			addScore = sendCard->lTableScore[i];
			m_pGameScene->playingState(i, addScore);
		}
			
	}
	
	//拷贝手牌
	for (int i = 0; i < GAME_PLAYER_Ox4; i++)
	{
			memset(m_cbHandCardData[i], 0, sizeof(byte)*MAX_COUNT);
			memcpy(m_cbHandCardData[i], sendCard->cbHandCardData[i], sizeof(byte)*MAX_COUNT);
	}

	///< 首先发送牌
	m_pGameScene->sendCardToPoint();

	bool alery_show_hard = false;
	
	pClientUserItem1->GetNickName();
	for (int i = 0; i < GAME_PLAYER_Ox4; i++)
	{
		IClientUserItem* pClientUserItem = kernel->GetTableUserItem(i);
		if (pClientUserItem && pClientUserItem->GetUserStatus() == US_PLAYING)
		{
			pClientUserItem->GetNickName();
			if (sendCard->bOxCard[i] != 0xFF);
			{
				if (kernel->GetMeChairID() == i)
				{
					OnMeGive(1);
					m_pGameScene->playingState(true);
					alery_show_hard = true;
				}
				else
				{
					otherUserShowHard(i);
				}
			}
		}
		
	}
	OnStartremove(51);
	OnStartremove(52);

	if (pClientUserItem1->GetUserStatus() == US_PLAYING)
	{
		if (!alery_show_hard)
			kernel->SetGameClock(kernel->GetMeChairID(), 53, 30);
	}
	

	return true;
}

/// 用户加注
void ClientKernelSink_Ox4::OnMeAddGold(int Index)
{
 	IClientKernel* kernel = IClientKernel::get();
	//发送下注消息

	CMD_C_AddScore addScore;
	memset(&addScore,0,sizeof(addScore));

	addScore.lScore = m_lTurnMaxGold /Index;

 	kernel->SendSocketData(SUB_C_ADD_SCORE, &addScore, sizeof(CMD_C_AddScore));

	m_pGameScene->showAddScoreTips(m_lTurnMaxGold / Index, kernel->GetMeChairID(), 0);
}

//用户开牌
void ClientKernelSink_Ox4::OnMeGive(int index)
{
	SoundManager::shared()->playSound("ox4_OPEN_CARD");
	IClientKernel* kernel = IClientKernel::get();
	byte bFuckYou[5];
	m_bUserOxCard[kernel->GetMeChairID()] = m_pGameLogic->GetOxCard(m_cbHandCardData[kernel->GetMeChairID()], MAX_COUNT);
	m_cbUserOxCard[kernel->GetMeChairID()] = m_pGameLogic->GetCardType(m_cbHandCardData[kernel->GetMeChairID()], MAX_COUNT,bFuckYou);

	if (index==0)  //抛
	{

		for(int i=0; i<5; i++)
		{
			CSpriteCard* cardxx1 = m_pGameScene->m_Card.getCardWithData(bFuckYou[i]);
			if (cardxx1&&cardxx1->m_bSelect==true)
			{
				cardxx1->setPosition(ccp(cardxx1->getPosition().x,cardxx1->getPosition().y-20));
				cardxx1->m_bSelect=false;
			}
		}
		m_Istan=true;
		OnStartremove(51);
		//发送摊牌消息
		CMD_C_OxCard OxCard;
		memset(&OxCard,0,sizeof(OxCard));

		OxCard.bOX = m_bUserOxCard[kernel->GetMeChairID()];

		kernel->SendSocketData(SUB_C_OPEN_CARD, &OxCard, sizeof(CMD_C_OxCard));
		CCPoint pos[5];
		///< 找到自己卡牌的id
		int my_card_id = kernel->SwitchViewChairID(kernel->GetMeChairID());
		if(m_bUserOxCard[kernel->GetMeChairID()])
		{
			if(m_cbUserOxCard[kernel->GetMeChairID()]!=0)
			{
				m_pGameLogic->GetOxCard(bFuckYou, MAX_COUNT);
				int index = 0;
				for (int i=0; i<MAX_COUNT; i++)
				{
					CSpriteCard* xxx2 = (CSpriteCard *)m_pGameScene->getChildByTag(8520 + my_card_id * 5 + i);
					pos[i] = xxx2->getPosition();
					xxx2->removeFromParent();

				}
				for (int i=0; i<MAX_COUNT; i++)
				{
					CSpriteCard* card=m_pGameScene->m_Card.getCardWithData(bFuckYou[i]);
					card->setTag(8520 + my_card_id * 5 + i);
					if(i>2)
					{
						card->setPosition(ccp(pos[i].x, pos[i].y + 30));
						card->m_bSelect=true;
						card->setZOrder(1);
					}
					else
					{
						card->setPosition(ccp(pos[i].x,pos[i].y));
						card->m_bSelect=false;
						card->setZOrder(1);
					}

					m_pGameScene->addChild(card);
				}

			}
			else
			{


			}

		}
		m_pGameScene->showOxCardType(m_cbUserOxCard[kernel->GetMeChairID()], my_card_id, 1);
	}
	else if (index == 1)
	{
		CCPoint pos[5];
		int my_card_id = kernel->SwitchViewChairID(kernel->GetMeChairID());
		if (m_bUserOxCard[kernel->GetMeChairID()])
		{
			if (m_cbUserOxCard[kernel->GetMeChairID()] != 0)
			{
				m_pGameLogic->GetOxCard(bFuckYou, MAX_COUNT);
				int index = 0;
				for (int i = 0; i < MAX_COUNT; i++)
				{
					CSpriteCard* xxx2 = (CSpriteCard *)m_pGameScene->getChildByTag(8520 + my_card_id * 5 + i);

					pos[i] = xxx2->getPosition();
					xxx2->removeFromParent();

				}
				for (int i = 0; i<MAX_COUNT; i++)
				{
					CSpriteCard* card = m_pGameScene->m_Card.getCardWithData(bFuckYou[i]);
					card->setTag(8520 + my_card_id * 5 + i);
					if (i>2)
					{
						card->setPosition(ccp(pos[i].x, pos[i].y + 30));
						card->m_bSelect = true;
						card->setZOrder(1);
					}
					else
					{
						card->setPosition(ccp(pos[i].x, pos[i].y));
						card->m_bSelect = false;
						card->setZOrder(1);
					}

					m_pGameScene->addChild(card);
				}

			}
			else
			{


			}

		}
		m_pGameScene->showOxCardType(m_cbUserOxCard[kernel->GetMeChairID()], my_card_id, 1);
	}
	else
	{
		int my_card_id = kernel->SwitchViewChairID(kernel->GetMeChairID());
		if(m_bUserOxCard[kernel->GetMeChairID()])
		{
			for(int i=0; i<5; i++)
			{

				CSpriteCard* cardxx1 = m_pGameScene->m_Card.getCardWithData(bFuckYou[i]);
				cardxx1->setPosition(ccp(cardxx1->getPosition().x,cardxx1->getPosition().y+20));
				cardxx1->m_bSelect=true;
				if(i>2)
				{
					cardxx1->setPosition(ccp(cardxx1->getPosition().x,cardxx1->getPosition().y-20));
					cardxx1->m_bSelect=false;
				}
			}

		}
		m_pGameScene->showOxCardType(m_cbUserOxCard[kernel->GetMeChairID()], my_card_id, 1);
	}
}
//用户加注
bool ClientKernelSink_Ox4::OnSubAddGold(const void * pBuffer, word wDataSize)
{
	if (wDataSize!=sizeof(CMD_S_AddScore)) return false;
	CMD_S_AddScore * pGameStart=(CMD_S_AddScore *)pBuffer;

	IClientKernel* kernel = IClientKernel::get();

	CMD_S_AddScore AddScore;
	memset(&AddScore,0,sizeof(AddScore));
	memcpy(&AddScore,pBuffer,wDataSize);



	//自己加注
	if(AddScore.wAddScoreUser==kernel->GetMeChairID())
		return true;

	m_pGameScene->showAddScoreTips(AddScore.lAddScoreCount, AddScore.wAddScoreUser, true);

	return true;
}
//发牌消息
bool ClientKernelSink_Ox4::OnSubSendCard(const void * pBuffer, word wDataSize)
{
	if (wDataSize != sizeof(CMD_S_SendCard)) return false;
	CMD_S_SendCard * sendCard = (CMD_S_SendCard *)pBuffer;

	m_pGameScene->m_Card.setCardArrayWithRow("ox4GameScene/Resource/game_card-hd.png", CSpriteCard::g_cbCardData, 55);

	//拷贝手牌
	for (int i = 0; i < GAME_PLAYER_Ox4; i++)
	{
		memset(m_cbHandCardData[i], 0, sizeof(byte)*MAX_COUNT);
		memcpy(m_cbHandCardData[i], sendCard->cbCardData[i], sizeof(byte)*MAX_COUNT);
	}

	//清除桌面上的牌和结算框
	for (int i = 0; i < 4; i++)
	{
		m_pGameScene->clearCardData(i);
	}

	m_pGameScene->sendCardStart();

	OnStartremove(51);
	OnStartremove(52);

	IClientKernel* kernel = IClientKernel::get();
	kernel->SetGameClock(kernel->GetMeChairID(), 53, 30);

	return true;
}
//用户放弃
bool ClientKernelSink_Ox4::OnSubGiveUp(const void * pBuffer, word wDataSize)
{
	if (wDataSize!=sizeof(CMD_S_PlayerExit)) return false;
	CMD_S_PlayerExit * PlayerExit=(CMD_S_PlayerExit *)pBuffer;

	IClientKernel* kernel = IClientKernel::get();
	IClientUserItem* pClientUserItem= kernel->GetTableUserItem(PlayerExit->wPlayerID);

	if (pClientUserItem && pClientUserItem->GetChairID() != kernel->GetMeChairID())
	{
		m_pGameScene->clearOtherUser(pClientUserItem->GetChairID());
	}
	return true;

}
//游戏结束
bool ClientKernelSink_Ox4::OnSubGameEnd(const void * pBuffer, word wDataSize)
{
	if (wDataSize!=sizeof(CMD_S_GameEnd)) return false;
	CMD_S_GameEnd * GameEnd=(CMD_S_GameEnd *)pBuffer;

	m_pGameScene->removeChildByTag(205);	

	IClientKernel* kernel = IClientKernel::get();
	//所有玩家摊牌
	m_pGameScene->m_xxx=1;

	int hello = kernel->GetMeChairID();
	if (GameEnd->lGameScore[hello] > 0)
	{
		SoundManager::shared()->playSound("ox4_GAME_WIN");
	}
	else
	{
		SoundManager::shared()->playSound("ox4_GAME_LOST");
	}

	for(int i=0;i<GAME_PLAYER_Ox4;i++)
	{
		IClientUserItem* pClientUserItem= kernel->GetTableUserItem(i);
		if(pClientUserItem!=NULL)
		{
			m_pGameScene->showGameEndScore(i,GameEnd->lGameScore[i],1,i);
		}
	}

	m_pGameScene->createButtonUserReday();

	OnStartremove(53);
	//显示空闲倒计时 

	kernel->SetGameClock(kernel->GetMeChairID(),50,30);

	return true;
}

bool ClientKernelSink_Ox4::OnSubOpenCard(const void * pBuffer, word wDataSize)
{

	if (wDataSize!=sizeof(CMD_S_Open_Card)) return false;
	CMD_S_Open_Card * openCard=(CMD_S_Open_Card *)pBuffer;

	IClientKernel* kernel = IClientKernel::get();

	//摊牌消息
	if (openCard->wPlayerID == kernel->GetMeChairID())
	{
		if (!m_Istan)
		{
			OnMeGive(1);
		}
		return true;
	}
		
	SoundManager::shared()->playSound("follow");
	
	otherUserShowHard(openCard->wPlayerID);


	return true;
}

void ClientKernelSink_Ox4::otherUserShowHard(int player_id)
{
	IClientKernel * kernel = IClientKernel::get();

	byte bFuckYou[5] = { 0 };
	IClientUserItem* pClientUserItem = kernel->GetTableUserItem(player_id);
	int my_card_id = kernel->SwitchViewChairID(player_id);
	if (pClientUserItem != NULL)
	{
		if (m_cbHandCardData[player_id][0] != 0)
		{

			m_cbUserOxCard[player_id] = m_pGameLogic->GetCardType(m_cbHandCardData[player_id], MAX_COUNT, bFuckYou);



			if (m_cbUserOxCard[player_id] >= 0 && m_cbUserOxCard[player_id] < 10)
			{

			}
			else if (m_cbUserOxCard[player_id] == 10)
			{
				m_pGameLogic->SortCardList(m_cbHandCardData[player_id], 5);
				if (m_pGameLogic->GetCardColor(m_cbHandCardData[player_id][0]) == 0x41
					|| m_pGameLogic->GetCardColor(m_cbHandCardData[player_id][0]) == 0x42)
				{

				}
				else
				{

				}

			}
			else if (m_cbUserOxCard[player_id] == 11)
			{
				
			}
			else if (m_cbUserOxCard[player_id] == 12)
			{
				
			}
			else if (m_cbUserOxCard[player_id] == 20)
			{
			
			}
		}
	}
	byte bFuckYou1[5] = { 0 };
	m_cbUserOxCard[player_id] = m_pGameLogic->GetCardType(m_cbHandCardData[player_id], MAX_COUNT, bFuckYou1);

	CCPoint pos[5];
	CCPoint pos1[5];
	if (m_cbUserOxCard[player_id] != 0)
	{

		for (int i = 0; i < MAX_COUNT; i++)
		{
			CSpriteCard* xxx2 = (CSpriteCard *)m_pGameScene->getChildByTag(8520 + my_card_id * 5 + i);

			pos[i] = xxx2->getPosition();
			xxx2->removeFromParent();

		}
		for (int i = 0; i<MAX_COUNT; i++)
		{
			CSpriteCard* card = m_pGameScene->m_Card.getCardWithData(bFuckYou1[i]);
			card->setTag(8520 + my_card_id * 5 + i);
			if (i>2)
			{
				card->setPosition(ccp(pos[i].x, pos[i].y + 30));
				card->m_bSelect = true;
				card->setZOrder(1);
			}
			else
			{
				card->setPosition(ccp(pos[i].x, pos[i].y));
				card->m_bSelect = false;
				card->setZOrder(1);
			}

			m_pGameScene->addChild(card);
		}
	}
	else
	{
		for (int i = 0; i < MAX_COUNT; i++)
		{
			CSpriteCard* xxx2 = (CSpriteCard *)m_pGameScene->getChildByTag(8520 + my_card_id * 5 + i);
			pos1[i] = xxx2->getPosition();
			xxx2->removeFromParent();
		}
		for (int i = 0; i < MAX_COUNT; i++)
		{
			CSpriteCard* card = m_pGameScene->m_Card.getCardWithData(m_cbHandCardData[player_id][i]);
			card->setTag(8520 + my_card_id * 5 + i);
			card->setPosition(ccp(pos1[i].x, pos1[i].y));
			m_pGameScene->addChild(card, 1);

		}
	}



	m_pGameScene->showOxCardType(m_cbUserOxCard[player_id], my_card_id, 1);
}
//根据游戏模式获取金币类型
longlong ClientKernelSink_Ox4::GetUserScoreEx(IClientUserItem* pGameUserItem)
{
	if (pGameUserItem == NULL)
	{
		return 0L;
	}

	return pGameUserItem->GetUserScore();
}

bool ClientKernelSink_Ox4::OnUpdate(float fDt)
{
	IClientKernel* kernel = IClientKernel::get();

	if(kernel == 0 || m_pGameScene == 0)
		return false;
	dword timeNow = CoTimer::getCurrentTime();
	word wMeChairID = kernel->GetMeChairID();

	if (wMeChairID < GAME_PLAYER)
	{

	}
	return false;
}

void ClientKernelSink_Ox4::OnDlgBlackWhite(int iUserID, int iMode, unsigned int data)
{

}

void ClientKernelSink_Ox4::OnDlgStorage(int iMode, longlong lValue, unsigned int data)
{

}
void ClientKernelSink_Ox4::OnStartremove(int tga)
{
	IClientKernel* kernel = IClientKernel::get();
	kernel->KillGameClock(tga);

}
//////////////////////////////////////////////////////////////////////////
//时钟事件
//用户时钟
void ClientKernelSink_Ox4::OnEventUserClock(word wChairID, word wUserClock)
{
	IClientKernel * kernel = IClientKernel::get();
	if (kernel && m_pGameScene && wChairID == kernel->GetMeChairID())
	{
		m_pGameScene->setMeUserClock(wUserClock);
	}
}

//时钟删除
bool ClientKernelSink_Ox4::OnEventGameClockKill(word wChairID)
{
	return true;
}

//时钟信息
bool ClientKernelSink_Ox4::OnEventGameClockInfo(word wChairID, uint nElapse, word wClockID)
{
	IClientKernel * kernel = IClientKernel::get();

	switch (wClockID)
	{
	case 50:	//游戏开始前等待
		{
			//自动处理
			if (nElapse==0)
			{
				//离开游戏
				if ((kernel->IsLookonMode()==false)&&(wChairID==kernel->GetMeChairID())) 
				{

				}
				return true;
			}

			//超时警告
			if ((nElapse<=5)&&(wChairID== kernel->GetMeChairID())&&(kernel->IsLookonMode()==false))
			{

			}

			return true;
		}
	case 51:	//叫庄等待
		{
			//自动处理
			if (nElapse==0)
			{
				IClientKernel* kernel = IClientKernel::get();
				if(m_wCurrentUser==kernel->GetMeChairID())
				{

					ButtonWithNoCall();
					OnStartremove(51);
				}
			
			   return true;
			}
			return true;
		}
	case 52:	//下注等待
		{
			//自动处理
			if (nElapse==0)
			{
				IClientKernel* kernel = IClientKernel::get();
				if(m_wCurrentUser!=kernel->GetMeChairID())
				{
					m_pGameScene->mFrameLayer4->hideBetButton();
					OnMeAddGold(8);
					OnStartremove(52);
				}
				return true;
			}
			return true;
		}
	case 53:	//开牌等待
		{
			//自动处理
			if (nElapse==0)
			{
				if(m_Istan==false)
				{
					OnMeGive(0);
					OnStartremove(53);
				}
				return true;
			}
			return true;
		}
	}

	return false;
}

//////////////////////////////////////////////////////////////////////////
//用户事件

//用户进入
void ClientKernelSink_Ox4::OnEventUserEnter(IClientUserItem * pIClientUserItem, bool bLookonUser)
{
	IClientKernel * kernel = IClientKernel::get();
	if (!m_pGameScene)
		m_pGameScene = GameScene_Ox4::create();

	if(!bLookonUser && m_pGameScene)
	{ 
		m_userid[pIClientUserItem->GetChairID()] = kernel->SwitchViewChairID(pIClientUserItem->GetChairID());
			m_pGameScene->showUserInfo(pIClientUserItem,pIClientUserItem->GetUserInfo()->cbUserStatus);
	}
}
//用户离开
void ClientKernelSink_Ox4::OnEventUserLeave(IClientUserItem * pIClientUserItem, bool bLookonUser)
{

	if(!bLookonUser)
	{
		///< 是不是观看用户
		word wChairID = pIClientUserItem->GetChairID();
		IClientKernel* kernel = IClientKernel::get();
		if (kernel->GetMeChairID() != INVALID_CHAIR && kernel->GetMeChairID() != wChairID)
		{
			///< 其他用户离开
			m_pGameScene->net_user_level(wChairID);
			m_pGameScene->clearCardData(wChairID);
		}
		else if(m_pGameScene)
		{
			G_NOTIFY_D("EVENT_GAME_EXIT", MTData::create()); 
		}
	}
	
}

//用户积分
void ClientKernelSink_Ox4::OnEventUserScore(IClientUserItem * pIClientUserItem, bool bLookonUser)
{
	if (!bLookonUser && m_pGameScene)
		G_NOTIFY_D("EVENT_USER_SCORE", MTData::create(0,0,0,"","","",pIClientUserItem));
}

//用户状态
void ClientKernelSink_Ox4::OnEventUserStatus(IClientUserItem * pIClientUserItem, bool bLookonUser)
{
	if (!bLookonUser)
		G_NOTIFY_D("EVENT_USER_STATUS", MTData::create((unsigned int)pIClientUserItem));

	IClientKernel* kernel = IClientKernel::get();
	if (m_pGameScene)
	{
		//US_READY
		m_pGameScene->net_user_state_update(pIClientUserItem->GetChairID(), pIClientUserItem->GetUserStatus());
	}


}

//用户属性
void ClientKernelSink_Ox4::OnEventUserAttrib(IClientUserItem * pIClientUserItem, bool bLookonUser)
{
}

//用户头像
void ClientKernelSink_Ox4::OnEventCustomFace(IClientUserItem * pIClientUserItem, bool bLookonUser)
{
}


//开始消息
int ClientKernelSink_Ox4::OnMessageStart(uint wParam, uint lParam)
{
	IClientKernel * kernel = IClientKernel::get();

	//游戏变量
	m_wCurrentUser = INVALID_CHAIR;

	//删除时间
	kernel->KillGameClock(IDI_START_GAME);

	return 0;
}

void ClientKernelSink_Ox4::OnGameTimer(float dt)
{
	OnTimer(IDI_GAME_TIME);
}


//时间消息
void ClientKernelSink_Ox4::OnTimer(uint nIDEvent)
{

	IClientKernel * kernel = IClientKernel::get();
	if (kernel == 0)
		return;

	//游戏时间
	if ((nIDEvent == IDI_GAME_TIME) && (m_wCurrentUser != INVALID_CHAIR))
	{
		//变量定义
		bool bWarnning = false;
		bool bDeductTime = false;

	
		//设置界面
		word wViewChairID = m_wCurrentUser;

		//发送超时
		if (m_wCurrentUser == kernel->GetMeChairID())
		{
			
		}

		return;
	}
}

void ClientKernelSink_Ox4::CloseGameDelayClient()
{
	m_pGameScene->ExitGameTiming();
}

