#ifndef _GameScene_Ox4_H_
#define _GameScene_Ox4_H_

#include "cocos2d.h"
#include "cocos-ext.h"
#include "CMD_Ox4.h"
#include "FrameLayer_Ox4.h"
#include "PlayerLayer_Ox4.h"
#include "Kernel/kernel/user/IClientUserItem.h"
#include "common/IKeybackListener.h"
#include "common/TouchLayer.h"
#include "common/SpriteCard.h"
#include "Tools/tools/MessageLayer.h"

#define GAME_PLAYER_Ox4 4

using namespace Ox4;
using namespace Message;

USING_NS_CC;
USING_NS_CC_EXT;
class CGameClientView;
class PlayerLayer_Ox4;
class FrameLayer_Ox4;

class GameScene_Ox4 
	: public Scene
	, public ITouchSink
	,public IKeybackListener
{
public:
	//创建方法
	CREATE_FUNC(GameScene_Ox4);

	//构造函数
	GameScene_Ox4();

	~GameScene_Ox4();

	//初始化方法
	virtual bool init();

	static GameScene_Ox4* shared();
	///< 初始化游戏基础数据(很重要)
	static void initGameBaseData();
	 GLView *_openGLView;

	///< 场景进入
	virtual void onEnter();
	///< 场景退出
	virtual void onExit();
	///< 返回大厅回调(无用)
	void onKeybackClicked();
	///< 返回大厅的回调
	void onBackToRoom(Node* node);

	CCPoint			m_UserPoint[GAME_PLAYER_Ox4];				//玩家位置
	CCPoint			m_UserScorePoint[GAME_PLAYER_Ox4];			//玩家位置

	CCPoint			m_IdleScorePoint[GAME_PLAYER_Ox4];			//闲家注码位置
	CCPoint         m_UserAddedPoint[GAME_PLAYER_Ox4];			//用户已加注筹码		
	CCPoint			m_BankerPoint[GAME_PLAYER_Ox4];				//庄家位置

	CCPoint			m_OpenFontPoint[GAME_PLAYER_Ox4];			//tanpai
	CCPoint			m_OxPoint[GAME_PLAYER_Ox4];					//牌类型显示的点

	int				 m_xxx;
	int			    cardCount;									//当前发第几张牌

	MessageLayer*						mMessageLayer;	///< 消息层

public:

	//场景动画完成
	virtual void onEnterTransitionDidFinish();

	//场景动画开始
	virtual void onExitTransitionDidStart();

	// 添加消息列表
	void add_msg(const std::string& msg);

	//消息显示
	void func_msg_show();

	//读取下一条消息
	void func_msg_start_next();

	//显示下一条消息
	void msg_start_next(bool bRemoveFront);
	/// 界面显示时间的回调
	void setMeUserClock(int time);
	/// 显示加分按钮(只有第一个参数有效)
 	void ShowAddButton(longlong	Score);
	/// 显示分数结算Tips
 	void showAddScoreTips(longlong	Score,int userIndex,bool Tybe);

	///< 选择分数按钮的回调
 	void ccButtonAdd0(Ref* obj,Control::EventType e);
	///< 选择分数按钮的回调
 	void ccButtonAdd1(Ref* obj,Control::EventType e);
	///< 选择分数按钮的回调
 	void ccButtonAdd2(Ref* obj,Control::EventType e);
	///< 选择分数按钮的回调
 	void ccButtonAdd3(Ref* obj,Control::EventType e);
	///< 选择分数按钮的回调
 	void ccButtonAdd4(Node* sender);
	///< 选择分数按钮的回调
 	void ccButtonAdd5(Node* sender);
	/// 显示分数结算
	void showScoreTips(int charId, int64 score = 0);
	///< 显示用户信息
	void showUserInfo(IClientUserItem *pIClientUserItem, byte cbUserStatus);
	void showGameEndScore(int userName1,longlong lScore1, int type,int conunt);


	///< 准备状态(空闲状态)
	void createButtonUserReday();
	///< 准备状态(空闲状态)
	void freeState();
	///< 叫庄状态
	void callBankerState(int chair_id);
	///< 下注状态
	void addGoldState(int chair_id, long long maxValue);
	///< 游戏状态
	void playingState(int addGoldChair_id, long long addGoldScore);
	/// 游戏状态, 自己已经梭哈
	void playingState(bool me_alery_showhard);

	/// 开始发送牌
	void sendCardStart();
	///< 直接让牌在指定的位置
	void sendCardToPoint();
	///< 显示自己的手牌
	void ShowMeCard(Node* sender);
	void showCallBankerButton();
	void ccButtonUserCall(Ref* obj,Control::EventType e);
	void ccButtonUserNoCall(Ref* obj,Control::EventType e);
	//显示牛牛牌类型
	void showOxCardType(int cardtype,int index,int type);
	void showWaitCallBankerWithIndex();
	void showBankerWithIndex(int Index);
	void showTipsWithType(int Index);
	void showTanpaiButton();
	void ccButtonUser(Ref* obj,Control::EventType e);
	void ccButtonUserNo(Ref* obj,Control::EventType e);

private:
	void startUpdate();
	void stopUpdate();
	void frameUpdate(float dt);
	void closeCallback(cocos2d::Node* obj);

private:
	//定时器
	void func_send_time_check(float dt);
	Point locationFromTouch(Touch* touch);

	void onBtnClick(Ref* obj,Control::EventType e);

	///< 断线重连
	void func_Reconnect_on_loss(cocos2d::Ref * obj);
	void reconnect_on_loss(float dt);
public:
	///< 是断线重连
	static bool is_Reconnect_on_loss();
public:
	virtual bool onTouchBegan(Touch *pTouch, Event *pEvent);
	virtual void onTouchMoved(Touch *pTouch, Event *pEvent);
	virtual void onTouchEnded(Touch *pTouch, Event *pEvent);
	///< 倒计时退出游戏
	void  ExitGameTiming();
	///< 注销内核退出游戏
	void  CloseKernelExitGame(float dt);

private:
	CGlobalUserInfo *	pGlobalUserInfo;	//用户信息
	tagUserInsureInfo*	pUserInsureInfo;	//用户账户信息
	tagGlobalUserData*  pGlobalUserData;	//获取奖牌数
	Sprite*				spMessage_bg;		//系统消息
	Label*				mMsgContents[2];	//消息内容
	int					mMsgContentIndex;	//消息内容索引
	bool				is_touch_fire_;		//是否开炮		

	Label*				m_NickName;			//玩家昵称
	Label*				coinRate ;			//渔币兑换金币比率
	Label*				mAwardNum;			//奖牌数量
	
	TouchLayer*		    touch_layer_;		//触摸层	

	//按钮
 	ControlButton*	    close_bt;			// 关闭按钮

public:
	typedef std::list<std::string>		MSG_LIST;			//消息列表
	typedef MSG_LIST::iterator			MSG_LIST_ITER;	
	MSG_LIST							mMessageList;		//消息列表对象;

	CGameClientView*					mGameView;
	PlayerLayer_Ox4*					mPlayerLayer4;	///< 玩家层;
	FrameLayer_Ox4*						mFrameLayer4;	///< 框架层;
	CSpriteCard                         m_Card;
	std::vector<CSpriteCard *>			mCardList[2];		///< 所有卡的容器

public:
	void net_user_state_update(int chair_id, byte user_state);
	///< 用户离开
	void net_user_level(int chair_id);
	///< 用户摊牌
	void net_user_show_hard(int chair_id);
public:
	void clearPlayerPrepare();
	///< 在框架层上面显示文字,(中间栏的内容)
	void showContentForFrameLayer(std::string str);
	///< 显示庄家标志和框
	void showBankerIconAtPlayerNode(int chairId);
	///< 清理其他用户
	void clearOtherUser(int chair_id);
	///< 清理牌数据
	void clearCardData(int chair_id);
	///< 退出游戏
	void closeGameNotify(cocos2d::Ref * ref);
	///< 显示结算框
	void showSettlementFrame(int chair_id, bool isShow);
	
};
#endif // _GameScene_SH2_H_