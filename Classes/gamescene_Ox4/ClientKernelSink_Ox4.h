#ifndef _ClientKernelSink_Ox4_H_
#define _ClientKernelSink_Ox4_H_

#pragma once

#include "cocos2d.h"
#include "GameLogic_Ox4.h"
#include "CMD_Ox4.h"
#include "GameScene_Ox4.h"
#include "Kernel/kernel/game/IClientKernelSink.h"


USING_NS_CC;
class CSoundSetting;
class GameScene_Ox4;
class GameLogic_Ox4;
//////////////////////////////////////////////////////////////////////////
#define GAME_PLAYER_Ox4			4
#define	MAX_COUNT				5
//游戏引擎
class ClientKernelSink_Ox4 :public Ref ,public IClientKernelSink
{
	//游戏变量
public:
	GameLogic_Ox4 *					m_pGameLogic;						//游戏逻辑
	word							m_wBankerUser;						//庄家玩家
	word							m_wCurrentUser;						//当前用户

	char							m_szUserAccounts[2][LEN_ACCOUNTS];	//玩家帐号

	bool							m_SendCard;                         //发牌标志
	byte							m_wSendIndex;						//发牌索引
	byte							m_cbSendCardCount;					//发牌张数
	byte							m_cbHandCardData[GAME_PLAYER_Ox4][MAX_COUNT];
	int								m_nSendChair;						//发送座位	
	int								m_nSendCount;						//发牌计数		
	CCPoint							m_CardPoint[GAME_PLAYER_Ox4];		//手牌位置

	longlong						m_lBasicGold;						//单元数目
	longlong						m_lTurnMaxGold;						//最大下注
	longlong						m_lTurnBasicGold;					//基础下注
	bool							m_bCanShowHand;						//是否能梭哈
	bool							m_bShowHand;						//是否已梭哈
	longlong						m_lTableScore[GAME_PLAYER_Ox4];		//玩家总下注
	bool							m_GameIsStart;
	int								m_nMultiples[4];
	int								m_cbSendCard;
	byte							m_cbPlayStatus[GAME_PLAYER_Ox4];		//玩家状态
	bool							m_bCallBanker;						//请求庄家

	byte							m_bUserOxCard[GAME_PLAYER_Ox4];		//用户牛牛数据
	byte							m_cbUserOxCard[GAME_PLAYER_Ox4];		//用户牌点数据

	bool	m_Istan;
	bool	m_IsZhongTu;
	int                             m_userid[GAME_PLAYER_Ox4];
	//函数定义
public:
	//构造函数
	ClientKernelSink_Ox4();
	//析构函数
	virtual ~ClientKernelSink_Ox4();

	//控制接口
public:
	//启动游戏
	virtual bool SetupGameClient();
	//重置游戏
	virtual void ResetGameClient();
	//关闭游戏
	virtual void CloseGameClient(int exit_tag = 0);

	//框架事件
public:
	//系统滚动消息
	virtual bool OnGFTableMessage(const char* szMessage);
	//全局消息
	virtual bool OnGFGlobalMessage(const char* szMessage);
	//等待提示
	virtual bool OnGFWaitTips(bool bWait);
	//比赛信息
	virtual bool OnGFMatchInfo(tagMatchInfo* pMatchInfo);
	//比赛等待提示
	virtual bool OnGFMatchWaitTips(tagMatchWaitTip* pMatchWaitTip);
	//比赛结果
	virtual bool OnGFMatchResult(tagMatchResult* pMatchResult);

	//游戏事件
public:
	//旁观消息
	virtual bool OnEventLookonMode(void* data, int dataSize);
	//场景消息
	virtual bool OnEventSceneMessage(byte cbGameStatus, bool bLookonUser, void* data, int dataSize);
	//游戏消息
	virtual bool OnEventGameMessage(int sub, void* data, int dataSize);

	//时钟事件
public:
	//用户时钟
	virtual void OnEventUserClock(word wChairID, word wUserClock);
	//时钟删除
	virtual bool OnEventGameClockKill(word wChairID);
	//时钟信息
	virtual bool OnEventGameClockInfo(word wChairID, uint nElapse, word wClockID);

	//用户事件
public:
	//用户进入
	virtual void OnEventUserEnter(IClientUserItem * pIClientUserItem, bool bLookonUser);
	//用户离开
	virtual void OnEventUserLeave(IClientUserItem * pIClientUserItem, bool bLookonUser);
	//用户积分
	virtual void OnEventUserScore(IClientUserItem * pIClientUserItem, bool bLookonUser);
	//用户状态
	virtual void OnEventUserStatus(IClientUserItem * pIClientUserItem, bool bLookonUser);
	//用户属性
	virtual void OnEventUserAttrib(IClientUserItem * pIClientUserItem, bool bLookonUser);
	//用户头像
	virtual void OnEventCustomFace(IClientUserItem * pIClientUserItem, bool bLookonUser);

	//消息定义
public:
	//开始消息
	int OnMessageStart(uint wParam, uint lParam);

	//内部函数
protected:
	//更新按钮
	bool UpdateManualControl(word wViewStepCount);
	//时间消息
	void OnTimer(uint nIDEvent);
	void OnGameTimer(float dt);

	//LKPY函数
	////////////////////////////////////////////////////////////////////
public:
	static ClientKernelSink_Ox4* GetInstance();
	static void Release();

	bool InitClientKernel(int szCMD);

	bool OnUpdate(float fDt);

	bool OnRender(float hscale, float vscale);

	void SetGameActive(bool active);

	//获取系统消息
	std::string GetGlobalMessage();

public:

	void OnDlgBlackWhite(int iUserID, int iMode, unsigned int data);

	void OnDlgStorage(int iMode, longlong lValue, unsigned int data);
protected:
	//根据游戏模式获取金币类型
	longlong GetUserScoreEx(IClientUserItem* pGameUserItem);

private:
	CSoundSetting*		m_pSoundSetting;
	bool				m_bShowHelp;
	word				m_wShowUserInfoCharID;
	bool				m_bGameReady;
	bool				m_bActive;
	CCPoint				m_Offest;
	float				m_fElapse;

	bool				m_bMirrorShow;
	std::string			szGlobalMessage;
	CCPoint				m_ptOffest;
protected:
	static ClientKernelSink_Ox4*	s_Instance;

private:
	GameScene_Ox4*			m_pGameScene;
	bool					mIsNetworkPrepared;
public:
		void OnStartremove(int tga);

		//叫庄
		bool OnUserCallBanker(const void * pBuffer, word wDataSize);
		//游戏开始
		bool OnSubGameStart(const void * pBuffer, word wDataSize);
		//用户加注
		bool OnSubAddGold(const void * pBuffer, word wDataSize);
		//用户放弃
		bool OnSubGiveUp(const void * pBuffer, word wDataSize);
		//发牌消息
		bool OnSubSendCard(const void * pBuffer, word wDataSize);
		//游戏结束
		bool OnSubGameEnd(const void * pBuffer, word wDataSize);
		///< 用户摊牌
		bool OnSubOpenCard(const void * pBuffer, word wDataSize);	
		//断线重连_叫庄
		bool onSubRecordConnectBanker(const void * pBuffer, int wDataSize);
		//断线重连_下注
		bool onSubRecordConnectAddGold(const void * pBuffer, int wDataSize);
		//断线重连_游戏
		bool onSubRecordConnectPlaying(const void * pBuffer, int wDataSize);
		//用户加注
		void OnMeAddGold(int Index);
		//用户开牌
		void OnMeGive(int index);
		//看底牌回调
		void Showback(Node* sender);
		//叫庄回调(发送)
		void ButtonWithCallBanker();
		//无人叫庄(发送)
		void ButtonWithNoCall();

		///< 别的用户摊牌
		void otherUserShowHard(int player_id);

		virtual void CloseGameDelayClient();

};

extern ClientKernelSink_Ox4 gClientKernelSink_Ox4;
#endif // _ClientKernelSink_H_