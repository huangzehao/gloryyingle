﻿//
//  SpriteCard.cpp
//  YYGameProject
//
//  Created by 曾 亚东 on 13-5-30.
//
//

#include "SpriteCard.h"

//默认全局值，可根据实际修改
//CCArray* CSpriteCard::g_pCardArray= CCArray::create();

Vector<CSpriteCard*> CSpriteCard::g_pCardArray;

byte CSpriteCard::g_cbMaskValue=0x0F;
byte CSpriteCard::g_cbMaskColor=0xF0;
byte CSpriteCard::g_cbCardBack=0x43;
float CSpriteCard::g_fCardSpace=22.f;

// byte CSpriteCard::g_cbCardData[CARD_MAXCOUNT]=
// {
// 	0x01,0x02,0x03,0x04,0x05,0x06,0x07,0x08,0x09,0x0A,0x0B,0x0C,0x0D,	//∑ΩøÈ A - K
// 	0x11,0x12,0x13,0x14,0x15,0x16,0x17,0x18,0x19,0x1A,0x1B,0x1C,0x1D,	//√∑ª® A - K
// 	0x21,0x22,0x23,0x24,0x25,0x26,0x27,0x28,0x29,0x2A,0x2B,0x2C,0x2D,	//∫ÏÃ“ A - K
// 	0x31,0x32,0x33,0x34,0x35,0x36,0x37,0x38,0x39,0x3A,0x3B,0x3C,0x3D,	//∫⁄Ã“ A - K
//  	0x41,0x42,0x43,
// };


byte CSpriteCard::g_cbCardData[CARD_MAXCOUNT] =
{
	0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0A, 0x0B, 0x0C, 0x0D,	//∑ΩøÈ A - K
	0x11, 0x12, 0x13, 0x14, 0x15, 0x16, 0x17, 0x18, 0x19, 0x1A, 0x1B, 0x1C, 0x1D,	//√∑ª® A - K
	0x21, 0x22, 0x23, 0x24, 0x25, 0x26, 0x27, 0x28, 0x29, 0x2A, 0x2B, 0x2C, 0x2D,	//∫ÏÃ“ A - K
	0x31, 0x32, 0x33, 0x34, 0x35, 0x36, 0x37, 0x38, 0x39, 0x3A, 0x3B, 0x3C, 0x3D,	//∫⁄Ã“ A - K
	0x4E, 0x4F, 0x43, 0x44, 0x45, 0x46, 0x47
};


CSpriteCard::CSpriteCard()
{
   // initSpriteSelector();
    //initSpriteParamData();
}

CSpriteCard::~CSpriteCard()
{
    
    
}

CSpriteCard* CSpriteCard::spriteCard(byte cbData)
{
    CSpriteCard* pCard=new CSpriteCard;
    pCard->initValue();
    pCard->m_cbData=cbData;
    pCard->autorelease();
    
    return pCard;
}

CSpriteCard* CSpriteCard::spriteCardWithFile(char* file)
{
    CSpriteCard* pCard= new CSpriteCard;
    pCard->initWithFile(file);
    pCard->initValue();
    pCard->autorelease();
    return pCard;
}

CSpriteCard* CSpriteCard::spriteCardWithFile(char* file, CCRect rect)
{
    CSpriteCard* pCard=new CSpriteCard;
    pCard->autorelease();
    pCard->initWithFile(file, rect);
    pCard->initValue();
    
    return pCard;
}

CSpriteCard* CSpriteCard::spriteCardWithTexture(CCTexture2D* pTex, CCRect rect)
{
    CSpriteCard* pCard = new CSpriteCard;
    pCard->autorelease();
    pCard->initWithTexture(pTex, rect);
    pCard->initValue();
    
    return pCard;
}

CSpriteCard* CSpriteCard::spriteCardWithCopy(CSpriteCard* pCard)
{
    CSpriteCard* pNewCard = new CSpriteCard;
    pNewCard->initWithTexture(pCard->getTexture(), pCard->getTextureRect());
    
    pNewCard->m_cbColor = pCard->m_cbColor;
    pNewCard->m_cbValue = pCard->m_cbValue;
    pNewCard->m_cbData  = pCard->m_cbData;
    pNewCard->m_cbStatus= pCard->m_cbStatus;
    pNewCard->m_cbMark  = pCard->m_cbMark;
    pNewCard->m_bSelect = pCard->m_bSelect;
    
    strcpy(pNewCard->m_pCardName, pCard->m_pCardName);
	pNewCard->setScale(mCardScale);
    pNewCard->autorelease();
    return pNewCard;
}



void CSpriteCard::initValue()
{
    m_cbColor   = 0;
    m_cbValue   = 0;
    m_cbData    = 0;
    m_wIndex    = 0;
    memset(m_pCardName, 0, sizeof(m_pCardName));
    
    m_PointPre  = Vec2::ZERO;
    m_bTouch    = true;
	m_bSwallows = true;
    m_wViewID   = 0;
    
    m_cbStatus= 0;
    m_cbMark  = 0;
    m_bSelect = 0;
    
    //initSpriteSelector();
}



void CSpriteCard::onEnter()
{
    CCSprite::onEnter();
	if(m_bTouch){
		//CCDirector::sharedDirector()->getTouchDispatcher()->addTargetedDelegate(this, kYYProiorityCard, m_bSwallows);
	}
}
void CSpriteCard::onExit()
{
    CCSprite::onExit();
    if(m_bTouch)
	{
		//CCDirector::sharedDirector()->getTouchDispatcher()->removeDelegate(this);
	}
}

void CSpriteCard::setCardTexture(CSpriteCard* pCard)
{
    this->setTexture(pCard->getTexture());
    this->setTextureRect(pCard->getTextureRect());
}


bool CSpriteCard::onTouchBegan(Touch *pTouch, Event *pEvent)
{
//    CCPoint point = this->convertTouchToNodeSpaceAR(pTouch);
	
//     if(YYNodeSpaceBoundRect(this).containsPoint(point))
//     {
//         if(m_pSelectorTarget && m_pSelectorBegan)
//         {
//             (m_pSelectorTarget->*m_pSelectorBegan)(this);
//         }
//         
//         return true;
//     }

    return false;
}

void CSpriteCard::onTouchMoved(Touch *pTouch, Event *pEvent)
{

//    CCPoint point = this->convertTouchToNodeSpaceAR(pTouch);
    
//     if(YYNodeSpaceBoundRect(this).containsPoint(point))
//     {
//         if(m_pSelectorTarget && m_pSelectorMoved)
//         {
//             (m_pSelectorTarget->*m_pSelectorMoved)(this);
//         }
//     }
}
void CSpriteCard::onTouchEnded(Touch *pTouch, Event *pEvent)
{
//    CCPoint point = this->convertTouchToNodeSpaceAR(pTouch);
    
//     if(YYNodeSpaceBoundRect(this).containsPoint(point))
//     {
//         if(m_pSelectorTarget && m_pSelectorEnded)
//         {
//             (m_pSelectorTarget->*m_pSelectorEnded)(this);
//         }
//     }
}




/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//设置扑克 宽高
void CSpriteCard::setCardArrayWithFile(char* file, float fWidth, float fHeight)
{
    //获得tex
    CCTexture2D* pTex=CCTextureCache::sharedTextureCache()->addImage(file);
    
    //设置扑克
    CSpriteCard::setCardArrayWithTexture(pTex, g_cbCardData, CARD_MAXCOUNT, fWidth, fHeight);
}

void CSpriteCard::setCardArrayWithData(char* file, byte cbCardData, word wCol, word wRow)
{
    //获得tex
    CCTexture2D* pTex=CCTextureCache::sharedTextureCache()->addImage(file);
    float fWidth = pTex->getContentSize().width/wRow;
    float fHeight = pTex->getContentSize().height/wCol;

    //牌值与花色
    int color = CSpriteCard::GetCardColor(cbCardData)>>4;
    int value = ((cbCardData==0x4E)||(cbCardData==0x4F))?CSpriteCard::GetCardValue(cbCardData)%14:CSpriteCard::GetCardValue(cbCardData)-1;
    
    //扑克创建
    CSpriteCard* pCard=CSpriteCard::spriteCard(cbCardData);
    pCard->initWithTexture(pTex, CCRectMake(fWidth*value, fHeight*color, fWidth, fHeight));
    pCard->m_cbValue = value;
    pCard->m_cbColor = color;
    //pCard->m_wIndex = g_pCardArray->count();
	pCard->m_wIndex = g_pCardArray.size();
    pCard->m_cbData = cbCardData;
    
    //设置对象
	g_pCardArray.pushBack(pCard);
    //g_pCardArray->addObject(pCard);
}

//设置扑克 行列
void CSpriteCard::setCardArrayWithRow(char* file, const byte cbCardData[], word wCount, word wCol, word wRow)
{
    //获得tex
    CCTexture2D* pTex=CCTextureCache::sharedTextureCache()->addImage(file);
    float fWidth = pTex->getContentSize().width/wRow;
    float fHeight = pTex->getContentSize().height/wCol;

    //设置扑克
    CSpriteCard::setCardArrayWithTexture(pTex, cbCardData, wCount, fWidth, fHeight);
}

//设置扑克
void CSpriteCard::setCardArrayWithTexture(CCTexture2D* pTex, const byte cbCardData[], word wCount,float fWidth, float fHeight)
{
    //reset
    if(g_pCardArray.size())
        g_pCardArray.clear();
    
    //创建扑克对象
    for (word i=0; i<wCount; i++)
    {
        //牌值与花色
		int color = CSpriteCard::GetCardColor(cbCardData[i])>>4;
		int value = 0;
		if (((cbCardData[i] == 0x4E) || (cbCardData[i] == 0x4F)))
		{
			value = CSpriteCard::GetCardValue(cbCardData[i]) % 14;
		} 
		else
		{
			value = CSpriteCard::GetCardValue(cbCardData[i]) - 1;
		}
        //扑克创建
        CSpriteCard* pCard=CSpriteCard::spriteCard(cbCardData[i]);
        pCard->initWithTexture(pTex, CCRectMake(fWidth*value, fHeight*color, fWidth, fHeight));
        pCard->m_cbValue = value;
        pCard->m_cbColor = color;
        pCard->m_wIndex = i;
        pCard->m_cbData = cbCardData[i];
		pCard->setScale(mCardScale);
        //设置对象
        g_pCardArray.pushBack(pCard);
    }
}

void CSpriteCard::setCardWithData(CSpriteCard* pCard)
{
     g_pCardArray.pushBack(pCard);
}

void CSpriteCard::setCardWithData(CSpriteCard* pCard, byte cbData)
{
    pCard->setCardData(cbData);
     g_pCardArray.pushBack(pCard);
}

CSpriteCard* CSpriteCard::getCardWithData(byte cbData)
{
	/*
    CCObject* pObject=NULL;
    CCARRAY_FOREACH(g_pCardArray, pObject)
    {
        CSpriteCard* pCard=(CSpriteCard*)pObject;
        
        if(pCard->getCardData()==cbData)
            return pCard;
    }*/
	if (cbData == 0x41)
	{
		cbData = 0x4E;
	}
	else if (cbData == 0x42)
	{
		cbData = 0x4F;
	}
	
	for (CSpriteCard *pCard : g_pCardArray)
	{
		if (pCard->getCardData() == cbData)
		{
			pCard->setScale(mCardScale);
			return pCard;
		}
			
	}
    
    return NULL;
}
CSpriteCard* CSpriteCard::getCardWithTag(word wTag)
{
	for (CSpriteCard *pCard : g_pCardArray)
	{
		if (pCard->getTag() == wTag)
		{
			pCard->setScale(mCardScale);
			return pCard;
		}
	}

	return NULL;
}
CSpriteCard* CSpriteCard::getCardCopyWithData(byte cbData)
{
    return CSpriteCard::spriteCardWithCopy(CSpriteCard::getCardWithData(cbData));
}

CSpriteCard* CSpriteCard::getCardWithBack()
{
    return CSpriteCard::getCardCopyWithData(CSpriteCard::g_cbCardBack);
}
CSpriteCard* CSpriteCard::getCardWithIndex(word wIndex)
{
	
    CSpriteCard* pCard=(CSpriteCard*)g_pCardArray.at(wIndex);
    assert(pCard->m_wIndex==wIndex);
    return pCard;
}
CSpriteCard* CSpriteCard::getCardCopyWithIndex(word wIndex)
{
    return CSpriteCard::spriteCardWithCopy(CSpriteCard::getCardWithIndex(wIndex));
}

void CSpriteCard::setTouchProiorty(int nProiorty, bool bSwallows/*=true*/)
{
	m_bSwallows=bSwallows;

	if(isRunning() && m_bTouch)
	{
		//Director()->getTouchDispatcher()->removeDelegate(this);
		//Director()->getTouchDispatcher()->addTargetedDelegate(this, nProiorty, m_bSwallows);
	}
}

void CSpriteCard::setAutoScaleSize(float as)
{
	mCardScale = as;
}

float CSpriteCard::mCardScale = 0.7f;


