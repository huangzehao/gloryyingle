#ifndef _IKeybackListener_H_
#define _IKeybackListener_H_

class IKeybackListener
{
public:
	virtual ~IKeybackListener(){};
	virtual void onKeybackClicked() = 0;
};

#endif // _IKeybackListener_H_