#include "ui/UIWidget.h"
#include <math.h>
#include "cocos2d.h"
#include "cocos-ext.h"

USING_NS_CC_EXT;
using namespace cocos2d::ui;

#define ANCHOR_LEFT_BOTTOM Vec2(0, 0)
#define ANCHOR_RIGHT_BOTTOM Vec2(1, 0)
#define ANCHOR_BOTTOM Vec2(0.5, 0)
#define ANCHOR_LEFT Vec2(0, 0.5)
#define ANCHOR_CENTER Vec2(0.5, 0.5)
#define ANCHOR_RIGHT Vec2(1, 0.5)
#define ANCHOR_LEFT_TOP Vec2(0, 1)
#define ANCHOR_TOP Vec2(0.5, 1)
#define ANCHOR_RIGHT_TOP Vec2(1, 1)

template<typename T>
static Vec2 leftTopInner(T& node) { return Vec2(0, node->getContentSize().height); }
template<typename T>
static Vec2 leftInner(T& node)		  { return Vec2(0, node->getContentSize().height / 2); }
template<typename T>
static Vec2 rightTopInner(T& node)    { return Vec2(node->getContentSize().width, node->getContentSize().height); };
template<typename T>
static Vec2 rightInner(T& node)       { return Vec2(node->getContentSize().width, node->getContentSize().height / 2); };
template<typename T>
static Vec2 topInner(T& node)		  { return Vec2(node->getContentSize().width / 2, node->getContentSize().height); };
template<typename T>
static Vec2 leftBottomInner(T& node)  { return Vec2(0, 0); };
template<typename T>
static Vec2 rightBottomInner(T& node) { return Vec2(node->getContentSize().width, 0); };
template<typename T>
static Vec2 bottomInner(T& node)      { return Vec2(node->getContentSize().width / 2, 0); };
template<typename T>
static Vec2 centerInner(T& node)      { return Vec2(node->getContentSize().width / 2, node->getContentSize().height / 2); };
template<typename T>
static Vec2 leftTopOutter(T& node)    { Rect rect = node->getBoundingBox(); return Vec2(rect.origin.x, rect.origin.y + rect.size.height); };
template<typename T>
static Vec2 leftOutter(T& node)       { Rect rect = node->getBoundingBox(); return Vec2(rect.origin.x, rect.origin.y + rect.size.height / 2); };
template<typename T>
static Vec2 rightTopOutter(T& node)   { Rect rect = node->getBoundingBox(); return Vec2(rect.origin.x + rect.size.width, rect.origin.y + rect.size.height); };
template<typename T>
static Vec2 rightOutter(T& node)      { Rect rect = node->getBoundingBox(); return Vec2(rect.origin.x + rect.size.width, rect.origin.y + rect.size.height / 2); };
template<typename T>
static Vec2 topOutter(T& node)        { Rect rect = node->getBoundingBox(); return Vec2(rect.origin.x + rect.size.width / 2, rect.origin.y + rect.size.height); };
template<typename T>
static Vec2 leftBottomOutter(T& node) { Rect rect = node->getBoundingBox(); return Vec2(rect.origin.x, rect.origin.y); };
template<typename T>
static Vec2 rightBottomOutter(T& node){ Rect rect = node->getBoundingBox(); return Vec2(rect.origin.x + rect.size.width, rect.origin.y); };
template<typename T>
static Vec2 bottomOutter(T& node)     { Rect rect = node->getBoundingBox(); return Vec2(rect.origin.x + rect.size.width / 2, rect.origin.y); };
template<typename T>
static Vec2 centerOutter(T& node)     { Rect rect = node->getBoundingBox(); return Vec2(rect.origin.x + rect.size.width / 2, rect.origin.y + rect.size.height / 2); };

/**
* 以任意锚点作为参照 设置node在父view的位置,(根据偏移计算，不会改变影响node的锚点)
* @param {cocos2d::Node} node
* @param {CCPoint} position
* @param {CCPoint} visualAnchorPoint
*/
template<typename T>
static void setPos(T const& node, Vec2 position, Vec2 visualAnchorPoint) {
	Vec2 nodeAnchorPoint = node->getAnchorPoint();
	if (node->isIgnoreAnchorPointForPosition()){
		nodeAnchorPoint = Vec2(0, 0);
	}
	float width = node->getBoundingBox().size.width;
	float height = node->getBoundingBox().size.height;
	float offsetX = (nodeAnchorPoint.x - visualAnchorPoint.x) * width;
	float offsetY = (nodeAnchorPoint.y - visualAnchorPoint.y) * height;
	node->setPosition(Vec2(position.x + offsetX, position.y + offsetY));
}

/**
* 将一组Node/Widget按矩阵排列
* @param {Array} array
* @param {Number|int} col 列数
* @param {cc.Size} [gapSize] 间距 默认0，紧密排列，没有间距
* @param {cc.Size} [eachContentSize] 按这个Size排列 默认取第一个的size，如果这组Node/Widget不等大，请传入一个合适的size
* @returns {*|cc.Node|ccui.Widget}
*/
template <typename T>
static Layout* arrangeMatrixByColumn(Vector<T*> const& children, const ssize_t col, const Size gapSize, const Size eachContentSize) {
	Layout* root = ui::Layout::create();

	int cnt = children->size();
	if (cnt == 0) {
		return root;
	}

	int fullRow = ceil(cnt / col);
	int rem = cnt % col;
	int row = fullRow + (rem > 0 ? 1 : 0);

	if (eachContentSize.equals(Size::ZERO)) {
		eachContentSize = children->front()->getBoundingBox().size;
	}

	Size size = Size(0, 0);
	size.width = ((col == 1) ? (eachContentSize.width) : (eachContentSize.width * col + gapSize.width * (col - 1)));
	size.height = ((row == 1) ? (eachContentSize.height) : (eachContentSize.height * row + gapSize.height * (row - 1)));

	root->setContentSize(size);

	float posX = eachContentSize.width * 0.5;
	float posY = size.height - eachContentSize.height * 0.5;
	for (ssize_t i = 0; i < cnt; ++i) {
		T* child = children->at(i);
		root->addChild(child);
		setPos(child, Vec2(posX, posY), Vec2(0.5, 0.5));

		if (i % col < col - 1) {
			posX += (eachContentSize.width + gapSize.width);
		}
		else {
			posX = eachContentSize.width * 0.5;
			posY -= (eachContentSize.height + gapSize.height);
		}
	}

	return root;
};

/**
* 将一组Node/Widget按矩阵排列(但限制行数)
* @param {Array} array
* @param {Number|int} row 行数
* @param {cc.Size} [gapSize] 间距 默认0，紧密排列，没有间距
* @param {cc.Size} [eachContentSize] 按这个Size排列 默认取第一个的size，如果这组Node/Widget不等大，请传入一个合适的size
* @returns {*|cc.Node|ccui.Widget}
*/
template <typename T>
static Layout* arrangeMatrixByRow(Vector<T> const& children, const ssize_t row, const Size gapSize) {
	Layout* root = Layout::create();

	int cnt = children->size();
	if (cnt == 0) {
		return root;
	}

	ssize_t col = ceil(cnt / row); //列数

	Size eachContentSize = children->front()->getBoundingBox().size;

	Size size = Size(0, 0);
	size.width = ((col == 1) ? (eachContentSize.width) : (eachContentSize.width * col + gapSize.width * (col - 1)));
	size.height = ((row == 1) ? (eachContentSize.height) : (eachContentSize.height * row + gapSize.height * (row - 1)));

	root->setContentSize(size);

	ssize_t posX = 0;
	ssize_t posY = 0;
	for (ssize_t i = 0; i < cnt; ++i) {
		Node* child = children->at(i);
		root->addChild(child);

		ssize_t curCol = ceil((i + 1) / row);
		ssize_t rem = (i + 1 + row) % row;
		ssize_t curRow = (rem == 0 ? row : rem);

		posX = eachContentSize.width  * (curCol - 1) + gapSize.width * (curCol - 1);
		posY = eachContentSize.height * (row - curRow) + gapSize.height * (row - curRow); //从上往下排

		setPos(child, Vec2(posX, posY), Vec2(0, 0));
	}

	return root;
};

enum UIAlign
{
	UI_ALIGNMENT_VERTICAL_CENTER = 0,
	UI_ALIGNMENT_VERTICAL_LEFT,
	UI_ALIGNMENT_VERTICAL_RIGHT,
	UI_ALIGNMENT_HORIZONTAL_TOP,
	UI_ALIGNMENT_HORIZONTAL_CENTER,
	UI_ALIGNMENT_HORIZONTAL_BOTTOM
};

/**
* 创建一个Layout, 其所有子View 水平组合
* @param {Number} padding
* @param {Number|UIAlign:: alignment
* @param {Array} array or ...
* @returns {ui::Layout}
*/
template <typename T>
static Layout* createPanelAlignHorizontal(float padding, const UIAlign alignment, T* child, ...)
{
	Vector<T*> children;
	va_list args;
	va_start(args, child);
	T* iChild = child;
	while(iChild){
		children.pushBack(iChild);
		iChild = va_arg(args, T*);
	}
	va_end(args);

	return createPanelAlignHorizontal(padding, alignment, children);
};

/**
* 创建一个 Layout, 其所有子View 水平组合
* @param {Number} padding
* @param {Number|UIAlign:: alignment
* @param {Vector} Vector or ...
* @returns {ui::Layout}
*/
template <typename T>
static Layout* createPanelAlignHorizontal(const float padding, const UIAlign alignment, Vector<T*>& children)
{
	// 使用 UIPanel 绝对坐标
	// 算出宽高
	float width = 0;
	float height = 0;
	for (ssize_t i = 0; i < children.size(); ++i)
	{
		T* child = children.at(i);
		width += child->getBoundingBox().size.width;
		width += padding;
		height = fmax(height, child->getBoundingBox().size.height);
	}

	// 减去最后的间隔
	width -= padding;

	float offsetX = 0;

	ui::Layout* panel = Layout::create();
	panel->setContentSize(Size(width, height));
	panel->setAnchorPoint(Vec2(0.5, 0.5));

	for (ssize_t i = 0; i < children.size(); ++i)
	{
		T* child = children.at(i);
		panel->addChild(child);
		float offsetY = 0;
		switch (alignment)
		{
		case UIAlign::UI_ALIGNMENT_HORIZONTAL_TOP:
			offsetY = height - child->getBoundingBox().size.height / 2;
			break;

		case UIAlign::UI_ALIGNMENT_HORIZONTAL_CENTER:
			offsetY = height / 2;
			break;

		case UIAlign::UI_ALIGNMENT_HORIZONTAL_BOTTOM:
			offsetY = child->getBoundingBox().size.height / 2;
			break;

		default:
			offsetY = height / 2;
			break;
		}

		offsetX += child->getBoundingBox().size.width / 2;
		setPos(child, Vec2(offsetX, offsetY), Vec2(0.5, 0.5));
		offsetX += child->getBoundingBox().size.width / 2;
		offsetX += padding;
	}

	return panel;
};


/**
* 创建一个 Layout, 其所有子View 垂直组合
* @param {Number} padding
* @param {Number| alignment
* @param {Array} array
* @returns {ccui.Layout}
*/
template <typename T>
static Layout* createPanelAlignVertical(const float padding, const UIAlign alignment, T* child, ...)
{
	Vector<T*> children;
	va_list args;
	va_start(args, child);
	T* iChild = child;
	while (iChild){
		children.pushBack(iChild);
		iChild = va_arg(args, T*);
	}
	va_end(args);

	return createPanelAlignVertical(padding, alignment, children);
};

/**
* 创建一个 Layout, 其所有子View 垂直组合(注：从顶部开始组合)
* @param {Number} padding
* @param {Number| alignment
* @param {Array} array
* @returns {ccui.Layout}
*/
template <typename T>
static Layout* createPanelAlignVertical(const float padding, const UIAlign alignment, Vector<T*>&  children)
{
	float width = 0;
	float height = 0;
	for (ssize_t i = 0; i < children.size(); ++i)
	{
		T* child = children.at(i);
		height += child->getBoundingBox().size.height;
		height += padding;
		width = fmax(width, child->getBoundingBox().size.width);
	}
	// 减去最后的间隔
	height -= padding;

	float offsetY = height;
	auto* panel = Layout::create();
	panel->setContentSize(Size(width, height));
	panel->setAnchorPoint(Vec2(0.5, 0.5));

	for (ssize_t i = 0; i < children.size(); ++i)
	{
		T* child = children.at(i);
		panel->addChild(child);

		float offsetX = 0;
		switch (alignment)
		{
		case UIAlign::UI_ALIGNMENT_VERTICAL_LEFT:
			offsetX = child->getBoundingBox().size.width / 2;
			break;
		case UIAlign::UI_ALIGNMENT_VERTICAL_CENTER:
			offsetX = width / 2;
			break;
		case UIAlign::UI_ALIGNMENT_VERTICAL_RIGHT:
			offsetX = width - child->getBoundingBox().size.width / 2;
			break;
		default:
			offsetX = width - child->getBoundingBox().size.width / 2;
			break;
		}
		offsetY -= child->getBoundingBox().size.height / 2;
		setPos(child, Vec2(offsetX, offsetY), Vec2(0.5, 0.5));
		offsetY -= child->getBoundingBox().size.height / 2;
		offsetY -= padding;
	}

	return panel;
};

/**
* 创建一个 Layout, 其所有子View 垂直组合(注：从底部开始组合)
*/
template <typename T>
static Layout* createPanelAlignVerticalBottom(const float padding, const UIAlign alignment, Vector<T*>& children)
{
	float width = 0;
	float height = 0;
	for (ssize_t i = 0; i < children.size(); ++i)
	{
		T* child = children.at(i);
		height += child->getBoundingBox().size.height;
		height += padding;
		width = fmax(width, child->getBoundingBox().size.width);
	}
	// 减去最后的间隔
	height -= padding;

	float offsetY = 0;
	auto* panel = Layout::create();
	panel->setContentSize(Size(width, height));
	panel->setAnchorPoint(Vec2(0.5, 0.5));

	for (ssize_t i = 0; i < children.size(); ++i)
	{
		T* child = children.at(i);
		panel->addChild(child);

		float offsetX = 0;
		switch (alignment)
		{
		case UIAlign::UI_ALIGNMENT_VERTICAL_LEFT:
			offsetX = child->getBoundingBox().size.width / 2;
			break;
		case UIAlign::UI_ALIGNMENT_VERTICAL_CENTER:
			offsetX = width / 2;
			break;
		case UIAlign::UI_ALIGNMENT_VERTICAL_RIGHT:
			offsetX = width - child->getBoundingBox().size.width / 2;
			break;
		default:
			offsetX = width - child->getBoundingBox().size.width / 2;
			break;
		}
		offsetY += child->getBoundingBox().size.height / 2;
		setPos(child, Vec2(offsetX, offsetY), Vec2(0.5, 0.5));
		offsetY += child->getBoundingBox().size.height / 2;
		offsetY += padding;
	}

	return panel;
};

/**
* 获取节点(子的虚拟锚点)在某祖先节点上的位置
@param {*|cc.Node} Node
* @param {*|cc.Node} ancestorNode
* @param {*|Vec2oint} [visualAnchorPoint]  你想要以node的那个锚点计算位置(默认结果是以node本身锚点返回坐标)
* returns {Vec2oint}
*/
template<typename T, typename TP>
static Vec2 getPosAtAncestor(T const& node, TP const& ancestorNode, const Vec2& visualAnchorPoint)
{
	//ASSERT(T->getParent())
	Size boundingBoxSize = node->getBoundingBox().size;
	Vec2 anchorPoint = node->getAnchorPoint();
	Vec2 nodePoint = Vec2(boundingBoxSize.width * (visualAnchorPoint.x - anchorPoint.x), boundingBoxSize.height * (visualAnchorPoint.y - anchorPoint.y));

	Vec2 position = ancestorNode->convertToNodeSpace(node->convertToWorldSpaceAR(nodePoint));
	return position;
};

/**
* 替换view
*/
template<typename O, typename N>
void replaceWidget(O& oldView, N& newView)
{
	if (oldView && newView && oldView->getParent())
	{
		Vec2 pos = oldView->getPosition();
		Vec2 anchorPoint = oldView->getAnchorPoint();
		if (oldView->isIgnoreAnchorPointForPosition()) anchorPoint = Vec2::ZERO;
		oldView->getParent()->addChild(newView);
		setPos(newView, pos, anchorPoint);
		oldView->removeFromParent();
	}
}

/**
* GBK->UTF（方便log）
*/
static string UTEXT(const char* gb2312)
{
// 	if (CC_TARGET_PLATFORM == CC_PLATFORM_WIN32)
// 	{
// 		int len = MultiByteToWideChar(CP_ACP, 0, gb2312, -1, NULL, 0);
// 		wchar_t* wstr = new wchar_t[len + 1];//+1 "/0"
// 		memset(wstr, 0, len + 1);
// 		MultiByteToWideChar(CP_ACP, 0, gb2312, -1, wstr, len);
// 		len = WideCharToMultiByte(CP_UTF8, 0, wstr, -1, NULL, 0, NULL, NULL);
// 		char* str = new char[len + 1];
// 		memset(str, 0, len + 1);
// 		WideCharToMultiByte(CP_UTF8, 0, wstr, -1, str, len, NULL, NULL);
// 		if (wstr) delete[] wstr;
// 		string stdStr = string(str);
// 		delete[] str;
// 		return stdStr;
// 	}
// 	else
// 	{
// 		return string(gb2312);
// 	}	
}