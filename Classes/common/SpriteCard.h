﻿//
//  SpriteCard.h
//  YYGameProject
//
//  Created by 曾 亚东 on 13-5-30.
//
//

#ifndef __YYGameProject__SpriteCard__
#define __YYGameProject__SpriteCard__

#include <iostream>
#include "cocos2d.h"
#include "common/TouchLayer.h"
#include "TouchSprite.h"
USING_NS_CC;

#define CARD_MAXCOUNT   59
typedef unsigned char byte;
typedef unsigned short word;
class CSpriteCard : public TouchSprite
{
public:
    CSpriteCard();
    ~CSpriteCard();
    
public:
    static CSpriteCard* spriteCard(byte cbData);
    static CSpriteCard* spriteCardWithFile(char* file);
    static CSpriteCard* spriteCardWithFile(char* file, CCRect rect);
    static CSpriteCard* spriteCardWithTexture(CCTexture2D* pTex, CCRect rect);
    static CSpriteCard* spriteCardWithCopy(CSpriteCard* pCard);
	static void setAutoScaleSize(float as);
public:
    void initValue();
    
public:
    void onEnter();
    void onExit();
    
public:
	void setTouchProiorty(int nProiorty, bool bSwallows=true);    
	virtual bool onTouchBegan(Touch *pTouch, Event *pEvent);
	virtual void onTouchMoved(Touch *pTouch, Event *pEvent);
	virtual void onTouchEnded(Touch *pTouch, Event *pEvent);

public:
    void setCardTexture(CSpriteCard* pCard);
    
public:
	void setChairID(word wChairID) {m_wChairID=wChairID;}
    void setViewID(word wViewID) {m_wViewID=wViewID;}
    void setCardData(byte cbData) {m_cbData=cbData;}
    void setCardColor(byte cbColor) {m_cbColor=cbColor;}
    void setCardValue(byte cbValue) {m_cbValue=cbValue;}
    void resetStatus() {setStatus(0); setMark(0); setSelect(false); setColor(ccWHITE);};
    
	word getChairID()	{return m_wChairID;}
    word getViewID()	{return m_wViewID;}
    byte getCardData()  {return m_cbData;}
    byte getCardColor() {return m_cbColor;}
    byte getCardValue() {return m_cbValue;}
    float getCardSpace(){return g_fCardSpace;}
    
    void setStatus(byte cbStatus)   {m_cbStatus=cbStatus;}
    void setMark(byte cbMark)       {m_cbMark=cbMark;}
    void setSelect(bool bSelect)    {m_bSelect=bSelect;}
    
    byte getStatus(){return m_cbStatus;}
    byte getMark()  {return m_cbMark;}
    word getIndex() {return m_wIndex;}
    bool getSelect(){return m_bSelect;}
    
public:
    //扑克数据
    byte            m_cbData;               //扑克数据
    byte            m_cbValue;              //扑克牌值
    byte            m_cbColor;              //扑克花色
    char            m_pCardName[20];        //扑克名字
    word            m_wViewID;              //所属玩家
	word			m_wChairID;				//所属位置
    
public:
    //操作变量
    byte            m_cbStatus;             //扑克状态
    byte            m_cbMark;               //扑克标签
    bool            m_bSelect;              //扑克选中
    
private:
    word            m_wIndex;               //扑克索引

public:
    bool            m_bTouch;               //touch支持
	bool            m_bSwallows;            //是否拦截

public:
    CCPoint         m_PointPre;             //上个坐标
    
protected:
	static float			mCardScale;
    
public:
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //扑克字典
    static Vector<CSpriteCard*>      g_pCardArray;
    
    //扑克操作数据
    static byte             g_cbCardData[CARD_MAXCOUNT];
    static byte             g_cbMaskValue;                  //牌值掩码
    static byte             g_cbMaskColor;                  //花色掩码
    static byte             g_cbCardBack;                   //背景牌值
    static float            g_fCardSpace;                   //扑克间隔

    //扑克数据
    static byte GetCardValue(byte cbCardData) { return cbCardData&g_cbMaskValue; }
	static byte GetCardColor(byte cbCardData) { return cbCardData&g_cbMaskColor; }
    
    //扑克游戏使用
    //设置数组扑克
    static void setCardArrayWithFile(char* file, float fWidth, float fHeight);
    static void setCardArrayWithRow(char* file, const byte cbCardData[], word wCount, word wCol=5, word wRow=13);
    static void setCardArrayWithData(char* file, byte cbCardData, word wCol=5, word wRow=13);
    static void setCardArrayWithTexture(CCTexture2D* pTex, const byte cbCardData[], word wCount, float fWidth, float fHeight);
    static void setCardWithData(CSpriteCard* pCard, byte cbData);
    static void setCardWithData(CSpriteCard* pCard);
    
    //获得数组扑克
    static CSpriteCard* getCardWithData(byte cbData);
	static CSpriteCard* getCardWithTag(word wTag);
    static CSpriteCard* getCardCopyWithData(byte cbData);
    static CSpriteCard* getCardWithIndex(word wIndex);
    static CSpriteCard* getCardCopyWithIndex(word wIndex);
    static CSpriteCard* getCardWithBack();
    
};
#endif /* defined(__YYGameProject__SpriteCard__) */
