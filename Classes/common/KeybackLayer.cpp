#include "KeybackLayer.h"

//////////////////////////////////////////////////////////////////////////
KeybackLayer* __gKeybackLayer = 0;

KeybackLayer* KeybackLayer::getInstance()
{
	return __gKeybackLayer;
}

//////////////////////////////////////////////////////////////////////////
KeybackLayer::KeybackLayer()
{

}

KeybackLayer::~KeybackLayer()
{
	if (__gKeybackLayer == this)
		__gKeybackLayer = 0;
}

//初始化方法
bool KeybackLayer::init()
{
	do 
	{
		CC_BREAK_IF(!Layer::init());
		__gKeybackLayer = this;

		auto listener = EventListenerKeyboard::create();
		listener->onKeyReleased = CC_CALLBACK_2(Layer::onKeyReleased, this);

		_eventDispatcher->addEventListenerWithSceneGraphPriority(listener, this);
		return true;
	} while (0);

	return false;
}


void KeybackLayer::onKeyReleased(EventKeyboard::KeyCode keyCode, Event* event)
{
	if (keyCode == EventKeyboard::KeyCode::KEY_ESCAPE)
	{
		if (!mListeners.empty())
		{
			mListeners.back()->onKeybackClicked();
		}
	}
}

void KeybackLayer::addKeybackListener(IKeybackListener* listener)
{
	removeKeybackListener(listener);
	mListeners.push_back(listener);
}

void KeybackLayer::removeKeybackListener(IKeybackListener* listener)
{
	auto it = find(mListeners.begin(), mListeners.end(), listener);
	if (it != mListeners.end())
	{
		mListeners.erase(it);
	}
}
