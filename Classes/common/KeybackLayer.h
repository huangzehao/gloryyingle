#ifndef _KeybackLayer_H_
#define _KeybackLayer_H_

#include "cocos2d.h"
USING_NS_CC;

#include <list>
#include "IKeybackListener.h"

class KeybackLayer : public Layer
{
public:
	//创建方法
	CREATE_FUNC(KeybackLayer);

	static KeybackLayer* getInstance();
public:
	//构造函数
	KeybackLayer();
	~KeybackLayer();
	//初始化方法
	virtual bool init() override;
	void onKeyReleased(EventKeyboard::KeyCode keyCode, Event* event);

	void addKeybackListener(IKeybackListener* listener);
	void removeKeybackListener(IKeybackListener* listener);
private:
	std::list<IKeybackListener*>	mListeners;
};
#endif // _KeybackLayer_H_