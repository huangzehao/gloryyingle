#ifndef _TouchLayer_H_
#define _TouchLayer_H_

#include "cocos2d.h"

class ITouchSink
{
public:
	virtual ~ITouchSink(){};
	virtual bool onTouchBegan(cocos2d::Touch *pTouch, cocos2d::Event *pEvent)=0;
	virtual void onTouchMoved(cocos2d::Touch *pTouch, cocos2d::Event *pEvent)=0;
	virtual void onTouchEnded(cocos2d::Touch *pTouch, cocos2d::Event *pEvent)=0;
};

class TouchLayer
	: public cocos2d::Layer
{
public:
	CREATE_FUNC(TouchLayer);

public:
	TouchLayer();
	~TouchLayer();
	bool init();
	void setSink(ITouchSink* pITouchSink);
	//void setTouchEnabled(bool flag);

	virtual bool onTouchBegan(cocos2d::Touch *pTouch, cocos2d::Event *pEvent);
	virtual void onTouchMoved(cocos2d::Touch *pTouch, cocos2d::Event *pEvent);
	virtual void onTouchEnded(cocos2d::Touch *pTouch, cocos2d::Event *pEvent);


private:
	bool		mIsTouchEnabled;
	ITouchSink*	mITouchSink;
};

#endif // _TouchLayer_H_