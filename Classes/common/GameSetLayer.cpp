#include "GameSetLayer.h"
#include "Tools/Manager/SoundManager.h"


GameSetLayer::GameSetLayer()
{
}


GameSetLayer::~GameSetLayer()
{
}

bool GameSetLayer::init()
{
	if (!BaseLayer::initWithJsonFile("cocos_ui/NewUI/GameSetLayer.json"))
	{
		return false;
	}

	Slider_Music = dynamic_cast<Slider *>(m_root_widget->getChildByName("Slider_Music"));
	Slider_Music->addEventListenerSlider(this, SEL_SlidPercentChangedEvent(&GameSetLayer::sliderMusicTouch));
	mMusic = (int)SoundManager::shared()->getMusicVolume();
	Slider_Music->setPercent(mMusic);

	Slider_Sound = dynamic_cast<Slider *>(m_root_widget->getChildByName("Slider_Sound"));
	Slider_Sound->addEventListenerSlider(this, SEL_SlidPercentChangedEvent(&GameSetLayer::sliderSoundTouch));
	mSound = (int)SoundManager::shared()->getSoundVolume();
	Slider_Sound->setPercent(mSound);

	Button * btn_music_add = dynamic_cast<Button *>(m_root_widget->getChildByName("btn_music_add"));
	btn_music_add->addTouchEventListener(this, SEL_TouchEvent(&GameSetLayer::btnMusicAddTouch));

	Button * btn_music_sub = dynamic_cast<Button *>(m_root_widget->getChildByName("btn_music_sub"));
	btn_music_sub->addTouchEventListener(this, SEL_TouchEvent(&GameSetLayer::btnMusicSubTouch));

	Button * btn_sound_add = dynamic_cast<Button *>(m_root_widget->getChildByName("btn_sound_add"));
	btn_sound_add->addTouchEventListener(this, SEL_TouchEvent(&GameSetLayer::btnSoundAddTouch));

	Button * btn_sound_sub = dynamic_cast<Button *>(m_root_widget->getChildByName("btn_sound_sub"));
	btn_sound_sub->addTouchEventListener(this, SEL_TouchEvent(&GameSetLayer::btnSoundSubTouch));

	Button * btn_close = dynamic_cast<Button *>(m_root_widget->getChildByName("btn_close"));
	btn_close->addTouchEventListener([=](Ref * ref, Widget::TouchEventType eType)
	{
		if (eType == Widget::TouchEventType::ENDED)
		{
			this->removeFromParent();
		}
	});
 	return true;
}

void GameSetLayer::btnMusicAddTouch(Ref * ref, TouchEventType eType)
{
	if (eType == TouchEventType::TOUCH_EVENT_ENDED)
	{
		addMusicPrecent();
	}
}

void GameSetLayer::btnMusicSubTouch(Ref * ref, TouchEventType eType)
{
	if (eType == TouchEventType::TOUCH_EVENT_ENDED)
	{
		subMusicPrecent();
	}
}

void GameSetLayer::btnSoundAddTouch(Ref * ref, TouchEventType eType)
{
	if (eType == TouchEventType::TOUCH_EVENT_ENDED)
	{
		addSoundPrecent();
	}
}

void GameSetLayer::btnSoundSubTouch(Ref * ref, TouchEventType eType)
{
	if (eType == TouchEventType::TOUCH_EVENT_ENDED)
	{
		subSoundPrecent();
	}
}

void GameSetLayer::sliderMusicTouch(Ref* ref, SliderEventType eType)
{
	if (eType == SliderEventType::SLIDER_PERCENTCHANGED)
	{
		int percent = Slider_Music->getPercent();
		mMusic = percent;
		SoundManager::shared()->setMusicVolume(mMusic);
	}
}

void GameSetLayer::sliderSoundTouch(Ref* ref, SliderEventType eType)
{
	if (eType == SliderEventType::SLIDER_PERCENTCHANGED)
	{
		int percent = Slider_Sound->getPercent();
		mSound = percent;
		SoundManager::shared()->setSoundVolume(mSound);
	}
}

void GameSetLayer::addMusicPrecent()
{
	if (mMusic + 1 <= 100)
	{
		mMusic++;
		SoundManager::shared()->setMusicVolume(mMusic);
	}
}

void GameSetLayer::addSoundPrecent()
{
	if (mSound + 1 <= 100)
	{
		mSound++;
		SoundManager::shared()->setSoundVolume(mSound);
	}
}

void GameSetLayer::subMusicPrecent()
{
	if (mMusic - 1 >= 0)
	{
		mMusic--;
		SoundManager::shared()->setMusicVolume(mMusic);
	}
}

void GameSetLayer::subSoundPrecent()
{
	if (mSound - 1 >= 0)
	{
		mSound--;
		SoundManager::shared()->setSoundVolume(mSound);
	}
}
