#ifndef GAMESETLAYER_H_
#define GAMESETLAYER_H_

#include "Platform/PFView/BaseLayer.h"
#include "cocos2d.h"
#include "ui/CocosGUI.h"

USING_NS_CC;
using namespace ui;

class GameSetLayer : public BaseLayer
{
public:
	GameSetLayer();
	~GameSetLayer();

	CREATE_FUNC(GameSetLayer);

	virtual bool init();
protected:
	Slider * Slider_Music;
	Slider * Slider_Sound;

private:
	void btnMusicAddTouch(Ref * ref, TouchEventType eType);
	void btnMusicSubTouch(Ref * ref, TouchEventType eType);
	void btnSoundAddTouch(Ref * ref, TouchEventType eType);
	void btnSoundSubTouch(Ref * ref, TouchEventType eType);

	void sliderMusicTouch(Ref* ref, SliderEventType eType);
	void sliderSoundTouch(Ref* ref, SliderEventType eType);

	void addMusicPrecent();
	void addSoundPrecent();
	void subMusicPrecent();
	void subSoundPrecent();

	int mSound;
	int mMusic;
};


#endif

