#include "TouchLayer.h"

USING_NS_CC;

//////////////////////////////////////////////////////////////////////////
TouchLayer::TouchLayer()
	: mIsTouchEnabled(false)
	, mITouchSink(0)
{
}

TouchLayer::~TouchLayer()
{
}

//初始化方法
bool TouchLayer::init()
{
	do 
	{
		CC_BREAK_IF(!CCLayer::init());
		return true;
	} while (0);

	return false;
}

// void TouchLayer::setTouchEnabled(bool flag)
// {
// 	if (mIsTouchEnabled != flag){
// 		mIsTouchEnabled = flag;
// 		if(flag){
// 			CCDirector::getInstance()->getTouchDispatcher()->addTargetedDelegate(this, 1, true);
// 		}else{
// 			CCDirector::getInstance()->getTouchDispatcher()->removeDelegate(this);
// 		}
// 	}
// }

void TouchLayer::setSink(ITouchSink* pITouchSink)
{
	mITouchSink = pITouchSink;
}

bool TouchLayer::onTouchBegan(CCTouch* touch, CCEvent* event)
{
	return (mITouchSink && mITouchSink->onTouchBegan(touch, event));
}

void TouchLayer::onTouchMoved(CCTouch* touch, CCEvent* event)
{
	if (mITouchSink)
		mITouchSink->onTouchMoved(touch, event);
}

void TouchLayer::onTouchEnded(CCTouch* touch, CCEvent* event)
{
	if (mITouchSink)
		mITouchSink->onTouchEnded(touch, event);
}

