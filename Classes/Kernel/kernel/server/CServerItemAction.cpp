#include "Kernel/kernel/server/CServerItem.h"
#include "Tools/tools/Convert.h"
#include "Tools/Dialog/NewDialog.h"
#include "Tools/tools/StringData.h"

//查找桌子
bool CServerItem::SearchGameTable(tagSearchTable & SearchTable)
{
	//变量定义
	word wNullCount=0;
	word wChairUser=mTableViewFrame.GetChairCount();
	word wMeTableID=mMeUserItem->GetTableID();

	//百人游戏
	if (CServerRule::IsAllowDynamicJoin(mServerAttribute.dwServerRule)==true)
	{
		if(mTableViewFrame.GetChairCount() >= MAX_CHAIR)
		{
			SearchTable.wResultTableID=0;
			SearchTable.wResultChairID=MAX_CHAIR;
			return true;
		}
	}

	//搜索桌子
	for (word i=0;i<mTableViewFrame.GetTableCount();i++)
	{
		//设置索引
		SearchTable.wResultTableID=(SearchTable.wStartTableID+i)%mTableViewFrame.GetTableCount();

		//搜索处理
		if (wMeTableID!=SearchTable.wResultTableID)
		{
			//获取桌子
			ITableView * pITableView=mTableViewFrame.GetTableViewItem(SearchTable.wResultTableID);

			//搜索过虑
			if (pITableView->GetPlayFlag()==true) continue;
			if ((SearchTable.bFilterPass==true)&&(pITableView->GetLockerFlag()==true)) continue;

			//寻找空位置
			wNullCount=pITableView->GetNullChairCount(SearchTable.wResultChairID);

			//判断数目
			if (wNullCount>0)
			{
				//效验规则
				char strDescribe[256];
				if (EfficacyTableRule(SearchTable.wResultTableID,SearchTable.wResultChairID,false,strDescribe)==false)
				{
					continue;
				}

				//数目判断
				if ((SearchTable.bOneNull==true)&&(wNullCount==1)) return true;
				if ((SearchTable.bTwoNull==true)&&(wNullCount==2)) return true;
				if ((SearchTable.bNotFull==true)&&(wNullCount<wChairUser)) return true;
				if ((SearchTable.bAllNull==true)&&(wNullCount==wChairUser)) return true;
			}
		}
	}

	//设置数据
	SearchTable.wResultTableID=INVALID_TABLE;
	SearchTable.wResultChairID=INVALID_CHAIR;

	return false;
}

//快速加入
bool CServerItem::PerformQuickSitDown()
{
	mIsQuickSitDown=true;
	return SendSocketData(MDM_GR_USER, SUB_GR_USER_CHAIR_REQ);
} 

bool CServerItem::PerformAutoJoin()
{
	//变量定义
	tagSearchTable SearchTable;
	memset(&SearchTable, 0, sizeof(SearchTable));

	//搜索条件
	SearchTable.bNotFull=true;
	SearchTable.bOneNull=true;
	SearchTable.bTwoNull=(mTableViewFrame.GetChairCount()!=2);
	SearchTable.bAllNull=false;
	SearchTable.bFilterPass=true;
	
	//搜索结果
	SearchTable.wResultTableID=INVALID_TABLE;
	SearchTable.wResultChairID=INVALID_CHAIR;
	SearchTable.wStartTableID=0;

	//重入判断
	word meTableID = mMeUserItem->GetTableID();
	word meChairID = mMeUserItem->GetChairID();
	if (meTableID == INVALID_TABLE && meChairID == INVALID_CHAIR)
	{
		//搜索桌子
		SearchGameTable(SearchTable);
	}
	else
	{
		SearchTable.wResultTableID = meTableID;
		SearchTable.wResultChairID = meChairID;
	}
	mFindTableID = SearchTable.wResultTableID;

	//搜索桌子
	if (mFindTableID==INVALID_CHAIR)
	{
		//搜索条件
		SearchTable.bAllNull=true;
		SearchTable.bNotFull=true;
		SearchTable.bOneNull=true;
		SearchTable.bFilterPass=true;
		SearchTable.bTwoNull=(mTableViewFrame.GetChairCount()!=2);

		//搜索结果
		SearchTable.wResultTableID=INVALID_TABLE;
		SearchTable.wResultChairID=INVALID_CHAIR;
		SearchTable.wStartTableID=mFindTableID+1;

		//搜索桌子
		SearchGameTable(SearchTable);
		mFindTableID=SearchTable.wResultTableID;
	}

	//结果判断
	if (mFindTableID!=INVALID_CHAIR)
	{
		//效验数据
		ASSERT(SearchTable.wResultTableID!=INVALID_TABLE);
		ASSERT(SearchTable.wResultChairID!=INVALID_CHAIR);

		//设置数据
		word wChairID=SearchTable.wResultChairID;
		mTableViewFrame.VisibleTable(mFindTableID);
		mTableViewFrame.FlashGameChair(mFindTableID,wChairID);

		//自动坐下
		CParameterGlobal * pParameterGlobal=CParameterGlobal::shared();
		if (pParameterGlobal->m_bAutoSitDown==true) 
			PerformSitDownAction(mFindTableID,wChairID,true);
	}
	else
	{
		//提示消息
		if (mIStringMessageSink)
			mIStringMessageSink->InsertPromptString("抱歉，现在暂时没有可以加入的游戏桌，请稍后再次尝试！", DLG_MB_OK);
	}

	return false;
}


//执行旁观
bool CServerItem::PerformLookonAction(word wTableID, word wChairID)
{
	PLAZZ_PRINTF("CServerItem::PerformLookonAction...\n");

	if (!IsService())
		return false;

	//效验数据
	ASSERT(wTableID!=INVALID_TABLE);
	ASSERT(wChairID!=INVALID_CHAIR);

	//状态过滤
	if (mServiceStatus!=ServiceStatus_ServiceIng) return false;
	if ((mReqTableID==wTableID)&&(mReqChairID==wChairID)) return false;

	//自己状态
	if (mMeUserItem->GetUserStatus()>=US_PLAYING)
	{
		//提示消息
		if (mIStringMessageSink)
			mIStringMessageSink->InsertPromptString(a_u8("您正在游戏中，暂时不能离开，请先结束当前游戏！"), DLG_MB_OK);
		return false;
	}

	//权限判断
	if (CUserRight::CanLookon(mUserAttribute.dwUserRight)==false)
	{
		//提示消息
		if (mIStringMessageSink)
			mIStringMessageSink->InsertPromptString(a_u8("抱歉，您暂时没有旁观游戏的权限！"), DLG_MB_OK);
		return false;
	}

	//清理界面
	if ((mReqTableID!=INVALID_TABLE)&&(mReqChairID!=INVALID_CHAIR)&&(mReqChairID!=MAX_CHAIR))
	{
		IClientUserItem * pIClientUserItem=mTableViewFrame.GetClientUserItem(mReqTableID,mReqChairID);
		if (pIClientUserItem==mMeUserItem) mTableViewFrame.SetClientUserItem(mReqTableID,mReqChairID,0);
	}

	//设置变量
	mReqTableID=wTableID;
	mReqChairID=wChairID;
	mFindTableID=INVALID_TABLE;

	//设置界面
	mTableViewFrame.VisibleTable(wTableID);

	PLAZZ_PRINTF("CServerItem::PerformLookonAction send\n");
	//发送命令
	SendLookonPacket(wTableID,wChairID);

	return true;
}

//执行起立
bool CServerItem::PerformStandUpAction(word wTableID, word wChairID)
{
	PLAZZ_PRINTF("CServerItem::PerformStandUpAction...\n");

	if (!IsService())
		return false;

	//效验数据
	ASSERT(wTableID!=INVALID_TABLE);
	ASSERT(wChairID!=INVALID_CHAIR);

	//状态过滤
	if (mServiceStatus!=ServiceStatus_ServiceIng) return false;
	if ((mReqTableID==wTableID)&&(mReqChairID==wChairID)) return false;

	//自己状态
	if (mMeUserItem->GetUserStatus()>=US_PLAYING)
	{
		//提示消息
		if (mIStringMessageSink)
			mIStringMessageSink->InsertPromptString(a_u8("您正在游戏中，暂时不能离开，请先结束当前游戏！"), DLG_MB_OK);

		return false;
	}

	//设置变量
	mReqTableID=INVALID_TABLE;
	mReqChairID=INVALID_CHAIR;
	mFindTableID=INVALID_TABLE;

	//设置界面
	mTableViewFrame.VisibleTable(wTableID);

	PLAZZ_PRINTF("CServerItem::PerformStandUpAction send\n");
	//发送命令
	SendStandUpPacket(wTableID,wChairID,FALSE);

	return true;
}

//执行坐下
bool CServerItem::PerformSitDownAction(word wTableID, word wChairID, bool bEfficacyPass, const char * psw)
{
	PLAZZ_PRINTF("CServerItem::PerformSitDownAction...\n");

	if (!IsService())
		return false;

	//状态过滤
	if (mServiceStatus!=ServiceStatus_ServiceIng) return false;
	if ((mReqTableID!=INVALID_TABLE)&&(mReqTableID==wTableID)) return false;
	if ((mReqChairID!=INVALID_CHAIR)&&(mReqChairID==wChairID)) return false;

	//自己状态
	if (mMeUserItem->GetUserStatus()>=US_PLAYING)
	{
		//提示消息
		if (wTableID != mMeUserItem->GetTableID() && wChairID != mMeUserItem->GetChairID())
		{
			if (mIStringMessageSink)
				mIStringMessageSink->InsertPromptString(a_u8("您正在游戏中，暂时不能离开，请先结束当前游戏！"), DLG_MB_OK);
			return false;
		}
	}

	//权限判断
	if (CUserRight::CanPlay(mUserAttribute.dwUserRight)==false)
	{
		//提示消息
		if (mIStringMessageSink)
			mIStringMessageSink->InsertPromptString(a_u8("抱歉，您暂时没有加入游戏的权限！"), DLG_MB_OK);

		return false;
	}

	//桌子效验
	if ((wTableID!=INVALID_TABLE)&&(wChairID!=INVALID_CHAIR))
	{
		char strDescribe[256]={0};
		if ((wChairID!=MAX_CHAIR)&&(EfficacyTableRule(wTableID,wChairID,false,strDescribe)==false))
		{
			NewDialog::create(SSTRING("System_Tips_35"), NewDialog::NONEBUTTON);

			return false;
		}
	}

	//密码判断
	char szPassword[LEN_PASSWORD] = {0};
	if ((mMeUserItem->GetMasterOrder()==0)&&(bEfficacyPass==true)&&(wTableID!=INVALID_TABLE)&&(wChairID!=INVALID_CHAIR)&&(wChairID!=MAX_CHAIR))
	{
		//变量定义
		bool bLocker=mTableViewFrame.GetLockerFlag(wTableID);

		memcpy(szPassword, psw, strlen(psw));
		//密码判断
		/*if(bLocker)
		{
			////设置密码
			//CDlgTablePassword DlgTablePassword;
			//DlgTablePassword.SetPromptString(T_T("该桌已设置进入密码，请输入密码。"));
			//if (DlgTablePassword.DoModal()!=IDOK) return false;
			////设置密码
			//tstrcpyn(szPassword,DlgTablePassword.m_szPassword,CountArray(szPassword));

			//PLAZZ_PRINTF(a_u8("//TODO:该桌已设置进入密码，请输入密码。 未实现!!!!\n"));
			return false;
		}*/
	}

	//清理界面
	if ((mReqTableID!=INVALID_TABLE)&&(mReqChairID!=INVALID_CHAIR)&&(mReqChairID!=MAX_CHAIR))
	{
		IClientUserItem * pIClientUserItem=mTableViewFrame.GetClientUserItem(mReqTableID,mReqChairID);
		if (pIClientUserItem==mMeUserItem) mTableViewFrame.SetClientUserItem(mReqTableID,mReqChairID,0);
	}

	//设置界面
	if ((wChairID!=MAX_CHAIR)&&(wTableID!=INVALID_TABLE)&&(wChairID!=INVALID_CHAIR))
	{
		mTableViewFrame.VisibleTable(wTableID);
		mTableViewFrame.SetClientUserItem(wTableID,wChairID,mMeUserItem);
	}

	//设置变量
	mReqTableID=wTableID;
	mReqChairID=wChairID;
	mFindTableID=INVALID_TABLE;

// 	wTableID = 10;
// 	wChairID = 5;

	PLAZZ_PRINTF("CServerItem::PerformSitDownAction send... wTableID =%d  wCharID =%d \n,", wTableID, wChairID);
	//发送命令
	SendSitDownPacket(wTableID,wChairID,szPassword);

	return true;
}

//执行购买
bool CServerItem::PerformBuyProperty(byte cbRequestArea,const char* pszNickName, word wItemCount, word wPropertyIndex)
{
	PLAZZ_PRINTF("CServerItem::PerformBuyProperty...\n");

	if (!IsService())
		return false;

	//PLAZZ_PRINTF(a_u8("//TODO:执行购买。 未实现!!!!\n"));
	return true;

	////变量定义
	//ASSERT(CGamePropertyManager::GetInstance()!=0);
	//CGamePropertyManager * pGamePropertyManager=CGamePropertyManager::GetInstance();

	////查找道具
	//ASSERT(pGamePropertyManager->GetPropertyItem(wPropertyIndex)!=0);
	//CGamePropertyItem * pGamePropertyItem=pGamePropertyManager->GetPropertyItem(wPropertyIndex);
	//if(pGamePropertyItem==0) return false;

	////查找用户
	//IClientUserItem * pIClientUserItem=m_PlazaUserManagerModule->SearchUserByNickName(pszNickName);

	////用户判断
	//if (pIClientUserItem==0)
	//{
	//	return false;
	//}

	////变量定义
	//CMD_GR_C_PropertyBuy PropertyBuy;
	//zeromemory(&PropertyBuy,sizeof(PropertyBuy));

	////设置变量
	//PropertyBuy.cbRequestArea=cbRequestArea;
	//PropertyBuy.wItemCount=wItemCount;
	//PropertyBuy.wPropertyIndex=wPropertyIndex;
	//PropertyBuy.dwTargetUserID=pIClientUserItem->GetUserID();
	//PropertyBuy.cbConsumeScore=FALSE;

	////发送消息
	//m_TCPSocketModule->SendData(MDM_GR_USER,SUB_GR_PROPERTY_BUY,&PropertyBuy,sizeof(PropertyBuy));

	//return true;
}
