#include "Kernel/kernel/server/CServerItem.h"
#include "Tools/tools/PacketAide.h"
#include "Kernel/kernel/game/IClientKernel.h"

//////////////////////////////////////////////////////////////////////////
//用户接口

//自己位置
word CServerItem::GetMeChairID()
{
	if (mMeUserItem == 0) return INVALID_CHAIR;
	return mMeUserItem->GetChairID();
}

//自己位置
IClientUserItem * CServerItem::GetMeUserItem()
{
	return mMeUserItem;
}

//游戏用户
IClientUserItem * CServerItem::GetTableUserItem(word wChairID)
{
	return mUserManager->EnumUserItem(wChairID);
}

//查找用户
IClientUserItem * CServerItem::SearchUserByUserID(dword dwUserID)
{
	return mUserManager->SearchUserByUserID(dwUserID);
}

//查找用户
IClientUserItem * CServerItem::SearchUserByGameID(dword dwGameID)
{
	return mUserManager->SearchUserByGameID(dwGameID);
}

//查找用户
IClientUserItem * CServerItem::SearchUserByNickName(const char* szNickName)
{
	return mUserManager->SearchUserByNickName(szNickName);
}
///< 获取用户数
dword CServerItem::GetActiveUserCount()
{
	return mUserManager->GetActiveUserCount();
}

//获取对应桌子是否锁的状态
bool CServerItem::GetTableLockState(int tableId)
{
	return mTableViewFrame.GetLockerFlag(tableId);
}

//获取对应桌子的游戏状态
bool CServerItem::GetTableGameState(int tableId)
{
	return mTableViewFrame.GetPlayFlag(tableId);
}

//////////////////////////////////////////////////////////////////////////
// IUserManagerSink
//////////////////////////////////////////////////////////////////////////
void CServerItem::OnUserItemAcitve(IClientUserItem* pIClientUserItem)
{
	//判断自己
	if (mMeUserItem==0)
	{
		mMeUserItem=pIClientUserItem;
	}

	////插入列表
	//m_UserListControl.InsertDataItem(pIClientUserItem);

	//设置桌子
	byte cbUserStatus=pIClientUserItem->GetUserStatus();
	if ((cbUserStatus>=US_SIT)&&(cbUserStatus!=US_LOOKON))
	{
		word wTableID=pIClientUserItem->GetTableID();
		word wChairID=pIClientUserItem->GetChairID();
		mTableViewFrame.SetClientUserItem(wTableID,wChairID,pIClientUserItem);
	}

	//提示信息
	CParameterGlobal * pParameterGlobal=CParameterGlobal::shared();
	if ((pParameterGlobal->m_bNotifyUserInOut==true)&&(mServiceStatus==ServiceStatus_ServiceIng))
	{
		if (mIStringMessageSink)
			mIStringMessageSink->InsertUserEnter(pIClientUserItem->GetNickName());
	}

 	if (mIServerItemSink)
 		mIServerItemSink->OnGRUserEnter(pIClientUserItem);

	// add by lesten  --2015/5/18--
//  	if (IClientKernel::get())
//  		IClientKernel::get()->OnGFUserEnter(pIClientUserItem);
}

void CServerItem::OnUserItemDelete(IClientUserItem* pIClientUserItem)
{
	//变量定义
	word wLastTableID=pIClientUserItem->GetTableID();
	word wLastChairID=pIClientUserItem->GetChairID();
	dword dwLeaveUserID=pIClientUserItem->GetUserID();
	tagUserInfo * pMeUserInfo=mMeUserItem->GetUserInfo();

	//变量定义
	ASSERT(CParameterGlobal::shared());
	CParameterGlobal * pParameterGlobal=CParameterGlobal::shared();

	//离开处理
	if ((wLastTableID!=INVALID_TABLE)&&(wLastChairID!=INVALID_CHAIR))
	{
		//获取用户
		IClientUserItem * pITableUserItem=mTableViewFrame.GetClientUserItem(wLastTableID,wLastChairID);
		if (pITableUserItem==pIClientUserItem) mTableViewFrame.SetClientUserItem(wLastTableID,wLastChairID,0);

		//离开通知
		if ((pIClientUserItem==mMeUserItem)||(wLastTableID==pMeUserInfo->wTableID))
		{
			tagUserStatus UserStatus;
			UserStatus.cbUserStatus=US_NULL;
			UserStatus.wTableID=INVALID_TABLE;
			UserStatus.wChairID=INVALID_CHAIR;

			if (IClientKernel::get())
				IClientKernel::get()->OnGFUserStatus(pIClientUserItem->GetUserID(), UserStatus);
		}
	}

	////聊天对象
	//m_ChatControl.DeleteUserItem(pIClientUserItem);

	////删除列表
	//m_UserListControl.DeleteDataItem(pIClientUserItem);

	////查找窗口
	//CDlgSearchUser * pDlgSearchUser=CDlgSearchUser::GetInstance();
	//if (pDlgSearchUser!=0) pDlgSearchUser->OnUserItemDelete(pIClientUserItem,this);

	////私聊窗口
	//for (INT_PTR i=0;i<m_DlgWhisperItemArray.GetCount();i++)
	//{
	//	CDlgWhisper * pDlgWhisper=m_DlgWhisperItemArray[i];
	//	if (pDlgWhisper->m_hWnd!=0) pDlgWhisper->OnEventUserLeave(pIClientUserItem);
	//}

	////菜单对象
	//for (INT_PTR i=0;i<m_MenuUserItemArray.GetCount();i++)
	//{
	//	//获取用户
	//	IClientUserItem * pIChatUserItem=m_MenuUserItemArray[i];
	//	if (pIChatUserItem->GetUserID()==dwLeaveUserID) m_MenuUserItemArray.RemoveAt(i);
	//}

	if (mIServerItemSink)
		mIServerItemSink->OnGRUserDelete(pIClientUserItem);

	//提示信息
	if ((pParameterGlobal->m_bNotifyUserInOut==true)&&(mServiceStatus==ServiceStatus_ServiceIng))
	{
		if (mIStringMessageSink)
			mIStringMessageSink->InsertUserLeave(pIClientUserItem->GetNickName());
	}

}

void CServerItem::OnUserFaceUpdate(IClientUserItem * pIClientUserItem)
{
	//变量定义
	tagUserInfo * pUserInfo=pIClientUserItem->GetUserInfo();
	tagCustomFaceInfo * pCustomFaceInfo=pIClientUserItem->GetCustomFaceInfo();

	////列表更新
	//m_UserListControl.UpdateDataItem(pIClientUserItem);

	////查找窗口
	//CDlgSearchUser * pDlgSearchUser=CDlgSearchUser::GetInstance();
	//if (pDlgSearchUser!=0) pDlgSearchUser->OnUserItemUpdate(pIClientUserItem,this);

	////私聊窗口
	//for (INT_PTR i=0;i<m_DlgWhisperItemArray.GetCount();i++)
	//{
	//	CDlgWhisper * pDlgWhisper=m_DlgWhisperItemArray[i];
	//	if (pDlgWhisper->m_hWnd!=0) pDlgWhisper->OnEventUserStatus(pIClientUserItem);
	//}

	if (mIServerItemSink)
		mIServerItemSink->OnGRUserUpdate(pIClientUserItem);

	//更新桌子
	if ((pUserInfo->wTableID!=INVALID_TABLE)&&(pUserInfo->cbUserStatus!=US_LOOKON))
	{
		mTableViewFrame.UpdateTableView(pUserInfo->wTableID);
	}

	//更新游戏
	if ((pUserInfo->wTableID!=INVALID_TABLE)&&(mMeUserItem->GetTableID()==pUserInfo->wTableID))
	{
		if (IClientKernel::get())
			IClientKernel::get()->OnGFUserCustomFace(pUserInfo->dwUserID,pUserInfo->dwCustomID,*pCustomFaceInfo);
	}
}

void CServerItem::OnUserItemUpdate(IClientUserItem* pIClientUserItem, const tagUserScore& LastScore)
{
	//变量定义
	tagUserInfo * pUserInfo=pIClientUserItem->GetUserInfo();
	tagUserInfo * pMeUserInfo=mMeUserItem->GetUserInfo();

	////列表更新
	//m_UserListControl.UpdateDataItem(pIClientUserItem);

	////查找窗口
	//CDlgSearchUser * pDlgSearchUser=CDlgSearchUser::GetInstance();
	//if (pDlgSearchUser!=0) pDlgSearchUser->OnUserItemUpdate(pIClientUserItem,this);

	////私聊窗口
	//for (INT_PTR i=0;i<m_DlgWhisperItemArray.GetCount();i++)
	//{
	//	CDlgWhisper * pDlgWhisper=m_DlgWhisperItemArray[i];
	//	if (pDlgWhisper->m_hWnd!=0) pDlgWhisper->OnEventUserStatus(pIClientUserItem);
	//}
		
	//房间界面通知
	if (pIClientUserItem==mMeUserItem)
	{
		//变量定义
		CGlobalUserInfo * pGlobalUserInfo=CGlobalUserInfo::GetInstance();
		tagGlobalUserData * pGlobalUserData=pGlobalUserInfo->GetGlobalUserData();
		tagUserInsureInfo * pUserInsureData=pGlobalUserInfo->GetUserInsureInfo();

		//设置变量
		pUserInsureData->lUserScore+=pIClientUserItem->GetUserScore()-LastScore.lScore;	
		pUserInsureData->lUserInsure+=pIClientUserItem->GetUserInsure()-LastScore.lInsure;
		pGlobalUserData->lUserScore = pUserInsureData->lUserScore;
		pGlobalUserData->lUserInsure = pUserInsureData->lUserInsure;
		pGlobalUserData->lUserIngot = LastScore.lIngot;
//		pGlobalUserData->dwUserMedal+=pIClientUserItem->GetUserMedal()-LastScore.dwUserMedal;
	}

	if (mIServerItemSink)
		mIServerItemSink->OnGRUserUpdate(pIClientUserItem);

	//游戏通知
	if ((pMeUserInfo->wTableID!=INVALID_TABLE)&&(pUserInfo->wTableID==pMeUserInfo->wTableID))
	{
		//变量定义
		tagUserScore UserScore;
		zeromemory(&UserScore,sizeof(UserScore));

		//设置变量
		UserScore.lScore=pUserInfo->lScore;
		UserScore.lGrade=pUserInfo->lGrade;
		UserScore.lInsure=pUserInfo->lInsure;
		UserScore.lIngot = pUserInfo->lIngot;
		UserScore.dwWinCount=pUserInfo->dwWinCount;
		UserScore.dwLostCount=pUserInfo->dwLostCount;
		UserScore.dwDrawCount=pUserInfo->dwDrawCount;
		UserScore.dwFleeCount=pUserInfo->dwFleeCount;
//		UserScore.dwUserMedal=pUserInfo->dwUserMedal;
		UserScore.dwExperience=pUserInfo->dwExperience;
		UserScore.lLoveLiness=pUserInfo->lLoveLiness;

		//发送数据
		if (IClientKernel::get())
			IClientKernel::get()->OnGFUserScore(pUserInfo->dwUserID,UserScore);
		return;
	}
}

void CServerItem::OnUserItemUpdate(IClientUserItem* pIClientUserItem, const tagUserStatus& LastStatus)
{
	//PLAZZ_PRINTF("CServerItem::OnUserItemUpdate\n");
	//变量定义
	tagUserInfo * pUserInfo=pIClientUserItem->GetUserInfo();
	tagUserInfo * pMeUserInfo=mMeUserItem->GetUserInfo();
	word wNowTableID=pIClientUserItem->GetTableID(),wLastTableID=LastStatus.wTableID;
	word wNowChairID=pIClientUserItem->GetChairID(),wLastChairID=LastStatus.wChairID;
	byte cbNowStatus=pIClientUserItem->GetUserStatus(),cbLastStatus=LastStatus.cbUserStatus;
	//PLAZZ_PRINTF("CServerItem::OnUserItemUpdate_1\n");
	////列表更新
	//m_UserListControl.UpdateDataItem(pIClientUserItem);

	////查找窗口
	//CDlgSearchUser * pDlgSearchUser=CDlgSearchUser::GetInstance();
	//if (pDlgSearchUser!=0) pDlgSearchUser->OnUserItemUpdate(pIClientUserItem,this);

	////私聊窗口
	//for (INT_PTR i=0;i<m_DlgWhisperItemArray.GetCount();i++)
	//{
	//	CDlgWhisper * pDlgWhisper=m_DlgWhisperItemArray[i];
	//	if (pDlgWhisper->m_hWnd!=0) pDlgWhisper->OnEventUserStatus(pIClientUserItem);
	//}

	// 更新界面上的 分数
	if (mIServerItemSink)
	{
		//PLAZZ_PRINTF("CServerItem::OnUserItemUpdate_2\n");
		mIServerItemSink->OnGRUserUpdate(pIClientUserItem);
	}
		
	
	//桌子离开
	if ((wLastTableID!=INVALID_TABLE)&&((wLastTableID!=wNowTableID)||(wLastChairID!=wNowChairID)))
	{
		//PLAZZ_PRINTF("CServerItem::OnUserItemUpdate_3\n");
		IClientUserItem * pITableUserItem=mTableViewFrame.GetClientUserItem(wLastTableID,wLastChairID);
		if (pITableUserItem==pIClientUserItem) mTableViewFrame.SetClientUserItem(wLastTableID,wLastChairID,0);
	}
	
	//桌子加入
	if ((wNowTableID!=INVALID_TABLE)&&(cbNowStatus!=US_LOOKON)&&((wNowTableID!=wLastTableID)||(wNowChairID!=wLastChairID)))
	{
		//PLAZZ_PRINTF("CServerItem::OnUserItemUpdate_4\n");
		//厌恶判断（黑名单）
		if(pUserInfo->dwUserID != pMeUserInfo->dwUserID && cbNowStatus == US_SIT && pMeUserInfo->wTableID == wNowTableID)
		{
			//变量定义
			ASSERT(CParameterGlobal::shared()!=0);
			CParameterGlobal * pParameterGlobal=CParameterGlobal::shared();

			//厌恶玩家
			if (pParameterGlobal->m_bLimitDetest==true)
			{
				if (pIClientUserItem->GetUserCompanion()==CP_DETEST)
				{
					PACKET_AIDE_SIZE(64);
					packet.write2Byte(wNowTableID);
					packet.write2Byte(wNowChairID);
					packet.write4Byte(pMeUserInfo->dwUserID);
					packet.write4Byte(pUserInfo->dwUserID);
					SendSocketData(MDM_GR_USER,SUB_GR_USER_REPULSE_SIT,packet.getBuffer(),packet.getPosition());

					//CMD_GR_UserRepulseSit UserRepulseSit;
					//UserRepulseSit.wTableID=wNowTableID;
					//UserRepulseSit.wChairID=wNowChairID;
					//UserRepulseSit.dwUserID=pMeUserInfo->dwUserID;
					//UserRepulseSit.dwRepulseUserID=pUserInfo->dwUserID;
					//SendSocketData(MDM_GR_USER,SUB_GR_USER_REPULSE_SIT,&UserRepulseSit,sizeof(UserRepulseSit));
				}
			}
		}
		mTableViewFrame.SetClientUserItem(wNowTableID,wNowChairID,pIClientUserItem);
	}
	
	//桌子状态
	if ((mTableViewFrame.GetChairCount() < MAX_CHAIR)&&(wNowTableID!=INVALID_TABLE)&&(wNowTableID==wLastTableID)&&(wNowChairID==wLastChairID))
	{
		//PLAZZ_PRINTF("CServerItem::OnUserItemUpdate_5\n");
		mTableViewFrame.UpdateTableView(wNowTableID);
	}
	
	//离开通知
	if ((wLastTableID!=INVALID_TABLE)&&((wNowTableID!=wLastTableID)||(wNowChairID!=wLastChairID)))
	{
		//PLAZZ_PRINTF("CServerItem::OnUserItemUpdate_6\n");
		//游戏通知
		if ((pIClientUserItem==mMeUserItem)||(wLastTableID==pMeUserInfo->wTableID))
		{
			tagUserStatus UserStatus;
			UserStatus.wTableID=wNowTableID;
			UserStatus.wChairID=wNowChairID;
			UserStatus.cbUserStatus=cbNowStatus;
			if (IClientKernel::get())
				IClientKernel::get()->OnGFUserStatus(pUserInfo->dwUserID,UserStatus);
		}

		if (pIClientUserItem==mMeUserItem)
		{
			//设置变量
			mReqTableID=INVALID_TABLE;
			mReqChairID=INVALID_CHAIR;

			if (CServerRule::IsAllowQuickMode(mServerAttribute.dwServerRule))
			{//快速模式的
				IntermitConnect(true);
				if (mIServerItemSink)
					mIServerItemSink->OnGFServerClose("");
			}
		}
		//return;
	}
	
	//加入处理
	if ((wNowTableID!=INVALID_TABLE)&&((wNowTableID!=wLastTableID)||(wNowChairID!=wLastChairID)))
	{
		//PLAZZ_PRINTF("CServerItem::OnUserItemUpdate_7\n");
		//自己判断
		if (mMeUserItem==pIClientUserItem)
		{
			//PLAZZ_PRINTF("CServerItem::OnUserItemUpdate_7_1\n");
			//设置变量
			mReqTableID=INVALID_TABLE;
			mReqChairID=INVALID_CHAIR;

			if (IClientKernel::get()==0)
			{
				//启动进程
				if (!mIServerItemSink || !mIServerItemSink->StartGame())
				{
					OnGFGameClose(GameExitCode_CreateFailed);
					return;
				}
			}
			else
			{
				//已存在场景，初始化内核
				IClientKernel::get()->Init();
			}
		}
		
		//游戏通知
		if ((mMeUserItem!=pIClientUserItem)&&(pMeUserInfo->wTableID==wNowTableID))
		{
			//PLAZZ_PRINTF("CServerItem::OnUserItemUpdate_8\n");
			ASSERT(wNowChairID!=INVALID_CHAIR);
			if (IClientKernel::get())
				IClientKernel::get()->OnGFUserEnter(pIClientUserItem);
		}

		return;
	}
	
	//状态改变
	if ((wNowTableID!=INVALID_TABLE)&&(wNowTableID==wLastTableID)&&(pMeUserInfo->wTableID==wNowTableID))
	{
		//PLAZZ_PRINTF("CServerItem::OnUserItemUpdate_9\n");
		//游戏通知
		tagUserStatus UserStatus;
		UserStatus.wTableID=wNowTableID;
		UserStatus.wChairID=wNowChairID;
		UserStatus.cbUserStatus=cbNowStatus;

		if (IClientKernel::get())
			IClientKernel::get()->OnGFUserStatus(pUserInfo->dwUserID,UserStatus);

		return;
	}
	
	return;
}

//用户更新
void CServerItem::OnUserItemUpdate(IClientUserItem * pIClientUserItem, const tagUserAttrib & UserAttrib)
{
	//变量定义
	word wMeTableID=mMeUserItem->GetTableID();
	word wUserTableID=pIClientUserItem->GetTableID();

	////列表更新
	//m_UserListControl.UpdateDataItem(pIClientUserItem);

	////查找窗口
	//CDlgSearchUser * pDlgSearchUser=CDlgSearchUser::GetInstance();
	//if (pDlgSearchUser!=0) pDlgSearchUser->OnUserItemUpdate(pIClientUserItem,this);
	
	////私聊窗口
	//for (INT_PTR i=0;i<m_DlgWhisperItemArray.GetCount();i++)
	//{
	//	CDlgWhisper * pDlgWhisper=m_DlgWhisperItemArray[i];
	//	if (pDlgWhisper->m_hWnd!=0) pDlgWhisper->OnEventUserStatus(pIClientUserItem);
	//}

	if (mIServerItemSink)
		mIServerItemSink->OnGRUserUpdate(pIClientUserItem);

	//通知游戏
	if ((wMeTableID!=INVALID_TABLE)&&(wMeTableID==wUserTableID))
	{
		//变量定义
		tagUserAttrib UserAttrib;
		UserAttrib.cbCompanion=pIClientUserItem->GetUserCompanion();

		//发送通知

		if (IClientKernel::get())
			IClientKernel::get()->OnGFUserAttrib(pIClientUserItem->GetUserID(),UserAttrib);
	}
	return;
}
