#include "CGPConversionMission.h"
#include "Tools/tools/PacketAide.h"
#define SIGNIN_MISSION_NULL		0
#define SIGNIN_MISSION_QUERY	1
#define SIGNIN_MISSION_DONE		2

CGPConversionMission::CGPConversionMission(const char* url, int port)
: CSocketMission(url, port)
{
	mIGPConversionMissionSink = 0;
	mMissionType = SIGNIN_MISSION_NULL;
}

// 设置回调接口
void CGPConversionMission::setMissionSink(IGPConversionMissionSink* pIGPSignInMissionSink)
{
	mIGPConversionMissionSink = pIGPSignInMissionSink;
}
//查询元宝兑换
void CGPConversionMission::query()
{
	mMissionType = SIGNIN_MISSION_QUERY;
	start();
}
void CGPConversionMission::done(int64 lExchangeIngot)
{
	m_lExchangeIngot = lExchangeIngot;
	mMissionType = SIGNIN_MISSION_DONE;
	start();
}

void CGPConversionMission::onEventTCPSocketLink()
{
	PLAZZ_PRINTF("CGPConversionMission::onEventTCPSocketLink\n");

	//变量定义
	CGlobalUserInfo * pGlobalUserInfo=CGlobalUserInfo::GetInstance();
	tagGlobalUserData * pGlobalUserData=pGlobalUserInfo->GetGlobalUserData();

	switch (mMissionType)
	{	
		//查询元宝兑换
		case SIGNIN_MISSION_QUERY:
		{
									 send(MDM_GP_USER_SERVICE, SUB_GP_EXCHANGE_QUERY, NULL, 0);
									 break;
		}
		//执行签到
		case SIGNIN_MISSION_DONE:
		{
									//打包数据
									PACKET_AIDE_SIZE(512);
									packet.write4Byte(pGlobalUserData->dwUserID);
									packet.write8Byte(m_lExchangeIngot);
									packet.writeUTF16(u8_2(DF::shared()->GetMachineID()), LEN_MACHINE_ID);
									//发送数据
									send(MDM_GP_USER_SERVICE, SUB_GP_EXCHANGE_SCORE, packet.getBuffer(), packet.getPosition());
									break;
		}
		default:
		stop();
		break;
	}
	
}

void CGPConversionMission::onEventTCPSocketShut()
{
	PLAZZ_PRINTF("CGPConversionMission::onEventTCPSocketShut\n");
}

void CGPConversionMission::onEventTCPSocketError(int errorCode)
{
	PLAZZ_PRINTF("CGPConversionMission::onEventTCPSocketError code[%d]\n", errorCode);
}

bool CGPConversionMission::onEventTCPSocketRead(int main, int sub, void* data, int size) 
{
	PLAZZ_PRINTF("CGPConversionMission::onEventTCPSocketRead main:%d sub:%d size:%d\n", main, sub, size);
	if (main != MDM_GP_USER_SERVICE)
	{
		return false;
	}
	switch (sub)
	{
		//兑换参数
		case SUB_GP_EXCHANGE_PARAMETER:
		{

									return onSubQueryInfoResult(data, size);
		}
		//兑换结果
		case SUB_GP_EXCHANGE_RESULT_:
		{
									  return onSubDoneResult(data, size);
		}
	}

	return false;
}

bool CGPConversionMission::onSubQueryInfoResult(void* data, int size )
{
	//查询参数
// 	struct CMD_GP_ExchangeParameter
// 	{
// 		WORD              wExchangeRate;            //兑换比率
// 		WORD              wMemberCount;           //会员数目
// 		tagMemberParameter        MemberParameter[10];        //会员参数
// 	};

	PACKET_AIDE_DATA(data);

	word wExchangeRate = packet.read2Byte();

	stop();

	if (mIGPConversionMissionSink)
		mIGPConversionMissionSink->onConversionQueryInfoResult(wExchangeRate);

	return true;
}

bool CGPConversionMission::onSubDoneResult(void* data, int size)
{
	//兑换结果
// 	struct CMD_GP_ExchangeResult
// 	{
// 		bool              bSuccessed;             //成功标识
// 		SCORE             lCurrScore;             //当前游戏币
// 		SCORE             lCurrIngot;             //当前元宝
// 		TCHAR             szNotifyContent[128];       //提示内容
// 	};

	PACKET_AIDE_DATA(data);
	bool bSuccessed = packet.readByte();
	SCORE lCurrScore = packet.read8Byte();
	SCORE lCurrIngot = packet.read8Byte();

	int len = (size - 17) / 2;
	u2string sDescribe;
	sDescribe.resize(len + 1, '\0');
	packet.readUTF16(&sDescribe[0], len);
	stop();

	if (bSuccessed)
	{
		CGlobalUserInfo * pGlobalUserInfo = CGlobalUserInfo::GetInstance();
		tagGlobalUserData * pGlobalUserData = pGlobalUserInfo->GetGlobalUserData();
		pGlobalUserData->lUserScore = lCurrScore;
		pGlobalUserData->lUserIngot = lCurrIngot;
	}
	if (mIGPConversionMissionSink)
		mIGPConversionMissionSink->onConversionResult(bSuccessed, lCurrIngot, u2_8(&sDescribe[0]));

	return true;
}