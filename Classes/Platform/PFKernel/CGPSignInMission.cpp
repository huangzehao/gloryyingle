#include "CGPSignInMission.h"
#include "Tools/tools/PacketAide.h"
#define SIGNIN_MISSION_NULL		0
#define SIGNIN_MISSION_QUERY	1
#define SIGNIN_MISSION_DONE		2

CGPSignInMission::CGPSignInMission(const char* url, int port)
: CSocketMission(url, port)
{
	mIGPSignInMissionSink = 0;
	mMissionType = SIGNIN_MISSION_NULL;
}

// 设置回调接口
void CGPSignInMission::setMissionSink(IGPSignInMissionSink* pIGPSignInMissionSink)
{
	mIGPSignInMissionSink = pIGPSignInMissionSink;
}
//查询签到
void CGPSignInMission::query()
{
	mMissionType = SIGNIN_MISSION_QUERY;
	start();
}
void CGPSignInMission::done()
{
	mMissionType = SIGNIN_MISSION_DONE;
	start();
}

void CGPSignInMission::onEventTCPSocketLink()
{
	PLAZZ_PRINTF("CGPSignInMission::onEventTCPSocketLink\n");

	//变量定义
	CGlobalUserInfo * pGlobalUserInfo=CGlobalUserInfo::GetInstance();
	tagGlobalUserData * pGlobalUserData=pGlobalUserInfo->GetGlobalUserData();

	switch (mMissionType)
	{		// 查询
		case SIGNIN_MISSION_QUERY:
		{
									 //打包数据
									 PACKET_AIDE_SIZE(512);
									 packet.write4Byte(pGlobalUserData->dwUserID);
									 packet.writeUTF16(u8_2(pGlobalUserData->szPassword), LEN_PASSWORD);
									 //发送数据
									 send(MDM_GP_USER_SERVICE, SUB_GP_CHECKIN_QUERY, packet.getBuffer(), packet.getPosition());

									 break;
		}
		//执行签到
		case SIGNIN_MISSION_DONE:
		{
									//打包数据
									PACKET_AIDE_SIZE(512);
									packet.write4Byte(pGlobalUserData->dwUserID);
									packet.writeUTF16(u8_2(pGlobalUserData->szPassword), LEN_PASSWORD);
									packet.writeUTF16(u8_2(DF::shared()->GetMachineID()), LEN_MACHINE_ID);
									//发送数据
									send(MDM_GP_USER_SERVICE, SUB_GP_CHECKIN_DONE, packet.getBuffer(), packet.getPosition());
									break;
		}
	default:
		stop();
		break;
	}
	
}

void CGPSignInMission::onEventTCPSocketShut()
{
	PLAZZ_PRINTF("CGPSignInMission::onEventTCPSocketShut\n");
}

void CGPSignInMission::onEventTCPSocketError(int errorCode)
{
	PLAZZ_PRINTF("CGPSignInMission::onEventTCPSocketError code[%d]\n", errorCode);
}

bool CGPSignInMission::onEventTCPSocketRead(int main, int sub, void* data, int size) 
{
	PLAZZ_PRINTF("CGPSignInMission::onEventTCPSocketRead main:%d sub:%d size:%d\n", main, sub, size);
	if (main != MDM_GP_USER_SERVICE)
	{
		return false;
	}
	switch (sub)
	{
		//签到信息
		case SUB_GP_CHECKIN_INFO:
		{

									return onSubQueryInfoResult(data, size);
		}
		//签到结果
		case SUB_GP_CHECKIN_RESULT:
		{
									  return onSubDoneResult(data, size);
		}
	}

	return false;
}

bool CGPSignInMission::onSubQueryInfoResult(void* data, int size )
{
	// 签到信息
	// struct CMD_GP_CheckInInfo
	// {
	// 	WORD              wSeriesDate;            //连续日期
	// 	bool              bTodayChecked;            //签到标识
	// 	SCORE             lRewardGold[LEN_WEEK];        //奖励金币
	// };

	PACKET_AIDE_DATA(data);
	word seriesdata = packet.read2Byte();
	bool bTodyChecked = packet.readByte();

	SCORE lRewardGold[7];
	for (int i = 0; i < 7; i++)
	{
		lRewardGold[i] = packet.read8Byte();
	}

	stop();

	if (mIGPSignInMissionSink)
		mIGPSignInMissionSink->onSignInQueryInfoResult(seriesdata, bTodyChecked, lRewardGold);
	return true;
}

bool CGPSignInMission::onSubDoneResult(void* data, int size)
{
	// 签到结果
	// struct CMD_GP_CheckInResult
	// {
	// 	bool              bSuccessed;             //成功标识
	// 	SCORE             lScore;					//当前金币
	// 	TCHAR             szNotifyContent[128];       //提示内容
	// };

	PACKET_AIDE_DATA(data);
	bool bSuccessed = packet.readByte();
	SCORE lScore = packet.read8Byte();
	const char * szNotifyContent = (const char *)(packet.getBuffer());

	stop();
	if (bSuccessed)
	{
		CGlobalUserInfo * pGlobalUserInfo = CGlobalUserInfo::GetInstance();
		tagGlobalUserData * pGlobalUserData = pGlobalUserInfo->GetGlobalUserData();
		pGlobalUserData->lUserScore = lScore;
	}
	if (mIGPSignInMissionSink)
		mIGPSignInMissionSink->onSignInDoneResult(bSuccessed, lScore, szNotifyContent);
	return true;
}