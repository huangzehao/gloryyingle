#include "CGPLowProtectMission.h"
#include "Tools/tools/PacketAide.h"
#define MISSION_REQUERK_LOW_NULL	0
#define MISSION_REQUICK_LOW__LOAD	1
#define MISSION_REQUICK_LOW_TAKE	2

CGPLowProtectMission::CGPLowProtectMission(const char * url, int port)
: CSocketMission(url, port)
{
	mIGPLowProtectMissionSink = 0;
	mMissionType = MISSION_REQUERK_LOW_NULL;
}

void CGPLowProtectMission::setMissionSink(IGPLowProtectMissionLink* pIGPLowProtectMissionSink)
{
	mIGPLowProtectMissionSink = pIGPLowProtectMissionSink;
}

void CGPLowProtectMission::requickLowProtectInfo()
{
	mMissionType = MISSION_REQUICK_LOW__LOAD;
	start();
}

void CGPLowProtectMission::obtainLowProtect()
{
	mMissionType = MISSION_REQUICK_LOW_TAKE;
	start();
}

void CGPLowProtectMission::onEventTCPSocketLink()
{
	PLAZZ_PRINTF("CGPSignInMission::onEventTCPSocketLink\n");

	//变量定义
	CGlobalUserInfo * pGlobalUserInfo = CGlobalUserInfo::GetInstance();
	tagGlobalUserData * pGlobalUserData = pGlobalUserInfo->GetGlobalUserData();

	switch (mMissionType)
	{
		case MISSION_REQUICK_LOW__LOAD:
		{
										  send(MDM_GP_USER_SERVICE, SUB_GP_BASEENSURE_LOAD, NULL, 0);
										  break;
		}
		case MISSION_REQUICK_LOW_TAKE:
		{
										 //打包数据
										 PACKET_AIDE_SIZE(512);
										 packet.write4Byte(pGlobalUserData->dwUserID);
										 packet.writeUTF16(u8_2(pGlobalUserData->szPassword), LEN_PASSWORD);
										 packet.writeUTF16(u8_2(DF::shared()->GetMachineID()), LEN_MACHINE_ID);

										 send(MDM_GP_USER_SERVICE, SUB_GP_BASEENSURE_TAKE, packet.getBuffer(), packet.getPosition());
										 break;
		}
		default:
		break;
	}
}

void CGPLowProtectMission::onEventTCPSocketShut()
{
	PLAZZ_PRINTF("CGPSignInMission::onEventTCPSocketShut\n");


}

void CGPLowProtectMission::onEventTCPSocketError(int errorCode)
{
	PLAZZ_PRINTF("CGPSignInMission::onEventTCPSocketError code[%d]\n", errorCode);
}

bool CGPLowProtectMission::onEventTCPSocketRead(int main, int sub, void* data, int dataSize)
{
	PLAZZ_PRINTF("CGPSignInMission::onEventTCPSocketRead main:%d sub:%d size:%d\n", main, sub, dataSize);

	if (main != MDM_GP_USER_SERVICE)
	{
		return false;
	}
	switch (sub)
	{
		case SUB_GP_BASEENSURE_PARAMETER:
		{
											// 低保参数
											// struct CMD_GP_BaseEnsureParamter
											// {
											// 	SCORE             lScoreCondition;          //游戏币条件
											// 	SCORE             lScoreAmount;           //游戏币数量
											// 	BYTE              cbTakeTimes;            //领取次数
											// };
											return onRequickLowProtectInfo(data, dataSize);
		}
		case SUB_GP_BASEENSURE_RESULT:
		{
										 // 低保结果
										 // struct CMD_GP_BaseEnsureResult
										 // {
										 // 	bool              bSuccessed;             //成功标识
										 // 	SCORE             lGameScore;             //当前游戏币
										 // 	TCHAR             szNotifyContent[128];       //提示内容
										 // };
										 return  CGPLowProtectMission::onObtainLowProtect(data, dataSize);
		}
		default:
		break;
	}
	return true;
}

void CGPLowProtectMission::sendRequickLowProtectInfo()
{

}

void CGPLowProtectMission::sendObtainLowProtect()
{

}

bool CGPLowProtectMission::onRequickLowProtectInfo(void * data, int size)
{
	// 低保参数
	// struct CMD_GP_BaseEnsureParamter
	// {
	// 	SCORE             lScoreCondition;          //游戏币条件
	// 	SCORE             lScoreAmount;           //游戏币数量
	// 	BYTE              cbTakeTimes;            //领取次数
	// };

	PACKET_AIDE_DATA(data);
	SCORE lScoreCondition = packet.read8Byte();
	SCORE lScoreAmount = packet.read8Byte();
	byte cbTakeTimes = packet.readByte();

	stop();

	if (mIGPLowProtectMissionSink)
		mIGPLowProtectMissionSink->onSignInQueryInfoResult(lScoreCondition, lScoreAmount, cbTakeTimes);

	
	return true;
}

bool CGPLowProtectMission::onObtainLowProtect(void * data, int size)
{
	// 低保结果
	// struct CMD_GP_BaseEnsureResult
	// {
	// 	bool              bSuccessed;             //成功标识
	// 	SCORE             lGameScore;             //当前游戏币
	// 	TCHAR             szNotifyContent[128];       //提示内容
	// };

	PACKET_AIDE_DATA(data);
	bool bSuccessed = packet.readByte();
	SCORE lGameScore = packet.read8Byte();
	const char * szNotifyContent = (const char *)(packet.getBuffer());
	stop();

	if (bSuccessed)
	{
		CGlobalUserInfo * pGlobalUserInfo = CGlobalUserInfo::GetInstance();
		tagGlobalUserData * pGlobalUserData = pGlobalUserInfo->GetGlobalUserData();
		pGlobalUserData->lUserScore = lGameScore;
	}
	

	if (mIGPLowProtectMissionSink)
		mIGPLowProtectMissionSink->onSignInDoneResult(bSuccessed, lGameScore, szNotifyContent);

	return true;
}
