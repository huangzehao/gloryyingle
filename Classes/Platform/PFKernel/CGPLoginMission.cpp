#include "cocos2d.h"
#include "CGPLoginMission.h"
#include "Tools/tools/StringData.h"
#include "Tools/tools/PacketAide.h"
#define MISSION_LOGIN_ACCOUNT	1
#define MISSION_LOGIN_GAMEID	2
#define MISSION_REGISTER		3
#define MISSION_UPDATE_INFO		4
#define MISSION_SERVER_INFO		5
#define MISSION_SYSTEM_MESSAGE	6
#define MISSION_LOGIN_TENCENT		7

USING_NS_CC;

CGPLoginMission::CGPLoginMission(const char* url, int port)
	: CSocketMission(url, port)
{
	mMissionType = 0;
	mIGPLoginMissionSink = 0;
	mAllowType.push_back(2);
	mAllowType.push_back(4);
	mAllowType.push_back(5);
	mAllowType.push_back(6);
	mAllowType.push_back(8);
}


// 设置回调接口
void CGPLoginMission::setMissionSink(IGPLoginMissionSink* pIGPLoginMissionSink)
{
	mIGPLoginMissionSink = pIGPLoginMissionSink;
}

// 账号登陆
void CGPLoginMission::loginAccount(const tagGPLoginAccount& LoginAccount)
{
	memcpy(&mLoginAccount, &LoginAccount, sizeof(mLoginAccount));
	mMissionType = MISSION_LOGIN_ACCOUNT;
	start();
}

// I D登陆
void CGPLoginMission::loginGameID(const tagGPLoginGameID& LoginGameID)
{
	memcpy(&mLoginGameID, &LoginGameID, sizeof(mLoginGameID));
	mMissionType = MISSION_LOGIN_GAMEID;
	start();
}

void CGPLoginMission::registerServer(const tagGPRegisterAccount& RegisterAccount)
{
	memcpy(&mRegisterAccount, &RegisterAccount, sizeof(mRegisterAccount));
	mMissionType = MISSION_REGISTER;
	start();
}

void CGPLoginMission::loginTencent(const tagGPLoginTencent& LoginTencent)
{
	memcpy(&mLoginTencent, &LoginTencent, sizeof(mLoginTencent));
	mMissionType = MISSION_LOGIN_TENCENT;
	start();
}

void CGPLoginMission::updateOnlineInfo()
{
	mMissionType = MISSION_UPDATE_INFO;
	start();
}

//更新类型
bool CGPLoginMission::updateServerInfo(uint16 kind)
{
	KIND_ITER it = std::find(mKindList.begin(), mKindList.end(), kind);

	if (it != mKindList.end())
	{
		return false;
	}

	it = std::find(mKindWaitList.begin(), mKindWaitList.end(), kind);

	if (it != mKindWaitList.end())
	{
		return false;
	}

	mKindList.push_back(kind);
	mMissionType = MISSION_SERVER_INFO;
	start();

	return true;
}

bool CGPLoginMission::getSystemMessage()
{
	CServerListData * pServerListData = CServerListData::shared();
	pServerListData->m_SystemMessageVector.clear();

	mMissionType = MISSION_SYSTEM_MESSAGE;
	start();
	return true;
}

//发送登陆信息
bool CGPLoginMission::sendLoginAccount(const tagGPLoginAccount& LoginAccount)
{
	//PLAZZ_PRINTF(a_u8("CGPLoginMission::sendLoginAccount\n"));
	//加密密码
	const char* sPassword = DF::MD5Encrypt(LoginAccount.szPassword);

	//变量定义
	byte cbBuffer[SIZE_PACK_DATA];
	PacketAide packet(cbBuffer);

	//打包数据
	packet.write2Byte(DF::shared()->GetGameKindID());
	packet.write4Byte(DF::shared()->GetPlazaVersion());
	packet.writeByte(DF::shared()->GetDeviceType());
	packet.writeUTF16(u8_2(sPassword), LEN_PASSWORD);
	packet.writeUTF16(u8_2(LoginAccount.szAccounts), LEN_ACCOUNTS);
	packet.writeUTF16(u8_2(DF::shared()->GetMachineID()), LEN_MACHINE_ID);
	packet.writeUTF16(u8_2(DF::shared()->GetMobilePhone()), LEN_MOBILE_PHONE);

	//保存密码
	CGlobalUserInfo * pGlobalUserInfo=CGlobalUserInfo::GetInstance();
	tagGlobalUserData * pGlobalUserData=pGlobalUserInfo->GetGlobalUserData();
	strncpy(pGlobalUserData->szPassword,sPassword, countarray(pGlobalUserData->szPassword));

	//发送数据
	send(MDM_MB_LOGON, SUB_MB_LOGON_ACCOUNTS, cbBuffer, packet.getPosition());
	return true;
}

//发送登陆信息
bool CGPLoginMission::sendLoginGameID(const tagGPLoginGameID& LoginGameID)
{
	//TEST_NULL
	//加密密码
	const char* sPassword = DF::MD5Encrypt(LoginGameID.szPassword);

	//变量定义
	byte cbBuffer[SIZE_PACK_DATA];
	PacketAide packet(cbBuffer);
	//打包数据
	packet.write2Byte(DF::shared()->GetGameKindID());
	packet.write4Byte(DF::shared()->GetPlazaVersion());
	packet.writeByte(DF::shared()->GetDeviceType());
	packet.write4Byte(LoginGameID.dwGameID);
	packet.writeUTF16(u8_2(sPassword), LEN_PASSWORD);
	packet.writeUTF16(u8_2(DF::shared()->GetMachineID()), LEN_MACHINE_ID);
	packet.writeUTF16(u8_2(DF::shared()->GetMobilePhone()), LEN_MOBILE_PHONE);

	//保存密码
	CGlobalUserInfo * pGlobalUserInfo=CGlobalUserInfo::GetInstance();
	tagGlobalUserData * pGlobalUserData=pGlobalUserInfo->GetGlobalUserData();
	strcpy(pGlobalUserData->szPassword,sPassword);

	//发送数据
	send(MDM_MB_LOGON, SUB_MB_LOGON_GAMEID, cbBuffer, packet.getPosition());		
	return true;


	/*
	//变量定义
	byte cbBuffer[SIZE_PACK_DATA];

	////变量定义
	//tchar szPasswordMD5[LEN_MD5];
	//DF::MD5Encrypt(LoginGameID.szPassword,szPasswordMD5);

	//变量定义
	CMD_GP_LogonGameID * pLogonGameID=(CMD_GP_LogonGameID *)cbBuffer;

	//系统信息
	pLogonGameID->cbDeviceType=DF::shared()->GetDeviceType();
	pLogonGameID->wModuleID=DF::shared()->GetPlazzKindID();
	pLogonGameID->dwPlazaVersion=DF::shared()->GetPlazaVersion();
	DF::shared()->GetMobilePhone(pLogonGameID->szMobilePhone);

	//机器标识
	DF::shared()->GetMachineID(pLogonGameID->szMachineID);

	//登录信息
	DF::MD5Encrypt(LoginGameID.szPassword,pLogonGameID->szPassword);
	//tstrcpyn(pLogonGameID->szPassword,szPasswordMD5,CountArray(pLogonGameID->szPassword));
	pLogonGameID->cbValidateFlags=LoginGameID.cbValidateFlags;
	pLogonGameID->dwGameID=LoginGameID.dwGameID;

	//保存密码
	CGlobalUserInfo * pGlobalUserInfo=CGlobalUserInfo::GetInstance();
	tagGlobalUserData * pGlobalUserData=pGlobalUserInfo->GetGlobalUserData();
	tstrcpyn(pGlobalUserData->szPassword,pLogonGameID->szPassword,countarray(pGlobalUserData->szPassword));

	//发送数据
	send(MDM_GP_LOGON, SUB_GP_LOGON_GAMEID, cbBuffer, sizeof(CMD_GP_LogonGameID));		
	//*/
	return true;
}

bool CGPLoginMission::sendLoginTencent(const tagGPLoginTencent& LoginTencent)
{
	////其它登录
	//struct CMD_MB_LogonOtherPlatform
	//{
	//	//系统信息
	//	WORD							wModuleID;							//模块标识
	//	DWORD							dwPlazaVersion;						//广场版本
	//	BYTE                            cbDeviceType;                       //设备类型
	//
	//	//登录信息
	//	BYTE							cbGender;							//用户性别
	//	BYTE							cbPlatformID;						//平台编号
	//	TCHAR							szUserUin[LEN_USER_UIN];			//用户Uin
	//	TCHAR							szNickName[LEN_NICKNAME];			//用户昵称
	//	TCHAR							szCompellation[LEN_COMPELLATION];	//真实名字
	//
	//	//连接信息
	//	TCHAR							szMachineID[LEN_MACHINE_ID];		//机器标识
	//	TCHAR							szMobilePhone[LEN_MOBILE_PHONE];	//电话号码
	//};

	PACKET_AIDE_SIZE(SIZE_PACK_DATA);
	packet.write2Byte(DF::shared()->GetGameKindID());
	packet.write4Byte(DF::shared()->GetPlazaVersion());
	packet.writeByte(DF::shared()->GetDeviceType());
	packet.writeByte(LoginTencent.cbGender);
	packet.writeByte(LoginTencent.cbPlatformID);
	packet.writeUTF16(u8_2(LoginTencent.szUserUin), LEN_USER_UIN);
	packet.writeUTF16(u8_2(LoginTencent.szNickName), LEN_NICKNAME);
	packet.writeUTF16(u8_2(LoginTencent.szCompellation), LEN_COMPELLATION);
	packet.writeUTF16(u8_2(DF::shared()->GetMachineID()), LEN_MACHINE_ID);
	packet.writeUTF16(u8_2(DF::shared()->GetMobilePhone()), LEN_MOBILE_PHONE);

	//发送数据
	send(MDM_MB_LOGON, SUB_MB_LOGON_OTHERPLATFORM, packet.getBuffer(), packet.getPosition());

	return true;
}

//发送注册信息
bool CGPLoginMission::sendRegisterPacket(const tagGPRegisterAccount& RegisterAccount)
{
	//TEST_NULL
	//变量定义
	PACKET_AIDE_SIZE(SIZE_PACK_DATA);
	packet.write2Byte(DF::shared()->GetGameKindID());
	packet.write4Byte(DF::shared()->GetPlazaVersion());
	packet.writeByte(DF::shared()->GetDeviceType());
	packet.writeUTF16(u8_2(DF::MD5Encrypt(RegisterAccount.szLogonPass)), LEN_PASSWORD);
	packet.write2Byte(RegisterAccount.wFaceID);
	packet.writeByte(RegisterAccount.cbGender);
	packet.writeUTF16(u8_2(RegisterAccount.szAccounts), LEN_ACCOUNTS);
	packet.writeUTF16(u8_2(RegisterAccount.szNickName), LEN_NICKNAME);
	packet.writeUTF16(u8_2(DF::shared()->GetMachineID()), LEN_MACHINE_ID);
	packet.writeUTF16(u8_2(DF::shared()->GetMobilePhone()), LEN_MOBILE_PHONE);
	
	//保存密码
	CGlobalUserInfo * pGlobalUserInfo=CGlobalUserInfo::GetInstance();
	tagGlobalUserData * pGlobalUserData=pGlobalUserInfo->GetGlobalUserData();
	strncpy(pGlobalUserData->szPassword,DF::MD5Encrypt(RegisterAccount.szLogonPass),countarray(pGlobalUserData->szPassword));

	//发送数据
	send(MDM_MB_LOGON, SUB_MB_REGISTER_ACCOUNTS, packet.getBuffer(), packet.getPosition());		
	return true;

	////变量定义
	//byte cbBuffer[SIZE_PACK_DATA];

	//////变量定义
	////tchar szLogonPassMD5[LEN_MD5];
	////tchar szInsurePassMD5[LEN_MD5];
	////CWHEncrypt::MD5Encrypt(RegisterAccount.szLogonPass,szLogonPassMD5);
	////CWHEncrypt::MD5Encrypt(RegisterAccount.szInsurePass,szInsurePassMD5);

	////变量定义
	//CMD_GP_RegisterAccounts * pRegisterAccounts=(CMD_GP_RegisterAccounts *)cbBuffer;

	////系统信息
	//pRegisterAccounts->cbDeviceType=DF::shared()->GetDeviceType();
	//pRegisterAccounts->wModuleID=DF::shared()->GetPlazzKindID();
	//pRegisterAccounts->dwPlazaVersion=DF::shared()->GetPlazaVersion();
	//DF::shared()->GetMobilePhone(pRegisterAccounts->szMobilePhone);

	////机器标识
	//DF::shared()->GetMachineID(pRegisterAccounts->szMachineID);

	////基本信息
	//pRegisterAccounts->wFaceID=RegisterAccount.wFaceID;
	//pRegisterAccounts->cbGender=RegisterAccount.cbGender;
	//pRegisterAccounts->cbValidateFlags=RegisterAccount.cbValidateFlags;

	//DF::MD5Encrypt(RegisterAccount.szLogonPass,pRegisterAccounts->szLogonPass);
	//DF::MD5Encrypt(RegisterAccount.szInsurePass,pRegisterAccounts->szInsurePass);

	//tstrcpyn(pRegisterAccounts->szAccounts,RegisterAccount.szAccounts,countarray(pRegisterAccounts->szAccounts));
	//tstrcpyn(pRegisterAccounts->szNickName,RegisterAccount.szNickName,countarray(pRegisterAccounts->szNickName));
	//tstrcpyn(pRegisterAccounts->szSpreader,RegisterAccount.szSpreader,countarray(pRegisterAccounts->szSpreader));
	////tstrcpyn(pRegisterAccounts->szLogonPass,szLogonPassMD5,CountArray(pRegisterAccounts->szLogonPass));
	////tstrcpyn(pRegisterAccounts->szInsurePass,szInsurePassMD5,CountArray(pRegisterAccounts->szInsurePass));
	//tstrcpyn(pRegisterAccounts->szPassPortID,RegisterAccount.szPassPortID,countarray(pRegisterAccounts->szPassPortID));
	//tstrcpyn(pRegisterAccounts->szCompellation,RegisterAccount.szCompellation,countarray(pRegisterAccounts->szCompellation));
	//
	////保存密码
	//CGlobalUserInfo * pGlobalUserInfo=CGlobalUserInfo::GetInstance();
	//tagGlobalUserData * pGlobalUserData=pGlobalUserInfo->GetGlobalUserData();
	//tstrcpyn(pGlobalUserData->szPassword,pRegisterAccounts->szLogonPass,countarray(pGlobalUserData->szPassword));

	////发送数据
	//send(MDM_GP_LOGON, SUB_GP_REGISTER_ACCOUNTS, cbBuffer, sizeof(CMD_GP_RegisterAccounts));		
	//return true;
}


// 更新人数
bool CGPLoginMission::sendUpdateOnlineInfoPacket()
{
	PACKET_AIDE_SIZE(SIZE_PACK_DATA);
	packet.setPosition(2);
	//变量定义
	int count = 0;

	//变量定义
	CServerListData * pServerListData=CServerListData::shared();
	CGameServerItemMap::iterator it = pServerListData->GetServerItemMapBegin();
	CGameServerItem * pGameServerItem=0;

	while ((pGameServerItem=pServerListData->EmunGameServerItem(it)))
	{
		//设置房间
		if (pGameServerItem!=0)
		{
			tagGameServer * pGameServer=&pGameServerItem->m_GameServer;
			packet.write2Byte(pGameServer->wServerID);
			count++;
		}
	}

	int pos = packet.getPosition();
	packet.setPosition(0);
	packet.write2Byte(count);
	packet.setPosition(pos);
	//发送数据
	send(MDM_GP_SERVER_LIST,SUB_GP_GET_ONLINE, packet.getBuffer(), packet.getPosition());

	return true;

	////变量定义
	//CMD_GP_GetOnline GetOnline;
	//zeromemory(&GetOnline,sizeof(GetOnline));

	////变量定义
	//CServerListData * pServerListData=CServerListData::shared();
	//CGameServerItemMap::iterator it = pServerListData->GetServerItemMapBegin();
	//CGameServerItem * pGameServerItem=0;

	//while (pGameServerItem=pServerListData->EmunGameServerItem(it))
	//{
	//	//溢出判断
	//	if (GetOnline.wServerCount>=countarray(GetOnline.wOnLineServerID))
	//	{
	//		ASSERT(FALSE);
	//		break;
	//	}

	//	//设置房间
	//	if (pGameServerItem!=0)
	//	{
	//		tagGameServer * pGameServer=&pGameServerItem->m_GameServer;
	//		GetOnline.wOnLineServerID[GetOnline.wServerCount++]=pGameServer->wServerID;
	//	}
	//}

	////发送数据
	//word wHeadSize=sizeof(GetOnline)-sizeof(GetOnline.wOnLineServerID);
	//word wSendSize=wHeadSize+sizeof(GetOnline.wOnLineServerID[0])*GetOnline.wServerCount;
	//send(MDM_GP_SERVER_LIST,SUB_GP_GET_ONLINE,&GetOnline,wSendSize);

	//return true;
}

// 更新类型房间列表
void CGPLoginMission::sendUpdateServerInfo()
{
	//列表任务
	if (mKindList.size()>0L)
	{
		//变量定义
		uint16 wKindIDItem[MAX_KIND];
		uint16 wKindIDCount=(uint16)tmin(countarray(wKindIDItem),mKindList.size());

		//变量定义
		KIND_ITER it = mKindList.begin();
		int i = 0;
		for (; i < wKindIDCount && it != mKindList.end(); ++it)
		{
			wKindIDItem[i] = *it;
			mKindWaitList.push_back(wKindIDItem[i]);
			++i;
		}
		
		mKindList.clear();

		//发送数据
		send(MDM_GP_SERVER_LIST,SUB_GP_GET_SERVER,wKindIDItem,wKindIDCount*sizeof(uint16));
	}
	else
	{
		stop();
	}
}

//获取游戏消息
bool CGPLoginMission::sendGetSystemMessage()
{
	send(MDM_GP_SYSTEM, SUB_GP_GET_MESSAGE, nullptr, 0);
	return true;
}

void CGPLoginMission::onEventTCPSocketLink()
{
	//PLAZZ_PRINTF(a_u8("CGPLoginMission::onEventTCPSocketLink\n"));

	switch (mMissionType)
	{
		// 登陆
	case MISSION_LOGIN_ACCOUNT:
		sendLoginAccount(mLoginAccount);
		break;
		// 登陆
	case MISSION_LOGIN_GAMEID:
		sendLoginGameID(mLoginGameID);
		break;
	case MISSION_LOGIN_TENCENT:
		sendLoginTencent(mLoginTencent);
		break;
		// 注册
	case MISSION_REGISTER:
		sendRegisterPacket(mRegisterAccount);
		break;
		// 更新人数
	case MISSION_UPDATE_INFO:
		sendUpdateOnlineInfoPacket();
		break;
		// 更新类型房间列表
	case MISSION_SERVER_INFO:
		sendUpdateServerInfo();
		break;
		// 获取系统消息
	case MISSION_SYSTEM_MESSAGE:
		sendGetSystemMessage();
		break;
	}

	return;
}

void CGPLoginMission::onEventTCPSocketShut()
{
	//PLAZZ_PRINTF(a_u8("CGPLoginMission::onEventTCPSocketShut\n"));
}

void CGPLoginMission::onEventTCPSocketError(int errorCode)
{
	PLAZZ_PRINTF(a_u8("CGPLoginMission::onEventTCPSocketError code[%d]\n"), errorCode);
	if (mIGPLoginMissionSink)
		mIGPLoginMissionSink->onGPError(errorCode);
}

bool CGPLoginMission::onEventTCPSocketRead(int main, int sub, void* data, int size) 
{
	PLAZZ_PRINTF(a_u8("onEventTCPSocketRead main:%d sub:%d size:%d\n"), main, sub, size);
	switch (main)
	{
	case MDM_MB_LOGON:			return onSocketMainLogon(sub, data, size);
	case MDM_GP_SERVER_LIST:	return onSocketMainServerList(sub, data, size);
	case MDM_GP_SYSTEM:			return onSocketMainSystem(sub, data, size);
	default:
		break;
	}
	return false;
}

//////////////////////////////////////////////////////////////////////////
// 登陆信息
bool CGPLoginMission::onSocketMainLogon(int sub, void* data, int size)
{
	switch (sub)
	{
		//登录成功
	case SUB_MB_LOGON_SUCCESS:		return onSocketSubLogonSuccess(data, size);
		//登录失败
	case SUB_MB_LOGON_FAILURE:		return onSocketSubLogonFailure(data, size);
// 		//登录失败
// 	case SUB_GP_VALIDATE_MBCARD:	return onSocketSubLogonValidateMBCard(data, size);
// 		//升级提示
// 	case SUB_MB_UPDATE_NOTIFY:		return onSocketSubUpdateNotify(data, size);
// 		//登录完成
	case SUB_GP_LOGON_FINISH:		return onSocketSubLogonFinish(data, size);
	case SUB_GP_GROWLEVEL_CONFIG: return true;
	}

	return false;
}

//登录成功
bool CGPLoginMission::onSocketSubLogonSuccess(void* data, int size)
{
	PACKET_AIDE_DATA(data);

	//变量定义
	CGlobalUserInfo * pGlobalUserInfo=CGlobalUserInfo::GetInstance();
	tagGlobalUserData * pGlobalUserData=pGlobalUserInfo->GetGlobalUserData();
	// 新版本换位置了
	//tagUserInsureInfo * pUserInsureInfo=pGlobalUserInfo->GetUserInsureInfo();

	//保存信息
	pGlobalUserData->wFaceID = packet.read2Byte();
	pGlobalUserData->cbGender = packet.readByte();
	pGlobalUserData->dwCustomID = packet.read4Byte();
	pGlobalUserData->dwUserID = packet.read4Byte();
	pGlobalUserData->dwGameID = packet.read4Byte();
	pGlobalUserData->dwExperience = packet.read4Byte();
	pGlobalUserData->dwLoveLiness = packet.read4Byte();

	//账号、动态密码
	u2string sAccount;
	u2string sDynamicPass;
	sAccount.resize(LEN_ACCOUNTS + 1, '\0');
	sDynamicPass.resize(LEN_PASSWORD + 1, '\0');
	packet.readUTF16(&sAccount[0], LEN_ACCOUNTS);
	packet.readUTF16(&sDynamicPass[0], LEN_PASSWORD);
	strncpy(pGlobalUserData->szNickName, u2_8(&sAccount[0]), countarray(pGlobalUserData->szNickName));
	strncpy(pGlobalUserData->szDynamicPass, u2_8(&sDynamicPass[0]), countarray(pGlobalUserData->szDynamicPass));
	strncpy(pGlobalUserData->szAccounts, mLoginAccount.szAccounts, sizeof(pGlobalUserData->szAccounts));
	//金币信息
	pGlobalUserData->lUserScore = packet.read8Byte();
	pGlobalUserData->lUserIngot = packet.read8Byte();
	pGlobalUserData->lUserInsure = packet.read8Byte();
	pGlobalUserData->dUserBeans = packet.read8Byte();
	pGlobalUserData->cbInsureEnabled = packet.readByte();
	
// 	while (size > packet.getPosition())
// 	{
// 		//提取数据
// 		word wDataSize = packet.read2Byte();
// 		word wDataDescribe = packet.read2Byte();
// 		if (wDataDescribe==0) break;
// 
// 		//提取数据
// 		switch (wDataDescribe)
// 		{
// 		case DTP_GP_MEMBER_INFO:	//会员信息
// 			{
// 
// 				pGlobalUserData->cbMemberOrder = packet.readByte();
// 				pGlobalUserData->MemberOverDate.wYear = packet.read2Byte();
// 				pGlobalUserData->MemberOverDate.wMonth = packet.read2Byte();
// 				pGlobalUserData->MemberOverDate.wDayOfWeek = packet.read2Byte();
// 				pGlobalUserData->MemberOverDate.wDay = packet.read2Byte();
// 				pGlobalUserData->MemberOverDate.wHour = packet.read2Byte();
// 				pGlobalUserData->MemberOverDate.wMinute = packet.read2Byte();
// 				pGlobalUserData->MemberOverDate.wSecond = packet.read2Byte();
// 				pGlobalUserData->MemberOverDate.wMilliseconds = packet.read2Byte();
// 				
// 				break;
// 			}
// 		case DTP_GP_UNDER_WRITE:	//个性签名
// 			{
// 				u2string sUnderWrite;
// 				sUnderWrite.resize(LEN_UNDER_WRITE+1, '\0');
// 				packet.readUTF16(&sUnderWrite[0], wDataSize/2);
// 				strncpy(pGlobalUserData->szUnderWrite, u2_8(&sUnderWrite[0]), countarray(pGlobalUserData->szUnderWrite));
// 				break;
// 			}
// 		}
// 	}

	//本地加载
	if (pGlobalUserData->dwCustomID!=0)
	{
		//CCustomFaceManager * pCustomFaceManager=CCustomFaceManager::GetInstance();
		//pCustomFaceManager->LoadUserCustomFace(pLogonSuccess->dwUserID,pLogonSuccess->dwCustomID,pGlobalUserData->CustomFaceInfo);
	}

	if (mIGPLoginMissionSink)
		mIGPLoginMissionSink->onGPLoginSuccess();

	return true;
}

//登录失败
bool CGPLoginMission::onSocketSubLogonFailure(void* data, int size)
{
	//TEST_NULL
	//效验数据
	ASSERT(size>=4);
	if (size<4) return false;

	//关闭连接
	stop();

	PACKET_AIDE_DATA(data);
	//错误代码
	int lResultColde = packet.read4Byte();
	
	//描述信息
	int len=(size-4)/2;
	u2string str;
	str.resize(len+1,'\0');
	packet.readUTF16(&str[0], len);

	//显示消息
	if (mIGPLoginMissionSink)
		mIGPLoginMissionSink->onGPLoginFailure(u2_8(&str[0]));
	return true;

	////变量定义
	//CMD_GP_LogonFailure * pLogonFailure=(CMD_GP_LogonFailure *)data;

	////效验数据
	//ASSERT(size>=(sizeof(CMD_GP_LogonFailure)-sizeof(pLogonFailure->szDescribeString)));
	//if (size<(sizeof(CMD_GP_LogonFailure)-sizeof(pLogonFailure->szDescribeString))) return false;
	//	
	////关闭处理
	//stop();

	////显示消息
	//if (mIGPLoginMissionSink)
	//	mIGPLoginMissionSink->onGPLoginFailure(pLogonFailure->szDescribeString);
	//return true;
}

//升级提示
bool CGPLoginMission::onSocketSubUpdateNotify(void* data, int size)
{
	//效验参数
	ASSERT(size>=6);
	if (size<6) return false;

	PACKET_AIDE_DATA(data);
	byte cbMustUpdate = packet.readByte();
	byte cbAdviceUpdate = packet.readByte();
	dword dwCurrentVersion = packet.read4Byte();

	u2string sURL;
	sURL.resize(260+1, '\0');

	while (size > packet.getPosition())
	{
		//提取数据
		word wDataSize = packet.read2Byte();
		word wDataDescribe = packet.read2Byte();

		if (wDataDescribe==0) break;

		////提取数据
		//switch (wDataDescribe)
		//{
		////case DTP_GP_URL_DOWNLOAD:	//下载路径
		////	{
		////		packet.readUTF16(&sURL[0], wDataSize/2);
		////		break;
		////	}		
		//default:
		//	break;
		//}
	}

	// 关闭连接升级
	stop();


	//提示消息
	if ((cbMustUpdate==TRUE)||(cbAdviceUpdate==TRUE))
	{
		//构造提示
		if (cbMustUpdate==FALSE)
		{
			//构造提示
			const char* pszString=SSTRING("System_Tips_20");

			if (mIGPLoginMissionSink && !mIGPLoginMissionSink->onGPUpdateNotify(0, pszString))
			{
				if (MISSION_LOGIN_ACCOUNT==mMissionType)
					mLoginAccount.cbValidateFlags = MB_VALIDATE_FLAGS;
				else 
					mLoginGameID.cbValidateFlags = MB_VALIDATE_FLAGS;
				// 执行登陆
				start();
				return true;
			}
		}
		else
		{
			//构造提示
			const char* pszString = SSTRING("System_Tips_21");
			//下载大厅
			return (mIGPLoginMissionSink && !mIGPLoginMissionSink->onGPUpdateNotify(1, pszString));
		}
	}

	return false;
}

//登录失败(密保卡)
bool CGPLoginMission::onSocketSubLogonValidateMBCard(void* data, int size)
{
	//PLAZZ_PRINTF(a_u8("登陆失败(密保卡)\n"));
	//效验参数
	ASSERT(size==4);
	if (size!=4) return false;

	PACKET_AIDE_DATA(data);
	uint uMBCardID = packet.read4Byte();

	////消息处理
	//CDLGMBCard DLGMBCard;
	//DLGMBCard.SetMBCard(pValidateMBCard->uMBCardID);
	//if (IDOK==DLGMBCard.DoModal())
	//{
	//	SendLogonPacket(0);
	//}
	//else
	{
		stop();
		////重新登录
		//if (m_bRegister==false)
		//{
		//	ShowLogon();
		//}
		//else
		//{
		//	ShowRegister();
		//}
	}

	return true;

	////效验参数
	//ASSERT(size==sizeof(CMD_GP_ValidateMBCard));
	//if (size!=sizeof(CMD_GP_ValidateMBCard)) return false;

	//////进度界面
	////HideStatusWindow();

	////消息处理
	//CMD_GP_ValidateMBCard * pValidateMBCard=(CMD_GP_ValidateMBCard *)data;

	//////消息处理
	////CDLGMBCard DLGMBCard;
	////DLGMBCard.SetMBCard(pValidateMBCard->uMBCardID);
	////if (IDOK==DLGMBCard.DoModal())
	////{
	////	SendLogonPacket(0);
	////}
	////else
	//{
	//	stop();

	//	////结束任务
	//	//GetMissionManager()->ConcludeMissionItem(this, true);

	//	////重新登录
	//	//if (m_bRegister==false)
	//	//{
	//	//	ShowLogon();
	//	//}
	//	//else
	//	//{
	//	//	ShowRegister();
	//	//}
	//}

	//return true;
}

//登录完成
bool CGPLoginMission::onSocketSubLogonFinish(void* data, int size)
{
	int baseSize = 4;
	//效验参数
	ASSERT(size==baseSize);
	if (size!=baseSize) return false;

	PACKET_AIDE_DATA(data);
	word wIntermitTime = packet.read2Byte();
	word wOnLineCountTime = packet.read2Byte();

	//变量定义
	CParameterGlobal * pParameterGlobal=CParameterGlobal::shared();

	//时间配置
	pParameterGlobal->m_wIntermitTime=wIntermitTime;
	pParameterGlobal->m_wOnLineCountTime=wOnLineCountTime;

	//变量定义
	//CGlobalUserInfo * pGlobalUserInfo=CGlobalUserInfo::GetInstance();
	//tagGlobalUserData * pGlobalUserData=pGlobalUserInfo->GetGlobalUserData();

	////网络加载
	//if ((pGlobalUserData->dwCustomID!=0)&&(pGlobalUserData->CustomFaceInfo.dwDataSize==0L))
	//{
	//	CCustomFaceManager * pCustomFaceManager=CCustomFaceManager::GetInstance();
	//	pCustomFaceManager->LoadUserCustomFace(pGlobalUserData->dwUserID,pGlobalUserData->dwCustomID);
	//}


	//关闭处理(获取完基本信息了)
	stop();

	if (mIGPLoginMissionSink)
		mIGPLoginMissionSink->onGPLoginComplete();

	return true;

	////效验参数
	//ASSERT(size==sizeof(CMD_GP_LogonFinish));
	//if (size!=sizeof(CMD_GP_LogonFinish)) return false;

	////变量定义
	//CMD_GP_LogonFinish * pLogonFinish=(CMD_GP_LogonFinish *)data;

	//////变量定义
	////CParameterGlobal * pParameterGlobal=CParameterGlobal::shared();

	//////时间配置
	////pParameterGlobal->m_wIntermitTime=pLogonFinish->wIntermitTime;
	////pParameterGlobal->m_wOnLineCountTime=pLogonFinish->wOnLineCountTime;


	////变量定义
	//CGlobalUserInfo * pGlobalUserInfo=CGlobalUserInfo::GetInstance();
	//tagGlobalUserData * pGlobalUserData=pGlobalUserInfo->GetGlobalUserData();

	//////网络加载
	////if ((pGlobalUserData->dwCustomID!=0)&&(pGlobalUserData->CustomFaceInfo.dwDataSize==0L))
	////{
	////	CCustomFaceManager * pCustomFaceManager=CCustomFaceManager::GetInstance();
	////	pCustomFaceManager->LoadUserCustomFace(pGlobalUserData->dwUserID,pGlobalUserData->dwCustomID);
	////}


	////关闭处理(获取完基本信息了)
	//stop();

	//if (mIGPLoginMissionSink)
	//	mIGPLoginMissionSink->onGPLoginComplete();
	//return true;
}

//////////////////////////////////////////////////////////////////////////
// 列表信息
bool CGPLoginMission::onSocketMainServerList(int sub, void* data, int size)
{
 	switch (sub)
	{
		case SUB_GP_LIST_TYPE:		return onSocketListType(data, size);
		case SUB_GP_LIST_KIND:		return onSocketListKind(data, size);
		case SUB_GP_LIST_NODE:		return onSocketListNode(data, size);
		case SUB_GP_LIST_PAGE:		return onSocketListPage(data, size);
		case SUB_GP_LIST_SERVER:	return onSocketListServer(data, size);
		//case SUB_GP_LIST_MATCH:		return onSocketListMatch(data, size);
		case SUB_GP_LIST_FINISH:	return onSocketListFinish(data, size);
		case SUB_GP_SERVER_FINISH:	return onSocketServerFinish(data, size);
		case SUB_GR_KINE_ONLINE:	return onSocketKindOnline(data, size);
		case SUB_GR_SERVER_ONLINE:	return onSocketServerOnline(data, size);
// 	case SUB_GR_ONLINE_FINISH:	return onSocketOnlineFinish(data, size);
// 	case SUB_GP_VIDEO_OPTION:	return onSocketVideoOption(data, size);
		default:
			return true;
	}

	return false;
}

//种类信息
bool CGPLoginMission::onSocketListType(void* data, int size)
{
	//PLAZZ_PRINTF(a_u8("种类信息\n"));
	int itemSize = 2+2+2+LEN_TYPE*2;
	//效验参数
	ASSERT(size%itemSize==0);
	if (size%itemSize!=0) return false;

	//变量定义
	int iItemCount=size/itemSize;
	PACKET_AIDE_DATA(data);

	//获取对象
	ASSERT(CServerListData::shared()!=0);
	CServerListData * pServerListData=CServerListData::shared();

	u2string sTypename;
	
	//更新数据
	for (int i=0;i<iItemCount;i++)
	{
		tagGameType GameType;
		GameType.wJoinID = packet.read2Byte();
		GameType.wSortID = packet.read2Byte();
		GameType.wTypeID = packet.read2Byte();

		sTypename.resize(LEN_TYPE+1, '\0');
		packet.readUTF16(&sTypename[0], LEN_TYPE);
		strncpy(GameType.szTypeName, u2_8(&sTypename[0]), countarray(GameType.szTypeName));
		log("%s id is%d", GameType.szTypeName, GameType.wTypeID);
		if (checkRoomTypeIdIsPass(GameType.wTypeID))
			pServerListData->InsertGameType(&GameType);
	}

	return true;
}

bool  CGPLoginMission::checkRoomTypeIdIsPass(int wTypeID)
{
	for (auto it : mAllowType)
	{
		if (it == wTypeID) return true;
	}
	return false;
}

//类型信息
bool CGPLoginMission::onSocketListKind(void* data, int size)
{
	//PLAZZ_PRINTF(a_u8("类型信息\n"));
	int itemSize=2+2+2+2+2+4+4+4+LEN_KIND*2+LEN_PROCESS*2;
	//效验参数
	ASSERT(size%itemSize==0);
	if (size%itemSize!=0) return false;

	//变量定义
	int iItemCount=size/itemSize;

	//获取对象
	ASSERT(CServerListData::shared()!=0);
	CServerListData * pServerListData=CServerListData::shared();

	PACKET_AIDE_DATA(data);

	u2string str;

	//更新数据
	for (int i=0;i<iItemCount;i++)
	{
		tagGameKind GameKind;
		GameKind.wTypeID = packet.read2Byte();
		GameKind.wJoinID = packet.read2Byte();
		GameKind.wSortID = packet.read2Byte();
		GameKind.wKindID = packet.read2Byte();
		GameKind.wGameID = packet.read2Byte();
		GameKind.dwOnLineCount = packet.read4Byte();
		GameKind.dwAndroidCount = packet.read4Byte();
		GameKind.dwFullCount = packet.read4Byte();

		str.resize(LEN_KIND+1, '\0');
		packet.readUTF16(&str[0], LEN_KIND);
		strncpy(GameKind.szKindName, u2_8(&str[0]), countarray(GameKind.szKindName));

		str.resize(LEN_PROCESS+1, '\0');
		packet.readUTF16(&str[0], LEN_PROCESS);
		strncpy(GameKind.szProcessName, u2_8(&str[0]), countarray(GameKind.szProcessName));
		pServerListData->InsertGameKind(&GameKind);
	}

	return true;

	////效验参数
	//ASSERT(size%sizeof(tagGameKind)==0);
	//if (size%sizeof(tagGameKind)!=0) return false;

	////变量定义
	//int iItemCount=size/sizeof(tagGameKind);
	//tagGameKind * pGameKind=(tagGameKind *)(data);

	////获取对象
	//ASSERT(CServerListData::shared()!=0);
	//CServerListData * pServerListData=CServerListData::shared();

	////更新数据
	//for (int i=0;i<iItemCount;i++)
	//{
	//	pServerListData->InsertGameKind(pGameKind++);
	//}

	//return true;
}

// 节点信息
bool CGPLoginMission::onSocketListNode(void* data, int size)
{
	//PLAZZ_PRINTF(a_u8("节点信息\n"));
	int itemSize=2+2+2+2+LEN_NODE*2;
	//效验参数
	ASSERT(size%itemSize==0);
	if (size%itemSize!=0) return false;

	//变量定义
	int iItemCount=size/itemSize;

	//获取对象
	ASSERT(CServerListData::shared()!=0);
	CServerListData * pServerListData=CServerListData::shared();

	PACKET_AIDE_DATA(data);
	u2string str;

	//更新数据
	for (int i=0;i<iItemCount;i++)
	{
		tagGameNode GameNode;
		GameNode.wKindID = packet.read2Byte();
		GameNode.wJoinID = packet.read2Byte();
		GameNode.wSortID = packet.read2Byte();
		GameNode.wNodeID = packet.read2Byte();

		str.resize(LEN_NODE+1, '\0');
		packet.readUTF16(&str[0], LEN_NODE);
		strncpy(GameNode.szNodeName, u2_8(&str[0]), countarray(GameNode.szNodeName));

		pServerListData->InsertGameNode(&GameNode);
	}

	return true;
	////效验参数
	//ASSERT(size%sizeof(tagGameNode)==0);
	//if (size%sizeof(tagGameNode)!=0) return false;

	////变量定义
	//int iItemCount=size/sizeof(tagGameNode);
	//tagGameNode * pGameNode=(tagGameNode *)(data);

	////获取对象
	//ASSERT(CServerListData::shared()!=0);
	//CServerListData * pServerListData=CServerListData::shared();

	////更新数据
	//for (int i=0;i<iItemCount;i++)
	//{
	//	pServerListData->InsertGameNode(pGameNode++);
	//}

	//return true;
}

// 页面信息
bool CGPLoginMission::onSocketListPage(void* data, int size)
{
	//PLAZZ_PRINTF(a_u8("页面信息\n"));
	int itemSize=2+2+2+2+2+LEN_PAGE*2;
	//效验参数
	ASSERT(size%itemSize==0);
	if (size%itemSize!=0) return false;

	//变量定义
	int iItemCount=size/itemSize;

	//获取对象
	ASSERT(CServerListData::shared()!=0);
	CServerListData * pServerListData=CServerListData::shared();

	PACKET_AIDE_DATA(data);
	u2string str;

	//更新数据
	for (int i=0;i<iItemCount;i++)
	{
		tagGamePage GamePage;
		GamePage.wPageID = packet.read2Byte();
		GamePage.wKindID = packet.read2Byte();
		GamePage.wNodeID = packet.read2Byte();
		GamePage.wSortID = packet.read2Byte();
		GamePage.wOperateType = packet.read2Byte();

		str.resize(LEN_PAGE+1, '\0');
		packet.readUTF16(&str[0], LEN_PAGE);
		strncpy(GamePage.szDisplayName, u2_8(&str[0]), countarray(GamePage.szDisplayName));

		pServerListData->InsertGamePage(&GamePage);
	}

	return true;
	////效验参数
	//ASSERT(size%sizeof(tagGamePage)==0);
	//if (size%sizeof(tagGamePage)!=0) return false;

	////变量定义
	//int iItemCount=size/sizeof(tagGamePage);
	//tagGamePage * pGamePage=(tagGamePage *)(data);

	////获取对象
	//ASSERT(CServerListData::shared()!=0);
	//CServerListData * pServerListData=CServerListData::shared();

	////更新数据
	//for (int i=0;i<iItemCount;i++)
	//{
	//	pServerListData->InsertGamePage(pGamePage++);
	//}

	//return true;
}

// 房间列表
bool CGPLoginMission::onSocketListServer(void* data, int size)
{
	//PLAZZ_PRINTF(a_u8("房间列表\n"));
	int itemSize = 2 + 2 + 2 + 2 + 2 + 2 + 2 + 8 + 8 + 4 + 4 + 4 + 4 + LEN_SERVERADDR * 2 + LEN_SERVER * 2;
	//效验参数
	ASSERT(size%itemSize==0);
	if (size%itemSize!=0) return false;

	//变量定义
	int iItemCount=size/itemSize;

	//获取对象
	ASSERT(CServerListData::shared()!=0);
	CServerListData * pServerListData=CServerListData::shared();

	PACKET_AIDE_DATA(data);
	u2string str;

	//更新数据
	for (int i=0;i<iItemCount;i++)
	{
		tagGameServer GameServer;

		GameServer.wKindID = packet.read2Byte();
		GameServer.wNodeID = packet.read2Byte();
		GameServer.wSortID = packet.read2Byte();
		GameServer.wServerID = packet.read2Byte();
		GameServer.wServerKind = packet.read2Byte();
		GameServer.wServerType = packet.read2Byte();
		GameServer.wServerPort = packet.read2Byte();
		GameServer.lCellScore = packet.read8Byte();
		GameServer.lEnterScore = packet.read8Byte();
		GameServer.dwServerRule = packet.read4Byte();
		GameServer.dwOnLineCount = packet.read4Byte();
		GameServer.dwAndroidCount = packet.read4Byte();
		GameServer.dwFullCount = packet.read4Byte();

		str.resize(LEN_SERVERADDR+1, '\0');
		packet.readUTF16(&str[0], LEN_SERVERADDR);
		strncpy(GameServer.szServerAddr, u2_8(&str[0]), countarray(GameServer.szServerAddr));

		str.resize(LEN_SERVER+1, '\0');
		packet.readUTF16(&str[0], LEN_SERVER);
		strncpy(GameServer.szServerName, u2_8(&str[0]), countarray(GameServer.szServerName));

 		pServerListData->InsertGameServer(&GameServer);
	}

	return true;
	////效验参数
	//ASSERT(size%sizeof(tagGameServer)==0);
	//if (size%sizeof(tagGameServer)!=0) return false;

	////变量定义
	//int iItemCount=size/sizeof(tagGameServer);
	//tagGameServer * pGameServer=(tagGameServer *)(data);

	////获取对象
	//ASSERT(CServerListData::shared()!=0);
	//CServerListData * pServerListData=CServerListData::shared();

	////更新数据
	//for (int i=0;i<iItemCount;i++)
	//{
	//	pServerListData->InsertGameServer(pGameServer++);
	//}

	//return true;
}

//比赛列表
bool CGPLoginMission::onSocketListMatch(void* data, int size)
{
	return true;
}

// 列表完成
bool CGPLoginMission::onSocketListFinish(void* data, int size)
{
	//PLAZZ_PRINTF(a_u8("列表完成\n"));
	ASSERT(CServerListData::shared()!=0);
	if (CServerListData::shared()!=0) 
	{
		CServerListData::shared()->OnEventListFinish();
	}
	return true;
}

//房间完成
bool CGPLoginMission::onSocketServerFinish(void* data, int size)
{
	//PLAZZ_PRINTF(a_u8("房间完成\n"));

	//效验数据
	ASSERT(size%sizeof(word)==0);
	if (size%sizeof(word)!=0) return false;

	//获取对象
	ASSERT(CServerListData::shared()!=0);
	CServerListData * pServerListData=CServerListData::shared();

	//任务处理
	for (uint i=0;i<(size/sizeof(word));i++)
	{
		uint16 kind = ((uint16 *)data)[i];
		mKindWaitList.remove(kind);
		pServerListData->OnEventKindFinish(kind);
	}

	stop();
	return true;
}

//////////////////////////////////////////////////////////////////////////
// 在线更新
//////////////////////////////////////////////////////////////////////////
//类型在线
bool CGPLoginMission::onSocketKindOnline(void* data, int size)
{
	//PLAZZ_PRINTF(a_u8("类型在线\n"));
	//变量定义
	PACKET_AIDE_DATA(data);
	word wKindCount = packet.read2Byte();
	//获取对象
	CServerListData * pServerListData=CServerListData::shared();

	//设置人数
	for (int i=0;i<wKindCount;i++)
	{
		word wKindID = packet.read2Byte();
		dword dwOnLineCount = packet.read4Byte();
		pServerListData->SetKindOnLineCount(wKindID,dwOnLineCount);
	}

	return true;

	////变量定义
	//CMD_GP_KindOnline * pKindOnline=(CMD_GP_KindOnline *)data;
	//word wHeadSize=(sizeof(CMD_GP_KindOnline)-sizeof(pKindOnline->OnLineInfoKind));

	////效验数据
	//ASSERT((size>=wHeadSize)&&(size==(wHeadSize+pKindOnline->wKindCount*sizeof(tagOnLineInfoKind))));
	//if ((size<wHeadSize)||(size!=(wHeadSize+pKindOnline->wKindCount*sizeof(tagOnLineInfoKind)))) return false;

	////获取对象
	//CServerListData * pServerListData=CServerListData::shared();

	////设置人数
	//for (int i=0;i<pKindOnline->wKindCount;i++)
	//{
	//	tagOnLineInfoKind * pOnLineInfoKind=&pKindOnline->OnLineInfoKind[i];
	//	pServerListData->SetKindOnLineCount(pOnLineInfoKind->wKindID,pOnLineInfoKind->dwOnLineCount);
	//}

	//return true;
}

//房间在线
bool CGPLoginMission::onSocketServerOnline(void* data, int size)
{
	//PLAZZ_PRINTF(a_u8("房间在线\n"));
	//变量定义
	PACKET_AIDE_DATA(data);
	word wServerCount=packet.read2Byte();

	//获取对象
	CServerListData * pServerListData=CServerListData::shared();

	//设置人数
	for (int i=0;i<wServerCount;i++)
	{
		word wServerID = packet.read2Byte();
		dword dwOnLineCount = packet.read4Byte();
		pServerListData->SetServerOnLineCount(wServerID,dwOnLineCount);
	}

	return true;

	////变量定义
	//CMD_GP_ServerOnline * pServerOnline=(CMD_GP_ServerOnline *)data;
	//word wHeadSize=(sizeof(CMD_GP_ServerOnline)-sizeof(pServerOnline->OnLineInfoServer));

	////效验数据
	//ASSERT((size>=wHeadSize)&&(size==(wHeadSize+pServerOnline->wServerCount*sizeof(tagOnLineInfoServer))));
	//if ((size<wHeadSize)||(size!=(wHeadSize+pServerOnline->wServerCount*sizeof(tagOnLineInfoServer)))) return false;

	////获取对象
	//CServerListData * pServerListData=CServerListData::shared();

	////设置人数
	//for (int i=0;i<pServerOnline->wServerCount;i++)
	//{
	//	tagOnLineInfoServer * pOnLineInfoServer=&pServerOnline->OnLineInfoServer[i];
	//	pServerListData->SetServerOnLineCount(pOnLineInfoServer->wServerID,pOnLineInfoServer->dwOnLineCount);
	//}

	//return true;
}

// 在线更新完成
bool CGPLoginMission::onSocketOnlineFinish(void* data, int size)
{
	//PLAZZ_PRINTF(a_u8("在线更新完成\n"));
	//获取对象
	CServerListData * pServerListData=CServerListData::shared();
	//完成通知
	pServerListData->SetServerOnLineFinish();
	stop();
	return true;
}

//////////////////////////////////////////////////////////////////////////

//视频配置
bool CGPLoginMission::onSocketVideoOption(void* data, int size)
{
	//PLAZZ_PRINTF(a_u8("视频配置\n"));
	//效验数据
	ASSERT(size==6);
	if(size!=6) return false;

	//变量定义
	PACKET_AIDE_DATA(data);
	word wAVServerPort = packet.read2Byte();
	dword dwAVServerAddr = packet.read4Byte();

	//转化地址
	byte * pClientAddr=(byte *)&dwAVServerAddr;
	PLAZZ_PRINTF(a_u8("onSocketVideoOption ip:%d.%d.%d.%d port:%d\n"), 
		pClientAddr[0],pClientAddr[1],pClientAddr[2],pClientAddr[3],
		wAVServerPort);
	////获取对像
	//CPlatformFrame *pPlatfromFrame=CPlatformFrame::GetInstance();
	//if(pPlatfromFrame==0) return false;

	//pPlatfromFrame->SetVideoOption(pAVServerOption->wAVServerPort,pAVServerOption->dwAVServerAddr);
	return true;

	////效验数据
	//ASSERT(size==sizeof(tagAVServerOption));
	//if(size!=sizeof(tagAVServerOption)) return false;

	////变量定义
	//tagAVServerOption *pAVServerOption=(tagAVServerOption*)data;

	////转化地址
	//tchar szClientAddr[16]=T_T("");
	//byte * pClientAddr=(byte *)&pAVServerOption->dwAVServerAddr;
	//_tPRINTF(T_T("ip:%d.%d.%d.%d port:%d\n"), 
	//	pClientAddr[0],pClientAddr[1],pClientAddr[2],pClientAddr[3],
	//	pAVServerOption->wAVServerPort);
	//////获取对像
	////CPlatformFrame *pPlatfromFrame=CPlatformFrame::GetInstance();
	////if(pPlatfromFrame==0) return false;

	////pPlatfromFrame->SetVideoOption(pAVServerOption->wAVServerPort,pAVServerOption->dwAVServerAddr);
	//return true;
}

//系统消息
bool CGPLoginMission::onSocketMainSystem(int sub, void* data, int size)
{
	switch (sub)
	{
		//系统消息
		case SUB_GP_SYSTEM_MESSAGE:
		{
									  //获取对象
									  CServerListData * pServerListData = CServerListData::shared();

									  PACKET_AIDE_DATA(data);
									  u2string str;

									  tagSystemMessage pSystemMessage;
									  word wType = packet.read2Byte();

									  if (wType == SMT_GP_ROLL)
									  {
										  pSystemMessage.dwMessageID = packet.read4Byte();
										  pSystemMessage.dwTimeRate = packet.read4Byte();

										  str.resize(1024 + 1, '\0');
										  packet.readUTF16(&str[0], 1024);
										  strncpy(pSystemMessage.szString, u2_8(&str[0]), countarray(pSystemMessage.szString));
									  }

									  //pServerListData->m_SystemMessageVector.push_back(&pSystemMessage);
									  pServerListData->InsertSystemMessage(&pSystemMessage);
									    
									  return true;
		}
		//系统消息接收完成
		case SUB_Gp_SYSTEM_MESSAGE_FINISH:
		{
											 CServerListData * pServerListData = CServerListData::shared();
											 pServerListData->OnEventSystemMessageFinish();
											 stop();
											 return true;
		}
	}
	return false;
}
