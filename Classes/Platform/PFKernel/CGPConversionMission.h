#ifndef _CGPConversionMission_H_
#define _CGPConversionMission_H_
#include "cocos2d.h"
#include "Platform/PlatformHeader.h"
#include "Kernel/network/CSocketMission.h"

class IGPConversionMissionSink
{
public:
	virtual ~IGPConversionMissionSink(){}
	virtual void onConversionQueryInfoResult(word wExchangeRate) {};
	virtual void onConversionResult(bool bSuccessed, SCORE lCurrIngot, const char* szDescription) {};
};

//////////////////////////////////////////////////////////////////////////
// 签到任务
//////////////////////////////////////////////////////////////////////////
class CGPConversionMission
	: public CSocketMission
{
public:
	CGPConversionMission(const char* url, int port);
	// 设置回调接口
	void setMissionSink(IGPConversionMissionSink* pIGPSignInMissionSink);
	//查询元宝兑换
	void query();
	//兑换
	void done(int64 lExchangeIngot);
	//////////////////////////////////////////////////////////////////////////
	// ISocketEngineSink
public:
	virtual void onEventTCPSocketLink();
	virtual void onEventTCPSocketShut();
	virtual void onEventTCPSocketError(int errorCode);
	virtual bool onEventTCPSocketRead(int main, int sub, void* data, int dataSize);

private:
	bool onSubQueryInfoResult(void* data, int size);
	bool onSubDoneResult(void* data, int size);

private:
	// 回调
	IGPConversionMissionSink* mIGPConversionMissionSink;

	// 任务类型
	uint8 mMissionType;
	int64 m_lExchangeIngot;
}; // CGPMessageMission

#endif // _CGPMessageMission_H_