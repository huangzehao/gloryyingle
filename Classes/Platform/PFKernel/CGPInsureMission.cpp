#include "CGPInsureMission.h"
#include "Tools/Dialog/NewDialog.h"
#include "Tools/tools/StringData.h"
#include "Tools/tools/PacketAide.h"
CGPInsureMission::CGPInsureMission(const char* url, int port)
: CSocketMission(url, port)
{
	mMissionType = 0;
	mIGPInsureMissionSink = 0;
}

void CGPInsureMission::setMissionSink(IGPInsureMissionSink* pIGPInsureMissionSink)
{
	mIGPInsureMissionSink = pIGPInsureMissionSink;
}

void CGPInsureMission::query()
{
	mMissionType = INSURE_MISSION_QUERY;
	start();
}

void CGPInsureMission::save(int64 score)
{
	mScore = score;
	mMissionType = INSURE_MISSION_SAVE;
	start();
}

void CGPInsureMission::take(int64 score, const char* szInsurePass)
{
	mScore = score;
	memcpy(mInsurePass, szInsurePass, sizeof(mInsurePass));
	mMissionType = INSURE_MISSION_TAKE;
	start();
}

void CGPInsureMission::transfer(int64 score, const char* szInsurePass, const char* szNickName, uint8 cbByNickName)
{
	mScore = score;
	mByNickName = cbByNickName;
	memcpy(mInsurePass, szInsurePass, sizeof(mInsurePass));
	memcpy(mNickName, szNickName, sizeof(mNickName));
	mMissionType = INSURE_MISSION_TRANSFER;
	start();
}

void CGPInsureMission::queryAccessRecord()
{
	mMissionType = INSURE_MISSION_QUERY_ACCESS;
	start();
}

void CGPInsureMission::queryTransfeRecord()
{
	mMissionType = INSURE_MISSION_QUERY_TRANSFER;
	start();
}

void CGPInsureMission::enable(const char* szLogonPass, const char* szInsurePass)
{
	memcpy(mLogonPass, szLogonPass, sizeof(mLogonPass));
	memcpy(mInsurePass, szInsurePass, sizeof(mInsurePass));
	mMissionType = INSURE_MISSION_ENABLE;
	start();
}

void CGPInsureMission::onEventTCPSocketLink()
{
	PLAZZ_PRINTF("CGPInsureMission::onEventTCPSocketLink\n");


	switch (mMissionType)
	{
		// 查询
	case INSURE_MISSION_QUERY:
	{
								 //TEST_NULL
								 //变量定义
								 CGlobalUserInfo * pGlobalUserInfo = CGlobalUserInfo::GetInstance();
								 tagGlobalUserData * pGlobalUserData = pGlobalUserInfo->GetGlobalUserData();

								 //打包数据
								 PACKET_AIDE_SIZE(512);
								 packet.write4Byte(pGlobalUserData->dwUserID);
								 packet.writeUTF16(u8_2(pGlobalUserData->szPassword), LEN_PASSWORD);

								 //发送数据
								 send(MDM_GP_USER_SERVICE, SUB_GP_QUERY_INSURE_INFO, packet.getBuffer(), packet.getPosition());

								 break;
	}
		// 存入
	case INSURE_MISSION_SAVE:
	{
								//TEST_NULL
								//变量定义
								CGlobalUserInfo * pGlobalUserInfo = CGlobalUserInfo::GetInstance();
								tagGlobalUserData * pGlobalUserData = pGlobalUserInfo->GetGlobalUserData();

								//打包数据
								PACKET_AIDE_SIZE(512);
								packet.write4Byte(pGlobalUserData->dwUserID);
								packet.write8Byte(mScore);
								packet.writeUTF16(u8_2(DF::shared()->GetMachineID()), LEN_MACHINE_ID);

								//发送数据
								send(MDM_GP_USER_SERVICE, SUB_GP_USER_SAVE_SCORE, packet.getBuffer(), packet.getPosition());

								break;
	}
		// 取款
	case INSURE_MISSION_TAKE:
	{
								//变量定义
								CGlobalUserInfo * pGlobalUserInfo = CGlobalUserInfo::GetInstance();
								tagGlobalUserData * pGlobalUserData = pGlobalUserInfo->GetGlobalUserData();

								//打包数据
								PACKET_AIDE_SIZE(512);
								packet.write4Byte(pGlobalUserData->dwUserID);
								packet.write8Byte(mScore);
								packet.writeUTF16(u8_2(DF::MD5Encrypt(mInsurePass)), LEN_PASSWORD);
								packet.writeUTF16(u8_2(DF::shared()->GetMachineID()), LEN_MACHINE_ID);

								//发送数据
								send(MDM_GP_USER_SERVICE, SUB_GP_USER_TAKE_SCORE, packet.getBuffer(), packet.getPosition());

								break;
	}
		// 转账
	case INSURE_MISSION_TRANSFER:
	{
									PACKET_AIDE_SIZE(128);
									packet.writeByte(0);
									packet.writeUTF16(u8_2(mNickName), LEN_NICKNAME);

									//发送数据
									send(MDM_GP_USER_SERVICE, SUB_GP_QUERY_USER_INFO_REQUEST, packet.getBuffer(), packet.getPosition());

									////变量定义
									//CMD_GP_QueryUserInfoRequest QueryUserInfoRequest;
									//zeromemory(&QueryUserInfoRequest,sizeof(QueryUserInfoRequest));

									////设置变量
									//QueryUserInfoRequest.cbByNickName=mByNickName;
									//tstrcpyn(QueryUserInfoRequest.szNickName,mNickName,countarray(QueryUserInfoRequest.szNickName));

									////发送数据
									//send(MDM_GP_USER_SERVICE,SUB_GP_QUERY_USER_INFO_REQUEST,&QueryUserInfoRequest,sizeof(QueryUserInfoRequest));
									break;
	}
	//查询存取记录 
	case  INSURE_MISSION_QUERY_ACCESS:
	{
										   CGlobalUserInfo * pGlobalUserInfo = CGlobalUserInfo::GetInstance();
										   tagGlobalUserData * pGlobalUserData = pGlobalUserInfo->GetGlobalUserData();
										   PACKET_AIDE_SIZE(512);
										   packet.write4Byte(pGlobalUserData->dwUserID);

										   //发送数据
										   send(MDM_GP_USER_SERVICE, SUB_GP_QUERY_ACCESSRECORD, packet.getBuffer(), packet.getPosition());

										   break;
	}
	//查询转账记录 
	case  INSURE_MISSION_QUERY_TRANSFER:
	{
										 CGlobalUserInfo * pGlobalUserInfo = CGlobalUserInfo::GetInstance();
										 tagGlobalUserData * pGlobalUserData = pGlobalUserInfo->GetGlobalUserData();
										 PACKET_AIDE_SIZE(512);
										 packet.write4Byte(pGlobalUserData->dwUserID);

										 //发送数据
										 send(MDM_GP_USER_SERVICE, SUB_GP_QUERY_TRANSRECORD, packet.getBuffer(), packet.getPosition());

										 break;
	}
		// 开通银行
	case INSURE_MISSION_ENABLE:
	{
								  //变量定义
								  CGlobalUserInfo * pGlobalUserInfo = CGlobalUserInfo::GetInstance();
								  tagGlobalUserData * pGlobalUserData = pGlobalUserInfo->GetGlobalUserData();

								  //打包数据
								  PACKET_AIDE_SIZE(512);
								  packet.write4Byte(pGlobalUserData->dwUserID);
								  packet.writeUTF16(u8_2(DF::MD5Encrypt(mLogonPass)), LEN_PASSWORD);
								  packet.writeUTF16(u8_2(DF::MD5Encrypt(mInsurePass)), LEN_PASSWORD);
								  packet.writeUTF16(u8_2(DF::shared()->GetMachineID()), LEN_MACHINE_ID);

								  send(MDM_GP_USER_SERVICE, SUB_GP_USER_ENABLE_INSURE, packet.getBuffer(), packet.getPosition());
								  break;
	}
	}
}

void CGPInsureMission::onEventTCPSocketShut()
{
	PLAZZ_PRINTF("CGPInsureMission::onEventTCPSocketShut\n");
}

void CGPInsureMission::onEventTCPSocketError(int errorCode)
{
	PLAZZ_PRINTF("CGPInsureMission::onEventTCPSocketError code[%d]\n", errorCode);
}

bool CGPInsureMission::onEventTCPSocketRead(int main, int sub, void* data, int size)
{
	PLAZZ_PRINTF("CGPInsureMission::onEventTCPSocketRead main:%d sub:%d size:%d\n", main, sub, size);
	if (main != MDM_GP_USER_SERVICE)
	{
		return false;
	}

	switch (sub)
	{
		//转账查询
	case SUB_GP_QUERY_USER_INFO_RESULT:		return onSubQueryInfoResult(data, size);
		//银行资料
	case SUB_GP_USER_INSURE_INFO:			return onSubInsureInfo(data, size);
		//银行成功
	case SUB_GP_USER_INSURE_SUCCESS:		return onSubInsureSuccess(data, size);
		//银行失败
	case SUB_GP_USER_INSURE_FAILURE:		return onSubInsureFailure(data, size);
		//操作成功
	case SUB_GP_OPERATE_SUCCESS:			return onSubOperateSuccess(data, size);
		//操作失败
	case SUB_GP_OPERATE_FAILURE:			return onSubOperateFailure(data, size);
		//开通结果
	case SUB_GP_USER_INSURE_ENABLE_RESULT:	return onSubInsureEnableResult(data, size);
		//使用记录结果
	case SUB_GP_USER_ACCESS_RECORD_RESULT:	return onAccessQueryInfo(data, size);
		//使用记录完成
	case SUB_GP_USER_ACCESS_RECORD_FINISH:	return onAccessEnd(data, size);
		//转账记录结果
	case SUB_GP_USER_TRANSFER_RECORD_RESULT:return onTransferQueryInfo(data, size);
		//转账记录完成
	case SUB_GP_USER_TRANSFER_RECORD_FINISH:return onTransferQueryEnd(data, size);
	}

	return false;
}

//转账查询
bool CGPInsureMission::onSubQueryInfoResult(void* data, int size)
{
	int baseSize = 1;
	//效验参数
	ASSERT(size >= baseSize);
	if (size < baseSize) return false;

	//变量定义
	PACKET_AIDE_DATA(data);

	dword dwTargetGameID = packet.read4Byte();
	u2string sNickname;
	sNickname.resize(LEN_NICKNAME + 1, '\0');
	packet.readUTF16(&sNickname[0], LEN_NICKNAME);

	//构造消息
	char szMessage[128] = { 0 };
	sprintf(szMessage, SSTRING("bank_query_info_1"), u2_8(&sNickname[0]), dwTargetGameID, mScore);

	if (mIGPInsureMissionSink)
	{

// 		if (mIGPInsureMissionSink->onInsureTransferConfirm(szMessage))
// 		{
// 			//变量定义
// 			CGlobalUserInfo * pGlobalUserInfo = CGlobalUserInfo::GetInstance();
// 			tagGlobalUserData * pGlobalUserData = pGlobalUserInfo->GetGlobalUserData();
// 
// 			PACKET_AIDE_SIZE(512);
// 			packet.write4Byte(pGlobalUserData->dwUserID);
// 			packet.write8Byte(mScore);
// 			packet.writeUTF16(u8_2(DF::MD5Encrypt(mInsurePass)), LEN_PASSWORD);
// 			packet.writeUTF16(sNickname, LEN_NICKNAME);
// 			packet.writeUTF16(u8_2(DF::shared()->GetMachineID()), LEN_MACHINE_ID);
// 			packet.writeUTF16(u8_2(mNickName), LEN_NICKNAME);			//转账备注（临时填写昵称）
// 
// 			//发送数据
// 			send(MDM_GP_USER_SERVICE, SUB_GP_USER_TRANSFER_SCORE, packet.getBuffer(), packet.getPosition());
// 			return true;
// 		}

		NewDialog::create(szMessage, NewDialog::AFFIRMANDCANCEL, [=]()
		{
			CGlobalUserInfo * pGlobalUserInfo = CGlobalUserInfo::GetInstance();
			tagGlobalUserData * pGlobalUserData = pGlobalUserInfo->GetGlobalUserData();

			PACKET_AIDE_SIZE(512);
			packet.write4Byte(pGlobalUserData->dwUserID);
			packet.write8Byte(mScore);
			packet.writeUTF16(u8_2(DF::MD5Encrypt(mInsurePass)), LEN_PASSWORD);
			packet.writeUTF16(sNickname, LEN_NICKNAME);
			packet.writeUTF16(u8_2(DF::shared()->GetMachineID()), LEN_MACHINE_ID);
			packet.writeUTF16(u8_2(mNickName), LEN_NICKNAME);			//转账备注（临时填写昵称）

			//发送数据
			CGPInsureMission::send(MDM_GP_USER_SERVICE, SUB_GP_USER_TRANSFER_SCORE, packet.getBuffer(), packet.getPosition());
			

		});
	}


		//关闭连接
		//stop();

	

	
	return true;

	////效验参数
	//ASSERT(size==sizeof(CMD_GP_UserTransferUserInfo));
	//if (size<sizeof(CMD_GP_UserTransferUserInfo)) return false;

	////变量定义
	//CMD_GP_UserTransferUserInfo * pTransferUserInfo=(CMD_GP_UserTransferUserInfo *)data;

	////构造消息
	//tchar szMessage[128]={0};
	//_sntprintf(szMessage, countarray(szMessage), T_T("您确定要给[%s], ID:%d 赠送%I64d 星豆吗?"), pTransferUserInfo->szNickName, pTransferUserInfo->dwTargetGameID, mScore);

	//if (mIGPInsureMissionSink)
	//{
	//	if (mIGPInsureMissionSink->onInsureTransferConfirm(szMessage))
	//	{
	//		////变量定义
	//		//CMD_GP_UserTransferScore UserTransferScore;
	//		//zeromemory(&UserTransferScore,sizeof(UserTransferScore));

	//		////变量定义
	//		//CGlobalUserInfo * pGlobalUserInfo=CGlobalUserInfo::GetInstance();
	//		//tagGlobalUserData * pGlobalUserData=pGlobalUserInfo->GetGlobalUserData();

	//		////设置变量
	//		//UserTransferScore.cbByNickName=mByNickName;
	//		//UserTransferScore.lTransferScore=mScore;
	//		//UserTransferScore.dwUserID=pGlobalUserData->dwUserID;
	//		//DF::shared()->GetMachineID(UserTransferScore.szMachineID);
	//		//DF::MD5Encrypt(mInsurePass,UserTransferScore.szPassword);
	//		//tstrcpyn(UserTransferScore.szNickName,mNickName,countarray(UserTransferScore.szNickName));
	//	
	//		////发送数据
	//		//send(MDM_GP_USER_SERVICE,SUB_GP_USER_TRANSFER_SCORE,&UserTransferScore,sizeof(UserTransferScore));
	//		return true;
	//	}
	//}

	////关闭连接
	//stop();
	//return true;
}

//银行资料
bool CGPInsureMission::onSubInsureInfo(void* data, int size)
{
	int baseSize = 1 + 2 + 2 + 2 + 2 + 8 + 8 + 8;
	//效验参数
	ASSERT(size >= baseSize);
	if (size < baseSize) return false;

	//变量定义
	CGlobalUserInfo * pGlobalUserInfo = CGlobalUserInfo::GetInstance();
	tagUserInsureInfo * pInsureInfo = pGlobalUserInfo->GetUserInsureInfo();
	tagGlobalUserData* pGlobalUserData = pGlobalUserInfo->GetGlobalUserData();

	PACKET_AIDE_DATA(data);

	pInsureInfo->cbEnjoinTransfer = packet.readByte();
	pInsureInfo->wRevenueTake = packet.read2Byte();
	pInsureInfo->wRevenueTransfer = packet.read2Byte();
	pInsureInfo->wRevenueTransferMember = packet.read2Byte();
	pInsureInfo->wServerID = packet.read2Byte();
	pInsureInfo->lUserScore = packet.read8Byte();
	pInsureInfo->lUserInsure = packet.read8Byte();
	pInsureInfo->lTransferPrerequisite = packet.read8Byte();

	pGlobalUserData->lUserScore = pInsureInfo->lUserScore;
	pGlobalUserData->lUserInsure = pInsureInfo->lUserInsure;
	//关闭连接
	stop();

	if (mIGPInsureMissionSink)
		mIGPInsureMissionSink->onInsureInfo();

	return true;

	////效验参数
	//ASSERT(size==sizeof(CMD_GP_UserInsureInfo));
	//if (size<sizeof(CMD_GP_UserInsureInfo)) return false;

	////变量定义
	//CMD_GP_UserInsureInfo * pUserInsureInfo=(CMD_GP_UserInsureInfo *)data;

	//CGlobalUserInfo * pGlobalUserInfo=CGlobalUserInfo::GetInstance();
	//tagUserInsureInfo * pInsureInfo=pGlobalUserInfo->GetUserInsureInfo();

	//pInsureInfo->wRevenueTake			= pUserInsureInfo->wRevenueTake;
	//pInsureInfo->wRevenueTransfer		= pUserInsureInfo->wRevenueTransfer;
	//pInsureInfo->wServerID				= pUserInsureInfo->wServerID;
	//pInsureInfo->lUserScore				= pUserInsureInfo->lUserScore;
	//pInsureInfo->lUserInsure			= pUserInsureInfo->lUserInsure;
	//pInsureInfo->lTransferPrerequisite	= pUserInsureInfo->lTransferPrerequisite;

	////关闭连接
	//stop();

	//if (mIGPInsureMissionSink)
	//	mIGPInsureMissionSink->onInsureInfo();
	//return true;
}

//银行成功
bool CGPInsureMission::onSubInsureSuccess(void* data, int size)
{
	int baseSize = 4 + 8 + 8;
	//效验参数
	ASSERT(size >= baseSize);
	if (size < baseSize) return false;

	//变量定义
	CGlobalUserInfo * pGlobalUserInfo = CGlobalUserInfo::GetInstance();
	tagGlobalUserData * pGlobalUserData = pGlobalUserInfo->GetGlobalUserData();
	tagUserInsureInfo * pInsureInfo = pGlobalUserInfo->GetUserInsureInfo();

	PACKET_AIDE_DATA(data);
	packet.read4Byte();
	//更新
	pInsureInfo->lUserScore = packet.read8Byte();
	pInsureInfo->lUserInsure = packet.read8Byte();
	pGlobalUserData->lUserScore = pInsureInfo->lUserScore;
	pGlobalUserData->lUserInsure = pInsureInfo->lUserInsure;

	int len = (size - baseSize) / 2;
	u2string sDescribe;
	sDescribe.resize(len + 1, '\0');
	packet.readUTF16(&sDescribe[0], len);

	//关闭连接
	stop();

	//显示消息
	if (mIGPInsureMissionSink)
		mIGPInsureMissionSink->onInsureSuccess(mMissionType, u2_8(&sDescribe[0]));
	return true;

	////效验参数
	//CMD_GP_UserInsureSuccess * pUserInsureSuccess=(CMD_GP_UserInsureSuccess *)data;
	//ASSERT(size>=(sizeof(CMD_GP_UserInsureSuccess)-sizeof(pUserInsureSuccess->szDescribeString)));
	//if (size<(sizeof(CMD_GP_UserInsureSuccess)-sizeof(pUserInsureSuccess->szDescribeString))) return false;

	////变量定义
	//CGlobalUserInfo * pGlobalUserInfo   = CGlobalUserInfo::GetInstance();
	//tagUserInsureInfo * pInsureInfo=pGlobalUserInfo->GetUserInsureInfo();

	////更新
	//pInsureInfo->lUserScore			= pUserInsureSuccess->lUserScore;
	//pInsureInfo->lUserInsure		= pUserInsureSuccess->lUserInsure;

	////关闭连接
	//stop();

	////显示消息
	//if (mIGPInsureMissionSink)
	//	mIGPInsureMissionSink->onInsureSuccess(mMissionType, pUserInsureSuccess->szDescribeString);
	//return true;
}

//银行失败
bool CGPInsureMission::onSubInsureFailure(void* data, int size)
{
	int baseSize = 4;
	//效验参数
	ASSERT(size >= baseSize);
	if (size < baseSize) return false;

	PACKET_AIDE_DATA(data);
	int lResultCode = packet.read4Byte();
	int len = (size - baseSize) / 2;
	u2string sDescribe;
	sDescribe.resize(len + 1, '\0');
	packet.readUTF16(&sDescribe[0], len);

	//关闭连接
	stop();

	//显示消息
	if (mIGPInsureMissionSink)
		mIGPInsureMissionSink->onInsureFailure(mMissionType, u2_8(&sDescribe[0]));
	return true;

	////效验参数
	//CMD_GP_UserInsureFailure * pUserInsureFailure=(CMD_GP_UserInsureFailure *)data;
	//ASSERT(size>=(sizeof(CMD_GP_UserInsureFailure)-sizeof(pUserInsureFailure->szDescribeString)));
	//if (size<(sizeof(CMD_GP_UserInsureFailure)-sizeof(pUserInsureFailure->szDescribeString))) 
	//	return false;

	////关闭连接
	//stop();

	////显示消息
	//if (mIGPInsureMissionSink)
	//	mIGPInsureMissionSink->onInsureFailure(mMissionType, pUserInsureFailure->szDescribeString);
	//return true;
}

//操作成功
bool CGPInsureMission::onSubOperateSuccess(void* data, int size)
{
	int baseSize = 4;
	//效验参数
	ASSERT(size >= baseSize);
	if (size < baseSize) return false;

	PACKET_AIDE_DATA(data);
	int lResultCode = packet.read4Byte();
	int len = (size - baseSize) / 2;
	u2string sDescribe;
	sDescribe.resize(len + 1, '\0');
	packet.readUTF16(&sDescribe[0], len);

	//关闭连接
	stop();

	//显示消息
	if (mIGPInsureMissionSink)
		mIGPInsureMissionSink->onInsureSuccess(mMissionType, u2_8(&sDescribe[0]));
	return true;

	////变量定义
	//CMD_GP_OperateSuccess * pOperateSuccess=(CMD_GP_OperateSuccess *)data;

	////效验数据
	//ASSERT(size>=(sizeof(CMD_GP_OperateSuccess)-sizeof(pOperateSuccess->szDescribeString)));
	//if (size<(sizeof(CMD_GP_OperateSuccess)-sizeof(pOperateSuccess->szDescribeString))) return false;

	////变量定义
	//CGlobalUserInfo * pGlobalUserInfo=CGlobalUserInfo::GetInstance();
	//tagGlobalUserData * pGlobalUserData=pGlobalUserInfo->GetGlobalUserData();

	////关闭连接
	//stop();

	////显示消息
	//if (mIGPInsureMissionSink)
	//	mIGPInsureMissionSink->onInsureSuccess(mMissionType, pOperateSuccess->szDescribeString);

	//return true;
}

// 操作失败
bool CGPInsureMission::onSubOperateFailure(void* data, int size)
{
	int baseSize = 4;
	//效验参数
	ASSERT(size >= baseSize);
	if (size < baseSize) return false;

	PACKET_AIDE_DATA(data);
	int lResultCode = packet.read4Byte();
	int len = (size - baseSize) / 2;
	u2string sDescribe;
	sDescribe.resize(len + 1, '\0');
	packet.readUTF16(&sDescribe[0], len);


	//关闭连接
	stop();

	//显示消息
	if (mIGPInsureMissionSink)
		mIGPInsureMissionSink->onInsureFailure(mMissionType, u2_8(&sDescribe[0]));
	return true;

	////效验参数
	//CMD_GP_OperateFailure * pOperateFailure=(CMD_GP_OperateFailure *)data;
	//ASSERT(size>=(sizeof(CMD_GP_OperateFailure)-sizeof(pOperateFailure->szDescribeString)));
	//if (size<(sizeof(CMD_GP_OperateFailure)-sizeof(pOperateFailure->szDescribeString))) return false;

	////关闭连接
	//stop();

	////显示消息
	//if (mIGPInsureMissionSink)
	//	mIGPInsureMissionSink->onInsureFailure(mMissionType, pOperateFailure->szDescribeString);
	//return true;
}

//开通结果
bool CGPInsureMission::onSubInsureEnableResult(void* data, int size)
{
	int baseSize = 1;
	//校验参数
	ASSERT(size >= baseSize);
	if (size < baseSize) return false;

	//变量定义
	CGlobalUserInfo* pGlobalUserInfo = CGlobalUserInfo::GetInstance();
	tagGlobalUserData * pGlobalUserData = pGlobalUserInfo->GetGlobalUserData();

	// 	//开通结果
	// 	struct CMD_GP_UserInsureEnableResult
	// 	{
	// 		BYTE              cbInsureEnabled;          //使能标识
	// 		TCHAR             szDescribeString[128];        //描述消息
	// 	};

	PACKET_AIDE_DATA(data);
	pGlobalUserData->cbInsureEnabled = packet.readByte();
	int len = (size - baseSize) / 2;
	u2string sDescribe;
	sDescribe.resize(len + 1, '\0');
	packet.readUTF16(&sDescribe[0], len);


	//关闭连接
	stop();

	//显示消息
	if (mIGPInsureMissionSink)
		mIGPInsureMissionSink->onInsureEnableResult(INSURE_MISSION_ENABLE, u2_8(&sDescribe[0]));

	return true;
}
//银行存取记录信息查询
bool CGPInsureMission::onAccessQueryInfo(void* data, int size)
{
	//操作记录
// 	struct tagUserAccessRecord
// 	{
// 		word                            wRecordID;                             //记录ID
// 		SCORE							lScore;									//转账金币数量
// 		byte							cbType;									//1：存， 2：取
// 		word                            wYear;									//时间
// 		word                            wMonth;
// 		word                            wDay;
// 		word                            wHour;
// 		word                            wMinute;
// 		word                            wSecond;
// 	};

	int itemSize = 23;
	//效验参数
	ASSERT(size%itemSize == 0);
	if (size%itemSize != 0) return false;

	//变量定义
	int iItemCount = size / itemSize;

	//变量定义
	CGlobalUserInfo * pGlobalUserInfo = CGlobalUserInfo::GetInstance();
	PACKET_AIDE_DATA(data);

	for (int i = 0; i < iItemCount; i++)
	{
		tagUserAccessRecord * pUserAccessRecord = pGlobalUserInfo->GetrAccessUserData();
		pUserAccessRecord->wRecordID = packet.read2Byte();
		pUserAccessRecord->lScore = packet.read8Byte();
		pUserAccessRecord->cbType = packet.readByte();
		pUserAccessRecord->wYear = packet.read2Byte();
		pUserAccessRecord->wMonth = packet.read2Byte();
		pUserAccessRecord->wDay = packet.read2Byte();
		pUserAccessRecord->wHour = packet.read2Byte();
		pUserAccessRecord->wMinute = packet.read2Byte();
		pUserAccessRecord->wSecond = packet.read2Byte();

		if (mIGPInsureMissionSink)
			mIGPInsureMissionSink->onAccessRecord(pUserAccessRecord);
	}
	return true;
}
//银行存取记录信息查询完毕
bool CGPInsureMission::onAccessEnd(void* data, int size)
{
	if (mIGPInsureMissionSink)
		mIGPInsureMissionSink->onAccessRecordEnd();
	//关闭连接
	stop();

	return true;
}
//银行转账记录信息查询
bool CGPInsureMission::onTransferQueryInfo(void* data, int size)
{
	//银行转账记录
// 	struct tagUserTransferRecord
// 	{
// 		word                            wRecordID;                              //记录ID
// 		dword                           dwSourceGameID;                         //赠送用户ID
// 		dword                           dwTargetGameID;                         //获赠用户ID
// #if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID || CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
// 		tchar                           szSourceNickName[LEN_NICKNAME * 2];     //赠送用户昵称
// 		tchar                           szTargetNickName[LEN_NICKNAME * 2];     //获赠用户昵称
// #elif (CC_TARGET_PLATFORM == CC_PLATFORM_WIN32)
// 		tchar                           szSourceNickName[LEN_NICKNAME];         //赠送用户昵称
// 		tchar                           szTargetNickName[LEN_NICKNAME];         //获赠用户昵称
// #endif
// 		SCORE							lScore;									//转账金币数量
// 		word                            wYear;									//赠送时间
// 		word                            wMonth;
// 		word                            wDay;
// 		word                            wHour;
// 		word                            wMinute;
// 		word                            wSecond;
// 	};

	int itemSize = 158;
	//效验参数
	ASSERT(size%itemSize == 0);
	if (size%itemSize != 0) return false;

	//变量定义
	int iItemCount = size / itemSize;

	//变量定义
	CGlobalUserInfo * pGlobalUserInfo = CGlobalUserInfo::GetInstance();
	PACKET_AIDE_DATA(data);

	for (int i = 0; i < iItemCount; i++)
	{
		tagUserTransferRecord * pUserAccessRecord = pGlobalUserInfo->GetTransferUserData();
		pUserAccessRecord->wRecordID = packet.read2Byte();
		pUserAccessRecord->dwSourceGameID = packet.read4Byte();
		pUserAccessRecord->dwTargetGameID = packet.read4Byte();

		//赠送用户昵称、获赠用户昵称
		u2string szSourceNickName;
		u2string szTargetNickName;
		szSourceNickName.resize(LEN_NICKNAME + 1, '\0');
		szTargetNickName.resize(LEN_NICKNAME + 1, '\0');
		packet.readUTF16(&szSourceNickName[0], LEN_NICKNAME);
		packet.readUTF16(&szTargetNickName[0], LEN_NICKNAME);
		strncpy(pUserAccessRecord->szSourceNickName, u2_8(&szSourceNickName[0]), countarray(pUserAccessRecord->szSourceNickName));
		strncpy(pUserAccessRecord->szTargetNickName, u2_8(&szTargetNickName[0]), countarray(pUserAccessRecord->szTargetNickName));

		pUserAccessRecord->lScore = packet.read8Byte();
		pUserAccessRecord->wYear = packet.read2Byte();
		pUserAccessRecord->wMonth = packet.read2Byte();
		pUserAccessRecord->wDay = packet.read2Byte();
		pUserAccessRecord->wHour = packet.read2Byte();
		pUserAccessRecord->wMinute = packet.read2Byte();
		pUserAccessRecord->wSecond = packet.read2Byte();

		if (mIGPInsureMissionSink)
			mIGPInsureMissionSink->onTransferRecord(pUserAccessRecord);
	}
	return true;
}
//银行转账记录信息查询完毕
bool CGPInsureMission::onTransferQueryEnd(void* data, int size)
{
	if (mIGPInsureMissionSink)
		mIGPInsureMissionSink->onTransferRecordEnd();

	//关闭连接
	stop();

	return true;
}
