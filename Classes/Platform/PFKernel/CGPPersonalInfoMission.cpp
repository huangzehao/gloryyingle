#include "CGPPersonalInfoMission.h"
#include "Tools/tools/PacketAide.h"
CGPPersonalInfoMission::CGPPersonalInfoMission(const char* url, int port)
: CSocketMission(url, port)
{
	mMissionType = MISSION_LEVEL;
	mIGPPersonalInfoMissionSink = 0;
	need_request_lvInfo = true;
}

// 设置回调接口
void CGPPersonalInfoMission::setMissionSink(IGPPersonalInfoMissionSink* pIGPPersonalInfoMissionSink)
{
	mIGPPersonalInfoMissionSink = pIGPPersonalInfoMissionSink;
}

void CGPPersonalInfoMission::requestLevelInfo()
{
	if (need_request_lvInfo){
		mMissionType = MISSION_LEVEL;
		start();
	}
	else{
		if (mIGPPersonalInfoMissionSink){

			CGlobalUserInfo *pCGlobalUserInfo = CGlobalUserInfo::GetInstance();
			tagGrowLevelParameter * param_ = pCGlobalUserInfo->GetUserGrowLevelParameter();

			mIGPPersonalInfoMissionSink->onQueryLevelUpTipsSuccess(param_->wCurrLevelID, param_->dwExperience, param_->dwUpgradeExperience, param_->lUpgradeRewardGold, param_->lUpgradeRewardIngot);
		}
	}	
}


void CGPPersonalInfoMission::requesttPersionInfo()
{
	mMissionType = MISSION_PERSON_INFO;
	start();
}


void CGPPersonalInfoMission::onEventTCPSocketLink()
{
	PLAZZ_PRINTF("CGPPasswordMission::onEventTCPSocketLink\n");


	switch (mMissionType)
	{
		// 登陆密码
	case MISSION_LEVEL:
	{
									//变量定义
 									CGlobalUserInfo *pGlobalUserInfo = CGlobalUserInfo::GetInstance();
 									tagGlobalUserData * pGlobalUserData = pGlobalUserInfo->GetGlobalUserData();
 
 									//变量定义
 									PACKET_AIDE_SIZE(SIZE_PACK_DATA);
 									packet.write4Byte(pGlobalUserData->dwUserID);
 									packet.writeUTF16(u8_2(pGlobalUserData->szPassword), LEN_PASSWORD);
 									packet.writeUTF16(u8_2(DF::shared()->GetMachineID()), LEN_MACHINE_ID);
 									send(MDM_GP_USER_SERVICE, SUB_GP_GROWLEVEL_QUERY, packet.getBuffer(), packet.getPosition());

								   break;
	}
		// 银行密码
	case MISSION_NOTICE:
	{
									//变量定义
// 									CGlobalUserInfo * pGlobalUserInfo = CGlobalUserInfo::GetInstance();
// 									tagGlobalUserData * pGlobalUserData = pGlobalUserInfo->GetGlobalUserData();
// 
// 									//变量定义
// 									PACKET_AIDE_SIZE(512);
// 									packet.write4Byte(pGlobalUserData->dwUserID);
// 									packet.writeUTF16(u8_2(DF::MD5Encrypt(mDstInsure)), LEN_PASSWORD);
// 									packet.writeUTF16(u8_2(DF::MD5Encrypt(mSrcInsure)), LEN_PASSWORD);
// 
// 									//发送数据
// 									send(MDM_GP_USER_SERVICE, SUB_GP_MODIFY_INSURE_PASS, packet.getBuffer(), packet.getPosition());

									////变量定义
									//CGlobalUserInfo * pGlobalUserInfo=CGlobalUserInfo::GetInstance();
									//tagGlobalUserData * pGlobalUserData=pGlobalUserInfo->GetGlobalUserData();

									////变量定义
									//CMD_GP_ModifyInsurePass ModifyInsurePass;
									//zeromemory(&ModifyInsurePass,sizeof(ModifyInsurePass));

									////加密密码
									//tchar szSrcPassword[LEN_PASSWORD]=T_T("");
									//tchar szDesPassword[LEN_PASSWORD]=T_T("");
									//DF::MD5Encrypt(mSrcInsure,szSrcPassword);
									//DF::MD5Encrypt(mDstInsure,szDesPassword);

									////构造数据
									//ModifyInsurePass.dwUserID=pGlobalUserData->dwUserID;
									//tstrcpyn(ModifyInsurePass.szScrPassword,szSrcPassword,countarray(ModifyInsurePass.szScrPassword));
									//tstrcpyn(ModifyInsurePass.szDesPassword,szDesPassword,countarray(ModifyInsurePass.szDesPassword));

									////发送数据
									//send(MDM_GP_USER_SERVICE,SUB_GP_MODIFY_INSURE_PASS,&ModifyInsurePass,sizeof(ModifyInsurePass));
									break;
	}
	case MISSION_PERSON_INFO:
		//变量定义
		CGlobalUserInfo *pGlobalUserInfo = CGlobalUserInfo::GetInstance();
		tagGlobalUserData * pGlobalUserData = pGlobalUserInfo->GetGlobalUserData();

		//变量定义
		PACKET_AIDE_SIZE(SIZE_PACK_DATA);
		packet.write4Byte(pGlobalUserData->dwUserID);
		packet.writeUTF16(u8_2(pGlobalUserData->szPassword), LEN_PASSWORD);
		packet.writeUTF16(u8_2(DF::shared()->GetMachineID()), LEN_MACHINE_ID);
		send(MDM_GP_USER_SERVICE, SUB_GP_GROWLEVEL_QUERY, packet.getBuffer(), packet.getPosition());
		break;
	}

}

void CGPPersonalInfoMission::onEventTCPSocketShut()
{
	PLAZZ_PRINTF("CGPPersonalInfoMission::onEventTCPSocketShut\n");
}

void CGPPersonalInfoMission::onEventTCPSocketError(int errorCode)
{
	PLAZZ_PRINTF("CGPPersonalInfoMission::onEventTCPSocketError code[%d]\n", errorCode);
}

bool CGPPersonalInfoMission::onEventTCPSocketRead(int main, int sub, void* data, int size)
{
	PLAZZ_PRINTF("CGPPasswordMission::onEventTCPSocketRead main:%d sub:%d size:%d\n", main, sub, size);
	if (main != MDM_GP_USER_SERVICE)
	{
		return false;
	}

	switch (sub)
	{
		//操作成功
	case SUB_GP_GROWLEVEL_QUERY:	return onQueryLevelInfoSuccess(data, size);
		//操作失败
	case SUB_GP_GROWLEVEL_PARAMETER:	return onQueryLevelUpTipsSuccess(data, size);
	}

	return false;
}

bool CGPPersonalInfoMission::onQueryLevelInfoSuccess(void *data, int size_)
{
// 	if (mIGPPersonalInfoMissionSink){
// 		mIGPPersonalInfoMissionSink->onGPUpdateSuccess(data, size_);
// 	}
		return true;
}

bool CGPPersonalInfoMission::onQueryLevelUpTipsSuccess(void *data, int size_)
{
// 	struct tagGrowLevelParameter
// 	{
// 		word              wCurrLevelID;           //当前等级
// 		dword             dwExperience;           //当前经验
// 		dword             dwUpgradeExperience;        //下级经验
// 		SCORE             lUpgradeRewardGold;         //升级奖励
// 		SCORE             lUpgradeRewardIngot;        //升级奖励
// 	};

	need_request_lvInfo = false;

	PACKET_AIDE_DATA(data);
	int cur_lv_ = packet.read2Byte();
	dword cur_exp_ = packet.read4Byte();
	dword next_exp_ = packet.read4Byte();
	long long reaward_gold_ = packet.read8Byte();
	long long reaward_ingot_ = packet.read8Byte();

	CGlobalUserInfo *pCGlobalUserInfo = CGlobalUserInfo::GetInstance();

	tagGrowLevelParameter * param_ = pCGlobalUserInfo->GetUserGrowLevelParameter();

	param_->wCurrLevelID = cur_lv_;
	param_->dwUpgradeExperience = next_exp_;
	param_->dwExperience = cur_exp_;
	param_->lUpgradeRewardIngot = reaward_ingot_;
	param_->lUpgradeRewardGold = reaward_gold_;

	if (mIGPPersonalInfoMissionSink){
		mIGPPersonalInfoMissionSink->onQueryLevelUpTipsSuccess(cur_lv_, cur_exp_, next_exp_, reaward_gold_, reaward_ingot_);
	}

	return true;
}

// 操作成功
// bool CGPPersonalInfoMission::onSubOperateSuccess(void* data, int size)
// {
// 	//效验数据
// 	ASSERT(size >= 4);
// 	if (size < 4) return false;
// 
// 	PACKET_AIDE_DATA(data);
// 	int lResultColde = packet.read4Byte();
// 	int len = (size - 4) / 2;
// 	u2string str;
// 	str.resize(len + 1, '\0');
// 	packet.readUTF16(&str[0], len);
// 
// 	//关闭连接
// 	stop();
// 
// 	//变量定义
// 	//CGlobalUserInfo * pGlobalUserInfo=CGlobalUserInfo::GetInstance();
// 	//tagGlobalUserData * pGlobalUserData=pGlobalUserInfo->GetGlobalUserData();
// 
// 	switch (mMissionType)
// 	{
// 		// 登陆密码
// 	case MISSION_PASSWORD_LOGIN:
// 	{
// 								   ////变量定义
// 								   //CGlobalUserInfo * pGlobalUserInfo=CGlobalUserInfo::GetInstance();
// 								   //tagGlobalUserData * pGlobalUserData=pGlobalUserInfo->GetGlobalUserData();
// 
// 								   ////密码资料
// 								   //tchar szDesPassword[LEN_PASSWORD]=T_T("");
// 								   //DF::MD5Encrypt(mDstLogin,szDesPassword);
// 								   //tstrcpyn(pGlobalUserData->szPassword,szDesPassword,countarray(pGlobalUserData->szPassword));
// 								   break;
// 	}
// 		// 银行密码
// 	case MISSION_PASSWORD_INSURE:
// 	{
// 									break;
// 	}
// 	}
// 
// 	//显示消息
// 	if (mIGPPersonalInfoMissionSink)
// 		mIGPPersonalInfoMissionSink->onGPUpdateSuccess(mMissionType, u2_8(&str[0]));
// 	return true;
// }
// 
// // 操作失败
// bool CGPPersonalInfoMission::onSubOperateFailure(void* data, int size)
// {
// 	//TEST_NULL
// 	//效验数据
// 	ASSERT(size >= 4);
// 	if (size < 4) return false;
// 
// 	PACKET_AIDE_DATA(data);
// 	int lResultColde = packet.read4Byte();
// 	int len = (size - 4) / 2;
// 	u2string str;
// 	str.resize(len + 1, '\0');
// 	packet.readUTF16(&str[0], len);
// 
// 	//关闭连接
// 	stop();
// 
// 	//显示消息
// 	if (mIGPPersonalInfoMissionSink)
// 		mIGPPersonalInfoMissionSink->onGPUpdateFailure(mMissionType, u2_8(&str[0]));
// 	return true;
// 
// 	////效验参数
// 	//CMD_GP_OperateFailure * pOperateFailure=(CMD_GP_OperateFailure *)data;
// 	//ASSERT(size>=(sizeof(CMD_GP_OperateFailure)-sizeof(pOperateFailure->szDescribeString)));
// 	//if (size<(sizeof(CMD_GP_OperateFailure)-sizeof(pOperateFailure->szDescribeString))) return false;
// 
// 	////关闭连接
// 	//stop();
// 
// 	////显示消息
// 	//if (mIGPPasswordMissionSink)
// 	//	mIGPPasswordMissionSink->onGPPasswordFailure(mMissionType, pOperateFailure->szDescribeString);
// 	//return true;
// }