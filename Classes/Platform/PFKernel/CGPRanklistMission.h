#ifndef _CGPRanklistMission_H_
#define _CGPRanklistMission_H_
#include "cocos2d.h"
#include "Platform/PlatformHeader.h"
#include "Kernel/network/CSocketMission.h"

///< 回调
class ICGPRanklistMissionLink
{
public:
	virtual ~ICGPRanklistMissionLink(){}
	virtual void onAddRank(tagUserRankingResult * pUserRankingResult){}			//添加排行记录
	virtual void onAddRankEnd(){}												//添加排行记录结束
};

class CGPRanklistMission : public CSocketMission
{
public:
	CGPRanklistMission(const char * url, int port);
	// 设置回调接口
	void setMissionSink(ICGPRanklistMissionLink* pIGPLowProtectMissionSink);
	///< 请求查询财富排行
	void queryRank();
	// ISocketEngineSink//这个是虚方法
public:
	virtual void onEventTCPSocketLink();
	virtual void onEventTCPSocketShut();
	virtual void onEventTCPSocketError(int errorCode);
	virtual bool onEventTCPSocketRead(int main, int sub, void* data, int dataSize);

private:
	//排行信息查询
	bool onRankQueryInfo(void * data, int size);
private:
	// 签到类型
	uint8 mMissionType;	

private:
	// 回调
	ICGPRanklistMissionLink* mIGPLowProtectMissionSink;
};
#endif