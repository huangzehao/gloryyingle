#include "CGPExchangeMission.h"
#include "Tools/tools/PacketAide.h"

CGPExchangeMission::CGPExchangeMission(const char* url, int port)
: CSocketMission(url, port)
{
	mIGPExchangeMissionSink = 0;
}

// 设置回调接口
void CGPExchangeMission::setMissionSink(IGPExchangeMissionSink* pIGPExchangeMissionSink)
{
	mIGPExchangeMissionSink = pIGPExchangeMissionSink;
}

void CGPExchangeMission::exchange(uint16 item)
{
	mItem = item;
	start();
}

void CGPExchangeMission::onEventTCPSocketLink()
{
	PLAZZ_PRINTF("CGPExchangeMission::onEventTCPSocketLink\n");

	//变量定义
	CGlobalUserInfo * pGlobalUserInfo=CGlobalUserInfo::GetInstance();
	tagGlobalUserData * pGlobalUserData=pGlobalUserInfo->GetGlobalUserData();

	//变量定义
	PACKET_AIDE_SIZE(512);
	packet.write4Byte(pGlobalUserData->dwUserID);
	packet.write2Byte(mItem);
	packet.writeUTF16(u8_2(pGlobalUserData->szNickName), LEN_NICKNAME);
			
	//发送数据
	send(MDM_GP_USER_SERVICE,SUB_GP_EXCHANGE, packet.getBuffer(), packet.getPosition());
}

void CGPExchangeMission::onEventTCPSocketShut()
{
	PLAZZ_PRINTF("CGPExchangeMission::onEventTCPSocketShut\n");
}

void CGPExchangeMission::onEventTCPSocketError(int errorCode)
{
	PLAZZ_PRINTF("CGPExchangeMission::onEventTCPSocketError code[%d]\n", errorCode);
}

bool CGPExchangeMission::onEventTCPSocketRead(int main, int sub, void* data, int size) 
{
	PLAZZ_PRINTF("CGPExchangeMission::onEventTCPSocketRead main:%d sub:%d size:%d\n", main, sub, size);
	if (main != MDM_GP_USER_SERVICE)
	{
		return false;
	}

	switch (sub)
	{
		//兑换结果
	case SUB_GP_EXCHANGE_RESULT:	return onSubExchangeSuccess(data, size);
	}

	return false;
}

// 兑换结果
bool CGPExchangeMission::onSubExchangeSuccess(void* data, int size)
{
	//效验数据
	ASSERT(size>=10);
	if (size<10) return false;

	PACKET_AIDE_DATA(data);
	uint32 dwUserID = packet.read4Byte();
	int wItemID = packet.read2Byte();
	int lResultCode = packet.read4Byte();
	int len = (size-10)/2;
	u2string str;
	str.resize(len+1,'\0');
	packet.readUTF16(&str[0], len);

	//关闭连接
	stop();

	//显示消息
	if (mIGPExchangeMissionSink)
		mIGPExchangeMissionSink->onGPExchangeResult(lResultCode, u2_8(&str[0]));
	return true;
}
