#ifndef _CGPSignInMission_H_
#define _CGPSignInMission_H_
#include "cocos2d.h"
#include "Platform/PlatformHeader.h"
#include "Kernel/network/CSocketMission.h"

class IGPSignInMissionSink
{
public:
	virtual ~IGPSignInMissionSink(){}
	virtual void onSignInQueryInfoResult(word wSeriesDate, bool bTodayChecked, SCORE lRewardGold[7]) {};
	virtual void onSignInDoneResult(bool bSuccessed, SCORE lScore, const char* szDescription) {};
};

//////////////////////////////////////////////////////////////////////////
// 签到任务
//////////////////////////////////////////////////////////////////////////
class CGPSignInMission
	: public CSocketMission
{
public:
	CGPSignInMission(const char* url, int port);
	// 设置回调接口
	void setMissionSink(IGPSignInMissionSink* pIGPSignInMissionSink);
	//查询签到
	void query();
	//签到
	void done();
	//////////////////////////////////////////////////////////////////////////
	// ISocketEngineSink
public:
	virtual void onEventTCPSocketLink();
	virtual void onEventTCPSocketShut();
	virtual void onEventTCPSocketError(int errorCode);
	virtual bool onEventTCPSocketRead(int main, int sub, void* data, int dataSize);

private:
	bool onSubQueryInfoResult(void* data, int size);
	bool onSubDoneResult(void* data, int size);

private:
	// 回调
	IGPSignInMissionSink* mIGPSignInMissionSink;

	// 任务类型
	uint8 mMissionType;
}; // CGPMessageMission

#endif // _CGPMessageMission_H_