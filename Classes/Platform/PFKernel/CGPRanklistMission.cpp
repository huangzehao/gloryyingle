#include "CGPRanklistMission.h"
#include "Tools/tools/PacketAide.h"
#define MISSION_REQUERK_RANK_NULL	0
#define MISSION_REQUICK_RANK_QUERY  1

CGPRanklistMission::CGPRanklistMission(const char * url, int port)
: CSocketMission(url, port)
{
	mIGPLowProtectMissionSink = 0;
	mMissionType = MISSION_REQUERK_RANK_NULL;
}

void CGPRanklistMission::setMissionSink(ICGPRanklistMissionLink* pIGPLowProtectMissionSink)
{
	mIGPLowProtectMissionSink = pIGPLowProtectMissionSink;
}

void CGPRanklistMission::queryRank()
{
	mMissionType = MISSION_REQUICK_RANK_QUERY;
	start();
}

void CGPRanklistMission::onEventTCPSocketLink()
{
	switch (mMissionType)
	{
	case MISSION_REQUICK_RANK_QUERY:
		{
			 send(MDM_GP_USER_SERVICE, SUB_GP_QUERY_SCORERANKING, NULL, 0);
			 break;
		}
		default:
		break;
	}
}

void CGPRanklistMission::onEventTCPSocketShut()
{
	PLAZZ_PRINTF("CGPSignInMission::onEventTCPSocketShut\n");
}

void CGPRanklistMission::onEventTCPSocketError(int errorCode)
{
	PLAZZ_PRINTF("CGPSignInMission::onEventTCPSocketError code[%d]\n", errorCode);
}

bool CGPRanklistMission::onEventTCPSocketRead(int main, int sub, void* data, int dataSize)
{
	PLAZZ_PRINTF("CGPSignInMission::onEventTCPSocketRead main:%d sub:%d size:%d\n", main, sub, dataSize);

	if (main != MDM_GP_USER_SERVICE)
	{
		return false;
	}
	switch (sub)
	{
		case SUB_GP_SCORERANKING_RESULT:
		{
			return onRankQueryInfo(data, dataSize);
		}
		default:
		break;
	}
	return true;
}

bool CGPRanklistMission::onRankQueryInfo(void * data, int size)
{
// 	struct CMD_GP_ScoreRankingResult
// 	{
// 		WORD              wFaceID;											//头像标识
// 		SCORE             lUserInsure;										//用户银行
// 		TCHAR             szNickName[LEN_ACCOUNTS];							//用户昵称
// 	};
	int itemSize = 74;
	//效验参数
	ASSERT(size%itemSize == 0);
	if (size%itemSize != 0) return false;

	//变量定义
	int iItemCount = size / itemSize;

	//变量定义
	CGlobalUserInfo * pGlobalUserInfo = CGlobalUserInfo::GetInstance();
	PACKET_AIDE_DATA(data);

	for (int i = 0; i < iItemCount; i++)
	{
		tagUserRankingResult * pUserRankingResult = pGlobalUserInfo->GetRankingResult();
		pUserRankingResult->wFaceID = packet.read2Byte();
		pUserRankingResult->lUserInsure = packet.read8Byte();

		u2string szTargetNickName;
		szTargetNickName.resize(LEN_NICKNAME + 1, '\0');
		packet.readUTF16(&szTargetNickName[0], LEN_NICKNAME);
		strncpy(pUserRankingResult->szNickName, u2_8(&szTargetNickName[0]), countarray(pUserRankingResult->szNickName));
		PLAZZ_PRINTF("%s\n", pUserRankingResult->szNickName);

		if (mIGPLowProtectMissionSink)
			mIGPLowProtectMissionSink->onAddRank(pUserRankingResult);
	}

	if (mIGPLowProtectMissionSink)
		mIGPLowProtectMissionSink->onAddRankEnd();

	//关闭连接
	stop();

	return true;
}
