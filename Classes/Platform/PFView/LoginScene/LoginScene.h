#ifndef _LogonScene_H_
#define _LogonScene_H_

#include "cocos2d.h"
#include "cocos-ext.h"
#include "Platform/PFKernel/CGPLoginMission.h"
#include "ui/CocosGUI.h"

using namespace cocos2d;

class LoginLayer;
class RegisterLayer;

class LoginScene
	: public cocos2d::CCScene
	, IGPLoginMissionSink
{
public:
	CREATE_FUNC(LoginScene);

private:
	LoginScene();
	~LoginScene();
	bool init();

public:
	virtual void onEnterTransitionDidFinish();

	//////////////////////////////////////////////////////////////////////////
	// click
private:
	void onBtnClick(cocos2d::Ref* pSender, cocos2d::extension::Control::EventType e);
	void onCloseCallback(cocos2d::Node *pNode);

	void OnUpdateRequest(Node* node);
	//////////////////////////////////////////////////////////////////////////
	// notify
private:
	///< 注册界面点击回调
	void registerOpenRequestNotify(cocos2d::Ref* obj);
	///< 账号登录按钮
	void accountLoginEvent(cocos2d::Ref * ref, ui::TouchEventType event_);
	///< 注册界面按钮
	void accountRegisterEvent(cocos2d::Ref * ref, ui::TouchEventType event_);
	///< 快速游戏按钮
	void qucikStarEvent(cocos2d::Ref * ref, ui::TouchEventType event_);
	//void platformAuthFailureNotify(cocos2d::Ref* obj);
	void platformAuthSuccessNotify(cocos2d::Ref* obj);
	void platformGameExitNotify(cocos2d::Ref* obj);
	void onGameExit(float delta);

	//////////////////////////////////////////////////////////////////////////
	// IGPLoginMissionSink
public:
	virtual void onGPLoginSuccess();
	virtual void onGPLoginComplete();
	virtual bool onGPUpdateNotify(byte cbMustUpdate, const char* szDescription);
	virtual void onGPLoginFailure(const char* szDescription);
	virtual void onGPError(int err);
private:
	LoginLayer *	mUILogin;
	RegisterLayer * mUIRegister;
	//////////////////////////////////////////////////////////////////////////
	// mission
	CGPLoginMission	mLogionMission;
};
#endif // _LogonScene_H_
