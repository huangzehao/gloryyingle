//#include "ShlwGame/ShlwDefine/GameHeader.h"
#include "LoginLayer.h"
#include "Tools/tools/StringData.h"
#include "Platform/PFView/LoginScene/LoginScene.h"
#include "Tools/core/Encrypt.h"
#include "Tools/Dialog/NewDialog.h"
#include "Tools/tools/SimpleTools.h"
#include "Platform/PFView/LoadScene/UpdatePackageLayer.h"
#include "Tools/Manager/SoundManager.h"
#include "Tools/tools/Convert.h"
#include "Tools/tools/MTNotification.h"
#include "Tools/core/CCUserDefault.h"

#define IMG_LOGON_LOGIN_1		"LobbyResources/GameRest/btn_green0.png"
#define IMG_LOGON_LOGIN_2		"LobbyResources/GameRest/game_type_bg0.png"
#define IMG_LOGON_REGISTER_1	"LobbyResources/GameRest/btn_green1.png"
#define IMG_LOGON_REGISTER_2	"LobbyResources/GameRest/game_type_bg0.png"
#define FONT_DEFAULT            "TrebuchetMS-Bold"

extern const char* ACCOUNT_ACCOUNT_SECRECT;
extern const char* ACCOUNT_PASSWORD_SECRECT;
extern const char* ACCOUNT_SECTION;
extern const char* ACCOUNT_ACCOUNT;
extern const char* ACCOUNT_PASSWORD;

LoginLayer::LoginLayer()
{
	isSave = false;
}

LoginLayer::~LoginLayer()
{

}

bool LoginLayer::init()
{
	///< 新UI
	return newInit();
}

// 新UI初始化
bool LoginLayer::newInit()
{
	if (!BaseLayer::initWithJsonFile("LobbyResources/LoginLayer.json")) return false;

	Panel_cell = dynamic_cast<ui::Layout*>(m_root_widget->getChildByName("Panel_cell"));

	tf_account = dynamic_cast<ui::TextField*>(Panel_cell->getChildByName("img_account")->getChildByName("tf_account"));

	tf_password = dynamic_cast<ui::TextField*>(Panel_cell->getChildByName("img_account")->getChildByName("tf_password"));

	ui::Button *btn_login_account = dynamic_cast<ui::Button*>(Panel_cell->getChildByName("btn_login_account"));
	btn_login_account->addTouchEventListener(this, ui::SEL_TouchEvent(&LoginLayer::loginAccountTouchEvent));

	ui::Button *btn_register = dynamic_cast<ui::Button*>(Panel_cell->getChildByName("btn_accountr"));
	btn_register->addTouchEventListener(this, ui::SEL_TouchEvent(&LoginLayer::reisterTouchEvent));

	ui::Button *btn_close = dynamic_cast<ui::Button*>(Panel_cell->getChildByName("btn_close"));
	btn_close->addTouchEventListener(this, ui::SEL_TouchEvent(&LoginLayer::loginHideTouchEvent));

	//加载历史账号密码数据
	loadLocationData();

// 	m_pAccountEdit = cocos2d::ui::EditBox::create(Size(318, 58), "lobbyRes/common_res/common_edit_textIcon.png");
// 	m_pAccountEdit->setAnchorPoint(cocos2d::Point(0.f, 0.5f));
// 	m_pAccountEdit->setPosition(cocos2d::Point(200,300));
// 	m_pAccountEdit->setMaxLength(LEN_ACCOUNTS);
// 	m_pAccountEdit->setFontSize(28);
// 	m_pAccountEdit->setPlaceholderFont(FONT_DEFAULT, 20);
// 	m_pAccountEdit->setInputMode(cocos2d::ui::EditBox::InputMode::SINGLE_LINE);
// 	m_pAccountEdit->setPlaceholderFontColor(cocos2d::Color3B(214, 246, 255));
// 	m_pAccountEdit->setPlaceHolder("rrt");
// 	m_pAccountEdit->setFontColor(Color3B::YELLOW);
// 	this->addChild(m_pAccountEdit);

	return true;
}

void LoginLayer::uiLogonCallback(cocos2d::Ref* pSender)
{
	log("12345567");
}


Scene* LoginLayer::createLoginScene()
{
	Scene* sc_ = Scene::create();
	if (!sc_){
		return 0;
	}

	LoginLayer *layer_ = LoginLayer::create();
	sc_->addChild(layer_);

	return sc_;
}

void LoginLayer::passwordSavaTouchEvent(cocos2d::Ref * ref, ui::CheckBoxEventType event_)
{
	if (event_ == ui::CheckBoxEventType::CHECKBOX_STATE_EVENT_SELECTED)
	{
		tf_password->setPasswordEnabled(false);
		
		isSave = true;
	}
	else
	{
		tf_password->setPasswordEnabled(true);
		isSave = false;
	}
	tf_password->setString(tf_password->getString());
	tf_password->draw();
}

void LoginLayer::accountSelectTouchEvent(cocos2d::Ref * ref, ui::TouchEventType event_)
{
	if (event_ == ui::TouchEventType::TOUCH_EVENT_BEGAN){
		PLAY_BUTTON_CLICK_EFFECT
	}
	if (event_ == ui::TouchEventType::TOUCH_EVENT_ENDED){
		
		static bool click_state = false;
		ui::Button * check_1 = dynamic_cast<ui::Button *>(ref);
		ui::ImageView * img_check_1 = dynamic_cast<ui::ImageView *>(check_1->getChildByName("img_icon"));

	}
}

void LoginLayer::reisterTouchEvent(cocos2d::Ref * ref, ui::TouchEventType event_)
{
	if (event_ == ui::TouchEventType::TOUCH_EVENT_BEGAN){
		PLAY_BUTTON_CLICK_EFFECT
	}
	if (event_ == ui::TouchEventType::TOUCH_EVENT_ENDED){
		LoginScene *sc_ = dynamic_cast<LoginScene*>(getParent());
		CCAssert(sc_, "");

		G_NOTIFY_D("UI_REGISTER_OPEN_REQUEST", 0);
	};
}

void LoginLayer::loginAccountTouchEvent(cocos2d::Ref * ref, ui::TouchEventType event_)
{
	if (event_ == ui::TouchEventType::TOUCH_EVENT_BEGAN){
		PLAY_BUTTON_CLICK_EFFECT
	}
	if (event_ == ui::TouchEventType::TOUCH_EVENT_ENDED){
		//		Director::getInstance()->replaceScene(HelloWorld::createScene());

		const char* account = tf_account->getString().c_str();
		const char* pwd = tf_password->getString().c_str();

		if (utf8_len(account) == 0)
		{
			log("account not space");
			NewDialog::create(SSTRING("logon_account_null"), NewDialog::NONEBUTTON);
			return;
		}

		if (strlen(pwd) == 0)
		{
			NewDialog::create(SSTRING("logon_password_null"), NewDialog::NONEBUTTON);
			log("password not space");
			return;
		}

		//mLBTips->setString(SSTRING("logon_logoning"));
		
		log("account name is %s mima %s", tf_account->getString().c_str(), pwd);
		
		NewDialog::create(SSTRING("logon_logoning"), NewDialog::NONEBUTTON);
		G_NOTIFY_D("PLATFORM_AUTH_SUCCESS", MTData::create(0, 0, 0, account, pwd));


		//Director::getInstance()->replaceScene(TransitionFade::create(0.5f, ModeScene::create()));
	}
}

void LoginLayer::onEnterTransitionDidFinish()
{
	BaseLayer::onEnterTransitionDidFinish();

	if (isSave)
	{
		log("baocun!~~");
	}
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
	{
		//checkUpdate();
	}
#endif
}

void LoginLayer::loadLocationData()
{
	std::string sAccountValue = MyUserDefault::getInstance()->getStringForKey(ACCOUNT_SECTION, ACCOUNT_ACCOUNT);
	std::string sPwdValue = MyUserDefault::getInstance()->getStringForKey(ACCOUNT_SECTION, ACCOUNT_PASSWORD);

	int size = sAccountValue.size();
	CCLOG("UserDefault size is %d", size);
//	o_o::DecryptA((unsigned char*)&sAccountValue[0], size, 5);
	o_o::DecryptA((unsigned char*)&sPwdValue[0], sPwdValue.size());

	tf_account->setString(sAccountValue);
	tf_password->setString(sPwdValue);
}

void LoginLayer::checkUpdate()
{
	UpdatePackageLayer* update = UpdatePackageLayer::create();
	if (update != nullptr)
	{
		this->addChild(update, 100);
		update->onFinishCallback = std::bind(&LoginLayer::updateCheckFinishCallback, this,
			std::placeholders::_1,
			std::placeholders::_2,
			std::placeholders::_3,
			std::placeholders::_4);
		update->checkUpdate();
	}
}

void LoginLayer::updateCheckFinishCallback(bool updated, cocos2d::Node* pSender, const std::string& message, const std::string& storePath)
{
	if (updated)
	{
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
		//HN::Operator::requestChannel("sysmodule", "installApp", storePath);
#elif (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
		std::string url = "itms-services://?action=download-manifest&amp;url=http://app.757gm.com/game_update/";
		url.append(SSTRING("IOS_APP_VERSION_NAME_PLIST"));
		url.append(".plist");
		Application::getInstance()->openURL(url);
#else		
		;
#endif
		if (pSender != nullptr)
		{
			pSender->removeFromParent();
		}
	}
	else
	{
		std::string str(SSTRING("System_Tips_33"));
		str.append(message);
		NewDialog::create(str, NewDialog::NONEBUTTON);
		
		if (pSender != nullptr)
		{
			pSender->removeFromParent();
		}
	}
}

void LoginLayer::loginHideTouchEvent(cocos2d::Ref * ref, ui::TouchEventType event_)
{
	if (event_ == ui::TouchEventType::TOUCH_EVENT_BEGAN){
		PLAY_BUTTON_CLOSE_EFFECT
	}
	if (event_ == ui::TouchEventType::TOUCH_EVENT_ENDED && this->getPositionY() == 0){
		this->runAction(CCSequence::create(MoveBy::create(0.2f, Vec2(0, 850)), CCCallFunc::create(this, callfunc_selector(LoginLayer::closeMoveTo)), NULL));
	};
}

void LoginLayer::closeMoveTo()
{
	this->setVisible(false);
}


