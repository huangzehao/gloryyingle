#ifndef _UIRegister_H_
#define _UIRegister_H_

// #include "cocos2d.h"
// #include "cocos-ext.h"

/*#include "cocos/ui/UIEditBox/UIEditBox.h"*/

#include "Platform/PFView/BaseLayer.h"
#include "Platform/PFKernel/CGPLoginMission.h"
#include "ui/CocosGUI.h"

class RegisterLayer
	: public BaseLayer, IGPLoginMissionSink
{
public:
	CREATE_FUNC(RegisterLayer);

public:
	RegisterLayer();
	~RegisterLayer();
	bool init();
	///< 新ui的界面
	bool newInit();

public:
	//	void setMissionSink(IGPLoginMissionSink* pIGPLoginMissionSink);

private:
	void onCloseClick(cocos2d::Ref* obj);
	void onConfirmClick(cocos2d::Ref* obj);
	void onReadmeClick(cocos2d::Ref* obj);
	void onReadmeTickClick(cocos2d::Ref* obj);
	///<关闭移动回调
	void closeMoveTo();
private:
// 	///< 男人女人触摸选择
// 	void manAndWomanCheckBoxTouch(cocos2d::Ref * ref, ui::CheckBoxEventType event_);
	///< 注册按钮点击
	void registerTouchEvent(cocos2d::Ref * ref, ui::TouchEventType event_);
	///< 关闭按钮点击
	//void closeTouchEvent(cocos2d::Ref * ref, ui::TouchEventType event_);

protected:
	///<注册界面背景层
	ui::Layout * Panel_cell;
// 	///< 选择男
// 	ui::CheckBox * check_man;
// 	///< 选择女
// 	ui::CheckBox * check_women;
	///< 账号
	ui::TextField *txt_acount_;
// 	///< 昵称
// 	ui::TextField *txt_nickname_;
	///< 密码
	ui::TextField *pwd_;
	///< 确认密码
	ui::TextField *pwd_ensure_;
private:
	virtual void onGPLoginSuccess();
	virtual void onGPLoginFailure(const char* szDescription);
	// 	cocos2d::ui::EditBox*		mTfAccount;
	// 	cocos2d::ui::EditBox*		mTfPwd;
	// 	cocos2d::ui::EditBox*		mTfPwdConfirm;
	// 	cocos2d::MenuItemSprite*			mCbReadme1;
	// 	cocos2d::MenuItemSprite*			mCbReadme2;
	// 	cocos2d::Label*				mLbReadme;

	CGPLoginMission						mGPLoginMission;
};

#endif // _UIRegister_H_