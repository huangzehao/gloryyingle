
#ifndef __LOADING_LAYER_H__
#define __LOADING_LAYER_H__

#include "Platform/PFView/BaseLayer.h"
#include "ui/CocosGUI.h"

class LoginLayer : public BaseLayer
{
public:
	LoginLayer();
	virtual ~LoginLayer();

	bool init();
	bool newInit();
	CREATE_FUNC(LoginLayer);

	static Scene* createLoginScene();

	virtual void onEnterTransitionDidFinish();

protected:
	///< 账号保存
	/*CheckBox * cb_user_sava;*/
	///<登录大厅背景层
	ui::Layout * Panel_cell;
	///< 输入的账号
	ui::TextField * tf_account;
	///< 输入的密码
	ui::TextField * tf_password;

	cocos2d::ui::EditBox   *m_pAccountEdit;

	bool isSave;

private:
	///< 密码保存
	void passwordSavaTouchEvent(cocos2d::Ref * ref, ui::CheckBoxEventType event_);
	///< 账号选择
	void accountSelectTouchEvent(cocos2d::Ref * ref, ui::TouchEventType event_);
	///< 注册按钮
	void reisterTouchEvent(cocos2d::Ref * ref, ui::TouchEventType event_);
	///< 账号登录按钮
	void loginAccountTouchEvent(cocos2d::Ref * ref, ui::TouchEventType event_);
	///< 隐藏登录
	void loginHideTouchEvent(cocos2d::Ref * ref, ui::TouchEventType event_);
	///<关闭移动回调
	void closeMoveTo();

	void uiLogonCallback(cocos2d::Ref* pSender);
	///< 载入本地数据
	void loadLocationData();
	///< 检测更新
	void checkUpdate();
	///< 完成回调
	void updateCheckFinishCallback(bool updated, cocos2d::Node* pSender, const std::string& message, const std::string& storePath);
};

#endif // !__LOADING_LAYER_H__
