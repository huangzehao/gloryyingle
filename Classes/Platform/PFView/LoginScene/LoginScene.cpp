#include "LoginScene.h"
#include "Tools/tools/MTNotification.h"
#include "Tools/core/Encrypt.h"
#include "LoginLayer.h"
#include "RegisterLayer.h"
#include "Platform/PFView/ModeScene/ModeScene.h"
#include "Tools/Dialog/NewDialog.h"
#include "Tools/Manager/SoundManager.h"
#include "Platform/PlatformHeader.h"
#include "Tools/tools/StaticData.h"
#include "Tools/tools/gPlatform.h"
#include "Tools/ViewHeader.h"
#include "Tools/tools/StringData.h"
#include "Tools/core/CCUserDefault.h"
USING_NS_CC;
USING_NS_CC_EXT;

using namespace cocostudio;
using namespace ui;

//////////////////////////////////////////////////////////////////////////
const char* ACCOUNT_ACCOUNT_SECRECT = "account_secrect";
const char* ACCOUNT_PASSWORD_SECRECT = "password_secrect";
const char* ACCOUNT_SECTION = "account";
const char* ACCOUNT_ACCOUNT = "account";
const char* ACCOUNT_PASSWORD = "password";

//////////////////////////////////////////////////////////////////////////

#define IMG_BG				"logonscene_bg.png"
#define IMG_BT_KEFU_1		"logonscene_bt_kefu_1.png"
#define IMG_BT_KEFU_2		"logonscene_bt_kefu_2.png"

#define IMG_LOGON_LOGIN_1	"logonscene_logon_bt_logon_1.png"
#define IMG_LOGON_LOGIN_2	"logonscene_logon_bt_logon_2.png"
#define IMG_BT_BACK_1		"bt_back_1.png"
#define IMG_BT_BACK_2		"bt_back_1.png"

#define IMG_BT_SETTINGS_1	"bt_settings_1.png"
#define IMG_BT_SETTINGS_2	"bt_settings_2.png"
#define IMG_BT_HELP_1		"bt_help_1.png"
#define IMG_BT_HELP_2		"bt_help_1.png"

#define TAG_SETTINGS		1
#define TAG_KEFU			2
#define TAG_HELP			3

#define TAG_LOGIN			4
#define TAG_CLOSE			5

static ControlButton* createButton(const char* normal, const char* selected, const char* disable, cocos2d::Ref* target, cocos2d::extension::Control::Handler selector)
{
	// 	ui::Button * btn_now = ui::Button::create(normal, selected, disable);
	// 	btn_now->addTouchEventListener(target, selector);

	ui::Scale9Sprite* sptNormal = ui::Scale9Sprite::createWithSpriteFrameName(normal);
	ui::Scale9Sprite* sptSelected = selected == 0 ? 0 : ui::Scale9Sprite::createWithSpriteFrameName(selected);
	ui::Scale9Sprite* sptDisable = disable == 0 ? 0 : ui::Scale9Sprite::createWithSpriteFrameName(disable);

	//锟截闭帮拷钮
	ControlButton* bt = ControlButton::create();
	bt->setBackgroundSpriteForState(sptNormal, Control::State::NORMAL);
	if (sptSelected)
		bt->setBackgroundSpriteForState(sptSelected, Control::State::SELECTED);
	if (sptDisable)
		bt->setBackgroundSpriteForState(sptDisable, Control::State::DISABLED);
	bt->setPreferredSize(sptNormal->getPreferredSize());
	bt->addTargetWithActionForControlEvents(target, selector, Control::EventType::TOUCH_UP_INSIDE);

	return bt;
}

//////////////////////////////////////////////////////////////////////////
char szNickNameTmp_1[LEN_NICKNAME] = { 0 };//锟斤拷时锟斤拷锟斤拷,锟斤拷锟斤拷锟斤拷平台锟斤拷锟斤拷
//////////////////////////////////////////////////////////////////////////

LoginScene::LoginScene()
: mLogionMission(ADDRESS_URL, SERVER_PORT), mUILogin(nullptr), mUIRegister(nullptr)
{
	mLogionMission.setMissionSink(this);

	ArmatureDataManager::sharedArmatureDataManager()->addArmatureFileInfo("LobbyResources/LoginUI/shouyedonghua/shouye0.png", "LobbyResources/LoginUI/shouyedonghua/shouye0.plist", "LobbyResources/LoginUI/shouyedonghua/shouye.ExportJson");
	ArmatureDataManager::sharedArmatureDataManager()->addArmatureFileInfo("LobbyResources/LoginUI/zhecedonghua/zhuce0.png", "LobbyResources/LoginUI/zhecedonghua/zhuce0.plist", "LobbyResources/LoginUI/zhecedonghua/zhuce.ExportJson");
	CCSpriteFrameCache::sharedSpriteFrameCache()->addSpriteFramesWithFile("LobbyResources/GameList/GameList.plist");
}

LoginScene::~LoginScene()
{
	mLogionMission.setMissionSink(0);
	G_NOTIFY_UNREG("UI_REGISTER_OPEN_REQUEST");
	G_NOTIFY_UNREG("PLATFORM_AUTH_SUCCESS");
	//G_NOTIFY_UNREG("PLATFORM_AUTH_FAILURE");
	G_NOTIFY_UNREG("PLATFORM_GAME_EXIT");
// 	CCSpriteFrameCache::sharedSpriteFrameCache()->removeSpriteFramesFromFile(PLIST_PATH);
// 	CCTextureCache::sharedTextureCache()->removeTextureForKey(TEX_PATH);

	if (mUILogin){
		mUILogin->release();
		mUILogin = 0;
	}

	if (mUIRegister){
		mUIRegister->release();
		mUIRegister = 0;
	}
}

//锟斤拷始锟斤拷锟斤拷锟斤拷
bool LoginScene::init()
{
	do
	{
		CC_BREAK_IF(!CCScene::init());

		Armature* armature = Armature::create("shouye");
		armature->getAnimation()->playByIndex(0);
		auto visibleSize = Director::getInstance()->getVisibleSize();
		armature->setScale(1.11);
		armature->setPosition(Vec2(visibleSize.width / 2, visibleSize.height / 2));
		this->addChild(armature);

		Widget * mRoot = cocostudio::GUIReader::getInstance()->widgetFromJsonFile("LobbyResources/LoginScene.json");
		Widget * Panel_Main = dynamic_cast<Widget *>(mRoot->getChildByName("Panel_Main"));
		this->addChild(mRoot);

		Armature* armature2 = Armature::create("zhuce");
		armature2->getAnimation()->playByIndex(0);
		armature2->setScale(1.11);
		armature2->setPosition(Vec2(1177,276));
		this->addChild(armature2);

		LoginLayer * layer_ = LoginLayer::create();
		layer_->setTouchEnabled(true);
		this->addChild(layer_);

		mUILogin = layer_;
		mUILogin->setPositionY(850);
		mUILogin->setVisible(false);
		mUILogin->retain();

		ui::Button *zhangHaoDengLu = dynamic_cast<ui::Button*>(Panel_Main->getChildByName("zhangHaoDengLu"));
		zhangHaoDengLu->addTouchEventListener(this, ui::SEL_TouchEvent(&LoginScene::accountLoginEvent));

		ui::Button *quickRegister = dynamic_cast<ui::Button*>(Panel_Main->getChildByName("quickRegister"));
		quickRegister->addTouchEventListener(this, ui::SEL_TouchEvent(&LoginScene::accountRegisterEvent));

		ui::Button *quickGame = dynamic_cast<ui::Button*>(Panel_Main->getChildByName("quickGame"));
		quickGame->addTouchEventListener(this, ui::SEL_TouchEvent(&LoginScene::qucikStarEvent));

		G_NOTIFY_REG("UI_REGISTER_OPEN_REQUEST", LoginScene::registerOpenRequestNotify);

		G_NOTIFY_REG("PLATFORM_AUTH_SUCCESS", LoginScene::platformAuthSuccessNotify);
		//G_NOTIFY_REG("PLATFORM_AUTH_FAILURE", LoginScene::platformAuthFailureNotify);
		G_NOTIFY_REG("PLATFORM_GAME_EXIT", LoginScene::platformGameExitNotify);


		//物理按键返回
		auto listener = EventListenerKeyboard::create();
		listener->onKeyReleased = [](EventKeyboard::KeyCode code, Event *e){
			log("key code : %d", code);

			switch (code) {
			case EventKeyboard::KeyCode::KEY_ESCAPE:
				///< 确认退出平台
				NewDialog::createOnce(SSTRING("affirm_exit_now_account"), NewDialog::AFFIRMANDCANCEL, [=]()
				{
					Director::getInstance()->end();
				}
				);

				break;
			default:
				break;
			}
		};

		Director::getInstance()->getEventDispatcher()->addEventListenerWithSceneGraphPriority(listener, this);

		return true;
	} while (0);

	return false;
}

void LoginScene::accountLoginEvent(cocos2d::Ref * ref, ui::TouchEventType event_)
{
	if (event_ == ui::TouchEventType::TOUCH_EVENT_BEGAN){
		PLAY_BUTTON_CLICK_EFFECT
	}
	if (event_ == ui::TouchEventType::TOUCH_EVENT_ENDED && mUILogin->getPositionY() == 850){
		mUILogin->setVisible(true);

		mUILogin->runAction(CCSequence::create(
			MoveBy::create(0.25f, Vec2(0, -850)),
			MoveBy::create(0.1f, Vec2(0, 100)),
			MoveBy::create(0.05f, Vec2(0, -100)),
			MoveBy::create(0.04f, Vec2(0, 30)),
			MoveBy::create(0.03f, Vec2(0, -30)),
			0));
	};
}

void LoginScene::accountRegisterEvent(cocos2d::Ref * ref, ui::TouchEventType event_)
{
	if (event_ == ui::TouchEventType::TOUCH_EVENT_BEGAN){
		PLAY_BUTTON_CLICK_EFFECT
	}
	if (event_ == ui::TouchEventType::TOUCH_EVENT_ENDED){
		RegisterLayer* uiRegister = RegisterLayer::create();
		this->addChild(uiRegister);
	};
}

void LoginScene::qucikStarEvent(cocos2d::Ref * ref, ui::TouchEventType event_)
{
	if (event_ == ui::TouchEventType::TOUCH_EVENT_BEGAN){
		PLAY_BUTTON_CLICK_EFFECT
	}
	if (event_ == ui::TouchEventType::TOUCH_EVENT_ENDED){
		std::string sAccountValue = MyUserDefault::getInstance()->getStringForKey(ACCOUNT_SECTION, ACCOUNT_ACCOUNT);
		std::string sPwdValue = MyUserDefault::getInstance()->getStringForKey(ACCOUNT_SECTION, ACCOUNT_PASSWORD);
		
		if (sAccountValue == "" || sPwdValue == "")
		{
			return;
		}

		const char* account = sAccountValue.c_str();
		const char* pwd = sPwdValue.c_str();

		NewDialog::create(SSTRING("logon_logoning"), NewDialog::NONEBUTTON);
		G_NOTIFY_D("PLATFORM_AUTH_SUCCESS", MTData::create(0, 0, 0, account, pwd));
	};
}

void LoginScene::onEnterTransitionDidFinish()
{
	CCScene::onEnterTransitionDidFinish();

	PLAY_PLATFORM_BG_MUSIC
	//if (platformGetPlatform() != GAME_PLATFORM_DEFAULT)
	//{
	//	platformOpenLoginView();
	//}
}
//////////////////////////////////////////////////////////////////////////
// click
void LoginScene::onBtnClick(cocos2d::Ref* obj, cocos2d::extension::Control::EventType e)
{
// 	CCControl* ctr = (CCControl*)obj;
// 
// 	switch (ctr->getTag())
// 	{
// 	case TAG_KEFU:
// 	{
// 					 addChild(UIKefu::create());
// 	}
// 		break;
// 	case TAG_SETTINGS:
// 	{
// 						 UIShezhi* shezhi = UIShezhi::create();
// 						 shezhi->setPosition(cocos2d::Vec2(kRevolutionWidth / 2, kRevolutionHeight / 2));
// 						 addChild(shezhi);
// 	}
// 		break;
// 	case TAG_HELP:
// 	{
// 					 UIHelp* help = UIHelp::create();
// 					 help->setPosition(cocos2d::Vec2(kRevolutionWidth / 2, kRevolutionHeight / 2));
// 					 addChild(help);
// 	}
// 		break;
// 	case TAG_LOGIN:
// 	{
// 					  if (platformGetPlatform() != GAME_PLATFORM_DEFAULT)
// 					  {
// 						  platformOpenLoginView();
// 					  }
// 	}
// 		break;
// 	case TAG_CLOSE:
// 	{
// 					  if (platformGetPlatform() != GAME_PLATFORM_DEFAULT)
// 					  {
// 						  platformOpenExitView();
// 					  }
// 	}
// 		break;
// 	}
}

void LoginScene::onCloseCallback(cocos2d::Node *pNode)
{
	switch (pNode->getTag())
	{
	case DLG_MB_OK:
	{
					  onGameExit(0);
	}
		break;
	}
}

void LoginScene::registerOpenRequestNotify(cocos2d::Ref* obj)
{
	if (platformGetPlatform() == GAME_PLATFORM_DEFAULT && mUILogin->getPositionY() == 0)
	{
		mUILogin->runAction(MoveBy::create(0.2f, Vec2(0, 850)));
		mUILogin->setVisible(false);

		RegisterLayer* uiRegister = RegisterLayer::create();
		this->addChild(uiRegister);
	}
}

void LoginScene::platformAuthSuccessNotify(cocos2d::Ref* obj)
{
	EventCustom *event_ = (EventCustom*)obj;
	if (event_ == 0){
		return;
	}

	MTData* data = (MTData*)event_->getUserData();
	if (data == 0)
		return;

	tagGPLoginAccount loginAccount;
	loginAccount.cbValidateFlags = MB_VALIDATE_FLAGS | LOW_VER_VALIDATE_FLAGS;
	strcpy(loginAccount.szAccounts, data->mStr1.c_str());
	strcpy(loginAccount.szPassword, data->mStr2.c_str());
	strcpy(szNickNameTmp_1, data->mStr3.c_str());

	
	std::string sAccountValue = data->mStr1;
	std::string sPwdValue = data->mStr2;

	int size = sAccountValue.size();
	log("size is %d", size);
	sAccountValue.reserve(3 * sAccountValue.size());
	sPwdValue.reserve(3 * sPwdValue.size());

	
//	o_o::EncryptA((unsigned char*)&sAccountValue[0], size, 5);
	o_o::EncryptA((unsigned char*)&sPwdValue[0], sPwdValue.size());

	MyUserDefault::getInstance()->setStringForKey(ACCOUNT_SECTION, ACCOUNT_ACCOUNT, sAccountValue.c_str());
	MyUserDefault::getInstance()->setStringForKey(ACCOUNT_SECTION, ACCOUNT_PASSWORD, sPwdValue.c_str());

 	mLogionMission.loginAccount(loginAccount);


//	tagGPLoginTencent loginTencent;
//	loginTencent.cbGender = 1;
//	loginTencent.cbPlatformID = ULMByTencent;
//	strcpy(loginTencent.szUserUin, "147258");
//	strcpy(loginTencent.szNickName, "147258");
//	strcpy(loginTencent.szCompellation, "");

//	mLogionMission.loginTencent(loginTencent);
}

void LoginScene::platformGameExitNotify(cocos2d::Ref* obj)
{
	EventCustom *event_ = (EventCustom*)obj;
	if (event_ == 0){
		return;
	}

	MTData* data = (MTData*)event_->getUserData();
	if (data == 0)
		return;

	if (data != 0 && data->mData1 == 0)
		popup(SSTRING("exit_game"), SSTRING("exit_game_content"), DLG_MB_OK | DLG_MB_CANCEL, 0, this, callfuncN_selector(LoginScene::onCloseCallback));
	else
		this->scheduleOnce(schedule_selector(LoginScene::onGameExit), 0.3f);
}

void LoginScene::onGameExit(float delta)
{
#if (CC_TARGET_PLATFORM == CC_PLATFORM_WINRT) || (CC_TARGET_PLATFORM == CC_PLATFORM_WP8)
	CCMessageBox("You pressed the close button. Windows Store Apps do not implement a close button.", "Alert");
#else
	CCDirector::getInstance()->end();
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
	exit(0);
#endif
#endif
}
//////////////////////////////////////////////////////////////////////////
// IGPLoginMissionSink
void LoginScene::onGPLoginSuccess()
{
	if (Director::getInstance()->getScenesStack() == 1)
	{
		Director::getInstance()->replaceScene(TransitionFade::create(0.5f, ModeScene::create()));
	} 
	else
	{
		CCDirector::getInstance()->popScene();
	}
}

void LoginScene::onGPLoginComplete()
{
	G_NOTIFY_D("LOGON_COMPLETE", 0);
}

bool LoginScene::onGPUpdateNotify(byte cbMustUpdate, const char* szDescription)
{
	//锟斤拷锟截凤拷锟斤拷锟斤拷顺锟斤拷锟斤拷锟?
	if (cbMustUpdate)
	{
		popup(SSTRING("update_notify_title"), szDescription, DLG_MB_OK, 0, this, callfuncN_selector(LoginScene::OnUpdateRequest));
		return true;
	}

	popup(SSTRING("update_notify_title"), szDescription, DLG_MB_OK | DLG_MB_CANCEL, 0, this, callfuncN_selector(LoginScene::OnUpdateRequest));
	return true;
}


void LoginScene::onGPLoginFailure(const char* szDescription)
{
	//popup(SSTRING("system_tips_title"), szDescription);
	NewDialog::create(szDescription, NewDialog::AFFIRM);

}

void LoginScene::onGPError(int err)
{
	char info[256] = { 0 };
	sprintf(info, SSTRING("login_failed"), err);
	//popup(SSTRING("system_tips_title"), info);
	NewDialog::create(info, NewDialog::AFFIRM);
}


void LoginScene::OnUpdateRequest(Node* node)
{
	switch (node->getTag())
	{
	case DLG_MB_OK:
	{
					  //锟斤拷锟斤拷
					  PLAZZ_PRINTF("game update ...");
					  break;
	}
	}
}


