
//#include "ViewHeader.h"
#include "RegisterLayer.h"
#include "Tools/tools/MTNotification.h"
#include "Tools/core/Encrypt.h"
#include "Tools/Manager/SoundManager.h"
USING_NS_CC;

/*USING_NS_CC_EXT;*/

#include "Tools/tools/StringData.h"
#include "Tools/Dialog/Dialog.h"
#include "Platform/PFView/LoginScene/LoginScene.h"
#include "Tools/tools/StaticData.h"
#include "Tools/tools/Convert.h"
#include "Tools/core/CCUserDefault.h"
#include "Platform/PFDefine/msg/CMD_LogonServer.h"
#include "Tools/Dialog/NewDialog.h"

#define IMG_REGISTER_BG			"logonscene_register_bg.png"
#define IMG_REGISTER_CLOSE		"x.png"
#define IMG_REGISTER_README_1	"logonscene_register_cb_fuxuan_1.png"
#define IMG_REGISTER_README_2	"logonscene_register_cb_fuxuan_2.png"
#define IMG_REGISTER_CONFIRM_1	"logonscene_register_bt_queding_1.png"
#define IMG_REGISTER_CONFIRM_2	"logonscene_register_bt_queding_2.png"

extern const char* ACCOUNT_ACCOUNT_SECRECT;
extern const char* ACCOUNT_PASSWORD_SECRECT;
extern const char* ACCOUNT_SECTION;
extern const char* ACCOUNT_ACCOUNT;
extern const char* ACCOUNT_PASSWORD;
//////////////////////////////////////////////////////////////////////////
RegisterLayer::RegisterLayer()
: mGPLoginMission(ADDRESS_URL, SERVER_PORT)
{
	mGPLoginMission.setMissionSink(this);
}

RegisterLayer::~RegisterLayer()
{
	mGPLoginMission.setMissionSink(0);
}

//初始化方法
bool RegisterLayer::init()
{
	return newInit();
}

bool RegisterLayer::newInit()
{
	do
	{
		CC_BREAK_IF(!BaseLayer::initWithJsonFile("LobbyResources/RegisterLayer.json"));

		//界面下移
		m_root_widget->setPositionY(850);

		m_root_widget->runAction(CCSequence::create(
			MoveTo::create(0.25f, Vec2(0, 0)),
			MoveTo::create(0.1f, Vec2(0, 100)),
			MoveTo::create(0.05f, Vec2(0, 0)),
			MoveTo::create(0.04f, Vec2(0, 30)),
			MoveTo::create(0.03f, Vec2(0, 0)),
			0));

		Panel_cell = dynamic_cast<ui::Layout*>(m_root_widget->getChildByName("Panel_cell"));

		txt_acount_ = dynamic_cast<ui::TextField*>(Panel_cell->getChildByName("img_account")->getChildByName("tf_account"));
		CCAssert(txt_acount_, "");
		
// 		txt_nickname_ = dynamic_cast<ui::TextField*>(m_root_widget->getChildByName("Panel_2")->getChildByName("tf_name"));
// 		CCAssert(txt_nickname_, "");

		pwd_ = dynamic_cast<ui::TextField*>(Panel_cell->getChildByName("img_account")->getChildByName("tf_password_first"));
		CCAssert(pwd_, "");
		//pwd_->setString("chenkaiv587");
		pwd_ensure_ = dynamic_cast<ui::TextField*>(Panel_cell->getChildByName("img_account")->getChildByName("tf_password_double"));
		CCAssert(pwd_ensure_, "");
		//pwd_ensure_->setString("chenkaiv587");
		///< 男女要先写
// 		check_man = dynamic_cast<ui::CheckBox*>(m_root_widget->getChildByName("cb_male"));
// 		CCAssert(check_man, "");
// 		check_man->setSelected(true);
// 		check_women = dynamic_cast<ui::CheckBox*>(m_root_widget->getChildByName("cb_female"));
// 		CCAssert(check_women, "");

// 		check_man->addEventListenerCheckBox(this, ui::SEL_SelectedStateEvent(&RegisterLayer::manAndWomanCheckBoxTouch));
// 		check_women->addEventListenerCheckBox(this, ui::SEL_SelectedStateEvent(&RegisterLayer::manAndWomanCheckBoxTouch));


		//  按钮

		//现在没有了关闭按钮
		// 		ui::Button *btn_close = dynamic_cast<ui::Button*>(image_bg->getChildByName("btn_close"));
		// 		if (btn_close){
		// 			btn_close->addTouchEventListener([=](Ref* ref_, ui::Widget::TouchEventType event_type_){
		// 				if (event_type_ == ui::Widget::TouchEventType::ENDED){
		// 
		// 					LogonScene *sc_ = dynamic_cast<LogonScene*>(getParent());
		// 					if (sc_){
		// 						sc_->changeLayer(1);
		// 					}
		// 				}
		// 			});
		// 		}

		ui::Button *btn_register = dynamic_cast<ui::Button*>(Panel_cell->getChildByName("btn_register"));
		btn_register->addTouchEventListener(this, ui::SEL_TouchEvent(&RegisterLayer::registerTouchEvent));
		CCAssert(btn_register, "");

		ui::Button * btn_close = dynamic_cast<ui::Button *>(Panel_cell->getChildByName("btn_close"));
		CCAssert(btn_close, "");
		btn_close->addTouchEventListener([=](Ref* ref_, ui::Widget::TouchEventType event_type_)
		{
			if (event_type_ == ui::Widget::TouchEventType::BEGAN)
			{
				PLAY_BUTTON_CLOSE_EFFECT
			}
			if (event_type_ == ui::Widget::TouchEventType::ENDED)
			{
				m_root_widget->runAction(CCSequence::create(MoveTo::create(0.2f, Vec2(0, 850)), CCCallFunc::create(this, callfunc_selector(RegisterLayer::closeMoveTo)), NULL));
			}		
		});
		
		return true;
	} while (0);

	return false;
}


// void UIRegister::setMissionSink(IGPLoginMissionSink* pIGPLoginMissionSink)
// {
// 	mGPLoginMission.setMissionSink(pIGPLoginMissionSink);
// }

void RegisterLayer::onCloseClick(cocos2d::Ref* obj)
{
	//	removeFromParentAndCleanup(true);
	//	G_NOTIFY("UI_REGISTER_CLOSE", 0);
}

void RegisterLayer::onConfirmClick(cocos2d::Ref* obj)
{
	// 	const char* sAccount	= mTfAccount->getText();
	// 	const char* sPassword1	= mTfPwd->getText();
	// 	const char* sPassword2	= mTfPwdConfirm->getText();
	// 	
	// 	int len = utf8_len(sAccount);
	// 	if (len < 6 || len > 32)
	// 	{
	// 		popup(SSTRING("system_tips_title"), SSTRING("register_err_account_len"));
	// 		return;
	// 	}
	// 
	// 	len = utf8_len(sPassword1);
	// 	if (len < 6 || len > 32)
	// 	{
	// 		popup(SSTRING("system_tips_title"), SSTRING("register_err_password_len"));
	// 		return;
	// 	}
	// 
	// 	if (utf8_cmp(sPassword1, sPassword2) != 0)
	// 	{
	// 		popup(SSTRING("system_tips_title"), SSTRING("register_err_password_confirm"));
	// 		return;
	// 	}
	// 
	// 	if (!mCbReadme2->isVisible())
	// 	{
	// 		popup(SSTRING("system_tips_title"), SSTRING("register_err_readme"));
	// 		return;
	// 	}
	// 
	// 	tagGPRegisterAccount tagAccount;
	// 	memset(&tagAccount, 0, sizeof(tagAccount));
	// 	tagAccount.cbValidateFlags = LOW_VER_VALIDATE_FLAGS | MB_VALIDATE_FLAGS;
	// 	memcpy(tagAccount.szLogonPass, sPassword1, sizeof(tagAccount.szLogonPass));
	// 	memcpy(tagAccount.szInsurePass, sPassword1, sizeof(tagAccount.szInsurePass));
	// 	memcpy(tagAccount.szAccounts, sAccount, sizeof(tagAccount.szAccounts));
	// 	sprintf(tagAccount.szNickName, "%s_io", sAccount);
	// 
	// 	std::string sAccountValue = sAccount;
	// 	std::string sPwdValue = sPassword1;
	// 
	// 	sAccountValue.reserve(3*sAccountValue.size());
	// 	sPwdValue.reserve(3*sPwdValue.size());
	// 
	// 	o_o::EncryptA((unsigned char*)&sAccountValue[0], sAccountValue.size(), 5);
	// 	o_o::EncryptA((unsigned char*)&sPwdValue[0], sPwdValue.size());
	// 
	// 	MyUserDefault::getInstance()->setStringForKey(ACCOUNT_SECTION, ACCOUNT_ACCOUNT, sAccountValue.c_str());
	// 	MyUserDefault::getInstance()->setStringForKey(ACCOUNT_SECTION, ACCOUNT_PASSWORD, sPwdValue.c_str());
	// 
	// 	mGPLoginMission.registerServer(tagAccount);
}

void RegisterLayer::onReadmeClick(cocos2d::Ref* obj)
{
}

void RegisterLayer::onReadmeTickClick(cocos2d::Ref* obj)
{
	// 	if (mCbReadme1 == obj)
	// 	{
	// 		mCbReadme1->setEnabled(false);
	// 		mCbReadme1->setVisible(false);
	// 		mCbReadme2->setEnabled(true);
	// 		mCbReadme2->setVisible(true);
	// 	}
	// 	else
	// 	{
	// 		mCbReadme1->setEnabled(true);
	// 		mCbReadme1->setVisible(true);
	// 		mCbReadme2->setEnabled(false);
	// 		mCbReadme2->setVisible(false);
	// 	}
}


// void RegisterLayer::manAndWomanCheckBoxTouch(cocos2d::Ref * ref, ui::CheckBoxEventType event_)
// {
// 	if (event_ == ui::CheckBoxEventType::CHECKBOX_STATE_EVENT_SELECTED)
// 	{
// 		if (ref == check_man) check_women->setSelectedState(false);
// 		if (ref == check_women) check_man->setSelectedState(false);
// 
// 	}
// 	else
// 	{
// 		if (ref == check_man) check_women->setSelectedState(true);
// 		if (ref == check_women) check_man->setSelectedState(true);
// 
// 	}
// }

void RegisterLayer::registerTouchEvent(cocos2d::Ref * ref, ui::TouchEventType event_)
{
	if (event_ == ui::TouchEventType::TOUCH_EVENT_BEGAN){
		PLAY_BUTTON_CLICK_EFFECT
	}
	if (event_ == ui::TouchEventType::TOUCH_EVENT_ENDED){
		
		const char* sAccount = txt_acount_->getStringValue().c_str();
/*		const char* sNickName = txt_nickname_->getStringValue().c_str();*/

		const char* sPassword1 = pwd_->getStringValue().c_str();
		const char* sPassword2 = pwd_ensure_->getStringValue().c_str();

		int value_len = strlen(sAccount);
		int len = utf8_len(sAccount);
		if (value_len < 6 && (len < 6 || len > 32))
		{
			log("account  fail");
			NewDialog::create(SSTRING("register_err_account_len"), NewDialog::NONEBUTTON);
			return;
		}

// 		value_len = strlen(sNickName);
// 		len = utf8_len(sNickName);   //  nickName  现在先不发过去
// 		if (value_len < 6 && (len < 3 || len > 16))
// 		{
// 			NewDialog::create(SSTRING("register_err_account_len"), NewDialog::NONEBUTTON);
// 			log("nickname  fail");
// 			return;
// 		}

		len = utf8_len(sPassword1);
		if (len < 6 || len > 32)
		{
			log("password_1  fail");
			NewDialog::create(SSTRING("register_err_password_len"), NewDialog::NONEBUTTON);
			return;
		}

		if (utf8_cmp(sPassword1, sPassword2) != 0)
		{
			log("against fail");
			NewDialog::create(SSTRING("register_err_password_confirm"), NewDialog::NONEBUTTON);
			return;
		}

		tagGPRegisterAccount tagAccount;
		memset(&tagAccount, 0, sizeof(tagAccount));
		tagAccount.cbValidateFlags = LOW_VER_VALIDATE_FLAGS | MB_VALIDATE_FLAGS;
		if (rand() % 2 == 0)
		{
			//随机性别是男
			tagAccount.cbGender = 1;
			tagAccount.wFaceID = 4 + rand() % 4;
		}
		else
		{
			//随机性别是女
			tagAccount.cbGender = 0;
			tagAccount.wFaceID = rand() % 4;
		}
			
		memcpy(tagAccount.szLogonPass, sPassword1, sizeof(tagAccount.szLogonPass));
		memcpy(tagAccount.szInsurePass, sPassword1, sizeof(tagAccount.szInsurePass));
		memcpy(tagAccount.szAccounts, sAccount, sizeof(tagAccount.szAccounts));
		sprintf(tagAccount.szNickName, "%s", sAccount);

		std::string sAccountValue = sAccount;
		std::string sPwdValue = sPassword1;

		sAccountValue.reserve(3 * sAccountValue.size());
		sPwdValue.reserve(3 * sPwdValue.size());

//		o_o::EncryptA((unsigned char*)&sAccountValue[0], sAccountValue.size(), 5);
		o_o::EncryptA((unsigned char*)&sPwdValue[0], sPwdValue.size());

		MyUserDefault::getInstance()->setStringForKey(ACCOUNT_SECTION, ACCOUNT_ACCOUNT, sAccountValue.c_str());
		MyUserDefault::getInstance()->setStringForKey(ACCOUNT_SECTION, ACCOUNT_PASSWORD, sPwdValue.c_str());

		mGPLoginMission.registerServer(tagAccount);

	}
}

void RegisterLayer::onGPLoginSuccess()
{
	LoginScene * now_scene = dynamic_cast<LoginScene *>(Director::getInstance()->getRunningScene());
	now_scene->onGPLoginSuccess();
}

void RegisterLayer::onGPLoginFailure(const char* szDescription)
{
	LoginScene * now_scene = dynamic_cast<LoginScene *>(Director::getInstance()->getRunningScene());
	now_scene->onGPLoginFailure(szDescription);
}

void RegisterLayer::closeMoveTo()
{
	this->removeFromParent();
}

// void RegisterLayer::closeTouchEvent(cocos2d::Ref * ref, ui::TouchEventType event_)
// {
// 
// }

