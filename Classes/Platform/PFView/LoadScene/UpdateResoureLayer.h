#ifndef UPDATERESOURELAYER_H_
#define UPDATERESOURELAYER_H_

#include "cocos2d.h"
#include "Platform/PFView/BaseLayer.h"
#include "ui/CocosGUI.h"

USING_NS_CC;
USING_NS_CC_EXT;
class UpdateResoureLayer : public BaseLayer, public cocos2d::extension::AssetsManagerDelegateProtocol
{
public:
	UpdateResoureLayer();
	virtual ~UpdateResoureLayer();

	virtual bool init(int game_id);

	void upgrade(cocos2d::Ref* pSender);	//检查版本更新
	void reset(cocos2d::Ref* pSender);	//重置版本

	virtual void onError(cocos2d::extension::AssetsManager::ErrorCode errorCode);	//错误信息
	virtual void onProgress(int percent);	//更新下载进度
	virtual void onSuccess();	//下载成功

	virtual void onEnterTransitionDidFinish();
	void setCallBack(std::function<void()> func);
	///< 设置游戏id
	inline void setGameId(int game_id) { mGameId = game_id; }
	///< 获取资源名
	std::string getResourceName(int game_id);
	///< 获取版本名
	std::string getVersionName(int game_id);
	///< 获取保存的目录名
	std::string getSavePathName(int game_id);

	static UpdateResoureLayer * create(int game_id);
	///< 检查更新
	bool isHaveNewVersion();
private:
	cocos2d::extension::AssetsManager* getAssetManager();
	void initDownloadDir();	//创建下载目录
	///< 跳转到资源载入场景
	void skipLoadResourceSceme(bool isSucceed = true);

private:
	std::string _pathToSave;
	std::string _saveFileName;
	cocos2d::Label *_showDownloadInfo;
	cocos2d::ui::ImageView *img_people;

	ui::LoadingBar * pb_loading;
/*	ui::ImageView * img_loading_name;*/

	std::function<void()> mFunc;

	cocos2d::extension::AssetsManager *assetManager;

	int mGameId;
};


#endif

