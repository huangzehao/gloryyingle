#ifndef UPDATEPACKAGELAYER_H_
#define UPDATEPACKAGELAYER_H_

#include "cocos2d.h"
#include "cocos-ext.h"
#include "Platform/PFView/BaseLayer.h"
#include "ui/CocosGUI.h"

USING_NS_CC;
USING_NS_CC_EXT;
class UpdatePackageLayer : public BaseLayer
{
public:
	CREATE_FUNC(UpdatePackageLayer);

	virtual bool init();
	void checkUpdate();
	UpdatePackageLayer();
	~UpdatePackageLayer();
	///< 完成后的回调!!
	std::function<void(bool updated, cocos2d::Node * pSender, const std::string &message, const std::string & storePath)> onFinishCallback;

protected:
	static const std::string VERSION_CHECK_URL;
	static const std::string DOWNLOAD_URL;
	bool getOnlineVersion();
	bool hasNewVersion();
	void onProgressCallback(double total, double downloaded, const std::string &url, const std::string &customId);
	void onSuccessCallback(const std::string &url, const std::string &tmp, const std::string &customId);
	void onErrorCallback(const Downloader::Error &error);
private:
	std::string _onlineVersion;
	std::string _localVersion;
	std::string _storePath;
	std::string _jsonPath;
	std::string _downloadPath;
	std::string _downloadName;
	std::string _versionKey;
	std::shared_ptr<Downloader> _downloader;
	cocos2d::ui::LoadingBar* _progressTimer;
	//Sprite* _progressBg;
	ui::Text* _labelPercent;
	ui::Text* _labelMessage;
};


#endif

