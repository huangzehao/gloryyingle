#include "Tools/tools/MTNotification.h"
#include "LoadingLayer.h"
#include <pthread.h>
#include "Platform/PFView/ModeScene/ModeScene.h"
#include "Tools/Manager/SoundManager.h"
#include "Platform/PlatformHeader.h"
#include "Tools/Manager/SpriteHelper.h"
#include "Kernel/kernel/game/IClientKernel.h"
#include "Kernel/kernel/server/IServerItemSink.h"

#include "DntgGame/DntgKernel/DntgPathManager.h"

#include "DntgGame/DntgDefine/Game/DntgCMD_CatchBird.h"

#include "DntgGame/DntgView/Scene/DntgGameScene.h"

USING_NS_CC;

//#define PLIST_LOADSCENE	"loadscene/loadscene.plist"
//#define TEX_LOADSCENE	"loadscene/loadscene.pvr.ccz"

//////////////////////////////////////////////////////////////////////////
///< 是第一次载入资源
static bool isFirstLoadingResourse = true;
///< 前一个载入的资源名
std::string preLoadResourseName;
///< 后一个载入的资源名
std::string nowLoadResourseName;
//////////////////////////////////////////////////////////////////////////
LoadingLayer::LoadingLayer()
	: mLoadedNum(0)
	, mTotalNum(0)
	, mResources(0)
	, mGameId(600)
{
	mFunc = nullptr;
}

LoadingLayer::~LoadingLayer()
{
	//CCSpriteFrameCache::sharedSpriteFrameCache()->removeSpriteFramesFromFile(PLIST_LOADSCENE);
	//CCTextureCache::sharedTextureCache()->removeTextureForKey(TEX_LOADSCENE);
	mFunc = nullptr;
	CC_SAFE_RELEASE_NULL(mResources);
	this->unschedule(SEL_SCHEDULE(&LoadingLayer::checkComeIn));
}

//初始化方法
bool LoadingLayer::init()
{
	do 
	{
		CC_BREAK_IF(!BaseLayer::initWithJsonFile("LobbyResources/GameLoadingLayer.json"));
		CC_BREAK_IF(!this->setUpdateView());
		return true;
	} while (0);

	return false;
}

bool LoadingLayer::init(int wGameId)
{
	mGameId = wGameId;
	preLoadResourseName = nowLoadResourseName;
	nowLoadResourseName = this->getGameNameByGameId(mGameId);
		
	return this->init();
}

bool LoadingLayer::init(std::string wGameName)
{
	preLoadResourseName = nowLoadResourseName;
	nowLoadResourseName = wGameName;
	return this->init();
}


// 初始化组件
bool LoadingLayer::setUpdateView()
{
	do
	{

		pb_loading = dynamic_cast<ui::LoadingBar *>(m_root_widget->getChildByName("img_progressbar")->getChildByName("pb_loading"));
		CCAssert(pb_loading, "");

		img_GameType = dynamic_cast<ui::ImageView *>(m_root_widget->getChildByName("img_gameBg")->getChildByName("img_GameType"));
		img_GameType->loadTexture(StringUtils::format("LobbyResources/LobbyCommon/icon_%d.png", mGameId));

		this->scheduleOnce(SEL_SCHEDULE(&LoadingLayer::checkComeIn), 30.0f);
		return true;
	} while (0);

	return false;
}

void LoadingLayer::onEnterTransitionDidFinish()
{
	log("LoadingLayer::onEnterTransitionDidFinish");

	CCLayer::onEnterTransitionDidFinish();

	if (isFirstLoadingResourse || preLoadResourseName.compare(nowLoadResourseName))
	{
		///< 清除动画缓存
		AnimationCache::destroyInstance();
		///< 清理精灵内存缓存
		SpriteFrameCache::destroyInstance();
		///< 释放图片
		Director::getInstance()->getTextureCache()->removeUnusedTextures();

		if (isFishBirdGame())
		{
			Dntg::Path_Manager::purge();
		}

		std::string rootStr = nowLoadResourseName;

		std::string filePath;
		if (isFishBirdGame())
		{
			filePath = ("game/waterAni/waterAni.plist");
			CCSpriteFrameCache::sharedSpriteFrameCache()->addSpriteFramesWithFile(filePath);
		}
		rootStr = "game/" + nowLoadResourseName;

		filePath = rootStr.append("/resources.plist");
		mResources = cocos2d::Array::createWithContentsOfFile(filePath);
		CC_SAFE_RETAIN(mResources);
		mResources->retain();
		mLoadedNum = 0;
		mTotalNum = mResources == 0 ? 0 : mResources->count();
		// 	mCurW		= 0;
		// 	mTargetW	= 0;

		PLAZZ_PRINTF("line:%d", mTotalNum);
		// 启动加载
		this->scheduleUpdate();

		log("LoadingLayer::onEnterTransitionDidFinish 结束 end ");

		isFirstLoadingResourse = false;
	}
	else
	{
		pb_loading->setPercent(100.0f);

		if (mFunc)
		{
			mFunc();
		}

	}
	
}

void LoadingLayer::update(float delta)
{
	static const int FRAME_LOAD = 10;
	static int FRAME_CUR = FRAME_LOAD;
	if (!mTotalNum == 0)
	{
		float Xdistance = pb_loading->getContentSize().width / mTotalNum;

		if (FRAME_CUR >= FRAME_LOAD)
		{
			FRAME_CUR = 0;

			if (mLoadedNum < mTotalNum)
			{
				const char* name = ((CCString*)mResources->objectAtIndex(mLoadedNum))->getCString();
				char sPlist[80] = { 0 };
				sprintf(sPlist, "%s.plist", name);
				CCSpriteFrameCache::sharedSpriteFrameCache()->addSpriteFramesWithFile(sPlist);
				mLoadedNum++;

				//mTargetW = mOriginW*((float)mLoadedNum/mTotalNum);
			}
		}
		else
		{
			FRAME_CUR++;
		}

		// 	if (mCurW < mTargetW)
		// 	{
		// 		mCurW += (mTargetW - mCurW)/10+1;
		// 
		// 		if (mCurW >= mTargetW)
		// 		{
		// 			mCurW = mTargetW;
		// 		}
		// 	
		// 		mBar->setTextureRect(CCRectMake(mBar->getTextureRect().origin.x, mBar->getTextureRect().origin.y, mCurW, mBar->getTextureRect().size.height));
		// 	}
		//CCLOG("mCurW is %f, mTargetW is %f", mCurW, mTargetW);

		//CCLOG("pos x is %f", pos.x);
		if (mLoadedNum * 100 / mTotalNum > 10)
		{
			pb_loading->setPercent(mLoadedNum * 100 / mTotalNum);
		}

		//	log("current num is %d", mLoadedNum);
		CCTextureCache::sharedTextureCache()->getCachedTextureInfo();
		if (mLoadedNum < mTotalNum)
			return;
	}

	this->unscheduleUpdate();
	CC_SAFE_RELEASE_NULL(mResources);

	//mBar->setTextureRect(CCRectMake(mBar->getTextureRect().origin.x, mBar->getTextureRect().origin.y, mOriginW, mBar->getTextureRect().size.height));

	SpriteHelper::cacheAnimations("game/" + nowLoadResourseName);

	if (!nowLoadResourseName.compare("dntg"))
		Dntg::Path_Manager::shared();
	if (mFunc)
	{
		mFunc();
	}
	//G_NOTIFY_D("LOADING_COMPLETE", 0);

	return;
}

void LoadingLayer::setCallBack(std::function<void()> func)
{
	mFunc = func;
}

void LoadingLayer::checkComeIn(float dt)
{
	if (IClientKernel::get())
		IClientKernel::get()->Intermit(GameExitCode_Normal);
	else
	{
// 		Director * director = Director::getInstance();
// 		if (director->getRunningScene() != nullptr)
// 		{
// 			director->replaceScene(CCTransitionFade::create(0.5f, ModeScene::create()));
// 		}
// 		else
// 		{
// 			director->runWithScene(CCTransitionFade::create(0.5f, ModeScene::create()));
// 		}
		this->removeFromParent();
	}
	
}

LoadingLayer * LoadingLayer::create(int wGameId)
{
	LoadingLayer * layer = new LoadingLayer;
	if (layer && layer->init(wGameId))
	{
		layer->autorelease();
		return layer;
	}
	delete layer;
	layer = nullptr;
	return nullptr;
}

LoadingLayer * LoadingLayer::create(std::string wGameName)
{
	LoadingLayer * layer = new LoadingLayer;
	if (layer && layer->init(wGameName))
	{
		layer->autorelease();
		return layer;
	}
	delete layer;
	layer = nullptr;
	return nullptr;
}

std::string LoadingLayer::getGameNameByGameId(int game_id)
{
	if (game_id == DANAOTIANGO_KINDID)
	{
		return "dntg";
	}
	else if (game_id == BAIJIALE_KINDID)
	{
		return "baccarat";
	}
	else if (game_id == ZHAJINHUA_KINDID)
	{
		return "zhajinhua";
	}
	else if (game_id == DOUDIZHU_KINDID)
	{
		return "doudizhu";
	}
	else if (game_id == SIRENNIUNIU_KINDID)
	{
		return "ox4";
	}
}

bool LoadingLayer::isFishBirdGame()
{
	if (nowLoadResourseName.compare("dntg") == 0)
	{
		return true;
	}
	else
	{
		return false;
	}
}