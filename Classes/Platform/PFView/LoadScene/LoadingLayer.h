#ifndef _LoadingLayer_H_
#define _LoadingLayer_H_

#include "cocos2d.h"
#include "Platform/PFView/BaseLayer.h"
#include "ui/CocosGUI.h"

USING_NS_CC;

class LoadingLayer
	: public BaseLayer
{
public:
	//创建场景
	//创建方法
	//CREATE_FUNC(LoadingLayer);
	static LoadingLayer * create(int wGameId);
	static LoadingLayer * create(std::string wGameName);
public:
	//构造函数
	LoadingLayer();
	~LoadingLayer();
	//初始化方法
	virtual bool init(int wGameId);
	virtual bool init(std::string wGameName);
	virtual bool init();
	//进入完成
	virtual void onEnterTransitionDidFinish();
	//更新
	virtual void update(float delta);
	///< 设置回掉
	void setCallBack(std::function<void()> func);
	///< 超时检测
	void checkComeIn(float dt);

	std::string getGameNameByGameId(int game_id);

	///< 载入游戏基础数据
	//static void loadGameBaseData();
private:
	//初始化页面的基本纹理
	bool setUpdateView();	

	bool isFishBirdGame();
private:
	// 已加载数量
	int		mLoadedNum;
	// 加载数量
	int		mTotalNum;
	int		mGameId;
	
	float	mOriginW;
	float	mCurW;
	float	mTargetW;
	//cocos2d::Sprite*	mBar;
	cocos2d::CCArray*	mResources;

	ui::LoadingBar * pb_loading;
	ui::ImageView * img_GameType;

	std::function<void()> mFunc;


};
#endif // _LoadingLayer_H_