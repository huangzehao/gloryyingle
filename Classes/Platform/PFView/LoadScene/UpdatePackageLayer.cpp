#include "UpdatePackageLayer.h"
#include "Tools/tools/StringData.h"
#include "Tools/Dialog/NewDialog.h"

using namespace ui;

const std::string UpdatePackageLayer::DOWNLOAD_URL = std::string("http://ofuv3d1ck.bkt.clouddn.com/game_update/");

const std::string UpdatePackageLayer::VERSION_CHECK_URL =  std::string("http://ofuv3d1ck.bkt.clouddn.com/game_update/update_verson_ios.json");

UpdatePackageLayer::UpdatePackageLayer() : onFinishCallback(nullptr),
_labelPercent(nullptr),
_progressTimer(nullptr),
_labelMessage(nullptr)
{
	_downloadName = "gloryGame_ios";
	_versionKey = "versionCode";

	_downloader = std::make_shared<Downloader>();
	_downloader->setProgressCallback(std::bind(&UpdatePackageLayer::onProgressCallback, this,
		std::placeholders::_1,
		std::placeholders::_2,
		std::placeholders::_3,
		std::placeholders::_4));

	_downloader->setSuccessCallback(std::bind(&UpdatePackageLayer::onSuccessCallback, this,
		std::placeholders::_1,
		std::placeholders::_2,
		std::placeholders::_3));

	_downloader->setErrorCallback(std::bind(&UpdatePackageLayer::onErrorCallback, this,
		std::placeholders::_1));

	_localVersion = SSTRING("IOS_APP_VERSION");
	_onlineVersion = _localVersion;
	_downloadPath = DOWNLOAD_URL;

#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS || CC_TARGET_PLATFORM == CC_PLATFORM_WIN32 )

	_storePath = FileUtils::getInstance()->getWritablePath();
	_jsonPath = FileUtils::getInstance()->getWritablePath();
	_jsonPath.append("/version.json");
#endif

	_storePath.append(_downloadName);
	_downloadPath.append(_downloadName);

#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
	_storePath.append(".ipa");
	_downloadPath.append(".ipa");
#else
	_storePath.append(".apk");
	_downloadPath.append(".apk");
#endif
}


UpdatePackageLayer::~UpdatePackageLayer()
{
}

bool UpdatePackageLayer::init()
{
	return true;
}

static const int BUFFER_SIZE = 200;
static unsigned char buffer[BUFFER_SIZE] = { 0 };

void UpdatePackageLayer::checkUpdate()
{
	getOnlineVersion();
}

bool UpdatePackageLayer::getOnlineVersion()
{
	memset(buffer, 0, sizeof(buffer));
	_labelMessage->setString(SSTRING("System_Tips_30"));
	_downloader->downloadAsync(VERSION_CHECK_URL, _jsonPath, "check");

	return true;
}

bool UpdatePackageLayer::hasNewVersion()
{
	return _localVersion.compare(_onlineVersion) != 0;
}

void UpdatePackageLayer::onProgressCallback(double total, double downloaded, const std::string &url, const std::string &customId)
{
	if (customId.compare("check") == 0)
	{
		///< 不显示进度
	}
	else if (customId.compare("download") == 0)
	{
		///< 下载app时,显示进度
		if (_progressTimer != nullptr)
		{
			float percent = (float)(downloaded / total) * 100;
			_progressTimer->setPercent(percent);
		}

		if (_labelPercent != nullptr)
		{
			char str[50] = { 0 };
			sprintf(str, "%.2fKB / %.2fKB", downloaded / 1024, total / 1024);
			_labelPercent->setString(str);
		}
	}
	else
	{

	}
}

void UpdatePackageLayer::onSuccessCallback(const std::string &url, const std::string &tmp, const std::string &customId)
{
	if (customId.compare("check") == 0)
	{
		rapidjson::Document doc;
		doc.Parse<rapidjson::kParseDefaultFlags>(FileUtils::getInstance()->getStringFromFile(_jsonPath).c_str());
		if (doc.HasParseError() || !doc.IsObject() || !doc.HasMember(_versionKey.c_str()))
		{
			if (onFinishCallback != nullptr)
			{
				onFinishCallback(false, this, "json error", "");
			}

			return;
		}
		_onlineVersion = doc[_versionKey.c_str()].GetString();

		if (hasNewVersion())
		{
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID || CC_TARGET_PLATFORM == CC_PLATFORM_WIN32)

			auto prompt = NewDialog::create(SSTRING("System_Tips_31"), NewDialog::AFFIRMANDCANCEL, [=]()
			{
				if (_progressTimer != nullptr)
				{
					//_progressBg->setVisible(true);
					_progressTimer->setVisible(true);
					_progressTimer->setPercent(0.0f);
				}
				if (_labelMessage != nullptr)
				{
					_labelMessage->setString(SSTRING("System_Tips_32"));
				}
				_downloader->downloadAsync(_downloadPath, _storePath, "download");
			},
				[=]()
			{
				this->removeFromParent();
			});
// 			prompt->showPrompt(GBKToUtf8("发现新版本，是否现在下载？"));
// 			prompt->setPromptCanSelect();
// 			prompt->setCallBack([this](){
// 				if (_progressTimer != nullptr)
// 				{
// 					//_progressBg->setVisible(true);
// 					_progressTimer->setVisible(true);
// 					_progressTimer->setPercent(0.0f);
// 				}
// 				if (_labelMessage != nullptr)
// 				{
// 					_labelMessage->setString(GBKToUtf8("新版下载中..."));
// 				}
// 				_downloader->downloadAsync(_downloadPath, _storePath, "download");
// 			});
// 
// 			prompt->setCancelCallBack([this](){
// 				this->removeFromParent();
// 			});

#elif (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
			onFinishCallback(true, this, "", "");
			//this->removeFromParent();
#else
			//this->removeFromParent();
#endif
		}
		else
		{
			this->removeFromParent();
		}
	}
	else if (customId.compare("download") == 0)
	{
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID || CC_TARGET_PLATFORM == CC_PLATFORM_WIN32)
		do
		{
			CC_BREAK_IF(_progressTimer == nullptr);
			CC_BREAK_IF(onFinishCallback == nullptr);
			if (_progressTimer->getPercent() >= 100.f)
			{
				onFinishCallback(true, this, "success", _storePath);
			}
		} while (0);
#endif
	}
	else
	{

	}
}

void UpdatePackageLayer::onErrorCallback(const Downloader::Error &error)
{
	onFinishCallback(false, this, error.message, "");
}


