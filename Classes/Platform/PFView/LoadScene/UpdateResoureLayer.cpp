#include "UpdateResoureLayer.h"
#include "Tools/tools/MTNotification.h"
#include "Platform/PFView/ModeScene/ModeScene.h"
#include "Tools/Dialog/NewDialog.h"
#include "Tools/tools/StringData.h"

#if (CC_TARGET_PLATFORM != CC_PLATFORM_WIN32)
#include <dirent.h>
#include <sys/stat.h>
#endif

USING_NS_CC;
using namespace std;
//#define DOWNLOAD_FIEL	"gamescene"	//下载后保存的文件夹名
#define DOWNLOAD_URL_RESOURE "http://ofuv3d1ck.bkt.clouddn.com/GameResources/"
#define DOWNLOAD_URL_CHECK "http://ofuv3d1ck.bkt.clouddn.com/PhoneVersion/"

UpdateResoureLayer::UpdateResoureLayer() :
_pathToSave(""),
_showDownloadInfo(NULL),
mFunc(nullptr),
assetManager(nullptr),
mGameId(-1),
_saveFileName("")
{

}

UpdateResoureLayer::~UpdateResoureLayer()
{
	AssetsManager* assetManager = getAssetManager();
	CC_SAFE_DELETE(assetManager);
}


bool UpdateResoureLayer::init(int game_id)
{
	if (!BaseLayer::initWithJsonFile("LobbyResources/LoadingLayer.json"))
	{
		return false;
	}
	mGameId = game_id;
	_saveFileName = getSavePathName(game_id);
	Size winSize = Director::getInstance()->getWinSize();
	CCLOG("load start!!");
	initDownloadDir();
	CCLOG("load end!!");

	pb_loading = dynamic_cast<ui::LoadingBar *>(m_root_widget->getChildByName("img_progressbar")->getChildByName("pb_loading"));
	CCAssert(pb_loading, "");

	_showDownloadInfo = Label::createWithSystemFont("", "Arial", 30);
	_showDownloadInfo->setTextColor(Color4B::YELLOW);
	pb_loading->addChild(_showDownloadInfo, 10);
	_showDownloadInfo->setPosition(Vec2(pb_loading->getPositionX(), pb_loading->getPositionY()));

	img_people = dynamic_cast<ui::ImageView *>(m_root_widget->getChildByName("img_progressbar")->getChildByName("img_people"));

// 	img_loading_name = dynamic_cast<ui::ImageView *>(m_root_widget->getChildByName("img_loading_name"));
// 	CCAssert(img_loading_name, "");
// 	img_loading_name->loadTexture(StringUtils::format("LobbyResources/LobbyCommon/game_type_%d.png", game_id));
	return true;
}

void UpdateResoureLayer::onError(AssetsManager::ErrorCode errorCode)
{
	bool isSucceed = true;
	if (errorCode == AssetsManager::ErrorCode::NO_NEW_VERSION)
	{
		_showDownloadInfo->setString("no new version");
		isSucceed = true;
	}
	else if (errorCode == AssetsManager::ErrorCode::NETWORK)
	{
		_showDownloadInfo->setString("network error");
		isSucceed = false;
	}
	else if (errorCode == AssetsManager::ErrorCode::CREATE_FILE)
	{
		_showDownloadInfo->setString("create file error");
		isSucceed = false;
	}

	skipLoadResourceSceme(isSucceed);
}

void UpdateResoureLayer::onProgress(int percent)
{
	if (percent < 0)
		return;

	std::string tinfo = StringUtils::format("%d%%", percent);

	_showDownloadInfo->setString(tinfo);
	pb_loading->setPercent(percent);
	img_people->loadTexture(StringUtils::format("LobbyResources/ResourceLoading/img_man_%d.png",percent % 5));
}

void UpdateResoureLayer::onSuccess()
{
	CCLOG("download success");
	_showDownloadInfo->setString("download success");
	//std::string path = FileUtils::getInstance()->getWritablePath() + DOWNLOAD_FIEL;
	//FileUtils::getInstance()->addSearchPath(path, true);
	skipLoadResourceSceme(true);
}

AssetsManager* UpdateResoureLayer::getAssetManager()
{
	if (!assetManager)
	{
		string download_url = DOWNLOAD_URL_RESOURE + getResourceName(mGameId);
		string version_url = DOWNLOAD_URL_CHECK + getVersionName(mGameId);
		assetManager = new AssetsManager(download_url.c_str(),
			version_url.c_str(),
			_pathToSave.c_str());
		assetManager->setDelegate(this);
		assetManager->setConnectionTimeout(10);
		assetManager->setGameId(mGameId);
		assetManager->setPackageFileName(getResourceName(mGameId));
	}
	return assetManager;
}

void UpdateResoureLayer::initDownloadDir()
{
	CCLOG("initDownloadDir");
	_pathToSave = FileUtils::getInstance()->getWritablePath();
	_pathToSave += _saveFileName;
	CCLOG("Path: %s", _pathToSave.c_str());
#if (CC_TARGET_PLATFORM != CC_PLATFORM_WIN32)
	DIR *pDir = NULL;
	pDir = opendir(_pathToSave.c_str());
	if (!pDir)
	{
		mkdir(_pathToSave.c_str(), S_IRWXU | S_IRWXG | S_IRWXO);
	}
#else
	if ((GetFileAttributesA(_pathToSave.c_str())) == INVALID_FILE_ATTRIBUTES)
	{
		CreateDirectoryA(_pathToSave.c_str(), 0);
	}
#endif
	CCLOG("initDownloadDir end");
}

void UpdateResoureLayer::reset(Ref* pSender)
{
	_showDownloadInfo->setString("");
	// Remove downloaded files
#if (CC_TARGET_PLATFORM != CC_PLATFORM_WIN32)
	std::string command = "rm -r ";
	// Path may include space.
	command += "\"" + _pathToSave + "\"";
	system(command.c_str());
#else
	std::string command = "rd /s /q ";
	// Path may include space.
	command += "\"" + _pathToSave + "\"";
	system(command.c_str());
#endif
	getAssetManager()->deleteVersion();
	initDownloadDir();
}

void UpdateResoureLayer::upgrade(Ref* pSender)
{ 
	_showDownloadInfo->setString("");
	getAssetManager()->update();

}

void UpdateResoureLayer::onEnterTransitionDidFinish()
{
	Layer::onEnterTransitionDidFinish();
	upgrade(this);
}

void UpdateResoureLayer::skipLoadResourceSceme(bool isSucceed)
{
	log("UpdateResoureLayer::skipLoadResourceSceme");
	if (isSucceed)
	{
		///< 跳转到载入资源界面
		if (mFunc)
		{
			mFunc();
		}
	}
	else
	{
		///< 提示下载资源失败 
		NewDialog::create(SSTRING("System_Tips_27"), NewDialog::AFFIRM);
// 		NewDialog::create(SSTRING("System_Tips_27"), NewDialog::AFFIRM, [=]()
// 		{
// 			ModeScene *mc_ = dynamic_cast<ModeScene*>(Director::getInstance()->getRunningScene());
// 			if (mc_->isLayer(BaseModeLayer::ModeLayerTypeRoomList))
// 			{
// 				mc_->removeLayer(BaseModeLayer::ModeLayerTypeRoomList);
// 				mc_->changeModeLayer(BaseModeLayer::ModeLayerTypeDefault);
// 			}
// 		});	
	}

	this->removeFromParent();
}

void UpdateResoureLayer::setCallBack(std::function<void()> func)
{
	mFunc = func;
}

std::string UpdateResoureLayer::getResourceName(int game_id)
{
	if (game_id == DANAOTIANGO_KINDID)
	{
		return SSTRING("Game_Resource_601");
	}
	else if (game_id == BAIJIALE_KINDID)
	{
		return SSTRING("Game_Resource_122");
	}
	else if (game_id == ZHAJINHUA_KINDID)
	{
		return SSTRING("Game_Resource_6");
	}
	else if (game_id == DOUDIZHU_KINDID)
	{
		return SSTRING("Game_Resource_200");
	}
	else if (game_id == SIRENNIUNIU_KINDID)
	{
		return SSTRING("Game_Resource_203");
	}
}

std::string UpdateResoureLayer::getVersionName(int game_id)
{
	if (game_id == DANAOTIANGO_KINDID)
	{
		return SSTRING("Game_Version_601");
	}
	else if (game_id == BAIJIALE_KINDID)
	{
		return SSTRING("Game_Version_122");
	}
	else if (game_id == ZHAJINHUA_KINDID)
	{
		return SSTRING("Game_Version_6");
	}
	else if (game_id == DOUDIZHU_KINDID)
	{
		return SSTRING("Game_Version_200");
	}
	else if (game_id == SIRENNIUNIU_KINDID)
	{
		return SSTRING("Game_Version_203");
	}
}


std::string UpdateResoureLayer::getSavePathName(int game_id)
{
	if (game_id == DANAOTIANGO_KINDID)
	{
		return SSTRING("Game_SaveFile_601");
	}
	else if (game_id == BAIJIALE_KINDID)
	{
		return SSTRING("Game_SaveFile_122");
	}
	else if (game_id == ZHAJINHUA_KINDID)
	{
		return SSTRING("Game_SaveFile_6");
	}
	else if (game_id == DOUDIZHU_KINDID)
	{
		return SSTRING("Game_SaveFile_200");
	}
	else if (game_id == SIRENNIUNIU_KINDID)
	{
		return SSTRING("Game_SaveFile_203");
	}
}

UpdateResoureLayer * UpdateResoureLayer::create(int game_id)
{
	UpdateResoureLayer *pRet = new UpdateResoureLayer(); 
	if (pRet && pRet->init(game_id)) 
	{ 
		pRet->autorelease();
		return pRet;
	} 
	else 
	{ 
		delete pRet;
		pRet = NULL;
		return NULL;
	} 
}

bool UpdateResoureLayer::isHaveNewVersion()
{
	return false; getAssetManager()->checkUpdate();
}


