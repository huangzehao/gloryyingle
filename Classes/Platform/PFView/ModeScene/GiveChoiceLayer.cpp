
//#include "ViewHeader.h"
#include "GiveChoiceLayer.h"
#include "Tools/Manager/SoundManager.h"
//#include "MTNotification.h"

#include <cocos/ui/UIImageView.h>
#include <cocos/ui/UIText.h>
#include <cocos/ui/UITextField.h>
#include <cocos/ui/UIButton.h>
#include "TopHeadLayer.h"
#include "Tools/tools/YRCommon.h"
#include "Tools/Dialog/NewDialog.h"
#include "TopHeadLayer.h"
#include "ui/CocosGUI.h"
#include "Tools/tools/StaticData.h"
#include "Platform/PFDefine/data/GlobalUserInfo.h"
USING_NS_CC;
using namespace ui;

//////////////////////////////////////////////////////////////////////////
GiveChoiceLayer::GiveChoiceLayer()
	: mInsureMission(ADDRESS_URL, SERVER_PORT)
{
	mInsureMission.setMissionSink(this);
	m_type = BaseModeLayer::ModeLayerTypeBank;
	m_center_index = 1;
}

GiveChoiceLayer::~GiveChoiceLayer()
{
	mInsureMission.setMissionSink(0);
// 	G_NOTIFY_UNREG("USER_INFO");
// 	G_NOTIFY_UNREG("USER_SCORE");
// 	G_NOTIFY_UNREG("INSURE_QUERY");
// 	SpriteFrameCache::getInstance()->removeSpriteFramesFromFile(PLIST_PATH);
// 	Director::getInstance()->getTextureCache()->removeTextureForKey(TEXTURE_PATH);
}


//初始化方法
bool GiveChoiceLayer::init()
{
	return newInit();
}

bool GiveChoiceLayer::newInit()
{
	do
	{
		CC_BREAK_IF(!BaseLayer::initWithJsonFile("LobbyResources/PlatformBank.json"));

		return newInitCenterPanel();

	} while (0);

	return false;
}

bool GiveChoiceLayer::newInitCenterPanel()
{
	//界面下移
	m_root_widget->setPositionY(850);

	m_root_widget->runAction(CCSequence::create(
		MoveTo::create(0.25f, Vec2(0, 0)),
		MoveTo::create(0.1f, Vec2(0, 100)),
		MoveTo::create(0.05f, Vec2(0, 0)),
		MoveTo::create(0.04f, Vec2(0, 30)),
		MoveTo::create(0.03f, Vec2(0, 0)),
		0));

	//查询当前银行资料
	mInsureMission.query();

	// 存取
	ui::Button * btn_save_0 = dynamic_cast<ui::Button *>(m_root_widget->getChildByName("btn_save_0"));
	btn_save_0->setTouchEnabled(false);
	btn_save_0->setBright(false);
	// 赠送
	ui::Button * btn_give_0 = dynamic_cast<ui::Button *>(m_root_widget->getChildByName("btn_give_0"));
	// 记录
	ui::Button * btn_menulist_0 = dynamic_cast<ui::Button *>(m_root_widget->getChildByName("btn_menulist_0"));

	Node * panel_1 = m_root_widget->getChildByName(StringUtils::format("panel_1"));
	Node * panel_2 = m_root_widget->getChildByName(StringUtils::format("panel_2"));
	Node * panel_3 = m_root_widget->getChildByName(StringUtils::format("panel_3"));

	ui::ImageView * img_logo_1 = dynamic_cast<ui::ImageView *>(m_root_widget->getChildByName("img_logoBg")->getChildByName("img_logo_1"));
	ui::ImageView * img_logo_2 = dynamic_cast<ui::ImageView *>(m_root_widget->getChildByName("img_logoBg")->getChildByName("img_logo_2"));
	ui::ImageView * img_logo_3 = dynamic_cast<ui::ImageView *>(m_root_widget->getChildByName("img_logoBg")->getChildByName("img_logo_3"));

	btn_save_0->addTouchEventListener([=](Ref* ref_, ui::Widget::TouchEventType event_type_)
	{
		if (event_type_ == ui::Widget::TouchEventType::BEGAN)
		{
			PLAY_BUTTON_CLICK_EFFECT
		}
		if (event_type_ == ui::Widget::TouchEventType::ENDED)
		{
			//查询当前银行资料
			mInsureMission.query();

			btn_save_0->setTouchEnabled(false);
			btn_save_0->setBright(false);
			btn_give_0->setTouchEnabled(true);
			btn_give_0->setBright(true);
			btn_menulist_0->setTouchEnabled(true);
			btn_menulist_0->setBright(true);

			panel_1->setVisible(true);
			panel_2->setVisible(false);
			panel_3->setVisible(false);

			img_logo_1->setVisible(true);
			img_logo_2->setVisible(false);
			img_logo_3->setVisible(false);

			// 刷新信息
			this->flushPanel_1();
		}
	});

	
	btn_give_0->addTouchEventListener([=](Ref* ref_, ui::Widget::TouchEventType event_type_)
	{
		if (event_type_ == ui::Widget::TouchEventType::BEGAN)
		{
			PLAY_BUTTON_CLICK_EFFECT
		}
		if (event_type_ == ui::Widget::TouchEventType::ENDED)
		{
			//查询当前银行资料
			mInsureMission.query();

			btn_save_0->setTouchEnabled(true);
			btn_save_0->setBright(true);
			btn_give_0->setTouchEnabled(false);
			btn_give_0->setBright(false);
			btn_menulist_0->setTouchEnabled(true);
			btn_menulist_0->setBright(true);

			panel_1->setVisible(false);
			panel_2->setVisible(true);
			panel_3->setVisible(false);

			img_logo_1->setVisible(false);
			img_logo_2->setVisible(true);
			img_logo_3->setVisible(false);

			this->flushPanel_2();
		}
	});

	
	btn_menulist_0->addTouchEventListener([=](Ref* ref_, ui::Widget::TouchEventType event_type_)
	{
		if (event_type_ == ui::Widget::TouchEventType::BEGAN)
		{
			PLAY_BUTTON_CLICK_EFFECT
		}
		if (event_type_ == ui::Widget::TouchEventType::ENDED)
		{
			
			btn_save_0->setTouchEnabled(true);
			btn_save_0->setBright(true);
			btn_give_0->setTouchEnabled(true);
			btn_give_0->setBright(true);
			btn_menulist_0->setTouchEnabled(false);
			btn_menulist_0->setBright(false);

			panel_1->setVisible(false);
			panel_2->setVisible(false);
			panel_3->setVisible(true);

			img_logo_1->setVisible(false);
			img_logo_2->setVisible(false);
			img_logo_3->setVisible(true);

			this->flushPanel_3();
			mInsureMission.queryAccessRecord();
		}
	});

	//关闭按钮
	Button * btn_return = dynamic_cast<Button *>(m_root_widget->getChildByName("btn_return"));
	CCAssert(btn_return, "");
	btn_return->addTouchEventListener([=](cocos2d::Ref * ref_, ui::Widget::TouchEventType eventType_)
	{
		if (eventType_ == Widget::TouchEventType::BEGAN)
		{
			PLAY_BUTTON_CLOSE_EFFECT
		}
		if (eventType_ == Widget::TouchEventType::ENDED)
		{
			m_root_widget->runAction(CCSequence::create(MoveTo::create(0.2f, Vec2(0, 850)), CCCallFunc::create(this, callfunc_selector(GiveChoiceLayer::closeMoveTo)), NULL));
		}
	});

	newInitPanel_1();
	newInitPanel_2();
	newInitPanel_3();

	return true;
}

void GiveChoiceLayer::closeMoveTo()
{
	this->removeFromParent();
}

void GiveChoiceLayer::newInitPanel_1()
{
	CGlobalUserInfo * pGlobalUserInfo = CGlobalUserInfo::GetInstance();
	tagGlobalUserData * pGlobalUserData = pGlobalUserInfo->GetGlobalUserData();

	Node *center_panel_ = m_root_widget->getChildByName(StringUtils::format("panel_1"));
	CCAssert(center_panel_, "");

	TextAtlas * cur_money_panel_1 = dynamic_cast<ui::TextAtlas*>(center_panel_->getChildByName("Panel_1")->getChildByName("al_gane_num"));
	if (cur_money_panel_1){
		cur_money_panel_1->setString(StringUtils::format("%lld", pGlobalUserData->lUserScore));
	}

	TextAtlas * store_money_panel_2 = dynamic_cast<ui::TextAtlas*>(center_panel_->getChildByName("Panel_2")->getChildByName("al_bank_num"));
	if (store_money_panel_2){
		store_money_panel_2->setString(StringUtils::format("%lld", pGlobalUserData->lUserInsure));
	}

	txt_save_money_1 = dynamic_cast<ui::TextField*>(center_panel_->getChildByName("Panel_3")->getChildByName("tf_amount"));
	CCAssert(txt_save_money_1, "");

	txt_store_pwd_ = dynamic_cast<ui::TextField*>(center_panel_->getChildByName("Panel_4")->getChildByName("tf_password"));
	CCAssert(txt_store_pwd_, "");
	
	ui::Button *btn_save_ = dynamic_cast<ui::Button*>(center_panel_->getChildByName("btn_pop"));
	CCAssert(btn_save_, "");
	btn_save_->addTouchEventListener([=](Ref* ref_, ui::Widget::TouchEventType event_type_){
		if (event_type_ == ui::Widget::TouchEventType::BEGAN)
		{
			PLAY_BUTTON_CLICK_EFFECT
		}
		if (event_type_ == ui::Widget::TouchEventType::ENDED)
		{
			
			//mInsureMission.enable("111111", "222222");			
			mInsureMission.save(atoll(txt_save_money_1->getString().c_str())); //存入金币

		}
	});

	ui::Button *btn_get_ = dynamic_cast<ui::Button*>(center_panel_->getChildByName("btn_push"));
	CCAssert(btn_get_, "");
	btn_get_->addTouchEventListener([=](Ref* ref_, ui::Widget::TouchEventType event_type_){
		if (event_type_ == ui::Widget::TouchEventType::BEGAN)
		{
			PLAY_BUTTON_CLICK_EFFECT
		}
		if (event_type_ == ui::Widget::TouchEventType::ENDED)
		{	
			mInsureMission.take(atoll(txt_save_money_1->getString().c_str()), txt_store_pwd_->getString().c_str()); //取出
		}
	});

	//+1万
	ui::Button *btn_1w = dynamic_cast<ui::Button*>(center_panel_->getChildByName("btn_1w"));
	btn_1w->addTouchEventListener([=](Ref* ref_, ui::Widget::TouchEventType event_type_){
		if (event_type_ == ui::Widget::TouchEventType::BEGAN)
		{
			PLAY_BUTTON_CLICK_EFFECT
		}
		if (event_type_ == ui::Widget::TouchEventType::ENDED)
		{
			std::string totalMoney = StringUtils::format("%lld", atoll(txt_save_money_1->getString().c_str()) + 10000);
			txt_save_money_1->setString(totalMoney);
		}
	});
	//+10万
	ui::Button *btn_10w = dynamic_cast<ui::Button*>(center_panel_->getChildByName("btn_10w"));
	btn_10w->addTouchEventListener([=](Ref* ref_, ui::Widget::TouchEventType event_type_){
		if (event_type_ == ui::Widget::TouchEventType::BEGAN)
		{
			PLAY_BUTTON_CLICK_EFFECT
		}
		if (event_type_ == ui::Widget::TouchEventType::ENDED)
		{
			std::string totalMoney = StringUtils::format("%lld", atoll(txt_save_money_1->getString().c_str()) + 100000);
			txt_save_money_1->setString(totalMoney);
		}
	});
	//+100万
	ui::Button *btn_100w = dynamic_cast<ui::Button*>(center_panel_->getChildByName("btn_100w"));
	btn_100w->addTouchEventListener([=](Ref* ref_, ui::Widget::TouchEventType event_type_){
		if (event_type_ == ui::Widget::TouchEventType::BEGAN)
		{
			PLAY_BUTTON_CLICK_EFFECT
		}
		if (event_type_ == ui::Widget::TouchEventType::ENDED)
		{
			std::string totalMoney = StringUtils::format("%lld", atoll(txt_save_money_1->getString().c_str()) + 1000000);
			txt_save_money_1->setString(totalMoney);
		}
	});
	//+1000万
	ui::Button *btn_1000w = dynamic_cast<ui::Button*>(center_panel_->getChildByName("btn_1000w"));
	btn_1000w->addTouchEventListener([=](Ref* ref_, ui::Widget::TouchEventType event_type_){
		if (event_type_ == ui::Widget::TouchEventType::BEGAN)
		{
			PLAY_BUTTON_CLICK_EFFECT
		}
		if (event_type_ == ui::Widget::TouchEventType::ENDED)
		{
			std::string totalMoney = StringUtils::format("%lld", atoll(txt_save_money_1->getString().c_str()) + 10000000);
			txt_save_money_1->setString(totalMoney);
		}
	});
	//清除
	ui::Button *btn_clear = dynamic_cast<ui::Button*>(center_panel_->getChildByName("btn_clear"));
	btn_clear->addTouchEventListener([=](Ref* ref_, ui::Widget::TouchEventType event_type_){
		if (event_type_ == ui::Widget::TouchEventType::BEGAN)
		{
			PLAY_BUTTON_CLICK_EFFECT
		}
		if (event_type_ == ui::Widget::TouchEventType::ENDED)
		{
			txt_save_money_1->setString("");
		}
	});


}

void GiveChoiceLayer::newInitPanel_2()
{
	Node *center_panel_ = m_root_widget->getChildByName(StringUtils::format("panel_2"));
	CCAssert(center_panel_, "");

	Panel_Transfer = dynamic_cast<Layout *>(m_root_widget->getChildByName("panel_2")->getChildByName("Panel_Transfer"));

	Panel_cell_Transfer = dynamic_cast<Layout *>(Panel_Transfer->getChildByName("Panel_cell"));

	lv_record_Transfer = dynamic_cast<ui::ListView*>(Panel_Transfer->getChildByName("lv_record"));

	lab_into = dynamic_cast<ui::Text*>(Panel_Transfer->getChildByName("lab_into"));

	lab_rollout = dynamic_cast<ui::Text*>(Panel_Transfer->getChildByName("lab_rollout"));

	TextAtlas * cur_money_panel_2 = dynamic_cast<ui::TextAtlas*>(center_panel_->getChildByName("Panel_1")->getChildByName("al_gane_num"));
	if (cur_money_panel_2){
		//  加载资源
		
	}

	TextAtlas * store_money_panel_2 = dynamic_cast<ui::TextAtlas*>(center_panel_->getChildByName("Panel_2")->getChildByName("al_bank_num"));
	if (store_money_panel_2){
		//  加载资源
		
	}

	txt_game_id_ = dynamic_cast<ui::TextField*>(center_panel_->getChildByName("Panel_5")->getChildByName("tf_gamer_id"));
	CCAssert(txt_game_id_, "");
// 	txt_game_id_->addTouchEventListener([=](Ref* ref_, ui::Widget::TouchEventType event_type_){
// 		if (event_type_ == ui::Widget::TouchEventType::ENDED)
// 		{
// 
// 		}
// 	});

	tf_game_num_ = dynamic_cast<ui::TextField*>(center_panel_->getChildByName("Panel_3")->getChildByName("tf_amount"));
	CCAssert(tf_game_num_, "");

// 	tf_game_num_->addTouchEventListener([=](Ref* ref_, ui::Widget::TouchEventType event_type_){
// 		if (event_type_ == ui::Widget::TouchEventType::ENDED)
// 		{
// 
// 		}
// 	});

	txt_store_pwd2_ = dynamic_cast<ui::TextField*>(center_panel_->getChildByName("Panel_4")->getChildByName("tf_password"));
	CCAssert(txt_store_pwd2_, "");

// 	txt_store_pwd_->addTouchEventListener([=](Ref* ref_, ui::Widget::TouchEventType event_type_){
// 		if (event_type_ == ui::Widget::TouchEventType::ENDED)
// 		{
// 
// 		}
// 	});

	// 确认点击
	ui::Button *btn_save_ = dynamic_cast<ui::Button*>(center_panel_->getChildByName("btn_affrirm"));
	CCAssert(btn_save_, "");
	btn_save_->addTouchEventListener([=](Ref* ref_, ui::Widget::TouchEventType event_type_){
		if (event_type_ == ui::Widget::TouchEventType::BEGAN)
		{
			PLAY_BUTTON_CLICK_EFFECT
		}
		if (event_type_ == ui::Widget::TouchEventType::ENDED)
		{
			
			//mInsureMission.enable("111111", "222222");			
			//mInsureMission.save(Value(txt_save_money_->getText()).asInt()); //存入金币
			// 差个人物id,有点问题,先不急着解决了.
			CGlobalUserInfo * pGlobalUserInfo = CGlobalUserInfo::GetInstance();
			tagGlobalUserData * pGlobalUserData = pGlobalUserInfo->GetGlobalUserData();

			mInsureMission.transfer(atoll(tf_game_num_->getString().c_str()), txt_store_pwd2_->getString().c_str(), txt_game_id_->getString().c_str(), pGlobalUserData->dwUserID);
		}
	});

	//+1万
	ui::Button *btn_1w = dynamic_cast<ui::Button*>(center_panel_->getChildByName("btn_1w"));
	btn_1w->addTouchEventListener([=](Ref* ref_, ui::Widget::TouchEventType event_type_){
		if (event_type_ == ui::Widget::TouchEventType::BEGAN)
		{
			PLAY_BUTTON_CLICK_EFFECT
		}
		if (event_type_ == ui::Widget::TouchEventType::ENDED)
		{
			std::string totalMoney = StringUtils::format("%lld", atoll(tf_game_num_->getString().c_str()) + 10000);
			tf_game_num_->setString(totalMoney);
		}
	});
	//+10万
	ui::Button *btn_10w = dynamic_cast<ui::Button*>(center_panel_->getChildByName("btn_10w"));
	btn_10w->addTouchEventListener([=](Ref* ref_, ui::Widget::TouchEventType event_type_){
		if (event_type_ == ui::Widget::TouchEventType::BEGAN)
		{
			PLAY_BUTTON_CLICK_EFFECT
		}
		if (event_type_ == ui::Widget::TouchEventType::ENDED)
		{
			std::string totalMoney = StringUtils::format("%lld", atoll(tf_game_num_->getString().c_str()) + 100000);
			tf_game_num_->setString(totalMoney);
		}
	});
	//+100万
	ui::Button *btn_100w = dynamic_cast<ui::Button*>(center_panel_->getChildByName("btn_100w"));
	btn_100w->addTouchEventListener([=](Ref* ref_, ui::Widget::TouchEventType event_type_){
		if (event_type_ == ui::Widget::TouchEventType::BEGAN)
		{
			PLAY_BUTTON_CLICK_EFFECT
		}
		if (event_type_ == ui::Widget::TouchEventType::ENDED)
		{
			std::string totalMoney = StringUtils::format("%lld", atoll(tf_game_num_->getString().c_str()) + 1000000);
			tf_game_num_->setString(totalMoney);
		}
	});
	//+1000万
	ui::Button *btn_1000w = dynamic_cast<ui::Button*>(center_panel_->getChildByName("btn_1000w"));
	btn_1000w->addTouchEventListener([=](Ref* ref_, ui::Widget::TouchEventType event_type_){
		if (event_type_ == ui::Widget::TouchEventType::BEGAN)
		{
			PLAY_BUTTON_CLICK_EFFECT
		}
		if (event_type_ == ui::Widget::TouchEventType::ENDED)
		{
			std::string totalMoney = StringUtils::format("%lld", atoll(tf_game_num_->getString().c_str()) + 10000000);
			tf_game_num_->setString(totalMoney);
		}
	});
	//清除
	ui::Button *btn_clear = dynamic_cast<ui::Button*>(center_panel_->getChildByName("btn_clear"));
	btn_clear->addTouchEventListener([=](Ref* ref_, ui::Widget::TouchEventType event_type_){
		if (event_type_ == ui::Widget::TouchEventType::BEGAN)
		{
			PLAY_BUTTON_CLICK_EFFECT
		}
		if (event_type_ == ui::Widget::TouchEventType::ENDED)
		{
			tf_game_num_->setString("");
		}
	});
	//转账记录
	ui::Button *btn_transfer = dynamic_cast<ui::Button*>(center_panel_->getChildByName("btn_transfer"));
	btn_transfer->addTouchEventListener([=](Ref* ref_, ui::Widget::TouchEventType event_type_){
		if (event_type_ == ui::Widget::TouchEventType::BEGAN)
		{
			PLAY_BUTTON_CLICK_EFFECT
		}
		if (event_type_ == ui::Widget::TouchEventType::ENDED)
		{
			rollOutScore = 0;
			inToScore = 0;
			lv_record_Transfer->removeAllChildren();
			lv_record_Transfer->jumpToTop();
			mInsureMission.queryTransfeRecord();
			Panel_Transfer->setVisible(true);
		}
	});
	//转账记录
	ui::Button *btn_exit = dynamic_cast<ui::Button*>(Panel_Transfer->getChildByName("btn_exit"));
	btn_exit->addTouchEventListener([=](Ref* ref_, ui::Widget::TouchEventType event_type_){
		if (event_type_ == ui::Widget::TouchEventType::BEGAN)
		{
			PLAY_BUTTON_CLICK_EFFECT
		}
		if (event_type_ == ui::Widget::TouchEventType::ENDED)
		{
			Panel_Transfer->setVisible(false);
		}
	});
}

void GiveChoiceLayer::newInitPanel_3()
{
	Panel_cell = dynamic_cast<Layout *>(m_root_widget->getChildByName("panel_3")->getChildByName("Panel_cell"));

	lv_record = dynamic_cast<ui::ListView*>(m_root_widget->getChildByName("panel_3")->getChildByName("lv_record"));

	img_none = dynamic_cast<ui::ImageView*>(m_root_widget->getChildByName("panel_3")->getChildByName("img_none"));
	img_none->setVisible(true);
}

void GiveChoiceLayer::flushPanel_1()
{
	txt_save_money_1->setString("");
	txt_store_pwd_->setString("");

	CGlobalUserInfo * pGlobalUserInfo = CGlobalUserInfo::GetInstance();
	tagGlobalUserData * pGlobalUserData = pGlobalUserInfo->GetGlobalUserData();

	Node *center_panel_ = m_root_widget->getChildByName(StringUtils::format("panel_1"));
	CCAssert(center_panel_, "");

	ui::TextAtlas *cur_money_ = dynamic_cast<ui::TextAtlas*>(center_panel_->getChildByName("Panel_1")->getChildByName("al_gane_num"));
	if (cur_money_){
		cur_money_->setString(StringUtils::format("%lld", pGlobalUserData->lUserScore));
	}

	ui::TextAtlas *store_money_ = dynamic_cast<ui::TextAtlas*>(center_panel_->getChildByName("Panel_2")->getChildByName("al_bank_num"));
	if (store_money_){
		store_money_->setString(StringUtils::format("%lld", pGlobalUserData->lUserInsure));
	}
}

void  GiveChoiceLayer::flushPanel_2()
{
	txt_game_id_->setString("");
	tf_game_num_->setString("");
	txt_store_pwd2_->setString("");

	CGlobalUserInfo * pGlobalUserInfo = CGlobalUserInfo::GetInstance();
	tagGlobalUserData * pGlobalUserData = pGlobalUserInfo->GetGlobalUserData();

	Node *center_panel_ = m_root_widget->getChildByName(StringUtils::format("panel_2"));
	CCAssert(center_panel_, "");
	ui::TextAtlas *cur_money_ = dynamic_cast<ui::TextAtlas*>(center_panel_->getChildByName("Panel_1")->getChildByName("al_gane_num"));
	if (cur_money_){	
		cur_money_->setString(StringUtils::format("%lld", pGlobalUserData->lUserScore));
	}

	ui::TextAtlas *store_money_ = dynamic_cast<ui::TextAtlas*>(center_panel_->getChildByName("Panel_2")->getChildByName("al_bank_num"));
	if (store_money_){
		store_money_->setString(StringUtils::format("%lld", pGlobalUserData->lUserInsure));
	}
}

void  GiveChoiceLayer::flushPanel_3()
{
	lv_record->removeAllChildren();
	lv_record->jumpToTop();
}

void GiveChoiceLayer::onEnterTransitionDidFinish()
{
	Layer::onEnterTransitionDidFinish();

	CGlobalUserInfo * pGlobalUserInfo = CGlobalUserInfo::GetInstance();
	tagGlobalUserData * pGlobalUserData = pGlobalUserInfo->GetGlobalUserData();
 	if (pGlobalUserData->cbInsureEnabled)
 		mInsureMission.query();

}

bool GiveChoiceLayer::initCenterPanel()
{
	Node* content_bg_ = m_root_widget->getChildByName("content_bg");
	if (!content_bg_){
		log(" ServerListLayer RoomList json can not find content_bg_...");
		return false;
	}

	Node *center_panel_ = content_bg_->getChildByName(StringUtils::format( "center_panel%d", m_center_index));
	CCAssert(center_panel_, "");

	ui::Text *cur_money_ = dynamic_cast<ui::Text*>(center_panel_->getChildByName("cur_money"));
	if (cur_money_){
		//  加载资源
	}

	ui::Text *store_money_ = dynamic_cast<ui::Text*>(center_panel_->getChildByName("store_money"));
	if (store_money_){
		//  加载资源
	}

	ui::TextField *txt_save_money_ = dynamic_cast<ui::TextField*>(center_panel_->getChildByName("save_num")->getChildByName("txt"));
	CCAssert(txt_save_money_, "");
	txt_save_money_->addTouchEventListener([=](Ref* ref_, ui::Widget::TouchEventType event_type_){
		if (event_type_ == ui::Widget::TouchEventType::ENDED)
		{

		}
	});

	ui::TextField *txt_store_pwd_ = dynamic_cast<ui::TextField*>(center_panel_->getChildByName("store_pwd")->getChildByName("txt"));
	CCAssert(txt_store_pwd_, "");
	txt_store_pwd_->addTouchEventListener([=](Ref* ref_, ui::Widget::TouchEventType event_type_){
		if (event_type_ == ui::Widget::TouchEventType::ENDED)
		{

		}
	});

	ui::Button *btn_save_ = dynamic_cast<ui::Button*>(center_panel_->getChildByName("btn_save"));
	CCAssert(btn_save_, "");
	btn_save_->addTouchEventListener([=](Ref* ref_, ui::Widget::TouchEventType event_type_){
		if (event_type_ == ui::Widget::TouchEventType::BEGAN)
		{
			PLAY_BUTTON_CLICK_EFFECT
		}
		if (event_type_ == ui::Widget::TouchEventType::ENDED)
		{
			
			//mInsureMission.enable("111111", "222222");			
			mInsureMission.save(Value(txt_save_money_->getString()).asInt()); //存入金币
		}
	});

	ui::Button *btn_get_ = dynamic_cast<ui::Button*>(center_panel_->getChildByName("btn_get"));
	CCAssert(btn_get_, "");
	btn_get_->addTouchEventListener([=](Ref* ref_, ui::Widget::TouchEventType event_type_){
		if (event_type_ == ui::Widget::TouchEventType::BEGAN)
		{
			PLAY_BUTTON_CLICK_EFFECT
		}
		if (event_type_ == ui::Widget::TouchEventType::ENDED)
		{
			
			mInsureMission.take(Value(txt_save_money_->getString()).asInt(), txt_store_pwd_->getString().c_str()); //取出
		}
	});

	return true;
}



void GiveChoiceLayer::setDatas()
{
	onUserName(0);
	onUserScore(0);
}


void GiveChoiceLayer::setCloseDialogInfo(cocos2d::Ref* target, cocos2d::SEL_CallFuncN callfun, const std::string& title, const std::string& content)
{
	mTarget		= target;
	mCallback	= callfun;
	mTitle		= title;
	mContent	= content;
}

//////////////////////////////////////////////////////////////////////////
//通知消息
void GiveChoiceLayer::onUserName(cocos2d::Ref* obj)
{
}

void GiveChoiceLayer::onUserScore(cocos2d::Ref* obj)
{
	//变量定义
	CGlobalUserInfo * pGlobalUserInfo=CGlobalUserInfo::GetInstance();
 	tagUserInsureInfo * pUserInsureInfo=pGlobalUserInfo->GetUserInsureInfo();
 	tagGlobalUserData * pGlobalUserData=pGlobalUserInfo->GetGlobalUserData();

	//刷新银行资料数据
	flushPanel_1();
	flushPanel_2();
}

void GiveChoiceLayer::onInsureQuery(cocos2d::Ref* obj)
{
	return;
}

void GiveChoiceLayer::onDelayQuery(float delta)
{
	mInsureMission.query();
}

//////////////////////////////////////////////////////////////////////////
// IGPInsureMissionSink
void GiveChoiceLayer::onInsureInfo()
{
	onUserScore(0);
}

void GiveChoiceLayer::onInsureSuccess(int type, const char* szDescription)
{
	CGlobalUserInfo * pGlobalUserInfo = CGlobalUserInfo::GetInstance();
	tagGlobalUserData * pGlobalUserData = pGlobalUserInfo->GetGlobalUserData();
	//刷新银行资料数据
	flushPanel_1();
	flushPanel_2();

	NewDialog::create(szDescription, NewDialog::NONEBUTTON);
}

void GiveChoiceLayer::onInsureFailure(int type, const char* szDescription)
{
	NewDialog::create(szDescription, NewDialog::NONEBUTTON);
}

bool GiveChoiceLayer::onInsureTransferConfirm(const char* szMessage)
{

	return true;
}

void GiveChoiceLayer::onInsureEnableResult(int type, const char* szMessage)
{

}
void GiveChoiceLayer::onAccessRecord(tagUserAccessRecord * pUserAccessRecord)
{
	if (lv_record->getChildrenCount() == 0)
		img_none->setVisible(false);
	Widget *center_cell = Panel_cell->clone();
	Label_wYear = dynamic_cast<ui::Text*>(center_cell->getChildByName("Label_wYear"));
	Label_wMonth = dynamic_cast<ui::Text*>(center_cell->getChildByName("Label_wMonth"));
	Label_wDay = dynamic_cast<ui::Text*>(center_cell->getChildByName("Label_wDay"));
	Label_transfer = dynamic_cast<ui::Text*>(center_cell->getChildByName("Label_transfer"));
	Label_lScore = dynamic_cast<ui::Text*>(center_cell->getChildByName("Label_lScore"));
	img_type = dynamic_cast<ui::ImageView*>(center_cell->getChildByName("img_type"));

	Label_wYear->setString(StringUtils::format("%ld", pUserAccessRecord->wYear));
	Label_wMonth->setString(StringUtils::format("%ld", pUserAccessRecord->wMonth));
	Label_wDay->setString(StringUtils::format("%ld", pUserAccessRecord->wDay));
	Label_lScore->setString(StringUtils::format("%lld", pUserAccessRecord->lScore));

	if (pUserAccessRecord->cbType == 1)
	{
		Label_transfer->setString("存入银行");
		img_type->loadTexture("LobbyResources/BankLayer/img_save.png");
	}
	else
	{
		Label_transfer->setString("银行取出");
		img_type->loadTexture("LobbyResources/BankLayer/img_get.png");
		Label_lScore->setColor(ccc3(255,0,0));
	}

	lv_record->pushBackCustomItem(center_cell);
}

void GiveChoiceLayer::onTransferRecord(tagUserTransferRecord * pTransferUserData)
{
	CGlobalUserInfo * pGlobalUserInfo = CGlobalUserInfo::GetInstance();
	tagGlobalUserData * pGlobalUserData = pGlobalUserInfo->GetGlobalUserData();

	if (pTransferUserData->dwSourceGameID == pGlobalUserData->dwGameID)
	{
		rollOutScore += pTransferUserData->lScore;
		lab_rollout->setString(StringUtils::format("%lld", rollOutScore));
	}
	else
	{
		inToScore += pTransferUserData->lScore;
		lab_into->setString(StringUtils::format("%lld", inToScore));
	}

	Widget *center_cell = Panel_cell_Transfer->clone();
	Text * Label_wYear = dynamic_cast<ui::Text*>(center_cell->getChildByName("Label_wYear"));
	Text * Label_wMonth = dynamic_cast<ui::Text*>(center_cell->getChildByName("Label_wMonth"));
	Text * Label_wDay = dynamic_cast<ui::Text*>(center_cell->getChildByName("Label_wDay"));
	Text * Label_zizhuname = dynamic_cast<ui::Text*>(center_cell->getChildByName("Label_zizhuname"));
	Text * Label_zizhuid = dynamic_cast<ui::Text*>(center_cell->getChildByName("Label_zizhuid"));
	Text * Label_lScore = dynamic_cast<ui::Text*>(center_cell->getChildByName("Label_lScore"));
	Text * Label_jieshouname = dynamic_cast<ui::Text*>(center_cell->getChildByName("Label_jieshouname"));
	Text * Label_jieshouid = dynamic_cast<ui::Text*>(center_cell->getChildByName("Label_jieshouid"));

	Label_wYear->setString(StringUtils::format("%ld", pTransferUserData->wYear));
	Label_wMonth->setString(StringUtils::format("%ld", pTransferUserData->wMonth));
	Label_wDay->setString(StringUtils::format("%ld", pTransferUserData->wDay));
	Label_zizhuname->setString(StringUtils::format("%s", pTransferUserData->szSourceNickName));
	Label_zizhuid->setString(StringUtils::format("%ld", pTransferUserData->dwSourceGameID));
	Label_lScore->setString(StringUtils::format("%lld", pTransferUserData->lScore));
	Label_jieshouname->setString(StringUtils::format("%s", pTransferUserData->szTargetNickName));
	Label_jieshouid->setString(StringUtils::format("%ld", pTransferUserData->dwTargetGameID));

	lv_record_Transfer->pushBackCustomItem(center_cell);
}

void GiveChoiceLayer::onAccessRecordEnd()
{
	lv_record->jumpToTop();
}

void GiveChoiceLayer::onTransferRecordEnd()
{
	lv_record_Transfer->jumpToTop();
}
