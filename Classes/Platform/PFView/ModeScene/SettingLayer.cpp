#include "SettingLayer.h"
#include "Tools/Manager/SoundManager.h"
#include "Platform/PFDefine/GlobalConfig.h"
#include "Tools/tools/YRCommon.h"
#include "Platform/PFView/LoginScene/LoginScene.h"
#include "Tools/Dialog/NewDialog.h"
#include "Tools/tools/StringData.h"
#include "Platform/PFDefine/data/GlobalUserInfo.h"

#define IMG_TURN_OFF "LobbyResources/GameOptions/gui-setting-button-off.png"
#define IMG_TURN_ON "LobbyResources/GameOptions/gui-setting-button-on.png"

SettingLayer::SettingLayer()
{
}

SettingLayer::~SettingLayer()
{
}

bool SettingLayer::init()
{
	if (!BaseLayer::initWithJsonFile("LobbyResources/SettingLayer.json"))
		return false;

	initCompoent();

	return true;
}

void SettingLayer::initCompoent()
{
	CGlobalUserInfo * pGlobalUserInfo = CGlobalUserInfo::GetInstance();
	tagGlobalUserData * pGlobalUserData = pGlobalUserInfo->GetGlobalUserData();

	//界面下移
	m_root_widget->setPositionY(850);

	m_root_widget->runAction(CCSequence::create(
		MoveTo::create(0.25f, Vec2(0, 0)),
		MoveTo::create(0.1f, Vec2(0, 100)),
		MoveTo::create(0.05f, Vec2(0, 0)),
		MoveTo::create(0.04f, Vec2(0, 30)),
		MoveTo::create(0.03f, Vec2(0, 0)),
		0));

	Armature* armature = Armature::create("touxiangkuang");
	armature->getAnimation()->playByIndex(0);
	armature->setAnchorPoint(Vec2(0, 0));
	armature->setScale(0.9);
	armature->setPosition(Vec2(150, 268));
	m_root_widget->addChild(armature);

	img_headPanel = dynamic_cast<ImageView *>(m_root_widget->getChildByName("img_headPanel"));

	img_head = dynamic_cast<ImageView *>(img_headPanel->getChildByName("img_head"));
	img_head->loadTexture(YRComGetHeadImageById(pGlobalUserData->wFaceID));

	lab_name = dynamic_cast<ui::Text*>(img_headPanel->getChildByName("lab_name"));
	lab_name->setString(pGlobalUserData->szNickName);
	
	//切换账号
	btn_switch = dynamic_cast<Button *>(img_headPanel->getChildByName("btn_switch"));
	btn_switch->addTouchEventListener([=](cocos2d::Ref * ref_, ui::Widget::TouchEventType eventType_)
	{
		if (eventType_ == Widget::TouchEventType::BEGAN)
		{
			PLAY_BUTTON_CLICK_EFFECT
		}
		if (eventType_ == Widget::TouchEventType::ENDED)
		{
			///< 确认退出平台
			NewDialog::create(SSTRING("affirm_exit_now_account"), NewDialog::AFFIRMANDCANCEL, [=]()
			{
				///< 确认回调.
				this->removeFromParent();
				Director::getInstance()->pushScene(CCTransitionFade::create(0.5f, LoginScene::create()));
			}
			);
		}
	});

	//播发音乐
	btn_music = dynamic_cast<Button *>(m_root_widget->getChildByName("btn_music"));
 	CCAssert(btn_music, "");
	readMusicState(GlobalConfig::getInstance()->getIsPlayMusic());
	btn_music->addTouchEventListener([=](cocos2d::Ref * ref_, ui::Widget::TouchEventType eventType_)
	{
		if (eventType_ == Widget::TouchEventType::BEGAN)
		{
			PLAY_BUTTON_CLICK_EFFECT
		}
		if (eventType_ == Widget::TouchEventType::ENDED)
		{
			
			static bool isPlay = GlobalConfig::getInstance()->getIsPlayMusic();
			
			changeMusicState(!isPlay);
			/// 变成相反的状态
			isPlay = !isPlay;
		}
	});

	//播发音效
	btn_sound = dynamic_cast<Button *>(m_root_widget->getChildByName("btn_sound"));
 	CCAssert(btn_sound, "");
	readEffectState(GlobalConfig::getInstance()->getIsPlayEffect());
	btn_sound->addTouchEventListener([=](cocos2d::Ref * ref_, ui::Widget::TouchEventType eventType_)
	{
		if (eventType_ == Widget::TouchEventType::BEGAN)
		{
			PLAY_BUTTON_CLICK_EFFECT
		}
		if (eventType_ == Widget::TouchEventType::ENDED)
		{
			
			static bool isPlay = GlobalConfig::getInstance()->getIsPlayEffect();
			changeEffectState(!isPlay);
			/// 变成相反的状态
			isPlay = !isPlay;
		}	
	});

// 	/ 震动按钮
// 		btn_quake = dynamic_cast<Button *>(m_root_widget->getChildByName("btn_quake"));
// 		CCAssert(btn_quake, "");
// 		changeShakeState(GlobalConfig::getInstance()->getIsShake());
// 		btn_quake->addTouchEventListener([=](cocos2d::Ref * ref_, ui::Widget::TouchEventType eventType_)
// 		{
// 			if (eventType_ == Widget::TouchEventType::BEGAN)
// 			{
// 				PLAY_BUTTON_CLICK_EFFECT
// 			}
// 			if (eventType_ == Widget::TouchEventType::ENDED)
// 			{
// 				
// 				static bool isPlay = GlobalConfig::getInstance()->getIsShake();
// 				changeShakeState(!isPlay);
// 				/// 变成相反的状态
// 				isPlay = !isPlay;
// 	
// 	
// 			}
// 		
// 		});
// 	
// 		// 特效按钮
// 		btn_effect = dynamic_cast<Button *>(m_root_widget->getChildByName("btn_effect"));
// 		CCAssert(btn_effect, "");
// 		changeSpecialEffectState(GlobalConfig::getInstance()->getIsSpecialEffect());
// 		btn_effect->addTouchEventListener([=](cocos2d::Ref * ref_, ui::Widget::TouchEventType eventType_)
// 		{
// 			if (eventType_ == Widget::TouchEventType::BEGAN)
// 			{
// 				PLAY_BUTTON_CLICK_EFFECT
// 			}
// 			if (eventType_ == Widget::TouchEventType::ENDED)
// 			{
// 				
// 				static bool isPlay = GlobalConfig::getInstance()->getIsSpecialEffect();
// 				changeSpecialEffectState(!isPlay);
// 				/// 变成相反的状态
// 				isPlay = !isPlay;
// 	
// 	
// 			}
// 			
// 		});
// 	
// 		// 聊天按钮
// 		btn_chat = dynamic_cast<Button *>(m_root_widget->getChildByName("btn_chat"));
// 		CCAssert(btn_chat, "");
// 		changeChatState(GlobalConfig::getInstance()->getIsChat());
// 		btn_chat->addTouchEventListener([=](cocos2d::Ref * ref_, ui::Widget::TouchEventType eventType_)
// 		{
// 			if (eventType_ == Widget::TouchEventType::BEGAN)
// 			{
// 				PLAY_BUTTON_CLICK_EFFECT
// 			}
// 			if (eventType_ == Widget::TouchEventType::ENDED)
// 			{
// 				
// 				static bool isPlay = GlobalConfig::getInstance()->getIsChat();
// 				changeChatState(!isPlay);
// 				/// 变成相反的状态
// 				isPlay = !isPlay;
// 			}
// 		});

	//关闭按钮
	Button * btn_return = dynamic_cast<Button *>(m_root_widget->getChildByName("btn_return"));
	CCAssert(btn_return, "");
	btn_return->addTouchEventListener([=](cocos2d::Ref * ref_, ui::Widget::TouchEventType eventType_)
	{
		if (eventType_ == Widget::TouchEventType::BEGAN)
		{
			PLAY_BUTTON_CLOSE_EFFECT
		}
		if (eventType_ == Widget::TouchEventType::ENDED)
		{
			m_root_widget->runAction(CCSequence::create(MoveTo::create(0.2f, Vec2(0, 850)), CCCallFunc::create(this, callfunc_selector(SettingLayer::closeMoveTo)), NULL));
		}
	});

}

void SettingLayer::changeMusicState(bool isplay)
{
	GlobalConfig::getInstance()->setIsPlayMusic(isplay);
	if (isplay)
	{
		//变成打开
		PLAY_PLATFORM_BG_MUSIC
		btn_music->loadTextureNormal(IMG_TURN_ON);
	}
	else
	{
		//变成关闭
		SoundManager::shared()->stopMusic();
		btn_music->loadTextureNormal(IMG_TURN_OFF);
	}
}

void SettingLayer::changeEffectState(bool isplay)
{
	GlobalConfig::getInstance()->setIsPlayEffect(isplay);
	if (isplay)
	{
		//变成打开
		btn_sound->loadTextureNormal(IMG_TURN_ON);
	}
	else
	{
		//变成关闭
		btn_sound->loadTextureNormal(IMG_TURN_OFF);
	}
}

void SettingLayer::readMusicState(bool isplay)
{
	if (isplay)
	{
		btn_music->loadTextureNormal(IMG_TURN_ON);
	}
	else
	{
		btn_music->loadTextureNormal(IMG_TURN_OFF);
	}
}

void SettingLayer::readEffectState(bool isplay)
{
	if (isplay)
	{
		//变成打开
		btn_sound->loadTextureNormal(IMG_TURN_ON);
	}
	else
	{
		//变成关闭
		btn_sound->loadTextureNormal(IMG_TURN_OFF);
	}
}


void SettingLayer::closeMoveTo()
{
	this->removeFromParent();
}

// void SettingLayer::changeShakeState(bool isshake)
// {
// 	GlobalConfig::getInstance()->setIsShake(isshake);
// 	if (isshake)
// 	{
// 		//变成打开
// 		btn_quake->loadTextureNormal(IMG_TURN_ON);
// 	}
// 	else
// 	{
// 		//变成关闭
// 		btn_quake->loadTextureNormal(IMG_TURN_OFF);
// 	}
// }
// 
// void SettingLayer::changeSpecialEffectState(bool isshow)
// {
// 	GlobalConfig::getInstance()->setIsSpecialEffect(isshow);
// 	if (isshow)
// 	{
// 		//变成打开
// 		btn_effect->loadTextureNormal(IMG_TURN_ON);
// 	}
// 	else
// 	{
// 		//变成关闭
// 		btn_effect->loadTextureNormal(IMG_TURN_OFF);
// 	}
// }
// 
// void SettingLayer::changeChatState(bool ischat)
// {
// 	GlobalConfig::getInstance()->setIsChat(ischat);
// 	if (ischat)
// 	{
// 		//变成打开
// 		btn_chat->loadTextureNormal(IMG_TURN_ON);
// 		
// 	}
// 	else
// 	{
// 		//变成关闭
// 		btn_chat->loadTextureNormal(IMG_TURN_OFF);
// 	}
// }

void SettingLayer::onExitTransitionDidStart()
{
	BaseLayer::onExitTransitionDidStart();

	GlobalConfig::getInstance()->saveGlobalConfig();
}

