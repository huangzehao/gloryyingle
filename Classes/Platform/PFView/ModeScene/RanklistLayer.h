#ifndef _RanklistLayer_H_
#define _RanklistLayer_H_

#include "cocos2d.h"
#include "Platform/PFView/BaseLayer.h"
#include "Platform/PFKernel/CGPRanklistMission.h"
#include "ui/CocosGUI.h"

USING_NS_CC;

class RanklistLayer : public BaseLayer, ICGPRanklistMissionLink
{
public:
	//创建场景
	static cocos2d::CCScene* scene();
	//创建方法
	CREATE_FUNC(RanklistLayer);

public:
	//构造函数
	RanklistLayer();
	~RanklistLayer();
	//初始化方法
	virtual bool init();

	virtual void onAddRank(tagUserRankingResult * pUserRankingResult);		//添加排行记录
	virtual void onAddRankEnd();											//添加排行记录结束
private:
	//初始化页面的基本纹理
	bool setUpdateView();
	///< 关闭回调
	void closeButtonTouch(cocos2d::Ref * ref, ui::TouchEventType eventType_);
	///<绘制排行网页
	void drawRankWab();
	///<关闭移动回调
	void closeMoveTo();
private:
	ui::Layout * Panel_bg;
	ui::ImageView * img_bg;
	ui::Layout *Panel_cell;
	ui::ListView *lv_rank;
	ui::ImageView * img_num;
	ui::ImageView * img_head;
	ui::Text * lab_name;
	ui::TextAtlas * al_score;
	ui::Button * btn_close;
	int num;
	///< 回调接口
	CGPRanklistMission mRanklistMission;
};
#endif // _RanklistLayer_H_