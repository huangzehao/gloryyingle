#ifndef _PLAFORMBELOWBTN_H_
#define _PLAFORMBELOWBTN_H_

#include "cocos2d.h"
#include "Platform/PFView/BaseLayer.h"
#include "Platform/PFKernel/CGPIndividualMission.h"
#include "ui/CocosGUI.h"

USING_NS_CC;

class PlatformBelowBtn
	: public BaseLayer,
	public IGPIndividualMissionSink
{
public:
	//创建方法
	CREATE_FUNC(PlatformBelowBtn);

public:
	//构造函数
	PlatformBelowBtn();
	~PlatformBelowBtn();
	//初始化方法
	virtual bool init();

	//分享菜单
	void showShareMenuClickHandler();

	//移除分享网页
	void removeShareWebview(float dt);
private:
	//初始化页面的基本纹理
	bool setUpdateView();
	///< 关闭回调
	void closeButtonTouch(cocos2d::Ref * ref, ui::TouchEventType eventType_);

private:
	//查询个人资料
	CGPIndividualMission mIndividualMission;
};
#endif // _PayLayer_H_