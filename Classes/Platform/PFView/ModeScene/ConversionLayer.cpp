#include "ConversionLayer.h"
#include "TopHeadLayer.h"
#include "Tools/Manager/SoundManager.h"
#include "ui/CocosGUI.h"
#include "Tools/Dialog/NewDialog.h"
#include "Tools/tools/Convert.h"
#include "Tools/tools/YRCommon.h"
#include "Platform/PFDefine/data/GlobalUserInfo.h"
#include "Tools/tools/StaticData.h"
#include "Tools/tools/StringData.h"
USING_NS_CC;
using namespace ui;

CCScene* ConversionLayer::scene()
{
	CCScene* scene = CCScene::create();

	if (!scene)
		return 0;

	ConversionLayer* layer = ConversionLayer::create();
	if (layer)
		scene->addChild(layer);
	return scene;
}

//////////////////////////////////////////////////////////////////////////
ConversionLayer::ConversionLayer()
: mConversionMission(ADDRESS_URL, SERVER_PORT)
{
	mConversionMission.setMissionSink(this);
}

ConversionLayer::~ConversionLayer()
{
	mConversionMission.setMissionSink(0);
}

//初始化方法
bool ConversionLayer::init()
{
	do
	{
		CC_BREAK_IF(!BaseLayer::initWithJsonFile("LobbyResources/ConversionLayer.json"));
		CC_BREAK_IF(!this->setUpdateView());

		return true;
	} while (0);

	return false;
}

// 初始化组件
bool ConversionLayer::setUpdateView()
{
	do
	{
		CGlobalUserInfo * pGlobalUserInfo = CGlobalUserInfo::GetInstance();
		tagGlobalUserData * pGlobalUserData = pGlobalUserInfo->GetGlobalUserData();

		Panel_bg = dynamic_cast<Layout *>(m_root_widget->getParent()->getChildByName("Panel_bg"));

		img_bg = dynamic_cast<ui::ImageView *>(m_root_widget->getChildByName("img_bg"));

		btn_close = dynamic_cast<ui::Button *>(img_bg->getChildByName("btn_close"));
		btn_close->addTouchEventListener(this, SEL_TouchEvent(&ConversionLayer::closeButtonTouch));

		btn_confrim = dynamic_cast<ui::Button *>(img_bg->getChildByName("btn_confrim"));
		btn_confrim->addTouchEventListener(this, SEL_TouchEvent(&ConversionLayer::confrimButtonTouch));

		tf_duihuan = static_cast<ui::TextField *>(img_bg->getChildByName("Panel_duihuan")->getChildByName("tf_duihuan"));
		tf_duihuan->addEventListenerTextField(this, SEL_TextFieldEvent(&ConversionLayer::enterCallBack));

		lab_youxibi = dynamic_cast<ui::Text *>(img_bg->getChildByName("lab_youxibi"));
		al_yuanbao = dynamic_cast<ui::TextAtlas *>(img_bg->getChildByName("al_yuanbao"));
		al_youxibi = dynamic_cast<ui::TextAtlas *>(img_bg->getChildByName("al_youxibi"));

		btn_all = dynamic_cast<ui::Button*>(img_bg->getChildByName("btn_all"));
		btn_all->addTouchEventListener([=](Ref* ref_, ui::Widget::TouchEventType event_type_){
			if (event_type_ == ui::Widget::TouchEventType::BEGAN)
			{
				PLAY_BUTTON_CLICK_EFFECT
			}
			if (event_type_ == ui::Widget::TouchEventType::ENDED)
			{
				tf_duihuan->setString(StringUtils::format("%lld", pGlobalUserData->lUserIngot));
				al_youxibi->setString(StringUtils::format("%lld", pGlobalUserData->lUserIngot * m_ExchangeRate));
			}
		});

		//界面下移
		m_root_widget->setPositionY(850);

		m_root_widget->runAction(CCSequence::create(
			MoveTo::create(0.25f, Vec2(0, 0)),
			MoveTo::create(0.1f, Vec2(0, 100)),
			MoveTo::create(0.05f, Vec2(0, 0)),
			MoveTo::create(0.04f, Vec2(0, 30)),
			MoveTo::create(0.03f, Vec2(0, 0)),
			NULL));

		return true;
	} while (0);

	return false;
}

void ConversionLayer::closeButtonTouch(cocos2d::Ref * ref, ui::TouchEventType eventType_)
{
	if (eventType_ == TouchEventType::TOUCH_EVENT_BEGAN)
	{
		PLAY_BUTTON_CLOSE_EFFECT
	}
	if (eventType_ == TouchEventType::TOUCH_EVENT_ENDED)
	{
		Panel_bg->setVisible(false);
		m_root_widget->runAction(CCSequence::create(MoveTo::create(0.2f, Vec2(0, 850)), CCCallFunc::create(this, callfunc_selector(ConversionLayer::closeMoveTo)), NULL));
	}
}

void ConversionLayer::closeMoveTo()
{
	this->removeFromParent();
}

void ConversionLayer::confrimButtonTouch(cocos2d::Ref * ref, ui::TouchEventType eventType_)
{
	if (eventType_ == TouchEventType::TOUCH_EVENT_BEGAN)
	{
		PLAY_BUTTON_CLICK_EFFECT
	}
	if (eventType_ == TouchEventType::TOUCH_EVENT_ENDED)
	{
		if (utf8_len(tf_duihuan->getString().c_str()) == 0)
		{
			NewDialog::create(SSTRING("Conversion_Chinese_2"), NewDialog::NONEBUTTON);
			return;
		}
		mConversionMission.done(atoll(tf_duihuan->getString().c_str()));
	}
}

void ConversionLayer::onEnterTransitionDidFinish()
{
	BaseLayer::onEnterTransitionDidFinish();
	mConversionMission.query();
}

void ConversionLayer::onConversionQueryInfoResult(word wExchangeRate)
{
	CGlobalUserInfo * pGlobalUserInfo = CGlobalUserInfo::GetInstance();
	tagGlobalUserData * pGlobalUserData = pGlobalUserInfo->GetGlobalUserData();

	m_ExchangeRate = wExchangeRate;
	al_yuanbao->setString(StringUtils::format("%lld", pGlobalUserData->lUserIngot));
	lab_youxibi->setString(StringUtils::format(SSTRING("Conversion_Chinese"), m_ExchangeRate));
}

void ConversionLayer::onConversionResult(bool bSuccessed, SCORE lCurrIngot, const char* szDescription)
{
	if (bSuccessed)
	{
		al_yuanbao->setString(StringUtils::format("%lld", lCurrIngot));
		tf_duihuan->setString("");
		al_youxibi->setString("0");
	}

	NewDialog::create(szDescription, NewDialog::NONEBUTTON);
}

void ConversionLayer::enterCallBack(cocos2d::Ref* txt_, ui::TextFiledEventType event_type_)
{
	CGlobalUserInfo * pGlobalUserInfo = CGlobalUserInfo::GetInstance();
	tagGlobalUserData * pGlobalUserData = pGlobalUserInfo->GetGlobalUserData();

	std::string txt = tf_duihuan->getStringValue();
	bool isNumber = YRComStringIsNumber(txt);
	if (isNumber && atoll(tf_duihuan->getString().c_str()) != 0)
	{
		if (atoll(tf_duihuan->getString().c_str()) >= pGlobalUserData->lUserIngot)
		{
			tf_duihuan->setString(StringUtils::format("%lld", pGlobalUserData->lUserIngot));
			al_youxibi->setString(StringUtils::format("%lld", pGlobalUserData->lUserIngot * m_ExchangeRate));
		}		
		else
			al_youxibi->setString(StringUtils::format("%lld", atoll(tf_duihuan->getString().c_str()) * m_ExchangeRate));
	}
	else
	{
		tf_duihuan->setString("");
		al_youxibi->setString("0");
	}
}

