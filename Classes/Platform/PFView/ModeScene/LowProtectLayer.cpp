#include "LowProtectLayer.h"
#include "Tools/Dialog/NewDialog.h"
#include "Tools/Manager/SoundManager.h"
#include "Tools/tools/StaticData.h"
#include "Tools/tools/StringData.h"
USING_NS_CC;
using namespace ui;

LowProtectLayer::LowProtectLayer()
:mLowProtectMission(ADDRESS_URL, SERVER_PORT)
{
	mLowProtectMission.setMissionSink(this);
	degree = 0;
}

LowProtectLayer::~LowProtectLayer()
{
	mLowProtectMission.setMissionSink(0);
}

bool LowProtectLayer::init()
{
	if (!BaseLayer::initWithJsonFile("LobbyResources/LowProtectLayer.json"))
	{
		return false;
	}

	this->setZOrder(1);
	initCompoent();

	return true;
}

void LowProtectLayer::initCompoent()
{
	Panel_bg = dynamic_cast<Layout *>(m_root_widget->getParent()->getChildByName("Panel_bg"));

	img_bg = dynamic_cast<ui::ImageView *>(m_root_widget->getChildByName("img_bg"));
	CCAssert(img_bg, "");

	Button * btn_close = dynamic_cast<Button *>(img_bg->getChildByName("btn_close"));
	CCAssert(btn_close, "");
	btn_close->addTouchEventListener(this, SEL_TouchEvent(&LowProtectLayer::closeButtonTouch));

	Button * btn_confrim = dynamic_cast<Button *>(img_bg->getChildByName("btn_confrim"));
	CCAssert(btn_confrim, "");
	btn_confrim->addTouchEventListener(this, SEL_TouchEvent(&LowProtectLayer::particleButtonTouch));

	al_gamenum = dynamic_cast<TextAtlas *>(img_bg->getChildByName("al_gamenum"));
	CCAssert(al_gamenum, "");
	///< 这里游戏币数据,在这里载入!!!!
	al_gamenum->setString("1234");

	al_step = dynamic_cast<TextAtlas *>(img_bg->getChildByName("al_step"));
	CCAssert(al_step, "");
	al_step->setString("3");

	al_require_game_num = dynamic_cast<TextAtlas *>(img_bg->getChildByName("al_require_game_num"));
	CCAssert(al_require_game_num, "");
	al_require_game_num->setString("1234");

	//界面下移
	m_root_widget->setPositionY(850);

	m_root_widget->runAction(CCSequence::create(
		MoveTo::create(0.25f, Vec2(0, 0)),
		MoveTo::create(0.1f, Vec2(0, 100)),
		MoveTo::create(0.05f, Vec2(0, 0)),
		MoveTo::create(0.04f, Vec2(0, 30)),
		MoveTo::create(0.03f, Vec2(0, 0)),
		0));
}

void LowProtectLayer::closeButtonTouch(cocos2d::Ref * ref, ui::TouchEventType eventType_)
{
	if (eventType_ == TouchEventType::TOUCH_EVENT_BEGAN)
	{
		PLAY_BUTTON_CLOSE_EFFECT
	}
	if (eventType_ == TouchEventType::TOUCH_EVENT_ENDED)
	{
		Panel_bg->setVisible(false);
		m_root_widget->runAction(CCSequence::create(MoveTo::create(0.2f, Vec2(0, 850)), CCCallFunc::create(this, callfunc_selector(LowProtectLayer::closeMoveTo)), NULL));	
	}
}
void LowProtectLayer::particleButtonTouch(cocos2d::Ref * ref, ui::TouchEventType eventType_)
{
	if (eventType_ == TouchEventType::TOUCH_EVENT_BEGAN)
	{
		PLAY_BUTTON_CLICK_EFFECT
	}
	if (eventType_ == TouchEventType::TOUCH_EVENT_ENDED)
	{
		
		mLowProtectMission.obtainLowProtect();
	}
}


void LowProtectLayer::onSignInQueryInfoResult(SCORE lScoreCondition, SCORE lScoreAmount, byte cbTakeTimes)
{
	std::string str = StringUtils::format("%d", lScoreCondition);
	al_gamenum->setString(str);

	str = StringUtils::format("%d", lScoreAmount);
	al_require_game_num->setString(str);

	degree = cbTakeTimes;
	str = StringUtils::format("%d", cbTakeTimes);
	al_step->setString(str);
}

void LowProtectLayer::onSignInDoneResult(bool bSuccessed, SCORE lGameScore, const char* szDescription)
{
	if (bSuccessed)
	{
		degree--;
		al_step->setString(StringUtils::format("%d", degree));
		///< 领取成功
		Size visibleSize = Director::getInstance()->getVisibleSize();
		auto * particleSystem = ParticleSystemQuad::create("particle/bigwin_blowout_1.plist");
		particleSystem->setPosition(visibleSize.width / 2, visibleSize.height / 5 * 2);
		this->addChild(particleSystem);
	}
	else
	{
		NewDialog::create(SSTRING("Low_get_defeat"), NewDialog::NONEBUTTON);
	}
	return;
}

void LowProtectLayer::onEnterTransitionDidFinish()
{
	BaseLayer::onEnterTransitionDidFinish();

	mLowProtectMission.requickLowProtectInfo();
}

void LowProtectLayer::closeMoveTo()
{
	this->removeFromParent();
}
