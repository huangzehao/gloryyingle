#include "Tools/ViewHeader.h"
#include "UserInfoLayer.h"
#include "Tools/tools/MTNotification.h"
#include "ui/UIImageView.h"
#include "Platform/PFView/ModeScene/TopHeadLayer.h"
#include "Platform/PFView/ModeScene/ModeScene.h"
#include <cocos/ui/UITextAtlas.h>
#include "Tools/tools/YRCommon.h"
#include "Tools/Dialog/NewDialog.h"
#include "Tools/Manager/SoundManager.h"
#include "Platform/PFDefine/data/GlobalUserInfo.h"
#include "Tools/tools/StaticData.h"
#include "Tools/tools/StringData.h"
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID || CC_TARGET_PLATFORM == CC_PLATFORM_WIN32)
#include "cocostudio\CocoStudio.h"
#elif (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
#include "cocostudio/CCArmatureDataManager.h"
#include "cocostudio/CCArmature.h"
#endif

USING_NS_CC;
USING_NS_CC_EXT;
using namespace ui;
using namespace std;
using namespace cocostudio;

//////////////////////////////////////////////////////////////////////////

#define IMG_BG				"info.png"
#define IMG_BT_BACK_1		"bt_back_1.png"
#define IMG_BT_BACK_2		"bt_back_1.png"
#define IMG_BT_MAIL_1		"bt_message_1.png"
#define IMG_BT_MAIL_2		"bt_message_1.png"
#define IMG_BT_RECHARGE_1	"bt_recharge_1.png"
#define IMG_BT_RECHARGE_2	"bt_recharge_1.png"
#define IMG_BT_SETTING_1	"bt_settings_1.png"
#define IMG_BT_SETTING_2	"bt_settings_2.png"
#define IMG_BT_STORE_1		"bt_store_1.png"
#define IMG_BT_STORE_2		"bt_store_1.png"
#define IMG_BT_HELP_1		"bt_help_1.png"
#define IMG_BT_HELP_2		"bt_help_1.png"

#define TAG_CLOSE			0
#define TAG_SETTING			1
#define TAG_MAIL			2
#define TAG_STORE			3
#define TAG_RECHARGE		4
#define TAG_HELP			5

//< 本层的tag
#define TAG_USERINFOLAYER 100020

//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
UserInfoLayer::UserInfoLayer()
: mInsureMission(ADDRESS_URL, SERVER_PORT)
{
	m_type = BaseModeLayer::ModeLayerTypeInfo;
	mInsureMission.setMissionSink(this);
	isChangeHead = false;
	startHeadId = 0;
	endHeadId = 0;
}

UserInfoLayer::~UserInfoLayer()
{
	mInsureMission.setMissionSink(nullptr);
}
	//	mIndividualMission.setMissionSink(0);
	// 	G_NOTIFY_UNREG("USER_INFO");
	// 	G_NOTIFY_UNREG("USER_SCORE");
	// 	G_NOTIFY_UNREG("INSURE_QUERY");
	// 	SpriteFrameCache::getInstance()->removeSpriteFramesFromFile(PLIST_PATH);
	// 	Director::getInstance()->getTextureCache()->removeTextureForKey(TEXTURE_PATH);



//初始化方法
bool UserInfoLayer::init()
{
	return newInit();
}

bool UserInfoLayer::newInit()
{
	do
	{
		//变量定义
		CGlobalUserInfo * pGlobalUserInfo = CGlobalUserInfo::GetInstance();
		tagGlobalUserData * pGlobalUserData = pGlobalUserInfo->GetGlobalUserData();
		tagUserInsureInfo* pGlobalInsureInfo = pGlobalUserInfo->GetUserInsureInfo();
		tagIndividualUserData *pGlobalIndividualUserData = pGlobalUserInfo->GetIndividualUserData();
		// 用户等级
		tagGrowLevelParameter * pGlobaUserLevel = pGlobalUserInfo->GetUserGrowLevelParameter();

		CC_BREAK_IF(!BaseLayer::initWithJsonFile("LobbyResources/PlayformMyInfo.json"));

		//界面下移
		m_root_widget->setPositionY(850);

		m_root_widget->runAction(CCSequence::create(
			MoveTo::create(0.25f, Vec2(0, 0)),
			MoveTo::create(0.1f, Vec2(0, 100)),
			MoveTo::create(0.05f, Vec2(0, 0)),
			MoveTo::create(0.04f, Vec2(0, 30)),
			MoveTo::create(0.03f, Vec2(0, 0)),
			0));

		Armature* armature = Armature::create("touxiangkuang");
		armature->getAnimation()->playByIndex(0);
		armature->setAnchorPoint(Vec2(0, 0));
		armature->setPosition(Vec2(454, 389));
		m_root_widget->addChild(armature);

		///< 图片
		Node* content_bg_ = m_root_widget->getChildByName("img_headPanel");
		CCAssert(content_bg_, "");

		nickname_ = dynamic_cast<ui::Text*>(content_bg_->getChildByName("lab_nickname"));
		CCAssert(nickname_, "");
		if (nickname_){
			nickname_->setString(pGlobalUserData->szNickName);
		}

		game_id_ = dynamic_cast<ui::TextAtlas*>(content_bg_->getChildByName("al_id"));
		if (game_id_){
			game_id_->setString(StringUtils::format("%d", pGlobalUserData->dwGameID));
		}

		game_money_ = dynamic_cast<ui::TextAtlas*>(m_root_widget->getChildByName("img_jinbi")->getChildByName("al_jinbi"));
		if (game_money_){
			game_money_->setString(StringUtils::format("%lld", pGlobalUserData->lUserScore));
		}

		game_silver = dynamic_cast<ui::TextAtlas*>(m_root_widget->getChildByName("img_yuanbao")->getChildByName("al_yuanbao"));
		if (game_silver){
			game_silver->setString(StringUtils::format("%lld", pGlobalUserData->lUserIngot));
		}

		img_headBg = dynamic_cast<ui::ImageView*>(m_root_widget->getChildByName("img_headBg"));

// 		lv_ = dynamic_cast<ui::TextAtlas*>(img_headBg->getChildByName("img_levelkuang")->getChildByName("al_level"));
//  		CCAssert(lv_, "");
// 		if (lv_){
//  			//  设置玩家等级
//  			lv_->setString(StringUtils::format("%d", pGlobaUserLevel->wCurrLevelID));
//  		}

// 		 name_ = dynamic_cast<ui::Text*>(content_bg_->getChildByName("lab_name"));
// 		 if (name_){	
// 		 	name_->setString(pGlobalUserData->szAccounts);
// 		 }

// 		//个性签名
// 		ui::Text * qq = dynamic_cast<ui::Text*>(content_bg_->getChildByName("Label_45_0_0_1"));
// 		CCAssert(qq, "");
// 		qq->setString(StringUtils::format("%s", pGlobalUserData->szUnderWrite));

		ui::Button* btn_return = dynamic_cast<ui::Button *>(m_root_widget->getChildByName("btn_return"));
		btn_return->addTouchEventListener(this, ui::SEL_TouchEvent(&UserInfoLayer::closeButtonTouch));

		//< 下面是选择头像事件
		ui::ListView * head_list = dynamic_cast<ui::ListView*>(m_root_widget->getChildByName("lv_headlist"));
		CCAssert(head_list, "");
		for (int i = 1; i <= 8; i++)
		{
			ui::Widget * img_head = dynamic_cast<ui::Widget*>(head_list->getChildByName(StringUtils::format("img_head_%d", i)));
			CCAssert(img_head, "");
			img_head->setTouchEnabled(true);
			img_head->addTouchEventListener(this, ui::SEL_TouchEvent(&UserInfoLayer::imageHeadTouch));
		}

		updateInfo(pGlobalUserData->wFaceID);
		startHeadId = pGlobalUserData->wFaceID;
		endHeadId = startHeadId;
		return true;
	} while (0);

	return false;
}

void UserInfoLayer::closeButtonTouch(cocos2d::Ref * ref, ui::TouchEventType eventType_)
{
	if (eventType_ == TouchEventType::TOUCH_EVENT_BEGAN)
	{
		PLAY_BUTTON_CLOSE_EFFECT
	}
	if (eventType_ == TouchEventType::TOUCH_EVENT_ENDED)
	{
		ModeScene *mc_ = dynamic_cast<ModeScene*>(Director::getInstance()->getRunningScene());
		if (mc_){
			// 发送一条消息到信息类, 获取用户信息类
			UserInfoLayer * uInfo = dynamic_cast<UserInfoLayer *>(mc_->getLayerByType(BaseModeLayer::ModeLayerTypeInfo));
			if (isChangeHead)
			{
				uInfo->connectNetworkUpdateHead();
			} 
			else
			{
				uInfo->resumeStartHead();
			}
	
			m_root_widget->runAction(CCSequence::create(MoveTo::create(0.2f, Vec2(0, 850)), CCCallFunc::create(this, callfunc_selector(UserInfoLayer::closeMoveTo)), NULL));	
		}
	}
}

void UserInfoLayer::closeMoveTo()
{
	this->removeFromParent();
}

void UserInfoLayer::setDatas()
{
	onUserName(0);
	onUserScore(0);
}


void UserInfoLayer::setCloseDialogInfo(cocos2d::Ref* target, cocos2d::SEL_CallFuncN callfun, const std::string& title, const std::string& content)
{
	mTarget = target;
	mCallback = callfun;
	mTitle = title;
	mContent = content;
}


//////////////////////////////////////////////////////////////////////////
//按钮事件
void UserInfoLayer::onBtnClick(cocos2d::Ref* obj, cocos2d::extension::Control::EventType  e)
{
	Control* ctr = (Control*)obj;

// 	switch (ctr->getTag())
// 	{
// 	case TAG_CLOSE:
// 		popup(mTitle.c_str(), mContent.c_str(), DLG_MB_OK | DLG_MB_CANCEL, 0, mTarget, mCallback);
// 		break;
// 	case TAG_RECHARGE:
// 	{
// 						 UIPayment* payment = UIPayment::create();
// 						 payment->setPosition(Point(kRevolutionWidth / 2, kRevolutionHeight / 2));
// 						 getParent()->addChild(payment);
// 	}
// 		break;
// 	case TAG_MAIL:
// 	{
// 					 UIMessage* msg = UIMessage::create();
// 					 msg->setPosition(Point(kRevolutionWidth / 2, kRevolutionHeight / 2));
// 					 getParent()->addChild(msg);
// 	}
// 		break;
// 	case TAG_STORE:
// 	{
// 					  UIStore* store = UIStore::create();
// 					  store->setPosition(Point(kRevolutionWidth / 2, kRevolutionHeight / 2));
// 					  getParent()->addChild(store);
// 	}
// 		break;
// 	case TAG_SETTING:
// 	{
// 						UIShezhi* shezhi = UIShezhi::create();
// 						shezhi->setPosition(Point(kRevolutionWidth / 2, kRevolutionHeight / 2));
// 						getParent()->addChild(shezhi);
// 	}
// 		break;
// 	case TAG_HELP:
// 	{
// 					 UIHelp* help = UIHelp::create();
// 					 help->setPosition(Point(kRevolutionWidth / 2, kRevolutionHeight / 2));
// 					 getParent()->addChild(help);
// 	}
// 		break;
// 	}

}

void UserInfoLayer::imageHeadTouch(cocos2d::Ref * obj, cocos2d::ui::TouchEventType e)
{
	if (e == ui::TouchEventType::TOUCH_EVENT_BEGAN)
	{
		PLAY_BUTTON_CLICK_EFFECT
	}
	if (e == ui::TouchEventType::TOUCH_EVENT_ENDED)
	{
		
		isChangeHead = true;
		int tag = dynamic_cast<Node *>(obj)->getTag();
		int img_level = 0;
		switch (tag)
		{
		case UserInfoLayer::IMAGE_HEAD_1:
			img_level = 0;
			break;
		case UserInfoLayer::IMAGE_HEAD_2:
			img_level = 1;
			break;
		case UserInfoLayer::IMAGE_HEAD_3:
			img_level = 2;
			break;
		case UserInfoLayer::IMAGE_HEAD_4:
			img_level = 3;
			break;
		case UserInfoLayer::IMAGE_HEAD_5:
			img_level = 4;
			break;
		case UserInfoLayer::IMAGE_HEAD_6:
			img_level = 5;
			break;
		case UserInfoLayer::IMAGE_HEAD_7:
			img_level = 6;
			break;
		case UserInfoLayer::IMAGE_HEAD_8:
			img_level = 7;
			break;
		default:
			break;
		}

		updateInfo(img_level);
	}
}

void UserInfoLayer::updateInfo(int tag)
{
	ui::ImageView * img_head = dynamic_cast<ui::ImageView *>(img_headBg->getChildByName("img_head"));
	CCAssert(img_head, "");

	img_head->loadTexture(YRComGetHeadImageById(tag));

	endHeadId = tag;

	CGlobalUserInfo * pGlobalUserInfo = CGlobalUserInfo::GetInstance();
	tagUserInsureInfo * pUserInsureInfo = pGlobalUserInfo->GetUserInsureInfo();
	tagGlobalUserData * pGlobalUserData = pGlobalUserInfo->GetGlobalUserData();
	tagGrowLevelParameter * pGlobaUserLevel = pGlobalUserInfo->GetUserGrowLevelParameter();

	pGlobalUserData->wFaceID = endHeadId;

	if (nickname_){
		nickname_->setString(pGlobalUserData->szNickName);
	}
// 	if (lv_){
// 		//  设置玩家等级
// 		lv_->setString(StringUtils::format("%d", pGlobaUserLevel->wCurrLevelID));
// 	}
	if (game_id_){
		//  game_id_->setString();
		game_id_->setString(StringUtils::format("%d", pGlobalUserData->dwGameID));
	}
// 	if (name_){
// 		//
// 		name_->setString(pGlobalUserData->szAccounts);
// 	}
	if (game_money_){
		game_money_->setString(StringUtils::format("%lld", pGlobalUserData->lUserScore));
	}
	if (game_silver){
		game_silver->setString(StringUtils::format("%lld", pGlobalUserData->lUserIngot));
	}
}


//////////////////////////////////////////////////////////////////////////
//通知消息
void UserInfoLayer::onUserName(cocos2d::Ref* obj)
{
	//变量定义
	CGlobalUserInfo * pGlobalUserInfo = CGlobalUserInfo::GetInstance();
	tagGlobalUserData * pGlobalUserData = pGlobalUserInfo->GetGlobalUserData();

//	mLbNickname->setString(pGlobalUserData->szNickName);
}

void UserInfoLayer::onUserScore(cocos2d::Ref* obj)
{
	//变量定义
	CGlobalUserInfo * pGlobalUserInfo = CGlobalUserInfo::GetInstance();
	tagUserInsureInfo * pUserInsureInfo = pGlobalUserInfo->GetUserInsureInfo();
	tagGlobalUserData * pGlobalUserData = pGlobalUserInfo->GetGlobalUserData();

	pGlobalUserData->lUserScore = pUserInsureInfo->lUserScore;
	pGlobalUserData->lUserInsure = pUserInsureInfo->lUserInsure;

//	mLbScore->setString(toString(pGlobalUserData->lUserScore));
	//	mLbMedal->setString(toString((int)pGlobalUserData->dwUserMedal));
}

void UserInfoLayer::onInsureQuery(cocos2d::Ref* obj)
{
	// 	MTData* data = (MTData*)obj;
	// 	if (data != 0 && data->mData1 > 0)
	// 	{
	// 		this->unschedule(schedule_selector(UserInfoLayer::onDelayQuery));
	// 		this->scheduleOnce(schedule_selector(UserInfoLayer::onDelayQuery), data->mData1);
	// 	}
	// 	else
	// 	{
	// 		mInsureMission.query();
	// 	}
}

void UserInfoLayer::onDelayQuery(float delta)
{
	//mInsureMission.query();
}

//////////////////////////////////////////////////////////////////////////
// IGPInsureMissionSink
void UserInfoLayer::onInsureInfo()
{
	onUserScore(0);
	
}

void UserInfoLayer::onGPFaceInfo()
{
	isChangeHead = false;
	startHeadId = endHeadId;
}

void UserInfoLayer::onGPFaceSuccess(const char* szDescription)
{
	NewDialog::create(szDescription, NewDialog::NONEBUTTON);
	isChangeHead = false;
	startHeadId = endHeadId;
}

void UserInfoLayer::onGPFaceFailure(const char* szDescription)
{
	NewDialog::create(szDescription, NewDialog::NONEBUTTON);
}

bool UserInfoLayer::headAlearyChange()
{
	return isChangeHead && startHeadId != endHeadId;
}

void UserInfoLayer::connectNetworkUpdateHead()
{
	mInsureMission.setToSystemFace(endHeadId);
}

void UserInfoLayer::resumeStartHead()
{
	isChangeHead = false;
	updateInfo(startHeadId);
}

void UserInfoLayer::onEnterTransitionDidFinish()
{
	BaseLayer::onEnterTransitionDidFinish();
	
	CGlobalUserInfo * pGlobalUserInfo = CGlobalUserInfo::GetInstance();
	tagGlobalUserData * pGlobalUserData = pGlobalUserInfo->GetGlobalUserData();

	updateInfo(pGlobalUserData->wFaceID);
}



