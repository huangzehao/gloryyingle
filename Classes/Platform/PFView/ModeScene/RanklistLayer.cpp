#include "RanklistLayer.h"
#include "Tools/Manager/SoundManager.h"
#include "Platform/PFDefine/data/GlobalUserInfo.h"
#include "Tools/tools/StaticData.h"
#include "Tools/tools/YRCommon.h"

USING_NS_CC;
using namespace ui;

CCScene* RanklistLayer::scene()
{
	CCScene* scene = CCScene::create();

	if (!scene)
		return 0;

	RanklistLayer* layer = RanklistLayer::create();
	if (layer)
		scene->addChild(layer);
	return scene;
}

//////////////////////////////////////////////////////////////////////////
RanklistLayer::RanklistLayer()
:mRanklistMission(ADDRESS_URL, SERVER_PORT),
num(0)
{
	mRanklistMission.setMissionSink(this);
}

RanklistLayer::~RanklistLayer()
{
	mRanklistMission.setMissionSink(0);
}

//初始化方法
bool RanklistLayer::init()
{
	do
	{
		CC_BREAK_IF(!BaseLayer::initWithJsonFile("LobbyResources/RanklistLayer.json"));
		CC_BREAK_IF(!this->setUpdateView());

		return true;
	} while (0);

	return false;
}

// 初始化组件
bool RanklistLayer::setUpdateView()
{
	do
	{
		Panel_bg = dynamic_cast<Layout *>(m_root_widget->getParent()->getChildByName("Panel_bg"));

		img_bg = dynamic_cast<ui::ImageView *>(m_root_widget->getChildByName("img_bg"));

		Panel_cell = dynamic_cast<Layout *>(img_bg->getChildByName("Panel_cell"));

		lv_rank = dynamic_cast<ui::ListView*>(img_bg->getChildByName("lv_rank"));
		lv_rank->removeAllChildren();

		btn_close = dynamic_cast<ui::Button *>(img_bg->getChildByName("btn_close"));
		CCAssert(btn_close, "");
		btn_close->addTouchEventListener(this, SEL_TouchEvent(&RanklistLayer::closeButtonTouch));

		//界面下移
		m_root_widget->setPositionY(850);

		m_root_widget->runAction(CCSequence::create(
			MoveTo::create(0.25f, Vec2(0, 0)),
			MoveTo::create(0.1f, Vec2(0, 100)),
			MoveTo::create(0.05f, Vec2(0, 0)),
			MoveTo::create(0.04f, Vec2(0, 30)),
			MoveTo::create(0.03f, Vec2(0, 0)),
			CCCallFunc::create(this, callfunc_selector(RanklistLayer::drawRankWab)), NULL));

		return true;
	} while (0);

	return false;
}

void RanklistLayer::closeButtonTouch(cocos2d::Ref * ref, ui::TouchEventType eventType_)
{
	if (eventType_ == TouchEventType::TOUCH_EVENT_BEGAN)
	{
		PLAY_BUTTON_CLOSE_EFFECT
	}
	if (eventType_ == TouchEventType::TOUCH_EVENT_ENDED)
	{
		Panel_bg->setVisible(false);
		m_root_widget->runAction(CCSequence::create(MoveTo::create(0.2f, Vec2(0, 850)), CCCallFunc::create(this, callfunc_selector(RanklistLayer::closeMoveTo)), NULL));
	}
}

void RanklistLayer::drawRankWab()
{
	mRanklistMission.queryRank();
}

void RanklistLayer::closeMoveTo()
{
	this->removeFromParent();
}

void RanklistLayer::onAddRank(tagUserRankingResult * pUserRankingResult)
{
	Widget *center_cell = Panel_cell->clone();
	img_num = dynamic_cast<ui::ImageView*>(center_cell->getChildByName("img_num"));
	img_head = dynamic_cast<ui::ImageView*>(center_cell->getChildByName("img_headIconBg")->getChildByName("img_head"));
	lab_name = dynamic_cast<ui::Text*>(center_cell->getChildByName("img_headIconBg")->getChildByName("lab_name"));
	al_score = dynamic_cast<ui::TextAtlas*>(center_cell->getChildByName("al_score"));

	img_num->loadTexture(StringUtils::format("LobbyResources/Rank/num_%d.png", ++num));
	img_head->loadTexture(YRComGetHeadImageById(pUserRankingResult->wFaceID));
	lab_name->setString(StringUtils::format("%s", pUserRankingResult->szNickName));
	al_score->setString(StringUtils::format("%lld", pUserRankingResult->lUserInsure));

	lv_rank->pushBackCustomItem(center_cell);
}

void RanklistLayer::onAddRankEnd()
{
	lv_rank->jumpToTop();
}
