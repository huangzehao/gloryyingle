#ifndef _GIVE_CHOICE_LAYER__H__
#define _GIVE_CHOICE_LAYER__H__

// #include "cocos2d.h"
// #include "cocos-ext.h"

#include "cocos2d.h"
#include "ui/CocosGUI.h"
#include "Platform/PFView/BaseLayer.h"
#include "Platform/PFView/ModeScene/TopHeadLayer.h"
#include "Platform/PFKernel/CGPInsureMission.h"

class GiveChoiceLayer
	: public BaseModeLayer,
	public IGPInsureMissionSink
{
public:
	CREATE_FUNC(GiveChoiceLayer);

public:
	GiveChoiceLayer();
	virtual ~GiveChoiceLayer();
	bool init();
	bool initCenterPanel();
	///< 新的初始化
	bool newInit();
	///< 新的初始化中间面板
	bool newInitCenterPanel();

	//设置显示
	void setDatas();

	void setCloseDialogInfo(cocos2d::Ref* target, cocos2d::SEL_CallFuncN callfun, const std::string& title, const std::string& content);

private:
	void newInitPanel_1();
	void newInitPanel_2();
	void newInitPanel_3();

	// 刷新
	void flushPanel_1();
	void flushPanel_2();
	void flushPanel_3();

	///<关闭移动回调
	void closeMoveTo();

private:
	int m_center_index;
	//////////////////////////////////////////////////////////////////////////
	//通知消息
private:
	void onUserName(cocos2d::Ref* obj);
	void onUserScore(cocos2d::Ref* obj);
	void onInsureQuery(cocos2d::Ref* obj);
	void onDelayQuery(float delta);
public:
	void onEnterTransitionDidFinish();

	//////////////////////////////////////////////////////////////////////////
	// IGPInsureMissionSink
public:
	virtual void onInsureInfo();
	virtual void onInsureSuccess(int type, const char* szDescription);
	virtual void onInsureFailure(int type, const char* szDescription);
	virtual bool onInsureTransferConfirm(const char* szMessage);
	virtual void onInsureEnableResult(int type, const char* szMessage);
	virtual void onAccessRecord(tagUserAccessRecord * pUserAccessRecord);			//查询银行存取记录
	virtual void onTransferRecord(tagUserTransferRecord * pTransferUserData);		//查询银行转账记录
	virtual void onAccessRecordEnd();
	virtual void onTransferRecordEnd();
private:
	cocos2d::Label* mLbNickname;
	cocos2d::Label* mLbScore;
	cocos2d::Label* mLbMedal;
	///////////1
	ui::TextField *txt_save_money_1;
	ui::TextField *txt_store_pwd_;
	///////////2
	ui::TextField *txt_game_id_;
	ui::TextField * tf_game_num_;
	ui::TextField *txt_store_pwd2_;
	ui::Layout *Panel_Transfer;
	ui::Layout *Panel_cell_Transfer;
	ui::ListView *lv_record_Transfer;
	ui::Text * lab_into;
	ui::Text * lab_rollout;
	int64 rollOutScore;
	int64 inToScore;
	///////////3
	ui::Text * Label_wYear;
	ui::Text * Label_wMonth;
	ui::Text * Label_wDay;
	ui::Text * Label_transfer;
	ui::Text * Label_lScore;
	ui::ImageView * img_type;

	ui::Layout *Panel_cell;
	ui::ListView *lv_record;
	ui::ImageView *img_none;

	//////////////////////////////////////////////////////////////////////////
	cocos2d::Ref*		mTarget;
	cocos2d::SEL_CallFuncN	mCallback;
	std::string				mTitle;
	std::string				mContent;

	CGPInsureMission	mInsureMission;
};

#endif // _GIVE_CHOICE_LAYER__H__