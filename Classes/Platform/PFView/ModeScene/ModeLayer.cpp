#include "ModeLayer.h"
#include "ModeScene.h"
#include "TopHeadLayer.h"

#include "Tools/Dialog/NewDialog.h"
#include "Tools/tools/StaticData.h"
#include "Tools/tools/StringData.h"
#include "Tools/tools/SimpleTools.h"
#include "Tools/Manager/SoundManager.h"
#include "Tools/Dialog/TailorView.h"
#include "Platform/PFView/LoadScene/UpdateResoureLayer.h"
#include "Platform/PFView/ServerListScene/ServerListLayer.h"

USING_NS_CC;
USING_NS_CC_EXT;
//////////////////////////////////////////////////////////////////////////

// #define PLIST_MODESCENE			"modescene/modescene.plist"
// #define TEX_MODESCENE			"modescene/modescene.pvr.ccz"
// 
// #define IMG_MODESCENE_MODE_1	"mode_join.png"
// #define IMG_MODESCENE_MODE_2	"mode_beilv.png"
// #define IMG_MODESCENE_MODE_3	"mode_battle.png"

#define GAME_TYPE_IMAGE_YELLO_0 "LobbyResources/GameRest/game_type_bg0.png"
#define GAME_TYPE_IMAGE_YELLO_1 "LobbyResources/GameRest/game_type_bg1.png"
#define GAME_TYPE_IMAGE_YELLO_2 "LobbyResources/GameRest/game_type_bg2.png"

#define GAME_TYPE_IMAGE_GREEN_0 "LobbyResources/GameRest/btn_green0.png"
#define GAME_TYPE_IMAGE_GREEN_1 "LobbyResources/GameRest/btn_green1.png"
#define GAME_TYPE_IMAGE_GREEN_2 "LobbyResources/GameRest/btn_green2.png"

#define GAME_TXT_TYPE_1 "LobbyResources/GameRest/txt_Platform_1.png"
#define GAME_TXT_TYPE_2 "LobbyResources/GameRest/txt_Platform_2.png"
#define GAME_TXT_TYPE_3 "LobbyResources/GameRest/txt_Platform_3.png"
#define GAME_TXT_TYPE_4 "LobbyResources/GameRest/txt_Platform_4.png"
#define GAME_TXT_TYPE_5 "LobbyResources/GameRest/txt_Platform_5.png"
#define GAME_TXT_TYPE_6 "LobbyResources/GameRest/txt_Platform_6.png"
#define GAME_TXT_TYPE_7 "LobbyResources/GameRest/txt_Platform_7.png"
#define GAME_TXT_TYPE_8 "LobbyResources/GameRest/txt_Platform_8.png"

//////////////////////////////////////////////////////////////////////////
ModeLayer::ModeLayer() :isMove(false), m_time(0)
	,mLoginMission(ADDRESS_URL, SERVER_PORT)
	, m_cur_gametype_cell(0), isFirst(true)
{
	m_type = BaseModeLayer::ModeLayerTypeDefault;
	CServerListData::shared()->SetServerListDataSink(this);
}

ModeLayer::~ModeLayer()
{
// 	SpriteFrameCache::getInstance()->removeSpriteFramesFromFile(PLIST_MODESCENE);
// 	Director::getInstance()->getTextureCache()->removeTextureForKey(TEX_MODESCENE);
	this->unschedule(SEL_SCHEDULE(&ModeLayer::MessageUpdate));
}

const char *getImagePathByTag(int tag_, bool is_focus_, bool is_last_ = false)
{
	if (is_last_){
		return is_focus_ ? GAME_TYPE_IMAGE_GREEN_2 : GAME_TYPE_IMAGE_YELLO_2;
	}

	switch (tag_+1)
	{
	case 1:
		return is_focus_ ? GAME_TYPE_IMAGE_GREEN_0 : GAME_TYPE_IMAGE_YELLO_0;

		break;		
	default:
		return is_focus_ ? GAME_TYPE_IMAGE_GREEN_1 : GAME_TYPE_IMAGE_YELLO_1;
		break;
	}

	return 0;
}

const char * getTxtImagePathById(int id_)
{
	switch (id_)
	{
		// 我的比赛
	case 1:
		return GAME_TXT_TYPE_1;
		break;
		//推荐游戏
	case 2:
		return GAME_TXT_TYPE_2;
		break;
		// 牌类游戏
	case 3:
		return GAME_TXT_TYPE_3;
		break;
		// 麻将游戏
	case 4:
		return GAME_TXT_TYPE_4;
		break;
		//财富游戏
	case 5:
		return GAME_TXT_TYPE_5;
		break;
		// 休闲游戏
	case 6:
		return GAME_TXT_TYPE_6;
		break;
		// 手机游戏
	case 7:
		return GAME_TXT_TYPE_7;
		break;
	case 8:
		return GAME_TXT_TYPE_8;
		break;
	default:
		break;
	}
	return "";
}

//初始化方法
bool ModeLayer::init()
{
	return newInit();

}

///< 新的初始化方法
bool ModeLayer::newInit()
{
	do
	{
		const char * sec_file = "LobbyResources/PlayformIndex.json";
		CC_BREAK_IF(!BaseLayer::initWithJsonFile(sec_file));

		return true;


	} while (0);

	return false;
}


//////////////////////////////////////////////////////////////////////////
// IServerListDataSink
//完成通知
bool ModeLayer::setDatas()
{
	if (isFirst)
	{
		isFirst = false;
		return newSetDatas();
	} 
	else
	{
		return false;
	}
	
}


bool ModeLayer::newSetDatas()
{
	do
	{
		m_BgRoll = ui::Layout::create();
		m_BgRoll->setAnchorPoint(Vec2(0.5f, 0.5f));
		m_BgRoll->setContentSize(Size(807, 67));
		m_BgRoll->setSize(Size(807, 67));
		m_BgRoll->setBackGroundImage("LobbyResources/LobbyCommon/img_trumpet.png");

		Size winSize = Director::getInstance()->getWinSize();
		m_BgRoll->setPosition(Vec2(winSize.width / 2, winSize.height - 130));
		m_BgRoll->setClippingEnabled(true);
		m_BgRoll->setVisible(false);
		this->addChild(m_BgRoll);

		mBgLayout = ui::Layout::create();
		mBgLayout->setAnchorPoint(Vec2(0.5f, 0.5f));
		mBgLayout->setContentSize(Size(710, 67));
		mBgLayout->setSize(Size(710, 67));
		mBgLayout->setPosition(Vec2(760 / 2 + 50, 44));
		mBgLayout->setClippingEnabled(true);
		m_BgRoll->addChild(mBgLayout);

		mLabMessage = Label::create();
		mLabMessage->setSystemFontSize(30);
		mLabMessage->setTextColor(Color4B::YELLOW);
		mLabMessage->setAnchorPoint(Vec2(0.0f, 0.0f));
		mBgLayout->addChild(mLabMessage);

		this->scheduleOnce(SEL_SCHEDULE(&ModeLayer::sendSystemMessage), 0.2);
		this->schedule(SEL_SCHEDULE(&ModeLayer::MessageUpdate), 1);

		///< 类型列表
		ui::ListView *list = dynamic_cast<ui::ListView *>(m_root_widget->getChildByName("game_type_list"));
		list->removeAllChildren();
		ui::ImageView *cell_0 = dynamic_cast<ui::ImageView*>(m_root_widget->getChildByName("game_type_cell0"));
		ui::ImageView *cell_ = dynamic_cast<ui::ImageView*>(m_root_widget->getChildByName("game_type_cell1"));
		ui::ImageView *cell_2 = dynamic_cast<ui::ImageView*>(m_root_widget->getChildByName("game_type_cell2"));

		float pos_y_ = list->getContentSize().height / 2;// -cell_->getContentSize().height / 2;
		float interval_x_ = 10;

		float half_size_x_ = cell_->getContentSize().width / 2;

		CServerListData *list_data_ = CServerListData::shared();
		CGameTypeItemMap::iterator it = list_data_->GetTypeItemMapBegin();
		CGameTypeItem *item_ = 0;

		int disant = list->getItemsMargin();
		int i = 0;
		int type_cell_count_ = list_data_->getTypeCount();
		while ((item_ = list_data_->EmunGameTypeItem(it)))
		{
			if (type_cell_count_ == i){
				break;
			}

			tagGameType *type_ = &(item_->m_GameType);

			ui::Widget *game_cell_;
			if (i == 0){
				game_cell_ = cell_0->clone();
			}
			else if (i == type_cell_count_ - 1){
				game_cell_ = cell_2->clone();
			}
			else{
				game_cell_ = cell_->clone();
			}

			list->addChild(game_cell_);
			game_cell_->setTag(i);

			game_cell_->addTouchEventListener([=](Ref* ref_, ui::Widget::TouchEventType event_type_){
				if (event_type_ == ui::Widget::TouchEventType::BEGAN){
					PLAY_BUTTON_CLICK_EFFECT
				}
				if (event_type_ == ui::Widget::TouchEventType::ENDED){
					
					//runGame("game", "myGame");
					
					ui::ImageView *sender_ = dynamic_cast<ui::ImageView*>(ref_);
					if (sender_){
						sender_->loadTexture(getImagePathByTag(sender_->getTag(), true, ((sender_->getTag() + 1) == type_cell_count_)));
					}
					else{
						return;
					}

					if (sender_ == m_cur_gametype_cell){
						return;
					}

					if (m_cur_gametype_cell){
						m_cur_gametype_cell->loadTexture(getImagePathByTag(m_cur_gametype_cell->getTag(), false, ((m_cur_gametype_cell->getTag() + 1) == type_cell_count_)));
					}
					m_cur_gametype_cell = sender_;

					initGameList();
				}
			});

			if (i == 0){
				m_cur_gametype_cell = dynamic_cast<ui::ImageView*>(game_cell_);
				m_cur_gametype_cell->loadTexture(getImagePathByTag(m_cur_gametype_cell->getTag(), true));
			}

			game_cell_->setUserData((void*)type_);

			//  循环
			i++;
		}

		Node *page_view_ = m_root_widget->getChildByName("game_list");
		CC_BREAK_IF(!page_view_);

		panel_oneList = dynamic_cast<ui::Layout *>(m_root_widget->getChildByName("panel_oneList"));

		m_page_vew = page_view_;

		return initGameList();

	} while (false);

	return false;
}

void ModeLayer::moveEndCall()
{
	//log("come in!!");
	isMove = false;
	if (mMessageQueue.size() < 1)
		m_BgRoll->setVisible(false);
}

void ModeLayer::MessageUpdate(float delta)
{
	m_time++;
/*	log("the isMove is ----- %d ---- %d", isMove, m_time);*/

	CServerListData * pServerListData = CServerListData::shared();
	for (int i = 0; i < m_TimeRate.size(); i++)
	{
		if (m_time % m_TimeRate[i] == 0)
		{
			std::string xx = StringUtils::format("%s", pServerListData->m_SystemMessageVector[i]->szString);
			mMessageQueue.push(xx);
		}
	}

	///< 更新消息
	if (isMove) return;
	if (mMessageQueue.size() < 1) return;
	m_BgRoll->setVisible(true);
	std::string text = mMessageQueue.front();
	mMessageQueue.pop(); 
	mLabMessage->setString(text);

	isMove = true;
	Size label_pos = mBgLayout->getSize() ;
	mLabMessage->setPosition(Vec2(label_pos.width, 5));
	float move_distance = text.size() * 21;// +label_pos.width;
	if (move_distance < label_pos.width * 1.5f)
		move_distance += label_pos.width;

	float time = move_distance / 100 < 25 ? 25 : move_distance / 100;
	auto move = MoveBy::create(move_distance / 150, Vec2(-move_distance, 0));
	auto sequence = Sequence::create(move, CallFunc::create(this, callfunc_selector(ModeLayer::moveEndCall)), NULL);
	mLabMessage->runAction(sequence);

}

bool ModeLayer::initGameList()
{
	if (m_page_vew){
		m_page_vew->removeAllChildren();
	}

	CServerListData *list_data_ = CServerListData::shared();
	CGameKindItemMap::iterator it = list_data_->GetKindItemMapBegin();
	CGameKindItem *item_ = 0;

	int count = 0;
	int type_cell_count_ = list_data_->getKindCount();
	tagGameType *game_type_ = (tagGameType *)m_cur_gametype_cell->getUserData();
	tagGameKindVertor.clear();

	while ((item_ = list_data_->EmunGameKindItem(it)))
	{
		if (type_cell_count_ == count){
			break;
		}

		// 列出当前type的 游戏种类
		tagGameKind *type_ = &(item_->m_GameKind);
 		if (game_type_->wTypeID != type_->wTypeID && game_type_->wTypeID != type_->wJoinID){
 			count++;
 			continue;
 		}

		//添加游戏种类
		if (!isVisibleGameKind(type_->wKindID))
		{
			count++;
			continue;
		}
		tagGameKindVertor.push_back(type_);
		count++;
	}
	//按照排序索引排序
	for (int i = 0; !tagGameKindVertor.empty() && i < tagGameKindVertor.size(); i++)
	{
		for (int j = 0; j < tagGameKindVertor.size() - i - 1; j++)
		{
			if (tagGameKindVertor[j]->wSortID>tagGameKindVertor[j + 1]->wSortID)
			{
				tagGameKind *temp = tagGameKindVertor[j];
				tagGameKindVertor[j] = tagGameKindVertor[j + 1];
				tagGameKindVertor[j + 1] = temp;
			}
		}
	}
	//绘制游戏卡牌 
	for (int i = 0; !tagGameKindVertor.empty() && i < tagGameKindVertor.size(); i++)
	{
		tagGameKind *type_ = tagGameKindVertor[i];
		log("name is %s, id is %d", type_->szKindName, type_->wKindID);

		Layout * game_cell_ = dynamic_cast<ui::Layout *>(panel_oneList->clone());
		///< 开启交互
		game_cell_->setTouchEnabled(true);
		m_page_vew->addChild(game_cell_);

		game_cell_->addTouchEventListener([=](Ref* ref_, ui::Widget::TouchEventType event_type_){

			switch (event_type_)
			{
			case cocos2d::ui::Widget::TouchEventType::BEGAN:
			{
				PLAY_BUTTON_CLICK_EFFECT
				break;
			}
			case cocos2d::ui::Widget::TouchEventType::MOVED:
				break;
			case cocos2d::ui::Widget::TouchEventType::ENDED:
			{
				tagGameKind *kind_ = (tagGameKind *)(dynamic_cast<Node*>(ref_)->getUserData());

				UpdateResoureLayer * layer = UpdateResoureLayer::create(kind_->wKindID);
				bool haveUpdate = layer->isHaveNewVersion();

				if (haveUpdate)
				{
					layer->setCallBack([=]()
					{
						bool alery_have = false;
						if (kind_){
							alery_have = mLoginMission.updateServerInfo(kind_->wKindID);  // 请求房间列表消息
							if (!alery_have)
								OnGameKindFinish(kind_->wKindID);
						}
						else
						{
							NewDialog::create("jingqingqidai", NewDialog::AFFIRM);
						}
					});
					Director::getInstance()->getRunningScene()->addChild(layer, 3);
				}
				else
				{

					bool alery_have = false;
					if (kind_){
						alery_have = mLoginMission.updateServerInfo(kind_->wKindID);  // 请求房间列表消息
						if (!alery_have)
							OnGameKindFinish(kind_->wKindID);
					}
					else
					{
						NewDialog::create("jingqingqidai", NewDialog::AFFIRM);
					}
				}

			}
			case cocos2d::ui::Widget::TouchEventType::CANCELED:
				break;
			default:
				break;
			}
		});

		ImageView * img_gameType = dynamic_cast<ui::ImageView *>(game_cell_->getChildByName("img_gameType"));
		img_gameType->loadTexture(StringUtils::format("game_%d.png", type_->wKindID), TextureResType::PLIST);

		game_cell_->setUserData((void*)type_);
	}

	dynamic_cast< ui::ListView* >(m_page_vew)->requestRefreshView();

	return true;
}

bool ModeLayer::isVisibleGameKind(uint16 wKindID)
{
	switch (wKindID)
	{
		case DANAOTIANGO_KINDID:
 		case BAIJIALE_KINDID:
    	case ZHAJINHUA_KINDID:
 		case DOUDIZHU_KINDID:
  		case SIRENNIUNIU_KINDID:
			break;
		default:
			return false;
	}

	return true;
}

void ModeLayer::onModeClick(cocos2d::Ref* pSender, ControlButton::EventType e)
{
	//Control* ctr = (Control*)pSender;
	//G_NOTIFY("MODE_SELECTED", MTData::create(ctr->getTag()-1));
}

//////////////////////////////////////////////////////////////////////////
// IServerListDataSink
//游戏种类星系 接收 完成通知
void ModeLayer::OnGameItemFinish()
{
	setDatas();
}

// 房间列表接收完成 完成通知
void ModeLayer::OnGameKindFinish(uint16 wKindID)
{
	ModeScene *mc_ = dynamic_cast<ModeScene*>(Director::getInstance()->getRunningScene());
	Scene * nowScene = Director::getInstance()->getRunningScene();

	if (mc_ && nowScene->getChildByTag(101) == NULL){
		mc_->setSelectRoomId(wKindID);
		ServerListLayer * layer = ServerListLayer::create(1);
		nowScene->addChild(layer,0,101);
	}
}

//更新通知
void ModeLayer::OnGameItemUpdateFinish()
{
}

//系统消息接收完成
void ModeLayer::OnSystemMessageFinish()
{
	m_TimeRate.clear();

	CServerListData * pServerListData = CServerListData::shared();
	for (int i = 0; i < pServerListData->m_SystemMessageVector.size(); i++)
	{
		m_TimeRate.push_back(pServerListData->m_SystemMessageVector[i]->dwTimeRate);
	}
}

//插入通知
void ModeLayer::OnGameItemInsert(CGameListItem * pGameListItem)
{
	//构造数据
	switch (pGameListItem->GetItemGenre())
	{
	case ItemGenre_Type:		//游戏类型
	{
									break;
	}
	case ItemGenre_Kind:		//游戏种类
	{
									break;
	}
	case ItemGenre_Node:		//游戏节点
	{
									break;
	}
	case ItemGenre_Server:		//游戏房间
	{
									// 									if (addServer((CGameServerItem*)pGameListItem))
									// 									{
									// 									}
									// 									break;
	}
	case ItemGenre_Page:	//定制子项
	{
								break;
	}
	}
}

//更新通知
void ModeLayer::OnGameItemUpdate(CGameListItem * pGameListItem)
{
	//构造数据
	switch (pGameListItem->GetItemGenre())
	{
	case ItemGenre_Type:		//游戏类型
	{
									break;
	}
	case ItemGenre_Kind:		//游戏种类
	{
									break;
	}
	case ItemGenre_Node:		//游戏节点
	{
									break;
	}
	case ItemGenre_Server:		//游戏房间
	{
									// 									updateServer((CGameServerItem*)pGameListItem);
									// 									//更新视图
									// 									break;
	}
	case ItemGenre_Page:	//定制子项
	{
								break;
	}
	}
}

//删除通知
void ModeLayer::OnGameItemDelete(CGameListItem * pGameListItem)
{
	//构造数据
	switch (pGameListItem->GetItemGenre())
	{
	case ItemGenre_Type:		//游戏类型
	{
									break;
	}
	case ItemGenre_Kind:		//游戏种类
	{
									break;
	}
	case ItemGenre_Node:		//游戏节点
	{
									break;
	}
	case ItemGenre_Server:		//游戏房间
	{
									// 									if (removeServer((CGameServerItem*)pGameListItem))
									// 									{
									// 									}
									// 									break;
	}
	case ItemGenre_Page:	//定制子项
	{
								break;
	}
	}
}

void ModeLayer::onEnterTransitionDidFinish()
{
	CCLayer::onEnterTransitionDidFinish();
}

void ModeLayer::sendSystemMessage(float dt)
{
	mLoginMission.getSystemMessage();
}

