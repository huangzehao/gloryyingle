#ifndef _ModeScene_H_
#define _ModeScene_H_

#include "2d/CCScene.h"
#include "Platform/PFKernel/CGPIndividualMission.h"
#include "Platform/PFView/BaseLayer.h"

class TopHeadLayer;
class PlatformBelowBtn;
class ModeScene
	: public cocos2d::Scene
	, IGPIndividualMissionSink
{
public:
	//CREATE_FUNC(ModeScene);
	static ModeScene * create();

	///< 设置选择房间的ID
	void setSelectRoomId(int roomId);
	int getSelectRoomId();
private:
	ModeScene();
	~ModeScene();
	bool init();

	
public:
	virtual void onEnterTransitionDidFinish();

public:
	virtual void addChild(Node* child_) override;

	BaseModeLayer *getLayerByType(BaseModeLayer::ModeLayerType type);
	bool removeLayer(BaseModeLayer::ModeLayerType type);

	bool changeModeLayer(BaseModeLayer::ModeLayerType type, CGameServerItem * mitem = nullptr );

	bool isLayer(BaseModeLayer::ModeLayerType type){
		return type == m_type;
	}
	///< 后台返回
	void func_background_reback(cocos2d::Ref* obj);
	//桌子创建完毕进入
	void createFinishGoto(Ref* obj);
private:
	void closeCallback(cocos2d::Node *pNode);

	BaseModeLayer::ModeLayerType m_type;

	bool m_need_remove_after_move;
	BaseLayer *m_cur_layer;
	BaseModeLayer *layer;

	static ModeScene * _Platform_Scene;

	int mRoomId;
protected:
	TopHeadLayer * top_layer_;
	PlatformBelowBtn * platformBtn_layer_;
};
#endif // _ModeScene_H_