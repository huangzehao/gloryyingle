#include "PlatformBankDredge.h"
#include "GiveChoiceLayer.h"
#include "ModeScene.h"
#include "TopHeadLayer.h"
#include "Platform/PFView/LoginScene/LoginScene.h"
#include "Tools/Dialog/NewDialog.h"
#include "Tools/Manager/SoundManager.h"
#include "Tools/tools/StaticData.h"
#include "Platform/PFDefine/data/GlobalUserInfo.h"
USING_NS_CC;
using namespace ui;

CCScene* PlatformBankDredge::scene()
{
	CCScene* scene = CCScene::create();

	if (!scene)
		return 0;

	PlatformBankDredge* layer = PlatformBankDredge::create();
	if (layer)
		scene->addChild(layer);
	return scene;
}

//////////////////////////////////////////////////////////////////////////
PlatformBankDredge::PlatformBankDredge()
: mInsureMission(ADDRESS_URL, SERVER_PORT)
{
	mInsureMission.setMissionSink(this);
	m_type = BaseModeLayer::ModeLayerTypeBankOpen;
}

PlatformBankDredge::~PlatformBankDredge()
{
	mInsureMission.setMissionSink(0);
}

//初始化方法
bool PlatformBankDredge::init()
{
	do
	{
		CC_BREAK_IF(!BaseLayer::initWithJsonFile("LobbyResources/PlatformBankDredge.json"));
		CC_BREAK_IF(!this->setUpdateView());

		return true;
	} while (0);

	return false;
}

// 初始化组件
bool PlatformBankDredge::setUpdateView()
{
	do
	{
		//界面下移
		m_root_widget->setPositionY(850);

		m_root_widget->runAction(CCSequence::create(
			MoveTo::create(0.25f, Vec2(0, 0)),
			MoveTo::create(0.1f, Vec2(0, 100)),
			MoveTo::create(0.05f, Vec2(0, 0)),
			MoveTo::create(0.04f, Vec2(0, 30)),
			MoveTo::create(0.03f, Vec2(0, 0)),
			0));

		btn_Dredge = dynamic_cast<ui::Button *>(m_root_widget->getChildByName("btn_Dredge"));
		CCAssert(btn_Dredge, "");
		btn_Dredge->addTouchEventListener(this, SEL_TouchEvent(&PlatformBankDredge::dredgeButtonTouch));

		tf_bankPW = dynamic_cast<ui::TextField *>(m_root_widget->getChildByName("Panel_1")->getChildByName("tf_bankPW"));
		CCAssert(tf_bankPW, "");

		tf_agBankPw = dynamic_cast<ui::TextField *>(m_root_widget->getChildByName("Panel_2")->getChildByName("tf_agBankPw"));
		CCAssert(tf_agBankPw, "");

		tf_accPw = dynamic_cast<ui::TextField *>(m_root_widget->getChildByName("Panel_3")->getChildByName("tf_accPw"));
		CCAssert(tf_accPw, "");

		btn_close = dynamic_cast<ui::Button *>(m_root_widget->getChildByName("btn_close"));
		CCAssert(btn_close, "");
		btn_close->addTouchEventListener(this, SEL_TouchEvent(&PlatformBankDredge::closeButtonTouch));

		return true;
	} while (0);

	return false;
}
void PlatformBankDredge::closeButtonTouch(cocos2d::Ref * ref, ui::TouchEventType eventType_)
{
	if (eventType_ == TouchEventType::TOUCH_EVENT_BEGAN)
	{
		PLAY_BUTTON_CLOSE_EFFECT
	}
	if (eventType_ == TouchEventType::TOUCH_EVENT_ENDED)
	{
		m_root_widget->runAction(CCSequence::create(MoveTo::create(0.2f, Vec2(0, 850)), CCCallFunc::create(this, callfunc_selector(PlatformBankDredge::closeMoveTo)), NULL));
	}
}

void PlatformBankDredge::closeMoveTo()
{
	this->removeFromParent();
}

void PlatformBankDredge::dredgeButtonTouch(cocos2d::Ref * ref, ui::TouchEventType eventType_)
{
	if (eventType_ == TouchEventType::TOUCH_EVENT_BEGAN)
	{
		PLAY_BUTTON_CLICK_EFFECT
	}
	if (eventType_ == TouchEventType::TOUCH_EVENT_ENDED)
	{
		
		std::string str_pw = tf_bankPW->getString();
		std::string str_agPw = tf_agBankPw->getString();

		if (str_pw.size() >= 6 && str_agPw.size() >= 6)
		{
			if (str_pw == str_agPw)
			{
				mInsureMission.enable(tf_accPw->getString().c_str(), str_pw.c_str());
			}
		}
		
	}
}

void PlatformBankDredge::onEnterTransitionDidFinish()
{
	BaseModeLayer::onEnterTransitionDidFinish();
}

void PlatformBankDredge::onInsureInfo()
{

}

void PlatformBankDredge::onInsureSuccess(int type, const char* szDescription)
{

}

void PlatformBankDredge::onInsureFailure(int type, const char* szDescription)
{
	NewDialog::create(szDescription, NewDialog::NONEBUTTON);
}

bool PlatformBankDredge::onInsureTransferConfirm(const char* szMessage)
{
	return true;
}


void PlatformBankDredge::onInsureEnableResult(int type, const char* szMessage)
{
	CGlobalUserInfo* pGlobalUserInfo = CGlobalUserInfo::GetInstance();
	tagGlobalUserData * pGlobalUserData = pGlobalUserInfo->GetGlobalUserData();

	bool InsureEnable = pGlobalUserData->cbInsureEnabled;

	ModeScene *mc_ = dynamic_cast<ModeScene*>(Director::getInstance()->getRunningScene());
	///< 开通成功
	if (mc_ && pGlobalUserData->cbInsureEnabled){
		this->removeFromParent();

		Scene * nowScene = Director::getInstance()->getRunningScene();
		GiveChoiceLayer * layer = GiveChoiceLayer::create();
		nowScene->addChild(layer);
	}
	else
	{
		///< 提示开通失败
		NewDialog::create(szMessage, NewDialog::NONEBUTTON);
	}
}


