#include "PlatformBelowBtn.h"
#include "SignInLayer.h"
#include "LowProtectLayer.h"
#include "PlatformBankDredge.h"
#include "GiveChoiceLayer.h"
#include "PayLayer.h"
#include "SettingLayer.h"
#include "RanklistLayer.h"
#include "Tools/Manager/SoundManager.h"
#include "Tools/tools/StaticData.h"
#include "Tools/Dialog/NewDialog.h"
#include "Tools/tools/StringData.h"
#include "Tools/tools/gPlatform.h"
//#include "C2DXShareSDK/C2DXShareSDK.h"
//#include "C2DXShareSDK/C2DXShareSDKTypeDef.h"

#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID || CC_TARGET_PLATFORM == CC_PLATFORM_WIN32)
#include "cocostudio\CocoStudio.h"
#elif (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
#include "cocostudio/CCArmatureDataManager.h"
#include "cocostudio/CCArmature.h"
#endif

USING_NS_CC;
using namespace ui;
using namespace std;
using namespace cocostudio;
//using namespace cn::sharesdk;

#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID || CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
using namespace cocos2d::experimental::ui;
#endif

//////////////////////////////////////////////////////////////////////////
PlatformBelowBtn::PlatformBelowBtn()
:mIndividualMission(ADDRESS_URL, SERVER_PORT)
{
	mIndividualMission.setMissionSink(this);
	// 加载资源文件 png 图片，plist 文件和 ExportJson 动画文件，一般而言可以在场景运行之初加载此资源
	ArmatureDataManager::sharedArmatureDataManager()->addArmatureFileInfo("LobbyResources/GameSelect/shangchenganniu0.png", "LobbyResources/GameSelect/shangchenganniu0.plist", "LobbyResources/GameSelect/shangchenganniu.ExportJson");
}

PlatformBelowBtn::~PlatformBelowBtn()
{
	mIndividualMission.setMissionSink(0);
}

//初始化方法
bool PlatformBelowBtn::init()
{
	do
	{
		CC_BREAK_IF(!BaseLayer::initWithJsonFile("LobbyResources/PlatformBelowBtn.json"));
		CC_BREAK_IF(!this->setUpdateView());

		return true;
	} while (0);

	return false;
}

// 初始化组件
bool PlatformBelowBtn::setUpdateView()
{
	do
	{
		CGlobalUserInfo * pGlobalUserInfo = CGlobalUserInfo::GetInstance();
		tagGlobalUserData * pGlobalUserData = pGlobalUserInfo->GetGlobalUserData();

		ui::Widget * panel_button = dynamic_cast<ui::Widget *>(m_root_widget->getChildByName("panel_button"));

		///< 打开签到面板
		ui::Button * btn_signIn = dynamic_cast<ui::Button *>(panel_button->getChildByName("btn_signIn"));
		CCAssert(btn_signIn, "");
		btn_signIn->addTouchEventListener([=](cocos2d::Ref * ref_, ui::Widget::TouchEventType eventType_)
		{
			if (eventType_ == ui::Widget::TouchEventType::BEGAN)
			{
				PLAY_BUTTON_CLICK_EFFECT
			}
			if (eventType_ == ui::Widget::TouchEventType::ENDED)
			{
				Scene * nowScene = Director::getInstance()->getRunningScene();
				SignInLayer * layer = SignInLayer::create();

				nowScene->addChild(layer);
			}
		});

		///< 打开低保面板
		ui::Button * btn_lowProtect = dynamic_cast<ui::Button *>(panel_button->getChildByName("btn_lowProtect"));
		CCAssert(btn_lowProtect, "");
		btn_lowProtect->addTouchEventListener([=](cocos2d::Ref * ref_, ui::Widget::TouchEventType eventType_)
		{
			if (eventType_ == ui::Widget::TouchEventType::BEGAN)
			{
				PLAY_BUTTON_CLICK_EFFECT
			}
			if (eventType_ == ui::Widget::TouchEventType::ENDED)
			{
				Scene * nowScene = Director::getInstance()->getRunningScene();
				LowProtectLayer * layer = LowProtectLayer::create();

				nowScene->addChild(layer);
			}
		});

		///< 打开银行面板
		ui::Button * btn_bank = dynamic_cast<ui::Button *>(panel_button->getChildByName("btn_bank"));
		CCAssert(btn_bank, "");
		btn_bank->addTouchEventListener([=](cocos2d::Ref * ref_, ui::Widget::TouchEventType eventType_)
		{
			if (eventType_ == ui::Widget::TouchEventType::BEGAN)
			{
				PLAY_BUTTON_CLICK_EFFECT
			}
			if (eventType_ == ui::Widget::TouchEventType::ENDED)
			{
				if (pGlobalUserData->cbInsureEnabled)
				{
					//正常进入银行界面
					Scene * nowScene = Director::getInstance()->getRunningScene();
					GiveChoiceLayer * layer = GiveChoiceLayer::create();
					nowScene->addChild(layer);
				}
				else
				{
					//第一次进入银行设置密码界面
					Scene * nowScene = Director::getInstance()->getRunningScene();
					PlatformBankDredge * layer = PlatformBankDredge::create();
					nowScene->addChild(layer);
				}
			}
		});

		///< 打开商城充值面板
		ui::Button * btn_shop = dynamic_cast<ui::Button *>(panel_button->getChildByName("btn_shop"));
		CCAssert(btn_shop, "");
		btn_shop->addTouchEventListener([=](cocos2d::Ref * ref_, ui::Widget::TouchEventType eventType_)
		{
			if (eventType_ == ui::Widget::TouchEventType::BEGAN)
			{
				PLAY_BUTTON_CLICK_EFFECT
			}
			if (eventType_ == ui::Widget::TouchEventType::ENDED)
			{
// 				Scene * nowScene = Director::getInstance()->getRunningScene();
// 				PayLayer * layer = PayLayer::create();
// 
// 				nowScene->addChild(layer);
				NewDialog::create(SSTRING("pay_gold"), NewDialog::NONEBUTTON);
			}
		});

		// 商城外层骨骼动画
		// 这里直接使用 CsAnim ，而此信息保存在 CsAnim.ExportJson 中，与其创建的项目属性相对应
		Armature* armature = Armature::create("shangchenganniu");
		// 设置当前运行动画的索引，一个“工程”可以建立多个动画
		armature->getAnimation()->playByIndex(0);
		// 设置位置信息
		armature->setPosition(Vec2(710, panel_button->getPositionY() + 30));
		// 添加到容器，当前运行的场景之中
		this->addChild(armature);
 
		///< 打开排行面板
		ui::Button * btn_ranking = dynamic_cast<ui::Button *>(panel_button->getChildByName("btn_ranking"));
		CCAssert(btn_ranking, "");
		btn_ranking->addTouchEventListener([=](cocos2d::Ref * ref_, ui::Widget::TouchEventType eventType_)
		{
			if (eventType_ == ui::Widget::TouchEventType::BEGAN)
			{
				PLAY_BUTTON_CLICK_EFFECT
			}
			if (eventType_ == ui::Widget::TouchEventType::ENDED)
			{
				///< 打开游戏排行
				Scene * nowScene = Director::getInstance()->getRunningScene();
				RanklistLayer * layer = RanklistLayer::create();

				nowScene->addChild(layer);
			}
		});

		///< 打开设置面板
		ui::Button * btn_options = dynamic_cast<ui::Button *>(panel_button->getChildByName("btn_options"));
		CCAssert(btn_options, "");
		btn_options->addTouchEventListener([=](cocos2d::Ref * ref_, ui::Widget::TouchEventType eventType_)
		{
			if (eventType_ == ui::Widget::TouchEventType::BEGAN)
			{
				PLAY_BUTTON_CLICK_EFFECT
			}
			if (eventType_ == ui::Widget::TouchEventType::ENDED)
			{
				Scene * nowScene = Director::getInstance()->getRunningScene();
				SettingLayer * layer = SettingLayer::create();

				nowScene->addChild(layer);
			}
		});

		///< 打开分享面板
		ui::Button * btn_share = dynamic_cast<ui::Button *>(panel_button->getChildByName("btn_share"));
		CCAssert(btn_share, "");
		btn_share->addTouchEventListener([=](cocos2d::Ref * ref_, ui::Widget::TouchEventType eventType_)
		{
 			if (eventType_ == ui::Widget::TouchEventType::BEGAN)
 			{
 				PLAY_BUTTON_CLICK_EFFECT
 			}
 			if (eventType_ == ui::Widget::TouchEventType::ENDED)
 			{
 				///< 分享sdk
 				//showShareMenuClickHandler();
				int type = atoll(SSTRING("WECHAT_SHARE_TYPE"));
				QSJniFun::shareUrlWX(SSTRING("WECHAT_SHARE_URL"), SSTRING("WECHAT_SHARE_TITTLE"), SSTRING("WECHAT_SHARE_DESC"), type);
 			}
		});

		return true;
	} while (0);

	return false;
}
void PlatformBelowBtn::closeButtonTouch(cocos2d::Ref * ref, ui::TouchEventType eventType_)
{
	if (eventType_ == TouchEventType::TOUCH_EVENT_ENDED)
	{
		this->removeFromParent();
	}
}
//
////分享回调
//void shareContentResultHandler(int seqId, cn::sharesdk::C2DXResponseState state, cn::sharesdk::C2DXPlatType platType, __Dictionary *result)
//{
//	switch (state)
//	{
//	case cn::sharesdk::C2DXResponseStateSuccess:
//	{
//												Size visibleSize = Director::getInstance()->getVisibleSize();
//												Size webSize = Director::getInstance()->getVisibleSize();
//												webSize.height = 0.01f;
//												webSize.width = 0.01f;
//
//												CGlobalUserInfo * pGlobalUserInfo = CGlobalUserInfo::GetInstance();
//												tagGlobalUserData * pGlobalUserData = pGlobalUserInfo->GetGlobalUserData();
//
//												int m_platType;
//
//												switch (platType)
//												{
//												case cn::sharesdk::C2DXPlatTypeWeChat: m_platType = 1;
//													break;
//												case cn::sharesdk::C2DXPlatTypeWeChatMoments: m_platType = 2;
//													break;
//												case cn::sharesdk::C2DXPlatTypeQQ: m_platType = 3;
//													break;
//												case cn::sharesdk::C2DXPlatTypeWeChatFavorites: m_platType = 4;
//													break;
//												default:
//													m_platType = 0;
//													break;
//												}
//
//// 												std::string Link = StringUtils::format("http://www.757game.com/Moblie/Share.aspx?userid=%d$%d", pGlobalUserData->dwUserID, m_platType);
//// 
//// #if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID || CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
//// 												WebView *webView = WebView::create();
//// 												webView->setPosition(Vec2(3000,1500));
//// 												webView->setContentSize(webSize);
//// 
//// 												webView->loadURL(Link);
//// 												webView->setScalesPageToFit(true);
//// 												webView->setOnShouldStartLoading([](WebView *sender, const std::string &url){
//// 
//// 													return true;
//// 												});
//// 												webView->setOnDidFinishLoading([](WebView *sender, const std::string &url){
//// 
//// 												});
//// 												webView->setOnDidFailLoading([](WebView *sender, const std::string &url){
//// 
//// 												});
//// 
//// 												Scene * nowScene = Director::getInstance()->getRunningScene();
//// 												webView->setTag(100);
//// 												nowScene->addChild(webView,0);
//// 
//// 												nowScene->scheduleOnce(SEL_SCHEDULE(&PlatformBelowBtn::removeShareWebview), 1.0f);
//// 
//// #end/*												log("Share is Success , pGlobalUserData->dwUserID is %d platType is %d", pGlobalUserData->dwUserID,platType);*/
//	}
//		break;
//	case cn::sharesdk::C2DXResponseStateFail:
//	{
//												log("Share is Fail");
//												//回调错误信息
//												__Array *allKeys = result->allKeys();
//												allKeys->retain();
//												for (int i = 0; i < allKeys->count(); i++)
//												{
//													__String *key = (__String*)allKeys->getObjectAtIndex(i);
//													Ref *obj = result->objectForKey(key->getCString());
//
//													log("key = %s", key->getCString());
//													if (dynamic_cast<__String *>(obj))
//													{
//														log("value = %s", dynamic_cast<__String *>(obj)->getCString());
//														cocos2d::Director::getInstance()->getRunningScene()->runAction(cocos2d::Sequence::createWithTwoActions(cocos2d::DelayTime::create(2), cocos2d::CallFunc::create([=]() {
//															NewDialog::create(cocos2d::StringUtils::format("key = %s, value = %s", key->getCString(), dynamic_cast<__String *>(obj)->getCString()), NewDialog::AFFIRM);
//														})));
//
//													}
//													else if (dynamic_cast<__Integer *>(obj))
//													{
//														log("value = %d", dynamic_cast<__Integer *>(obj)->getValue());
//														cocos2d::Director::getInstance()->getRunningScene()->runAction(cocos2d::Sequence::createWithTwoActions(cocos2d::DelayTime::create(2), cocos2d::CallFunc::create([=]() {
//															NewDialog::create(cocos2d::StringUtils::format("key = %s, value = %d", key->getCString(), dynamic_cast<__Integer *>(obj)->getValue()), NewDialog::AFFIRM);
//														})));
//													}
//													else if (dynamic_cast<__Double *>(obj))
//													{
//														log("value = %f", dynamic_cast<__Double *>(obj)->getValue());
//														cocos2d::Director::getInstance()->getRunningScene()->runAction(cocos2d::Sequence::createWithTwoActions(cocos2d::DelayTime::create(2), cocos2d::CallFunc::create([=]() {
//															NewDialog::create(cocos2d::StringUtils::format("key = %s, value = %f", key->getCString(), dynamic_cast<__Double *>(obj)->getValue()), NewDialog::AFFIRM);
//														})));
//													}
//												}
//
//
//	}
//		break;
//	case cn::sharesdk::C2DXResponseStateCancel:
//	{
//												  log("Cancel");
//	}
//		break;
//	default:
//		break;
//	}
//
//}


void PlatformBelowBtn::removeShareWebview(float dt)
{
	Scene * nowScene = Director::getInstance()->getRunningScene();
	nowScene->removeChildByTag(100,true);
}
//
//void PlatformBelowBtn::showShareMenuClickHandler()
//{
//	__Dictionary *content = __Dictionary::create();
//
//#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID || CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
//	content->setObject(__String::create("赢乐电玩城，好玩又刺激 人气超火爆，土豪云集！"), "title");
//	//content->setObject(__String::create("http://ofuv3d1ck.bkt.clouddn.com/Icon/icon.png"), "image");
//	content->setObject(__String::create("https://fir.im/y9sv"), "url");
//#endif
//
//#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
//	//可以自定义分享平台，如果平台传入NULL，此时显示所有初始化的平台//
//	C2DXArray *platforms = C2DXArray::create();//
//	__Integer *weixin = new __Integer(cn::sharesdk::C2DXPlatTypeWeChat);//
//	__Integer *qq = new __Integer(cn::sharesdk::C2DXPlatTypeQQ);//
//	__Integer *weipengyou = new __Integer(cn::sharesdk::C2DXPlatTypeWeChatMoments);//
//	__Integer *qqkongjian = new __Integer(cn::sharesdk::C2DXPlatTypeQZone);//
//	__Integer *xinlang = new __Integer(cn::sharesdk::C2DXPlatTypeSinaWeibo);//
//	__Integer *youjian = new __Integer(cn::sharesdk::C2DXPlatTypeMail);//
//	platforms->addObject(weixin);//
//	platforms->addObject(qq);//
//	platforms->addObject(weipengyou);//
//	platforms->addObject(qqkongjian);//
//	platforms->addObject(xinlang);//
//	platforms->addObject(youjian);//
//	C2DXShareSDK::showShareMenu(platforms, content, 100, 100, shareContentResultHandler);
//#elif (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
//	C2DXShareSDK::showShareMenu(NULL, content, 100, 100, shareContentResultHandler);
//#endif
//
//	
//}
//
