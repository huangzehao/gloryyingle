#ifndef _UserInfoLayer_H_
#define _UserInfoLayer_H_

#include "cocos2d.h"
#include "ui/CocosGUI.h"
#include "Platform/PFView/BaseLayer.h"
#include "Platform/PFKernel/CGPFaceMission.h"

class UserInfoLayer
	: public BaseModeLayer
	, public IGPFaceMissionSink
{
public:
	CREATE_FUNC(UserInfoLayer);

public:
	UserInfoLayer();
	virtual ~UserInfoLayer();
	bool init();
	///< 新初始化
	bool newInit();
	//设置显示
	void setDatas();

	void setCloseDialogInfo(cocos2d::Ref* target, cocos2d::SEL_CallFuncN callfun, const std::string& title, const std::string& content);

	///< 头像是否被改动!
	bool headAlearyChange();
	//////////////////////////////////////////////////////////////////////////
	//更新图片信息
	void updateInfo(int tag);
	virtual void onEnterTransitionDidFinish();
private:
	// 第一版本的事件了
	void onBtnClick(cocos2d::Ref* obj, cocos2d::extension::Control::EventType  e);
	void imageHeadTouch(cocos2d::Ref * obj, cocos2d::ui::TouchEventType e);
	//////////////////////////////////////////////////////////////////////////
	//通知消息
private:
	void onUserName(cocos2d::Ref* obj);
	void onUserScore(cocos2d::Ref* obj);
	void onInsureQuery(cocos2d::Ref* obj);
	void onDelayQuery(float delta);

	//////////////////////////////////////////////////////////////////////////
	// IGPInsureMissionSink
private:
	virtual void onInsureInfo();

	///< 回调
public:
	virtual void onGPFaceInfo();
	virtual void onGPFaceSuccess(const char* szDescription);
	virtual void onGPFaceFailure(const char* szDescription);
	///< 关闭回调
	void closeButtonTouch(cocos2d::Ref * ref, ui::TouchEventType eventType_);
	///<关闭移动回调
	void closeMoveTo();
	///< 链接网络更新头像,
	void connectNetworkUpdateHead();
	void resumeStartHead();
private:
//	cocos2d::Label* mLbNickname;
//	cocos2d::Label* mLbScore;
//	cocos2d::Label* mLbMedal;
	///< 昵称
	ui::Text *nickname_;
	///< 用户名
	ui::Text * name_;
	///< 游戏钱
	ui::TextAtlas *game_money_;
	///< 游戏元宝
	ui::TextAtlas *game_silver;
	///< 游戏id
	ui::TextAtlas *game_id_;
// 	///< 游戏等级
// 	ui::TextAtlas* lv_;
	///< 玩家头像
	ui::ImageView * img_headBg;
	//////////////////////////////////////////////////////////////////////////
	cocos2d::Ref*		mTarget;
	cocos2d::SEL_CallFuncN	mCallback;
	std::string				mTitle;
	std::string				mContent;

	// 头像的标签, 可能需要传递给服务器数据的时候进行传输
	enum HEAdTAG
	{
		IMAGE_HEAD_1 = 960,
		IMAGE_HEAD_2 = 962,
		IMAGE_HEAD_3 = 964,
		IMAGE_HEAD_4 = 965,
		IMAGE_HEAD_5 = 966,
		IMAGE_HEAD_6 = 967,
		IMAGE_HEAD_7 = 968,
		IMAGE_HEAD_8 = 969,

	};

	// 是否改变头像
	bool isChangeHead;
	// 一开始的头像id
	int startHeadId;
	// 结束头像id
	int endHeadId;
	CGPFaceMission	mInsureMission;
};

#endif // _UserInfoLayer_H_