#include "TopHeadLayer.h"
#include "Tools/tools/MTNotification.h"
#include "Platform/PFView/ModeScene/ModeScene.h"
#include "Platform/PFView/LoginScene/LoginScene.h"
#include "Tools/tools/StaticData.h"
#include "Tools/tools/StringData.h"
#include "GiveChoiceLayer.h"
#include "SettingLayer.h"
#include "PayLayer.h"
#include "ConversionLayer.h"
#include "Tools/Dialog/NewDialog.h"
#include "Platform/PFView/ModeScene/UserInfoLayer.h"
#include "Tools/tools/YRCommon.h"
#include "Tools/Manager/SoundManager.h"
#include "Platform/PFDefine/data/GlobalUserInfo.h"
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID || CC_TARGET_PLATFORM == CC_PLATFORM_WIN32)
#include "cocostudio\CocoStudio.h"
#elif (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
#include "cocostudio/CCArmatureDataManager.h"
#include "cocostudio/CCArmature.h"
#endif

USING_NS_CC;
using namespace ui;
using namespace std;
using namespace cocostudio;

TopHeadLayer::TopHeadLayer()
: m_bank_mission(ADDRESS_URL, SERVER_PORT), 
m_personal_mission(ADDRESS_URL, SERVER_PORT), 
m_individual_mission(ADDRESS_URL, SERVER_PORT)
{
	m_type = (BaseModeLayer::ModeLayerType::ModeLayerTypeDefault);
	m_bank_mission.setMissionSink(this);
	m_personal_mission.setMissionSink(this);
	m_individual_mission.setMissionSink(this);
	// 加载资源文件 png 图片，plist 文件和 ExportJson 动画文件，一般而言可以在场景运行之初加载此资源
	ArmatureDataManager::sharedArmatureDataManager()->addArmatureFileInfo("LobbyResources/GameSelect/touxiangkuang0.png", "LobbyResources/GameSelect/touxiangkuang0.plist", "LobbyResources/GameSelect/touxiangkuang.ExportJson");
}

TopHeadLayer::~TopHeadLayer()
{
	m_bank_mission.setMissionSink(0);
	m_personal_mission.setMissionSink(0);
	m_individual_mission.setMissionSink(0);
	this->unscheduleUpdate();
}

bool TopHeadLayer::init()
{
	return newInit();
}

///< 新UI的创建
bool TopHeadLayer::newInit()
{
	do
	{
		CGlobalUserInfo * pGlobalUserInfo = CGlobalUserInfo::GetInstance();
		tagGlobalUserData * pGlobalUserData = pGlobalUserInfo->GetGlobalUserData();
		tagUserInsureInfo* pGlobalInsureInfo = pGlobalUserInfo->GetUserInsureInfo();

		CC_BREAK_IF(!BaseLayer::initWithJsonFile("LobbyResources/PlayformTopHead.json"));

		Node *panel_top = m_root_widget->getChildByName("panel_top");
		CC_BREAK_IF(!panel_top);

// 		ui::Button *btn_bank = dynamic_cast<ui::Button*>(panel_top->getChildByName("btn_bank"));
// 		btn_bank->addTouchEventListener([=](Ref* sender_, ui::Widget::TouchEventType event_type_){
// 			if (event_type_ == ui::Widget::TouchEventType::BEGAN){
// 				PLAY_BUTTON_CLICK_EFFECT
// 			}
// 			if (event_type_ == ui::Widget::TouchEventType::ENDED){
// 				
// 				ModeScene *mc_ = dynamic_cast<ModeScene*>(Director::getInstance()->getRunningScene());
// 				if (mc_){
// 					if (mc_->isLayer(BaseModeLayer::ModeLayerTypeInfo))
// 					{
// 						// 发送一条消息到信息类, 获取用户信息类
// 						UserInfoLayer * uInfo = dynamic_cast<UserInfoLayer *>(mc_->getLayerByType(BaseModeLayer::ModeLayerTypeInfo));
// 						///< 改变过头像,询问是否保存
// 						NewDialog::create(SSTRING("info_Panel_headInfoaffirm"), NewDialog::AFFIRMANDCANCEL, [=]()
// 						{
// 							///< 确认回调.
// 							//1.首先联网更新更新
// 							uInfo->connectNetworkUpdateHead();
// 							///< 可以使用银行了.就直接进入银行
// 							if (mc_ && pGlobalUserData->cbInsureEnabled){
// 								mc_->changeModeLayer(BaseModeLayer::ModeLayerTypeBank);
// 							}
// 							else{
// 								mc_->changeModeLayer(BaseModeLayer::ModeLayerTypeBankOpen);
// 							}
// 						},
// 							[=](){
// 							uInfo->resumeStartHead();
// 						}
// 						);
// 					}
// 					else
// 					{
// 						///< 可以使用银行了.就直接进入银行
// 						if (mc_ && pGlobalUserData->cbInsureEnabled){
// 							mc_->changeModeLayer(BaseModeLayer::ModeLayerTypeBank);
// 						}
// 						else{
// 							mc_->changeModeLayer(BaseModeLayer::ModeLayerTypeBankOpen);
// 						}
// 					}
// 				}		
// 			}
// 		});
// 
// 		ui::Button *btn_back = dynamic_cast<ui::Button*>(panel_top->getChildByName("btn_back"));
// 		btn_back->addTouchEventListener([=](Ref* sender_, ui::Widget::TouchEventType event_type_){
// 
// 			if (event_type_ == ui::Widget::TouchEventType::BEGAN){
// 				PLAY_BUTTON_CLICK_EFFECT
// 				
// 			}
// 			if (event_type_ == ui::Widget::TouchEventType::ENDED){
// 				btn_back->setTouchEnabled(false);
// 				this->scheduleOnce(SEL_SCHEDULE(&TopHeadLayer::btnClickEvent), 0.3f);
// 
// 				ModeScene *mc_ = dynamic_cast<ModeScene*>(Director::getInstance()->getRunningScene());
// 				if (mc_){
// 					if (mc_->isLayer(BaseModeLayer::ModeLayerTypeDefault)){
// 						///< 确认退出平台
// 						NewDialog::create(SSTRING("back_to_login_content"), NewDialog::AFFIRMANDCANCEL, [=]()
// 						{
// 							///< 确认回调.
// 							Director::getInstance()->pushScene(CCTransitionFade::create(0.5f, LoginScene::create()));
// 						}
// 						);
// 						
// 					}
// 					else if (mc_->isLayer(BaseModeLayer::ModeLayerTypeInfo))
// 					{
// 						// 发送一条消息到信息类, 获取用户信息类
// 						UserInfoLayer * uInfo = dynamic_cast<UserInfoLayer *>(mc_->getLayerByType(BaseModeLayer::ModeLayerTypeInfo));
// 						if (uInfo && uInfo->headAlearyChange())
// 						{
// 							///< 改变过头像,询问是否保存
// 							NewDialog::create(SSTRING("info_Panel_headInfoaffirm"), NewDialog::AFFIRMANDCANCEL, [=]()
// 							{
// 								///< 确认回调.
// 								//1.首先联网更新更新
// 								uInfo->connectNetworkUpdateHead();
// 								mc_->changeModeLayer(BaseModeLayer::ModeLayerTypeDefault);
// 							},
// 								[=](){
// 								uInfo->resumeStartHead();
// 							}
// 							);
// 						}
// 						else
// 						{
// 							mc_->changeModeLayer(BaseModeLayer::ModeLayerTypeDefault);
// 						}
// 						
// 					}
// 					else if (mc_->isLayer(BaseModeLayer::ModeLayerTypeRoomList))
// 					{
// 						mc_->removeLayer(BaseModeLayer::ModeLayerTypeRoomList);
// 						mc_->changeModeLayer(BaseModeLayer::ModeLayerTypeDefault);
// 					}
// 					else if (mc_->isLayer(BaseModeLayer::ModeLayerTypeRoomInfo))
// 					{
// 						mc_->removeLayer(BaseModeLayer::ModeLayerTypeRoomInfo);
// 						mc_->changeModeLayer(BaseModeLayer::ModeLayerTypeRoomList);
// 					}
// 					else
// 					{
// 						mc_->changeModeLayer(BaseModeLayer::ModeLayerTypeDefault);
// 					}
// 				}
// 			}
// 		});
// 
// 		///< 转换函数需要微调
// 		ui::Button *btn_set = dynamic_cast<ui::Button*>(panel_top->getChildByName("btn_set"));
// 		btn_set->addTouchEventListener([=](Ref* sender_, ui::Widget::TouchEventType event_type_){
// 			if (event_type_ == ui::Widget::TouchEventType::BEGAN){
// 				PLAY_BUTTON_CLICK_EFFECT
// 			}
// 			if (event_type_ == ui::Widget::TouchEventType::ENDED){
// 				
// 
// 				ModeScene *mc_ = dynamic_cast<ModeScene*>(Director::getInstance()->getRunningScene());
// 				if (mc_){
// 					if (mc_->isLayer(BaseModeLayer::ModeLayerTypeInfo))
// 					{
// 						// 发送一条消息到信息类, 获取用户信息类
// 						UserInfoLayer * uInfo = dynamic_cast<UserInfoLayer *>(mc_->getLayerByType(BaseModeLayer::ModeLayerTypeInfo));
// 						///< 改变过头像,询问是否保存
// 						NewDialog::create(SSTRING("info_Panel_headInfoaffirm"), NewDialog::AFFIRMANDCANCEL, [=]()
// 						{
// 							///< 确认回调.
// 							//1.首先联网更新更新
// 							uInfo->connectNetworkUpdateHead();
// 							Scene *mc_ = (Director::getInstance()->getRunningScene());
// 							SettingLayer * setLayer = SettingLayer::create();
// 							mc_->addChild(setLayer);
// 						},
// 							[=](){
// 							uInfo->resumeStartHead();
// 						}
// 						);
// 					}
// 					else if (mc_->isLayer(BaseModeLayer::ModeLayerTypeBank))
// 					{///< 打开设置面板
// #if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID || CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
// 						webView->setVisible(false);
// #endif
// 						mc_->changeModeLayer(BaseModeLayer::ModeLayerTypeDefault);
// 						Scene *mc_ = (Director::getInstance()->getRunningScene());
// 						SettingLayer * setLayer = SettingLayer::create();
// 						mc_->addChild(setLayer);
// 					}
// 					else
// 					{
// 						Scene *mc_ = (Director::getInstance()->getRunningScene());
// 						SettingLayer * setLayer = SettingLayer::create();
// 						mc_->addChild(setLayer);
// 					}
// 				}
// 			}
// 		});

		ui::Layout *Panel_headInfo = dynamic_cast<ui::Layout*>(panel_top->getChildByName("Panel_headInfo"));

		ui::Button *btn_info = dynamic_cast<ui::Button*>(Panel_headInfo->getChildByName("btn_info"));
		btn_info->addTouchEventListener([=](Ref* sender_, ui::Widget::TouchEventType event_type_){
			if (event_type_ == ui::Widget::TouchEventType::BEGAN){
				PLAY_BUTTON_CLICK_EFFECT
			}
			if (event_type_ == ui::Widget::TouchEventType::ENDED){
				
				ModeScene *mc_ = dynamic_cast<ModeScene*>(Director::getInstance()->getRunningScene());
				if (mc_){
					Scene * nowScene = Director::getInstance()->getRunningScene();
					UserInfoLayer * layer = UserInfoLayer::create();
					nowScene->addChild(layer);
				}
			}
		});

		Armature* armature = Armature::create("touxiangkuang");
		armature->getAnimation()->playByIndex(0);
		armature->setScale(1.11);
		armature->setAnchorPoint(Vec2(0, 0));
		armature->setPosition(Vec2(0, 615));
		this->addChild(armature);

		m_txt_name = dynamic_cast<ui::Text*>(Panel_headInfo->getChildByName("img_namekuang")->getChildByName("lab_name"));
		if (m_txt_name){
			m_txt_name->setString(pGlobalUserData->szNickName);
		}

		al_level = dynamic_cast<ui::TextAtlas*>(Panel_headInfo->getChildByName("img_levelkuang")->getChildByName("al_level"));

		levelBar = dynamic_cast<ui::LoadingBar *>(Panel_headInfo->getChildByName("img_levelkuang")->getChildByName("levelBar"));
		CCAssert(levelBar, "");

		img_head = dynamic_cast<ui::ImageView *>(Panel_headInfo->getChildByName("img_head"));
		CCAssert(img_head, "");
		img_head->loadTexture(YRComGetHeadImageById(pGlobalUserData->wFaceID));

		Node *gold_bg_ = panel_top->getChildByName("img_gold");
		CCAssert(gold_bg_, "");

		money_ = dynamic_cast<ui::TextAtlas*>(gold_bg_->getChildByName("al_gold_num"));
		if (money_){
			money_->setString(StringUtils::format("%lld", pGlobalUserData->lUserScore));
		}

		///< 增加金币
		ui::Button *btn_add_gold_ = dynamic_cast<ui::Button*>(gold_bg_->getChildByName("btn_add_gold"));
		btn_add_gold_->addTouchEventListener([=](Ref* sender_, ui::Widget::TouchEventType event_type_){
			if (event_type_ == ui::Widget::TouchEventType::BEGAN){
				PLAY_BUTTON_CLICK_EFFECT
			}
			if (event_type_ == ui::Widget::TouchEventType::ENDED){
				Scene * nowScene = Director::getInstance()->getRunningScene();
				PayLayer * layer = PayLayer::create();

				nowScene->addChild(layer);
			}
		});

		Node *silver_bg_ = panel_top->getChildByName("img_diamond");
		CCAssert(silver_bg_, "");

		silver_ = dynamic_cast<ui::TextAtlas*>(silver_bg_->getChildByName("al_diamand_num"));
		if (silver_){
			silver_->setString(StringUtils::format("%lld", pGlobalUserData->lUserIngot));
		}

		///< 增加元宝
		ui::Button *btn_add_silver_ = dynamic_cast<ui::Button*>(silver_bg_->getChildByName("btn_add_diamond"));
		btn_add_silver_->addTouchEventListener([=](Ref* sender_, ui::Widget::TouchEventType event_type_){
			if (event_type_ == ui::Widget::TouchEventType::BEGAN){
				PLAY_BUTTON_CLICK_EFFECT
			}
			if (event_type_ == ui::Widget::TouchEventType::ENDED){
				///< 打开元宝兑换
				Scene * nowScene = Director::getInstance()->getRunningScene();
				ConversionLayer * layer = ConversionLayer::create();

				nowScene->addChild(layer);
			}
		});

		this->scheduleUpdate();
		return true;
	} while (false);

	return false;
}

void TopHeadLayer::onEnter()
{
	BaseLayer::onEnter();
	MyNotification::getInstance()->registerEvent("JFT_PAY_RESULT", [=](cocos2d::EventCustom* event) {
		int payStatus = *static_cast<int*>(event->getUserData());
		if (1 == payStatus)
		{
			m_individual_mission.queryIntegral();

			cocos2d::Director::getInstance()->getRunningScene()->runAction(cocos2d::Sequence::createWithTwoActions(cocos2d::DelayTime::create(2), cocos2d::CallFunc::create([=]() {
				NewDialog::create(SSTRING("payment_pay_finish"), NewDialog::AFFIRM);
			})));
		}
		else
		{
			cocos2d::Director::getInstance()->getRunningScene()->runAction(cocos2d::Sequence::createWithTwoActions(cocos2d::DelayTime::create(2), cocos2d::CallFunc::create([=]() {
				NewDialog::create(SSTRING("pay_fail"), NewDialog::AFFIRM);
			})));
		}
	});
}

void TopHeadLayer::onExit()
{
	BaseLayer::onExit();
	G_NOTIFY_UNREG("JFT_PAY_RESULT");
}


void TopHeadLayer::onEnterTransitionDidFinish()
{
	Layer::onEnterTransitionDidFinish();

	this->scheduleOnce(SEL_SCHEDULE(&TopHeadLayer::flushCoin), 0.5f);
	m_personal_mission.requestLevelInfo();

	this->scheduleUpdate();
}

void TopHeadLayer::onQueryLevelInfoSuccess(SCORE, SCORE, tchar[])
{
}

void TopHeadLayer::onQueryLevelUpTipsSuccess(int cur_lv_, int cur_exp_, dword next_exp_, dword reward_gold_, SCORE reward_ingot_)
{
	if (al_level){
		al_level->setString(StringUtils::format("%d", cur_lv_));
		float precennt = cur_exp_ * 100.0f / next_exp_;
		levelBar->setPercent(precennt);
	}	
}

void TopHeadLayer::onGPIndividualInfo(int type)
{
	this->update(0);
}

void TopHeadLayer::update(float delta)
{
	//log("ModeLayerType is %d ModeLayerType is %s", m_type, m_type);
	m_type = m_type;
	CGlobalUserInfo * pGlobalUserInfo = CGlobalUserInfo::GetInstance();
	tagGlobalUserData * pGlobalUserData = pGlobalUserInfo->GetGlobalUserData();

	if (money_){
		DIGIT_SCALE(money_, pGlobalUserData->lUserScore);	
		std::string str = StringUtils::format("%lld", pGlobalUserData->lUserScore);
		money_->setString(str);
	}

	if (silver_){
		DIGIT_SCALE(silver_, pGlobalUserData->lUserIngot);
		silver_->setString(StringUtils::format("%lld", pGlobalUserData->lUserIngot));
	}

	//< 更新头像
	img_head->loadTexture(YRComGetHeadImageById(pGlobalUserData->wFaceID));
	//< 更新名字
	m_txt_name->setString(pGlobalUserData->szNickName);

}

void TopHeadLayer::btnClickEvent(float dt)
{
	Node *panel_top = m_root_widget->getChildByName("img_Panel_headInfo0");

	ui::Button *btn_back = dynamic_cast<ui::Button*>(panel_top->getChildByName("btn_back"));
	btn_back->setTouchEnabled(true);
}

void TopHeadLayer::flushCoin(float dt)
{
	m_bank_mission.query();
}

