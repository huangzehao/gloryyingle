#ifndef _ModeLayer_H_
#define _ModeLayer_H_

// #include "cocos2d.h"
// #include "cocos-ext.h"

#include <ExtensionMacros.h>

#include "Platform/PFView/BaseLayer.h"
#include <GUI/CCControlExtension/CCControlButton.h>
#include "Platform/PFKernel/CGPLoginMission.h"
USING_NS_CC;
USING_NS_CC_EXT;
using namespace cocos2d::ui;

#include <ui/UIImageView.h>
#include "Platform/PFDefine/data/ServerListData.h"
#include "Platform/PFView/ModeScene/TopHeadLayer.h"

class ModeLayer
	: public BaseModeLayer
	, public IServerListDataSink
{
public:
	CREATE_FUNC(ModeLayer);

private:
	ModeLayer();
	~ModeLayer();
	bool init();
	bool newInit();

private:
	void onModeClick(cocos2d::Ref* pSender,  ControlButton::EventType e);

	bool initGameList();

	//判断显示游戏
	bool isVisibleGameKind(uint16 wKindID);

	cocos2d::ui::ImageView *m_cur_gametype_cell;
	cocos2d::Sprite		   *mMsgBackground;

	Layout * panel_oneList;
	Node *m_page_vew;
	Node * m_pUIlayout;
	//////////////////////////////////////////////////////////////////////////
	// IServerListDataSink
public:
	//完成通知
	virtual void OnGameItemFinish();
	//完成通知
	virtual void OnGameKindFinish(uint16 wKindID);
	//更新通知
	virtual void OnGameItemUpdateFinish();
	//系统消息接收完成
	virtual void OnSystemMessageFinish();

	//更新通知
public:
	//插入通知
	virtual void OnGameItemInsert(CGameListItem * pGameListItem);
	//更新通知
	virtual void OnGameItemUpdate(CGameListItem * pGameListItem);
	//删除通知
	virtual void OnGameItemDelete(CGameListItem * pGameListItem);

public:
	bool setDatas();
	///< 新的设置数据
	bool newSetDatas();

	virtual void onEnterTransitionDidFinish();

	void moveEndCall();

	void  sendSystemMessage(float dt);

	virtual void MessageUpdate(float delta);

private:
	CGPLoginMission	mLoginMission;
	ui::Layout * m_BgRoll;
	ui::Layout * mBgLayout;
	Label * mLabMessage;
	bool isMove;
	int m_time;
	bool isFirst;
	///< 消息队列
	std::queue<std::string> mMessageQueue;
	//时间频率容器
	std::vector<int>		m_TimeRate;		
	std::vector<tagGameKind *> tagGameKindVertor;
	
};

#endif // _ModeLayer_H_