#include "SignInLayer.h"
#include "time.h"
#include "Tools/Dialog/NewDialog.h"
#include "Tools/Manager/SoundManager.h"
#include "Tools/tools/StringData.h"

#include "Tools/tools/StaticData.h"
using namespace ui;

USING_NS_CC;

SignInLayer::SignInLayer()
: mSignInMission(ADDRESS_URL, SERVER_PORT)
{
	mSignInMission.setMissionSink(this);
	mTodayChecked = true;
}


SignInLayer::~SignInLayer()
{
	mSignInMission.setMissionSink(0);
}

bool SignInLayer::init()
{
	if (!BaseLayer::initWithJsonFile("LobbyResources/SignInfoLayer.json"))
	{
		return false;
	}

	this->setZOrder(1);
	initComponent();

	return true;
}

void SignInLayer::updateDayDate(std::vector<SignInInfoStruct> tDayList)
{
	for (int i = 0; i < tDayList.size(); i++)
	{
		SignInInfoStruct it = tDayList[i];
		mDayList[i] = it;
		addListData(it, i + 100);
	}
}

void SignInLayer::updateDayDate()
{
	int i = 0;
	for (auto it : mDayList)
	{
		addListData(it.second, i + 100);
		i++;
	}
}

void SignInLayer::initComponent()
{
	Panel_bg = dynamic_cast<Layout *>(m_root_widget->getParent()->getChildByName("Panel_bg"));

	img_bg = dynamic_cast<ui::ImageView *>(m_root_widget->getChildByName("img_bg"));

	img_signBg = dynamic_cast<ui::ImageView *>(img_bg->getChildByName("img_signBg"));

	Button * btn_close = dynamic_cast<Button *>(img_bg->getChildByName("btn_close"));
	CCAssert(btn_close, "");
	btn_close->addTouchEventListener(this, SEL_TouchEvent(&SignInLayer::closeButtonTouch));

	Button * btn_signin = dynamic_cast<Button *>(img_bg->getChildByName("btn_confrim"));
	CCAssert(btn_signin, "");
	btn_signin->addTouchEventListener(this, SEL_TouchEvent(&SignInLayer::signInButtonTouch));

	//��������
	m_root_widget->setPositionY(850);

	m_root_widget->runAction(CCSequence::create(
		MoveTo::create(0.25f, Vec2(0, 0)),
		MoveTo::create(0.1f, Vec2(0, 100)),
		MoveTo::create(0.05f, Vec2(0, 0)),
		MoveTo::create(0.04f, Vec2(0, 30)),
		MoveTo::create(0.03f, Vec2(0, 0)),
		0));
}

void SignInLayer::signInButtonTouch(cocos2d::Ref * ref, ui::TouchEventType eventType_)
{
	if (eventType_ == TouchEventType::TOUCH_EVENT_BEGAN)
	{
		PLAY_BUTTON_CLICK_EFFECT
	}
	if (eventType_ == TouchEventType::TOUCH_EVENT_ENDED)
	{
		mSignInMission.done();
	}
}

void SignInLayer::closeButtonTouch(cocos2d::Ref * ref, ui::TouchEventType eventType_)
{
	if (eventType_ == TouchEventType::TOUCH_EVENT_BEGAN)
	{
		PLAY_BUTTON_CLOSE_EFFECT
	}
	if (eventType_ == TouchEventType::TOUCH_EVENT_ENDED)
	{
		Panel_bg->setVisible(false);
		m_root_widget->runAction(CCSequence::create(MoveTo::create(0.2f, Vec2(0, 850)), CCCallFunc::create(this, callfunc_selector(SignInLayer::closeMoveTo)), NULL));
	}
}
void SignInLayer::particleButtonTouch()
{
	Size visibleSize = Director::getInstance()->getVisibleSize();
	auto * particleSystem = ParticleSystemQuad::create("particle/bigwin_blowout_1.plist");
	particleSystem->setPosition(visibleSize.width / 2, visibleSize.height / 5 * 2);
	this->addChild(particleSystem);
}

void SignInLayer::addListData(SignInInfoStruct signInfo, int tag)
{
	Layout * mPanel = (Layout *)img_signBg->getChildByTag(tag);
	
	ImageView * img_nowday = dynamic_cast<ImageView *>(mPanel->getChildByName("img_sign"));
	if (signInfo.isDay)
		img_nowday->setVisible(true);
	else
		img_nowday->setVisible(false);

	TextAtlas * al_gold_num = dynamic_cast<TextAtlas *>(mPanel->getChildByName("al_nums"));
	al_gold_num->setString(StringUtils::format("%d",signInfo.gold));
}

void SignInLayer::onSignInQueryInfoResult(word wSeriesDate, bool bTodayChecked, SCORE lRewardGold[7])
{
	now_day = wSeriesDate > 7 ? 7 : wSeriesDate;

	std::vector<SignInInfoStruct> test_Vector;

	for (int i = 0, j = 0; i < 7; i++)
	{
		SignInInfoStruct info(i + 1, lRewardGold[i], false);
		if (j < wSeriesDate) info.isDay = true;
		test_Vector.push_back(info);
		j++;
	}
	this->updateDayDate(test_Vector);
	mTodayChecked = bTodayChecked;

	return;
}

void SignInLayer::onSignInDoneResult(bool bSuccessed, SCORE lScore, const char* szDescription)
{
	if (bSuccessed)
	{
		particleButtonTouch();
		now_day = now_day > 7 ? 7 : now_day+1;
		mDayList[now_day-1].isDay = true;
		
		///< ????
		updateDayDate();
		//log("info %s", szDescription);
	}
	else
	{
		NewDialog::create(SSTRING("Sign_In_ago"), NewDialog::NONEBUTTON);
	}
	return;
}

void SignInLayer::onEnterTransitionDidFinish()
{
	BaseLayer::onEnterTransitionDidFinish();
	mSignInMission.query();
}

void SignInLayer::closeMoveTo()
{
	this->removeFromParent();
}

