#ifndef SETTINGLAYER_H_
#define SETTINGLAYER_H_

#include "cocos2d.h"
#include "Platform/PFView/BaseLayer.h"
#include "ui/CocosGUI.h"

USING_NS_CC;
using namespace ui;
using namespace std;
using namespace cocostudio;

class SettingLayer : public BaseLayer
{
public:
	SettingLayer();
	~SettingLayer();
	bool init();
	void initCompoent();
	CREATE_FUNC(SettingLayer);
	virtual void onExitTransitionDidStart();
private:
	///< 改变音乐播发状态!
	void changeMusicState(bool isplay);
	///< 读取音效播放状态
	void changeEffectState(bool isplay);
	///< 读取音乐播发状态!
	void readMusicState(bool isplay);
	///< 读取音效播放状态
	void readEffectState(bool isplay);
	///<关闭移动回调
	void closeMoveTo();
// 	///< 改变震动的状态
// 	void changeShakeState(bool isshake);
// 	///< 改变特殊效果状态
// 	void changeSpecialEffectState(bool isshow);
// 	///< 改变聊天状态
// 	void changeChatState(bool ischat);

protected:
	Button  * btn_music;
	Button  * btn_sound;
	Button  * btn_switch;
	ImageView * img_headPanel;
	ImageView * img_head;
	Text * lab_name;
};

#endif

