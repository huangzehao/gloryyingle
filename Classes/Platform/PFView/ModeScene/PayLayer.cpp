#include "PayLayer.h"
#include "Tools/Manager/SoundManager.h"
#include "Tools/Dialog/NewDialog.h"
#include "Tools/tools/StringData.h"
#include "Tools/tools/gPlatform.h"
#include "Tools/tools/MTNotification.h"
#include "Platform/PFDefine/data/GlobalUserInfo.h"

USING_NS_CC;
using namespace ui;
extern "C"
{

#if CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID

	JNIEXPORT void JNICALL Java_org_cocos2dx_cpp_PayHelper_payJFTResult(JNIEnv* env, jclass method, jint param)
	{
		cocos2d::log("JinCallBack :%d", param);
		int payStatus = param;
		G_NOTIFY_D("JFT_PAY_RESULT", &payStatus);
	}

#endif
}
CCScene* PayLayer::scene()
{
	CCScene* scene = CCScene::create();

	if (!scene)
		return 0;

	PayLayer* layer = PayLayer::create();
	if (layer)
		scene->addChild(layer);
	return scene;
}

//////////////////////////////////////////////////////////////////////////
PayLayer::PayLayer()
{

}

PayLayer::~PayLayer()
{

}

//初始化方法
bool PayLayer::init()
{
	do
	{
		CC_BREAK_IF(!BaseLayer::initWithJsonFile("LobbyResources/PayLayer.json"));
		CC_BREAK_IF(!this->setUpdateView());

// #if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
// 		ShellApiJni::regCallback(this);
// #endif

		return true;
	} while (0);

	return false;
}

// 初始化组件
bool PayLayer::setUpdateView()
{
	do
	{
		Panel_bg = dynamic_cast<Layout *>(m_root_widget->getParent()->getChildByName("Panel_bg"));

		img_bg = dynamic_cast<ui::ImageView *>(m_root_widget->getChildByName("img_bg"));

		btn_close = dynamic_cast<ui::Button *>(img_bg->getChildByName("btn_close"));
		CCAssert(btn_close, "");
		btn_close->addTouchEventListener(this, SEL_TouchEvent(&PayLayer::closeButtonTouch));

		btn_zfb = dynamic_cast<ui::Button *>(img_bg->getChildByName("btn_zfb"));
		btn_wx = dynamic_cast<ui::Button *>(img_bg->getChildByName("btn_wx"));

		//btn_wx->setEnabled(true);
		//btn_wx->setBright(false);
		btn_wx->setVisible(false);
		btn_zfb->setVisible(false);

		payType = 1;

		btn_wx->addTouchEventListener([=](Ref* ref_, ui::Widget::TouchEventType event_type_)
		{
			if (event_type_ == ui::Widget::TouchEventType::BEGAN)
			{
				PLAY_BUTTON_CLICK_EFFECT
			}
			if (event_type_ == ui::Widget::TouchEventType::ENDED)
			{
				btn_wx->setTouchEnabled(false);
				btn_wx->setBright(false);
				btn_zfb->setTouchEnabled(true);
				btn_zfb->setBright(true);

				payType = 1;
			}
		});

		btn_zfb->addTouchEventListener([=](Ref* ref_, ui::Widget::TouchEventType event_type_)
		{
			if (event_type_ == ui::Widget::TouchEventType::BEGAN)
			{
				PLAY_BUTTON_CLICK_EFFECT
			}
			if (event_type_ == ui::Widget::TouchEventType::ENDED)
			{
				btn_zfb->setTouchEnabled(false);
				btn_zfb->setBright(false);
				btn_wx->setTouchEnabled(true);
				btn_wx->setBright(true);

				payType = 2;
			}
		});

		btn_buy_1 = dynamic_cast<ui::Button *>(img_bg->getChildByName("btn_buy_1"));
		btn_buy_1->setTag(1010);
		btn_buy_1->addTouchEventListener(this, SEL_TouchEvent(&PayLayer::doPayButtonTouch));

		btn_buy_2 = dynamic_cast<ui::Button *>(img_bg->getChildByName("btn_buy_2"));
		btn_buy_2->setTag(1020);
		btn_buy_2->addTouchEventListener(this, SEL_TouchEvent(&PayLayer::doPayButtonTouch));

		btn_buy_3 = dynamic_cast<ui::Button *>(img_bg->getChildByName("btn_buy_3"));
		btn_buy_3->setTag(1030);
		btn_buy_3->addTouchEventListener(this, SEL_TouchEvent(&PayLayer::doPayButtonTouch));

		btn_buy_4 = dynamic_cast<ui::Button *>(img_bg->getChildByName("btn_buy_4"));
		btn_buy_4->setTag(1050);
		btn_buy_4->addTouchEventListener(this, SEL_TouchEvent(&PayLayer::doPayButtonTouch));

		btn_buy_5 = dynamic_cast<ui::Button *>(img_bg->getChildByName("btn_buy_5"));
		btn_buy_5->setTag(1100);
		btn_buy_5->addTouchEventListener(this, SEL_TouchEvent(&PayLayer::doPayButtonTouch));

		btn_buy_6 = dynamic_cast<ui::Button *>(img_bg->getChildByName("btn_buy_6"));
		btn_buy_6->setTag(1200);
		btn_buy_6->addTouchEventListener(this, SEL_TouchEvent(&PayLayer::doPayButtonTouch));

		btn_buy_7 = dynamic_cast<ui::Button *>(img_bg->getChildByName("btn_buy_7"));
		btn_buy_7->setTag(1500);
		btn_buy_7->addTouchEventListener(this, SEL_TouchEvent(&PayLayer::doPayButtonTouch));

		//界面下移
		m_root_widget->setPositionY(850);

		m_root_widget->runAction(CCSequence::create(
			MoveTo::create(0.25f, Vec2(0, 0)),
			MoveTo::create(0.1f, Vec2(0, 100)),
			MoveTo::create(0.05f, Vec2(0, 0)),
			MoveTo::create(0.04f, Vec2(0, 30)),
			MoveTo::create(0.03f, Vec2(0, 0)),
			0));

		return true;
	} while (0);

	return false;
}

#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
// void PayLayer::call(int code, std::string message) {
// 	LOGD("=====> call : %d, %s", code, message.c_str());
// 	std::ostringstream oss;
// 	oss << "return value: " << code;
// }
#endif

void PayLayer::doPayButtonTouch(cocos2d::Ref * ref, ui::TouchEventType eventType_)
{
	if (eventType_ == TouchEventType::TOUCH_EVENT_BEGAN)
	{
		PLAY_BUTTON_CLICK_EFFECT
	}
	if (eventType_ == TouchEventType::TOUCH_EVENT_ENDED)
	{
		CGlobalUserInfo * pGlobalUserInfo = CGlobalUserInfo::GetInstance();
		tagGlobalUserData * pGlobalUserData = pGlobalUserInfo->GetGlobalUserData();
		//pGlobalUserData->dwUserMedal += 
		Node* node = dynamic_cast<Node*>(ref);
		int index = node->getTag() - 1000;
		payRMB_ = node->getTag() - 1000;
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
// 		ShellApiJni::PayOrder payOrder;
// 		payOrder.M_appId_ = "14081714462317168447";
// 		payOrder.M_amount_ = StringUtils::format("%d", index);
// 		payOrder.M_goodsName_ = StringUtils::format("购买%d元游戏币", index);
// 		payOrder.M_payId_ = ShellApiJni::srand();
// 		payOrder.M_playerId_ = StringUtils::format("%d", pGlobalUserData->dwUserID);
// 		// payOrder.M_channelId_ = "default";
// 		ShellApiJni::pay(payOrder);
#endif
#if(CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
//		std::string mountStr = StringUtils::format("%d", index);
//		std::string userIdStr = StringUtils::format("%d", pGlobalUserData->dwUserID);
//		SDKToolKit::payAction(mountStr, userIdStr);
#endif
		//NewDialog::create(SSTRING("pay_gold"), NewDialog::NONEBUTTON);
		std::string useridStr = cocos2d::StringUtils::toString(CGlobalUserInfo::GetInstance()->GetGlobalUserData()->dwUserID);
		platformOpenJunFuTongWeChetPay(useridStr.c_str(), payRMB_);
	}
}

void PayLayer::closeButtonTouch(cocos2d::Ref * ref, ui::TouchEventType eventType_)
{
	if (eventType_ == TouchEventType::TOUCH_EVENT_BEGAN)
	{
		PLAY_BUTTON_CLOSE_EFFECT
	}
	if (eventType_ == TouchEventType::TOUCH_EVENT_ENDED)
	{
		Panel_bg->setVisible(false);
		m_root_widget->runAction(CCSequence::create(MoveTo::create(0.2f, Vec2(0, 850)), CCCallFunc::create(this, callfunc_selector(PayLayer::closeMoveTo)), NULL));
	}
}

void PayLayer::closeMoveTo()
{
	this->removeFromParent();
}


