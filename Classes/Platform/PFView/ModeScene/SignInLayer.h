#ifndef _SIGNINLAYER_H_
#define _SIGNINLAYER_H_

#include "cocos2d.h"
#include "Platform/PFView/BaseLayer.h"
#include "ui/CocosGUI.h"
#include "Platform/PFKernel/CGPSignInMission.h"
#include "Platform/PlatformHeader.h"
//USING_NS_CC;

class SignInInfoStruct
{
public:
	int day;
	int gold;
	bool isDay;
	SignInInfoStruct(int t_day = 1, int t_gold = 1, bool t_isDay = false)
	{
		day = t_day;
		gold = t_gold;
		isDay = t_isDay;
	}
};
class SignInLayer : public BaseLayer, public IGPSignInMissionSink
{
public:
	SignInLayer();
	~SignInLayer();
	CREATE_FUNC(SignInLayer);
	bool init();
	
	void updateDayDate(std::vector<SignInInfoStruct> tDayList);
	void updateDayDate();
	///< 界面加载完成
	virtual void onEnterTransitionDidFinish();
protected:
	std::map<int , SignInInfoStruct> mDayList;
	cocos2d::ui::Layout * Panel_bg;
	ui::ImageView * img_bg;
	ui::ImageView * img_signBg;

	bool mTodayChecked;
	///< 今天日期
	int now_day;
private:
	///< 初始化组件
	void initComponent();
	///< 签到回调
	void signInButtonTouch(cocos2d::Ref * ref, ui::TouchEventType eventType_);
	///< 关闭回调
	void closeButtonTouch(cocos2d::Ref * ref, ui::TouchEventType eventType_);
	///< 签到粒子效果
	void particleButtonTouch();
	///< 增加List中的数据
	void addListData(SignInInfoStruct signInfo, int tag);
	///<关闭移动回调
	void closeMoveTo();

public:
	virtual void onSignInQueryInfoResult(word wSeriesDate, bool bTodayChecked, SCORE lRewardGold[7]);
	virtual void onSignInDoneResult(bool bSuccessed, SCORE lScore, const char* szDescription);

	CGPSignInMission	mSignInMission;

};

#endif


