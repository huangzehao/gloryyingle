#ifndef _PlatformBankDredge_H_
#define _PlatformBankDredge_H_

#include "cocos2d.h"
#include "Platform/PFView/BaseLayer.h"
#include "ui/CocosGUI.h"
#include "Platform/PFView/ModeScene/TopHeadLayer.h"
#include "Platform/PFKernel/CGPInsureMission.h"
USING_NS_CC;

class PlatformBankDredge
	: public BaseModeLayer, IGPInsureMissionSink
{
public:
	//创建场景
	static cocos2d::CCScene* scene();
	//创建方法
	CREATE_FUNC(PlatformBankDredge);

public:
	//构造函数
	PlatformBankDredge();
	~PlatformBankDredge();
	//初始化方法
	virtual bool init();

public:
	void onEnterTransitionDidFinish();
private:
	//初始化页面的基本纹理
	bool setUpdateView();
	///< 关闭回调
	void closeButtonTouch(cocos2d::Ref * ref, ui::TouchEventType eventType_);
	///< 开通银行
	void dredgeButtonTouch(cocos2d::Ref * ref, ui::TouchEventType eventType_);
	///<关闭移动回调
	void closeMoveTo();
	// IGPInsureMissionSink
public:
	virtual void onInsureInfo();
	virtual void onInsureSuccess(int type, const char* szDescription);
	virtual void onInsureFailure(int type, const char* szDescription);
	virtual bool onInsureTransferConfirm(const char* szMessage);
	//    开通
	virtual void onInsureEnableResult(int type, const char* szMessage);
private:

	ui::Button	  *	btn_Dredge;
	ui::TextField * tf_bankPW;
	ui::TextField * tf_agBankPw;
	ui::TextField * tf_accPw;
	ui::Button	  *	btn_close;

	CGPInsureMission	mInsureMission;

};
#endif // _PlatformBankDredge_H_