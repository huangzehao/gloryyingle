#ifndef _ConversionLayer_H_
#define _ConversionLayer_H_

#include "cocos2d.h"
#include "Platform/PFView/BaseLayer.h"
#include "Platform/PFKernel/CGPConversionMission.h"
#include "ui/CocosGUI.h"

USING_NS_CC;

class ConversionLayer
	: public BaseLayer
	, public IGPConversionMissionSink
{
public:
	//创建场景
	static cocos2d::CCScene* scene();
	//创建方法
	CREATE_FUNC(ConversionLayer);

public:
	//构造函数
	ConversionLayer();
	~ConversionLayer();
	//初始化方法
	virtual bool init();

	///< 界面加载完成
	virtual void onEnterTransitionDidFinish();

	virtual void onConversionQueryInfoResult(word wExchangeRate);

	virtual void onConversionResult(bool bSuccessed, SCORE lCurrIngot, const char* szDescription);

	CGPConversionMission mConversionMission;
private:
	//初始化页面的基本纹理
	bool setUpdateView();
	///< 关闭回调
	void closeButtonTouch(cocos2d::Ref * ref, ui::TouchEventType eventType_);
	///< 确认兑换
	void confrimButtonTouch(cocos2d::Ref * ref, ui::TouchEventType eventType_);
	///< 全部按钮
	void allButtonTouch(cocos2d::Ref * ref, ui::TouchEventType eventType_);
	///<关闭移动回调
	void closeMoveTo();
	void enterCallBack(cocos2d::Ref* txt_, ui::TextFiledEventType event_type_);
private:
	ui::Layout * Panel_bg;
	ui::ImageView * img_bg;
	ui::Button * btn_close;
	ui::Button * btn_confrim;
	ui::Button * btn_all;
	ui::TextAtlas * al_yuanbao;
	ui::TextAtlas * al_youxibi;
	ui::TextField * tf_duihuan;
	ui::Text * lab_youxibi;
	word m_ExchangeRate;
};
#endif // _ConversionLayer_H_