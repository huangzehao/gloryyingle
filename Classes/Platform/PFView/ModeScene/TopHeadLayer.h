
#ifndef TOP_HEAD_LAYER__H__
#define TOP_HEAD_LAYER__H__

#include "Platform/PFView/BaseLayer.h"
#include "ui/CocosGUI.h"
#include "Platform/PlatformHeader.h"
#include "Platform/PFKernel/CGPPersonalInfoMission.h"
#include "Platform/PFKernel/CGPInsureMission.h"
#include "Platform/PFKernel/CGPIndividualMission.h"

class TopHeadLayer : public BaseLayer
	, public IGPInsureMissionSink
	, public IGPPersonalInfoMissionSink
	, public IGPIndividualMissionSink
{
public:
	TopHeadLayer();
	virtual ~TopHeadLayer();

	bool init();
	bool newInit();

	void onEnter() override;
	void onExit() override;

	void setModeLayerType(BaseModeLayer::ModeLayerType m_typ) { m_type = m_typ; }

	CREATE_FUNC(TopHeadLayer);

	void onEnterTransitionDidFinish();
	void update(float delta);
	void flushCoin(float dt);
	virtual void onQueryLevelInfoSuccess(SCORE, SCORE, tchar[]);
	virtual void onQueryLevelUpTipsSuccess(int cur_lv_, int cur_exp_, dword next_exp_, dword reward_gold_, SCORE reward_ingot_);

	void onGPIndividualInfo(int type) override;
	///< ��ť����¼�
	void btnClickEvent(float dt);
	CGPInsureMission m_bank_mission;
	CGPPersonalInfoMission m_personal_mission;
	CGPIndividualMission m_individual_mission;
private:
	ui::Text *m_txt_name;
	ui::TextAtlas *al_level;
	ui::TextAtlas *money_;
	ui::TextAtlas *silver_;
	ui::ImageView * img_head;
	ui::LoadingBar * levelBar;
	BaseModeLayer::ModeLayerType m_type;


};

#endif  //  TOP_HEAD_LAYER__H__