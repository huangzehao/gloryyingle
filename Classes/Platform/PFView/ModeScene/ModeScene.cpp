#include "ModeLayer.h"
#include "UserInfoLayer.h"
#include "GiveChoiceLayer.h"
#include "ModeScene.h"
#include "Tools/tools/gPlatform.h"
#include "Tools/tools/StringData.h"

#include "Platform/PFView/ServerListScene/ServerListLayer.h"
#include "Platform/PFView/ServerScene/ServerScene.h"

#include <2d/CCActionInstant.h>
#include "PlatformBankDredge.h"
#include "Tools/tools/SimpleTools.h"
#include "Tools/Dialog/NewDialog.h"
#include "PlatformBelowBtn.h"
#include "Tools/tools/MTNotification.h"

USING_NS_CC;

#define RIGHT_POSITION_X   (Director::getInstance()->getWinSize().width )
#define RIGHT_POSITION_Y   0
//(Director::getInstance()->getWinSize().width/2 )
#define LEFT_POSITION_X   -(Director::getInstance()->getWinSize().width )
#define CREATEFINISH_GOTO  "createFinish_Goto"

//////////////////////////////////////////////////////////////////////////
extern char szNickNameTmp[LEN_NICKNAME];//临时名字,用其他平台改名

//////////////////////////////////////////////////////////////////////////

ModeScene * ModeScene::_Platform_Scene = nullptr;

ModeScene::ModeScene()
	//
	: m_need_remove_after_move(false), mRoomId(-1)
{
	//注册事件
	G_NOTIFY_REG(CREATEFINISH_GOTO, ModeScene::createFinishGoto);
}

ModeScene::~ModeScene()
{
	G_NOTIFY_UNREG(CREATEFINISH_GOTO);
	//mIndividualMission.setMissionSink(0);
	if (m_cur_layer){
		//m_cur_layer->release();
		m_cur_layer = 0;
	}

	if (_Platform_Scene != nullptr)
	{
		_Platform_Scene->release();
	}

	//G_NOTIFY_UNREG("GAME_BACKGROUND_REBACK");
}

ModeScene *  ModeScene::create()
{
	if (_Platform_Scene == nullptr)
	{
		_Platform_Scene = new ModeScene();
		if (_Platform_Scene && _Platform_Scene->init())
		{
			_Platform_Scene->autorelease();
			_Platform_Scene->retain();
			return _Platform_Scene;
		}
		delete _Platform_Scene;
		return nullptr;
	}
	else
	{
		return _Platform_Scene;
	}

}
//初始化方法
bool ModeScene::init()
{
	do 
	{
		CC_BREAK_IF(!Scene::init());

		//1.最底层是背景图
		Sprite * background = Sprite::create("LobbyResources/LobbyCommon/layer_bg.png");
		background->setScale(1.065);
		background->setPosition(Vec2(Director::getInstance()->getWinSize().width / 2, Director::getInstance()->getWinSize().height / 2));
		this->addChild(background);

		//2.第三层是底部导航栏
		platformBtn_layer_ = PlatformBelowBtn::create();
		this->addChild(platformBtn_layer_);

		//3.第四层是中部游戏导航栏
		ModeLayer* mode = ModeLayer::create();
		m_cur_layer = mode;
		m_type = BaseModeLayer::ModeLayerTypeDefault;
		this->addChild(mode);

		//4.第二层是顶部导航栏
		top_layer_ = TopHeadLayer::create();
		top_layer_->setModeLayerType(BaseModeLayer::ModeLayerType::ModeLayerTypeDefault);
		this->addChild(top_layer_);


		//物理按键返回
		auto listener = EventListenerKeyboard::create();
		listener->onKeyReleased = [](EventKeyboard::KeyCode code, Event *e){
			log("key code : %d", code);

			switch (code) {
			case EventKeyboard::KeyCode::KEY_ESCAPE:
				///< 确认退出平台
				NewDialog::createOnce(SSTRING("affirm_exit_now_account"), NewDialog::AFFIRMANDCANCEL, [=]()
				{
					Director::getInstance()->end();
				}
				);

				break;
			default:
				break;
			}
		};

		Director::getInstance()->getEventDispatcher()->addEventListenerWithSceneGraphPriority(listener, this);

		return true;
	} while (0);

	return false;
}

bool ModeScene::changeModeLayer(BaseModeLayer::ModeLayerType type, CGameServerItem * mitem)
{
	if (m_type == type){
		return true;
	}
	m_type = type;

 	layer = getLayerByType(type);
// 	if (layer && type == BaseModeLayer::ModeLayerTypeRoomInfo)
// 	{
// 		layer->removeFromParent();
// 		layer = nullptr;
// 	}
// 
// 	if (layer && type == BaseModeLayer::ModeLayerTypeRoomListGo)
// 	{
// 		layer->removeFromParent();
// 		layer = nullptr;
// 	}
	bool direct_right = m_type != BaseModeLayer::ModeLayerTypeDefault;  // false

	if (!layer) {
	//	direct_right = true;

		switch (type)
		{
		case BaseModeLayer::ModeLayerTypeBank:
			layer = GiveChoiceLayer::create();
			break;
		case BaseModeLayer::ModeLayerTypeDefault:
			layer = ModeLayer::create();
			break;
		case  BaseModeLayer::ModeLayerTypeSetting:
			layer = 0;
			break;
		case BaseModeLayer::ModeLayerTypeRoomList:
			layer = ServerListLayer::create(1);
			break;
 		case BaseModeLayer::ModeLayerTypeRoomListBack:
 			layer = ServerListLayer::create(1);
 			break;
		case  BaseModeLayer::ModeLayerTypeInfo:
			layer = UserInfoLayer::create();
			break;
		case BaseModeLayer::ModeLayerTypeBankOpen:
			layer = PlatformBankDredge::create();
			break;
		case BaseModeLayer::ModeLayerTypeRoomInfo:
			layer = ServerScene::create(mitem);
			break;
		default:
			layer = 0;
			break;
		}
	}

	CCASSERT( layer, " layer must not null ");

	Vec2 orgin_pos_(RIGHT_POSITION_X, RIGHT_POSITION_Y);
	Vec2 tar_pos_(LEFT_POSITION_X , RIGHT_POSITION_Y);

	if (!direct_right || m_type == BaseModeLayer::ModeLayerTypeRoomListBack){
		orgin_pos_ = tar_pos_;
		tar_pos_ = Vec2(RIGHT_POSITION_X, RIGHT_POSITION_Y);
	}
	layer->setPosition(orgin_pos_);

 	if (type != BaseModeLayer::ModeLayerTypeRoomInfo)
 	{
		layer->runAction(MoveTo::create(0.5f, Vec2(0, 0)));
		m_cur_layer->runAction(Sequence::create(MoveTo::create(0.5f, tar_pos_), CallFunc::create([=](){
			if (m_need_remove_after_move){
				m_cur_layer->removeFromParent();
				m_need_remove_after_move = false;
			}
			m_cur_layer = layer;
		}), 0));
	}
	else
	{
		layer->runAction(CCSequence::create(DelayTime::create(0.5f), MoveTo::create(0.5f, Vec2(0, 0)), NULL));
		m_cur_layer->runAction(Sequence::create(DelayTime::create(0.5f), MoveTo::create(0.5f, tar_pos_), CallFunc::create([=](){
			if (m_need_remove_after_move){
				m_cur_layer->removeFromParent();
				m_need_remove_after_move = false;
			}
			m_cur_layer = layer;
		}), 0));
	}

	addChild(layer);

	return true;
}

void ModeScene::createFinishGoto(Ref* obj)
{
	bool direct_right = m_type != BaseModeLayer::ModeLayerTypeDefault;

	Vec2 orgin_pos_(RIGHT_POSITION_X, RIGHT_POSITION_Y);
	Vec2 tar_pos_(LEFT_POSITION_X, RIGHT_POSITION_Y);

	layer->runAction(MoveTo::create(0.5f, Vec2(0, 0)));
	m_cur_layer->runAction(Sequence::create(MoveTo::create(0.5f, tar_pos_), CallFunc::create([=](){
		if (m_need_remove_after_move){
			m_cur_layer->removeFromParent();
			m_need_remove_after_move = false;
		}
		m_cur_layer = layer;
	}), 0));

	addChild(layer);
}


void ModeScene::addChild(Node* child_)
{
	//  没有父节点的时候才添加进去
	if (!child_->getParent()){
		Scene::addChild(child_);
	}
}

BaseModeLayer* ModeScene::getLayerByType(BaseModeLayer::ModeLayerType type)
{
	for (auto child_ : getChildren())
	{
		BaseModeLayer *layer_ = dynamic_cast<BaseModeLayer*>(child_);
		if (layer_){

			if (layer_->isModeLayerType(type)){
				return layer_;
			}
		}
	}
	return 0;
}

bool ModeScene::removeLayer(BaseModeLayer::ModeLayerType type)
{
	if (isLayer(type)){
		m_need_remove_after_move = true;
		return true;
	}
	
	return false;
}

void ModeScene::onEnterTransitionDidFinish()
{
	Scene::onEnterTransitionDidFinish();

	func_background_reback(this);


	//其他平台,改命
// 	if (platformGetPlatform() != GAME_PLATFORM_DEFAULT)
// 	{
// 		////变量定义
// 		CGlobalUserInfo * pGlobalUserInfo=CGlobalUserInfo::GetInstance();
// 		tagGlobalUserData * pGlobalUserData=pGlobalUserInfo->GetGlobalUserData();
// 
// 		if (utf8_cmp(szNickNameTmp, pGlobalUserData->szNickName) != 0)
// 		{
// 			tagModifyIndividual individual;
// 			zeromemory(&individual, sizeof(individual));
// 			individual.cbGender = pGlobalUserData->cbGender;
// 			sprintf(individual.szNickName, "%s", szNickNameTmp);
// 
// 			mIndividualMission.modify(individual);
// 		}
// 	}
}

void ModeScene::closeCallback(cocos2d::Node *pNode)
{
	switch (pNode->getTag())
	{
	case DLG_MB_OK:
		{
	//		G_NOTIFY("LOADING_COMPLETE", 0);
		}
		break;
	}
}


void ModeScene::setSelectRoomId(int roomId)
{
	mRoomId = roomId;
}

int ModeScene::getSelectRoomId()
{
	return mRoomId;
}

void ModeScene::func_background_reback(cocos2d::Ref* obj)
{
	if (SimpleTools::isEnterGame)
	{
		///< 如果进入过游戏
		Layer * dialog = NewDialog::createNoLoadScene(SSTRING("Exception_exit"), NewDialog::AFFIRM, false);
		Scene::addChild(dialog, 0xFFFFF);
		SimpleTools::isEnterGame = false;
	}
}

//////////////////////////////////////////////////////////////////////////
// IGPLoginMissionSink
// void ModeScene::onGPIndividualInfo(int type)
//{
//	G_NOTIFY("USER_INFO", 0);
//}
//
//void ModeScene::onGPIndividualSuccess(int type, const char* szDescription)
//{
//	G_NOTIFY("USER_INFO", 0);
//}