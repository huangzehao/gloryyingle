#ifndef _LOWPROTECTLAYER_H_
#define _LOWPROTECTLAYER_H_

#include "cocos2d.h"
#include "Platform/PFView/BaseLayer.h"
#include "ui/CocosGUI.h"
#include "Platform/PFKernel/CGPLowProtectMission.h"
#include "Platform/PlatformHeader.h"

class LowProtectLayer : public BaseLayer, IGPLowProtectMissionLink
{
public:
	LowProtectLayer();
	~LowProtectLayer();
	bool init();
	void initCompoent();
	CREATE_FUNC(LowProtectLayer);

	///< 界面加载完成
	virtual void onEnterTransitionDidFinish();

	///< 回调接口
	// 低保信息
	virtual void onSignInQueryInfoResult(SCORE lScoreCondition, SCORE lScoreAmount, byte cbTakeTimes);
	// 低保按钮确认
	virtual void onSignInDoneResult(bool bSuccessed, SCORE lGameScore, const char* szDescription);
private:
	void closeButtonTouch(cocos2d::Ref * ref, ui::TouchEventType eventType_);
	void particleButtonTouch(cocos2d::Ref * ref, ui::TouchEventType eventType_);
	///<关闭移动回调
	void closeMoveTo();
	ui::ImageView * img_bg;
	cocos2d::ui::Layout * Panel_bg;
	cocos2d::ui::TextAtlas * al_gamenum;
	cocos2d::ui::TextAtlas * al_step;
	cocos2d::ui::TextAtlas * al_require_game_num;
	// 剩余次数
	int degree;
	///< 回调接口
	CGPLowProtectMission mLowProtectMission;
};

#endif

