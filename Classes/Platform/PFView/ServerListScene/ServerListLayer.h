#ifndef _ServerListLayer_H_
#define _ServerListLayer_H_

#include "cocos2d.h"
#include "cocos-ext.h"
#include "Platform/PFView/BaseLayer.h"
#include "Platform/PFView/ModeScene/TopHeadLayer.h"
class BaseModeLayer;

using namespace cocos2d::ui;

class ServerListLayer
	: public BaseModeLayer

{
public:
	CREATE_FUNC(ServerListLayer);
	static ServerListLayer* create(int mode);

private:
	ServerListLayer();
	virtual ~ServerListLayer();
	bool init(void);

	//设置服务器列表
	void setDatas();

public:
	virtual void onEnterTransitionDidFinish();
	//按钮回调
private:
	bool addServer(CGameServerItem* pGameServerItem);
	bool removeServer(CGameServerItem* pGameServerItem);
	void updateServer(CGameServerItem* pGameServerItem);
	//隐藏加载中
	void hideLoading(float dt);
	///< 关闭回调
	void closeButtonTouch(cocos2d::Ref * ref, ui::TouchEventType eventType_);
	///< 关闭移动回调
	void closeMoveTo();

private:
	///< 房间背景层
	ImageView * img_RoomBg;
	///< 房间层
	ImageView * img_roomType;
	///< 房间选择滑动列表
	ListView * lv_roomlistHor;
	///< 一个房间节点层
	Layout * panel_oneList;
	///< 正在加载层
	Layout* panel_loading;
	///< 返回按钮
	Button* btn_return;
	///< 游戏房间容器
	std::vector<CGameServerItem *> CGameServerItemVertor;
};

#endif // _ServerListLayer_H_