#include "ServerListLayer.h"
#include "Platform/PFView/ModeScene/UserInfoLayer.h"
#include "ServerListScene.h"
#include "Tools/tools/MTNotification.h"

USING_NS_CC;

//////////////////////////////////////////////////////////////////////////
ServerListScene* ServerListScene::create(int mode)
{
	ServerListScene* scene = new ServerListScene();
	if (scene && scene->init(mode))
	{
		scene->autorelease();
		return scene;
	}

	delete scene;
	return 0;
}
//////////////////////////////////////////////////////////////////////////

ServerListScene::ServerListScene()
{
}

ServerListScene::~ServerListScene()
{
}

//初始化方法
bool ServerListScene::init(int mode)
{
	do
	{
		CC_BREAK_IF(!Scene::init());

		mMode = mode;

		// 		UserInfoLayer* info =UserInfoLayer::create();
		// 		info->setCloseDialogInfo(this, callfuncN_selector(ServerListScene::closeCallback), SSTRING("back_to_mode"), SSTRING("back_to_mode_content"));
		// 		addChild(info, 0);
		// 
		// 		TimeAwardLayer* timeaward = TimeAwardLayer::create();
		// 		addChild(timeaward);

		ServerListLayer* server_list = ServerListLayer::create(mMode);
		addChild(server_list, 0);
		return true;
	} while (0);

	return false;
}

void ServerListScene::closeCallback(cocos2d::Node *pNode)
{
	switch (pNode->getTag())
	{
	case DLG_MB_OK:
	{
					  //			G_NOTIFY("LOGON_COMPLETE", 0);
	}
		break;
	}
}