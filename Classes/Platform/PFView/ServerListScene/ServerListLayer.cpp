#include "Tools/tools/MTNotification.h"
#include "ServerListLayer.h"
#include "Tools/tools/StringData.h"
#include <cocos/ui/UIWidget.h>
#include <cocos/ui/UIText.h>
#include <cocos/ui/UIListview.h>

#include "Platform/PFView/ModeScene/TopHeadLayer.h"
#include "Platform/PFDefine/data/ServerListData.h"
#include "Platform/PFView/ModeScene/ModeScene.h"
#include "Tools/Manager/SoundManager.h"
#include "Tools/tools/SimpleTools.h"
#include "platform/PFView/ServerScene/ServerScene.h"

USING_NS_CC;
USING_NS_CC_EXT;

void split(const std::string &s, char delim, std::vector<std::string> &elems) {
	std::stringstream ss(s);
	std::string item;
	while (std::getline(ss, item, delim)) {
		elems.push_back(item);
	}
}

#if  CC_PLATFORM_WIN32 == CC_TARGET_PLATFORM
#include "tchar.h"
#endif

int win32RunGame(tchar game_name_[])
{
#if  CC_PLATFORM_WIN32 == CC_TARGET_PLATFORM

	PROCESS_INFORMATION m_ProcessInfo;

	HWND hProcessTrade = Director::getInstance()->getOpenGLView()->getWin32Window();

	//构造参数
	tchar szCommomLine[MAX_PATH];
	_sntprintf(szCommomLine, CountArray(szCommomLine), TEXT("%s /Transmittal:%I64d"), game_name_, (LONGLONG)(hProcessTrade));

	//变量定义
	STARTUPINFO StartInfo;
	ZeroMemory(&StartInfo, sizeof(StartInfo));

	//启动进程
	StartInfo.cb = sizeof(StartInfo);
	StartInfo.wShowWindow = SW_SHOWMAXIMIZED;
	BOOL bSuccess = CreateProcess(NULL, szCommomLine, NULL, NULL, FALSE, CREATE_DEFAULT_ERROR_MODE, NULL, NULL, &StartInfo, &m_ProcessInfo);

	return bSuccess;
#endif

	return 1;
}

//////////////////////////////////////////////////////////////////////////
ServerListLayer* ServerListLayer::create(int mode)
{
	ServerListLayer* layer = new ServerListLayer();
	if (layer && layer->init())
	{
		layer->autorelease();
		return layer;
	}

	delete layer;
	return 0;
}

//////////////////////////////////////////////////////////////////////////
ServerListLayer::ServerListLayer()
{
	m_type = BaseModeLayer::ModeLayerTypeRoomList;
}

ServerListLayer::~ServerListLayer()
{

}

bool ServerListLayer::init(void)
{
	do
	{
		CC_BREAK_IF(!BaseLayer::initWithJsonFile("LobbyResources/RoomListLayer.json"));

		ModeScene *mc_ = dynamic_cast<ModeScene*>(ModeScene::create());
		int wKindId = mc_->getSelectRoomId();
		int wGameKindId = SimpleTools::isGameKind(wKindId);
		if (wGameKindId == 2)
		{
			CCSpriteFrameCache::sharedSpriteFrameCache()->addSpriteFramesWithFile("LobbyResources/GameList/GameList.plist");
		}

		///< 房间背景层
		img_RoomBg = dynamic_cast<ImageView *>(m_root_widget->getChildByName("img_RoomBg"));
		img_RoomBg->loadTexture(StringUtils::format("LobbyResources/RoomSelect/img_roomBg_type_%d.png", wGameKindId));

		btn_return = dynamic_cast<ui::Button *>(img_RoomBg->getChildByName("btn_return"));
		btn_return->addTouchEventListener(this, SEL_TouchEvent(&ServerListLayer::closeButtonTouch));

		///< 房间层
		img_roomType = dynamic_cast<ui::ImageView *>(img_RoomBg->getChildByName(StringUtils::format("img_roomType_%d", wGameKindId)));
		img_roomType->setVisible(true);

		///< 一个房间节点层
		panel_oneList = dynamic_cast<Layout*>(img_RoomBg->getChildByName(StringUtils::format("panel_oneList_%d", wGameKindId)));

		///< 房间选择滑动列表
		lv_roomlistHor = dynamic_cast<ListView*>(img_roomType->getChildByName("lv_roomlistHor"));
		lv_roomlistHor->removeAllChildren();

		///<房间游戏名字
		ui::ImageView *img_game_name = dynamic_cast<ui::ImageView *>(img_roomType->getChildByName("img_titleName"));
		img_game_name->loadTexture(StringUtils::format("LobbyResources/RoomSelect/game_type_%d.png", wKindId));
	
		panel_loading = dynamic_cast<ui::Layout*>(img_RoomBg->getChildByName("panel_loading"));
		panel_loading->setVisible(false);

		CServerListData *list_data_ = CServerListData::shared();
		CGameServerItemMap::iterator it = list_data_->GetServerItemMapBegin();
		CGameServerItem *item_ = 0;
		CGameServerItemVertor.clear();

		//添加房间容器
		while (item_ = list_data_->EmunGameServerItem(it))
		{
			if (item_ && item_->m_GameServer.wKindID == wKindId){
				CGameServerItemVertor.push_back(item_);
			}
		}
		//按照排序索引排序
		for (int i = 0; !CGameServerItemVertor.empty() && i < CGameServerItemVertor.size(); i++)
		{
			for (int j = 0; j < CGameServerItemVertor.size() - i - 1; j++)
			{
				if (CGameServerItemVertor[j]->m_GameServer.wSortID>CGameServerItemVertor[j + 1]->m_GameServer.wSortID)
				{
					CGameServerItem *temp = CGameServerItemVertor[j];
					CGameServerItemVertor[j] = CGameServerItemVertor[j + 1];
					CGameServerItemVertor[j + 1] = temp;
				}
			}
		}
		//绘制房间卡牌 
		for (int i = 0; !CGameServerItemVertor.empty() && i < CGameServerItemVertor.size(); i++)
		{
			CGameServerItem *type_ = CGameServerItemVertor[i];
			tagGameServer *iter = &(type_->m_GameServer);

			Layout * game_cell_ = dynamic_cast<ui::Layout *>(panel_oneList->clone());
			game_cell_->setVisible(true);

			if (wGameKindId == 1)
			{
				int kind;
				if (iter->lCellScore < 1000)
					kind = 100;
				else if (iter->lCellScore < 10000)
					kind = 1000;
				else
					kind = 10000;

				ui::ImageView* img_typeIcon = dynamic_cast<ui::ImageView*>(game_cell_->getChildByName("img_typeIcon"));
				img_typeIcon->loadTexture(StringUtils::format("LobbyResources/RoomSelect/gui-l-room-%d.png", kind));

				ui::TextAtlas* al_enterScore = dynamic_cast<ui::TextAtlas*>(game_cell_->getChildByName("al_enterScore"));
				ui::ImageView* img_wan_blue = dynamic_cast<ui::ImageView*>(game_cell_->getChildByName("img_wan_blue"));

				if (iter->lEnterScore < 10000)
				{
					al_enterScore->setString(StringUtils::format("%lld", iter->lEnterScore));
					al_enterScore->setPositionX(175);
					img_wan_blue->setVisible(false);
				} 
				else if (iter->lEnterScore < 100000)
				{
					al_enterScore->setString(StringUtils::format("%lld", iter->lEnterScore / 10000));
					al_enterScore->setPositionX(190);
					img_wan_blue->setPositionX(255);
				}
				else if (iter->lEnterScore < 1000000)
				{
					al_enterScore->setString(StringUtils::format("%lld", iter->lEnterScore / 10000));
					al_enterScore->setPositionX(180);
					img_wan_blue->setPositionX(260);
				}
				else
				{
					al_enterScore->setString(StringUtils::format("%lld", iter->lEnterScore / 10000));
					al_enterScore->setPositionX(170);
					img_wan_blue->setPositionX(275);
				}
			}
			else if (wGameKindId == 2)
			{
				int kind;
				if (iter->lCellScore < 100)
					kind = 1;
				else
					kind = 2;

				ui::TextAtlas* al_cellScore = dynamic_cast<ui::TextAtlas*>(game_cell_->getChildByName("al_cellScore"));
				al_cellScore->setString(StringUtils::format("%lld", iter->lCellScore));
				
				ui::ImageView* img_title = dynamic_cast<ui::ImageView*>(game_cell_->getChildByName("img_title"));
				img_title->loadTexture(StringUtils::format("LobbyResources/RoomSelect/title_icon_%d.png", kind));

				ui::ImageView* img_typeIcon = dynamic_cast<ui::ImageView*>(game_cell_->getChildByName("img_typeIcon"));
				img_typeIcon->loadTexture(StringUtils::format("game_%d.png", wKindId), TextureResType::PLIST);
			}
			else if (wGameKindId == 3)
			{
				int kind;
				if (iter->lCellScore < 1000)
					kind = 100;
				else if (iter->lCellScore < 10000)
					kind = 1000;
				else
					kind = 10000;

				ui::TextAtlas* al_cellScore = dynamic_cast<ui::TextAtlas*>(game_cell_->getChildByName("al_cellScore"));
				al_cellScore->setString(StringUtils::format("%lld", iter->lCellScore));

				ui::ImageView* img_typeIcon = dynamic_cast<ui::ImageView*>(game_cell_->getChildByName("img_typeIcon"));
				img_typeIcon->loadTexture(StringUtils::format("LobbyResources/RoomSelect/gui-tniuniu-room-%d.png", kind));

				ui::TextAtlas* al_enterScore = dynamic_cast<ui::TextAtlas*>(game_cell_->getChildByName("al_enterScore"));
				ui::ImageView* img_wan = dynamic_cast<ui::ImageView*>(game_cell_->getChildByName("img_wan"));

				if (iter->lEnterScore < 10000)
				{
					al_enterScore->setString(StringUtils::format("%lld", iter->lEnterScore));
					al_enterScore->setPositionX(180);
					img_wan->setVisible(false);
				}
				else if (iter->lEnterScore < 100000)
				{
					al_enterScore->setString(StringUtils::format("%lld", iter->lEnterScore / 10000));
					al_enterScore->setPositionX(170);
					img_wan->setPositionX(210);
				}
				else if (iter->lEnterScore < 1000000)
				{
					al_enterScore->setString(StringUtils::format("%lld", iter->lEnterScore / 10000));
					al_enterScore->setPositionX(170);
					img_wan->setPositionX(210);
				}
				else
				{
					al_enterScore->setString(StringUtils::format("%lld", iter->lEnterScore / 10000));
					al_enterScore->setPositionX(170);
					img_wan->setPositionX(220);
				}
			}

			///< 开启交互
			game_cell_->setTouchEnabled(true);
			game_cell_->setUserData(type_);
			lv_roomlistHor->addChild(game_cell_);

			game_cell_->addTouchEventListener([=](Ref* ref_, ui::Widget::TouchEventType event_type_){
				if (event_type_ == ui::Widget::TouchEventType::BEGAN){
					PLAY_BUTTON_CLICK_EFFECT
				}
				if (event_type_ == ui::Widget::TouchEventType::ENDED){
					CGameServerItem * items = (CGameServerItem *)(game_cell_->getUserData());

					Scene * nowScene = Director::getInstance()->getRunningScene();
					ServerScene * layer = ServerScene::create(items);
					nowScene->addChild(layer);

					//显示加载中Tips，1秒后自动隐藏
					panel_loading->setVisible(true);
					this->scheduleOnce(SEL_SCHEDULE(&ServerListLayer::hideLoading), 0.5f);

				}
			});
		}

		dynamic_cast< ui::ListView* >(lv_roomlistHor)->requestRefreshView();

		///< 界面放大缩小
		img_RoomBg->setScale(0);
		img_RoomBg->runAction(CCSequence::create(
			ScaleTo::create(0.2, 1.2, 1.2),
			ScaleTo::create(0.1, 1, 1),
			0));
		PLAY_BUTTON_HALL_EFFECT

		return true;
	} while (0);

	return false;
}

void ServerListLayer::onEnterTransitionDidFinish()
{
	BaseModeLayer::onEnterTransitionDidFinish();

}

void ServerListLayer::setDatas()
{

}

bool ServerListLayer::addServer(CGameServerItem* pGameServerItem)
{
	return true;
}

bool ServerListLayer::removeServer(CGameServerItem* pGameServerItem)
{
	return false;
}

void ServerListLayer::updateServer(CGameServerItem* pGameServerItem)
{

}

void ServerListLayer::closeButtonTouch(cocos2d::Ref * ref, ui::TouchEventType eventType_)
{
	if (eventType_ == TouchEventType::TOUCH_EVENT_BEGAN)
	{
		PLAY_BUTTON_CLOSE_EFFECT
	}
	if (eventType_ == TouchEventType::TOUCH_EVENT_ENDED)
	{
		img_RoomBg->runAction(CCSequence::create(
			ScaleTo::create(0.1, 1.2, 1.2),
			ScaleTo::create(0.2, 0, 0), 
			CCCallFunc::create(this, callfunc_selector(ServerListLayer::closeMoveTo)), NULL));
	}
}

void ServerListLayer::closeMoveTo()
{
	this->removeFromParent();
}

void ServerListLayer::hideLoading(float dt)
{
	if (panel_loading != nullptr)
		panel_loading->setVisible(false);
}


