#ifndef _ServerListScene_H_
#define _ServerListScene_H_

#include "cocos2d.h"

class ServerListScene
	: public cocos2d::CCScene
{
public:
	static ServerListScene* create(int mode);

private:
	ServerListScene();
	~ServerListScene();
	bool init(int mode);

private:
	void closeCallback(cocos2d::Node *pNode);
private:
	int mMode;

};
#endif // _ServerListScene_H_