#ifndef ROOMLISTLAYOUT_H_
#define ROOMLISTLAYOUT_H_

#include "Platform/PFView/BaseLayer.h"
//#include "ViewHeader.h"
#include "ui/CocosGUI.h"
#include "../ModeScene/TopHeadLayer.h"

USING_NS_CC;
using namespace ui;

class RoomListLayout : public BaseModeLayer
{
public:
	RoomListLayout();
	~RoomListLayout();
	RoomListLayout * create(int numberPeople);
	bool init(word numberPeople, word tKindID, dword dwServerRule);
	bool init();
	void initCompoent();
	void initLockPanel();
	void initInputPswPanel();
	virtual void onEnterTransitionDidFinish();
	virtual void SendUserRulePacket(bool isLockTable, std::string password);
	void initData();
	void initCompoent100();
protected:
	///< 人数
	word mNumberPeople;
	///< 游戏ID
	word mKindId;
	///////////////////////////////////////////////////////////////////////////////////////
	///< 二人的桌面
protected:
	///< 所有桌子
	ListView * lv_tableListVer;
	///< 一排桌子
	ListView * lv_tableListHor;
	///< 一个桌子
	Layout * Panel_unit;
	//
	Layout * Panel_units;

	///< 锁桌的密码
	TextField *tf_psw;

	///< 输出密码
	TextField *tf_password;
	///<锁桌面板
	Layout * Panel_LockDesktop;
	///< 房间背景层
	ImageView * img_RoomBg;
	///< 输入密码面板
	Layout * Panel_InputPas;
	Button * btn_lock;			//锁桌
	Button * btn_join;			//快速加入
	Button * btn_exit;			//退出
	Button * btn_return;		//返回按钮

protected:
	short mTableId;
	short mChirdId;
	///< 是本桌锁桌
	bool isLockTable;
	///< 是进入别人的锁桌
	bool isJoinLockTable;
private:
	virtual void btnJoinCallBack(cocos2d::Ref * ref, TouchEventType eType);
	virtual void btnBanCheatCallBack(cocos2d::Ref * ref, TouchEventType eType);
	virtual void btnLockDeskCallBack(cocos2d::Ref * ref, TouchEventType eType);
	virtual void closeButtonTouch(cocos2d::Ref * ref, ui::TouchEventType eventType_);

	virtual void btnLockDeskAffrimCallBack(cocos2d::Ref * ref, TouchEventType eType);
	virtual void btnLockDeskCancelCallBack(cocos2d::Ref * ref, TouchEventType eType);
	virtual void btnInputPswCancelCallBack(cocos2d::Ref * ref, TouchEventType eType);
	virtual void btnInputPswAffrimCallBack(cocos2d::Ref * ref, TouchEventType eType);
	virtual void cbSelectLockDesktopCallBack(cocos2d::Ref * ref, CheckBoxEventType eType);

	/////////////////////////////////////////////////////////////////////////////////////////
	///< 100人游戏
protected:
	ui::ImageView * img_game_bg;
	ui::Button * btn_join_game;
	ui::Button * btn_game_begin;
private:
	
};
#endif


