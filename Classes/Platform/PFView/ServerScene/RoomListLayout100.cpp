#include "RoomListLayout.h"
#include "../ModeScene/ModeScene.h"
#include "Tools/tools/SimpleTools.h"
#include "Tools/Dialog/NewDialog.h"

void RoomListLayout::initCompoent100()
{
	img_game_bg = dynamic_cast<ui::ImageView *>(m_root_widget->getChildByName("img_game_bg"));

	btn_join_game = dynamic_cast<Button *>(m_root_widget->getChildByName("btn_join_game"));
	btn_join_game->addTouchEventListener(this, SEL_TouchEvent(&RoomListLayout::btnJoinCallBack));

	btn_game_begin = dynamic_cast<Button *>(m_root_widget->getChildByName("btn_game_begin"));
	btn_game_begin->addTouchEventListener(this, SEL_TouchEvent(&RoomListLayout::btnJoinCallBack));

}
