#ifndef ROOMINFOLIST_H_
#define ROOMINFOLIST_H_

#include "Platform/PFView/BaseLayer.h"
#include "cocos2d.h"

class RoomInfoLayer : public BaseModeLayer
{
public:
	RoomInfoLayer();
	~RoomInfoLayer();

	CREATE_FUNC(RoomInfoLayer);
	bool init();
};



#endif
