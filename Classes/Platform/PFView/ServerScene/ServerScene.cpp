//工具头文件
#include "Platform/PFView/ModeScene/ModeScene.h"
#include "Platform/PFView/LoadScene/LoadingLayer.h"
#include "ServerScene.h"
#include "Tools/tools/MTNotification.h"
#include "Tools/Dialog/NewDialog.h"
#include "Platform/PFView/ModeScene/ModeScene.h"
#include "Tools/tools/StringData.h"
#include "Tools/ViewHeader.h"
#include "Tools/tools/YRCommon.h"
#include "Tools/tools/MTNotification.h"
#include "Tools/Manager/SpriteHelper.h"
//游戏内核通信
#include "DntgGame/DntgKernel/DntgClientKernelSink.h"
#include "gamescene_Baccarat/ClientKernelSink_Baccarat.h"
#include "gamescene_ZJH/ClientKernelSink_zhajinhua.h"
#include "gamescene_DouDiZhu/ClientKereneSink_doudizhu.h"
#include "gamescene_Ox4/ClientKernelSink_Ox4.h"

//游戏游戏场景
#include "DntgGame/DntgView/Scene/DntgGameScene.h"
#include "gamescene_Baccarat/GameScene_Baccarat.h"
#include "gamescene_ZJH/GameScene_zhajinhua.h"
#include "gamescene_DouDiZhu/GameScene_doudizhu.h"
#include "gamescene_Ox4/GameScene_Ox4.h"

USING_NS_CC;

#define CREATEFINISH_GOTO  "createFinish_Goto"
//////////////////////////////////////////////////////////////////////////
//ServerScene *ServerScene::gServerScene = 0;
ServerScene* ServerScene::create(CGameServerItem* pGameServerItem)
{
// 	if (gServerScene){
// 		gServerScene->init(pGameServerItem);
// 		return gServerScene;
// 	}
	ServerScene * gServerScene = new ServerScene();
	if (gServerScene && gServerScene->init(pGameServerItem))
	{
		gServerScene->autorelease();
		return gServerScene;
	}

	delete gServerScene;
	gServerScene = 0;
	return 0;
}

bool ServerScene::isServerSceneExist(void)
{
//	return gServerScene != 0;
	Scene * now_Scene = Director::getInstance()->getRunningScene();
	if (now_Scene->getName() == "GameScene")
		return true;
	else
		return false;
}

void ServerScene::destroyServerScene()
{
// 	if (gServerScene)
// 		delete gServerScene;
// 	gServerScene = 0;
}
//////////////////////////////////////////////////////////////////////////

ServerScene::ServerScene()
: mMessageTemp(std::string())
{
	mServerItem = 0;
	mLoading = nullptr;

	//注册事件
	G_NOTIFY_REG("EXIT_SERVERSCENE", ServerScene::onEventExitLayer);
}

ServerScene::~ServerScene()
{
	if (IServerItem::get())
	{
		mServerItem->SetServerItemSink(0);
		mServerItem->SetChatSink(0);
		mServerItem->SetStringMessageSink(0);
	}

	if (IServerItem::get())
	{
		IServerItem::get()->IntermitConnect(true);
	}

	IServerItem::destory();
	mServerItem = 0;

	//销毁事件
	G_NOTIFY_UNREG("EXIT_SERVERSCENE");
	
	PLAZZ_PRINTF("~ServerScene");
}

//初始化方法
bool ServerScene::init(CGameServerItem* pGameServerItem)
{
	do 
	{
		CC_BREAK_IF(!RoomListLayout::init());
		mGameServerItem = pGameServerItem;

//		mServerItem = IServerItem::get();
		disconnectServer();

		connectServer();

		this->setName("ServerScene");

		return true;
	} while (0);

	return false;
}

void ServerScene::onEnterTransitionDidFinish()
{	

}

void ServerScene::onQuickSitdown(int table_id, int chair_id)
{
	word meTableID = table_id;
	word meChairID = chair_id;

	if (meTableID == INVALID_TABLE && meChairID == INVALID_CHAIR)
		mServerItem->PerformQuickSitDown();
	else if (mServerItem && mServerItem->GetServerAttribute().wChairCount >= 100 || mServerItem->GetServerAttribute().wChairCount == 1)
	{
		onThousandGameSitdown(table_id, chair_id);
	}
	else
	{
		int totalTable = mServerItem->GetTotalTableCount();

		if (meTableID != INVALID_TABLE && meChairID != INVALID_CHAIR && meTableID < totalTable)
		{
			std::string pass_value = tf_psw->getString();
			std::string join_pass_value = tf_password->getString();

			if (isLockTable && !pass_value.empty())
				mServerItem->PerformSitDownAction(meTableID, meChairID, true, pass_value.c_str());
			else if (isJoinLockTable && !join_pass_value.empty())
			{
				mServerItem->PerformSitDownAction(meTableID, meChairID, true, join_pass_value.c_str());
			}
			else
				 mServerItem->PerformSitDownAction(meTableID, meChairID, false);
		}
	}
}

void ServerScene::onThousandGameSitdown(short wTableId, short wChairId)
{
	if (mServerItem)
	{
		mServerItem->PerformSitDownAction(wTableId, wChairId, false);
	}
}

//////////////////////////////////////////////////////////////////////////
// IServerListDataSink
//完成通知
void ServerScene::OnGameItemFinish()
{
	//setDatas();
}

//完成通知
void ServerScene::OnGameKindFinish(uint16 wKindID)
{
	ModeScene *mc_ = dynamic_cast<ModeScene*>(Director::getInstance()->getRunningScene());
	if (mc_){
		mc_->changeModeLayer(BaseModeLayer::ModeLayerTypeRoomList);
	}
}

//更新通知
void ServerScene::OnGameItemUpdateFinish()
{
}

//插入通知
void ServerScene::OnGameItemInsert(CGameListItem * pGameListItem)
{
	//构造数据
	switch (pGameListItem->GetItemGenre())
	{
		case ItemGenre_Type:		//游戏类型
		{
										break;
		}
		case ItemGenre_Kind:		//游戏种类
		{
										break;
		}
		case ItemGenre_Node:		//游戏节点
		{
										break;
		}
		case ItemGenre_Server:		//游戏房间
		{
// 									if (addServer((CGameServerItem*)pGameListItem))
// 									{
// 									}
// 									break;
		}
		case ItemGenre_Page:	//定制子项
		{
									break;
		}
	}
}

//更新通知
void ServerScene::OnGameItemUpdate(CGameListItem * pGameListItem)
{
	//构造数据
	switch (pGameListItem->GetItemGenre())
	{
		case ItemGenre_Type:		//游戏类型
		{
										break;
		}
		case ItemGenre_Kind:		//游戏种类
		{
										break;
		}
		case ItemGenre_Node:		//游戏节点
		{
										break;
		}
		case ItemGenre_Server:		//游戏房间
		{
	// 									updateServer((CGameServerItem*)pGameListItem);
	// 									//更新视图
	// 									break;
		}
		case ItemGenre_Page:	//定制子项
		{
									break;
		}
	}
}

//删除通知
void ServerScene::OnGameItemDelete(CGameListItem * pGameListItem)
{
	//构造数据
	switch (pGameListItem->GetItemGenre())
	{
		case ItemGenre_Type:		//游戏类型
		{
										break;
		}
		case ItemGenre_Kind:		//游戏种类
		{
										break;
		}
		case ItemGenre_Node:		//游戏节点
		{
										break;
		}
		case ItemGenre_Server:		//游戏房间
		{
	// 									if (removeServer((CGameServerItem*)pGameListItem))
	// 									{
	// 									}
	// 									break;
		}
		case ItemGenre_Page:	//定制子项
		{
									break;
		}
	}
}

//////////////////////////////////////////////////////////////////////////
//登陆信息

//请求失败
void ServerScene::onGRRequestFailure(const std::string& szDescribeString)
{
	PLAZZ_PRINTF("onGRRequestFailure %s", szDescribeString.c_str());
/*	popup(SSTRING("gr_request_failure"), szDescribeString.c_str());*/
	NewDialog::create(szDescribeString.c_str(), NewDialog::NONEBUTTON);
}

//登陆成功
void ServerScene::OnGRLogonSuccess()
{
	PLAZZ_PRINTF(SSTRING("gr_login_success"));
}

//登陆失败
void ServerScene::OnGRLogonFailure(long lErrorCode, const std::string& sDescribeString)
{
	G_NOTIFY_D("BACK_TO_SERVER_LIST", MTData::create(0, 0, 0, SSTRING("gr_login_failed"), sDescribeString));
}

//登陆完成
void ServerScene::OnGRLogonFinish()
{
	//PLAZZ_PRINTF(t8("登陆完成"));

	//关闭提示
	//m_DlgStatus.HideStatusWindow();

	//激活房间
	//CPlatformFrame * pPlatformFrame=CPlatformFrame::GetInstance();
	//if (pPlatformFrame!=0) pPlatformFrame->ActiveServerViewItem(this);
	//if (pPlatformFrame!=0) pPlatformFrame->AddLastPlayGame(m_GameServer.wServerID);

	dword dwServerRule = mServerItem->GetServerAttribute().dwServerRule;

	//规则判断
	if (CServerRule::IsForfendGameRule(dwServerRule) == false)
	{
		//设置房间按钮可用
		//m_btTableButton4.EnableWindow(TRUE);
	}

	//锁桌判断
	if (CServerRule::IsForfendLockTable(dwServerRule) == false && mCServerItem->GetServerAttribute().wChairCount <= MAX_CHAIR_GENERAL)
	{
		//设置锁桌按钮可用
		//m_btTableButton3.EnableWindow(TRUE);

		//btn_join->setVisible(true);
	}

	//查找按钮
	if (CServerRule::IsAllowAvertCheatMode(dwServerRule) == false)
	{
		//允许查找
		//m_btTableButton2.EnableWindow(TRUE);
	}

	//重入判断
	IClientUserItem * item = mServerItem->GetMeUserItem();
	int chair_id = item->GetChairID();
	int table_id = item->GetTableID();
	int user_status = item->GetUserStatus();

	if (item->GetChairID() != INVALID_CHAIR && item->GetTableID() != INVALID_TABLE)
	{
		if ((item->GetUserStatus() == US_READY || item->GetUserStatus() == US_PLAYING))
		{
			CCLOG("RECORD START GAME !!!!!!");
			this->StartGame();
		}
	}

	//定时刷新桌子
	if (!mCServerItem) return;
	word tChairCount = mCServerItem->GetServerAttribute().wChairCount;
	word tKindID = mCServerItem->GetServerAttribute().wKindID;
	if ( !m_root_widget )
	{
		RoomListLayout::init(tChairCount, tKindID, dwServerRule);

		if (!CServerRule::IsAllowAvertCheatMode(dwServerRule))
		{
			if (tChairCount >= 100 || tChairCount == 1)
			{
				int tableID = mServerItem->GetSpaceTableId();
				int chairId = mServerItem->GetSpaceChairId(tableID);

				onQuickSitdown(tableID, chairId);
			}
			else if (tChairCount < 100)
			{
				createDesktopLayout();
			}

			flushDesktopLayout();
		}
		else
		{
			mServerItem->PerformSitDownAction(INVALID_TABLE, INVALID_CHAIR, false);
		}
	}
}

//游戏退出关闭连接层
void ServerScene::onEventExitLayer(Ref* obj)
{
	word tChairCount = mCServerItem->GetServerAttribute().wChairCount;

	if (tChairCount >= 100 || tChairCount == 1 || CServerRule::IsAllowAvertCheatMode(mServerItem->GetServerAttribute().dwServerRule))
	{
		this->removeFromParent();
	}
}

//更新通知
void ServerScene::OnGRUpdateNotify(byte cbMustUpdate, const std::string& szDescribeString)
{
	//返回房间或退出房间
	if (cbMustUpdate)
	{
		popup(SSTRING("gr_update_notify"), szDescribeString.c_str(), DLG_MB_OK, 0, this, callfuncN_selector(ServerScene::OnUpdateRequest));
		return;
	}

	popup(SSTRING("gr_update_notify"), szDescribeString.c_str(), DLG_MB_OK | DLG_MB_CANCEL, 0, this, callfuncN_selector(ServerScene::OnUpdateRequest));
}

void ServerScene::OnUpdateRequest(Node* node)
{
	switch (node->getTag())
	{
	case DLG_MB_OK:
	{
		//退出游戏
		if (IServerItem::get())
			IServerItem::get()->IntermitConnect(true);
		//更新
		//PLAZZ_PRINTF(t8("游戏更新....未实现"));
		break;
	}
	}
}

//////////////////////////////////////////////////////////////////////////
//配置信息

//列表配置
void ServerScene::OnGRConfigColumn()
{
	//PLAZZ_PRINTF(t8("列表配置\n"));
}
//房间配置
void ServerScene::OnGRConfigServer()
{
	//PLAZZ_PRINTF(t8("房间配置\n"));
	// 资源目录
	// char szResDirectory[LEN_KIND]=("");
	// GetGameResDirectory(szResDirectory,CountArray(szResDirectory));
	// 			
	// //创建桌子
	// bool bSuccess=m_TableViewFrame.ConfigTableFrame(wTableCount,wChairCount,m_dwServerRule,m_wServerType,m_GameServer.wServerID,szResDirectory);
	// 			
	// //错误处理
	// if (bSuccess==false)
	// {
	// 	//隐藏提示
	// 	m_DlgStatus.HideStatusWindow();
	// 			
	// 	//关闭连接
	// 	m_TCPSocketModule->CloseSocket();
	// 			
	// 	//提示信息
	// 	CInformation Information(AfxGetMainWnd());
	// 	INT nResult=Information.ShowMessageBox(("游戏资源文件加载失败或者格式错误，是否现在进行修复？"),MB_ICONERROR|MB_YESNO);
	// 			
	// 	//下载游戏
	// 	if (nResult==IDYES)
	// 	{
	// 		CGlobalUnits * pGlobalUnits=CGlobalUnits::GetInstance();
	// 		pGlobalUnits->DownLoadClient(m_GameKind.szKindName,m_GameKind.wGameID,m_GameServer.wServerID);
	// 	}
	// 			
	// 	//关闭房间
	// 	PostMessage(WM_COMMAND,IDM_DELETE_SERVER_ITEM,0);
	// }
}
//道具配置
void ServerScene::OnGRConfigProperty()
{
	//PLAZZ_PRINTF(t8("道具配置\n"));
}
//玩家权限配置
void ServerScene::OnGRConfigUserRight()
{
	//PLAZZ_PRINTF(t8("玩家权限配置\n"));
}

//配置完成
void ServerScene::OnGRConfigFinish()
{
	//PLAZZ_PRINTF(t8("配置完成\n"));

	////资源目录
	//char szResDirectory[LEN_KIND]=("");
	//GetGameResDirectory(szResDirectory,CountArray(szResDirectory));

	////游戏等级
	//ASSERT(m_GameLevelParserModule.GetInterface()!=0);
	//bool bSuccess=m_GameLevelParserModule->LoadGameLevelItem(m_GameKind.szKindName,szResDirectory,m_wServerType);

	////错误处理
	//if (bSuccess==false)
	//{
	//	//关闭连接
	//	m_TCPSocketModule->CloseSocket();

	//	//提示信息
	//	INT nResult=ShowInformation(("游戏等级配置读取失败或者格式错误，是否现在进行修复？"),MB_ICONERROR|MB_YESNO);

	//	//下载游戏
	//	if (nResult==IDYES)
	//	{
	//		CGlobalUnits * pGlobalUnits=CGlobalUnits::GetInstance();
	//		pGlobalUnits->DownLoadClient(m_GameKind.szKindName,m_GameKind.wGameID,m_GameServer.wServerID);
	//	}

	//	//关闭房间
	//	PostMessage(WM_COMMAND,IDM_DELETE_SERVER_ITEM,0);
	//}
}

//////////////////////////////////////////////////////////////////////////
//用户信息

//用户进入
void ServerScene::OnGRUserEnter(IClientUserItem* pIClientUserItem)
{
	char szDescribe[256] = { 0 };
	sprintf(szDescribe, ("用户进入:%s\n"), pIClientUserItem->GetNickName());
	PLAZZ_PRINTF(szDescribe);
}

//用户更新
void ServerScene::OnGRUserUpdate(IClientUserItem* pIClientUserItemconst)
{
	if (pIClientUserItemconst == IServerItem::get()->GetMeUserItem())
		G_NOTIFY_D("USER_SCORE", 0);

	flushPlayerDesktopLayout(pIClientUserItemconst);
}

//用户删除
void ServerScene::OnGRUserDelete(IClientUserItem* pIClientUserItem)
{
	//char szDescribe[256]={0};
	//sprintf(szDescribe, t8("用户删除:%s\n"), pIClientUserItem->GetNickName());
	//PLAZZ_PRINTF(szDescribe);
}

//////////////////////////////////////////////////////////////////////////
//框架消息

//用户邀请
void ServerScene::OnGFUserInvite(const std::string& szMessage)
{
	popup(SSTRING("gr_user_invite"), szMessage.c_str());
}
//用户邀请失败
void ServerScene::OnGFUserInviteFailure(const std::string& szMessage)
{
	popup(SSTRING("gr_user_invite_failed"), szMessage.c_str());
}

//房间退出
void ServerScene::OnGFServerClose(const std::string& szMessage)
{
	mMessageTemp = szMessage;

	PLAZZ_PRINTF("ServerScene::OnGFServerClose %s", mMessageTemp.c_str());

	//< 提示退出
	NewDialog::create(mMessageTemp, NewDialog::NONEBUTTON,nullptr, nullptr, [=]()
	{
		//		G_NOTIFY_D("MODE_SELECTED", MTData::create(1));
		if (IClientKernel::get())
		{
			G_NOTIFY_D("RECONNECT_ON_LOSS", MTData::create());
		}

		if (mLoading)
		{
			mLoading->removeFromParent();
			mLoading = nullptr;
		}
	});
	//延迟退出
	//this->scheduleOnce(schedule_selector(ServerScene::func_server_close), 0.3f);

}

//启动游戏
bool ServerScene::StartGame()
{
	mLoading = LoadingLayer::create(mGameServerItem->m_GameServer.wKindID);
	mLoading->setCallBack([=](){
		this->CreateKernel();
	});
	Director::getInstance()->getRunningScene()->addChild(mLoading, 3);

	return true;
}
//创建游戏内核
bool ServerScene::CreateKernel()
{
	IClientKernel* kernel = IClientKernel::create();

	if (kernel == 0)
		return false;

	//ASSERT(nullptr, "this is very very Important");
	else if (mGameServerItem->m_GameServer.wKindID == DANAOTIANGO_KINDID)
		kernel->SetClientKernelSink(&Dntg::ClientKernelSink::getInstance());
	else if (mGameServerItem->m_GameServer.wKindID == BAIJIALE_KINDID)
		kernel->SetClientKernelSink(&ClientKernelSink_Baccarat::getInstance());
	else if (mGameServerItem->m_GameServer.wKindID == ZHAJINHUA_KINDID)
		kernel->SetClientKernelSink(ClientKernelSink_zhajinhua::getInstance());
	else if (mGameServerItem->m_GameServer.wKindID == DOUDIZHU_KINDID)
		kernel->SetClientKernelSink(&ClientKereneSink_doudizhu::getInstance());
	else if (mGameServerItem->m_GameServer.wKindID == SIRENNIUNIU_KINDID)
		kernel->SetClientKernelSink(&gClientKernelSink_Ox4);

	kernel->setGameServerItem(mGameServerItem);
	if (kernel->Init())
	{
		if (mLoading)
			mLoading->removeFromParent();
		mLoading = nullptr;
		return true;
	}

	mLoading->removeFromParent();

	IClientKernel::destory();
	return false;
}


void ServerScene::func_server_close(float dt)
{
	if (!mMessageTemp.empty())
	{
		G_NOTIFY_D("BACK_TO_SERVER_LIST", MTData::create(0, 0, 0, SSTRING("gr_exit_room"), mMessageTemp));
	}
	else
	{
		G_NOTIFY_D("BACK_TO_SERVER_LIST", 0);
	}
}

// void ServerScene::clickAddButtonTouch(cocos2d::Ref * ref, cocos2d::ui::TouchEventType eType)
// {
// 	if (eType == ui::TouchEventType::TOUCH_EVENT_BEGAN)
// 	{
// 
// 	}
// 	else if (eType == ui::TouchEventType::TOUCH_EVENT_ENDED)
// 	{
// 		bool isServer = mServerItem->IsService();
// 		if (!isServer)
// 		{
// 			connectServer();
// 		}
// 
// 		mLoading = LoadingLayer::create(mGameServerItem->m_GameServer.wKindID);
// 		mLoading->setCallBack([=](){
// 			onQuickSitdown(0, 0);
// 		});
// 		Director::getInstance()->getRunningScene()->addChild(mLoading, 3);
// 		
// 	
// 		//connectServer();
// 	}
// }

void ServerScene::connectServer()
{
	if (mLoading)
	{
		mLoading->removeFromParent();
		mLoading = nullptr;
	}
	
	disconnectServer();

	if (mServerItem == 0)
	{
		mServerItem = IServerItem::create();
		mCServerItem = dynamic_cast<CServerItem *>(mServerItem);
		mServerItem->SetServerItemSink(this);
		///< 载入游戏基础数据
		loadGameBaseData();
		mServerItem->ConnectServer(mGameServerItem, 0, 0);

		//CServerListData::shared()->SetServerListDataSink(this);
	}
	
}

void ServerScene::disconnectServer()
{
	if (mServerItem){
		if (IServerItem::get())
		{
			mServerItem->SetServerItemSink(0);
			mServerItem->SetChatSink(0);
			mServerItem->SetStringMessageSink(0);
		}

		IServerItem::destory();
		mServerItem = 0;

		if (IServerItem::get())
		{
			IServerItem::get()->IntermitConnect(true);
		}
	}
}

void ServerScene::loadGameBaseData()
{
	if (mGameServerItem->m_GameServer.wKindID == DANAOTIANGO_KINDID)
	{
		Dntg::GameScene::initGameBaseData();
	}
	else if (mGameServerItem->m_GameServer.wKindID == BAIJIALE_KINDID)
	{
		GameScene_Baccarat::initGameBaseData();
	}
	else if (mGameServerItem->m_GameServer.wKindID == ZHAJINHUA_KINDID)
	{
		GameScene_zhajinhua::initGameBaseData();
	}
	else if (mGameServerItem->m_GameServer.wKindID == DOUDIZHU_KINDID)
	{
		GameScene_doudizhu::initGameBaseData();
	}
	else if (mGameServerItem->m_GameServer.wKindID == SIRENNIUNIU_KINDID)
	{
		GameScene_Ox4::initGameBaseData();
	}
}

//创建桌子
void ServerScene::createDesktopLayout()
{
	if (!mGameServerItem) return;
	tagServerAttribute ServerAttribute = mCServerItem->GetServerAttribute();
	
	word wHorCount = 2;		//一页桌子数量
	if (ServerAttribute.wChairCount == 1 || ServerAttribute.wChairCount == 2)
		wHorCount = 3;
	word wHorTemp = 0;
	Layout * new_layer;
	for (word wTable = 0; wTable < ServerAttribute.wTableCount; wTable++)
	{
		if (wHorTemp == 0)
			new_layer = dynamic_cast<Layout *>(lv_tableListHor->clone());
		Layout * now_panel = dynamic_cast<Layout *>(Panel_unit->clone());
		now_panel->setTag(wTable);
		now_panel->setVisible(true);
		now_panel->setScale(0.7);
		new_layer->addChild(now_panel);
		TextAtlas * Label_num = dynamic_cast<TextAtlas *>(now_panel->getChildByName("al_num"));
		Label_num->setString(StringUtils::format("%d", wTable + 1));
		for (word wChair = 0; wChair < ServerAttribute.wChairCount; wChair++)
		{
			Button * btn_chair = dynamic_cast<Button *>(now_panel->getChildByTag(wChair));
			//btn_chair->setTag(wTable);
			btn_chair->addTouchEventListener(this, SEL_TouchEvent(&ServerScene::btnDesktopClickTop));
		}

		wHorTemp++;


		mDesktopList.push_back(now_panel);
		if (wHorTemp == wHorCount)
		{
			lv_tableListVer->pushBackCustomItem(new_layer);
			wHorTemp = 0;
		}
	}

 	if (wHorTemp != 0)
		lv_tableListVer->pushBackCustomItem(new_layer);
}

void ServerScene::flushDesktopLayout()
{
	//创建桌子
	if (!mCServerItem) return;

	word tChairCount = mCServerItem->GetServerAttribute().wChairCount;
	dword dwServerRule = mCServerItem->GetServerAttribute().dwServerRule;

// 	if (tChairCount >= 100)
// 	{
// 		bool isPlayer = mCServerItem->GetTableGameState(0);
// 		if (isPlayer)
// 		{
// 			btn_join_game->setVisible(false);
// 			btn_game_begin->setVisible(true);
// 		}
// 		else
// 		{
// 			btn_join_game->setVisible(true);
// 			btn_game_begin->setVisible(false);
// 		}
// 
// 		return;
// 	}

	static int numCount = 0;
	dword userCount = mCServerItem->GetActiveUserCount();

	clearDesktopLayout();

	for (int i = 0; i < userCount; i++)
	{
		IClientUserItem * useritem = mCServerItem->GetTableUserItem(i);
		if (useritem)
		{
			std::string username = useritem->GetNickName();
			byte sex = useritem->GetGender();
			int tableid = useritem->GetTableID();
			int chairid = useritem->GetChairID();
			bool isLock = false;
			bool isPlay = false;
			int vipLevel = useritem->GetMemberOrder(); ///<会员等级
			int64 gameScore = useritem->GetUserScore(); ///<游戏积分
			if (tableid != INVALID_TABLE && tableid < mDesktopList.size())
			{
				isLock = mCServerItem->GetTableLockState(tableid);
				isPlay = mCServerItem->GetTableGameState(tableid);
			}
			//CCLOG("nickname %s---table %d---chair %d---isLock %d---isPlay %d ", username.c_str(), tableid, chairid, isLock, isPlay);

			if (tableid != INVALID_TABLE && tableid < mDesktopList.size())
			{
				Layout * now_panel = mDesktopList[tableid];

				for (word wChairId = 0; wChairId < mNumberPeople; wChairId++)
				{
					if (chairid == wChairId)
					{
						Text * lab_name = dynamic_cast<Text *>(now_panel->getChildByTag(10 + wChairId));
						Button * btn_chair = dynamic_cast<Button *>(now_panel->getChildByTag(wChairId));
						ImageView * img_face = dynamic_cast<ImageView *>(btn_chair->getChildByTag(100 + wChairId));
						ImageView * img_head = dynamic_cast<ImageView *>(img_face->getChildByName("img_head"));
						ImageView * img_gameBegin = dynamic_cast<ImageView *>(now_panel->getChildByName("img_begin"));
						ImageView * img_gameLock = dynamic_cast<ImageView *>(now_panel->getChildByName("img_lock"));

						lab_name->setString(username);
						lab_name->setVisible(true);
						img_gameBegin->setVisible(isPlay);
						img_gameLock->setVisible(isLock);

						if (useritem->GetFaceID() % 8 < 4)
						{
							//性别女
							img_head->loadTexture(StringUtils::format("LobbyResources/headIcon/women_%d.png", useritem->GetFaceID() % 8 + 1));
						} 
						else
						{
							//性别男
							img_head->loadTexture(StringUtils::format("LobbyResources/headIcon/men_%d.png", useritem->GetFaceID() % 8 - 3));
						}

						img_face->setVisible(true);
						if (vipLevel > 0)
							lab_name->setTextColor(Color4B::RED);
					}
				}
			}
		}
	}
	
}

void ServerScene::flushPlayerDesktopLayout(IClientUserItem * pIClientUserItem)
{
	if (!mCServerItem) return;

	word tChairCount = mCServerItem->GetServerAttribute().wChairCount;
	dword dwServerRule = mCServerItem->GetServerAttribute().dwServerRule;

// 	if (tChairCount >= 100)
// 	{
// 		bool isPlayer = mCServerItem->GetTableGameState(0);
// 		if (isPlayer)
// 		{
// 			btn_join_game->setVisible(false);
// 			btn_game_begin->setVisible(true);
// 		}
// 		else
// 		{
// 			btn_join_game->setVisible(true);
// 			btn_game_begin->setVisible(false);
// 		}
// 		return;
// 	}

	word wTableID = pIClientUserItem->GetTableID();
	word wChairID = pIClientUserItem->GetChairID();

	if (wTableID != INVALID_TABLE && wTableID < mDesktopList.size())
	{
		Widget * now_panel = getLayoutByTableID(wTableID);
		//初始化人物信息
		Button* btn_chair = dynamic_cast<Button *>(now_panel->getChildByTag(wChairID));
		ImageView* img_face = dynamic_cast<ImageView *>(btn_chair->getChildByTag(100 + wChairID));
		ImageView * img_head = dynamic_cast<ImageView *>(img_face->getChildByName("img_head"));

		if (pIClientUserItem->GetFaceID() % 8 < 4)
		{
			//性别女
			img_head->loadTexture(StringUtils::format("LobbyResources/headIcon/women_%d.png", pIClientUserItem->GetFaceID() % 8 + 1));
		}
		else
		{
			//性别男
			img_head->loadTexture(StringUtils::format("LobbyResources/headIcon/men_%d.png", pIClientUserItem->GetFaceID() % 8 - 3));
		}
	}

	//全局刷新状态
	dword userCount = mCServerItem->GetActiveUserCount();
	clearDesktopLayout();

	for (int i = 0; i < userCount; i++)
	{
		IClientUserItem * useritem = mCServerItem->GetTableUserItem(i);
		if (useritem)
		{
			std::string username = useritem->GetNickName();
			int tableid = useritem->GetTableID();
			int chairid = useritem->GetChairID();
			bool isLock = false;
			bool isPlay = false;
			int vipLevel = useritem->GetMemberOrder(); ///<会员等级

			if (tableid != INVALID_TABLE && tableid < mDesktopList.size())
			{
				Layout * now_panel = mDesktopList[tableid];
				isLock = mCServerItem->GetTableLockState(tableid);
				isPlay = mCServerItem->GetTableGameState(tableid);
				for (word wChairId = 0; wChairId < mNumberPeople; wChairId++)
				{
					if (chairid == wChairId)
					{
						Text * lab_name = dynamic_cast<Text *>(now_panel->getChildByTag(10 + wChairId));
						Button * btn_chair = dynamic_cast<Button *>(now_panel->getChildByTag(wChairId));
						ImageView * img_face = dynamic_cast<ImageView *>(btn_chair->getChildByTag(100 + wChairId));
						ImageView * img_gameBegin = dynamic_cast<ImageView *>(now_panel->getChildByName("img_begin"));
						ImageView * img_gameLock = dynamic_cast<ImageView *>(now_panel->getChildByName("img_lock"));
						TextAtlas * al_num = dynamic_cast<TextAtlas *>(now_panel->getChildByName("al_num"));

						lab_name->setString(username);
						lab_name->setVisible(true);
						img_gameBegin->setVisible(isPlay);
						img_gameLock->setVisible(isLock);
						al_num->setVisible(!isPlay);
						img_face->setVisible(true);
						if (vipLevel > 0)
							lab_name->setTextColor(Color4B::RED);
					}
				}
			}
		}
	}
}


Layout * ServerScene::getLayoutByTableID(word wTableID)
{
	for (int i = 0; !mDesktopList.empty() && i < mDesktopList.size(); i++)
	{
		Layout * now_layout = mDesktopList[i];
		if (mDesktopList[i]->getTag() == wTableID)
		{
			return now_layout;
		}
	}
	return nullptr;
}

void ServerScene::clearDesktopLayout()
{
	for (auto it : mDesktopList)
	{
		Widget * now_panel = it;

		//初始化人物信息
		for (word i = 0; i < mNumberPeople; i++)
		{
			Text* lab_name = dynamic_cast<Text *>(now_panel->getChildByTag(10 + i));
			Button* btn_chair = dynamic_cast<Button *>(now_panel->getChildByTag(i));
			ImageView* img_face = dynamic_cast<ImageView *>(btn_chair->getChildByTag(100 + i));
			lab_name->setVisible(false);
			lab_name->setTextColor(Color4B::WHITE);
			img_face->setVisible(false);
		}

		//初始化状态
		ImageView * img_gameBegin = dynamic_cast<ImageView *>(now_panel->getChildByName("img_begin"));
		ImageView * img_gameLock = dynamic_cast<ImageView *>(now_panel->getChildByName("img_lock"));
		TextAtlas * al_num = dynamic_cast<TextAtlas *>(now_panel->getChildByName("al_num"));
		img_gameBegin->setVisible(false);
		img_gameLock->setVisible(false);
		al_num->setVisible(true);
	}
}

void ServerScene::btnDesktopClickTop(cocos2d::Ref * ref, cocos2d::ui::TouchEventType eType)
{
	if (eType == TouchEventType::TOUCH_EVENT_ENDED)
	{
		Button * now_btn = dynamic_cast<Button *>(ref);
		int tableid = now_btn->getParent()->getTag();
		bool isLock = false;
		bool isPlay = false;
		if (tableid != INVALID_TABLE && tableid < mDesktopList.size())
		{
			isLock = mCServerItem->GetTableLockState(tableid);
			isPlay = mCServerItem->GetTableGameState(tableid);
		}

		mTableId = tableid;
		mChirdId = now_btn->getTag();
		if (isLock)
		{
			Panel_InputPas->setVisible(true);
			Panel_InputPas->setTouchEnabled(true);
		}
		else
		{
			onQuickSitdown(mTableId, mChirdId);
		}
	}
}

void ServerScene::btnDesktopClickDown(cocos2d::Ref * ref, cocos2d::ui::TouchEventType eType)
{
	if (eType == TouchEventType::TOUCH_EVENT_ENDED)
	{
		Button * now_btn = dynamic_cast<Button *>(ref);
		int tableid = now_btn->getTag();

		bool isLock = false;
		bool isPlay = false;
		if (tableid != INVALID_TABLE && tableid < mDesktopList.size())
		{
			isLock = mCServerItem->GetTableLockState(tableid);
			isPlay = mCServerItem->GetTableGameState(tableid);
		}

		mTableId = tableid;
		mChirdId = 1;
		if (isLock)
		{
			Panel_InputPas->setVisible(true);
			Panel_InputPas->setTouchEnabled(true);
		}
		else
		{
			onQuickSitdown(mTableId, mChirdId);
		}
	}
}

void ServerScene::btnJoinCallBack(cocos2d::Ref * ref, TouchEventType eType)
{
	if (eType == TouchEventType::TOUCH_EVENT_ENDED)
	{
		int tableID = mServerItem->GetSpaceTableId();
		int chairId = mServerItem->GetSpaceChairId(tableID);

		onQuickSitdown(tableID, chairId);
	}
}

void ServerScene::btnBanCheatCallBack(cocos2d::Ref * ref, TouchEventType eType)
{
	if (eType == TouchEventType::TOUCH_EVENT_ENDED)
	{
		mServerItem->PerformSitDownAction(INVALID_TABLE, INVALID_CHAIR, false);
	}
}

void ServerScene::btnInputPswAffrimCallBack(cocos2d::Ref * ref, TouchEventType eType)
{
	if (eType == TouchEventType::TOUCH_EVENT_ENDED)
	{
		isJoinLockTable = true;
		Panel_InputPas->setTouchEnabled(false);
		Panel_InputPas->setVisible(false);
		onQuickSitdown(mTableId, mChirdId);
	}
}

void ServerScene::btnExitCallBack(cocos2d::Ref * ref, TouchEventType eType)
{
	if (eType == TouchEventType::TOUCH_EVENT_ENDED)
	{
		disconnectServer();
		ModeScene *mc_ = dynamic_cast<ModeScene*>(Director::getInstance()->getRunningScene());
		if (mc_)
		{
			mc_->changeModeLayer(BaseModeLayer::ModeLayerTypeRoomListBack);
		}
	}
}

void ServerScene::SendUserRulePacket(bool isLockTable, std::string password)
{
	mServerItem->SendUserPassWordPacket(isLockTable, password);
}

void ServerScene::closeButtonTouch(cocos2d::Ref * ref, ui::TouchEventType eventType_)
{
	if (eventType_ == TouchEventType::TOUCH_EVENT_BEGAN)
	{
		PLAY_BUTTON_CLOSE_EFFECT
	}
	if (eventType_ == TouchEventType::TOUCH_EVENT_ENDED)
	{
		img_RoomBg->runAction(CCSequence::create(
			ScaleTo::create(0.1, 1.2, 1.2),
			ScaleTo::create(0.2, 0, 0),
			CCCallFunc::create(this, callfunc_selector(ServerScene::closeMoveTo)), NULL));
	}
}

void ServerScene::closeMoveTo()
{
	this->removeFromParent();
}
