#ifndef _ServerScene_H_
#define _ServerScene_H_

#include "cocos2d.h"
#include "Kernel/kernel/server/IServerItemSink.h"
#include "Platform/PFDefine/data/ServerListData.h"
#include "kernel/kernel/server/CServerItem.h"
#include "RoomListLayout.h"

class LoadingLayer;


class ServerScene :public RoomListLayout
	, IServerItemSink
	, IServerListDataSink
{
public:

	//	static ServerScene *gServerScene;
	static bool isServerSceneExist(void);

	static ServerScene* create(CGameServerItem* pGameServerItem);
	static void destroyServerScene();
private:
	ServerScene();
	virtual ~ServerScene();
	bool init(CGameServerItem* pGameServerItem);
public:
	virtual void onEnterTransitionDidFinish();

private:
	//快速坐下命令
	void onQuickSitdown(int chair_id, int table_id);
	//百人游戏的坐庄
	void onThousandGameSitdown(short wTableId, short wChairId);
	//游戏退出关闭连接层
	void onEventExitLayer(Ref* obj);

	//////////////////////////////////////////////////////////////////////////
	// IServerItemSink
	////////////////////////////////////////////////////////////////////////// 
	//登陆信息
public:
	//坐下失败
	virtual void onGRRequestFailure(const std::string& sDescribeString);
	//登陆成功
	virtual void OnGRLogonSuccess();
	//登陆失败
	virtual void OnGRLogonFailure(long lErrorCode, const std::string& sDescribeString);
	//登陆完成
	virtual void OnGRLogonFinish();
	//更新通知
	virtual void OnGRUpdateNotify(byte cbMustUpdate, const std::string& sDescribeString);
	void OnUpdateRequest(Node* node);
	virtual void SendUserRulePacket(bool isLockTable, std::string password);

	//配置信息
public:
	//列表配置
	virtual void OnGRConfigColumn();
	//房间配置
	virtual void OnGRConfigServer();
	//道具配置
	virtual void OnGRConfigProperty();
	//玩家权限配置
	virtual void OnGRConfigUserRight();
	//配置完成
	virtual void OnGRConfigFinish();

	//用户信息
public:
	//用户进入
	virtual void OnGRUserEnter(IClientUserItem* pIClientUserItem);
	//用户更新
	virtual void OnGRUserUpdate(IClientUserItem* pIClientUserItemconst);
	//用户删除
	virtual void OnGRUserDelete(IClientUserItem* pIClientUserItem);

	//框架消息
public:
	//用户邀请
	virtual void OnGFUserInvite(const std::string& szMessage);
	//用户邀请失败
	virtual void OnGFUserInviteFailure(const std::string& szMessage);
	//房间退出
	virtual void OnGFServerClose(const std::string& szMessage);
	//创建游戏内核
	virtual bool CreateKernel();
	//启动游戏
	virtual bool StartGame();

	//////////////////////////////////////////////////////////////////////////
	// IServerListDataSink
public:
	//状态通知
public:
	//完成通知
	virtual void OnGameItemFinish();
	//完成通知
	virtual void OnGameKindFinish(uint16 wKindID);
	//更新通知
	virtual void OnGameItemUpdateFinish();
	//完成通知
	virtual void OnSystemMessageFinish() {};

	//更新通知
public:
	//插入通知
	virtual void OnGameItemInsert(CGameListItem * pGameListItem);
	//更新通知
	virtual void OnGameItemUpdate(CGameListItem * pGameListItem);
	//删除通知
	virtual void OnGameItemDelete(CGameListItem * pGameListItem);

private:
	virtual void createDesktopLayout();
	virtual void flushDesktopLayout();
	virtual void flushPlayerDesktopLayout(IClientUserItem * pIClientUserItem);
	virtual void clearDesktopLayout();

	virtual void btnExitCallBack(cocos2d::Ref * ref, TouchEventType eType);
	void btnDesktopClickTop(cocos2d::Ref * ref, cocos2d::ui::TouchEventType eType);
	void btnDesktopClickDown(cocos2d::Ref * ref, cocos2d::ui::TouchEventType eType);
	virtual void btnJoinCallBack(cocos2d::Ref * ref, TouchEventType eType);
	virtual void btnBanCheatCallBack(cocos2d::Ref * ref, TouchEventType eType);
	virtual void closeButtonTouch(cocos2d::Ref * ref, ui::TouchEventType eventType_);
	virtual void btnInputPswAffrimCallBack(cocos2d::Ref * ref, TouchEventType eType);
	Layout * getLayoutByTableID(word wTableID);

	//辅助函数
public:
	void func_server_close(float dt);
	///< 链接服务器
	void connectServer();
	///< 断开连接
	void disconnectServer();
	///< 载入游戏资源数据
	void loadGameBaseData();
	///< 关闭移动回调
	void closeMoveTo();

private:
	CGameServerItem*	mGameServerItem;
	IServerItem*		mServerItem;
	CServerItem*		mCServerItem;

	LoadingLayer *		mLoading;

	std::string			mMessageTemp;

protected:
	///<用户界面刷新
	std::vector<cocos2d::ui::Layout *> mDesktopList;
};
#endif // _ServerScene_H_