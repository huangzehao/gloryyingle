#include "RoomListLayout.h"
#include "ServerScene.h"
#include "../ModeScene/ModeScene.h"
#include "Tools/tools/MTNotification.h"
#include "Tools/tools/SimpleTools.h"
#include "Tools/tools/YRCommon.h"
#include "Tools/tools/StringData.h"
#include "Tools/Manager/SoundManager.h"
#include "Kernel/kernel/server/CServerItem.h"
#include "Tools/Dialog/NewDialog.h"

RoomListLayout::RoomListLayout()
{
	m_type = ModeLayerType::ModeLayerTypeRoomInfo;
	isLockTable = false;
	isJoinLockTable = false;
}


RoomListLayout::~RoomListLayout()
{
}

bool RoomListLayout::init(word numberPeople, word tKindID ,dword dwServerRule)
{
	mNumberPeople = numberPeople;
	mKindId = tKindID;

	//加载资源
	if (CServerRule::IsAllowAvertCheatMode(dwServerRule) == false)
	{
		//正常桌子房间
		if (mNumberPeople > 1 && mNumberPeople < 100)
		{
			if (!BaseLayer::initWithJsonFile("LobbyResources/RoomInfoLayer.json"))
				return false;

			initCompoent();
			initLockPanel();
			initInputPswPanel();
		}
	}
	return true;
}

bool RoomListLayout::init()
{
	return true;
}


void RoomListLayout::initCompoent()
{
	ModeScene *mc_ = dynamic_cast<ModeScene*>(ModeScene::create());
	int wKindId = mc_->getSelectRoomId();
	int wGameKindId = SimpleTools::isGameKind(wKindId);

	///< 房间背景层
	img_RoomBg = dynamic_cast<ImageView *>(m_root_widget->getChildByName("img_RoomBg"));

	///< 界面放大缩小
	img_RoomBg->setScale(0);
	img_RoomBg->runAction(CCSequence::create(
		ScaleTo::create(0.2, 1.2, 1.2),
		ScaleTo::create(0.1, 1, 1),
		0));
	PLAY_BUTTON_HALL_EFFECT

	///<房间背景
	ImageView *img_Room_bg = dynamic_cast<ImageView *>(img_RoomBg->getChildByName("img_Room_bg"));
	img_Room_bg->loadTexture(StringUtils::format("LobbyResources/RoomSelect/img_roomBg_type_%d.png", wGameKindId));
	///<房间游戏名字
	ImageView *img_game_name = dynamic_cast<ImageView *>(img_RoomBg->getChildByName("img_titleName"));
	img_game_name->loadTexture(StringUtils::format("LobbyResources/RoomSelect/game_type_%d.png", wKindId));

	ImageView *img_titleBg = dynamic_cast<ImageView *>(img_RoomBg->getChildByName("img_titleBg"));
	ImageView *img_listBg = dynamic_cast<ImageView *>(img_RoomBg->getChildByName("img_listBg"));
	if (wGameKindId == 3)
	{
		img_titleBg->setVisible(false);
		img_listBg->setVisible(false);
		img_game_name->setPositionY(300);
		img_game_name->setScale(0.9);
	}

	btn_join = dynamic_cast<Button *>(img_RoomBg->getChildByName("btn_join"));
	btn_join->addTouchEventListener(this, SEL_TouchEvent(&RoomListLayout::btnJoinCallBack));

	btn_lock = dynamic_cast<Button *>(img_RoomBg->getChildByName("btn_lock"));
	btn_lock->addTouchEventListener(this, SEL_TouchEvent(&RoomListLayout::btnLockDeskCallBack));

	btn_return = dynamic_cast<ui::Button *>(img_RoomBg->getChildByName("btn_return"));
	btn_return->addTouchEventListener(this, SEL_TouchEvent(&RoomListLayout::closeButtonTouch));

	lv_tableListVer = dynamic_cast<ListView *>(img_RoomBg->getChildByName("lv_tableListVer"));

	lv_tableListHor = dynamic_cast<ListView *>(img_RoomBg->getChildByName("lv_tableListHor"));

	Panel_units = dynamic_cast<Layout *>(img_RoomBg->getChildByName("Panel_tablelistHor"));

	//根据桌子人数确定桌子
	if (mNumberPeople == 1)
		Panel_unit = dynamic_cast<Layout *>(img_RoomBg->getChildByName("Panel_Table_1"));
	else if (mNumberPeople == 2)
		Panel_unit = dynamic_cast<Layout *>(img_RoomBg->getChildByName("Panel_Table_2"));
	else if (mNumberPeople == 3)
		Panel_unit = dynamic_cast<Layout *>(img_RoomBg->getChildByName("Panel_Table_3"));
	else if (mNumberPeople == 4)
		Panel_unit = dynamic_cast<Layout *>(img_RoomBg->getChildByName("Panel_Table_4"));		
	else if (mNumberPeople == 5)
		Panel_unit = dynamic_cast<Layout *>(img_RoomBg->getChildByName("Panel_Table_5"));
	else if (mNumberPeople == 6)
	{
		if (mKindId == 1002)
		{
			Panel_unit = dynamic_cast<Layout *>(img_RoomBg->getChildByName("Panel_Table_1002"));
		} 
		else
		{
			Panel_unit = dynamic_cast<Layout *>(img_RoomBg->getChildByName("Panel_Table_6"));
		}
	}
	else if (mNumberPeople == 7)
		Panel_unit = dynamic_cast<Layout *>(img_RoomBg->getChildByName("Panel_Table_7"));
	else if (mNumberPeople == 8)
		Panel_unit = dynamic_cast<Layout *>(img_RoomBg->getChildByName("Panel_Table_8"));
}

void RoomListLayout::btnJoinCallBack(cocos2d::Ref * ref, TouchEventType eType)
{
	if (eType == TouchEventType::TOUCH_EVENT_ENDED)
	{

	}
}

void RoomListLayout::btnBanCheatCallBack(cocos2d::Ref * ref, TouchEventType eType)
{
	if (eType == TouchEventType::TOUCH_EVENT_ENDED)
	{

	}
}

void RoomListLayout::btnLockDeskCallBack(cocos2d::Ref * ref, TouchEventType eType)
{
	if (eType == TouchEventType::TOUCH_EVENT_ENDED)
	{
		Panel_LockDesktop->setVisible(true);
		Panel_LockDesktop->setTouchEnabled(true);
	}
}

void RoomListLayout::onEnterTransitionDidFinish()
{
	BaseModeLayer::onEnterTransitionDidFinish();

	initData();
}

void RoomListLayout::initData()
{

}

void RoomListLayout::initLockPanel()
{
	Panel_LockDesktop = dynamic_cast<Layout *>(img_RoomBg->getChildByName("Panel_LockDesktop"));
	Panel_LockDesktop->setTouchEnabled(false);
	Panel_LockDesktop->setVisible(false);
	CheckBox * cb_lockdesktop = dynamic_cast<CheckBox *>(Panel_LockDesktop->getChildByName("cb_lockdesktop"));
	cb_lockdesktop->addEventListenerCheckBox(this, SEL_SelectedStateEvent(&RoomListLayout::cbSelectLockDesktopCallBack));
	tf_psw = dynamic_cast<TextField *>(Panel_LockDesktop->getChildByName("Panel_psw")->getChildByName("tf_psw"));
	tf_psw->setTouchEnabled(false);

	Button *btn_affirm = dynamic_cast<Button *>(Panel_LockDesktop->getChildByName("btn_affirm"));
	btn_affirm->addTouchEventListener(this, SEL_TouchEvent(&RoomListLayout::btnLockDeskAffrimCallBack));
	Button * btn_cancel = dynamic_cast<Button *>(Panel_LockDesktop->getChildByName("btn_cancel"));
	btn_cancel->addTouchEventListener(this, SEL_TouchEvent(&RoomListLayout::btnLockDeskCancelCallBack));

}

void RoomListLayout::initInputPswPanel()
{
	Panel_InputPas = dynamic_cast<Layout *>(img_RoomBg->getChildByName("Panel_InputPas"));
	Panel_InputPas->setTouchEnabled(false);
	Panel_InputPas->setVisible(false);

	tf_password = dynamic_cast<TextField *>(Panel_InputPas->getChildByName("Panel_password")->getChildByName("tf_password"));
	Button *btn_affirm = dynamic_cast<Button *>(Panel_InputPas->getChildByName("btn_affirm"));
	btn_affirm->addTouchEventListener(this, SEL_TouchEvent(&RoomListLayout::btnInputPswAffrimCallBack));

	Button * btn_cancel = dynamic_cast<Button *>(Panel_InputPas->getChildByName("btn_cancel"));
	btn_cancel->addTouchEventListener(this, SEL_TouchEvent(&RoomListLayout::btnInputPswCancelCallBack));

}

void RoomListLayout::btnLockDeskAffrimCallBack(cocos2d::Ref * ref, TouchEventType eType)
{
	if (eType == TouchEventType::TOUCH_EVENT_ENDED)
	{
		std::string txt = tf_psw->getString();
		bool isNumber = YRComStringIsNumber(txt);
		if (isNumber)
		{
			Panel_LockDesktop->setVisible(false);
			Panel_LockDesktop->setTouchEnabled(false);
			SendUserRulePacket(isLockTable, txt);
		}
		else
		{
			NewDialog::create(SSTRING("table_password_null"), NewDialog::NONEBUTTON);
		}
	}
}

void RoomListLayout::btnLockDeskCancelCallBack(cocos2d::Ref * ref, TouchEventType eType)
{
	if (eType == TouchEventType::TOUCH_EVENT_ENDED)
	{
		Panel_LockDesktop->setVisible(false);
		Panel_LockDesktop->setTouchEnabled(false);
	}
}

void RoomListLayout::btnInputPswCancelCallBack(cocos2d::Ref * ref, TouchEventType eType)
{
	if (eType == TouchEventType::TOUCH_EVENT_ENDED)
	{
		isJoinLockTable = false;
		Panel_InputPas->setTouchEnabled(false);
		Panel_InputPas->setVisible(false);
	}
}

void RoomListLayout::btnInputPswAffrimCallBack(cocos2d::Ref * ref, TouchEventType eType)
{
	if (eType == TouchEventType::TOUCH_EVENT_ENDED)
	{

	}
}

void RoomListLayout::cbSelectLockDesktopCallBack(cocos2d::Ref * ref, CheckBoxEventType eType)
{
	if (eType == CheckBoxEventType::CHECKBOX_STATE_EVENT_SELECTED)
	{
		tf_psw->setTouchEnabled(true);
		isLockTable = true;
	}
	else if (eType == CheckBoxEventType::CHECKBOX_STATE_EVENT_UNSELECTED)
	{
		tf_psw->setTouchEnabled(false);
		isLockTable = false;
	}
}

RoomListLayout * RoomListLayout::create(int numberPeople)
{
// 	RoomListLayout * layer = new RoomListLayout;
// 	//if (layer && layer->init(numberPeople))
// 	{
// 		layer->autorelease();
// 		return layer;
// 	}
// 
// 	delete layer;
// 	layer = nullptr;
	return nullptr;
}

void RoomListLayout::SendUserRulePacket(bool isLockTable, std::string password)
{

}

void RoomListLayout::closeButtonTouch(cocos2d::Ref * ref, ui::TouchEventType eventType_)
{

}


