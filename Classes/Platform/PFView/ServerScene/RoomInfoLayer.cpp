#include "RoomInfoLayer.h"
#include "ServerScene.h"

USING_NS_CC;
using namespace ui;

RoomInfoLayer::RoomInfoLayer()
{
	m_type = BaseModeLayer::ModeLayerTypeRoomInfo;
}


RoomInfoLayer::~RoomInfoLayer()
{
}

bool RoomInfoLayer::init()
{
	if (!BaseLayer::initWithJsonFile("LobbyResources/RoomInfoLayer.json"))
	{
		return false;
	}

	return true;
}
