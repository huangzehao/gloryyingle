#ifndef PLATFORMHEADER_H_
#define PLATFORMHEADER_H_

// #include "Platform/PFView/BaseLayer.h"
// 
// #include "Platform/PFView/LoadScene/UpdatePackageLayer.h"
// 
// #include "Platform/PFView/LoginScene/LoginLayer.h"
// #include "Platform/PFView/LoginScene/LoginScene.h"
// #include "Platform/PFView/LoginScene/RegisterLayer.h"
// 
// #include "Platform/PFView/ModeScene/ConversionLayer.h"
// #include "Platform/PFView/ModeScene/ExchangeShopLayer.h"
// #include "Platform/PFView/ModeScene/GiveChoiceLayer.h"
// #include "Platform/PFView/ModeScene/LowProtectLayer.h"
// #include "Platform/PFView/ModeScene/ModeLayer.h"
// #include "Platform/PFView/ModeScene/ModeScene.h"
// #include "Platform/PFView/ModeScene/PayLayer.h"
// #include "Platform/PFView/ModeScene/PlatformBankDredge.h"
// #include "Platform/PFView/ModeScene/RanklistLayer.h"
// #include "Platform/PFView/ModeScene/SettingLayer.h"
// #include "Platform/PFView/ModeScene/SignInLayer.h"
// #include "Platform/PFView/ModeScene/TopHeadLayer.h"
// #include "Platform/PFView/ModeScene/UserInfoLayer.h"
// 
// #include "Platform/PFView/ServerListScene/ServerListLayer.h"
// #include "Platform/PFView/ServerListScene/ServerListScene.h"
// 
// #include "Platform/PFView/ServerScene/ServerScene.h"
// 
// 
// #include "Platform/PFKernel/CGPExchangeMission.h"
// #include "Platform/PFKernel/CGPFaceMission.h"
// #include "Platform/PFKernel/CGPIndividualMission.h"
// #include "Platform/PFKernel/CGPInsureMission.h"
// #include "Platform/PFKernel/CGPKefuMission.h"
// #include "Platform/PFKernel/CGPLowProtectMission.h"
// #include "Platform/PFKernel/CGPLoginMission.h"
// #include "Platform/PFKernel/CGPMachineMission.h"
// #include "Platform/PFKernel/CGPMessageMission.h"
// #include "Platform/PFKernel/CGPPasswordMission.h"
// #include "Platform/PFKernel/CGPPersonalInfoMission.h"
// #include "Platform/PFKernel/CGPSignInMission.h"
// #include "Platform/PFKernel/CGPTimeAwardMission.h"
// #include "Platform/PFKernel/CGPUnderWriteMission.h"
// #include "Platform/PFKernel/GPMission.h"
// 
// #include "Platform/PFDefine/GlobalConfig.h"
// 
#include "Platform/PFDefine/data/GlobalUnits.h"
#include "Platform/PFDefine/data/GlobalUserInfo.h"
#include "Platform/PFDefine/data/OptionParameter.h"
#include "Platform/PFDefine/data/ServerListData.h"

#include "Platform/PFDefine/df/Define.h"
#include "Platform/PFDefine/df/DF.h"
#include "Platform/PFDefine/df/Macro.h"
#include "Platform/PFDefine/df/Module.h"
#include "Platform/PFDefine/df/Packet.h"
#include "Platform/PFDefine/df/Platform.h"
#include "Platform/PFDefine/df/Property.h"
#include "Platform/PFDefine/df/RightDefine.h"
#include "Platform/PFDefine/df/ServerRule.h"
#include "Platform/PFDefine/df/ServerRule.h"
#include "Platform/PFDefine/df/Struct.h"
#include "Platform/PFDefine/df/types.h"

#include "cocos2d.h"
#define PLAZZ_PRINTF cocos2d::log

#include "Platform/PFDefine/msg/CMD_Commom.h"
#include "Platform/PFDefine/msg/CMD_GameServer.h"
#include "Platform/PFDefine/msg/CMD_LogonServer.h"

#endif
