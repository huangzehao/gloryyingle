#include "Tools/tools/MTNotification.h"
#include "PlayerNode_zhajinhua.h"
#include "Tools/tools/SimpleTools.h"
#include "Tools/Manager/SpriteHelper.h"
#include "Tools/tools/YRCommon.h"

#define  P_WIDTH      kRevolutionWidth
#define  P_WIDTH_2    P_WIDTH/2

#define  P_HEIGHT    kRevolutionHeight
#define  P_HEIGHT_2    P_HEIGHT/2

const int PLAYER_POSTION[][2] = {
	//{ 710, 680 },
	//{ 1170, 650 },
// 	{ 1320, 400 },
// 	{ 1220, 150 },
// 	{ 710, 70 + 60 },
// 	{ 200, 150 },
// 	{ 100, 400 },
	//{ 250, 650 },{ 710, 130 },

	{ 1220, 530 },
	{ 1220, 305 },
	{ 315, 70 },
	{ 200, 305 },
	{ 200, 530 },
};
const int INFO_POSTION[][2] = {
	//{ 840, 660 },
	//{ 1300, 630 },
// 	{ 1350, 300 },
// 	{ 1350, 130 },
// 	{ 840, 110 },
// 	{ 70, 130 },
// 	{ 70, 300 },
	//{ 120, 630 },{ 840, 110 },

	{ 1350, 510 },
	{ 1350, 285 },
	{ 445, 50 },
	{ 70, 285 },
	{ 70, 510 },
};

PlayerNode_zhajinhua::PlayerNode_zhajinhua()
{
}


PlayerNode_zhajinhua::~PlayerNode_zhajinhua()
{

}

bool PlayerNode_zhajinhua::init(int chair_id, Widget *mRoot)
{
	if (!Node::init())
	{
		return false;
	}
	//��ʼ�����Ӻ�
	mChairId = chair_id;
	img_PlayerNode = dynamic_cast<ImageView *>(mRoot->getChildByName(StringUtils::format("PlayerNode_%d", mChairId)));
	img_PlayerNode->setVisible(false);
/*	img_compare = dynamic_cast<ImageView *>(mRoot->getChildByName("Panel_Main")->getChildByName("img_compare"));*/
	img_head = dynamic_cast<ImageView *>(img_PlayerNode->getChildByName("img_headBg")->getChildByName("img_head"));
	lab_name = dynamic_cast<Text *>(img_PlayerNode->getChildByName("lab_name"));
	lab_money = dynamic_cast<TextAtlas *>(img_PlayerNode->getChildByName("al_score"));
	img_jinbi_kuang = dynamic_cast<ImageView *>(img_PlayerNode->getChildByName("img_jinbi_kuang"));
//	img_jinbi_kuang->setVisible(true);
	lable_score = dynamic_cast<Text *>(img_jinbi_kuang->getChildByName("lab_score"));
//	lable_score->setVisible(false);
	img_prepare = dynamic_cast<ImageView *>(img_PlayerNode->getChildByName("img_prepare"));

	img_arrow = ImageView::create("zhajinhuaGameScene/zhajinhua/zjh-fj-bp01.png");
	img_flash_head = Sprite::create("zhajinhuaGameScene/zhajinhua/img_flash_headbg_1.png");
	Animation * animation = Animation::create();
	animation->addSpriteFrameWithFile("zhajinhuaGameScene/zhajinhua/img_flash_headbg_0.png");
	animation->addSpriteFrameWithFile("zhajinhuaGameScene/zhajinhua/img_flash_headbg_1.png");
	animation->addSpriteFrameWithFile("zhajinhuaGameScene/zhajinhua/img_flash_headbg_2.png");
	animation->setDelayPerUnit(0.2f);
	Animate * animate = Animate::create(animation);
	img_flash_head->runAction(RepeatForever::create(animate));
	img_flash_head->setAnchorPoint(ccp(0, 0));
	img_PlayerNode->addChild(img_flash_head, 1);
	img_arrow->setAnchorPoint(ccp(-0.5,-1.7));
	img_PlayerNode->addChild(img_arrow,1);
// 	img_head_bg->addChild(img_flash_head, 1);
// 
	btn_head = ui::Button::create("zhajinhuaGameScene/zhajinhua/img_flash_headbg_0.png", "zhajinhuaGameScene/zhajinhua/img_flash_headbg_0.png");
	btn_head->setAnchorPoint(ccp(0, 0));
	img_PlayerNode->addChild(btn_head);
//	img_head_bg->addChild(btn_head);
// 
// 	img_head_bg->setPosition(Vec2(PLAYER_POSTION[chair_id][0], PLAYER_POSTION[chair_id][1]));
// 	img_player_info->setPosition(Vec2(INFO_POSTION[chair_id][0], INFO_POSTION[chair_id][1]));
// 
 	btn_head->addTouchEventListener(this, ui::SEL_TouchEvent(&PlayerNode_zhajinhua::OnBtnHeadCellBack));
// 
	btn_head->setTag(chair_id);
	img_flash_head->setVisible(false);
	btn_head->setVisible(false);
	img_arrow->setVisible(false);
//   	img_head_bg->setVisible(false);
//   	img_player_info->setVisible(false);
	return true;
}

PlayerNode_zhajinhua * PlayerNode_zhajinhua::create(int chard_id, Widget * mRoot)
{
	PlayerNode_zhajinhua * node = new PlayerNode_zhajinhua;
	if (node && node->init(chard_id, mRoot))
	{
		node->autorelease();
		return node;
	}
	delete node;
	return nullptr;
}
void PlayerNode_zhajinhua::setFaceID(int wFaceID)
{
	img_head->loadTexture(YRComGetHeadImageById(wFaceID));
}

void PlayerNode_zhajinhua::setHeadInfo(bool isVisible)
{
	img_PlayerNode->setVisible(isVisible);
}

void PlayerNode_zhajinhua::setNickName(const char * name)
{
	lab_name->setString(name);
}

void PlayerNode_zhajinhua::setScore(long long score)
{
	total_money = score;
	lab_money->setString(StringUtils::format("%lld",total_money));
}

void PlayerNode_zhajinhua::setBuyMoney(long long money)
{
	std::string str_mon = StringUtils::format("%lld", money);
	lable_score->setString(str_mon);

	if (money < 0)
	{
		lable_score->setTextColor(Color4B::RED);
	}
}

void PlayerNode_zhajinhua::showState(int chair_id, int state)
{
	img_chat_kuang = Sprite::create("zhajinhuaGameScene/zhajinhua/img_chat_kuang.png");
	img_chat_kuang->setPosition(Vec2(PLAYER_POSTION[chair_id][0], PLAYER_POSTION[chair_id][1] + 90));
	this->addChild(img_chat_kuang);

	char stateStr[80];
	sprintf(stateStr, "zhajinhuaGameScene/zhajinhua/img_state_%d.png", state);
	img_chat_state = Sprite::create(stateStr);
	img_chat_state->setPosition(Vec2(52, 52));
	img_chat_kuang->addChild(img_chat_state);

	img_chat_kuang->runAction(CCSequence::create(
		DelayTime::create(1.5),
		CCCallFuncND::create(this, callfuncND_selector(PlayerNode_zhajinhua::clearState), 0),
		0));
}

void PlayerNode_zhajinhua::clearState(cocos2d::Node *target, void* data)
{
	img_chat_kuang->removeFromParent();
}

void PlayerNode_zhajinhua::showHeadButton(bool bShow)
{
 	img_flash_head->setVisible(bShow);
 	btn_head->setVisible(bShow);
	img_arrow->setVisible(bShow);
}

void PlayerNode_zhajinhua::clearUser(bool isLevel)
{
	if (isLevel)
	{
		img_PlayerNode->setVisible(false);
	}
}

ImageView * PlayerNode_zhajinhua::getHead()
{
	return img_head;
}

Text * PlayerNode_zhajinhua::getNickName()
{
	return lab_name;
}

TextAtlas * PlayerNode_zhajinhua::getScore()
{
	return lab_money;
}

void PlayerNode_zhajinhua::setIsPrepare(bool isPre)
{
	img_prepare->setVisible(isPre);
}

void PlayerNode_zhajinhua::setShowGoldKuang(bool isShow)
{
	img_jinbi_kuang->setVisible(isShow);
//	lable_score->setVisible(isShow);
}

void PlayerNode_zhajinhua::OnBtnHeadCellBack(Ref * ref, ui::TouchEventType eType)
{
	if (eType == ui::TouchEventType::TOUCH_EVENT_ENDED)
	{
		G_NOTIFY_D(ZJH_EVENT_HEAD, ref);
	}
	return;
}
// void PlayerNode_zhajinhua::setHeadAnimation(int mType)
// {
// 	char mTypeStr[80];
// 	sprintf(mTypeStr, "HeadAnimation_%d_%d", mFaceID, mType);
// 
// 	img_head->stopAllActions();
// 	img_head->runAction(CCSequence::create(
// 		SpriteHelper::createAnimate(mTypeStr),
// 		CCCallFunc::create(this, callfunc_selector(PlayerNode_zhajinhua::setHeadFreeAnimation)),
// 		0));
// }

// void PlayerNode_zhajinhua::setHeadFreeAnimation()
// {
// 	char mTypeStr[80];
// 	sprintf(mTypeStr, "HeadAnimation_%d_%d", mFaceID, 0);
// 
// 	img_head->stopAllActions();
// 	auto animate = SpriteHelper::createAnimate(mTypeStr);
// 	img_head->runAction(RepeatForever::create(animate));
// }

void PlayerNode_zhajinhua::addMoney(long long money)
{
	total_money += money;
	std::string str_mon = StringUtils::format("%lld", total_money);
	lab_money->setString(str_mon);
}