#ifndef FRAMELAYER_ZHAJINHUA_H
#define FRAMELAYER_ZHAJINHUA_H

#include "cocos2d.h"
#include "cocos-ext.h"
#include "ui/CocosGUI.h"
#include "ui/UITextAtlas.h"
#include "cocostudio/CocoStudio.h"
#include "common/SpriteCard.h"
#include "Platform/PFDefine/df/types.h"
#include "cmd_zhajinhua.h"
#include "ui/UIButton.h"
//#include "NumberScroller.h"

//按钮值
#define ZJH_VALUE_NULL			0
#define ZJH_VALUE_READY			1		//准备
#define ZJH_VALUE_ALLIN			2		//全押
#define ZJH_VALUE_COMPARE		4		//比牌
#define ZJH_VALUE_RAISE			8		//加注
#define ZJH_VALUE_SEE			16		//看牌
#define ZJH_VALUE_FLOD			32		//弃牌
#define ZJH_VALUE_FOLLOW		64		//跟注
#define ZJH_VALUE_FOLLOWALL		128		//跟到底
#define ZJH_VALUE_JETTON		256		//加注数面板
#define ZJH_VALUE_JETTON1		512		//加注数按钮
#define ZJH_VALUE_JETTON2		1024
#define ZJH_VALUE_JETTON3		2048
#define ZJH_VALUE_OPEN			4096	//开牌
//按钮事件
#define ZJH_EVENT_READY			"ZJH_EVENT_READY"
#define ZJH_EVENT_ALLIN			"ZJH_EVENT_ALLIN"
#define ZJH_EVENT_COMPARE		"ZJH_EVENT_COMPARE"
#define ZJH_EVENT_RAISE			"ZJH_EVENT_RAISE"
#define ZJH_EVENT_SEE			"ZJH_EVENT_SEE"
#define ZJH_EVENT_FLOD			"ZJH_EVENT_FLOD"
#define ZJH_EVENT_FOLLOW		"ZJH_EVENT_FOLLOW"
#define ZJH_EVENT_FOLLOWALL		"ZJH_EVENT_FOLLOWALL"
#define ZJH_EVENT_OPEN			"ZJH_EVENT_OPEN"

//发牌动画完成
#define ZJH_EVENT_DISPATCHFINISH	"ZJH_EVENT_DISPATCHFINISH"
//比牌结束
#define ZJH_EVENT_COMPAREFINISH		"ZJH_EVENT_COMPAREFINISH"

USING_NS_CC;
USING_NS_CC_EXT;
using namespace ui;
class FrameLayer_zhajinhua : public cocos2d::Layer
{
public:
	FrameLayer_zhajinhua();
	~FrameLayer_zhajinhua();

/*	CREATE_FUNC(FrameLayer_zhajinhua);*/

	//初始化
	bool init(Widget *mRoot);
	static FrameLayer_zhajinhua * create(Widget *mRoot);
	//创建主面板
	void createMainPanel(Widget * mRoot);
// 	//创建控制面板
// 	void creatControlPanel(Widget * mRoot);
	//设置退出回调
	void setCloseDialogInfo(cocos2d::Ref* target, cocos2d::SEL_CallFuncN callfun, const std::string& title, const std::string& content);

	//按钮事件
private:
	//退出游戏
	void OnBtnReturnCellBack(Ref * ref, TouchEventType eType);
	//准备
	void OnBtnReadyCellBack(Ref * ref, TouchEventType eType);
	//全押
	void OnBtnAllinCellBack(Ref * ref, TouchEventType eType);
	//比牌
	void OnBtnCompareCellBack(Ref * ref, TouchEventType eType);
	//加注
	void OnBtnRaiseCellBack(Ref * ref, TouchEventType eType);
	//加注关闭
	void OnButtonJettonClose(Ref * ref, TouchEventType eType);
	//看牌
	void OnBtnSeeCellBack(Ref * ref, TouchEventType eType);
	//弃牌
	void OnBtnFlodCellBack(Ref * ref, TouchEventType eType);
	//跟注
	void OnBenFollowCellBack(Ref * ref, TouchEventType eType);
	//跟到底
	void OnCheckFollowAllCellBack(Ref * ref, CheckBoxEventType eType);
	//加注
	void OnButtonJettonCellBack(Ref * ref, TouchEventType eType);
	//开牌
	void OnButtonOpenCellBack(Ref * ref, TouchEventType eType);
	//显示控制
public:
	//设置时钟
	void SetClockShow(word wChairID, uint nElapse, word wClockID);
	//时钟消息
private:
	//准备时间
	void OnTimeStartGame(float dt);
	//功能函数
public:
	//初始化控件
	void InitControl();
	//设置按钮显示
	void SetBtnShow(word nBtnValue);
	//设置按钮状态
	void SetBtnEnable(word nBtnValue);
	//分数限制提示 左上信息
	void SetScoreInfo(SCORE lCellScore, SCORE lMaxCellScore);
	//桌面筹码
	void SetUserTableScore(word wChairID, SCORE lTableScore, SCORE lCurrentScore, bool bGamePlay = false);
	//庄家标志
	void SetBankerUser(word wBankerUse);
	//等待选择
	void SetWaitUserChoice(word wChoice);
	//处理控制
	void UpdateControl();
	//获取信息
	bool GetCompareInfo(){ return m_bCompareCard; };
	//获取信息
	bool GetBFollowAll() { return mBFollowAll; };
	//用户状态
  //  void UpdateUserStats(word wChairID, byte cbUserStats);
	//void UpdateUserStats(IClientUserItem *pIClientUserItem, byte cbUserStats);
	
	//游戏中途进入 创建扑克
	void CreateUserCard(word wChairID, byte cbCardData, word wCardID);
	//发牌动画
	void DispatchUserCard(word wChairID, byte cbCardData, word wCardID);
	//发牌动画回调
	void DispatchCardCall(CCNode * node);
	//比牌动画
	void PerformCompareCard(word wCompareUser[2], word wLoserUser);
	//移动扑克回调
	void MoveCardCall(CCNode * node);
	//闪电回调
	void FlashCardCall(CCNode * node);
	//比牌动画回调
	void CompareCardCall(CCNode * node);
	//设置看牌状态
	void SetLookCard(word wChairID, bool bLook);
	//设置扑克
	void SetCardData(const byte cbCardData[], word wChairID, dword dwCardCount);
	//加注筹码动画
	void AddJettonMove(word wChairID, SCORE lScore);
	//胜利筹码动画
	void WinJettonMove(word wChairID);
	//清理筹码回调
	void ClearJettonCall(CCNode * node);
	//界面结算
	void SetGameEndScore(word wChairID, SCORE lScore);
	//开始闪牌动画
	void StartFalshCard(word wFalshUser[]);
	//闪牌动画
	void FalshCard(float dt);
	//停止闪动
	void StopFalshCard();
	//压缩筹码
	void CompactJetIndexCell(float dt);
private:
	//位置变量
	Point					mPtPerson[GAME_PLAYER];						//人物位置
	Point					mPtBanker[GAME_PLAYER];						//庄家标识位置
	Point					mPtAddScore[GAME_PLAYER];					//加注分数位置
	Point					mPtWinLostScore[GAME_PLAYER];				//输赢分数位置
	Point					mPtClock[GAME_PLAYER];						//时钟位置
	Point					mPtSendCard;								//发牌位置
	Point					mPtCard[GAME_PLAYER];						//牌位置
	Point					mPtUserCpmpare;								//比牌位置
	Point					mPtUserJetton[GAME_PLAYER];					//玩家筹码开始、结束位置
	Point					mPtAllJetton;								//下注筹码位置
	//控件变量
	Widget *				mPanelMain;									//主面板
	ImageView * Image_bg;
	Widget *				mPanelJetton;								//加注面板
	ImageView *				mClockTime;									//时钟
	ImageView *				mScoreTopBack;								//分数限制提示
	ImageView *				mUserReady[GAME_PLAYER];					//准备图片
	ImageView *				mImgBanker;									//庄家标识
	std::vector<Sprite *>	mJettonVector;								//筹码容器
	ImageView *				mLabAddScoreBG[GAME_PLAYER];				//玩家加注背景
	ImageView *				mImgWinLostScore[GAME_PLAYER];				//输赢分数

	//按钮控件
	Button *				mBtnReturn;									//退出游戏
	Button *				mBtnStart;									//准备
//	Button *				mBtnAllin;									//全押
	Button *				mBtnCompare;								//比牌
	// 	比牌信息图片
	ImageView * img_compare;
	Button *				mBtnRaise;									//加注
	Button *				mBtnSee;									//看牌
	Button *				mBtnFlod;									//弃牌
	Button *				mBtnFollow;									//跟注
//	Button *				mBtnOpen;									//开牌
	Button *                mBtnJetton_close;                           //关闭加注界面按钮
	Button *				mBtnJetton_1;								//加注按钮
	Button *				mBtnJetton_2;
	Button *				mBtnJetton_3;
	CheckBox *				mCheckFollowAll;							//跟到底

	//游戏变量
	word					mWClockTime;								//倒计时时间
	bool					mBFollowAll;								//跟到底
	word					m_wCompareUser[2];							//比牌用户
	word					m_wLostUser;								//失败用户
	word					m_wFlashUser[GAME_PLAYER];					//闪牌用户

	//退出变量
	cocos2d::Ref*			mTarget;
	cocos2d::SEL_CallFuncN	mCallback;
	std::string				mTitle;
	std::string				mContent;

	//数据变量
	SCORE					m_lStopUpdataScore[GAME_PLAYER];			//保持数目
	bool					m_bCompareCard;								//选比标志
	word					m_wWaitUserChoice;							//等待标志
	SCORE					m_lAddScoreBtn[3];							//加注按钮对应数值

	//筹码压缩
	SCORE					m_lTableScoreAll;							//桌面总下注

public:
	//牌
	CSpriteCard				m_Card;
	CSpriteCard	*			mCardList[GAME_PLAYER][MAX_COUNT];

	//动画标识
	word m_wActionCount;						//动画数量
};

#endif