#ifndef _ClientKernelSink_ZHAJINHUA_H_
#define _ClientKernelSink_ZHAJINHUA_H_

#include "cocos2d.h"
#include "Kernel/kernel/game/IClientKernelSink.h"
#include "Kernel/kernel/game/IClientKernel.h"
#include "GameScene_zhajinhua.h"

USING_NS_CC;

#define  GAME_PLAYER_ZHAJINHUA   5
class ClientKernelSink_zhajinhua
	: public Ref
	, public IClientKernelSink
{
public:
	ClientKernelSink_zhajinhua();
	~ClientKernelSink_zhajinhua();

	static ClientKernelSink_zhajinhua * getInstance()
	{
		static ClientKernelSink_zhajinhua * gClientKernelSink = new ClientKernelSink_zhajinhua();
		return gClientKernelSink;
	}

 	//控制接口
 public:
 	//启动游戏
 	virtual bool SetupGameClient();
 	//重置游戏
 	virtual void ResetGameClient();
 	//关闭游戏
 	virtual void CloseGameClient(int exit_tag = 0);
 
 	//框架事件
 public:
 	//系统滚动消息
 	virtual bool OnGFTableMessage(const char* szMessage);
 	//全局消息
 	virtual bool OnGFGlobalMessage(const char* szMessage);
 	//等待提示
 	virtual bool OnGFWaitTips(bool bWait);
 	//比赛信息
 	virtual bool OnGFMatchInfo(tagMatchInfo* pMatchInfo);
 	//比赛等待提示
 	virtual bool OnGFMatchWaitTips(tagMatchWaitTip* pMatchWaitTip);
 	//比赛结果
 	virtual bool OnGFMatchResult(tagMatchResult* pMatchResult);
 
 	//游戏事件
 public:
 	//旁观消息
 	virtual bool OnEventLookonMode(void* data, int dataSize);
 	//场景消息
 	virtual bool OnEventSceneMessage(byte cbGameStatus, bool bLookonUser, void* data, int dataSize);
 	//游戏消息
 	virtual bool OnEventGameMessage(int sub, void* data, int dataSize);
 
 	//时钟事件
 public:
 	//用户时钟
 	virtual void OnEventUserClock(word wChairID, word wUserClock);
 	//时钟删除
 	virtual bool OnEventGameClockKill(word wChairID);
 	//时钟信息
 	virtual bool OnEventGameClockInfo(word wChairID, uint nElapse, word wClockID);
 
 	//用户事件
 public:
 	//用户进入
 	virtual void OnEventUserEnter(IClientUserItem * pIClientUserItem, bool bLookonUser);
 	//用户离开
 	virtual void OnEventUserLeave(IClientUserItem * pIClientUserItem, bool bLookonUser);
 	//用户积分
 	virtual void OnEventUserScore(IClientUserItem * pIClientUserItem, bool bLookonUser);
 	//用户状态
 	virtual void OnEventUserStatus(IClientUserItem * pIClientUserItem, bool bLookonUser);
 	//用户属性
 	virtual void OnEventUserAttrib(IClientUserItem * pIClientUserItem, bool bLookonUser);
 	//用户头像
 	virtual void OnEventCustomFace(IClientUserItem * pIClientUserItem, bool bLookonUser);
 
 	//时间消息
	void OnTimer(uint nIDEvent);

	//消息处理
private:
	//*************场景消息
	//游戏状态
	bool OnEventSceneFree(const void* data, int dataSize);
	bool OnEventScenePlay(const void* data, int dataSize);
	//*************游戏消息
	//游戏开始
	bool OnSubGameStart(const void* pBuffer, int wDataSize);
	//用户放弃
	bool OnSubGiveUp(const void * pBuffer, int wDataSize);
	//用户加注
	bool OnSubAddScore(const void * pBuffer, int wDataSize);
	//用户看牌
	bool OnSubLookCard(const void * pBuffer, int wDataSize);
	//用户比牌
	bool OnSubCompareCard(const void * pBuffer, int wDataSize);
	//用户开牌
	bool OnSubOpenCard(const void * pBuffer, int wDataSize);
	//用户强退
	bool OnSubPlayerExit(const void * pBuffer, int wDataSize);
	//游戏结束
	bool OnSubGameEnd(const void * pBuffer, int wDataSize);

	//消息映射
	//准备
	void onEventReday(Ref* obj);
	//全押
	void onEventAllin(Ref* obj);
	//比牌
	void onEventCompare(Ref* obj);
	//开牌
	void OnEventOpen(Ref* obj);
	//加注
	void onEventRaise(Ref* obj);
	//看牌
	void onEvnetSee(Ref* obj);
	//弃牌
	void onEventFlod(Ref* obj);
	//跟注
	void onEvnetFollow(Ref* obj);
	//跟到底
	void onEvnetFollowAll(Ref* obj);
	//发牌动画完成
	void onEventDispatchfinish(Ref* obj);
	//头像点击
	void onEventHead(Ref * obj);
	//比牌完成
	void onEventCompareFinish(Ref * obj);

	virtual void CloseGameDelayClient();

private:
	//游戏场景
	GameScene_zhajinhua* m_pGameScene;
public:
	//游戏变量
	word					m_wCurrentUser;								//当前用户
	word					m_wBankerUser;								//庄家用户

	//用户状态
	byte					m_cbPlayStatus[GAME_PLAYER];				//游戏状态
	SCORE					m_lTableScore[GAME_PLAYER];					//下注数目

	//扑克变量
	byte					m_cbHandCardData[GAME_PLAYER][MAX_COUNT];	//桌面扑克

	//下注信息
	SCORE					m_lMaxScore;								//封顶数目
	SCORE					m_lMaxCellScore;							//单元上限
	SCORE					m_lCellScore;								//单元下注
	SCORE					m_lCurrentTimes;							//当前倍数
	SCORE					m_lUserMaxScore;							//最大分数
	bool					m_bMingZhu[GAME_PLAYER];					//看牌动作

	//输牌用户
	word					m_wLostUser;								//比牌失败
	word					m_wWinnerUser;								//胜利用户

	//动画信息
	word					m_wLostUserID[GAME_PLAYER];					//比败用户

	//椅子信息
	word					m_wViewChairID[GAME_PLAYER];				//玩家椅子

	//配置变量
	dword					m_dwCardHSpace;								//扑克象素
};

#endif

