  #include "Tools/tools/MTNotification.h"
#include "Tools/Manager/SoundManager.h"
#include "ClientKernelSink_zhajinhua.h"
#include "Kernel/kernel/server/IServerItemSink.h"
#include "Tools/tools/StringData.h"
#include "Tools/Dialog/Timer.h"
#include "Kernel/kernel/server/IServerItem.h"
#include "Tools/Dialog/NewDialog.h"

#include "cmd_zhajinhua.h"

USING_NS_CC;
using namespace zhajinhua;

ClientKernelSink_zhajinhua::ClientKernelSink_zhajinhua()
{
	m_pGameScene = nullptr;
	
	//注册事件
	G_NOTIFY_REG(ZJH_EVENT_READY, ClientKernelSink_zhajinhua::onEventReday);
	G_NOTIFY_REG(ZJH_EVENT_ALLIN, ClientKernelSink_zhajinhua::onEventAllin);
	G_NOTIFY_REG(ZJH_EVENT_COMPARE, ClientKernelSink_zhajinhua::onEventCompare);
	G_NOTIFY_REG(ZJH_EVENT_RAISE, ClientKernelSink_zhajinhua::onEventRaise);
	G_NOTIFY_REG(ZJH_EVENT_SEE, ClientKernelSink_zhajinhua::onEvnetSee);
	G_NOTIFY_REG(ZJH_EVENT_FLOD, ClientKernelSink_zhajinhua::onEventFlod);
	G_NOTIFY_REG(ZJH_EVENT_FOLLOW, ClientKernelSink_zhajinhua::onEvnetFollow);
	G_NOTIFY_REG(ZJH_EVENT_FOLLOWALL, ClientKernelSink_zhajinhua::onEvnetFollowAll);
	G_NOTIFY_REG(ZJH_EVENT_DISPATCHFINISH, ClientKernelSink_zhajinhua::onEventDispatchfinish);
	G_NOTIFY_REG(ZJH_EVENT_HEAD, ClientKernelSink_zhajinhua::onEventHead);
	G_NOTIFY_REG(ZJH_EVENT_COMPAREFINISH, ClientKernelSink_zhajinhua::onEventCompareFinish);
	G_NOTIFY_REG(ZJH_EVENT_OPEN, ClientKernelSink_zhajinhua::OnEventOpen);

	//用户信息
	m_wCurrentUser = INVALID_CHAIR;
	m_wBankerUser = INVALID_CHAIR;
	m_wWinnerUser = INVALID_CHAIR;

	//加注信息
	m_lMaxScore = 0L;
	m_lMaxCellScore = 0L;
	m_lCellScore = 0L;
	m_lCurrentTimes = 1L;
	m_lUserMaxScore = 0L;
	zeromemory(m_cbHandCardData, sizeof(m_cbHandCardData));
	zeromemory(m_lTableScore, sizeof(m_lTableScore));
	//椅子信息
	for (int i = 0; i < GAME_PLAYER; i++)
	{
		m_wLostUserID[i] = INVALID_CHAIR;
		m_wViewChairID[i] = INVALID_CHAIR;
		m_bMingZhu[i] = false;
	}
}


ClientKernelSink_zhajinhua::~ClientKernelSink_zhajinhua()
{
	m_pGameScene = nullptr;

	//销毁事件
	G_NOTIFY_UNREG(ZJH_EVENT_READY);
	G_NOTIFY_UNREG(ZJH_EVENT_ALLIN);
	G_NOTIFY_UNREG(ZJH_EVENT_COMPARE);
	G_NOTIFY_UNREG(ZJH_EVENT_RAISE);
	G_NOTIFY_UNREG(ZJH_EVENT_SEE);
	G_NOTIFY_UNREG(ZJH_EVENT_FLOD);
	G_NOTIFY_UNREG(ZJH_EVENT_FOLLOW);
	G_NOTIFY_UNREG(ZJH_EVENT_FOLLOWALL);
	G_NOTIFY_UNREG(ZJH_EVENT_DISPATCHFINISH);
	G_NOTIFY_UNREG(ZJH_EVENT_HEAD);
}

//启动游戏场景
bool ClientKernelSink_zhajinhua::SetupGameClient()
{
	PLAZZ_PRINTF("flow->ClientKernelSink::SetupGameClient\n");

	//创建场景
	Director * director = Director::getInstance();
	Scene * pScene = director->getRunningScene();
	if (pScene->getTag() == KIND_ID && pScene->getName() == "GameScene")
	{
		m_pGameScene = dynamic_cast<GameScene_zhajinhua *>(pScene);
	}
	else
	{
		if (!m_pGameScene)
			m_pGameScene = GameScene_zhajinhua::create();
	}

	//进入场景
	if (pScene != m_pGameScene)
		director->pushScene(CCTransitionFade::create(0.5f, m_pGameScene));
	else
	{
		if (IClientKernel::get())
		{
			IClientKernel::get()->SendGameOption();
		}
	}

	return false;
}

//重置游戏
void ClientKernelSink_zhajinhua::ResetGameClient()
{
}

//关闭游戏
void ClientKernelSink_zhajinhua::CloseGameClient(int exit_tag /*= 0*/)
{
	m_pGameScene = nullptr;

	if (!GameScene_zhajinhua::is_Reconnect_on_loss())
		G_NOTIFY_D("MODE_SELECTED", MTData::create(1));
}

//系统滚动消息
bool ClientKernelSink_zhajinhua::OnGFTableMessage(const char* szMessage)
{
	m_pGameScene->add_msg(szMessage);
	return true;
}

//全局消息
bool ClientKernelSink_zhajinhua::OnGFGlobalMessage(const char* szMessage)
{
// 	szGlobalMessage = szMessage;
// 	m_pGameScene->add_msg(szMessage);
	return true;
}

//等待提示
bool ClientKernelSink_zhajinhua::OnGFWaitTips(bool bWait)
{
	return true;
}

//比赛信息
bool ClientKernelSink_zhajinhua::OnGFMatchInfo(tagMatchInfo* pMatchInfo)
{
	return true;
}

//比赛等待提示
bool ClientKernelSink_zhajinhua::OnGFMatchWaitTips(tagMatchWaitTip* pMatchWaitTip)
{
	return true;
}

//比赛结果
bool ClientKernelSink_zhajinhua::OnGFMatchResult(tagMatchResult* pMatchResult)
{
	return true;
}
//////////////////////////////////////////////////////////////////////////
//游戏事件

//旁观消息
bool ClientKernelSink_zhajinhua::OnEventLookonMode(void* data, int dataSize)
{
	return true;
}

//场景消息
bool ClientKernelSink_zhajinhua::OnEventSceneMessage(byte cbGameStatus, bool bLookonUser, void* data, int dataSize)
{
	IClientKernel* kernel = IClientKernel::get();
	kernel->SetGameStatus(cbGameStatus);

	m_pGameScene->mFrameLayer->InitControl();

 	switch (cbGameStatus)
 	{
		case GAME_STATUS_FREE:		//空闲状态
 		{
										return OnEventSceneFree(data, dataSize);
 		}
		case GAME_STATUS_PLAY:		//游戏状态
 		{
										return OnEventScenePlay(data, dataSize);
 		}
 	}

	return false;
}

//游戏消息
bool ClientKernelSink_zhajinhua::OnEventGameMessage(int wSubCmdID, void* pData, int wDataSize)
{
 	switch (wSubCmdID)
 	{
		case SUB_S_GAME_START:		//游戏开始
 		{
										return OnSubGameStart(pData, wDataSize);
 		}
		case SUB_S_ADD_SCORE:		//用户下注
		{
										return OnSubAddScore(pData, wDataSize);
		}
		case SUB_S_LOOK_CARD:		//看牌消息
		{
										return OnSubLookCard(pData, wDataSize);
		}
		case SUB_S_COMPARE_CARD:	//比牌消息
		{
										return OnSubCompareCard(pData, wDataSize);
		}
		case SUB_S_OPEN_CARD:		//开牌消息
		{
										return OnSubOpenCard(pData, wDataSize);
		}
		case SUB_S_GIVE_UP:			//用户放弃
		{
										return OnSubGiveUp(pData, wDataSize);
		}
		case SUB_S_PLAYER_EXIT:		//用户强退
		{
										return OnSubPlayerExit(pData, wDataSize);
		}
		case SUB_S_GAME_END:		//游戏结束
		{
										return OnSubGameEnd(pData, wDataSize);
		}
		case SUB_S_WAIT_COMPARE:	//等待比牌
		{
										IClientKernel * kernel = IClientKernel::get();
										word wMeChairID = kernel->GetMeChairID();
										if (wMeChairID != m_wCurrentUser)
										{
											//设置时间
											kernel->SetGameClock(m_wCurrentUser, IDI_DISABLE, TIME_USER_COMPARE_CARD);

											//提示标识
											m_pGameScene->mFrameLayer->SetWaitUserChoice(TRUE);
										}

										return true;
		}
		case SUB_S_CHEAT_CARD:
		{
								 return true;
		}
 	}

	//过滤管理员消息
	if (wSubCmdID >= SUB_S_ADMIN_STORAGE_INFO && wSubCmdID <= SUB_S_DUPLICATE_USERROSTER)
		return true;

	//错误断言
	ASSERT(FALSE);

	return false;
}

//////////////////////////////////////////////////////////////////////////
//时钟事件
//用户时钟
void ClientKernelSink_zhajinhua::OnEventUserClock(word wChairID, word wUserClock)
{
}

//时钟删除
bool ClientKernelSink_zhajinhua::OnEventGameClockKill(word wChairID)
{
	m_pGameScene->mFrameLayer->SetClockShow(m_wViewChairID[wChairID], 0, 0);
	return true;
}

//时钟信息
bool ClientKernelSink_zhajinhua::OnEventGameClockInfo(word wChairID, uint nElapse, word wClockID)
{
	m_pGameScene->mFrameLayer->SetClockShow(m_wViewChairID[wChairID], nElapse, wClockID);

	IClientKernel * kernel = IClientKernel::get();

	switch (wClockID)
	{
		case IDI_START_GAME:			//等待开始
		{
											//警告通知
											if ((nElapse <= 5) && (kernel->IsLookonMode() == false))
												SoundManager::shared()->playSound("ZJH_GAME_WARN");
											break;
		}
		case IDI_USER_ADD_SCORE:		//加注
		{
											if (nElapse <= 0)
												G_NOTIFY(ZJH_EVENT_FLOD);

											if (wChairID == kernel->GetMeChairID() && m_pGameScene->mFrameLayer->GetBFollowAll() && nElapse <= 29)
											{
												G_NOTIFY(ZJH_EVENT_FOLLOW);
											}

											//警告通知
											if ((nElapse <= 5) && (kernel->GetMeChairID() == m_wCurrentUser) &&(kernel->IsLookonMode() == false))
												SoundManager::shared()->playSound("ZJH_GAME_WARN");

											break;
		}
		case IDI_USER_COMPARE_CARD:		//等待比牌
		{
											//中止判断
											if (nElapse == 0)
											{
												//删除定时器
												kernel->KillGameClock(IDI_USER_COMPARE_CARD);
												word wMeChairID = kernel->GetMeChairID();

												//设置状态
												PlayerNode_zhajinhua * node = nullptr;
												for (word i = 0; i < GAME_PLAYER; i++)
												{
													//设置头像点击
													node = m_pGameScene->mPlayerLayer->getPlayerNodeById(m_wViewChairID[i]);
													node->showHeadButton(false);
												}

												//构造变量
												CMD_C_CompareCard CompareCard;
												zeromemory(&CompareCard, sizeof(CompareCard));

												//查找上家
												for (int i = wMeChairID - 1;; i--)
												{
													if (i == -1)i = GAME_PLAYER - 1;
													if (m_cbPlayStatus[i] == TRUE)
													{
														CompareCard.wCompareUser = (word)i;
														break;
													}
												}

												//发送消息
												kernel->SendSocketData(SUB_C_COMPARE_CARD, &CompareCard, sizeof(CompareCard));

												return false;
											}

											//警告通知
											if ((nElapse <= 5) && (kernel->GetMeChairID() == m_wCurrentUser) && (kernel->IsLookonMode() == false))
												SoundManager::shared()->playSound("ZJH_GAME_WARN");

											break;
		}
		case IDI_DISABLE:
		{
							if (nElapse == 0)
							{
								//删除定时器
								kernel->KillGameClock(IDI_DISABLE);
							}
							break;
		}
		default:
		break;
	}
	return true;
}

//用户进入
void ClientKernelSink_zhajinhua::OnEventUserEnter(IClientUserItem * pIClientUserItem, bool bLookonUser)
{
	Director * director = Director::getInstance();
 	Scene * pScene = director->getRunningScene();
 	if (pScene->getTag() == KIND_ID && pScene->getName() == "GameScene")
 	{
 		///< 表示是自己!!
		m_pGameScene = dynamic_cast<GameScene_zhajinhua *>(pScene);
 	}
	else if (!m_pGameScene)
	{
		m_pGameScene = GameScene_zhajinhua::create();
	}

	if (!bLookonUser && m_pGameScene)
	{
		m_pGameScene->showUserInfo(pIClientUserItem, pIClientUserItem->GetUserInfo()->cbUserStatus);
	}
}
//用户离开
void ClientKernelSink_zhajinhua::OnEventUserLeave(IClientUserItem * pIClientUserItem, bool bLookonUser)
{
 	if (!bLookonUser)
 	{
 		///< 是不是观看用户
 		word wChairID = pIClientUserItem->GetChairID();
 		IClientKernel* kernel = IClientKernel::get();
 		if (kernel->GetMeChairID() != INVALID_CHAIR && kernel->GetMeChairID() != wChairID)
 		{
 			///< 其他用户离开
			m_pGameScene->netUserLevel(wChairID);
			m_pGameScene->clearCardData(wChairID);
			m_pGameScene->showScoreTips(wChairID);
 		}
 		else if (m_pGameScene)
 		{
 			// 			if (IClientKernel::get())
 			// 				IClientKernel::get()->Intermit(GameExitCode_Normal);
 
 			G_NOTIFY_D("EVENT_GAME_EXIT", MTData::create());
 		}
 	}
}

//用户积分
void ClientKernelSink_zhajinhua::OnEventUserScore(IClientUserItem * pIClientUserItem, bool bLookonUser)
{
	if (!bLookonUser && m_pGameScene)
		G_NOTIFY_D("EVENT_USER_SCORE", MTData::create(0, 0, 0, "", "", "", pIClientUserItem));
}

//用户状态
void ClientKernelSink_zhajinhua::OnEventUserStatus(IClientUserItem * pIClientUserItem, bool bLookonUser)
{
	if (!bLookonUser)
		G_NOTIFY_D("EVENT_USER_STATUS", MTData::create((unsigned int)pIClientUserItem));

 	IClientKernel* kernel = IClientKernel::get();
	if (m_pGameScene)
	{
		//US_READY
		m_pGameScene->net_user_state_update(pIClientUserItem->GetChairID(), pIClientUserItem->GetUserStatus());
 	}
}

//用户属性
void ClientKernelSink_zhajinhua::OnEventUserAttrib(IClientUserItem * pIClientUserItem, bool bLookonUser)
{
}

//用户头像
void ClientKernelSink_zhajinhua::OnEventCustomFace(IClientUserItem * pIClientUserItem, bool bLookonUser)
{
}

void ClientKernelSink_zhajinhua::OnTimer(uint nIDEvent)
{
	IClientKernel * kernel = IClientKernel::get();
	if (kernel == nullptr)
		return;
}
//空闲状态
bool ClientKernelSink_zhajinhua::OnEventSceneFree(const void* data, int dataSize)
{
	//校验数据
	if (dataSize != sizeof(CMD_S_StatusFree)) return false;
	CMD_S_StatusFree * pStatusFree = (CMD_S_StatusFree*)data;

	//转换位置
	IClientKernel * kernel = IClientKernel::get();
	for (word i = 0; i < GAME_PLAYER; i++)
		m_wViewChairID[i] = kernel->SwitchViewChairID(i);

	//设置控件
	if (kernel->IsAllowLookon() == false && kernel->GetMeUserItem()->GetUserStatus() != US_READY)
	{
		kernel->SetGameClock(kernel->GetMeChairID(), IDI_START_GAME, TIME_START_GAME);
		m_pGameScene->mFrameLayer->SetBtnShow(ZJH_VALUE_READY);
	}

	return true;
}
//游戏状态
bool ClientKernelSink_zhajinhua::OnEventScenePlay(const void* data, int dataSize)
{
	//校验数据
	if (dataSize != sizeof(CMD_S_StatusPlay)) return false;
	CMD_S_StatusPlay * pStatusPlay = (CMD_S_StatusPlay*)data;

	//转换位置
	IClientKernel * kernel = IClientKernel::get();
	for (word i = 0; i < GAME_PLAYER; i++)
		m_wViewChairID[i] = kernel->SwitchViewChairID(i);


	word wMeChairID = kernel->GetMeChairID();

	//加注信息
	m_lMaxCellScore = pStatusPlay->lMaxCellScore;
	m_lCellScore = pStatusPlay->lCellScore;
	m_lCurrentTimes = pStatusPlay->lCurrentTimes;
	m_lUserMaxScore = pStatusPlay->lUserMaxScore;

	//设置变量
	m_wBankerUser = pStatusPlay->wBankerUser;
	m_wCurrentUser = pStatusPlay->wCurrentUser;
	memcpy(m_bMingZhu, pStatusPlay->bMingZhu, sizeof(pStatusPlay->bMingZhu));
	memcpy(m_lTableScore, pStatusPlay->lTableScore, sizeof(pStatusPlay->lTableScore));
	memcpy(m_cbPlayStatus, pStatusPlay->cbPlayStatus, sizeof(pStatusPlay->cbPlayStatus));

	//设置扑克
	memcpy(&m_cbHandCardData[wMeChairID], &pStatusPlay->cbHandCardData, MAX_COUNT);

	//庄家标志
	word wID = m_wViewChairID[m_wBankerUser];
	m_pGameScene->mFrameLayer->SetBankerUser(wID);

	//左上信息
	m_pGameScene->mFrameLayer->SetScoreInfo(m_lCellScore, m_lMaxCellScore);

	//设置变量
	for (word i = 0; i<GAME_PLAYER; i++)
	{
		if (m_lTableScore[i]>0L)
		{
			m_pGameScene->mFrameLayer->SetUserTableScore(m_wViewChairID[i], m_lTableScore[i], m_lTableScore[i], true);
			m_pGameScene->mFrameLayer->AddJettonMove(m_wViewChairID[i], m_lTableScore[i]);
		}

		//获取用户
		IClientUserItem * pClientUserItem = kernel->GetTableUserItem(i);
		if (pClientUserItem == NULL) continue;
	}

	for (word i = 0; i < GAME_PLAYER; i++)
	{
		if (m_cbPlayStatus[i] == TRUE)
		{
			CSpriteCard::setAutoScaleSize(1.4);
			m_pGameScene->mFrameLayer->m_Card.setCardArrayWithRow("zhajinhuaGameScene/zhajinhua/img_poker_card.png", CSpriteCard::g_cbCardData, 59);
			break;
		}
	}

	//设置界面
	for (word i = 0; i < GAME_PLAYER; i++)
	{
		//设置位置
		word wViewChairID = m_wViewChairID[i];

		//设置扑克
		if (m_cbPlayStatus[i] == TRUE)
		{
			for (word wCard = 0; wCard < MAX_COUNT; wCard++)
			{
				m_pGameScene->mFrameLayer->CreateUserCard(wViewChairID, 0, wCard);
			}
			//m_GameClientView.m_CardControl[wViewChairID].SetCardData(m_cbHandCardData[i], MAX_COUNT);
			if ((i != wMeChairID || kernel->IsLookonMode()) && m_bMingZhu[i])
			{
				m_pGameScene->mFrameLayer->SetLookCard(wViewChairID, true);
			}
		}
	}

	if (m_bMingZhu[wMeChairID] && (!kernel->IsLookonMode() || kernel->IsAllowLookon()))
	{
		m_pGameScene->mFrameLayer->SetCardData(m_cbHandCardData[m_wViewChairID[wMeChairID]], m_wViewChairID[wMeChairID], MAX_COUNT);
	}

	//控件处理
	m_pGameScene->mFrameLayer->SetBtnShow(ZJH_VALUE_NULL);

	//判断控件
	if (m_wCurrentUser < GAME_PLAYER)
	{
		if (!(pStatusPlay->bCompareState))
		{
			//控件信息
			if (!kernel->IsLookonMode() && wMeChairID == m_wCurrentUser)
			{
				m_pGameScene->mFrameLayer->UpdateControl();
			}

			//设置时间
			kernel->SetGameClock(m_wCurrentUser, IDI_USER_ADD_SCORE, TIME_USER_ADD_SCORE);
		}
		else
		{
			if (!kernel->IsLookonMode() && wMeChairID == m_wCurrentUser)
			{
				//选择玩家比牌
				bool bCompareUser[GAME_PLAYER];
				for (word i = 0; i < GAME_PLAYER; i++)
				{
					if (m_cbPlayStatus[i] && wMeChairID != i)
						bCompareUser[m_wViewChairID[i]] = true;
					else
						bCompareUser[m_wViewChairID[i]] = false;
				}

				//设置比牌箭头

				//等待比牌
				kernel->SendSocketData(SUB_C_WAIT_COMPARE, nullptr, 0);
				
				//设置时间
				kernel->SetGameClock(wMeChairID, IDI_USER_COMPARE_CARD, TIME_USER_COMPARE_CARD);

				//提示标识
				m_pGameScene->mFrameLayer->SetWaitUserChoice(FALSE);
			}
			else
			{
				//设置比牌

				//设置时间
				kernel->SetGameClock(m_wCurrentUser, IDI_DISABLE, TIME_USER_COMPARE_CARD);

				//提示标识
				m_pGameScene->mFrameLayer->SetWaitUserChoice(TRUE);
			}
		}
	}

	return true;
}
//游戏开始
bool ClientKernelSink_zhajinhua::OnSubGameStart(const void * pBuffer, int wDataSize)
{
	if (wDataSize != sizeof(CMD_S_GameStart)) return false;
	CMD_S_GameStart* pGameStart = (CMD_S_GameStart*)pBuffer;

	IClientKernel* kernel = IClientKernel::get();
	//隐藏准备
	m_pGameScene->clearPlayerPrepare();
	//旁观界面
	if (kernel->IsLookonMode()) onEventReday(nullptr);

	kernel->SetGameStatus(GAME_STATUS_PLAY);

	//数据信息
	m_lCellScore = pGameStart->lCellScore;
	m_lMaxCellScore = pGameStart->lMaxScore;
	m_lCurrentTimes = pGameStart->lCurrentTimes;
	m_wCurrentUser = pGameStart->wCurrentUser;
	m_wBankerUser = pGameStart->wBankerUser;
	m_lUserMaxScore = pGameStart->lUserMaxScore;

	//界面设置
	m_pGameScene->mFrameLayer->SetBankerUser(m_wViewChairID[m_wBankerUser]);
	m_pGameScene->mFrameLayer->SetScoreInfo(m_lCellScore, m_lMaxCellScore);

	//设置变量
	SCORE lTableScore = 0L;
	for (word i = 0; i < GAME_PLAYER; i++)
	{
		m_wLostUserID[i] = INVALID_CHAIR;
		m_wViewChairID[i] = kernel->SwitchViewChairID(i);
		m_cbPlayStatus[i] = FALSE;

		//获取用户
		IClientUserItem * pClientUserItem = kernel->GetTableUserItem(i);
		if (pClientUserItem == nullptr) continue;

		//游戏信息		
		m_cbPlayStatus[i] = TRUE;  
		lTableScore += m_lCellScore;
		m_lTableScore[i] = m_lCellScore;
		m_pGameScene->mFrameLayer->SetUserTableScore(m_wViewChairID[i], m_lTableScore[i], m_lTableScore[i]);
		m_pGameScene->mFrameLayer->AddJettonMove(m_wViewChairID[i], m_lCellScore);
	}

	//派发扑克

	CSpriteCard::setAutoScaleSize(1.4);
	m_pGameScene->mFrameLayer->m_Card.setCardArrayWithRow("zhajinhuaGameScene/zhajinhua/img_poker_card.png", CSpriteCard::g_cbCardData, 59);

	for (word i = 0; i < MAX_COUNT; i++)
	{
		for (word j = m_wBankerUser; j < m_wBankerUser + GAME_PLAYER; j++)
		{
			word w = j%GAME_PLAYER;
			if (m_cbPlayStatus[w] == TRUE)
			{
				m_pGameScene->mFrameLayer->DispatchUserCard(m_wViewChairID[w], 0, i);
			}
		}
	}

	SoundManager::shared()->playSound("ZJH_GAME_START");

// 	////////发牌动画结束后的处理
// 	//设置时间
// 	if (m_wCurrentUser == INVALID_CHAIR)return 0;
// 	kernel->SetGameClock(m_wCurrentUser, IDI_USER_ADD_SCORE, TIME_USER_ADD_SCORE);
// 
// 	//过虑用户
// 	word wMeChairID = kernel->GetMeChairID();
// 	if (kernel->IsLookonMode() || wMeChairID != m_wCurrentUser)return true;
// 
// 	m_pGameScene->mFrameLayer->UpdateControl();

	return true;
}
//用户放弃
bool ClientKernelSink_zhajinhua::OnSubGiveUp(const void * pBuffer, int wDataSize)
{
	//效验数据
	if (wDataSize != sizeof(CMD_S_GiveUp)) return false;
	CMD_S_GiveUp * pGiveUp = (CMD_S_GiveUp *)pBuffer;

	IClientKernel* kernel = IClientKernel::get();

	//删除定时器
	if (kernel->IsLookonMode() || pGiveUp->wGiveUpUser != kernel->GetMeChairID()) kernel->KillGameClock(IDI_USER_ADD_SCORE);

	//旁观界面
	if (kernel->IsLookonMode()) onEventReday(nullptr);

	//设置变量
	m_cbPlayStatus[pGiveUp->wGiveUpUser] = FALSE;

	//变量定义
	word wGiveUpUser = pGiveUp->wGiveUpUser;
	word wViewChairID = m_wViewChairID[wGiveUpUser];

	//扑克变灰
	byte cbCardData[MAX_COUNT] = { 0x44, 0x44, 0x44 };
	m_pGameScene->mFrameLayer->SetCardData(cbCardData, wViewChairID, MAX_COUNT);

	//重新设置被选比牌用户
	if (m_wCurrentUser == kernel->GetMeChairID() && m_pGameScene->mFrameLayer->GetCompareInfo())
	{
		PlayerNode_zhajinhua * node = nullptr;
		for (int i = 0; i < GAME_PLAYER; i++)
		{
			//设置箭头
			if (m_cbPlayStatus[i] == TRUE && kernel->GetMeChairID() != i)
			{
				//设置头像点击
				node = m_pGameScene->mPlayerLayer->getPlayerNodeById(m_wViewChairID[i]);
				node->showHeadButton(true);
			}
		}
	}

	//状态设置
	if ((kernel->IsLookonMode() == false) && (pGiveUp->wGiveUpUser == kernel->GetMeChairID())) kernel->SetGameStatus(GAME_STATUS_FREE);

	//环境设置
	if ((kernel->IsLookonMode() == true) || (wGiveUpUser != kernel->GetMeChairID()))
	{
		SoundManager::shared()->playSound("ZJH_GIVE_UP");
	}

	IClientUserItem *pUserItem = kernel->GetTableUserItem(wGiveUpUser);
	if (pUserItem == nullptr) return true;
	bool bBoy = (pUserItem->GetFaceID() % 8 > 3 ? true : false);

	if (bBoy)
		SoundManager::shared()->playSound("ZJH_MAN_GIVEUP");
	else
		SoundManager::shared()->playSound("ZJH_WOMAN_GIVEUP");

	return true;
}
//用户加注
bool ClientKernelSink_zhajinhua::OnSubAddScore(const void * pBuffer, int wDataSize)
{
	//效验数据
	ASSERT(wDataSize == sizeof(CMD_S_AddScore));
	if (wDataSize != sizeof(CMD_S_AddScore)) return false;
	CMD_S_AddScore * pAddScore = (CMD_S_AddScore *)pBuffer;
	ASSERT(pAddScore->wCurrentUser < GAME_PLAYER);

	IClientKernel* kernel = IClientKernel::get();
	//删除定时器
	if (kernel->IsLookonMode() || pAddScore->wAddScoreUser != kernel->GetMeChairID())kernel->KillGameClock(IDI_USER_ADD_SCORE);

	//变量定义
	word wMeChairID = kernel->GetMeChairID();
	word wAddScoreUser = pAddScore->wAddScoreUser;
	word wViewChairID = m_wViewChairID[wAddScoreUser];
	int64 ago_lCurrentTimes = m_lCurrentTimes;

	m_lCurrentTimes = pAddScore->lCurrentTimes;
	ASSERT(m_lCurrentTimes <= m_lMaxCellScore / m_lCellScore);

	//加注处理
	if ((kernel->IsLookonMode() == true) || (wAddScoreUser != wMeChairID))
	{
		//下注金币
		m_lTableScore[pAddScore->wAddScoreUser] += pAddScore->lAddScoreCount;

		//加注界面
		m_pGameScene->mFrameLayer->SetUserTableScore(wViewChairID, m_lTableScore[pAddScore->wAddScoreUser], pAddScore->lAddScoreCount);
		//移动筹码
		m_pGameScene->mFrameLayer->AddJettonMove(wViewChairID, pAddScore->lAddScoreCount);

		IClientUserItem *pUserItem = kernel->GetTableUserItem(wAddScoreUser);
		if (pUserItem == nullptr) return true;
		bool bBoy = (pUserItem->GetFaceID() % 8 > 3 ? true : false);

		if (pAddScore->lAddScoreCount > 0)
		{
			//播放声音 by_zhen  
			if (m_cbPlayStatus[wAddScoreUser] == TRUE)
			{
				SoundManager::shared()->playSound("ZJH_ADD_SCORE");
			}

			if (ago_lCurrentTimes == m_lCurrentTimes)
			{
				if (bBoy)
					SoundManager::shared()->playSound("ZJH_MAN_FOLLOW");
				else
					SoundManager::shared()->playSound("ZJH_WOMAN_FOLLOW");
			}
			else
			{
				if (bBoy)
					SoundManager::shared()->playSound("ZJH_MAN_ADD");
				else
					SoundManager::shared()->playSound("ZJH_WOMAN_ADD");
			}
		}
	}

	//当前用户
	m_wCurrentUser = pAddScore->wCurrentUser;

	//控件信息
	if (pAddScore->wCompareState == FALSE && (!kernel->IsLookonMode()) && wMeChairID == m_wCurrentUser)
	{
		m_pGameScene->mFrameLayer->UpdateControl();
	}

	//设置时间
	if (pAddScore->wCompareState == FALSE)
	{
		kernel->SetGameClock(m_wCurrentUser, IDI_USER_ADD_SCORE, TIME_USER_ADD_SCORE);
	}

	return true;
}
//用户看牌
bool ClientKernelSink_zhajinhua::OnSubLookCard(const void * pBuffer, int wDataSize)
{
	//效验数据
	if (wDataSize != sizeof(CMD_S_LookCard)) return false;
	CMD_S_LookCard * pLookCard = (CMD_S_LookCard *)pBuffer;

	IClientKernel* kernel = IClientKernel::get();

	if (kernel->IsLookonMode() || m_wCurrentUser != kernel->GetMeChairID())
	{
		//删除定时
		kernel->KillGameClock(IDI_USER_ADD_SCORE);
		kernel->SetGameClock(m_wCurrentUser, IDI_USER_ADD_SCORE, TIME_USER_ADD_SCORE);
	}
	else
	{
		//设置扑克
		memcpy(m_cbHandCardData[kernel->GetMeChairID()], pLookCard->cbCardData, sizeof(pLookCard->cbCardData));
		m_pGameScene->mFrameLayer->SetCardData(m_cbHandCardData[kernel->GetMeChairID()], m_wViewChairID[kernel->GetMeChairID()], MAX_COUNT);
	}

 	//设置界面
 	word wId = pLookCard->wLookCardUser;
	if (kernel->GetMeChairID() != wId || kernel->IsLookonMode())
	{
		m_pGameScene->mFrameLayer->SetLookCard(m_wViewChairID[wId], true);
	}

	if ((kernel->IsLookonMode() == true) || (wId != kernel->GetMeChairID())) SoundManager::shared()->playSound("ZJH_CENTER_SEND_CARD");

	IClientUserItem *pUserItem = kernel->GetTableUserItem(wId);
	if (pUserItem == nullptr) return true;
	bool bBoy = (pUserItem->GetFaceID() % 8 > 3 ? true : false);

	if (bBoy)
		SoundManager::shared()->playSound("ZJH_MAN_LOOK");
	else
		SoundManager::shared()->playSound("ZJH_WOMAN_LOOK");

	return true;
}
//用户比牌
bool ClientKernelSink_zhajinhua::OnSubCompareCard(const void * pBuffer, int wDataSize)
{
	//效验数据
	if (wDataSize != sizeof(CMD_S_CompareCard)) return false;
	CMD_S_CompareCard * pCompareCard = (CMD_S_CompareCard *)pBuffer;

	IClientKernel* kernel = IClientKernel::get();

	//删除时间
	kernel->KillGameClock(IDI_DISABLE);

	//取消等待
	m_pGameScene->mFrameLayer->SetWaitUserChoice(INVALID_CHAIR);

	//输牌用户
	m_wLostUser = pCompareCard->wLostUser;
	for (word i = 0; i < GAME_PLAYER; i++)
	{
		if (m_wLostUserID[i] == INVALID_CHAIR)
		{
			m_wLostUserID[i] = m_wLostUser;
			break;
		}
	}

	//设置变量
	m_cbPlayStatus[m_wLostUser] = FALSE;

	//界面设置
	//PlaySoundRand(TEXT("PK"), 4, m_wLostUser);
	//状态设置
	if ((kernel->IsLookonMode() == false) && (m_wLostUser == kernel->GetMeChairID())) kernel->SetGameStatus(GAME_STATUS_FREE);

	//当前用户
	m_wCurrentUser = pCompareCard->wCurrentUser;

	//取消比牌背景
	word wViewChair1 = m_wViewChairID[pCompareCard->wCompareUser[0]];
	word wViewChair2 = m_wViewChairID[pCompareCard->wCompareUser[1]];

	//隐藏控件
	m_pGameScene->mFrameLayer->SetBtnShow(ZJH_VALUE_NULL);
	word wFalshUser[] = { wViewChair1, wViewChair2 };

	//动画效果
	m_pGameScene->mFrameLayer->PerformCompareCard(wFalshUser, m_wViewChairID[m_wLostUser]);

	return true;
}
//用户开牌
bool ClientKernelSink_zhajinhua::OnSubOpenCard(const void * pBuffer, int wDataSize)
{
	//效验数据
	if (wDataSize != sizeof(CMD_S_OpenCard)) return false;
	CMD_S_OpenCard* pOpenCard = (CMD_S_OpenCard *)pBuffer;

	IClientKernel* kernel = IClientKernel::get();

	//取消等待
	m_pGameScene->mFrameLayer->SetWaitUserChoice(INVALID_CHAIR);

	//胜利用户
	m_wWinnerUser = pOpenCard->wWinner;

	//隐藏控件
	m_pGameScene->mFrameLayer->SetBtnShow(ZJH_VALUE_NULL);

	//动画效果
	word wFalshUser[GAME_PLAYER];
	for (word i = 0; i < GAME_PLAYER; i++)if (m_cbPlayStatus[i] == TRUE)wFalshUser[i] = m_wViewChairID[i];
	m_pGameScene->mFrameLayer->StartFalshCard(wFalshUser);

	return true;
}
//用户强退
bool ClientKernelSink_zhajinhua::OnSubPlayerExit(const void * pBuffer, int wDataSize)
{
	//效验数据
	if (wDataSize != sizeof(CMD_S_PlayerExit)) return false;
	CMD_S_PlayerExit * pPlayerExit = (CMD_S_PlayerExit *)pBuffer;

	IClientKernel* kernel = IClientKernel::get();

	word wID = pPlayerExit->wPlayerID;

	//游戏信息
	ASSERT(m_cbPlayStatus[wID] == TRUE);
	m_cbPlayStatus[wID] = FALSE;
	m_cbPlayStatus[m_wViewChairID[wID]] = FALSE;

	return true;
}
//游戏结束
bool ClientKernelSink_zhajinhua::OnSubGameEnd(const void * pBuffer, int wDataSize)
{
	//效验参数
	if (wDataSize != sizeof(CMD_S_GameEnd)) return false;
	CMD_S_GameEnd * pGameEnd = (CMD_S_GameEnd *)pBuffer;

	IClientKernel* kernel = IClientKernel::get();

	word wMeChiar = kernel->GetMeChairID();

	//删除定时器
	kernel->KillGameClock(IDI_USER_ADD_SCORE);

	//处理控件
	m_pGameScene->mFrameLayer->SetBtnShow(ZJH_VALUE_NULL);

	//正常结束
	word lTax = m_lCellScore*pGameEnd->lGameTax / 1000;
	word wWinner = INVALID_CHAIR;
	for (word i = 0; i<GAME_PLAYER; i++)
	{
		//设置信息
		if (pGameEnd->lGameScore[i] != 0L)
		{
			if (pGameEnd->lGameScore[i]>0L)
			{
				wWinner = i;
			}
		}
	}

	//比牌与被比牌用户所看到的数据
	for (word j = 0; j < 4; j++)
	{
		word wUserID = pGameEnd->wCompareUser[wMeChiar][j];
		if (wUserID == INVALID_CHAIR)continue;

		word wViewChiar = m_wViewChairID[wUserID];
		m_pGameScene->mFrameLayer->SetLookCard(wViewChiar, false);
		m_pGameScene->mFrameLayer->SetCardData(pGameEnd->cbCardData[wUserID], wViewChiar, MAX_COUNT);
	}

	//自己扑克
	if (pGameEnd->wCompareUser[wMeChiar][0] != INVALID_CHAIR || m_bMingZhu[wMeChiar])
	{
		word wViewChiar = m_wViewChairID[wMeChiar];
		m_pGameScene->mFrameLayer->SetCardData(pGameEnd->cbCardData[wMeChiar], wViewChiar, MAX_COUNT);
	}

	//开牌结束
	if (pGameEnd->wEndState == 1)
	{
		m_pGameScene->mFrameLayer->StopFalshCard();
		//所有比牌用户
		if (m_cbPlayStatus[wMeChiar] == TRUE)
		{
			for (word i = 0; i < GAME_PLAYER; i++)
			{
				if (m_cbPlayStatus[i] == TRUE)
				{
					word wViewChiar = m_wViewChairID[i];
					m_pGameScene->mFrameLayer->SetLookCard(wViewChiar, false);
					m_pGameScene->mFrameLayer->SetCardData(pGameEnd->cbCardData[i], wViewChiar, MAX_COUNT);
				}
			}
		}
	}

	//数字滚动动画
	for (word i = 0; i < GAME_PLAYER; i++)
	{
		if (pGameEnd->lGameScore[i] == 0) continue;
		word wViewId = m_wViewChairID[i];
		m_pGameScene->mFrameLayer->SetGameEndScore(wViewId, pGameEnd->lGameScore[i]);
	}

	//回收筹码
 	if (wWinner<GAME_PLAYER)
 		m_pGameScene->mFrameLayer->WinJettonMove(m_wViewChairID[wWinner]);

 	//播放声音 by_zhen
 	if (kernel->IsLookonMode() == false)
 	{
		if (pGameEnd->lGameScore[kernel->GetMeChairID()]>0L) SoundManager::shared()->playSound("ZJH_GAME_WIN");
 		else
 		{
			SoundManager::shared()->playSound("ZJH_GAME_LOST");
 		}
 	}
	else SoundManager::shared()->playSound("ZJH_GAME_END");

	//开始按钮
	if (kernel->IsLookonMode() == false)
	{
		m_pGameScene->mFrameLayer->SetBtnShow(ZJH_VALUE_READY);
		kernel->SetGameClock(kernel->GetMeChairID(), IDI_START_GAME, TIME_START_GAME);
	}
	//显示玩家本局输赢情况
	for (int i = 0; i < GAME_PLAYER_ZHAJINHUA; i++)
	{
		m_pGameScene->showScoreTips(i, pGameEnd->lGameScore[i]);
	}

	//状态设置
	m_pGameScene->mFrameLayer->SetWaitUserChoice(INVALID_CHAIR);
	kernel->SetGameStatus(GAME_STATUS_FREE);

	return true;
}
//准备
void ClientKernelSink_zhajinhua::onEventReday(cocos2d::Ref* obj)
{
	IClientKernel* kernel = IClientKernel::get();

	//删除定时器
	if (!kernel->IsLookonMode()) kernel->KillGameClock(IDI_START_GAME);

	//状态变量
	m_wBankerUser = INVALID_CHAIR;
	m_wCurrentUser = INVALID_CHAIR;
	zeromemory(m_cbPlayStatus, sizeof(m_cbPlayStatus));
	zeromemory(m_lTableScore, sizeof(m_lTableScore));
	m_lMaxCellScore = 0L;
	m_lCellScore = 0L;

	PlayerNode_zhajinhua * node = nullptr;
	for (word i = 0; i < GAME_PLAYER; i++)
	{
		m_bMingZhu[i] = false;
		//设置头像点击
		node = m_pGameScene->mPlayerLayer->getPlayerNodeById(m_wViewChairID[i]);
		if (node)
			node->showHeadButton(false);
	}

	//发送消息
	if (!kernel->IsLookonMode()) kernel->SendUserReady(NULL, 0);

	//

// 	//测试代码
// 	 	word wFalshUser[] = { 0, 1, 2 };
// 	  	m_pGameScene->mFrameLayer->m_Card.setCardArrayWithRow("zhajinhuaGameScene/zhajinhua/img_poker_card.png", CSpriteCard::g_cbCardData, 59);
// 		
// 	 	//动画效果
// 		//m_pGameScene->mFrameLayer->PerformCompareCard(wFalshUser, m_wViewChairID[m_wLostUser]);
// 		for (word i = 0; i < MAX_COUNT; i++)
// 		{
// 			for (word j = 0; j < GAME_PLAYER; j++)
// 			{
// 				m_pGameScene->mFrameLayer->CreateUserCard(j, 0, i);
// 			}
// 		}
// 		m_pGameScene->mFrameLayer->StartFalshCard(wFalshUser);
}
//全押
void ClientKernelSink_zhajinhua::onEventAllin(Ref* obj)
{
// 	IClientKernel* kernel = IClientKernel::get();
// 
// 	int nAddScore = m_lMaxCellScore;
// 	ASSERT(nAddScore >= (m_lCellScore*m_lCurrentTimes));
// 
// 	//删除时间
// 	kernel->KillGameClock(IDI_USER_ADD_SCORE);
// 
// 	//获取筹码
// 	SCORE lCurrentScore = nAddScore;
// 	word wMeChairID = kernel->GetMeChairID();
// 
// 	//看牌加倍
// 	if (m_bMingZhu[wMeChairID]) lCurrentScore *= 2;
// 
// 	//预先处理
// 	m_lTableScore[wMeChairID] += lCurrentScore;
// 	m_pGameScene->mFrameLayer->SetUserTableScore(m_wViewChairID[wMeChairID], m_lTableScore[wMeChairID], lCurrentScore);
// 	//筹码动画
// 
// 	//处理按钮
// 	m_pGameScene->mFrameLayer->SetBtnShow(ZJH_VALUE_SEE | ZJH_VALUE_FLOD | ZJH_VALUE_FOLLOWALL);
// 	if (m_bMingZhu[wMeChairID])
// 		m_pGameScene->mFrameLayer->SetBtnEnable(ZJH_VALUE_FLOD | ZJH_VALUE_FOLLOWALL);
// 	else
// 		m_pGameScene->mFrameLayer->SetBtnEnable(ZJH_VALUE_SEE | ZJH_VALUE_FLOD | ZJH_VALUE_FOLLOWALL);
// 
// 	//发送消息
// 	CMD_C_AddScore AddScore;
// 	AddScore.wState = 0;
// 	AddScore.lScore = lCurrentScore;
// 
// 	kernel->SendSocketData(SUB_C_ADD_SCORE, &AddScore, sizeof(AddScore));
}
//比牌
void ClientKernelSink_zhajinhua::onEventCompare(Ref* obj)
{
	IClientKernel* kernel = IClientKernel::get();

	//删除时间
	kernel->KillGameClock(IDI_USER_ADD_SCORE);

	//判断明注
	word wMeChairID = kernel->GetMeChairID();
	SCORE lCurrentScore = (m_bMingZhu[wMeChairID]) ? (m_lCurrentTimes*m_lCellScore * 4) : (m_lCurrentTimes*m_lCellScore * 2);

	if ((m_lUserMaxScore - m_lTableScore[wMeChairID]) < lCurrentScore)
	{
		lCurrentScore = (m_lUserMaxScore - m_lTableScore[wMeChairID]) / m_lCellScore * m_lCellScore;
	}

	m_lTableScore[wMeChairID] += lCurrentScore;
	m_pGameScene->mFrameLayer->SetUserTableScore(m_wViewChairID[wMeChairID], m_lTableScore[wMeChairID], lCurrentScore);

	//筹码动画
	m_pGameScene->mFrameLayer->AddJettonMove(m_wViewChairID[wMeChairID], lCurrentScore);
	
	//发送消息
	//发送消息
	CMD_C_AddScore AddScore;
	AddScore.wState = TRUE;
	AddScore.lScore = lCurrentScore;
	kernel->SendSocketData(SUB_C_ADD_SCORE, &AddScore, sizeof(AddScore));

	//当前人数
	byte UserCount = 0;
	for (byte i = 0; i < GAME_PLAYER; i++)
	{
		if (m_cbPlayStatus[i] == TRUE)UserCount++;
	}

	//庄家在第一轮没下注只能跟上家比牌 或 只剩下两人
	if (m_wBankerUser == wMeChairID && (m_lTableScore[wMeChairID] - lCurrentScore) == m_lCellScore || UserCount == 2)
	{
		//构造变量
		CMD_C_CompareCard CompareCard;

		//查找上家
		;
		for (word i = wMeChairID - 1;; i--)
		{
			if (i == INVALID_CHAIR)
				i = GAME_PLAYER - 1;
			if (m_cbPlayStatus[i] == TRUE)
			{
				CompareCard.wCompareUser = i;
				break;
			}
		}
		//发送消息
		kernel->SendSocketData(SUB_C_COMPARE_CARD, &CompareCard, sizeof(CompareCard));
	}
	else
	{
		//选择玩家
		bool bCompareUser[GAME_PLAYER];
		PlayerNode_zhajinhua * node = nullptr;
		for (word i = 0; i < GAME_PLAYER; i++)
		{
			if (m_cbPlayStatus[i] == TRUE && wMeChairID != i)
			{
				//设置头像点击
				node = m_pGameScene->mPlayerLayer->getPlayerNodeById(m_wViewChairID[i]);
				node->showHeadButton(true);
				//bCompareUser[m_wViewChairID[i]] = TRUE;
			}
			//else bCompareUser[m_wViewChairID[i]] = FALSE;
		}

		//发送等待比牌消息
		kernel->SendSocketData(SUB_C_WAIT_COMPARE, NULL, 0);

		//设置时间
		kernel->SetGameClock(wMeChairID, IDI_USER_COMPARE_CARD, TIME_USER_COMPARE_CARD);

		//提示标识
	}
}
//开牌
void ClientKernelSink_zhajinhua::OnEventOpen(Ref* obj)
{
	IClientKernel* kernel = IClientKernel::get();

	//删除定时器
	kernel->KillGameClock(IDI_USER_ADD_SCORE);

	//查找人数
	word bUserCount = 0;
	for (word i = 0; i < GAME_PLAYER; i++)
	{
		if (m_cbPlayStatus[i] == TRUE) bUserCount++;
	}

	//两人比牌
	if (bUserCount == 2)
	{
		onEventCompare(nullptr);
		return;
	}

	//判断明注
	word wMeChairID = kernel->GetMeChairID();
	int64 lCurrentScore = (m_bMingZhu[wMeChairID]) ? (m_lCurrentTimes*m_lCellScore * 4) : (m_lCurrentTimes*m_lCellScore * 2);
	m_lTableScore[wMeChairID] += lCurrentScore;
	m_pGameScene->mFrameLayer->SetUserTableScore(m_wViewChairID[wMeChairID], m_lTableScore[wMeChairID], lCurrentScore);
	m_pGameScene->mFrameLayer->AddJettonMove(m_wViewChairID[wMeChairID], lCurrentScore);

	//发送消息
	CMD_C_AddScore AddScore;
	AddScore.wState = 2;
	AddScore.lScore = lCurrentScore;
	kernel->SendSocketData(SUB_C_ADD_SCORE, &AddScore, sizeof(AddScore));

	//发送消息
	kernel->SendSocketData(SUB_C_OPEN_CARD, NULL, 0);
}
//加注
void ClientKernelSink_zhajinhua::onEventRaise(Ref* obj)
{
	IClientKernel* kernel = IClientKernel::get();

	EventCustom* event = dynamic_cast<EventCustom*>(obj);

	Button* btnJetton = (Button*)event->getUserData();

	int nAddScore = btnJetton->getTag();
	SCORE lCurrentScore = nAddScore + m_lCellScore*m_lCurrentTimes;
	ASSERT(lCurrentScore >= (m_lCellScore*m_lCurrentTimes) && nAddScore <= m_lMaxCellScore);

	//删除时间
	kernel->KillGameClock(IDI_USER_ADD_SCORE);

	//获取筹码
	word wMeChairID = kernel->GetMeChairID();

	//看牌加倍
	if (m_bMingZhu[wMeChairID]) lCurrentScore *= 2;

	//预先处理
	m_lTableScore[wMeChairID] += lCurrentScore;
	m_pGameScene->mFrameLayer->SetUserTableScore(m_wViewChairID[wMeChairID], m_lTableScore[wMeChairID], lCurrentScore);
	//筹码动画
	m_pGameScene->mFrameLayer->AddJettonMove(m_wViewChairID[wMeChairID], lCurrentScore);

	//播放声音
	SoundManager::shared()->playSound("ZJH_ADD_SCORE");

	IClientUserItem *pUserItem = kernel->GetTableUserItem(wMeChairID);
	bool bBoy = (pUserItem->GetFaceID() % 8 > 3 ? true : false);

	if (bBoy)
		SoundManager::shared()->playSound("ZJH_MAN_ADD");
	else
		SoundManager::shared()->playSound("ZJH_WOMAN_ADD");

	//发送消息
	CMD_C_AddScore AddScore;
	AddScore.wState = 0;
	AddScore.lScore = lCurrentScore;

	kernel->SendSocketData(SUB_C_ADD_SCORE, &AddScore, sizeof(AddScore));
}
//跟注
void ClientKernelSink_zhajinhua::onEvnetFollow(Ref* obj)
{
	IClientKernel* kernel = IClientKernel::get();
	Button* btnJetton = dynamic_cast<Button*>(obj);

	int nAddScore = m_lCellScore*m_lCurrentTimes;

	//删除时间
	kernel->KillGameClock(IDI_USER_ADD_SCORE);

	//处理按钮
	m_pGameScene->mFrameLayer->SetBtnShow(ZJH_VALUE_FLOD | ZJH_VALUE_FOLLOWALL);

	//获取筹码
	SCORE lCurrentScore = nAddScore;
	word wMeChairID = kernel->GetMeChairID();

	//看牌加倍
	if (m_bMingZhu[wMeChairID]) lCurrentScore *= 2;

	//预先处理
	m_lTableScore[wMeChairID] += lCurrentScore;
	m_pGameScene->mFrameLayer->SetUserTableScore(m_wViewChairID[wMeChairID], m_lTableScore[wMeChairID], lCurrentScore);
	//筹码动画
	m_pGameScene->mFrameLayer->AddJettonMove(m_wViewChairID[wMeChairID], lCurrentScore);

	//播放声音
	SoundManager::shared()->playSound("ZJH_ADD_SCORE");

	IClientUserItem *pUserItem = kernel->GetTableUserItem(wMeChairID);
	bool bBoy = (pUserItem->GetFaceID() % 8 > 3 ? true : false);

	if (bBoy)
		SoundManager::shared()->playSound("ZJH_MAN_FOLLOW");
	else
		SoundManager::shared()->playSound("ZJH_WOMAN_FOLLOW");

	//发送消息
	CMD_C_AddScore AddScore;
	AddScore.wState = 0;
	AddScore.lScore = lCurrentScore;

	kernel->SendSocketData(SUB_C_ADD_SCORE, &AddScore, sizeof(AddScore));
}
//看牌
void ClientKernelSink_zhajinhua::onEvnetSee(Ref* obj)
{
	IClientKernel* kernel = IClientKernel::get();

	word wMeChairID = kernel->GetMeChairID();

	kernel->KillGameClock(IDI_USER_ADD_SCORE);
	kernel->SetGameClock(wMeChairID, IDI_USER_ADD_SCORE, TIME_USER_ADD_SCORE);

	m_bMingZhu[wMeChairID] = true;

	kernel->SendSocketData(SUB_C_LOOK_CARD, NULL, 0);

	m_pGameScene->mFrameLayer->UpdateControl();

	//播放声音
	SoundManager::shared()->playSound("ZJH_CENTER_SEND_CARD");
}
//弃牌
void ClientKernelSink_zhajinhua::onEventFlod(Ref* obj)
{
	IClientKernel* kernel = IClientKernel::get();

	//删除时间
	kernel->KillGameClock(IDI_USER_ADD_SCORE);

	m_pGameScene->mFrameLayer->SetBtnShow(ZJH_VALUE_NULL);

	//扑克变灰


	kernel->SendSocketData(SUB_C_GIVE_UP, NULL, 0);

	SoundManager::shared()->playSound("ZJH_GAME_UP");
}
//跟到底
void ClientKernelSink_zhajinhua::onEvnetFollowAll(Ref* obj)
{
	IClientKernel* kernel = IClientKernel::get();
}
//发牌动画完成
void ClientKernelSink_zhajinhua::onEventDispatchfinish(Ref* obj)
{
	//测试代码
// 	word wFalshUser[] = { 0, 1 }; m_wLostUser = 1;
// 	//动画效果
// 	m_pGameScene->mFrameLayer->PerformCompareCard(wFalshUser, 1);

	////////发牌动画结束后的处理
	//设置时间
	if (m_wCurrentUser == INVALID_CHAIR)return;
	IClientKernel * kernel = IClientKernel::get();
	kernel->SetGameClock(m_wCurrentUser, IDI_USER_ADD_SCORE, TIME_USER_ADD_SCORE);
	kernel->SendSocketData(SUB_C_ADD_SCORE_TIME);

	//过虑用户
	word wMeChairID = kernel->GetMeChairID();
	if (kernel->IsLookonMode() || wMeChairID != m_wCurrentUser)return;

	m_pGameScene->mFrameLayer->UpdateControl();
}
//头像点击
void ClientKernelSink_zhajinhua::onEventHead(Ref * obj)
{
	IClientKernel* kernel = IClientKernel::get();
	EventCustom* event = dynamic_cast<EventCustom*>(obj);
	Button* btnHead = (Button*)event->getUserData();

	//校验数据

	word wTemp = btnHead->getTag();
	word wChairID = INVALID_CHAIR;
	for (word i = 0; i < GAME_PLAYER; i++)
	{
		if (m_wViewChairID[i] == wTemp)
		{
			wChairID = i;
			break;
		}
	}
	ASSERT(wChairID != INVALID_CHAIR && m_cbPlayStatus[wChairID] == TRUE && wChairID != kernel->GetMeChairID());

	//删除定时器
	kernel->KillGameClock(IDI_USER_COMPARE_CARD);

	//设置状态
	PlayerNode_zhajinhua * node = nullptr;
	for (word i = 0; i < GAME_PLAYER; i++)
	{
		//设置头像点击
		node = m_pGameScene->mPlayerLayer->getPlayerNodeById(m_wViewChairID[i]);
		node->showHeadButton(false);
	}

	//发送比牌
	CMD_C_CompareCard CompareCard;
	zeromemory(&CompareCard, sizeof(CompareCard));
	CompareCard.wCompareUser = wChairID;

	kernel->SendSocketData(SUB_C_COMPARE_CARD, &CompareCard, sizeof(CompareCard));
	return;;
}
//比牌完成
void ClientKernelSink_zhajinhua::onEventCompareFinish(Ref * obj)
{
	IClientKernel * kernel = IClientKernel::get();
	//比牌结束
	if (m_wWinnerUser >= GAME_PLAYER)
	{
		if (m_wLostUser > GAME_PLAYER) return;
		ASSERT(m_wLostUser < GAME_PLAYER);

		//输牌用户
		word wLostUser = m_wLostUser;
		word wMeChairID = kernel->GetMeChairID();

		//变量定义
		word wViewChairID = m_wViewChairID[wLostUser];
		word wViewMeChairID = m_wViewChairID[wMeChairID];

 		//设置扑克
 		if (m_cbHandCardData[wMeChairID][0] != 0 && m_cbPlayStatus[wMeChairID] == TRUE && m_bMingZhu[wMeChairID])
 			m_pGameScene->mFrameLayer->SetCardData(m_cbHandCardData[wMeChairID], wViewMeChairID, MAX_COUNT);

		//声音
		//环境设置
		if ((!kernel->IsLookonMode()) && wLostUser == wMeChairID)
		{
			SoundManager::shared()->playSound("ZJH_GAME_LOST");
		}

		//玩家人数
		byte bCount = 0;
		for (word i = 0; i < GAME_PLAYER; i++) if (m_cbPlayStatus[i] == TRUE) bCount++;

		//判断结束
		if (bCount>1)
		{
			if (!kernel->IsLookonMode() && wMeChairID == m_wCurrentUser) m_pGameScene->mFrameLayer->UpdateControl();

			kernel->SetGameClock(m_wCurrentUser, IDI_USER_ADD_SCORE, TIME_USER_ADD_SCORE);
		}
		else
		{
			word i = 0;
			for (; i < GAME_PLAYER; i++)
			{
				if (m_cbPlayStatus[i] == TRUE) break;
			}
			if (i == wMeChairID || wMeChairID == m_wLostUser)
			{
				//发送消息
				kernel->SendSocketData(SUB_C_FINISH_FLASH, NULL, 0);
			}
		}
	}
	//开牌结束
	else
	{
		if (m_wWinnerUser > GAME_PLAYER) return;

		//比牌失败用户
		for (int i = 0; i < GAME_PLAYER; i++)
		{
			if (m_cbPlayStatus[i] == TRUE && i != m_wWinnerUser)
			{
				//扑克变裂
				word wViewChairID = m_wViewChairID[i];
				byte cbCardData[MAX_COUNT] = { 0x45, 0x45, 0x45 };
				m_pGameScene->mFrameLayer->SetCardData(cbCardData, wViewChairID, MAX_COUNT);

				//状态设置
				if (! kernel->IsLookonMode() && i == kernel->GetMeChairID())
				{
					kernel->SetGameStatus(GAME_STATUS_FREE);

					//音乐
					SoundManager::shared()->playSound("ZJH_GAME_LOST");
				}
			}
		}

		m_wWinnerUser = INVALID_CHAIR;

		//发送消息
		kernel->SendSocketData(SUB_C_FINISH_FLASH, NULL, 0);
	}
}

void ClientKernelSink_zhajinhua::CloseGameDelayClient()
{
	m_pGameScene->ExitGameTiming();
}
