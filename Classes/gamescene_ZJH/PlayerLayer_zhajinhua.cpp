#include "PlayerLayer_zhajinhua.h"
#include "Tools/tools/MTNotification.h"
#include "Kernel/kernel/game/IClientKernel.h"
#include "ClientKernelSink_zhajinhua.h"
#include "cmd_zhajinhua.h"
PlayerLayer_zhajinhua::PlayerLayer_zhajinhua()
{
	G_NOTIFY_REG("EVENT_USER_SCORE", PlayerLayer_zhajinhua::updateUserScore);
}


PlayerLayer_zhajinhua::~PlayerLayer_zhajinhua()
{
	G_NOTIFY_UNREG("EVENT_USER_SCORE");
}

PlayerLayer_zhajinhua * PlayerLayer_zhajinhua::create(Widget * mRoot)
{
	PlayerLayer_zhajinhua * node = new PlayerLayer_zhajinhua;
	if (node && node->init(mRoot))
	{
		node->autorelease();
		return node;
	}
	delete node;
	return nullptr;
}

bool PlayerLayer_zhajinhua::init(Widget * mRoot)
{
	if (!Layer::init())
	{
		return false;
	}

	for (int i = 0; i < GAME_PLAYER; i++)
	{
		PlayerNode_zhajinhua * node = PlayerNode_zhajinhua::create(i,mRoot);
		this->addChild(node);
		mPlayerList.push_back(node);
	}

	return true;
}

PlayerNode_zhajinhua * PlayerLayer_zhajinhua::getPlayerNodeById(int chair_id)
{
/*	cocos2d::log("--------8-----------");*/
	if (chair_id < mPlayerList.size())
	{
		return mPlayerList[chair_id];
	}

	return nullptr;
	
}

void PlayerLayer_zhajinhua::updateUserScore(cocos2d::Ref * ref)
{
/*	cocos2d::log("--------7-----------");*/
	EventCustom *event_ = (EventCustom*)ref;
	if (event_ == 0){
		return;
	}

	MTData* data = (MTData*)event_->getUserData();
	if (data == 0)
		return;

	IClientUserItem * item = (IClientUserItem *)data->mPData;

	IClientKernel * kernel = IClientKernel::get();
	PlayerNode_zhajinhua * node = nullptr;
	if (item)
	{
		int chair_id = item->GetChairID();
		node = getPlayerNodeById(kernel->SwitchViewChairID(chair_id));

		if (node)
		{
			node->setScore(item->GetUserScore());
		}
	}
}
