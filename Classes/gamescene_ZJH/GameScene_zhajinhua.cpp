#include "Tools/tools/MTNotification.h"
#include "common/KeybackLayer.h"
#include "Tools/tools/SimpleTools.h"
#include "Tools/ViewHeader.h"
#include "Tools/Manager/SoundManager.h"
#include "Tools/tools/gPlatform.h"
#include "Tools/tools/StringData.h"
#include "Kernel/kernel/game/IClientKernel.h"
#include "Tools/tools/StaticData.h"
#include "Kernel/kernel/server/IServerItemSink.h"
#include "Tools/Dialog/NewDialog.h"
#include "Platform/PFView/ServerScene/ServerScene.h"
#include "Platform/PFView/ModeScene/ModeScene.h"

#include "GameScene_zhajinhua.h"
#include "cmd_zhajinhua.h"

bool isZhajinhuaReconnectOnLoss = false;

#define  P_WIDTH      1420
#define  P_WIDTH_2    P_WIDTH/2

#define  P_HEIGHT     800
#define  P_HEIGHT_2   P_HEIGHT/2

GameScene_zhajinhua::GameScene_zhajinhua()
{
	touch_layer_ = nullptr;
	mFrameLayer = nullptr;

	//背景音乐
// 	char sSound[32] = { 0 };
// 	sprintf(sSound, "BGM_NORMAL_1");
// 	SoundManager::shared()->playMusic(sSound);
}

GameScene_zhajinhua::~GameScene_zhajinhua()
{
	if (touch_layer_)
	{
		touch_layer_->setTouchEnabled(false);
		touch_layer_->setSink(0);
		touch_layer_->release();
	}

	//停止音乐
 	SoundManager::shared()->stopMusic();
	PLAY_PLATFORM_BG_MUSIC
	G_NOTIFY_UNREG("GAME_CLOSE");
	G_NOTIFY_UNREG("EVENT_GAME_EXIT");

	mFrameLayer = nullptr;
	mPlayerLayer = nullptr;
}

bool GameScene_zhajinhua::init()
{
	do 
	{
		CC_BREAK_IF(!Scene::init());

		//1.解析主界面Json
		Widget * mRoot = cocostudio::GUIReader::getInstance()->widgetFromJsonFile("zhajinhuaGameScene/zhajinhua_main.json");
		//创建层
		//主界面层
		mFrameLayer = FrameLayer_zhajinhua::create(mRoot);
		this->addChild(mFrameLayer,1);
		mFrameLayer->setCloseDialogInfo(this, callfuncN_selector(GameScene_zhajinhua::closeCallBack), SSTRING("back_to_room"), SSTRING("back_to_room_content"));
		
		//玩家层
		mPlayerLayer = PlayerLayer_zhajinhua::create(mRoot);
		this->addChild(mPlayerLayer,6);

		mMessageLayer = MessageLayer::create();
		mMessageLayer->setName("mMessageLayer");
		addChild(mMessageLayer, 100);

		auto kernel = IClientKernel::get();
		if (kernel)
		{
			kernel->SetChatSink(mMessageLayer);
			kernel->SetStringMessageSink(mMessageLayer);
		}

		SoundManager::shared()->stopMusic();
		SoundManager::shared()->playMusic("ZJH_BACK_GROUD");

		///< 关闭游戏
		G_NOTIFY_REG("EVENT_GAME_EXIT", GameScene_zhajinhua::closeGameNotify);
		///< 断线重连
		G_NOTIFY_REG("RECONNECT_ON_LOSS", GameScene_zhajinhua::func_Reconnect_on_loss);

		this->setName("GameScene");
		this->setTag(KIND_ID);

		return true;
	} while (0);

	return false;
}

void GameScene_zhajinhua::initGameBaseData()
{
	DF::shared()->init(KIND_ID, GAME_PLAYER, VERSION_CLIENT, STATIC_DATA_STRING("appname"), platformGetPlatform());
}

void GameScene_zhajinhua::onEnterTransitionDidFinish()
{
 	Scene::onEnterTransitionDidFinish();
	auto ik = IClientKernel::get();
	if (ik)
	{
		ik->SendGameOption();
		//startUpdate();
	}

	touch_layer_ = 0;

	EventListenerTouchOneByOne* mListener = EventListenerTouchOneByOne::create();
	mListener->setSwallowTouches(true);
	mListener->onTouchBegan = CC_CALLBACK_2(GameScene_zhajinhua::onTouchBegan, this);
	mListener->onTouchMoved = CC_CALLBACK_2(GameScene_zhajinhua::onTouchMoved, this);
	mListener->onTouchEnded = CC_CALLBACK_2(GameScene_zhajinhua::onTouchEnded, this);
	_eventDispatcher->addEventListenerWithSceneGraphPriority(mListener, this);
}

void GameScene_zhajinhua::onExitTransitionDidStart()
{
	//stopUpdate();
	Scene::onExitTransitionDidStart();
}

void GameScene_zhajinhua::onKeybackClicked()
{
	popup(SSTRING("system_tips_title"), SSTRING("back_to_chair_select"), DLG_MB_OK | DLG_MB_CANCEL, 0, this, callfuncN_selector(GameScene_zhajinhua::onBackToRoom));
}

void GameScene_zhajinhua::onBackToRoom(Node* pNode)
{
	switch (pNode->getTag())
	{
		case DLG_MB_OK:
		{
						  if (IClientKernel::get())
							  IClientKernel::get()->Intermit(GameExitCode_Normal);
						  return;
		}
		break;
	}
}

void GameScene_zhajinhua::closeGameNotify(cocos2d::Ref * ref)
{
	if (IClientKernel::get())
		IClientKernel::get()->Intermit(GameExitCode_Normal);
}

bool GameScene_zhajinhua::onTouchBegan(Touch *pTouch, Event *pEvent)
{
	IClientKernel* kernel = IClientKernel::get();

	if (kernel == 0)
		return false;
	return true;
}

void GameScene_zhajinhua::onTouchMoved(Touch *pTouch, Event *pEvent)
{
	IClientKernel* kernel = IClientKernel::get();

	if (kernel == 0)
		return;
}

void GameScene_zhajinhua::onTouchEnded(Touch *pTouch, Event *pEvent)
{

}

void GameScene_zhajinhua::func_Reconnect_on_loss(cocos2d::Ref * obj)
{
	IClientKernel* kernel = IClientKernel::get();
	if (kernel == nullptr)
	{
		G_NOTIFY_D("MODE_SELECTED", MTData::create(1));
		return;
	}

	isZhajinhuaReconnectOnLoss = true;

	//断线重连
	this->schedule(SEL_SCHEDULE(&GameScene_zhajinhua::reconnect_on_loss), 1.5f);
}

void GameScene_zhajinhua::reconnect_on_loss(float dt)
{
	static float total_time = 0;
	total_time += dt;

	bool isHaveNet = false;

#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID || CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
	isHaveNet = SimpleTools::obtainNetWorkState();
#endif

#if (CC_TARGET_PLATFORM == CC_PLATFORM_WIN32)
	if (total_time > 10)
		isHaveNet = true;
#endif
	if (total_time > 15)
	{
		this->unschedule(SEL_SCHEDULE(&GameScene_zhajinhua::reconnect_on_loss));
		if (!isHaveNet)
		{
			log("mei you wang luo l!");
			///< 没有网络了..
			NewDialog::create(SSTRING("System_Tips_26"), NewDialog::AFFIRM, [=]()
			{
				if (IClientKernel::get())
					IClientKernel::get()->Intermit(GameExitCode_Normal);
			});
			isZhajinhuaReconnectOnLoss = false;
			total_time = 0.0f;
			return;
		}
	}

	if (isHaveNet)
	{
		if (total_time <= 15.0f)
			this->unschedule(SEL_SCHEDULE(&GameScene_zhajinhua::reconnect_on_loss));
		NewDialog::create(SSTRING("System_Tips_28"), NewDialog::NONEBUTTON, nullptr, nullptr, [=]()
		{
			ServerScene * tServer = dynamic_cast<ServerScene *>(ModeScene::create()->getChildByName("ServerScene"));
			if (tServer)
			{
				tServer->connectServer();
			}
			isZhajinhuaReconnectOnLoss = false;
		});
		total_time = 0.0f;
		isZhajinhuaReconnectOnLoss = true;
	}
	else
	{
		NewDialog::create(SSTRING("System_Tips_29"), NewDialog::NONEBUTTON);
	}
}

bool GameScene_zhajinhua::is_Reconnect_on_loss()
{
	return isZhajinhuaReconnectOnLoss;
}
// 添加消息
void GameScene_zhajinhua::add_msg(const std::string& msg)
{
	mMessageList.push_back(msg);

 	if (mMessageList.size() == 1)
 	{
 	
 	}
}

void GameScene_zhajinhua::func_msg_show()
{
	msg_start_next(false);
}

void GameScene_zhajinhua::func_msg_start_next()
{
	msg_start_next(true);
}

void GameScene_zhajinhua::msg_start_next(bool bRemoveFront)
{
	if (bRemoveFront && !mMessageList.empty())
		mMessageList.pop_front();

	if (mMessageList.empty())
	{
		
		return;
	}

	int index = mMsgContentIndex;
	mMsgContentIndex = (mMsgContentIndex + 1) % 2;
	Label* label = mMsgContents[index];

	label->setString(mMessageList.front().c_str());
}
//关闭游戏
void GameScene_zhajinhua::closeCallBack(cocos2d::Node* obj)
{
	switch (obj->getTag())
	{
		case DLG_MB_OK:
		{
						  if (IClientKernel::get())
							  IClientKernel::get()->Intermit(GameExitCode_Normal);
		}
		break;
	}
}

void GameScene_zhajinhua::showScoreTips(int charId, int64 score /*= 0*/)
{
	IClientKernel * kernel = IClientKernel::get();

	PlayerNode_zhajinhua * node = nullptr;
	node = mPlayerLayer->getPlayerNodeById(kernel->SwitchViewChairID(charId));

// 	if (score == 0)
// 		node->setShowGoldKuang(false);
// 	else
// 		node->setShowGoldKuang(true);
// 	node->setBuyMoney(score);
}

//显示用户
void GameScene_zhajinhua::showUserInfo(IClientUserItem * pIClientUserItem, byte cbUserStatus)
{
	IClientKernel * kernel = IClientKernel::get();
	
	PlayerNode_zhajinhua * node = nullptr;
	node = mPlayerLayer->getPlayerNodeById(kernel->SwitchViewChairID(pIClientUserItem->GetChairID()));

	if (pIClientUserItem->GetUserStatus() == US_READY)
	{
		node->setIsPrepare(true);
	}
	else
		node->setIsPrepare(false);

	//设置玩家信息
	node->setFaceID(pIClientUserItem->GetFaceID());
	node->setNickName(pIClientUserItem->GetNickName());
	node->setScore(pIClientUserItem->GetUserScore());
	node->setShowGoldKuang(false);
	node->setHeadInfo(true);
	///< 清理牌信息
	this->clearCardData(pIClientUserItem->GetChairID());
}

void GameScene_zhajinhua::net_user_state_update(int chair_id, byte user_state)
{
	auto ik = IClientKernel::get();
	if (ik == nullptr) return;
	PlayerNode_zhajinhua * node = nullptr;
	node = mPlayerLayer->getPlayerNodeById(ik->SwitchViewChairID(chair_id));
	if (user_state == US_NULL)
	{
		node->setIsPrepare(false);
	}
	else if (user_state == US_READY)
	{
		node->setIsPrepare(true);
		///< 清理牌
		clearCardData(chair_id);
	}
}

//用户离开
void GameScene_zhajinhua::netUserLevel(word wChairID)
{
	IClientKernel * kernel = IClientKernel::get();

	PlayerNode_zhajinhua * node = mPlayerLayer->getPlayerNodeById(kernel->SwitchViewChairID(wChairID));
	node->clearUser(true);
	this->clearCardData(kernel->GetMeChairID());
}

void GameScene_zhajinhua::clearPlayerPrepare()
{
	IClientKernel * kernel = IClientKernel::get();
	if (kernel == nullptr)return;
	for (int i = 0; i < GAME_PLAYER; i++)
	{
		IClientUserItem * pClientUserItem = kernel->GetTableUserItem(i);
		if (pClientUserItem != NULL)
		{
			int chair = kernel->SwitchViewChairID(i);
			auto node = mPlayerLayer->getPlayerNodeById(chair);
			node->setIsPrepare(false);
		}
	}
}

//显示动作
void GameScene_zhajinhua::showUserState(word wChairID, word wState)
{
	IClientKernel * kernel = IClientKernel::get();
	PlayerNode_zhajinhua * node = mPlayerLayer->getPlayerNodeById(wChairID);
	node->showState(wChairID, wState);
}
void GameScene_zhajinhua::ExitGameTiming()
{
	NewDialog::create(SSTRING("System_Tips_36"), NewDialog::NONEBUTTON);
	this->scheduleOnce(SEL_SCHEDULE(&GameScene_zhajinhua::CloseKernelExitGame), 2.5f);
}

void GameScene_zhajinhua::CloseKernelExitGame(float dt)
{
	IClientKernel::destory();
}

void GameScene_zhajinhua::clearCardData(int chair_id)
{
	IClientKernel * kernel = IClientKernel::get();

	int index = kernel->SwitchViewChairID(chair_id);
	for (int j = 0; j < 3; j++)
	{
		this->removeChildByTag(8520 + index * 5 + j);
	}
	removeChildByTag(600 + index);

}

// void GameScene_zhajinhua::clearOtherUser(int chair_id)
// {
// 	IClientKernel * kernel = IClientKernel::get();
// 	int index = kernel->SwitchViewChairID(chair_id);
// 	auto node = mPlayerLayer->getPlayerNodeById(index);
// 	node->clearUser(false);
// }
