#ifndef CMD_HK_FIVE_CARD_HEAD_FILE
#define CMD_HK_FIVE_CARD_HEAD_FILE
#include "Platform/PFDefine/df/types.h"
//////////////////////////////////////////////////////////////////////////
//公共宏定义

namespace zhajinhua
{
#pragma pack(push)  
#pragma pack(1)

#define KIND_ID							6									//游戏 I D
#define GAME_PLAYER						5									//游戏人数
#define GAME_NAME						TEXT("炸金花")						//游戏名字
#define MAX_COUNT						3									//扑克数目
#define VERSION_SERVER			    	PROCESS_VERSION(6,0,3)				//程序版本
#define VERSION_CLIENT				    PROCESS_VERSION(6,0,3)				//程序版本

#ifndef _UNICODE
#define myprintf	snprintf
#define mystrcpy	strcpy
#define mystrlen	strlen
#define myscanf		_snscanf
#define	myLPSTR		LPCSTR
#define myatoi      atoi
#define myatoi64    _atoi64
#else
#define myprintf	swprintf
#define mystrcpy	wcscpy
#define mystrlen	wcslen
#define myscanf		_snwscanf
#define	myLPSTR		LPWSTR
#define myatoi      _wtoi
#define myatoi64	_wtoi64
#endif

	//结束原因
#define GER_NO_PLAYER					0x10								//没有玩家
#define GER_COMPARECARD					0x20								//比牌结束
#define GER_OPENCARD					0x30								//开牌结束

	//游戏状态
#define GS_T_FREE					GAME_STATUS_FREE						//等待开始
#define GS_T_SCORE					GAME_STATUS_PLAY						//叫分状态
#define GS_T_PLAYING				GAME_STATUS_PLAY+1						//游戏进行

#define GAME_USER_MAGER				 536870912								//看牌
#define SERVER_LEN					32										//房间长度

	//定时器标识
#define IDI_START_GAME				200									//开始定时器
#define IDI_USER_ADD_SCORE			201									//加注定时器
#define IDI_USER_COMPARE_CARD		202									//选比牌用户定时器
#define IDI_DISABLE					203									//过滤定时器

	//时间标识
#define TIME_START_GAME				30									//开始定时器
#define TIME_USER_ADD_SCORE			30									//加注定时器
#define	TIME_USER_COMPARE_CARD		30									//比牌定时器

//////////////////////////////////////////////////////////////////////////
//服务器命令结构
#define SUB_S_GAME_START				100									//游戏开始
#define SUB_S_ADD_SCORE					101									//加注结果
#define SUB_S_GIVE_UP					102									//放弃跟注
#define SUB_S_COMPARE_CARD				105									//比牌跟注
#define SUB_S_LOOK_CARD					106									//看牌跟注
#define SUB_S_SEND_CARD					103									//发牌消息
#define SUB_S_GAME_END					104									//游戏结束
#define SUB_S_PLAYER_EXIT				107									//用户强退
#define SUB_S_OPEN_CARD					108									//开牌消息
#define SUB_S_WAIT_COMPARE				109									//等待比牌
#define SUB_S_ANDROID_CARD				110									//智能消息
#define SUB_S_CHEAT_CARD				111									//看牌消息

#define SUB_S_ADMIN_STORAGE_INFO		112									//刷新库存
#define SUB_S_RESULT_ADD_USERROSTER		113									//添加用户名单结果
#define SUB_S_RESULT_DELETE_USERROSTER	114									//删除用户名单结果
#define SUB_S_UPDATE_USERROSTER			115									//更新用户名单
#define SUB_S_REMOVEKEY_USERROSTER		116									//移除用户名单
#define SUB_S_DUPLICATE_USERROSTER		117									//重复用户名单


	struct CMD_S_GetAllCard
	{
		byte	cbCardData[GAME_PLAYER][MAX_COUNT];
	};
	struct CMD_S_WriteUserScore
	{
		word wTargetUser;
	};

	struct tagRobotConfig
	{
		int64						lRobotScoreMin;
		int64						lRobotScoreMax;
		int64						lRobotBankGet;
		int64						lRobotBankGetBanker;
		int64						lRobotBankStoMul;
	};

	//游戏状态
	struct CMD_S_StatusFree
	{
		int64							lCellScore;							//基础积分
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID || CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
		//房间信息
		tchar							szGameRoomName[SERVER_LEN * 2];
#elif (CC_TARGET_PLATFORM == CC_PLATFORM_WIN32)
		tchar							szGameRoomName[SERVER_LEN];				//房间名称
#endif	
		tagRobotConfig						RobotConfig;						//机器人配置
		int64							lStartStorage;						//起始库存
	};

	//游戏状态
	struct CMD_S_StatusPlay
	{
		//加注信息
		int64							lMaxCellScore;						//单元上限
		int64							lCellScore;							//单元下注
		int64							lCurrentTimes;						//当前倍数
		int64							lUserMaxScore;						//用户分数上限

		//状态信息
		word							wBankerUser;						//庄家用户
		word				 			wCurrentUser;						//当前玩家
		byte							cbPlayStatus[GAME_PLAYER];			//游戏状态
		bool							bMingZhu[GAME_PLAYER];				//看牌状态
		int64							lTableScore[GAME_PLAYER];			//下注数目

#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID || CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
		tchar							szGameRoomName[SERVER_LEN * 2];		//房间信息
#elif (CC_TARGET_PLATFORM == CC_PLATFORM_WIN32)
		tchar							szGameRoomName[SERVER_LEN];			//房间名称
#endif

		//扑克信息
		byte								cbHandCardData[3];					//扑克数据

		//状态信息
		bool								bCompareState;						//比牌状态
		tagRobotConfig						RobotConfig;						//机器人配置
		int64							lStartStorage;						//起始库存
	};

	//游戏开始
	struct CMD_S_GameStart
	{
		//下注信息
		int64							lMaxScore;							//最大下注
		int64							lCellScore;							//单元下注
		int64							lCurrentTimes;						//当前倍数
		int64							lUserMaxScore;						//分数上限

		//用户信息
		word								wBankerUser;						//庄家用户
		word				 				wCurrentUser;						//当前玩家
		byte								cbPlayStatus[GAME_PLAYER];			//用户状态
	};

	//用户下注
	struct CMD_S_AddScore
	{
		word								wCurrentUser;						//当前用户
		word								wAddScoreUser;						//加注用户
		word								wCompareState;						//比牌状态
		int64							lAddScoreCount;						//加注数目
		int64							lCurrentTimes;						//当前倍数
	};

	//用户放弃
	struct CMD_S_GiveUp
	{
		word								wGiveUpUser;						//放弃用户
	};

	//比牌数据包
	struct CMD_S_CompareCard
	{
		word								wCurrentUser;						//当前用户
		word								wCompareUser[2];					//比牌用户
		word								wLostUser;							//输牌用户
	};

	//看牌数据包
	struct CMD_S_LookCard
	{
		word								wLookCardUser;						//看牌用户
		byte								cbCardData[MAX_COUNT];				//用户扑克
	};

	//发送扑克
	struct CMD_S_SendCard
	{
		byte								cbCardData[GAME_PLAYER][MAX_COUNT];	//用户扑克
	};

	//开牌数据包
	struct CMD_S_OpenCard
	{
		word								wWinner;							//胜利用户
	};

	//游戏结束
	struct CMD_S_GameEnd
	{
		int64								lGameTax;							//游戏税收
		int64								lGameScore[GAME_PLAYER];			//游戏得分
		byte								cbCardData[GAME_PLAYER][3];			//用户扑克
		word								wCompareUser[GAME_PLAYER][4];		//比牌用户
		word								wEndState;							//结束状态
		bool								bDelayOverGame;						//延迟开始
		word								wServerType;						//房间类型
	};

	//用户退出
	struct CMD_S_PlayerExit
	{
		word								wPlayerID;							//退出用户
	};

	//等待比牌
	struct CMD_S_WaitCompare
	{
		word								wCompareUser;						//比牌用户
	};


	//////////////////////////////////////////////////////////////////////////

	//客户端命令结构
#define SUB_C_ADD_SCORE					1									//用户加注
#define SUB_C_GIVE_UP					2									//放弃消息
#define SUB_C_COMPARE_CARD				3									//比牌消息
#define SUB_C_LOOK_CARD					4									//看牌消息
#define SUB_C_OPEN_CARD					5									//开牌消息
#define SUB_C_WAIT_COMPARE				6									//等待比牌
#define SUB_C_FINISH_FLASH				7									//完成动画
#define SUB_C_ADD_SCORE_TIME			8									//完成动画

	//用户加注
	struct CMD_C_AddScore
	{
		int64							lScore;								//加注数目
		word								wState;								//当前状态
	};

	//比牌数据包
	struct CMD_C_CompareCard
	{
		word								wCompareUser;						//比牌用户
	};

#pragma pack(pop)
}

//////////////////////////////////////////////////////////////////////////

#endif
