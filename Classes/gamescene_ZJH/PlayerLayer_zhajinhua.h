#ifndef PLAYERLAYER_ZHAJINHUA_H_
#define PLAYERLAYER_ZHAJINHUA_H_

#include "cocos2d.h"
#include "PlayerNode_zhajinhua.h"

USING_NS_CC;

class PlayerLayer_zhajinhua : public Layer
{
public:
	PlayerLayer_zhajinhua();
	~PlayerLayer_zhajinhua();

	/*CREATE_FUNC(PlayerLayer_zhajinhua);*/
	static PlayerLayer_zhajinhua * create(Widget * mRoot);

	bool init(Widget * mRoot);

	PlayerNode_zhajinhua * getPlayerNodeById(int chair_id);

	void updateUserScore(cocos2d::Ref * ref);
protected:
	std::vector<PlayerNode_zhajinhua *> mPlayerList;


};

#endif

