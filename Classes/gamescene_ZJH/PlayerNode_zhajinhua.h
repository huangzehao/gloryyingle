#ifndef PLAYERNODE_ZHAJINHUA_H_
#define PLAYERNODE_ZHAJINHUA_H_

#include "cocos2d.h"
#include "ui/UIButton.h"
#include "ui/UIWidget.h"
#include "cocostudio/CocoStudio.h"
#include "ui/CocosGUI.h"

USING_NS_CC;
using namespace cocos2d::ui;
using namespace cocostudio;

//按钮事件
#define ZJH_EVENT_HEAD			"ZJH_EVENT_HEAD"			//头像点击

class PlayerNode_zhajinhua : public Node
{
public:
	PlayerNode_zhajinhua();
	~PlayerNode_zhajinhua();
	//CREATE_FUNC(PlayerNode_zhajinhua);
	bool init(int chair_id, Widget *mRoot);
	static PlayerNode_zhajinhua * create(int chard_id, Widget *mRoot);

	//设置头像
	void setFaceID(int wFaceID);
	//设置昵称
	void setNickName(const char * name);
	//设置积分
	void setScore(long long score);
	//设置用户信息
	void setHeadInfo(bool isVisible);
	void setBuyMoney(long long money);
	void setShowGoldKuang(bool isShow);
	void addMoney(long long money);
	void setIsPrepare(bool isPre);
	
	//清理用户
	void clearUser(bool isLevel);
	//展示喊话框
	void showState(int chair_id, int state);
	//清除喊话状态
	void clearState(cocos2d::Node *target, void* data);
	//显示比牌状态
	void showHeadButton(bool bShow);
	//设置玩家头像动画状态
	void setHeadAnimation(int mType);
	//设置玩家头像为等待状态
	void setHeadFreeAnimation();
//	void setIsPrepare(bool isPre);
	//获取头像
	ImageView * getHead();
	//获取昵称
	Text * getNickName();
	//获取积分
	TextAtlas * getScore();

	//头像点击
	void OnBtnHeadCellBack(Ref * ref, ui::TouchEventType eType);

	

protected:
 	ui::Button * btn_head;
 	Sprite * img_flash_head;
// 	///< 头像背景
// 	Sprite * img_head_bg;
// 	比牌信息
	ImageView * img_compare;
	ImageView * img_arrow;
	///< 用户信息框
	Sprite * img_player_info;
	///< 用户喊话框
	Sprite * img_chat_kuang;
	///< 用户喊话状态
	Sprite * img_chat_state;


	///< 椅子
	int mChairId;
	///< 头像ID
	int mFaceID;

	///<玩家节点主面板
	Widget * mPanelMain;
	///<玩家节点
	ImageView * img_PlayerNode;
	///< 头像
	ImageView * img_head;
	///< 名字
	Text * lab_name;
	///< 自己的钱
	TextAtlas * lab_money;
	///< 玩家已准备标识
	ImageView * img_prepare;
	///< 押注金币框
	ImageView * img_jinbi_kuang;
	///< 压注分数
	Text * lable_score;
	///< 主角分数
	TextAtlas * al_mScore;

	long long total_money;
};

#endif

