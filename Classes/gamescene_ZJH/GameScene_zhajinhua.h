#ifndef _GAMESCENE_ZHAJINHUA_H_
#define _GAMESCENE_ZHAJINHUA_H_

#include "cocos2d.h"
#include "cocos-ext.h"
#include "Kernel/kernel/user/IClientUserItem.h"
#include "common/IKeybackListener.h"
#include "common/TouchLayer.h"
#include "FrameLayer_zhajinhua.h"
#include "PlayerLayer_zhajinhua.h"
#include "Tools/tools/MessageLayer.h"

USING_NS_CC;
USING_NS_CC_EXT;

using namespace Message;

class GameScene_zhajinhua
	: public Scene
	, public ITouchSink
	, public IKeybackListener
{
public:
	//创建方法
	CREATE_FUNC(GameScene_zhajinhua);

	GameScene_zhajinhua();
	~GameScene_zhajinhua();

	//初始化方法
	virtual bool init();

	static GameScene_zhajinhua* shared();

	///< 初始化游戏基础数据
	static void initGameBaseData();

public:
	//场景动画完成
	virtual void onEnterTransitionDidFinish();
	//场景动画开始
	virtual void onExitTransitionDidStart();

public:
	virtual void onKeybackClicked();
	void onBackToRoom(Node* node);

	///< 退出游戏
	void closeGameNotify(cocos2d::Ref * ref);

	//触摸层
public:
	virtual bool onTouchBegan(Touch *pTouch, Event *pEvent);
	virtual void onTouchMoved(Touch *pTouch, Event *pEvent);
	virtual void onTouchEnded(Touch *pTouch, Event *pEvent);

	//断线重连
private:
	void func_Reconnect_on_loss(cocos2d::Ref * obj);
	void reconnect_on_loss(float dt);
public:
	static bool is_Reconnect_on_loss();
	///< 倒计时退出游戏
	void  ExitGameTiming();
	///< 注销内核退出游戏
	void  CloseKernelExitGame(float dt);

	//系统消息
public:
	void add_msg(const std::string& msg);					//添加消息列表
	void func_msg_show();									//消息显示
	void func_msg_start_next();								//读取下一条消息
	void msg_start_next(bool bRemoveFront);				 	//显示下一条消息

	//回调事件
private:
	//关闭游戏
	void closeCallBack(Node* obj);
	//功能函数
public:
	void net_user_state_update(int chair_id, byte user_state);
	/// 显示分数结算
	void showScoreTips(int charId, int64 score = 0);
	//显示用户
	void showUserInfo(IClientUserItem * pIClientUserItem, byte cbUserStatus);
	void clearPlayerPrepare();
	//用户离开
	void netUserLevel(word wChairID);
	///< 清理其他用户
/*	void clearOtherUser(int chair_id);*/
	///< 清理牌数据
	void clearCardData(int chair_id);
	//显示动作
	void showUserState(word wChairID, word wState);

	//消息变量
public:
	typedef std::list<std::string>		MSG_LIST;			//消息列表
	typedef MSG_LIST::iterator			MSG_LIST_ITER;
	MSG_LIST							mMessageList;		//消息列表对象
	Label*								mMsgContents[2];	//消息内容
	int									mMsgContentIndex;	//消息内容索引

	//场景变量
private:
	TouchLayer*		    touch_layer_;		//触摸层
public:
	FrameLayer_zhajinhua* mFrameLayer;		//主面板
	PlayerLayer_zhajinhua*	mPlayerLayer;	//玩家层
	MessageLayer*			mMessageLayer;	///< 消息层
};

#endif
