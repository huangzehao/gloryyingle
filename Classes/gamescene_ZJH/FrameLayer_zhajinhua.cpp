#include "Tools/tools/MTNotification.h"
#include "Tools/tools/StringData.h"
#include "ClientKernelSink_zhajinhua.h"
#include "Kernel/kernel/game/IClientKernel.h"
#include "Tools/Manager/SpriteHelper.h"
#include "Tools/Manager/SoundManager.h"
#include "common/GameSetLayer.h"
#include "Tools/tools/SimpleTools.h"
#include "Tools/Dialog/NewDialog.h"
#include "Tools/tools/Convert.h"
#include "Tools/ViewHeader.h"
#include "ui/UIWidget.h"

#include "FrameLayer_zhajinhua.h"
//#include "GameScene_zhajinhua.h"

#define MAX_JETTON_INDEX		18									//筹码面值数目
#define MAX_DRAW				30									//绘画最大筹码数

const int CARD_POSTION[][2] = {
	{ 1200, 400 },
	{ 1100, 220 },
	{ 815, 195 },
	{ 300, 220 },
	{ 200, 400 },
};

//筹码面值
const SCORE g_lJetonIndex[MAX_JETTON_INDEX] = { 10, 20, 50, 100, 200, 500, 1000, 2000, 5000, 10000, 20000, 50000, 100000, 200000, 500000, 1000000, 2000000, 5000000 };
//筹码资源路径
const std::string g_PathJettonIndex[MAX_JETTON_INDEX] = {
	"zhajinhuaGameScene/zhajinhua/Button_Jetton_10_1.png",
	"zhajinhuaGameScene/zhajinhua/Button_Jetton_20_1.png",
	"zhajinhuaGameScene/zhajinhua/Button_Jetton_50_1.png",
	"zhajinhuaGameScene/zhajinhua/Button_Jetton_100_1.png",
	"zhajinhuaGameScene/zhajinhua/Button_Jetton_200_1.png",
	"zhajinhuaGameScene/zhajinhua/Button_Jetton_500_1.png",
	"zhajinhuaGameScene/zhajinhua/Button_Jetton_1000_1.png",
	"zhajinhuaGameScene/zhajinhua/Button_Jetton_2000_1.png",
	"zhajinhuaGameScene/zhajinhua/Button_Jetton_5000_1.png",
	"zhajinhuaGameScene/zhajinhua/Button_Jetton_1Wan_1.png",
	"zhajinhuaGameScene/zhajinhua/Button_Jetton_2Wan_1.png",
	"zhajinhuaGameScene/zhajinhua/Button_Jetton_5Wan_1.png",
	"zhajinhuaGameScene/zhajinhua/Button_Jetton_10Wan_1.png",
	"zhajinhuaGameScene/zhajinhua/Button_Jetton_20Wan_1.png",
	"zhajinhuaGameScene/zhajinhua/Button_Jetton_50Wan_1.png",
	"zhajinhuaGameScene/zhajinhua/Button_Jetton_100Wan_1.png",
	"zhajinhuaGameScene/zhajinhua/Button_Jetton_200Wan_1.png",
	"zhajinhuaGameScene/zhajinhua/Button_Jetton_500Wan_1.png"
};

FrameLayer_zhajinhua::FrameLayer_zhajinhua()
{
	m_wActionCount = 0;
	for (word i = 0; i < GAME_PLAYER; i++)
	{
		m_lStopUpdataScore[i] = 0;
		m_wFlashUser[i] = INVALID_CHAIR;
	}
	mWClockTime = 0;
	mBFollowAll = false;
	m_wCompareUser[0] = INVALID_CHAIR;
	m_wCompareUser[1] = INVALID_CHAIR;
	m_wLostUser = INVALID_CHAIR;
	m_bCompareCard = false;
	m_wWaitUserChoice = INVALID_CHAIR;
	
	//筹码压缩
	m_lTableScoreAll = 0;
}

FrameLayer_zhajinhua::~FrameLayer_zhajinhua()
{
}

FrameLayer_zhajinhua * FrameLayer_zhajinhua::create(Widget *mRoot)
{
	FrameLayer_zhajinhua * node = new FrameLayer_zhajinhua;
	if (node && node->init(mRoot))
	{
		node->autorelease();
		return node;
	}
	delete node;
	return nullptr;
}

//初始化
bool FrameLayer_zhajinhua::init(Widget * mRoot)
{
	if (!Layer::init())
		return false;

	//游戏变量
	mBFollowAll = false;
	m_wActionCount = 0;

	//位置计算
	Size winSize = Director::getInstance()->getWinSize();

	mPtSendCard.setPoint(winSize.width / 2, winSize.height / 4 * 3);
	mPtUserCpmpare.setPoint(winSize.width / 2, winSize.height / 4 * 3 - 50);

	mPtClock[0].setPoint(940, 516);
	mPtClock[1].setPoint(940, 286);
	mPtClock[2].setPoint(800, 300);
	mPtClock[3].setPoint(410, 305);
	mPtClock[4].setPoint(410, 530);

	mPtCard[0].setPoint(1100, 540);
	mPtCard[1].setPoint(1100, 280);
	mPtCard[2].setPoint(710, 235);
	mPtCard[3].setPoint(280, 280);
	mPtCard[4].setPoint(280, 540);

	mPtUserJetton[0].setPoint(1300, 550);
	mPtUserJetton[1].setPoint(1330, 300);
	mPtUserJetton[2].setPoint(560, 220);
	mPtUserJetton[3].setPoint(150, 300);
	mPtUserJetton[4].setPoint(150, 550);
	mPtAllJetton.setPoint(winSize.width / 2, winSize.height / 2 + 80);

	mPtPerson[0].setPoint(1220, 530);
	mPtPerson[1].setPoint(1220, 305);
	mPtPerson[2].setPoint(315, 70);
	mPtPerson[3].setPoint(200, 305);
	mPtPerson[4].setPoint(200, 530);

	mPtBanker[0].setPoint(1220, 655);
	mPtBanker[1].setPoint(1220, 380);
	mPtBanker[2].setPoint(630, 310);
	mPtBanker[3].setPoint(200, 380);
	mPtBanker[4].setPoint(200, 655);

	mPtAddScore[0].setPoint(1120,632);
	mPtAddScore[1].setPoint(1120,372);
	mPtAddScore[2].setPoint(567,345);
	mPtAddScore[3].setPoint(320,372);
	mPtAddScore[4].setPoint(320,632);

	mPtWinLostScore[0].setPoint(1120, 662);
	mPtWinLostScore[1].setPoint(1120, 403);
	mPtWinLostScore[2].setPoint(715, 345);
	mPtWinLostScore[3].setPoint(320, 403);
	mPtWinLostScore[4].setPoint(320, 662);

	createMainPanel(mRoot);

	return true;
}
//创建主面板
void FrameLayer_zhajinhua::createMainPanel(Widget *mRoot)
{
// 	//窗口大小
// 	Size winSize = Director::getInstance()->getWinSize();

	//加载布局文件
	Image_bg = dynamic_cast<ImageView *>(mRoot->getChildByName("Image_bg"));

	Widget * Panel_Main = dynamic_cast<Widget *>(mRoot->getChildByName("Panel_Main"));
	mPanelMain = Panel_Main;

	mPanelJetton = dynamic_cast<Widget*>(mRoot->getChildByName("Panel_Jetton"));
	this->addChild(mRoot,0);
	//创建控件
	mBtnReturn = dynamic_cast<Button *>(mPanelMain->getChildByName("btn_Return"));
	mBtnReturn->addTouchEventListener(this, SEL_TouchEvent(&FrameLayer_zhajinhua::OnBtnReturnCellBack));

	mBtnStart = dynamic_cast<Button *>(mPanelMain->getChildByName("btn_start"));
	mBtnStart->addTouchEventListener(this, SEL_TouchEvent(&FrameLayer_zhajinhua::OnBtnReadyCellBack));

// 	mBtnAllin = dynamic_cast<Button *>(mPanelMain->getChildByName("Button_Allin"));
// 	mBtnAllin->addTouchEventListener(this, SEL_TouchEvent(&FrameLayer_zhajinhua::OnBtnAllinCellBack));

	mBtnCompare = dynamic_cast<Button *>(mPanelMain->getChildByName("btn_Compare"));
	mBtnCompare->addTouchEventListener(this, SEL_TouchEvent(&FrameLayer_zhajinhua::OnBtnCompareCellBack));

	img_compare = ImageView::create("zhajinhuaGameScene/zhajinhua/compare_notice.png");
	img_compare->setPosition(ccp(710, 450));
	this->addChild(img_compare, 150);
	img_compare->setVisible(false);

	mBtnRaise = dynamic_cast<Button *>(mPanelMain->getChildByName("btn_Raise"));
	mBtnRaise->addTouchEventListener(this, SEL_TouchEvent(&FrameLayer_zhajinhua::OnBtnRaiseCellBack));

	mBtnSee = dynamic_cast<Button *>(mPanelMain->getChildByName("btn_See"));
	mBtnSee->addTouchEventListener(this, SEL_TouchEvent(&FrameLayer_zhajinhua::OnBtnSeeCellBack));

	mBtnFlod = dynamic_cast<Button *>(mPanelMain->getChildByName("btn_Fold"));
	mBtnFlod->addTouchEventListener(this, SEL_TouchEvent(&FrameLayer_zhajinhua::OnBtnFlodCellBack));
	
	mBtnFollow = dynamic_cast<Button *>(mPanelMain->getChildByName("btn_Follow"));
	mBtnFollow->addTouchEventListener(this, SEL_TouchEvent(&FrameLayer_zhajinhua::OnBenFollowCellBack));
	
// 	mBtnOpen = dynamic_cast<Button *>(mPanelMain->getChildByName("btn_Open"));
// 	mBtnOpen->addTouchEventListener(this, SEL_TouchEvent(&FrameLayer_zhajinhua::OnButtonOpenCellBack));
	
	mBtnJetton_1 = dynamic_cast<Button*>(mPanelJetton->getChildByName("Button_Jetton1"));
	mBtnJetton_1->addTouchEventListener(this, SEL_TouchEvent(&FrameLayer_zhajinhua::OnButtonJettonCellBack));
	
	mBtnJetton_2 = dynamic_cast<Button*>(mPanelJetton->getChildByName("Button_Jetton2"));
	mBtnJetton_2->addTouchEventListener(this, SEL_TouchEvent(&FrameLayer_zhajinhua::OnButtonJettonCellBack));
	
	mBtnJetton_3 = dynamic_cast<Button*>(mPanelJetton->getChildByName("Button_Jetton3"));
	mBtnJetton_3->addTouchEventListener(this, SEL_TouchEvent(&FrameLayer_zhajinhua::OnButtonJettonCellBack));

	mBtnJetton_close = dynamic_cast<Button *>(mPanelJetton->getChildByName("btn_Jetton_close"));
	mBtnJetton_close->addTouchEventListener(this, SEL_TouchEvent(&FrameLayer_zhajinhua::OnButtonJettonClose));

	mCheckFollowAll = dynamic_cast<CheckBox *>(mPanelMain->getChildByName("btn_FollowAll"));
	mCheckFollowAll->addEventListenerCheckBox(this, SEL_SelectedStateEvent(&FrameLayer_zhajinhua::OnCheckFollowAllCellBack));

	mClockTime = dynamic_cast<ImageView*>(mPanelMain->getChildByName("img_clock_bg"));	
	
	mScoreTopBack = dynamic_cast<ImageView*>(mPanelMain->getChildByName("Image_ScoreTopBack"));

/*	std::string strReady;*/
	for (word i = 0; i < GAME_PLAYER; i++)
	{
		TextAtlas * mLabAddScore = TextAtlas::create("100", "zhajinhuaGameScene/zhajinhua/szp_zcm_num_Decry.png", 17, 24, "0");
		mLabAddScore->setPosition(ccp(100,22.5));
		mLabAddScore->setAnchorPoint(ccp(0.5, 0.5));
		mLabAddScore->setScale(1.3);
		mLabAddScore->setName("mLabAddScore");

		mLabAddScoreBG[i] = ImageView::create("zhajinhuaGameScene/zhajinhua_Main/img_gold_kuang.png");
		mLabAddScoreBG[i]->setScale(0.8);
		mLabAddScoreBG[i]->addChild(mLabAddScore);
		mLabAddScoreBG[i]->setPosition(mPtAddScore[i]);
		mLabAddScoreBG[i]->setVisible(false);
		this->addChild(mLabAddScoreBG[i]);

		//输赢分数
		TextAtlas * TextScore = TextAtlas::create("100", "zhajinhuaGameScene/zhajinhua/num_yellow.png", 21, 36, "0");
		TextScore->setAnchorPoint(ccp(0, 1));
		TextScore->setPosition(ccp(30,30.5));
		TextScore->setScale(0.7);
		TextScore->setName("lEndScore");
		mImgWinLostScore[i] = ImageView::create("zhajinhuaGameScene/zhajinhua/num_yellow_zheng.png");
		mImgWinLostScore[i]->addChild(TextScore);
		mImgWinLostScore[i]->setPosition(ccp(mPtWinLostScore[i].x -45, mPtWinLostScore[i].y));
		mImgWinLostScore[i]->setVisible(false);
		this->addChild(mImgWinLostScore[i]);
	}

 	mImgBanker = ImageView::create("zhajinhuaGameScene/zhajinhua/img_banker.png");
 	mImgBanker->setVisible(false);
	this->addChild(mImgBanker);

	//设置回调

	//加载帧动画
	CCSpriteFrameCache::sharedSpriteFrameCache()->addSpriteFramesWithFile("zhajinhuaGameScene/zhajinhua/vscard.plist");
}

void FrameLayer_zhajinhua::setCloseDialogInfo(cocos2d::Ref* target, cocos2d::SEL_CallFuncN callfun, const std::string& title, const std::string& content)
{
	mTarget = target;
	mCallback = callfun;
	mTitle = title;
	mContent = content;
}
//退出游戏
void FrameLayer_zhajinhua::OnBtnReturnCellBack(Ref * ref, TouchEventType eType)
{
	if (eType != TOUCH_EVENT_ENDED)
		return;

	ui::Button * btn_now = dynamic_cast<ui::Button *>(ref);
	int tag = btn_now->getTag();
	btn_now->setTag(DLG_MB_OK);
	IClientKernel * kernel = IClientKernel::get();

	if (!kernel && kernel->GetGameStatus() == GAME_STATUS_FREE)
	{
		(mTarget->*mCallback)(btn_now);
	}
	else
	{
		NewDialog::create(SSTRING("back_to_room_content"), NewDialog::AFFIRMANDCANCEL, [=]()
		{
			(mTarget->*mCallback)(btn_now);
		});
	}
}
//准备
void FrameLayer_zhajinhua::OnBtnReadyCellBack(Ref * ref, TouchEventType eType)
{
	if (eType != TOUCH_EVENT_ENDED)
		return;

	//初始化控件
	InitControl();

	//回调事件
	G_NOTIFY(ZJH_EVENT_READY);

}
//全押
void FrameLayer_zhajinhua::OnBtnAllinCellBack(Ref * ref, TouchEventType eType)
{
	if (eType != TOUCH_EVENT_ENDED)
		return;

	G_NOTIFY(ZJH_EVENT_ALLIN);
}
//比牌
void FrameLayer_zhajinhua::OnBtnCompareCellBack(Ref * ref, TouchEventType eType)
{
	if (eType != TOUCH_EVENT_ENDED)
		return;
	if (GAME_PLAYER == 2)
	{
	}
	img_compare->setVisible(true);
	SetBtnShow(ZJH_VALUE_RAISE | ZJH_VALUE_SEE | ZJH_VALUE_COMPARE | ZJH_VALUE_FLOD | ZJH_VALUE_FOLLOW);
	mBtnRaise->setEnabled(false);
	mBtnCompare->setEnabled(false);
	mBtnFlod->setEnabled(false);
	mBtnSee->setEnabled(false);
	mBtnFollow->setEnabled(false);

	mBtnRaise->setBright(false);
	mBtnCompare->setBright(false);
	mBtnFlod->setBright(false);
	mBtnSee->setBright(false);
	mBtnFollow->setBright(false);

	G_NOTIFY(ZJH_EVENT_COMPARE);
}
//加注
void FrameLayer_zhajinhua::OnBtnRaiseCellBack(Ref * ref, TouchEventType eType)
{
	if (eType != TOUCH_EVENT_ENDED)
		return;
	if (mPanelJetton->isVisible())
		mPanelJetton->setVisible(false);
	else
		mPanelJetton->setVisible(true);
}

void FrameLayer_zhajinhua::OnButtonJettonClose(Ref * ref, TouchEventType eType)
{
	if (eType != TOUCH_EVENT_ENDED)
		return;
		mPanelJetton->setVisible(false);
}
//看牌
void FrameLayer_zhajinhua::OnBtnSeeCellBack(Ref * ref, TouchEventType eType)
{
	if (eType != TOUCH_EVENT_ENDED)
		return;

	G_NOTIFY(ZJH_EVENT_SEE);
}
//弃牌
void FrameLayer_zhajinhua::OnBtnFlodCellBack(Ref * ref, TouchEventType eType)
{
	if (eType != TOUCH_EVENT_ENDED)
		return;

	G_NOTIFY(ZJH_EVENT_FLOD);
}
//跟注
void FrameLayer_zhajinhua::OnBenFollowCellBack(Ref * ref, TouchEventType eType)
{
	if (eType != TOUCH_EVENT_ENDED)
		return;

	IClientKernel * kernel = IClientKernel::get();
	word wMeChairID = kernel->GetMeChairID();
	ClientKernelSink_zhajinhua * kernelSink = ClientKernelSink_zhajinhua::getInstance();

	//处理按钮
	if (kernelSink->m_bMingZhu[wMeChairID])
	{
		mBtnSee->setEnabled(false);
		mBtnSee->setBright(false);
	}
	else
	{
		mBtnSee->setEnabled(true);
		mBtnSee->setBright(true);
	}

	G_NOTIFY(ZJH_EVENT_FOLLOW);
}
//加注
void FrameLayer_zhajinhua::OnButtonJettonCellBack(Ref * ref, TouchEventType eType)
{
	if (eType != TOUCH_EVENT_ENDED)
		return;

	IClientKernel * kernel = IClientKernel::get();
	word wMeChairID = kernel->GetMeChairID();
	ClientKernelSink_zhajinhua * kernelSink = ClientKernelSink_zhajinhua::getInstance();

	//处理按钮
	SetBtnShow(ZJH_VALUE_FLOD | ZJH_VALUE_FOLLOWALL);
	if (kernelSink->m_bMingZhu[wMeChairID])
	{
		mBtnSee->setEnabled(false);
		mBtnSee->setBright(false);
	}
	else
	{
		mBtnSee->setEnabled(true);
		mBtnSee->setBright(true);
	}

	//回调事件
	G_NOTIFY_D(ZJH_EVENT_RAISE, ref);
}
//跟到底
void FrameLayer_zhajinhua::OnCheckFollowAllCellBack(Ref * ref, CheckBoxEventType eType)
{
	if (eType == CHECKBOX_STATE_EVENT_SELECTED)
		mBFollowAll = true;
	else
		mBFollowAll = false;
	return;
}
//开牌
void FrameLayer_zhajinhua::OnButtonOpenCellBack(Ref * ref, TouchEventType eType)
{
	if (eType == TOUCH_EVENT_ENDED)
		return;

	SetBtnShow(ZJH_VALUE_NULL);

	G_NOTIFY(ZJH_EVENT_OPEN);
}
//设置时钟
void FrameLayer_zhajinhua::SetClockShow(word wChairID, uint nElapse, word wClockID)
{
	dynamic_cast<TextAtlas*>(mClockTime->getChildByName("img_clock_time"))->setString(StringUtils::format("%d", nElapse));
	mClockTime->setPosition(mPtClock[wChairID]);
	mClockTime->setVisible(true);


	switch (wClockID)
	{
		case IDI_START_GAME:			//等待开始
		{
											if (nElapse <= 0)
												this->scheduleOnce(SEL_SCHEDULE(&FrameLayer_zhajinhua::OnTimeStartGame), 1);
											break;
		}
		case IDI_USER_ADD_SCORE:		//加注
		{
// 											if (mBFollowAll)
// 												OnBenFollowCellBack(nullptr, TOUCH_EVENT_ENDED);
											break;
		}
		case IDI_USER_COMPARE_CARD:		//等待比牌
		{

										//清理界面
										//m_GameClientView.SetCompareCard(false, NULL);
									  break;
		}
		case IDI_DISABLE:
		{
							break;
		}
		default:
		break;
	}

	if (nElapse <= 0)
		mClockTime->setVisible(false);
	return;
}
//准备时间
void FrameLayer_zhajinhua::OnTimeStartGame(float dt)
{
	int tag = mBtnStart->getTag();
	mBtnStart->setTag(DLG_MB_OK);
	IClientKernel * kernel = IClientKernel::get();

	if (kernel->GetGameStatus() == GAME_STATUS_FREE)
	{
		(mTarget->*mCallback)(mBtnStart);
	}
	else
	{
		NewDialog::create(SSTRING("back_to_room_content"), NewDialog::AFFIRMANDCANCEL, [=]()
		{
			(mTarget->*mCallback)(mBtnStart);
		});
	}
}
//初始化控件
void FrameLayer_zhajinhua::InitControl()
{
	//设置控件
	mBtnStart->setVisible(false);
	mImgBanker->setVisible(false);

	mBtnStart->setEnabled(true);
//	mBtnAllin->setEnabled(true);
	mBtnCompare->setEnabled(true);
	mBtnRaise->setEnabled(true);
	mBtnSee->setEnabled(true);
	mBtnFlod->setEnabled(true);
	mBtnFollow->setEnabled(true);
	mBtnJetton_1->setEnabled(true);
	mBtnJetton_2->setEnabled(true);
	mBtnJetton_3->setEnabled(true);
	mCheckFollowAll->setEnabled(true);

	mBtnStart->setBright(true);
//	mBtnAllin->setBright(true);
	mBtnCompare->setBright(true);
	mBtnRaise->setBright(true);
	mBtnSee->setBright(true);
	mBtnFlod->setBright(true);
	mBtnFollow->setBright(true);
	mBtnJetton_1->setBright(true);
	mBtnJetton_2->setBright(true);
	mBtnJetton_3->setBright(true);
	mCheckFollowAll->setBright(true);

	mCheckFollowAll->setSelected(false);

	SetBtnShow(ZJH_VALUE_NULL);


	for (word i = 0; i < GAME_PLAYER; i++)
	{
		//清理牌
		for (word j = 0; j < MAX_COUNT; j++)
		{
			this->removeChild(mCardList[i][j]);
		}
		//清理结算分数
		dynamic_cast<TextAtlas*>(mImgWinLostScore[i]->getChildByName("lEndScore"))->setString("");
		mImgWinLostScore[i]->setVisible(false);
	}

	mBFollowAll = false;
	m_bCompareCard = false;
	m_lTableScoreAll = 0;

	SetUserTableScore(INVALID_CHAIR, 0L, 0L);
}
//设置按钮显示
void FrameLayer_zhajinhua::SetBtnShow(word nBtnValue)
{
	if ((nBtnValue & ZJH_VALUE_READY) != 0)
		mBtnStart->setVisible(true);
	else
		mBtnStart->setVisible(false);

// 	if ((nBtnValue & ZJH_VALUE_ALLIN) != 0)
// 		mBtnAllin->setVisible(true);
// 	else
// 		mBtnAllin->setVisible(false);

	if ((nBtnValue & ZJH_VALUE_COMPARE) != 0)
		mBtnCompare->setVisible(true);
	else
		mBtnCompare->setVisible(false);

	if ((nBtnValue & ZJH_VALUE_RAISE) != 0)
		mBtnRaise->setVisible(true);
	else
		mBtnRaise->setVisible(false);

	if ((nBtnValue & ZJH_VALUE_SEE) != 0)
		mBtnSee->setVisible(true);
	else
		mBtnSee->setVisible(false);

	if ((nBtnValue & ZJH_VALUE_FLOD) != 0)
		mBtnFlod->setVisible(true);
	else
		mBtnFlod->setVisible(false);

	if ((nBtnValue & ZJH_VALUE_FOLLOW) != 0)
		mBtnFollow->setVisible(true);
	else
		mBtnFollow->setVisible(false);

	if ((nBtnValue & ZJH_VALUE_FOLLOWALL) != 0)
		mCheckFollowAll->setVisible(true);
	else
		mCheckFollowAll->setVisible(false);

	if ((nBtnValue&ZJH_VALUE_JETTON))
		mPanelJetton->setVisible(true);
	else
		mPanelJetton->setVisible(false);

// 	if ((nBtnValue&ZJH_VALUE_OPEN))
// 		mBtnOpen->setVisible(true);
// 	else
// 		mBtnOpen->setVisible(false);
}
//设置按钮状态
void FrameLayer_zhajinhua::SetBtnEnable(word nBtnValue)
{
// 	if ((nBtnValue & ZJH_VALUE_READY) != 0)
// 		mBtnReady->setEnabled(true);
// 	else
// 		mBtnReady->setEnabled(false);

// 	if ((nBtnValue & ZJH_VALUE_ALLIN) != 0)
// 		mBtnAllin->setEnabled(true);
// 	else
// 		mBtnAllin->setEnabled(false);

	if ((nBtnValue & ZJH_VALUE_COMPARE) != 0)
		mBtnCompare->setEnabled(true);
	else
		mBtnCompare->setEnabled(false);

	if ((nBtnValue & ZJH_VALUE_RAISE) != 0)
		mBtnRaise->setEnabled(true);
	else
		mBtnRaise->setVisible(false);

	if ((nBtnValue & ZJH_VALUE_SEE) != 0)
		mBtnSee->setEnabled(true);
	else
		mBtnSee->setEnabled(false);

	if ((nBtnValue & ZJH_VALUE_FLOD) != 0)
		mBtnFlod->setEnabled(true);
	else
		mBtnFlod->setEnabled(false);

	if ((nBtnValue & ZJH_VALUE_FOLLOW) != 0)
		mBtnFollow->setEnabled(true);
	else
		mBtnFollow->setEnabled(false);

// 	if ((nBtnValue & ZJH_VALUE_OPEN) != 0)
// 		mBtnOpen->setEnabled(true);
// 	else
// 		mBtnOpen->setEnabled(false);

	if ((nBtnValue & ZJH_VALUE_FOLLOWALL) != 0)
		mCheckFollowAll->setEnabled(true);
	else
		mCheckFollowAll->setEnabled(false);

	if ((nBtnValue&ZJH_VALUE_JETTON1) || (nBtnValue&ZJH_VALUE_JETTON2) || (nBtnValue&ZJH_VALUE_JETTON3))
	{
		mPanelJetton->setVisible(true);
		if ((nBtnValue & ZJH_VALUE_JETTON1) != 0)
			mBtnJetton_1->setEnabled(true);
		else
			mBtnJetton_1->setEnabled(false);

		if ((nBtnValue & ZJH_VALUE_JETTON2) != 0)
			mBtnJetton_2->setEnabled(true);
		else
			mBtnJetton_2->setEnabled(false);

		if ((nBtnValue & ZJH_VALUE_JETTON3) != 0)
			mBtnJetton_3->setEnabled(true);
		else
			mBtnJetton_3->setEnabled(false);
	}
	else
		mPanelJetton->setVisible(false);
}
//分数限制提示
void FrameLayer_zhajinhua::SetScoreInfo(SCORE lCellScore, SCORE lMaxCellScore)
{
	if (lCellScore > 0)
	{
		dynamic_cast<TextAtlas*>(mScoreTopBack->getChildByName("AtlasLabel_MaxCellScore"))->setString(StringUtils::format("%lld", lMaxCellScore));
		dynamic_cast<TextAtlas*>(mScoreTopBack->getChildByName("AtlasLabel_CellScore"))->setString(StringUtils::format("%lld", lCellScore));
	}

	switch (lCellScore)
	{
		case 10:
		{
				   m_lAddScoreBtn[0] = 10;
				   m_lAddScoreBtn[1] = 20;
				   m_lAddScoreBtn[2] = 50;

				   //按钮
				   mBtnJetton_1->setTag(10);
				   mBtnJetton_2->setTag(20);
				   mBtnJetton_3->setTag(50);
				   mBtnJetton_1->loadTextures("zhajinhuaGameScene/zhajinhua/Button_Jetton_10_1.png","","zhajinhuaGameScene/zhajinhua/Button_Jetton_10_3.png");
				   mBtnJetton_2->loadTextures("zhajinhuaGameScene/zhajinhua/Button_Jetton_20_1.png", "", "zhajinhuaGameScene/zhajinhua/Button_Jetton_20_3.png");
				   mBtnJetton_3->loadTextures("zhajinhuaGameScene/zhajinhua/Button_Jetton_50_1.png", "", "zhajinhuaGameScene/zhajinhua/Button_Jetton_50_3.png");

				   break;
		}
		case 100:
		{
					m_lAddScoreBtn[0] = 100;
					m_lAddScoreBtn[1] = 200;
					m_lAddScoreBtn[2] = 500;
					mBtnJetton_1->setTag(100);
					mBtnJetton_2->setTag(200);
					mBtnJetton_3->setTag(500);
					mBtnJetton_1->loadTextures("zhajinhuaGameScene/zhajinhua/Button_Jetton_100_1.png", "", "zhajinhuaGameScene/zhajinhua/Button_Jetton_100_3.png");
					mBtnJetton_2->loadTextures("zhajinhuaGameScene/zhajinhua/Button_Jetton_200_1.png", "", "zhajinhuaGameScene/zhajinhua/Button_Jetton_200_3.png");
					mBtnJetton_3->loadTextures("zhajinhuaGameScene/zhajinhua/Button_Jetton_500_1.png", "", "zhajinhuaGameScene/zhajinhua/Button_Jetton_500_3.png");

					break;
		}
		case 1000:
		{
					 m_lAddScoreBtn[0] = 1000;
					 m_lAddScoreBtn[1] = 2000;
					 m_lAddScoreBtn[2] = 5000;
					 mBtnJetton_1->setTag(1000);
					 mBtnJetton_2->setTag(2000);
					 mBtnJetton_3->setTag(5000);
					 mBtnJetton_1->loadTextures("zhajinhuaGameScene/zhajinhua/Button_Jetton_1000_1.png", "", "zhajinhuaGameScene/zhajinhua/Button_Jetton_1000_3.png");
					 mBtnJetton_2->loadTextures("zhajinhuaGameScene/zhajinhua/Button_Jetton_2000_1.png", "", "zhajinhuaGameScene/zhajinhua/Button_Jetton_2000_3.png");
					 mBtnJetton_3->loadTextures("zhajinhuaGameScene/zhajinhua/Button_Jetton_5000_1.png", "", "zhajinhuaGameScene/zhajinhua/Button_Jetton_5000_3.png");

					 break;
		}
		case 10000:
		{
					  m_lAddScoreBtn[0] = 10000;
					  m_lAddScoreBtn[1] = 20000;
					  m_lAddScoreBtn[2] = 50000;
					  mBtnJetton_1->setTag(10000);
					  mBtnJetton_2->setTag(20000);
					  mBtnJetton_3->setTag(50000);
					  mBtnJetton_1->loadTextures("zhajinhuaGameScene/zhajinhua/Button_Jetton_1Wan_1.png", "", "zhajinhuaGameScene/zhajinhua/Button_Jetton_1Wan_3.png");
					  mBtnJetton_2->loadTextures("zhajinhuaGameScene/zhajinhua/Button_Jetton_2Wan_1.png", "", "zhajinhuaGameScene/zhajinhua/Button_Jetton_2Wan_3.png");
					  mBtnJetton_3->loadTextures("zhajinhuaGameScene/zhajinhua/Button_Jetton_5Wan_1.png", "", "zhajinhuaGameScene/zhajinhua/Button_Jetton_5Wan_3.png");

					  break;
		}
		case 100000:
		{
					   m_lAddScoreBtn[0] = 100000;
					   m_lAddScoreBtn[1] = 200000;
					   m_lAddScoreBtn[2] = 500000;
					   mBtnJetton_1->setTag(100000);
					   mBtnJetton_2->setTag(200000);
					   mBtnJetton_3->setTag(500000);
					   mBtnJetton_1->loadTextures("zhajinhuaGameScene/zhajinhua/Button_Jetton_10Wan_1.png", "", "zhajinhuaGameScene/zhajinhua/Button_Jetton_10Wan_3.png");
					   mBtnJetton_2->loadTextures("zhajinhuaGameScene/zhajinhua/Button_Jetton_20Wan_1.png", "", "zhajinhuaGameScene/zhajinhua/Button_Jetton_20Wan_3.png");
					   mBtnJetton_3->loadTextures("zhajinhuaGameScene/zhajinhua/Button_Jetton_50Wan_1.png", "", "zhajinhuaGameScene/zhajinhua/Button_Jetton_50Wan_3.png");

					   break;
		}
		case 1000000:
		{
						m_lAddScoreBtn[0] = 1000000;
						m_lAddScoreBtn[1] = 2000000;
						m_lAddScoreBtn[2] = 5000000;
						mBtnJetton_1->setTag(1000000);
						mBtnJetton_2->setTag(2000000);
						mBtnJetton_3->setTag(5000000);
						mBtnJetton_1->loadTextures("zhajinhuaGameScene/zhajinhua/Button_Jetton_100Wan_1.png", "", "zhajinhuaGameScene/zhajinhua/Button_Jetton_100Wan_3.png");
						mBtnJetton_2->loadTextures("zhajinhuaGameScene/zhajinhua/Button_Jetton_200Wan_1.png", "", "zhajinhuaGameScene/zhajinhua/Button_Jetton_200Wan_3.png");
						mBtnJetton_3->loadTextures("zhajinhuaGameScene/zhajinhua/Button_Jetton_500Wan_1.png", "", "zhajinhuaGameScene/zhajinhua/Button_Jetton_500Wan_3.png");

						break;
		}
	}
}
//桌面筹码
void FrameLayer_zhajinhua::SetUserTableScore(word wChairID, SCORE lTableScore, SCORE lCurrentScore, bool bGamePlay /* = false */)
{

	if (wChairID != INVALID_CHAIR)
	{
		if (bGamePlay)
		{
			Sprite * Jetton = nullptr;
			/*g_lJetonIndex[nIndex]*/
			int nIndex = MAX_JETTON_INDEX - 1;
			while (lTableScore > 0)
			{
				if (lTableScore >= g_lJetonIndex[nIndex])
				{
					lTableScore -= g_lJetonIndex[nIndex];
					Jetton = Sprite::create(g_PathJettonIndex[nIndex]);
					Vec2 new_pos = SimpleTools::getConfineToRandomVec2(mPtAllJetton, 100);
					Jetton->setPosition(new_pos);
					Jetton->setScale(0.5);
					this->addChild(Jetton,100);
					mJettonVector.push_back(Jetton);
				}
				else
				{
					nIndex--;
					if (nIndex < 0)
						break;
				}
			}
		}
		dynamic_cast<TextAtlas*>(mLabAddScoreBG[wChairID]->getChildByName("mLabAddScore"))->setString(StringUtils::format("%lld", lTableScore));
		mLabAddScoreBG[wChairID]->setVisible(true);
	}
	else
	{
		//重置控件
	}
}
//庄家标志
void FrameLayer_zhajinhua::SetBankerUser(word wBankerUser)
{
	mImgBanker->setPosition(ccp(mPtBanker[wBankerUser].x, mPtBanker[wBankerUser].y));
	mImgBanker->setVisible(true);
	return;
}
//等待标识
void FrameLayer_zhajinhua::SetWaitUserChoice(word wChoice)
{
	m_wWaitUserChoice = wChoice;
}
//处理控制
void FrameLayer_zhajinhua::UpdateControl()
{
	//显示按钮
	SetBtnShow(ZJH_VALUE_RAISE | ZJH_VALUE_COMPARE | ZJH_VALUE_FLOD | ZJH_VALUE_SEE | ZJH_VALUE_FOLLOW);	
	mBtnFlod->setEnabled(true);
	mBtnFlod->setBright(true);

	IClientKernel * kernel = IClientKernel::get();
	word wMeChairID = kernel->GetMeChairID();

	ClientKernelSink_zhajinhua * kernelSink = ClientKernelSink_zhajinhua::getInstance();

	//设置按钮状态
	//放弃、看牌
	mBtnSee->setEnabled(kernelSink->m_bMingZhu[wMeChairID] ? false : true);
	mBtnSee->setBright(kernelSink->m_bMingZhu[wMeChairID] ? false : true);
	//判断开牌
	SCORE lTmep = (kernelSink->m_bMingZhu[wMeChairID]) ? 6 : 5;
	if ((kernelSink->m_lUserMaxScore - kernelSink->m_lTableScore[wMeChairID]) > (kernelSink->m_lMaxCellScore*lTmep))
	{
		//查找上家
		word i = wMeChairID-1;
		for (;; i--)
		{
			if (i == -1) i = GAME_PLAYER - 1;
			if (kernelSink->m_cbPlayStatus[i]) break;
		}

 		//跟注
 		mBtnFollow->setEnabled(kernelSink->m_lTableScore[i] == kernelSink->m_lCellScore ? false : true);
 		mBtnFollow->setBright(kernelSink->m_lTableScore[i] == kernelSink->m_lCellScore ? false : true);

		//加注按钮
		mBtnJetton_1->setEnabled(kernelSink->m_lCurrentTimes*kernelSink->m_lCellScore < kernelSink->m_lMaxCellScore ? true : false);
		mBtnJetton_2->setEnabled((kernelSink->m_lCurrentTimes + 2)*kernelSink->m_lCellScore < kernelSink->m_lMaxCellScore ? true : false);
		mBtnJetton_3->setEnabled((kernelSink->m_lCurrentTimes + 5)*kernelSink->m_lCellScore < kernelSink->m_lMaxCellScore ? true : false);
		mBtnJetton_1->setBright(kernelSink->m_lCurrentTimes*kernelSink->m_lCellScore < kernelSink->m_lMaxCellScore ? true : false);
		mBtnJetton_2->setBright((kernelSink->m_lCurrentTimes + 2)*kernelSink->m_lCellScore < kernelSink->m_lMaxCellScore ? true : false);
		mBtnJetton_3->setBright((kernelSink->m_lCurrentTimes + 5)*kernelSink->m_lCellScore < kernelSink->m_lMaxCellScore ? true : false);

		//比牌
		if (wMeChairID == kernelSink->m_wBankerUser || kernelSink->m_lTableScore[wMeChairID] >= 2 * kernelSink->m_lCellScore)
		{
			mBtnCompare->setEnabled(true);
			mBtnCompare->setBright(true);
		}
		else
		{
			mBtnCompare->setEnabled(false);
			mBtnCompare->setBright(false);
		}
	}
	else
	{
		mBtnJetton_1->setEnabled(false);
		mBtnJetton_2->setEnabled(false);
		mBtnJetton_3->setEnabled(false);
		mBtnFollow->setEnabled(false);

		mBtnJetton_1->setBright(false);
		mBtnJetton_2->setBright(false);
		mBtnJetton_3->setBright(false);
		mBtnFollow->setBright(false);

		mBtnCompare->setEnabled(false);
		mBtnCompare->setBright(false);
		mBtnCompare->setVisible(false);

// 		mBtnOpen->setEnabled(true);
// 		mBtnOpen->setBright(true);
// 		mBtnOpen->setVisible(true);
	}
}
// 用户状态
// void FrameLayer_zhajinhua::UpdateUserStats(word wChairID, byte cbUserStats)
// {
// 	IClientKernel * kernel = IClientKernel::get();
// 	PlayerNode_zhajinhua * node = nullptr;
// 	PlayerLayer_zhajinhua * mPlayer = nullptr;
// 	node = mPlayer->getPlayerNodeById(kernel->SwitchViewChairID(pIClientUserItem->GetChairID()));
// 
// 	if (cbUserStats == US_READY)
// 		//node->setIsPrepare(true);
// 		mUserReady[wChairID]->setVisible(true);
// 	else
// 		//node->setIsPrepare(false);
// 		mUserReady[wChairID]->setVisible(false);
// 
// }
//游戏中途进入，创建扑克
void FrameLayer_zhajinhua::CreateUserCard(word wChairID, byte cbCardData, word wCardID)
{
	CSpriteCard* cardBack = m_Card.getCardWithBack();
	if (cardBack)
	{
		cardBack->setMark(cbCardData);
		cardBack->setTag(wCardID + wChairID * MAX_COUNT);
		cardBack->setPosition(ccp(mPtCard[wChairID].x + wCardID * 33, mPtCard[wChairID].y));
		//cardBack->setPosition(mPtCard[wChairID]);
		this->addChild(cardBack, 200);
		//cardBack->setTag(wCardID + wChairID * MAX_COUNT);
		mCardList[wChairID][wCardID] = cardBack;
		m_Card.setCardWithData(cardBack);
	}
}
//发牌动画
void FrameLayer_zhajinhua::DispatchUserCard(word wChairID, byte cbCardData, word wCardID)
{
	CSpriteCard* cardBack = m_Card.getCardWithBack();
	if (cardBack)
	{
		cardBack->setMark(cbCardData);
		cardBack->setScale(0);
		cardBack->setPosition(mPtSendCard);
		this->addChild(cardBack, 200);
		cardBack->setTag(wCardID + wChairID * MAX_COUNT);
		cardBack->runAction(CCSequence::create(DelayTime::create(m_wActionCount * 0.03), ScaleTo::create(0.01, 1.4, 1.4),
			CCMoveTo::create(0.3f, ccp(mPtCard[wChairID].x + wCardID * 33, mPtCard[wChairID].y)), CCCallFuncN::create(this, callfuncN_selector(FrameLayer_zhajinhua::DispatchCardCall)), NULL));
		mCardList[wChairID][wCardID] = cardBack;
		m_Card.setCardWithData(cardBack);
		m_wActionCount++;
	}
}
//发牌动作回调
void FrameLayer_zhajinhua::DispatchCardCall(CCNode * node)
{
	m_wActionCount--;

	SoundManager::shared()->playSound("ZJH_SEND_CARD");
	
	//发牌动作完成
	if (m_wActionCount <= 0)
	{
		G_NOTIFY(ZJH_EVENT_DISPATCHFINISH);
	}
}
//比牌动画
void FrameLayer_zhajinhua::PerformCompareCard(word wCompareUser[2], word wLoserUser)
{
	img_compare->setVisible(false);
	m_wCompareUser[0] = wCompareUser[0];
	m_wCompareUser[1] = wCompareUser[1];
	m_wLostUser = wLoserUser;
	CSpriteCard* cardBack;
	for (word i = 0; i < MAX_COUNT; i++)
	{
		//左边
		//获取扑克
		cardBack = m_Card.getCardWithTag(i + m_wCompareUser[0] * MAX_COUNT);

		//设置扑克
		cardBack->setPosition(mCardList[m_wCompareUser[0]][i]->getPosition());
		this->removeChild(mCardList[m_wCompareUser[0]][i]);
		mCardList[m_wCompareUser[0]][i] = cardBack;
		this->addChild(cardBack,200);

		//设置动作
 		cardBack->runAction(CCSequence::create(DelayTime::create(0), ScaleTo::create(0.01, 1.4, 1.4),
 			CCMoveTo::create(1.0f, ccp(mPtUserCpmpare.x - 200 + i * 33, mPtUserCpmpare.y)), NULL));

 		//右边
 		//获取扑克
 		cardBack = m_Card.getCardWithTag(i + m_wCompareUser[1] * MAX_COUNT);
 
 		//设置扑克
 		cardBack->setPosition(mCardList[m_wCompareUser[1]][i]->getPosition());
 		this->removeChild(mCardList[m_wCompareUser[1]][i]);
 		mCardList[m_wCompareUser[1]][i] = cardBack;
 		this->addChild(cardBack,200);
 
 		//设置动作
 		if (i == MAX_COUNT - 1)
 		{
 			//最后一次，设置回调
 			cardBack->runAction(CCSequence::create(DelayTime::create(0), ScaleTo::create(0.01, 1.4, 1.4),
 				CCMoveTo::create(1.0f, ccp(mPtUserCpmpare.x + 150 + i * 33, mPtUserCpmpare.y)), CCCallFuncN::create(this, callfuncN_selector(FrameLayer_zhajinhua::MoveCardCall)), NULL));
 		}
 		else
 		{
 			cardBack->runAction(CCSequence::create(DelayTime::create(0), ScaleTo::create(0.01, 1.4, 1.4),
 				CCMoveTo::create(1.0f, ccp(mPtUserCpmpare.x + 150 + i * 33, mPtUserCpmpare.y)), NULL));
 		}
	}
}
//移动扑克完成
void FrameLayer_zhajinhua::MoveCardCall(CCNode * node)
{
	//闪电动画
	CCSprite* spt = CCSprite::create();
	CCAnimate * animate = SpriteHelper::createAnimateManager("vscard_", 0, 20, 0.1);

	spt->runAction(CCSequence::create(DelayTime::create(0), animate, CCCallFuncN::create(this, callfuncN_selector(FrameLayer_zhajinhua::FlashCardCall)), NULL));
	this->addChild(spt, 200);
	spt->setPosition(ccp(mPtUserCpmpare.x,mPtUserCpmpare.y + 10));

	//播放声音
	SoundManager::shared()->playSound("ZJH_COMPARE_CARD");
}
//闪电回调
void FrameLayer_zhajinhua::FlashCardCall(CCNode * node)
{
	CSpriteCard* card;
	for (word i = 0; i < MAX_COUNT; i++)
	{
		//左边
		//获取扑克
		if (m_wCompareUser[0] == m_wLostUser)
			card = m_Card.getCardCopyWithData(0x45);
		else
			card = m_Card.getCardWithTag(i + m_wCompareUser[0] * MAX_COUNT);

		//设置扑克
		card->setPosition(mCardList[m_wCompareUser[0]][i]->getPosition());
		this->removeChild(mCardList[m_wCompareUser[0]][i]);
		mCardList[m_wCompareUser[0]][i] = card;
		this->addChild(card, 200);

		//设置动作
		card->runAction(CCSequence::create(DelayTime::create(0), ScaleTo::create(0.01, 1.4, 1.4),
			CCMoveTo::create(1.0f, ccp(mPtCard[m_wCompareUser[0]].x + i * 33, mPtCard[m_wCompareUser[0]].y)), NULL));

		//右边
		//获取扑克
		if (m_wCompareUser[1] == m_wLostUser)
			card = m_Card.getCardCopyWithData(0x45);
		else
			card = m_Card.getCardWithTag(i + m_wCompareUser[1] * MAX_COUNT);

		//设置扑克
		card->setPosition(mCardList[m_wCompareUser[1]][i]->getPosition());
		this->removeChild(mCardList[m_wCompareUser[1]][i]);
		mCardList[m_wCompareUser[1]][i] = card;
		this->addChild(card, 200);

		//设置动作
		if (i == MAX_COUNT - 1)
		{
			//最后一次，设置回调
			card->runAction(CCSequence::create(DelayTime::create(0), ScaleTo::create(0.01, 1.4, 1.4),
				CCMoveTo::create(1.0f, ccp(mPtCard[m_wCompareUser[1]].x + i * 33, mPtCard[m_wCompareUser[1]].y)), CCCallFuncN::create(this, callfuncN_selector(FrameLayer_zhajinhua::CompareCardCall)), NULL));
		}
		else
		{
			card->runAction(CCSequence::create(DelayTime::create(0), ScaleTo::create(0.01, 1.4, 1.4),
				CCMoveTo::create(1.0f, ccp(mPtCard[m_wCompareUser[1]].x + i * 33, mPtCard[m_wCompareUser[1]].y)), NULL));
		}
	}

	//停止声音
	SoundManager::shared()->stopSound("ZJH_COMPARE_CARD");
}
//比牌完成回调
void FrameLayer_zhajinhua::CompareCardCall(CCNode * node)
{


// 	CSpriteCard* cardLost = nullptr;
// 	for (word i = 0; i < MAX_COUNT; i++)
// 	{
// 		for (word j = 0; j < 2; j++)
// 		{
// 			if (m_wCompareUser[j] == m_wLostUser)
// 			{
// 				cardLost = m_Card.getCardCopyWithData(0x45);
// 				cardLost->setPosition(mCardList[m_wLostUser][i]->getPosition());
// 				this->removeChild(mCardList[m_wLostUser][i]);
// 				mCardList[m_wLostUser][i] = cardLost;
// 				this->addChild(cardLost, 0);
// 			}
// 			else
// 			{
// 				mCardList[m_wCompareUser[j]][i]->setVisible(true);
// 			}
// 		}
// 	}

	G_NOTIFY(ZJH_EVENT_COMPAREFINISH);
}
//设置看牌状态
void FrameLayer_zhajinhua::SetLookCard(word wChairID, bool bLook)
{
	CSpriteCard* cardLook = nullptr;
	for (word i = 0; i < MAX_COUNT; i++)
	{
		if (bLook)
		{
			cardLook = m_Card.getCardCopyWithData(0x47);
			cardLook->setScale(1.4);
			cardLook->setPosition(mCardList[wChairID][i]->getPosition());
			this->removeChild(mCardList[wChairID][i]);
			mCardList[wChairID][i] = cardLook;
			this->addChild(cardLook,200);
		}
		else
			mCardList[wChairID][i]->setCardData(0x42);
	}
}
//设置扑克
void FrameLayer_zhajinhua::SetCardData(const byte cbCardData[], word wChairID, dword dwCardCount)
{
	CSpriteCard* card = nullptr;
	for (word i = 0; i < dwCardCount; i++)
	{
		if (cbCardData[i] > 0x43)
			card = m_Card.getCardCopyWithData(cbCardData[i]);
		else
			card = m_Card.getCardWithData(cbCardData[i]);
		if (card != nullptr && mCardList[wChairID][i] != nullptr)
		{
			card->setPosition(mCardList[wChairID][i]->getPosition());
			this->removeChild(mCardList[wChairID][i]);
			mCardList[wChairID][i] = card;
			this->addChild(card,200);
		}
	}
}
//筹码加注动画
void FrameLayer_zhajinhua::AddJettonMove(word wChairID, SCORE lScore)
{
	//记录所有下注
	m_lTableScoreAll += lScore;
	//FrameLayer_zhajinhua * mFrameScence = FrameLayer_zhajinhua::create(Image_bg);
	
	//添加筹码
	Sprite * Jetton = nullptr;
	int nIndex = MAX_JETTON_INDEX - 1;
	while (lScore > 0)
	{
		if (lScore >= g_lJetonIndex[nIndex])
		{
			lScore -= g_lJetonIndex[nIndex];
			Jetton = Sprite::create(g_PathJettonIndex[nIndex]);
			Jetton->setPosition(ccp(mPtUserJetton[wChairID].x, mPtUserJetton[wChairID].y));
			Vec2 new_pos = SimpleTools::getConfineToRandomVec2(mPtAllJetton, 100);
			Jetton->runAction(CCMoveTo::create(0.3, new_pos));
			Jetton->setScale(0.5);
			this->addChild(Jetton);
			mJettonVector.push_back(Jetton);
		}
		else
		{
			nIndex--;
			if (nIndex < 0)
				break;
		}
	}

	//压缩筹码
	this->scheduleOnce(SEL_SCHEDULE(&FrameLayer_zhajinhua::CompactJetIndexCell), 0.7);
}
//胜利玩家
void FrameLayer_zhajinhua::WinJettonMove(word wChairID)
{
	for (word i = 0; !mJettonVector.empty() && i < mJettonVector.size(); i++)
	{
		if (i < mJettonVector.size() - 1)
		{
			mJettonVector[i]->runAction(CCMoveTo::create(0.3, mPtUserJetton[wChairID]));
		}
		else
		{
			mJettonVector[i]->runAction(CCSequence::create(CCMoveTo::create(0.3, mPtUserJetton[wChairID]),
				CCCallFuncN::create(this, callfuncN_selector(FrameLayer_zhajinhua::ClearJettonCall)), NULL));
		}
	}
}
//清理筹码回调
void FrameLayer_zhajinhua::ClearJettonCall(CCNode * node)
{
	for (int i = 0; !mJettonVector.empty() && i < mJettonVector.size(); i++)
	{
		Sprite * now_sprite = mJettonVector[i];
		now_sprite->removeFromParent();
	}
	mJettonVector.clear();

	//清理加注分数
	for (dword i = 0; i < GAME_PLAYER; i++)
	{
		dynamic_cast<TextAtlas*>(mLabAddScoreBG[i]->getChildByName("mLabAddScore"))->setString("");
		mLabAddScoreBG[i]->setVisible(false);
	}
}
//界面结算
void FrameLayer_zhajinhua::SetGameEndScore(word wChairID, SCORE lScore)
{
	dynamic_cast<TextAtlas*>(mImgWinLostScore[wChairID]->getChildByName("lEndScore"))->setString(StringUtils::format("%lld", lScore));
	if (lScore > 0)
		mImgWinLostScore[wChairID]->loadTexture("zhajinhuaGameScene/zhajinhua/num_yellow_zheng.png");
	else
		mImgWinLostScore[wChairID]->loadTexture("zhajinhuaGameScene/zhajinhua/num_yellow_fu.png");

	mImgWinLostScore[wChairID]->setVisible(true);
}


//开始闪牌动画
void FrameLayer_zhajinhua::StartFalshCard(word wFalshUser[])
{
	for (int i = 0; i < GAME_PLAYER; i++)
	{
		if (wFalshUser[i] < GAME_PLAYER)m_wFlashUser[i] = wFalshUser[i];
	}
	SoundManager::shared()->playSound("ZJH_COMPARE_CARD",true);
	this->schedule(SEL_SCHEDULE(&FrameLayer_zhajinhua::FalshCard), 0.2);
}
//闪牌动画
void FrameLayer_zhajinhua::FalshCard(float dt)
{
	byte cbCardData[] = { 0x44, 0x45, 0x46 };
	word wIndex = rand() % 3;
	CSpriteCard * card = m_Card.getCardCopyWithData(cbCardData[wIndex]);
	for (word i = 0; i < GAME_PLAYER; i++)
	{
		if (m_wFlashUser[i] != INVALID_CHAIR)
		{
			byte cbCard[MAX_COUNT] = { cbCardData[wIndex], cbCardData[wIndex], cbCardData[wIndex] };
			SetCardData(cbCard, m_wFlashUser[i], MAX_COUNT);
		}
	}
}
//停止闪牌
void FrameLayer_zhajinhua::StopFalshCard()
{
	SoundManager::shared()->stopSound("ZJH_COMPARE_CARD");
	this->unschedule(schedule_selector(FrameLayer_zhajinhua::FalshCard));
}
//压缩筹码
void FrameLayer_zhajinhua::CompactJetIndexCell(float dt)
{
	//桌面筹码数量
	if (mJettonVector.size() < MAX_DRAW)
		return;

	//清理桌面筹码
	for (int i = 0; !mJettonVector.empty() && i < mJettonVector.size(); i++)
	{
		Sprite * now_sprite = mJettonVector[i];
		now_sprite->removeFromParent();
	}
	mJettonVector.clear();

	//压缩筹码
	SCORE lScore = m_lTableScoreAll;
	int nIndex = MAX_JETTON_INDEX - 1;

	Sprite * Jetton = nullptr;
	while (lScore > 0)
	{
		if (lScore >= g_lJetonIndex[nIndex])
		{
			//添加绘画
			lScore -= g_lJetonIndex[nIndex];
			Jetton = Sprite::create(g_PathJettonIndex[nIndex]);
			Jetton->setPosition(SimpleTools::getConfineToRandomVec2(mPtAllJetton, 100));
			Jetton->setScale(0.5);
			this->addChild(Jetton);
			mJettonVector.push_back(Jetton);
		}
		else
		{
			nIndex--;
			if (nIndex < 0)
				break;
		}
	}

	return;
}