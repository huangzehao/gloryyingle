#include "Puker.h"

bool Puker::init(int point)
{
	if (!Sprite::init())
	{
		return false;
	}

	///添加plist缓存_
	SpriteFrameCache::getInstance()->addSpriteFramesWithFile("doudizhuGameScene\GameMainScene\puker\CardLayer1.plist");

	///获取扑克背景_
	spt_bg = Sprite::createWithSpriteFrameName("new_card_bg.png");
	addChild(spt_bg);

	///创建扑克_
	int_puker_point = point;
	createPuker(point);

	///设置扑克弹起状态_
	isUp = false;

	return true;
}

///创建扑克_
void Puker::createPuker(int point)
{
	
	if (point < 13)///方片_
	{
		spt_lab = Sprite::createWithSpriteFrameName("new_card_type_s_3.png");
		spt_lab->setPosition(20, 110);
		spt_bg->addChild(spt_lab);
		shapeThePuker(point % 13, 0);
	}
	else if (point < 26)///梅花_
	{
		spt_lab = Sprite::createWithSpriteFrameName("new_card_type_s_2.png");
		spt_lab->setPosition(20, 110);
		spt_bg->addChild(spt_lab);
		shapeThePuker(point % 13, 1);
	}
	else if (point < 39)///红心_
	{
		spt_lab = Sprite::createWithSpriteFrameName("new_card_type_s_1.png");
		spt_lab->setPosition(20, 110);
		spt_bg->addChild(spt_lab);
		shapeThePuker(point % 13, 2);
	}
	else if (point < 52)///黑桃_
	{
		spt_lab = Sprite::createWithSpriteFrameName("new_card_type_s_0.png");
		spt_lab->setPosition(20, 110);
		spt_bg->addChild(spt_lab);
		shapeThePuker(point % 13, 3);
	}
	else
	{
		if (point == Ox4E)///小王_
		{
			spt_small = Sprite::createWithSpriteFrameName("card13.png");
			spt_small->setPosition(Vec2(146 / 2, 178 /2));
			spt_bg->addChild(spt_small);
		}
		else if(point == Ox4F)///大王_spt_back
		{
			spt_big = Sprite::createWithSpriteFrameName("card14.png");
			spt_big->setPosition(Vec2(146 / 2, 178 / 2));
			spt_bg->addChild(spt_big);
		}
		else if (point == Ox5G)
		{
			spt_back = Sprite::createWithSpriteFrameName("cardback.png");
			spt_bg->removeFromParentAndCleanup(true);
			addChild(spt_back);
		}
		else if (point == Ox5H)
		{
			spt_turn_1 = Sprite::createWithSpriteFrameName("fs_card_ani_0.png");
			spt_bg->removeFromParentAndCleanup(true);
			addChild(spt_turn_1);
		}
		else
		{
			spt_turn_2 = Sprite::createWithSpriteFrameName("fs_card_ani_1.png");
			spt_bg->removeFromParentAndCleanup(true);
			addChild(spt_turn_2);
		}
		return;
	}

}

///拼扑克_
void Puker::shapeThePuker(int point, int lab)
{

	String* str_holder = nullptr;
	///设置点数_
	if (lab % 2 == 0)
	{
		str_holder = String::createWithFormat("new_card_num_1_%d.png", point);
	}
	else
	{
		str_holder = String::createWithFormat("new_card_num_0_%d.png", point);
	}
	spt_point = Sprite::createWithSpriteFrameName(str_holder->getCString());
	spt_point->setPosition(20, 150);
	spt_bg->addChild(spt_point);

	///设置中央图案_
	if (point > 9)
	{
		str_holder = String::createWithFormat("new_card_type_%d.png", point);
		spt_mid = Sprite::createWithSpriteFrameName(str_holder->getCString());
		spt_mid->setPosition(70, 90);
	}
	else
	{
		lab = 3 - lab;
		str_holder = String::createWithFormat("new_card_type_%d.png", lab);
		spt_mid = Sprite::createWithSpriteFrameName(str_holder->getCString());
		spt_mid->setPosition(80, 60);
	}

	///添加到中央图案背景上_
	spt_bg->addChild(spt_mid);

}

///获取盒子范围_
Rect Puker::getMyBoundingBox()
{
	return Rect(this->getPositionX() - 146 / 2, this->getPositionY() - 178 / 2, 146, 178);
}