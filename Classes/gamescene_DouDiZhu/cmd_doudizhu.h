#ifndef CMD_DOUDIZHU_HEAD_FILE
#define CMD_DOUDIZHU_HEAD_FILE
#include "Platform/PFDefine/df/types.h"
//////////////////////////////////////////////////////////////////////////
//公共宏定义
#pragma pack(1)

namespace doudizhu
{
	//游戏属性
#define KIND_ID						200									//游戏 I D
#define GAME_NAME					TEXT("斗地主")						//游戏名字

	//组件属性
#define GAME_PLAYER					3									//游戏人数
#define VERSION_SERVER				PROCESS_VERSION(6,0,3)				//程序版本
#define VERSION_CLIENT				PROCESS_VERSION(6,0,3)				//程序版本

//////////////////////////////////////////////////////////////////////////////////

	//数目定义
#define MAX_COUNT					20									//最大数目
#define FULL_COUNT					54									//全牌数目

	//逻辑数目
#define NORMAL_COUNT				17									//常规数目
#define DISPATCH_COUNT				51									//派发数目
#define GOOD_CARD_COUTN				38									//好牌数目

	//数值掩码
#define	MASK_COLOR					0xF0								//花色掩码
#define	MASK_VALUE					0x0F								//数值掩码

	//逻辑类型
#define CT_ERROR					0									//错误类型_
#define CT_SINGLE					1									//单牌类型_
#define CT_DOUBLE					2									//对牌类型_
#define CT_THREE					3									//三条类型_
#define CT_SINGLE_LINE				4									//单连类型_
#define CT_DOUBLE_LINE				5									//对连类型_
#define CT_THREE_LINE				6									//三连类型_
#define CT_THREE_TAKE_ONE			7									//三带一单_
#define CT_THREE_TAKE_TWO			8									//三带一对_
#define CT_FOUR_TAKE_ONE			9									//四带两单_
#define CT_FOUR_TAKE_TWO			10									//四带两对_
#define CT_SINGLE_FLY               11                                  //飞机带单_
#define CT_DOUBLE_FLY               12									//飞机带对_
#define CT_ANY_IS_OK                13                                  //任何类型_
#define CT_BOMB_CARD				14									//炸弹类型_
#define CT_MISSILE_CARD				15									//火箭类型_


//////////////////////////////////////////////////////////////////////////////////
//状态定义_

#define GAME_SCENE_FREE				GAME_STATUS_FREE					//等待开始
#define GAME_SCENE_CALL				GAME_STATUS_PLAY					//叫分状态
#define GAME_SCENE_PLAY				GAME_STATUS_PLAY+1					//游戏进行

	//空闲状态_
	struct CMD_S_StatusFree
	{
		//游戏属性_
		int64							lCellint64;							//基础积分
		int64							lRestrictint64;				    	//每局封顶

		//时间信息_
		byte							cbTimeOutCard;						//出牌时间
		byte							cbTimeCallint64;					//叫分时间
		byte							cbTimeStartGame;					//开始时间
		byte							cbTimeHeadOutCard;					//首出时间

		//历史积分_
		int64							lTurnint64[GAME_PLAYER];			//积分信息
		int64							lCollectint64[GAME_PLAYER];			//积分信息

#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID || CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
		//房间信息_
		tchar							szGameRoomName[32 * 2];
#elif (CC_TARGET_PLATFORM == CC_PLATFORM_WIN32)
		tchar							szGameRoomName[32];					//房间名称
#endif
	};

	//叫分状态_
	struct CMD_S_StatusCall
	{
		//时间信息
		byte							cbTimeOutCard;						//出牌时间
		byte							cbTimeCallint64;					//叫分时间
		byte							cbTimeStartGame;					//开始时间
		byte							cbTimeHeadOutCard;					//首出时间

		//游戏信息
		int64							lCellint64;							//单元积分
		int64							lRestrictint64;						//每局封顶
		word							wCurrentUser;						//当前玩家
		byte							cbBankerint64;						//庄家叫分
		byte							cbint64Info[GAME_PLAYER];			//叫分信息
		byte							cbHandCardData[NORMAL_COUNT];		//手上扑克

		//历史积分
		int64							lTurnint64[GAME_PLAYER];			//积分信息
		int64							lCollectint64[GAME_PLAYER];			//积分信息

	};

	//游戏状态_
	struct CMD_S_StatusPlay
	{
		//时间信息_
		byte							cbTimeOutCard;						//出牌时间
		byte							cbTimeCallint64;					//叫分时间
		byte							cbTimeStartGame;					//开始时间
		byte							cbTimeHeadOutCard;					//首出时间

		//游戏变量_
		int64							lCellint64;							//单元积分
		int64							lRestrictint64;						//每局封顶
		byte							cbBombCount;						//炸弹次数
		word							wBankerUser;						//庄家用户
		word							wCurrentUser;						//当前玩家
		byte							cbBankerint64;						//庄家叫分

		//出牌信息_
		word							wTurnWiner;							//胜利玩家
		byte							cbTurnCardCount;					//出牌数目
		byte							cbTurnCardData[MAX_COUNT];			//出牌数据

		//扑克信息_
		byte							cbBankerCard[3];					//游戏底牌
		byte							cbHandCardData[MAX_COUNT];			//手上扑克
		byte							cbHandCardCount[GAME_PLAYER];		//扑克数目
		// 	byte							cbSurplusCardData[FULL_COUNT];		//剩余扑克
		// 	byte							cbSurplusCardCount;					//剩余扑克

		//历史积分_
		int64							lTurnint64[GAME_PLAYER];			//积分信息
		int64							lCollectint64[GAME_PLAYER];			//积分信息

#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID || CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
		//房间信息_
		tchar							szGameRoomName[32 * 2];
#elif (CC_TARGET_PLATFORM == CC_PLATFORM_WIN32)
		tchar							szGameRoomName[32];					//房间名称
#endif


	};

	//////////////////////////////////////////////////////////////////////////////////
	//命令定义_

#define SUB_S_GAME_START			100									//游戏开始
#define SUB_S_CALL_int64			101									//用户叫分
#define SUB_S_BANKER_INFO			102									//庄家信息
#define SUB_S_OUT_CARD				103									//用户出牌
#define SUB_S_PASS_CARD				104									//用户放弃
#define SUB_S_GAME_CONCLUDE			105									//游戏结束
#define SUB_S_CHEAT_CARD			107									//作弊扑克

	//发送扑克_
	struct CMD_S_GameStart
	{
		word							wStartUser;							//开始玩家
		word				 			wCurrentUser;						//当前玩家
		byte							cbCardData[NORMAL_COUNT];			//扑克列表
		byte							cbValidCardData;					//明牌扑克
		byte							cbValidCardIndex;					//明牌位置

	};

	//机器人扑克_
	struct CMD_S_AndroidCard
	{
		byte							cbHandCard[GAME_PLAYER][NORMAL_COUNT];//手上扑克
		word							wCurrentUser;						  //当前玩家
		byte							cbBankerCard[3];				      //庄家扑克
	};

	//作弊扑克_
	struct CMD_S_CheatCard
	{
		word							wCardUser[GAME_PLAYER];					//作弊玩家_
		byte							cbUserCount;							//作弊数量_
		byte							cbCardData[GAME_PLAYER][MAX_COUNT];		//扑克列表_
		byte							cbCardCount[GAME_PLAYER];				//扑克数量_

	};

	//用户叫分_
	struct CMD_S_Callint64
	{
		word				 			wCurrentUser;						//当前玩家_
		word							wCallint64User;						//叫分玩家_
		byte							cbCurrentint64;						//当前叫分_
		byte							cbUserCallint64;					//上次叫分_
	};

	//庄家信息_
	struct CMD_S_BankerInfo
	{
		word				 			wBankerUser;						//庄家玩家_
		word				 			wCurrentUser;						//当前玩家_
		byte							cbBankerint64;						//庄家叫分_
		byte							cbBankerCard[3];					//庄家扑克_
	};

	//用户出牌_
	struct CMD_S_OutCard
	{
		byte							cbCardCount;						//出牌数目
		word				 			wCurrentUser;						//当前玩家
		word							wOutCardUser;						//出牌玩家
		byte							cbCardData[MAX_COUNT];				//扑克列表
	};

	//放弃出牌_
	struct CMD_S_PassCard
	{
		byte							cbTurnOver;							//一轮结束
		word				 			wCurrentUser;						//当前玩家
		word				 			wPassCardUser;						//放弃玩家
	};

	//游戏结束_
	struct CMD_S_GameConclude
	{
		//积分变量_
		int64							lCellint64;							//单元积分
		int64							lRestrictint64;						//每局封顶
		int64							lGameint64[GAME_PLAYER];			//游戏积分

		//春天标志_
		byte							bChunTian;							//春天标志
		byte							bFanChunTian;						//春天标志

		//炸弹信息_
		byte							cbBombCount;						//炸弹个数
		byte							cbEachBombCount[GAME_PLAYER];		//炸弹个数

		//游戏信息_
		byte							cbBankerint64;						//叫分数目
		byte							cbCardCount[GAME_PLAYER];			//扑克数目
		byte							cbHandCardData[FULL_COUNT];			//扑克列表
		bool							bFlee;								//结束逃跑
	};

	//////////////////////////////////////////////////////////////////////////////////
	//命令定义_

#define SUB_C_CALL_int64			1									//用户叫分
#define SUB_C_OUT_CARD				2									//用户出牌
#define SUB_C_PASS_CARD				3									//用户放弃

	//用户叫分_
	struct CMD_C_Callint64
	{
		byte							cbCallint64;						//叫分数目
	};

	//用户出牌_
	struct CMD_C_OutCard
	{
		byte							cbCardCount;						//出牌数目
		byte							cbCardData[MAX_COUNT];				//扑克数据
	};

}

//////////////////////////////////////////////////////////////////////////
#pragma pack()

#endif
