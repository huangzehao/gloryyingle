#ifndef FrameLayer_DOUDIZHU_H
#define FrameLayer_DOUDIZHU_H

#include "cocos2d.h"
#include "cocos-ext.h"
#include "ui/CocosGUI.h"
#include "Platform/PFDefine/df/types.h"
USING_NS_CC;

class Puker;
class GameFightingLayer;
class IClientUserItem;

class FrameLayer_doudizhu : public cocos2d::Layer
{
public:
	FrameLayer_doudizhu();
	~FrameLayer_doudizhu();

	CREATE_FUNC(FrameLayer_doudizhu);
	virtual bool init();

	///框架函数_
	void setCloseDialogInfo(cocos2d::Ref* target, cocos2d::SEL_CallFuncN callfun, const std::string& title, const std::string& content);
	///创建游戏主框架_
	void createMainPanel();
	///设置玩家准备状态_
	void setOtherUserPrepare(IClientUserItem *pIClientUserItem, byte user_state);
	///清理其他玩家离开痕迹_
	void cleanOtherUserPrepare(int chair_id);
	///设置游戏空闲状态信息显示_
	void setGameSceneFree(void * data, int dataSize);
	///设置游戏进行状态_
	void setGameScenePlay(void * data, int dataSize);
	///设置游戏开始_
	void setGameSceneStart(void * data, int dataSize);
	///设置游戏叫分_
	void setGameSceneCall(void * data, int dataSize);
	///设置游戏庄家信息_
	void setGameSceneBanker(void * data, int dataSize);
	///设置用户出牌信息_
	void setGameSceneOutCards(void* data, int dataSize);
	///设置放弃出牌信息_
	void setGameScenePassCards(void* data, int dataSize);
	///设置游戏结束_
	void setGameSceneOver(void* data, int dataSize);
	///设置左边的报警器
	void LightingLeft(float  dt);
	///设置右边的报警器
	void LightingRight(float  dt);
public:
	cocos2d::Ref*				mTarget;
	cocos2d::SEL_CallFuncN		mCallback;
	std::string					mTitle;
	std::string					mContent;
	ui::TextAtlas* alt_beishu_num;///显示倍数_
	ui::TextAtlas* alt_mygold;///玩家本人金币数量label_
public:

	///从游戏主场景返回游戏大厅通道_
	void func_affirm_exit_call_back(cocos2d::Ref *pSender);
	///游戏关闭按钮事件接收_
	void btnCloseGameCallBack(cocos2d::Ref * ref, ui::TouchEventType eType);
	///游戏设置按钮事件接收_
	void btnSetGameCallBack(cocos2d::Ref * ref, ui::TouchEventType eType);
	///游戏开始按钮事件返回_
	void btnStartGameCallBack(cocos2d::Ref * ref, ui::TouchEventType eType);
	///叫分_不叫按钮事件返回_
	void btnCallNoneCallBack(cocos2d::Ref * ref, ui::TouchEventType eType);
	///叫分_1分按钮事件返回_
	void btnCallOneCallBack(cocos2d::Ref * ref, ui::TouchEventType eType);
	///叫分_2分按钮事件返回_
	void btnCallTwoCallBack(cocos2d::Ref * ref, ui::TouchEventType eType);
	///叫分_3分按钮事件返回_
	void btnCallThreeCallBack(cocos2d::Ref * ref, ui::TouchEventType eType);

	///设置游戏主界面布局_
	void placeTheGameSceneLayout();
	///空闲状态倒计时_
	void updateFreeTimeBack(float dt);
	///游戏开始剩余扑克计数器累加_
	void updateStartCardsAdd(float dt);
	///叫分倒计时_
	void updateCallTimeBack(float dt);
	///倒计时_
	void updateTimeBack(float dt);


	///设置游戏叫分_子步骤_
	void setGameSceneCall();
	///设置成我的本地位置_
	void setMyChairPosition();
	///设置所有玩家性别_
	void setAllPlayerSex();

	Sprite* spt_me;///玩家自身帧动画_
// 	Sprite* spt_left;///左边玩家帧动画_
// 	Sprite* spt_right;///右边玩家帧动画_
	int int_faceId_me;
	int int_faceId_left;
	int int_faceId_right;
	int beishu;//游戏的倍数
	Animation* animation;

	ui::Widget *img_top_score;///主界面顶部的分数


	static cocos2d::CCAnimate* createAnimateManager(const char *frameName, int beginNum, int endNum, float delay, bool adverse = false, int count = 1);

private:
	ui::Widget* Panel_Main;///josn_文件子根节点
	ui::Widget* img_mydata; ///游戏场景底部玩家信息_
	ui::Text* txt_mynick;///玩家本人昵称label_
	
	ui::TextAtlas* txt_time_back;///空闲状态时间倒计时_
	ui::TextAtlas* txt_nums_player1;///玩家1剩余扑克数量_
	ui::TextAtlas* txt_nums_player2;///玩家2剩余扑克数量_

	
	
	ui::ImageView* img_cards_count_box1;///玩家1剩余扑克数量盒子_
	ui::ImageView* img_cards_count_box2;///玩家2剩余扑克数量盒子_

	ui::ImageView* img_game_table;///游戏桌子
	ui::ImageView* img_clock;///闹钟_
	ui::Button* btn_start;///开始按钮_
	ui::Widget* img_head_bg_me;///本人头像背景框_
	ui::ImageView* img_head_bg_player1;///玩家1头像背景框_
	ui::ImageView* img_head_bg_player2;///玩家1头像背景框_
	ui::ImageView* img_head_me;///本人头像
	ui::ImageView* img_head_player1;///玩家1头像
	ui::ImageView* img_head_player2;///玩家1头像

	ui::Widget* Panel_call;///叫分按钮盒子_
	ui::Button* btn_call_none;///不叫_
	ui::Button* btn_call_one;///1分_
	ui::Button* btn_call_two;///2 分_
	ui::Button* btn_call_three;///3 分_
	ui::ImageView* img_sc_one;
	ui::ImageView* img_sc_two;
	ui::ImageView* img_sc_three;
	ui::ImageView* img_sc_none;

	ui::ImageView* img_prepare_me;///准备图标_ me
	ui::ImageView* img_prepare_player1;///准备图标_ player1
	ui::ImageView* img_prepare_player2;///准备图标_ player2

	ui::Text* txt_name_left;///左边玩家姓名_
	ui::Text* txt_name_right;///右边玩家姓名_
	

	bool the_bar_poped;///判断个人信息条是否为已弹出状态_
	int the_sec_start;///游戏开始时间_
	int the_sec_call;///叫分时间_
	int the_sec_headOut;///首出时间_
	int the_sec_outCard;///出牌时间_

	int the_nums;///表情的种类数_序号从0开始
	int the_smile[20];///每个表情的帧总数_

	GameFightingLayer* fighting_layer;///战斗层_
	int time_cnts;///计时器专用计数_
	int int_start_user;///当前开始玩家_

	int chair_me;///我的座位_
	int chair_left;///左边_
	int chair_right;///右边_

	///玩家性别_
	bool sex_is_man_me;
	bool sex_is_man_left;
	bool sex_is_man_right;

	int int_pukers_left;///左边玩家剩余扑克数_
	int int_pukers_right;///右边玩家剩余扑克数_

	///退出是否需要提示_
	bool exit_is_tips;

	//左边桌子的报警器
	Sprite * img_light_left;
	Sprite * left_liang;
	bool leftIsHave;
	//右边桌子的报警器
	Sprite * img_light_right;
	Sprite * right_liang;
	bool rightIsHave;
};

#endif

