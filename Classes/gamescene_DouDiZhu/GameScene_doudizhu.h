#ifndef _GameScene_DOUDIZHU_H_
#define _GameScene_DOUDIZHU_H_

#include "cocos2d.h"
#include "cocos-ext.h"
#include "Kernel/kernel/user/IClientUserItem.h"
#include "common/IKeybackListener.h"
#include "common/TouchLayer.h"
#include "common/SpriteCard.h"
#include "Tools/tools/MessageLayer.h"

#define  P_WIDTH      1420
#define  P_WIDTH_2    P_WIDTH/2

#define  P_HEIGHT     800
#define  P_HEIGHT_2   P_HEIGHT/2

USING_NS_CC;
USING_NS_CC_EXT;

using namespace Message;

class FrameLayer_doudizhu;

class GameScene_doudizhu
	: public Scene
	, public ITouchSink
	, public IKeybackListener
{
public:
	//创建方法_
	CREATE_FUNC(GameScene_doudizhu);

	GameScene_doudizhu();
	~GameScene_doudizhu();

	///初始化方法_
	virtual bool init();

	static GameScene_doudizhu* shared();

	/// 初始化游戏基础数据_
	static void initGameBaseData();

public:
	//场景动画完成_
	virtual void onEnterTransitionDidFinish();

	//场景动画开始_
	virtual void onExitTransitionDidStart();

public:
	virtual void onKeybackClicked();
	void onBackToRoom(Node* node);
	///是断线重连_
	static bool is_Reconnect_on_loss();
	void func_Reconnect_on_loss(cocos2d::Ref * obj);
	void reconnect_on_loss(float dt);
public:
	virtual bool onTouchBegan(Touch *pTouch, Event *pEvent);
	virtual void onTouchMoved(Touch *pTouch, Event *pEvent);
	virtual void onTouchEnded(Touch *pTouch, Event *pEvent);
	void net_user_state_update(IClientUserItem *pIClientUserItem, byte user_state);
	void showUserInfo(IClientUserItem *pIClientUserItem, byte cbUserStatus);
	void cleanUserInfo(IClientUserItem *pIClientUserItem);
	///< 倒计时退出游戏
	void  ExitGameTiming();
	///< 注销内核退出游戏
	void  CloseKernelExitGame(float dt);

///各类返回方法_
public:
	void closeCallback(cocos2d::Node* obj);
	///< 退出游戏_
	void closeGameNotify(cocos2d::Ref* ref);
	///传递接收到的服务端信息到游戏场景_ free
	void transmitSetGameSceneFree(void* data, int dataSize);
	///传递接收到的服务端信息到游戏场景_ play
	void transmitSetGameScenePlay(void* data, int dataSize);
	///传递接收到的服务端信息到游戏场景_ start
	void transmitSetGameSceneStart(void* data, int dataSize);
	///传递接收到的服务端信息到游戏场景_ call
	void transmitSetGameSceneCall(void* data, int dataSize);
	///传递接收到的服务端信息到游戏场景_ banker
	void transmitSetGameSceneBanker(void* data, int dataSize);
	///传递接收到的服务端信息到游戏场景_ outCards
	void transmitSetGameSceneOutCards(void* data, int dataSize);
	///传递接收到的服务端信息到游戏场景_ PassCards
	void transmitSetGameScenePassCards(void* data, int dataSize);
	///传递接收到的服务端信息到游戏场景_ gameOver
	void transmitSetGameSceneOver(void* data, int dataSize);

//私有变量_
private:
	FrameLayer_doudizhu* frame_layer;
	MessageLayer*		mMessageLayer;	///< 消息层
};

#endif
