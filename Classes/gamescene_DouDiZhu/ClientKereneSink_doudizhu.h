#ifndef _ClientKernelSink_DOUDIZHU_H_
#define _ClientKernelSink_DOUDIZHU_H_

#include "cocos2d.h"
#include "Kernel/kernel/game/IClientKernelSink.h"
#include "Kernel/kernel/game/IClientKernel.h"

USING_NS_CC;

class CSoundSetting;
class GameScene_doudizhu;

class ClientKereneSink_doudizhu
	: public Ref
	, public IClientKernelSink
{
public:
	ClientKereneSink_doudizhu();
	~ClientKereneSink_doudizhu();

	static ClientKereneSink_doudizhu & getInstance()
	{
		static ClientKereneSink_doudizhu gClientKernelSink;
		return gClientKernelSink;
	}

 	//控制接口_
 public:
 	//启动游戏_
 	virtual bool SetupGameClient();
 	//重置游戏_
 	virtual void ResetGameClient();
 	//关闭游戏_
 	virtual void CloseGameClient(int exit_tag = 0);
 
 	//框架事件_
 public:
 	//系统滚动消息_
 	virtual bool OnGFTableMessage(const char* szMessage);
 	//全局消息_
 	virtual bool OnGFGlobalMessage(const char* szMessage);
 	//等待提示_
 	virtual bool OnGFWaitTips(bool bWait);
 	//比赛信息_
 	virtual bool OnGFMatchInfo(tagMatchInfo* pMatchInfo);
 	//比赛等待提示_
 	virtual bool OnGFMatchWaitTips(tagMatchWaitTip* pMatchWaitTip);
 	//比赛结果_
 	virtual bool OnGFMatchResult(tagMatchResult* pMatchResult);
 
 	//游戏事件_
 public:
 	//旁观消息_
 	virtual bool OnEventLookonMode(void* data, int dataSize);
 	//场景消息_
 	virtual bool OnEventSceneMessage(byte cbGameStatus, bool bLookonUser, void* data, int dataSize);
 	//游戏消息_
 	virtual bool OnEventGameMessage(int sub, void* data, int dataSize);
 
 	//时钟事件_
 public:
 	//用户时钟_
 	virtual void OnEventUserClock(word wChairID, word wUserClock);
 	//时钟删除_
 	virtual bool OnEventGameClockKill(word wChairID);
 	//时钟信息_
 	virtual bool OnEventGameClockInfo(word wChairID, uint nElapse, word wClockID);
 
 	//用户事件_
 public:
 	//用户进入_
 	virtual void OnEventUserEnter(IClientUserItem * pIClientUserItem, bool bLookonUser);
 	//用户离开_
 	virtual void OnEventUserLeave(IClientUserItem * pIClientUserItem, bool bLookonUser);
 	//用户积分_
 	virtual void OnEventUserScore(IClientUserItem * pIClientUserItem, bool bLookonUser);
 	//用户状态_
 	virtual void OnEventUserStatus(IClientUserItem * pIClientUserItem, bool bLookonUser);
 	//用户属性_
 	virtual void OnEventUserAttrib(IClientUserItem * pIClientUserItem, bool bLookonUser);
 	//用户头像_
 	virtual void OnEventCustomFace(IClientUserItem * pIClientUserItem, bool bLookonUser);

 	//消息处理_
public:
	///空闲状态消息处理_
	bool onEventSceneFree(void* data, int dataSize);
	///游戏进行状态_
	bool onEventSceneGameplaying(void* data, int dataSize);
	///游戏开始消息处理_
	bool onEventGameStart(void* data, int dataSize);
	///用户叫分消息处理_
	bool onEventGameCall(void* data, int dataSize);
	///庄家信息消息处理_
	bool onEventGameBanker(void* data, int dataSize);
	///用户出牌消息处理_
	bool OnEventGameOutCards(void* data, int dataSize);
	///用户放弃消息处理_
	bool OnEventGamePassCards(void* data, int dataSize);
	///游戏结束消息处理_
	bool OnEventGameOver(void* data, int dataSize);

	virtual void CloseGameDelayClient();

private:
	GameScene_doudizhu* m_pGameScene;
};

#endif

