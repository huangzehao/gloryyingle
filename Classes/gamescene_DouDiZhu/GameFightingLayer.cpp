#include "FrameLayer_doudizhu.h"
#include "Tools/tools/StringData.h"
#include "ClientKereneSink_doudizhu.h"
#include "Kernel/kernel/game/IClientKernel.h"
#include "Tools/Manager/SpriteHelper.h"
#include "GameScene_doudizhu.h"
#include "Tools/Manager/SoundManager.h"
#include "common/GameSetLayer.h"
#include "Tools/tools/SimpleTools.h"
#include "Tools/Dialog/NewDialog.h"
#include "Tools/tools/Convert.h"
#include "Tools/ViewHeader.h"
#include "ui/UIWidget.h"
#include "stdlib.h"
#include "GameLogic_Land.h"
#include "cmd_doudizhu.h"
#include "GameFightingLayer.h"
#include "Puker.h"


using namespace doudizhu;

USING_NS_CC_EXT;
using namespace ui;

#define Position_left Vec2(400, 600)
#define Position_right Vec2(1030, 600)
#define Position_me Vec2(710, 430)

#define the_puke_des 60
#define EndLayerZ_order 10000
#define AutoLayerZ_order 9999
#define ActionZ_order 9998

GameFightingLayer::GameFightingLayer()
{
	puke_move0 = nullptr;
	puke_move1 = nullptr;
	puke_move2 = nullptr;
}

GameFightingLayer::~GameFightingLayer()
{

}

bool GameFightingLayer::init(int headCard, int outCard)
{
	if (!Layer::init())
	{
		return false;
	}

	node_fighting = Node::create();
	addChild(node_fighting, ActionZ_order);
	ac_time = 2;


	///出牌时间和首出时间_
	the_sec_headOut = headCard;
	the_sec_outCard = outCard;
	b_turnOver = true;
	b_kingsD = false;
	b_twoKings = false;
	b_flee = false;
	out_enable = false;
	is_fighting_fierce = false;
	m_isMoved = false;

	///初始化扑克列表_
	logic_land = LogicLand::getInstance();
	int_cur_logic = CT_ANY_IS_OK;
	int_card_cnts = 0;
	my_puke_pointX = 230;
	int_curPuker_cnts = 0;
	tip_landPoint = 0;
	tip_cnts = 0;
	int_autos = 0;
	int_pre_logicPoint = 0;
	int_pukers_left = 17;
	int_pukers_right = 17;
	int_hui = 0;
	AutoPukers = nullptr;
	for (int i = 0; i < 20; i++)
	{
		my_puke[i] = nullptr;
		puke[i] = -1;
		//my_puke_up[i] = false;
		my_puke_touchMoved[i] = false;
		spt_hui[i] = nullptr;
		move_puke[i] = -1;
		out_point[i] = -1;
		cur_outPuke[i] = -1;
		out_puke_me[i] = nullptr;
		out_puke_left[i] = nullptr;
		out_puke_right[i] = nullptr;
	}

	this->setSwallowsTouches(false);
	createFightingPanel();

	///设置本地位置_
	setMyChairPosition();

	
	return true;
}

///创建战斗界面_
void GameFightingLayer::createFightingPanel()
{
	Widget* root = cocostudio::GUIReader::getInstance()->widgetFromJsonFile("doudizhuGameScene/GameFightingLayer.json");
	addChild(root);

	Panel_Fighting = dynamic_cast<Widget*>(root->getChildByName("Panel_Fighting"));
	
	
	///托管按钮_
	btn_autoPuker = dynamic_cast<Button*>(Panel_Fighting->getChildByName("btn_autoPuker"));
	btn_autoPuker->addTouchEventListener(this, SEL_TouchEvent(&GameFightingLayer::btnAutoPukerCallBack));

	///隐藏托管按钮_
	btn_autoPuker->setVisible(false);

	///左边谈话框_
	img_talk_left = dynamic_cast<ImageView*>(Panel_Fighting->getChildByName("img_talk_left"));
	img_talk_left->setVisible(false);
	///右边谈话框_
	img_talk_right = dynamic_cast<ImageView*>(Panel_Fighting->getChildByName("img_talk_right"));
	img_talk_right->setVisible(false);
	///本人谈话框_
	img_talk_me = dynamic_cast<ImageView*>(Panel_Fighting->getChildByName("img_talk_me"));
	img_talk_me->setVisible(false);

// 	///不叫_
// 	img_score_none = dynamic_cast<ImageView*>(Panel_Fighting->getChildByName("img_score_none"));
// 	///一分_
// 	img_score_one = dynamic_cast<ImageView*>(Panel_Fighting->getChildByName("img_score_one"));
// 	///二分_
// 	img_score_two = dynamic_cast<ImageView*>(Panel_Fighting->getChildByName("img_score_two"));
// 	///三分_
// 	img_score_three = dynamic_cast<ImageView*>(Panel_Fighting->getChildByName("img_score_three"));
// 	///不出_
// 	img_pass = dynamic_cast<ImageView*>(Panel_Fighting->getChildByName("img_pass"));

	///闹钟_
	ImageView * img_clock1 = dynamic_cast<ImageView*>(Panel_Fighting->getChildByName("img_clock"));
	img_clock = img_clock1->clone();
	this->addChild(img_clock, 20000);
	img_clock1->removeAllChildrenWithCleanup(true);
	
	///倒计时_
	txt_time_back = dynamic_cast<TextAtlas*>(img_clock->getChildByName("txt_time_back"));


	///出牌按钮盒子_
	Panel_out_cards = dynamic_cast<Widget*>(root->getChildByName("Panel_Fighting")->getChildByName("Panel_out_cards"));
	Panel_out_cards->setVisible(false);
	Panel_out_cards->setScale(1.3);
	Panel_out_cards->setPosition(Vec2(-200, -50));
	///不出_
	btn_pass = dynamic_cast<Button*>(root->getChildByName("Panel_Fighting")->getChildByName("Panel_out_cards")->getChildByName("btn_pass"));
	btn_pass->addTouchEventListener(this, SEL_TouchEvent(&GameFightingLayer::btnPassCallBack));
	img_pass = dynamic_cast<ImageView*>(btn_pass->getChildByName("img_pass"));
	///提示_
	btn_tip_cards = dynamic_cast<Button*>(root->getChildByName("Panel_Fighting")->getChildByName("Panel_out_cards")->getChildByName("btn_tip_cards"));
	btn_tip_cards->addTouchEventListener(this, SEL_TouchEvent(&GameFightingLayer::btnTipCardsCallBack));
	img_tip = dynamic_cast<ImageView*>(btn_tip_cards->getChildByName("img_tip"));
	///出牌_
	btn_out_cards = dynamic_cast<Button*>(root->getChildByName("Panel_Fighting")->getChildByName("Panel_out_cards")->getChildByName("btn_out_cards"));
	btn_out_cards->addTouchEventListener(this, SEL_TouchEvent(&GameFightingLayer::btnOutCardsCallBack));
	img_out = dynamic_cast<ImageView*>(btn_out_cards->getChildByName("img_out"));

	///游戏结束层_
	Widget* over = cocostudio::GUIReader::getInstance()->widgetFromJsonFile("doudizhuGameScene/GameOverLayer.json");
	addChild(over, EndLayerZ_order);

	Panel_end = dynamic_cast<Widget*>(over->getChildByName("Panel_end"));
	Panel_end->setVisible(false);
	///继续下一局按钮_
	btn_continue = dynamic_cast<Button*>(Panel_end->getChildByName("btn_continue"));
	btn_continue->addTouchEventListener(this, SEL_TouchEvent(&GameFightingLayer::btnContinueCallBack));

	///退出游戏按钮_
	btn_exit = dynamic_cast<Button*>(Panel_end->getChildByName("btn_exit"));
	btn_exit->addTouchEventListener(this, SEL_TouchEvent(&GameFightingLayer::btnExitCallBack));

	///炸弹个数_
	txt_num_bomb = dynamic_cast<Text*>(Panel_end->getChildByName("img_bomb_spring")->getChildByName("txt_num_bomb"));
	///春天_
	txt_num_spring = dynamic_cast<Text*>(Panel_end->getChildByName("img_bomb_spring")->getChildByName("txt_num_spring"));
	///地主胜利_
	img_win_land = dynamic_cast<ImageView*>(Panel_end->getChildByName("img_win_land"));
	///农民胜利_
	img_win_farmer = dynamic_cast<ImageView*>(Panel_end->getChildByName("img_win_farmer"));
	///地主失败_
	img_fail_land = dynamic_cast<ImageView*>(Panel_end->getChildByName("img_fail_land"));
	///农民失败_
	img_fail_farmer = dynamic_cast<ImageView*>(Panel_end->getChildByName("img_fail_farmer"));
	///失败分数字体_
	aTxt_fail = dynamic_cast<TextAtlas*>(Panel_end->getChildByName("aTxt_fail"));
	///胜利分数字体_
	aTxt_win = dynamic_cast<TextAtlas*>(Panel_end->getChildByName("aTxt_win"));
	///游戏场景底部玩家信息_
	img_mydata = dynamic_cast<ImageView*>(Panel_end->getChildByName("img_mydata"));
	///玩家本人昵称label_
	txt_mynick = dynamic_cast<Text*>(Panel_end->getChildByName("img_mydata")->getChildByName("panl_name")->getChildByName("txt_mynick"));
	///玩家本人金币数量label_
	alt_mygold = dynamic_cast<TextAtlas*>(Panel_end->getChildByName("img_mydata")->getChildByName("panl_money")->getChildByName("alt_mygold"));
	///本局游戏的倍数
	alt_beishu_num = dynamic_cast<TextAtlas*>(Panel_end->getChildByName("img_mydata")->getChildByName("img_beishubg")->getChildByName("alt_beishu_num"));


	///动画集成盒子_
	Panel_action = dynamic_cast<Widget*>(Panel_Fighting->getChildByName("Panel_action"));
	///飞机_
	img_plane = dynamic_cast<ImageView*>(Panel_action->getChildByName("img_plane"));
	///火箭_
	img_rocket = dynamic_cast<ImageView*>(Panel_action->getChildByName("img_rocket"));
	
	///小船_
	img_line_boat = dynamic_cast<ImageView*>(Panel_action->getChildByName("img_line_boat"));
	///反小船_
	img_line_reboat = dynamic_cast<ImageView*>(Panel_action->getChildByName("img_line_reboat"));
	///水_
	img_line_water = dynamic_cast<ImageView*>(Panel_action->getChildByName("img_line_water"));
	///炸弹_
	img_boom = dynamic_cast<ImageView*>(Panel_action->getChildByName("img_boom"));
	///炸弹效果0_
	img_boom_0 = dynamic_cast<ImageView*>(Panel_action->getChildByName("img_boom_0"));
	///炸弹效果1_
	img_boom_1 = dynamic_cast<ImageView*>(Panel_action->getChildByName("img_boom_1"));
	///炸弹效果2_
	img_boom_2 = dynamic_cast<ImageView*>(Panel_action->getChildByName("img_boom_2"));

}

///调用谈话框_
void GameFightingLayer::setTalkBox(int talk_witch, int chair_id)
{
	ImageView* img = nullptr;

	bool sex_is_man = true;

	///设置当前玩家信息_
	if (chair_id == chair_me)
	{
		img = img_talk_me;
		//sex_is_man = sex_is_man_me;
	}
	else if (chair_id == chair_left)
	{
		img = img_talk_left;
		//sex_is_man = sex_is_man_left;
	}
	else if (chair_id == chair_right)
	{
		img = img_talk_right;
		//sex_is_man = sex_is_man_right;
	}

	img->setVisible(true);

	Sprite* txt;
	Sprite* img_;
	String* str_txt;
	return;
	switch (talk_witch)
	{
	case talk_none:
		img->removeAllChildren();

		txt = Sprite::create("GameMainScene/btn/gui-l-tips-no-call.png");
		img->addChild(txt, 10);
		txt->setPosition(img->getSize() / 2);
		if (sex_is_man)
		{
			SoundManager::shared()->playSound("MAN_SCORE_NONE_LAND");
		}
		else
		{
			SoundManager::shared()->playSound("WOMAN_SCORE_NONE_LAND");
		}
		break;
	case talk_one:
		img->removeAllChildren();
// 		
		txt = Sprite::create("GameMainScene/btn/gui-l-tips-1.png");
		img->addChild(txt, 10);
		txt->setPosition(img->getSize() / 2);
		if (sex_is_man)
		{
			SoundManager::shared()->playSound("MAN_SCORE_ONE_LAND");
		}
		else
		{
			SoundManager::shared()->playSound("WOMAN_SCORE_ONE_LAND");
		}
		break;
	case talk_two:
		img->removeAllChildren();
		txt = Sprite::create("GameMainScene/btn/gui-l-tips-2.png");
		img->addChild(txt);
		txt->setPosition(img->getSize() / 2);
		if (sex_is_man)
		{
			SoundManager::shared()->playSound("MAN_SCORE_TWO_LAND");
		}
		else
		{
			SoundManager::shared()->playSound("WOMAN_SCORE_TWO_LAND");
		}
		break;
	case talk_three:
		img->removeAllChildren();
		txt = Sprite::create("GameMainScene/btn/gui-l-tips-3.png");
		img->addChild(txt);
		txt->setPosition(img->getSize() / 2);
		if (sex_is_man)
		{
			SoundManager::shared()->playSound("MAN_SCORE_THREE_LAND");
		}
		else
		{
			SoundManager::shared()->playSound("WOMAN_SCORE_THREE_LAND");
		}
		break;
	case talk_pass:
		img->removeAllChildren();
		txt = Sprite::create("GameMainScene/btn/gui-l-tips-no-out.png");
		img->addChild(txt);
		txt->setPosition(img->getSize() / 2);
		if (sex_is_man)
		{
			SoundManager::shared()->playSound("MAN_PASS_LAND");
		}
		else
		{
			SoundManager::shared()->playSound("WOMAN_PASS_LAND");
		}
		break;
	}

}

///调用过牌声音_
void GameFightingLayer::setSoundWlakPuker(int talk_which, bool cur_sex_is_man)
{

// 	ui::TextAtlas* beishu = f_layer->alt_beishu_num;
// 	log("==============================beishu： ", StringUtils::format("%lld", atoll(beishu->getString().c_str()) * 2));
//	f_layer->alt_beishu_num->setString(StringUtils::format("%lld", atoll(f_layer->alt_beishu_num->getString().c_str()) * 2));
	switch (talk_which)
	{
	case talk_boom:
		f_layer->alt_beishu_num->setString(StringUtils::format("%lld", atoll(f_layer->alt_beishu_num->getString().c_str()) * 2));

		if (cur_sex_is_man)
		{
			SoundManager::shared()->playSound("MAN_BOOM_LAND");
		}
		else
		{
			SoundManager::shared()->playSound("WOMAN_BOOM_LAND");
		}
		///开启战斗高潮背景音乐_
		if (!is_fighting_fierce)
		{
			SoundManager::shared()->stopMusic();
			SoundManager::shared()->playMusic("FIGHTING_FIERCE_LAND");
			is_fighting_fierce = true;
		}
		///调用炸弹动画_
		callBoomAction();
		SoundManager::shared()->playSound("AC_BOOM_LAND");
		break;
	case talk_da:
		if (cur_sex_is_man)
		{
			SoundManager::shared()->playSound("MAN_DA_LAND");
		}
		else
		{
			SoundManager::shared()->playSound("WOMAN_DA_LAND");
		}
		break;
	case talk_double:
		if (cur_sex_is_man)
		{
			SoundManager::shared()->playSound("MAN_DOUBLE_LAND");
		}
		else
		{
			SoundManager::shared()->playSound("WOMAN_DOUBLE_LAND");
		}
		break;
	case talk_four_two:
		if (cur_sex_is_man)
		{
			SoundManager::shared()->playSound("MAN_FOUR_TWO_LAND");
		}
		else
		{
			SoundManager::shared()->playSound("WOMAN_FOUR_TWO_LAND");
		}
		break;
	case talk_guan:
		if (cur_sex_is_man)
		{
			SoundManager::shared()->playSound("MAN_GUAN_LAND");
		}
		else
		{
			SoundManager::shared()->playSound("WOMAN_GUAN_LAND");
		}
		break;
	case talk_line_double:
		if (cur_sex_is_man)
		{
			SoundManager::shared()->playSound("MAN_LINE_DOUBLE_LAND");
		}
		else
		{
			SoundManager::shared()->playSound("WOMAN_LINE_DOUBLE_LAND");
		}
		///调用双顺动画_
		callLineDoubleAction();
		SoundManager::shared()->playSound("AC_LINE_LAND");
		break;
	case talk_line_single:
		if (cur_sex_is_man)
		{
			SoundManager::shared()->playSound("MAN_LINE_SINGLE_LAND");
		}
		else
		{
			SoundManager::shared()->playSound("WOMAN_LINE_SINGLE_LAND");
		}
		///调用单顺动画_
		callLineSingleAction();
		SoundManager::shared()->playSound("AC_LINE_LAND");
		break;
	case talk_line_three:
		if (cur_sex_is_man)
		{
			SoundManager::shared()->playSound("MAN_LINE_THREE_LAND");
		}
		else
		{
			SoundManager::shared()->playSound("WOMAN_LINE_THREE_LAND");
		}
		///调用三顺动画_
		callLineThreeAction();
		SoundManager::shared()->playSound("AC_LINE_LAND");
		break;
	case talk_plane:
		if (cur_sex_is_man)
		{
			SoundManager::shared()->playSound("MAN_PLANE_LAND");
		}
		else
		{
			SoundManager::shared()->playSound("WOMAN_PLANE_LAND");
		}
		///调用飞机动画_
		callPlaneAction();
		SoundManager::shared()->playSound("AC_PLANE_LAND");
		break;
	case talk_rocket:
		f_layer->alt_beishu_num->setString(StringUtils::format("%lld", atoll(f_layer->alt_beishu_num->getString().c_str()) * 2));
		

		if (cur_sex_is_man)
		{
			SoundManager::shared()->playSound("MAN_ROCKET_LAND");
		}
		else
		{
			SoundManager::shared()->playSound("WOMAN_ROCKET_LAND");
		}
		///开启战斗高潮背景音乐_
		if (!is_fighting_fierce)
		{
			SoundManager::shared()->stopMusic();
			SoundManager::shared()->playMusic("FIGHTING_FIERCE_LAND");
			is_fighting_fierce = true;
		}
		///调用火箭动画_
		callRocketAction();
		SoundManager::shared()->playSound("AC_ROCKET_LAND");
		break;
	case talk_single:
		if (cur_sex_is_man)
		{
			SoundManager::shared()->playSound("MAN_SINGLE_LAND");
		}
		else
		{
			SoundManager::shared()->playSound("WOMAN_SINGLE_LAND");
		}
		break;
	case talk_three:
		if (cur_sex_is_man)
		{
			SoundManager::shared()->playSound("MAN_THREE_LAND");
		}
		else
		{
			SoundManager::shared()->playSound("WOMAN_THREE_LAND");
		}
		break;
	case talk_three_one:
		if (cur_sex_is_man)
		{
			SoundManager::shared()->playSound("MAN_THREE_ONE_LAND");
		}
		else
		{
			SoundManager::shared()->playSound("WOMAN_THREE_ONE_LAND");
		}
		break;
	case talk_three_two:
		if (cur_sex_is_man)
		{
			SoundManager::shared()->playSound("MAN_THREE_TWO_LAND");
		}
		else
		{
			SoundManager::shared()->playSound("WOMAN_THREE_TWO_LAND");
		}
		break;
	case talk_win:
		if (cur_sex_is_man)
		{
			SoundManager::shared()->playSound("MAN_WIN_LAND");
		}
		else
		{
			SoundManager::shared()->playSound("WOMAN_WIN_LAND");
		}
		break;
	}
}

///获取逻辑对应的声音选项_
int GameFightingLayer::getTalkWhichByLogic(int cur_logic)
{
	switch (cur_logic)
	{
	case CT_SINGLE:
									return talk_single;
	case CT_DOUBLE:
									return talk_double;
	case CT_THREE:
									return talk_three;
	case CT_SINGLE_LINE:
									return talk_line_single;
	case CT_DOUBLE_LINE:
									return talk_line_double;
	case CT_THREE_LINE:
									return talk_line_three;
	case CT_THREE_TAKE_ONE:
									return talk_three_one;
	case CT_THREE_TAKE_TWO:
									return talk_three_two;
	case CT_FOUR_TAKE_ONE:
	case CT_FOUR_TAKE_TWO:
									return talk_four_two;
	case CT_SINGLE_FLY:
	case CT_DOUBLE_FLY:
									return talk_plane;
	case CT_BOMB_CARD:
									return talk_boom;
	case CT_MISSILE_CARD:
									return talk_rocket;

	}

}

///全清谈话框_
void GameFightingLayer::cleanAllTalkBox(int order)
{
	if (order == chair_me)
	{
		img_talk_me->removeAllChildrenWithCleanup(true);
		img_talk_me->setVisible(false);
	}
	else if (order == chair_left)
	{
		img_talk_left->removeAllChildrenWithCleanup(true);
		img_talk_left->setVisible(false);
	}
	else if (order == chair_right)
	{
		img_talk_right->removeAllChildrenWithCleanup(true);
		img_talk_right->setVisible(false);
	}
	else if (order == 100)
	{
		img_talk_me->removeAllChildrenWithCleanup(true);
		img_talk_left->removeAllChildrenWithCleanup(true);
		img_talk_right->removeAllChildrenWithCleanup(true);

		img_talk_me->setVisible(false);
		img_talk_left->setVisible(false);
		img_talk_right->setVisible(false);
	}

}

///全清桌面扑克_
void GameFightingLayer::cleanAllPukers(int order)
{
	for (int i = 0; i < 20; i++)
	{
		if (out_puke_left[i] && (order == chair_left || order == 100))
		{
			out_puke_left[i]->removeFromParentAndCleanup(true);
			out_puke_left[i] = nullptr;
		}

		if (out_puke_right[i] && (order == chair_right || order == 100))
		{
			out_puke_right[i]->removeFromParentAndCleanup(true);
			out_puke_right[i] = nullptr;
		}

		if (out_puke_me[i] && (order == chair_me || order == 100))
		{
			out_puke_me[i]->removeFromParentAndCleanup(true);
			out_puke_me[i] = nullptr;
		}
	}
}

///设置成我的本地位置_
void GameFightingLayer::setMyChairPosition()
{
	IClientKernel * kernel = IClientKernel::get();
	int int_holder = kernel->GetMeUserItem()->GetChairID();

	if (int_holder == 1)
	{
		chair_me = 1;
		chair_left = 0;
		chair_right = 2;
	}
	else if (int_holder == 0)
	{
		chair_me = 0;
		chair_left = 2;
		chair_right = 1;
	}
	else if (int_holder == 2)
	{
		chair_me = 2;
		chair_left = 1;
		chair_right = 0;
	}

	///设置玩家性别_
	setAllPlayerSex();
}

///设置所有玩家性别_
void GameFightingLayer::setAllPlayerSex()
{
	IClientKernel * kernel = IClientKernel::get();

// 	sex_is_man_me = (kernel->GetTableUserItem(chair_me)->GetFaceID() % 8 > 3 ? true : false);
// 	sex_is_man_left = (kernel->GetTableUserItem(chair_left)->GetFaceID() % 8 > 3 ? true : false);
// 	sex_is_man_right = (kernel->GetTableUserItem(chair_right)->GetFaceID() % 8 > 3 ? true : false);
	sex_is_man_me = 1;
	sex_is_man_left = 1;
	sex_is_man_right = 1;

}

///发牌动画_
void GameFightingLayer::dealPukersAction()
{
	puke_back = Puker::create(Ox5G);
	puke_back->setPosition(Vec2(710, 610));
	addChild(puke_back);

	puke_move0 = Puker::create(Ox5G);
	puke_move0->setPosition(Vec2(710, 610));
	addChild(puke_move0);

	puke_move1 = Puker::create(Ox5G);
	puke_move1->setPosition(Vec2(710, 610));
	addChild(puke_move1);

	puke_move2 = Puker::create(Ox5G);
	puke_move2->setPosition(Vec2(710, 610));
	addChild(puke_move2);

	puke_move0->runAction(Repeat::create(Sequence::create(MoveBy::create(0.1, Vec2(-400, 0)), MoveBy::create(0, Vec2(400, 0)), NULL), 17));
	puke_move1->runAction(Repeat::create(Sequence::create(MoveBy::create(0.1, Vec2(400, 0)), MoveBy::create(0, Vec2(-400, 0)), NULL), 17));
	puke_move2->runAction(Repeat::create(Sequence::create(MoveBy::create(0.1, Vec2(0, -400)), MoveBy::create(0, Vec2(0, 400)), NULL), 17));

	dt_dealpuker = 0;
	this->schedule(schedule_selector(GameFightingLayer::update_dealPukers), 0.1f);
	SoundManager::shared()->playSound("KA_1_LAND");
}

///延时处理发牌动画_
void GameFightingLayer::update_dealPukers(float dt)
{
	my_puke_pointX = 230+40;

	if (dt_dealpuker < int_curPuker_cnts)
	{
		my_puke[dt_dealpuker] = Puker::create(puke[dt_dealpuker]);
		my_puke[dt_dealpuker]->setPosition(Vec2(my_puke_pointX + dt_dealpuker * the_puke_des, 160));
		addChild(my_puke[dt_dealpuker]);

		if (dt_dealpuker % 4 == 0)
		{
			SoundManager::shared()->playSound("KA_2_LAND");
		}
		else if (dt_dealpuker % 2 == 0)
		{
			SoundManager::shared()->playSound("KA_1_LAND");
		}

	}
	else
	{
		puke_back->removeFromParentAndCleanup(true);
		puke_move0->runAction(MoveBy::create(0.3, Vec2(-180, 0)));
		puke_move1->runAction(MoveBy::create(0.3, Vec2(180, 0)));

		///调整扑克顺序_
		resetThePukers(int_curPuker_cnts);
		this->unschedule(schedule_selector(GameFightingLayer::update_dealPukers));
	}
	dt_dealpuker++;
}

///倒计时_
void GameFightingLayer::updateTimeBack(float dt)
{
	if (int_time_back > -1)
	{
		if (int_time_back == 0)
		{
			btn_out_cards->setEnabled(false);
		}

		if (int_time_back < 5)
		{
			SoundManager::shared()->playSound("TIME_BACK_LAND");
		}

		if (!b_twoKings)
		{
			String* str_holder = String::createWithFormat("%d", int_time_back);
			txt_time_back->setString(str_holder->getCString());
		}
		else
		{
			if (int_time_back < the_sec_outCard - 1)
			{
				img_clock->setVisible(true);
				b_twoKings = false;
				cleanAllTalkBox();
				cleanAllPukers();

				if (int_cur_player == chair_me)
				{
					///弹出出牌按钮盒子_
					Panel_out_cards->setVisible(true);
					btn_pass->setEnabled(false);
					btn_pass->setBright(false);
					btn_tip_cards->setEnabled(false);
					btn_tip_cards->setBright(false);
					btn_out_cards->setEnabled(false);
					btn_out_cards->setBright(false);
					img_tip->loadTexture("GameMainScene/btn/gui-l-text-tips-hui.png");
					img_pass->loadTexture("GameMainScene/btn/gui-l-text-no-out-hui.png");
					img_out->loadTexture("GameMainScene/btn/gui-l-text-out-card-hui.png");
					///设置当前出牌逻辑类型_
					int_cur_logic = CT_ANY_IS_OK;
					int_pre_logicPoint = 0;
					int_card_cnts = 0;
					tip_landPoint = 0;

					///判断选择牌型是否为可出_,设置出牌按钮可用_
					if (logic_land->myChoicePukerIsOk(int_cur_logic, int_card_cnts, int_pre_logicPoint, my_puke, int_curPuker_cnts))
					{
						btn_out_cards->setEnabled(true);
						btn_out_cards->setBright(true);
						img_out->loadTexture("GameMainScene/btn/gui-l-text-out-card.png");
					}

					if (int_autos == 2)
					{
						///整理所有扑克_
						for (int i = 0; i < int_curPuker_cnts; i++)
						{
							if (my_puke[i]->isUp)
							{
								my_puke[i]->runAction(MoveBy::create(0, Vec2(0, -50)));
								my_puke[i]->isUp = false;
							}
						}

						IClientKernel * kernel = IClientKernel::get();

						///把最小一张扑克发送出去_
						my_puke[int_curPuker_cnts - 1]->isUp = true;

						CMD_C_OutCard outCard;
						logic_land->chooseMyPukers(my_puke, &outCard, int_curPuker_cnts, puke);
						kernel->SendSocketData(SUB_C_OUT_CARD, &outCard, sizeof(outCard)-sizeof(outCard.cbCardData) + outCard.cbCardCount*sizeof(byte));

						///处理剩余扑克_
						int_curPuker_cnts = int_curPuker_cnts - outCard.cbCardCount;
						resetThePukers(int_curPuker_cnts);
						tip_landPoint = 0;
						tip_cnts = 0;
					}
					
				}

			}

		}
		int_time_back--;
	}
	else
	{
		img_clock->setVisible(false);

		this->unschedule(schedule_selector(GameFightingLayer::updateTimeBack));

		if (int_curPuker_cnts <= 0 || int_pukers_left <= 0 || int_pukers_right <= 0 || b_flee)
		{
			///退出本游戏场景_
			ui::Button * btn_now = Button::create();
			btn_now->setTag(DLG_MB_OK);
			IClientKernel * kernel = IClientKernel::get();
			((f_layer->mTarget)->*(f_layer->mCallback))(btn_now);

			return;
		}

		if (int_cur_player == chair_me)
		{
			IClientKernel * kernel = IClientKernel::get();

			///一轮结束的情况不能放弃出牌_
			if (b_turnOver)
			{
				///整理所有扑克_
				for (int i = 0; i < int_curPuker_cnts; i++)
				{
					if (my_puke[i]->isUp)
					{
						my_puke[i]->runAction(MoveBy::create(0, Vec2(0, -50)));
						my_puke[i]->isUp = false;
					}
				}

				///把最小一张扑克发送出去_
				my_puke[int_curPuker_cnts - 1]->isUp = true;

				CMD_C_OutCard outCard;
				logic_land->chooseMyPukers(my_puke, &outCard, int_curPuker_cnts, puke);
				kernel->SendSocketData(SUB_C_OUT_CARD, &outCard, sizeof(outCard)-sizeof(outCard.cbCardData) + outCard.cbCardCount*sizeof(byte));

				///处理剩余扑克_
				int_curPuker_cnts = int_curPuker_cnts - outCard.cbCardCount;
				resetThePukers(int_curPuker_cnts);
				tip_landPoint = 0;
				tip_cnts = 0;
				int_autos++;///自动出牌次数增加一次_
				if (int_autos == 2)
				{
					setAutoPuker(true);
				}
			}
			else
			{
				///放弃出牌_
				CMD_C_OutCard outCards;
				outCards.cbCardCount = 0;
				kernel->SendSocketData(SUB_C_PASS_CARD);
				int_autos++;///自动出牌次数增加一次_
				if (int_autos == 2)
				{
					setAutoPuker(true);
				}
			}

			///隐藏叫分按钮_
			Panel_out_cards->setVisible(false);
		}

	}
}

///翻牌动画_
void GameFightingLayer::update_turnPukers(float dt)
{
	int_time_turn++;
	if (puke_move0)
	{
		puke_move0->removeFromParentAndCleanup(true);
		puke_move0 = nullptr;
	}
	if (puke_move1)
	{
		puke_move1->removeFromParentAndCleanup(true);
		puke_move1 = nullptr;
	}
	if (puke_move2)
	{
		puke_move2->removeFromParentAndCleanup(true);
		puke_move2 = nullptr;
	}

	if (int_time_turn <= Ox5I)
	{
		puke_move0 = Puker::create(int_time_turn);
		puke_move1 = Puker::create(int_time_turn);
		puke_move2 = Puker::create(int_time_turn);

	}
	else
	{
		puke_move0 = Puker::create(banker_puker[0]);
		puke_move1 = Puker::create(banker_puker[1]);
		puke_move2 = Puker::create(banker_puker[2]);

		
// 		puke_move0->runAction((DelayTime::create(0), MoveTo::create(0.3f, Vec2(80, 250)), ScaleTo::create(0.3, 0.6, 0.6)));
// 		puke_move1->runAction((DelayTime::create(0), MoveTo::create(0.3f, Vec2(100, 250)), ScaleTo::create(0.3, 0.6, 0.6)));
// 		puke_move2->runAction((DelayTime::create(0), MoveTo::create(0.3f, Vec2(120, 250)), ScaleTo::create(0.3, 0.6, 0.6)));
//		puke_move0->runAction(( MoveTo::create(0.3f, Vec2(50, 100))));
// 		puke_move1->runAction((MoveTo::create(0.3f, Vec2(150, 100))));
// 		puke_move2->runAction((MoveTo::create(0.3f, Vec2(100, 100))));

		puke_move0->runAction(CCSequence::create(
			DelayTime::create(0.3),
			MoveTo::create(0.3f, ccp(573, 724)),
			NULL));
		puke_move0->runAction(CCSequence::create(
			DelayTime::create(0.3),
			ScaleTo::create(0.3, 0.7, 0.7),
			NULL));
		puke_move1->runAction(CCSequence::create(
			DelayTime::create(0.3),
			MoveTo::create(0.3f, ccp(825, 724)),		
			NULL));
		puke_move1->runAction(CCSequence::create(
			DelayTime::create(0.3),
			 ScaleTo::create(0.3, 0.7, 0.7),
			NULL));
		puke_move2->runAction(CCSequence::create(
			DelayTime::create(0.3),
			CCMoveTo::create(0.3f, ccp(699, 724)),
			NULL));
		puke_move2->runAction(CCSequence::create(
			DelayTime::create(0.3),
			ScaleTo::create(0.3, 0.7, 0.7),
			CCCallFuncND::create(this, callfuncND_selector(GameFightingLayer::showScore), 0),
			0));

		this->unschedule(schedule_selector(GameFightingLayer::update_turnPukers));
		this->schedule(schedule_selector(GameFightingLayer::updateTimeBack), 1.0f);

		///显示托管按钮_
		btn_autoPuker->setVisible(true);

		///插牌_
		if (int_banker_player == chair_me)
		{
			puke[17] = banker_puker[0];
			puke[18] = banker_puker[1];
			puke[19] = banker_puker[2];

			int_curPuker_cnts = 20;
			resetThePukers(int_curPuker_cnts);

			///设置当前出牌逻辑类型_
			int_cur_logic = CT_ANY_IS_OK;

			///打开出牌按钮盒子_
			img_talk_me->setVisible(false);
			img_talk_left->setVisible(false);
			img_talk_right->setVisible(false);
			Panel_out_cards->setVisible(true);
			btn_pass->setEnabled(false);
			btn_pass->setBright(false);
			btn_tip_cards->setEnabled(false);
			btn_tip_cards->setBright(false);
			btn_out_cards->setEnabled(false);
			btn_out_cards->setBright(false);
			img_tip->loadTexture("GameMainScene/btn/gui-l-text-tips-hui.png");
			img_pass->loadTexture("GameMainScene/btn/gui-l-text-no-out-hui.png");
			img_out->loadTexture("GameMainScene/btn/gui-l-text-out-card-hui.png");

			if (int_autos >= 2)///自动托管状态_
			{
				///整理所有扑克_
				for (int i = 0; i < int_curPuker_cnts; i++)
				{
					if (my_puke[i]->isUp)
					{
						my_puke[i]->runAction(MoveBy::create(0, Vec2(0, -50)));
						my_puke[i]->isUp = false;
					}
				}

				IClientKernel * kernel = IClientKernel::get();

				///把最小一张扑克发送出去_
				my_puke[int_curPuker_cnts - 1]->isUp = true;

				CMD_C_OutCard outCard;
				logic_land->chooseMyPukers(my_puke, &outCard, int_curPuker_cnts, puke);
				kernel->SendSocketData(SUB_C_OUT_CARD, &outCard, sizeof(outCard)-sizeof(outCard.cbCardData) + outCard.cbCardCount*sizeof(byte));

				///处理剩余扑克_
				int_curPuker_cnts = int_curPuker_cnts - outCard.cbCardCount;
				resetThePukers(int_curPuker_cnts);
				tip_landPoint = 0;
				tip_cnts = 0;
			}
		}
		else if (int_banker_player == chair_left)
		{
			int_pukers_left = 20;
		}
		else if (int_banker_player)
		{
			int_pukers_right = 20;
		}
		///开启触摸监听_
		setTouchListener(true);
	}

	puke_move0->setPosition(Vec2(710 - 180, 610));
	puke_move1->setPosition(Vec2(710 + 180, 610));
	puke_move2->setPosition(Vec2(710, 610));
	addChild(puke_move0);
	addChild(puke_move1);
	addChild(puke_move2);

}

void GameFightingLayer::showScore(cocos2d::Node *target, void* data){
	int bbs = f_layer->beishu;
	
	char stateStr[100];
	sprintf(stateStr, "GameMainScene/layout/gui-l-icon-score%d.png", bbs);
	((ImageView*)f_layer->img_top_score)->loadTexture(stateStr);
	f_layer->img_top_score->setVisible(true);
}

///调用单连动画_
void GameFightingLayer::callLineSingleAction()
{
	ImageView* water = (ImageView*)img_line_water->clone();
	node_fighting->addChild(water);
	water->setPosition(Vec2(710, 400));

	ImageView* boat = (ImageView*)img_line_boat->clone();
	node_fighting->addChild(boat);
	boat->setPosition(Vec2(500, 480));
	boat->runAction(MoveTo::create(1.5f, Vec2(710, 480)));

	///延时隐藏动画_
	this->schedule(schedule_selector(GameFightingLayer::update_hideAction), 1.6f);
}

///调用对连动画_
void GameFightingLayer::callLineDoubleAction()
{
	ImageView* water = (ImageView*)img_line_water->clone();
	node_fighting->addChild(water);
	water->setPosition(Vec2(710, 400));

	ImageView* boat = (ImageView*)img_line_boat->clone();
	node_fighting->addChild(boat);
	boat->setPosition(Vec2(500, 480));
	boat->runAction(MoveTo::create(1.5f, Vec2(710, 480)));

	ImageView* reboat = (ImageView*)img_line_reboat->clone();
	node_fighting->addChild(reboat);
	reboat->setPosition(Vec2(920, 480));
	reboat->runAction(MoveTo::create(1.5f, Vec2(710, 480)));

	///延时隐藏动画_
	this->schedule(schedule_selector(GameFightingLayer::update_hideAction), 1.6f);
}

///调用三顺动画_
void GameFightingLayer::callLineThreeAction()
{
	ImageView* water = (ImageView*)img_line_water->clone();
	node_fighting->addChild(water);
	water->setPosition(Vec2(710, 400));

	ImageView* boat = (ImageView*)img_line_boat->clone();
	node_fighting->addChild(boat);
	boat->setPosition(Vec2(500, 480));
	boat->runAction(MoveTo::create(1.5f, Vec2(710, 480)));

	ImageView* reboat = (ImageView*)img_line_reboat->clone();
	node_fighting->addChild(reboat);
	reboat->setPosition(Vec2(920, 480));
	reboat->runAction(MoveTo::create(1.5f, Vec2(710, 480)));

	ImageView* boat_ = (ImageView*)img_line_boat->clone();
	node_fighting->addChild(boat_);
	boat_->setPosition(Vec2(500, 450));
	boat_->runAction(MoveTo::create(1.5f, Vec2(710, 450)));

	///延时隐藏动画_
	this->schedule(schedule_selector(GameFightingLayer::update_hideAction), 1.6f);
}

///调用飞机动画_
void GameFightingLayer::callPlaneAction()
{
	ImageView* plane = (ImageView*)img_plane->clone();
	node_fighting->addChild(plane);

	plane->setPosition(Vec2(1570, 400));
	plane->runAction(MoveTo::create(2.4f, Vec2(-150, 400)));

	///延时隐藏动画_
	this->schedule(schedule_selector(GameFightingLayer::update_hideAction), 2.5f);
}

///调用火箭动画_
void GameFightingLayer::callRocketAction()
{
	ImageView* rocket = (ImageView*)img_rocket->clone();
	node_fighting->addChild(rocket);

	rocket->setPosition(Vec2(650, -300));
	rocket->runAction(MoveTo::create(1.8f, Vec2(650, 1100)));

	///延时隐藏动画_
	this->schedule(schedule_selector(GameFightingLayer::update_hideAction), 1.9f);
}

///调用炸弹动画_
void GameFightingLayer::callBoomAction()
{
	ImageView* boom = (ImageView*)img_boom->clone();
	node_fighting->addChild(boom);

	boom->setPosition(Vec2(650, 950));
	boom->runAction(MoveTo::create(0.5f, Vec2(650, 500)));

	///调用炸弹帧动画_
	boom_cnts = 0;
	this->schedule(schedule_selector(GameFightingLayer::update_boomAction), 0.3f);
}

///炸弹动画_
void GameFightingLayer::update_boomAction(float dt)
{
	if (boom_cnts == 0)
	{
		ImageView* boom_x = (ImageView*)img_boom_0->clone();
		node_fighting->addChild(boom_x);
		boom_x->setPosition(Vec2(710, 400));
		boom_cnts++;
	}
	else if (boom_cnts == 1)
	{
		node_fighting->removeAllChildrenWithCleanup(true);

		ImageView* boom_0 = (ImageView*)img_boom_0->clone();
		node_fighting->addChild(boom_0);
		boom_0->setPosition(Vec2(710, 400));
		boom_0->setScale(1.4);

		ImageView* boom_x = (ImageView*)img_boom_2->clone();
		node_fighting->addChild(boom_x);
		boom_x->setPosition(Vec2(650, 350));
		boom_cnts++;
	}
	else if (boom_cnts == 2)
	{
		node_fighting->removeAllChildrenWithCleanup(true);

		ImageView* boom_x = (ImageView*)img_boom_2->clone();
		node_fighting->addChild(boom_x);
		boom_x->setPosition(Vec2(650, 350));
		boom_cnts++;
	}
	else if (boom_cnts == 3)
	{
		node_fighting->removeAllChildrenWithCleanup(true);

		ImageView* boom_1 = (ImageView*)img_boom_1->clone();
		node_fighting->addChild(boom_1);
		boom_1->setPosition(Vec2(600, 400));

		ImageView* boom_x = (ImageView*)img_boom_2->clone();
		node_fighting->addChild(boom_x);
		boom_x->setPosition(Vec2(650, 350));
		boom_cnts++;
	}
	else if (boom_cnts == 4)
	{
		node_fighting->removeAllChildrenWithCleanup(true);

		ImageView* boom_x = (ImageView*)img_boom_1->clone();
		node_fighting->addChild(boom_x);
		boom_x->setPosition(Vec2(600, 400));
		boom_cnts++;
	}
	else
	{
		///延时隐藏动画_
		this->unschedule(schedule_selector(GameFightingLayer::update_boomAction));
		this->schedule(schedule_selector(GameFightingLayer::update_hideAction), 0.75f);
	}

}

///延时隐藏过牌动画_
void GameFightingLayer::update_hideAction(float dt)
{
	node_fighting->removeAllChildrenWithCleanup(true);
	this->unschedule(schedule_selector(GameFightingLayer::update_hideAction));
}

///调整扑克顺序_
void GameFightingLayer::resetThePukers(int cnts)
{
	///设置第一张扑克的位置_
	my_puke_pointX = 750 - (the_puke_des / 2) * (cnts - 1);

	///移除原来所有节点_
	for (int i = 0; i < 20; i++)
	{
		if (my_puke[i] != nullptr)
		{
			my_puke[i]->removeFromParentAndCleanup(true);
			my_puke[i] = nullptr;
		}
	}
	
	///重置扑克_
	for (int i = 0; i < cnts; i++)
	{
		int max = puke[i];
		for (int j = i + 1; j < cnts; j++)
		{
			if (convertLandPoint(max) < convertLandPoint(puke[j]))
			{
				int x = max;
				max = puke[j];
				puke[j] = x;
			}
		}
		puke[i] = max;
	}

	///重新创建有顺序的扑克_
	for (int i = 0; i < cnts; i++)
	{
		if (puke[i] == -1)
							return;
		my_puke[i] = Puker::create(puke[i]);
		bool b1 = puke[i] == banker_puker[0] || puke[i] == banker_puker[1] || puke[i] == banker_puker[2];
		if (b1 && int_curPuker_cnts == 20)
		{
			my_puke[i]->setPosition(Vec2(my_puke_pointX + i * the_puke_des, 200));
			my_puke[i]->runAction(Sequence::create(MoveBy::create(0.5, Vec2(0, 0)), MoveBy::create(0.15, Vec2(0, -40)), NULL));
		}
		else
		{
			my_puke[i]->setPosition(Vec2(my_puke_pointX + i * the_puke_des, 160));
		}
		addChild(my_puke[i]);
	}
}

///设置监听事件_
void GameFightingLayer::setTouchListener(bool turnOn)
{
	if (turnOn)
	{
		//触摸响应注册_ 
		touchListener = EventListenerTouchOneByOne::create();//创建单点触摸事件监听器_
		touchListener->setSwallowTouches(false);
		touchListener->onTouchBegan = CC_CALLBACK_2(GameFightingLayer::onTouchBegan, this);//触摸开始_
		touchListener->onTouchMoved = CC_CALLBACK_2(GameFightingLayer::onTouchMoved, this);//触摸移动_
		touchListener->onTouchEnded = CC_CALLBACK_2(GameFightingLayer::onTouchEnded, this);//触摸结束_
		_eventDispatcher->addEventListenerWithSceneGraphPriority(touchListener, this);//注册分发器_
	}
	else
	{
		_eventDispatcher->removeEventListener(touchListener);
	}
}

///玩家出牌显示_
void GameFightingLayer::displayOutCards(int pre_player, bool is_over)
{
	int puke_start_x = 0;///扑克位置起点_x
	int puke_pos_y = 520;///扑克的高度_
	int puke_step = 30;///扑克的间距_
	float s_scale = 0.7;///扑克缩放的大小_

	if (is_over)
	{
		puke_pos_y = puke_pos_y + 100;
	}

	///出牌显示_
	if (pre_player == chair_left)
	{
		puke_start_x = 350;

		///清空原先盒子内的扑克_
		for (int i = 0; i < 20; i++)
		{
			if (out_puke_left[i])
			{
				out_puke_left[i]->removeFromParentAndCleanup(true);
				out_puke_left[i] = nullptr;
			}
		}

		for (int i = 0; i < int_card_cnts; i++)
		{
			if (i >= 10)
			{
				out_puke_left[i] = Puker::create(out_point[i]);
				out_puke_left[i]->setPosition(Vec2(puke_start_x + puke_step * (i - 10), puke_pos_y - 50));
				Panel_Fighting->addChild(out_puke_left[i]);
				out_puke_left[i]->setScale(s_scale);
			}
			else
			{
				out_puke_left[i] = Puker::create(out_point[i]);
				out_puke_left[i]->setPosition(Vec2(puke_start_x + puke_step * i, puke_pos_y));
				Panel_Fighting->addChild(out_puke_left[i]);
				out_puke_left[i]->setScale(s_scale);
			}
		}

	}
	else if (pre_player == chair_right)
	{
		puke_start_x = 1070 - (int_card_cnts - 1) * puke_step;
		if (int_card_cnts > 10)
			puke_start_x = 1070 - (10 - 1) * puke_step;

		///清空原先盒子内的扑克_
		for (int i = 0; i < 20; i++)
		{
			if (out_puke_right[i])
			{
				out_puke_right[i]->removeFromParentAndCleanup(true);
				out_puke_right[i] = nullptr;
			}
		}

		for (int i = 0; i < int_card_cnts; i++)
		{
			if (i >= 10)
			{
				out_puke_right[i] = Puker::create(out_point[i]);
				out_puke_right[i]->setPosition(Vec2(puke_start_x + puke_step * (i - 10), puke_pos_y - 50));
				Panel_Fighting->addChild(out_puke_right[i]);
				out_puke_right[i]->setScale(s_scale);
			}
			else
			{
				out_puke_right[i] = Puker::create(out_point[i]);
				out_puke_right[i]->setPosition(Vec2(puke_start_x + puke_step * i, puke_pos_y));
				Panel_Fighting->addChild(out_puke_right[i]);
				out_puke_right[i]->setScale(s_scale);
			}

		}
	}
	else if (pre_player == chair_me)
	{
		puke_start_x = 725 - (puke_step / 2) * (int_card_cnts - 1);
		if (int_card_cnts > 10)
			puke_start_x = 750 - (puke_step / 2) * (10 - 1);
		puke_pos_y = 330;

		///清空原先盒子内的扑克_
		for (int i = 0; i < 20; i++)
		{
			if (out_puke_me[i])
			{
				out_puke_me[i]->removeFromParentAndCleanup(true);
				out_puke_me[i] = nullptr;
			}
		}

		for (int i = 0; i < int_card_cnts; i++)
		{
			if (i >= 10)
			{
				out_puke_me[i] = Puker::create(out_point[i]);
				out_puke_me[i]->setPosition(Vec2(puke_start_x + puke_step * (i - 10), puke_pos_y - 50));
				Panel_Fighting->addChild(out_puke_me[i]);
				out_puke_me[i]->setScale(s_scale);
			}
			else
			{
				out_puke_me[i] = Puker::create(out_point[i]);
				out_puke_me[i]->setPosition(Vec2(puke_start_x + puke_step * i, puke_pos_y));
				Panel_Fighting->addChild(out_puke_me[i]);
				out_puke_me[i]->setScale(s_scale);
			}

		}
	}

}

///设置托管扑克_
void GameFightingLayer::setAutoPuker(bool isOn)
{
	if (isOn)
	{
		AutoPukers = cocostudio::GUIReader::getInstance()->widgetFromJsonFile("doudizhuGameScene/AutoPukers.json");
		addChild(AutoPukers, AutoLayerZ_order);

		int_autos = 2;
		if (int_cur_player == chair_me)
			int_time_back = 0;

		///隐藏托管按钮_
		btn_autoPuker->setVisible(false);

		///取消托管按钮_
		btn_drop_autoPuker = dynamic_cast<Button*>(AutoPukers->getChildByName("btn_drop_autoPuker"));
		btn_drop_autoPuker->addTouchEventListener(this, SEL_TouchEvent(&GameFightingLayer::btnDropAutoPukerCallBack));
	}
	else
	{
		if (AutoPukers)
		{
			int_autos = 0;
			AutoPukers->removeFromParentAndCleanup(true);
			AutoPukers = nullptr;

			///隐藏托管按钮_
			btn_autoPuker->setVisible(true);
		}
		
	}

}

bool GameFightingLayer::onTouchBegan(Touch* touch, Event* event)
{
	m_isMoved = false;

	sLocalPos = convertToNodeSpace(touch->getLocation());
	bool b1 = sLocalPos.x > my_puke_pointX - 73;
	bool b2 = sLocalPos.x < my_puke_pointX + (int_curPuker_cnts - 1) * the_puke_des + 73;

	int_hui = 0;
	int_move_s = -1;
	int_move_left = 21;
	int_move_right = -1;
	for (int i = 0; i < 20; i++)
	{
		my_puke_touchMoved[i] = false;
		move_puke[i] = -1;
	}

	if ( b1 && b2 )
	{
		if (sLocalPos.y >= 21 + 50 && sLocalPos.y <= 199)
		{
			int order = ((int)sLocalPos.x - my_puke_pointX + 73) / the_puke_des;
			if (order > int_curPuker_cnts - 1)
				order = int_curPuker_cnts - 1;
			spt_hui[int_hui] = Sprite::create("doudizhuGameScene/GameMainScene/puker/puker_hui.png");
			my_puke[order]->addChild(spt_hui[int_hui]);
			int_hui++;
			my_puke_touchMoved[order] = true;
			int_move_s = order;
			return true;
		}
		else if (sLocalPos.y > 199 && sLocalPos.y < 199 + 50)
		{
			for (int i = int_curPuker_cnts - 1; i != -1; i--)
			{
				if (my_puke[i]->getMyBoundingBox().containsPoint(sLocalPos))
				{
					spt_hui[int_hui] = Sprite::create("doudizhuGameScene/GameMainScene/puker/puker_hui.png");
					my_puke[i]->addChild(spt_hui[int_hui]);
					int_hui++;
					my_puke_touchMoved[i] = true;
					int_move_s = i;
					return true;
				}
			}
		}
		else if (sLocalPos.y > 21 && sLocalPos.y < 21 + 50)
		{
			for (int i = int_curPuker_cnts - 1; i != -1; i--)
			{
				if (my_puke[i]->getMyBoundingBox().containsPoint(sLocalPos))
				{
					spt_hui[int_hui] = Sprite::create("doudizhuGameScene/GameMainScene/puker/puker_hui.png");
					my_puke[i]->addChild(spt_hui[int_hui]);
					int_hui++;
					my_puke_touchMoved[i] = true;
					int_move_s = i;
					return true;
				}
			}
		}
	}
	
	for (int i = 0; i < int_curPuker_cnts; i++)
	{
		if (my_puke[i]->isUp)
		{
			my_puke[i]->runAction(MoveBy::create(0, Vec2(0, -50)));
			my_puke[i]->isUp = false;
		}
	}
	
	tip_landPoint = int_pre_logicPoint;///提示选牌时设置提示点数复位_
	btn_out_cards->setEnabled(false);
	btn_out_cards->setBright(false);
	img_out->loadTexture("GameMainScene/btn/gui-l-text-out-card-hui.png");

	return false;
}

void GameFightingLayer::onTouchMoved(Touch* touch, Event* event)
{
	Point mLocalPos = convertToNodeSpace(touch->getLocation());
	bool b1 = mLocalPos.x > my_puke_pointX - 73;
	bool b2 = mLocalPos.x < my_puke_pointX + (int_curPuker_cnts - 1) * the_puke_des + 73;
	bool b3 = mLocalPos.y > 21 && mLocalPos.y < 199;

	if (b1 && b2 && b3)
	{
		int order = ((int)mLocalPos.x - my_puke_pointX + 73) / the_puke_des;
		if (order > int_curPuker_cnts - 1)
		{
			order = int_curPuker_cnts - 1;
		}

		///从左往右滑动_
		if (order > int_move_s && order >= int_move_right)
		{
			int_move_right = order;
			for (int i = int_move_s; i <= int_move_right; i++)
			{
				if (!my_puke_touchMoved[i])
				{
					spt_hui[int_hui] = Sprite::create("doudizhuGameScene/GameMainScene/puker/puker_hui.png");
					my_puke[i]->addChild(spt_hui[int_hui]);
					my_puke_touchMoved[i] = true;
					move_puke[int_hui - 1] = i;
					int_hui++;
				}
			}
			if (int_hui > 4)
				m_isMoved = true;
			return;
		}
		
		///从右往左滑动_
		if (order < int_move_s && order <= int_move_left)
		{
			int_move_left = order;
			for (int i = int_move_s; i >= int_move_left; i--)
			{
				if (!my_puke_touchMoved[i])
				{
					spt_hui[int_hui] = Sprite::create("doudizhuGameScene/GameMainScene/puker/puker_hui.png");
					my_puke[i]->addChild(spt_hui[int_hui]);
					my_puke_touchMoved[i] = true;
					move_puke[int_hui - 1] = i;
					int_hui++;
				}
			}

		}

	}

	if (int_hui > 4)
		m_isMoved = true;
}

void GameFightingLayer::onTouchEnded(Touch* touch, Event* event)
{
	for (int i = 0; i < int_hui; i++)
	{
		spt_hui[i]->removeFromParentAndCleanup(true);
		spt_hui[i] = nullptr;
		if (move_puke[i] != -1)
		{
			if (!my_puke[move_puke[i]]->isUp)
			{
				my_puke[move_puke[i]]->runAction(MoveBy::create(0.1, Vec2(0, 50)));
			}
			else
			{
				my_puke[move_puke[i]]->runAction(MoveBy::create(0.1, Vec2(0, -50)));
			}
			my_puke[move_puke[i]]->isUp = !(my_puke[move_puke[i]]->isUp);
		}
	}
	

	if (sLocalPos.y > 199 && sLocalPos.y < 199 + 50)
	{
		for (int i = int_curPuker_cnts - 1; i != -1; i--)
		{
			if (my_puke[i]->getMyBoundingBox().containsPoint(sLocalPos))
			{
				if (!my_puke[i]->isUp)
				{
					my_puke[i]->runAction(MoveBy::create(0.1, Vec2(0, 50)));
				}
				else
				{
					my_puke[i]->runAction(MoveBy::create(0.1, Vec2(0, -50)));
				}
				my_puke[i]->isUp = !(my_puke[i]->isUp);

				///判断选择牌型是否为可出_,设置出牌按钮可用_
				if (int_cur_logic == CT_ANY_IS_OK)
				{
					int_pre_logicPoint = 0;
					int_card_cnts = 0;
					tip_landPoint = 0;
				}
					

				if (logic_land->myChoicePukerIsOk(int_cur_logic, int_card_cnts, int_pre_logicPoint, my_puke, int_curPuker_cnts))
				{
					btn_out_cards->setEnabled(true);
					btn_out_cards->setBright(true);
					img_out->loadTexture("GameMainScene/btn/gui-l-text-out-card.png");
				}
				else
				{
					btn_out_cards->setEnabled(false);
					btn_out_cards->setBright(false);
					img_out->loadTexture("GameMainScene/btn/gui-l-text-out-card-hui.png");
				}

				return;
			}
		}
	}
	else if (sLocalPos.y > 21 && sLocalPos.y < 21 + 50)
	{
		for (int i = int_curPuker_cnts - 1; i != -1; i--)
		{
			if (my_puke[i]->getMyBoundingBox().containsPoint(sLocalPos))
			{
				if (!my_puke[i]->isUp)
				{
					my_puke[i]->runAction(MoveBy::create(0.1, Vec2(0, 50)));
				}
				else
				{
					my_puke[i]->runAction(MoveBy::create(0.1, Vec2(0, -50)));
				}
				my_puke[i]->isUp = !(my_puke[i]->isUp);

				///判断选择牌型是否为可出_,设置出牌按钮可用_
				if (int_cur_logic == CT_ANY_IS_OK)
				{
					int_pre_logicPoint = 0;
					int_card_cnts = 0;
				}

				if (logic_land->myChoicePukerIsOk(int_cur_logic, int_card_cnts, int_pre_logicPoint, my_puke, int_curPuker_cnts))
				{
					btn_out_cards->setEnabled(true);
					btn_out_cards->setBright(true);
					img_out->loadTexture("GameMainScene/btn/gui-l-text-out-card.png");
				}
				else
				{
					btn_out_cards->setEnabled(false);
					btn_out_cards->setBright(false);
					img_out->loadTexture("GameMainScene/btn/gui-l-text-out-card-hui.png");
				}

				return;
			}
		}
	}
	else
	{
		int order = ((int)sLocalPos.x - my_puke_pointX + 73) / the_puke_des;
		if (order > int_curPuker_cnts - 1)
		{
			order = int_curPuker_cnts - 1;
		}

		if (!my_puke[order]->isUp)
		{
			my_puke[order]->runAction(MoveBy::create(0.1, Vec2(0, 50)));
		}
		else
		{
			my_puke[order]->runAction(MoveBy::create(0.1, Vec2(0, -50)));
		}
		my_puke[order]->isUp = !(my_puke[order]->isUp);
	}
	if (m_isMoved)
		logic_land->autoSeclectPukers(my_puke, int_curPuker_cnts);

	///判断选择牌型是否为可出_,设置出牌按钮可用_
	if (int_cur_logic == CT_ANY_IS_OK)
	{
		int_pre_logicPoint = 0;
		int_card_cnts = 0;
		tip_landPoint = 0;
	}

	if (logic_land->myChoicePukerIsOk(int_cur_logic, int_card_cnts, int_pre_logicPoint, my_puke, int_curPuker_cnts))
	{
		btn_out_cards->setEnabled(true);
		btn_out_cards->setBright(true);
		img_out->loadTexture("GameMainScene/btn/gui-l-text-out-card.png");
	}
	else
	{
		btn_out_cards->setEnabled(false);
		btn_out_cards->setBright(false);
		img_out->loadTexture("GameMainScene/btn/gui-l-text-out-card-hui.png");
	}
	
}

void GameFightingLayer::btnPassCallBack(cocos2d::Ref * ref, ui::TouchEventType eType)
{
	if (eType == ui::TouchEventType::TOUCH_EVENT_ENDED)
	{
		///把所有选中扑克撤下_
		for (int i = 0; i < int_curPuker_cnts; i++)
		{
			if (my_puke[i]->isUp)
			{
				my_puke[i]->runAction(MoveBy::create(0, Vec2(0, -50)));
				my_puke[i]->isUp = false;
			}
		}

		IClientKernel * kernel = IClientKernel::get();

		///关闭定时器_
		this->unschedule(schedule_selector(GameFightingLayer::updateTimeBack));
		///放弃出牌_
		CMD_C_OutCard outCards;
		outCards.cbCardCount = 0;
		kernel->SendSocketData(SUB_C_PASS_CARD);

		///隐藏闹钟和叫分按钮_
		img_clock->setVisible(false);
		Panel_out_cards->setVisible(false);
	}
}

void GameFightingLayer::btnTipCardsCallBack(cocos2d::Ref * ref, ui::TouchEventType eType)
{
	if (eType == ui::TouchEventType::TOUCH_EVENT_BEGAN)
	{
		//SoundManager::shared()->playSound("ADD_SCORE");

		///取消自动托管_
		int_autos = 0;
		setAutoPuker(false);

	}
	else if (eType == ui::TouchEventType::TOUCH_EVENT_CANCELED)
	{
	}
	else if (eType == ui::TouchEventType::TOUCH_EVENT_ENDED)
	{
		IClientKernel * kernel = IClientKernel::get();

		tip_landPoint = logic_land->tipOutCards(my_puke, int_curPuker_cnts, int_cur_logic, int_card_cnts, tip_landPoint);
		
		if (tip_landPoint == -1)
		{
			if (tip_cnts == 0)///要不起_
			{
				if (int_autos < 2)
				{
					///关闭定时器_
					this->unschedule(schedule_selector(GameFightingLayer::updateTimeBack));
					///放弃出牌_
					CMD_C_OutCard outCards;
					outCards.cbCardCount = 0;
					kernel->SendSocketData(SUB_C_PASS_CARD);
				}

				///隐藏闹钟和叫分按钮_
				img_clock->setVisible(false);
				Panel_out_cards->setVisible(false);
			}
			tip_landPoint = int_pre_logicPoint;
			LogicLand::setB_Loop();
			btn_out_cards->setEnabled(false);
			btn_out_cards->setBright(false);
			img_out->loadTexture("GameMainScene/btn/gui-l-text-out-card-hui.png");
			out_enable = false;
		}
		else
		{
			tip_cnts++;
			if (int_cur_logic != CT_SINGLE_FLY && int_cur_logic != CT_DOUBLE_FLY)
			{
				btn_out_cards->setEnabled(true);
				btn_out_cards->setBright(true);
				img_out->loadTexture("GameMainScene/btn/gui-l-text-out-card.png");
				out_enable = true;
			}
			else
			{
				btn_out_cards->setEnabled(false);
				btn_out_cards->setBright(false);
				img_out->loadTexture("GameMainScene/btn/gui-l-text-out-card-hui.png");
				out_enable = false;
				if (tip_landPoint >= 50)
				{
					btn_out_cards->setEnabled(true);
					btn_out_cards->setBright(true);
					img_out->loadTexture("GameMainScene/btn/gui-l-text-out-card.png");
					out_enable = true;
				}

			}
			
		}

	}
}

void GameFightingLayer::btnOutCardsCallBack(cocos2d::Ref * ref, ui::TouchEventType eType)
{
	if (eType == ui::TouchEventType::TOUCH_EVENT_ENDED)
	{
		IClientKernel * kernel = IClientKernel::get();

		///发送扑克_
		CMD_C_OutCard outCard;
		logic_land->chooseMyPukers(my_puke, &outCard, int_curPuker_cnts, puke);
		kernel->SendSocketData(SUB_C_OUT_CARD, &outCard, sizeof(outCard)-sizeof(outCard.cbCardData) + outCard.cbCardCount*sizeof(byte));

		///处理剩余扑克_
		int_curPuker_cnts = int_curPuker_cnts - outCard.cbCardCount;
		resetThePukers(int_curPuker_cnts);
		tip_landPoint = 0;
		tip_cnts = 0;

		///关闭定时器_
		this->unschedule(schedule_selector(GameFightingLayer::updateTimeBack));

		///隐藏闹钟和叫分按钮_
		img_clock->setVisible(false);
		Panel_out_cards->setVisible(false);
	}
}

void GameFightingLayer::btnContinueCallBack(cocos2d::Ref * ref, ui::TouchEventType eType)
{
	if (eType == ui::TouchEventType::TOUCH_EVENT_ENDED)
	{
		///重新设置背景音乐_
		SoundManager::shared()->stopMusic();
		SoundManager::shared()->playMusic("BACK_MUSIC_LAND");

		///向服务器请求准备开始游戏_
		IClientKernel * kernel = IClientKernel::get();
		kernel->SendUserReady(NULL, 0);

		this->removeFromParent();
	}
}

void GameFightingLayer::btnExitCallBack(cocos2d::Ref * ref, ui::TouchEventType eType)
{
	if (eType == ui::TouchEventType::TOUCH_EVENT_ENDED)
	{
		///退出本游戏场景_
		ui::Button * btn_now = Button::create();
		btn_now->setTag(DLG_MB_OK);
		IClientKernel * kernel = IClientKernel::get();
		((f_layer->mTarget)->*(f_layer->mCallback))(btn_now);

		this->removeFromParent();
	}
}

void GameFightingLayer::btnDropAutoPukerCallBack(cocos2d::Ref * ref, ui::TouchEventType eType)
{
	if (eType == ui::TouchEventType::TOUCH_EVENT_ENDED)
	{
		///取消托管_
		setAutoPuker(false);
	}

}

void GameFightingLayer::btnAutoPukerCallBack(cocos2d::Ref * ref, ui::TouchEventType eType)
{
	if (eType == ui::TouchEventType::TOUCH_EVENT_ENDED)
	{
		///开启托管_
		setAutoPuker(true);
	}
}

///转换成地主点数_
int GameFightingLayer::convertLandPoint(int point)
{
	if (point < 52)
	{
		if (point % 13 == 0)
		{
			return 13 + 1;///A
		}
		else if (point % 13 == 1)
		{
			return 14 + 1;///2
		}
		else
		{
			return point % 13 + 1;
		}
	}
	else
	{
		if (point == Ox4E)
		{
			return 15 + 1;
		}
		else if (point == Ox4F)
		{
			return 16 + 1;
		}
	}
		
}

///设置游戏战斗开始_
void GameFightingLayer::setGameFightingStart(void * data, int dataSize)
{
	///设置玩家性别_
	setAllPlayerSex();

	///播放战斗开始音效_
	SoundManager::shared()->playSound("GAME_START_LAND");

	///转换结构体类型_
	CMD_S_GameStart* pStatusStart = (CMD_S_GameStart*)data;
	
	///将服务器端的扑克面值转换成本地想要的值_
	for (int i = 0; i < 17; i++)
	{
		puke[i] = convertTheMyPukerValue((pStatusStart->cbCardData)[i]);
	}

	///发牌动画_
	int_curPuker_cnts = 17;
	dealPukersAction();
	
}

///设置游戏进行状态_
void GameFightingLayer::setGameFightingPlay(void* data, int dataSize)
{
	
	///设置玩家性别_
	setAllPlayerSex();

	///播放战斗开始音效_
	SoundManager::shared()->playSound("GAME_START_LAND");

	///转换结构体类型_
	CMD_S_StatusPlay* pStatusPlay = (CMD_S_StatusPlay*)data;
	int_banker_player = pStatusPlay->wBankerUser;
	int_cur_player = pStatusPlay->wCurrentUser;

	///判断是否是首出_
	int sum = 0;
	for (int i = 0; i < 3; i++)
	{
		sum = sum + pStatusPlay->cbHandCardCount[i];
	}

	if (sum == 54)
	{
		int_time_back = the_sec_headOut;
	}
	else
	{
		int_time_back = the_sec_outCard;
	}

	///开启闹钟计时_
	img_clock->setVisible(true);
	String* str = String::createWithFormat("%d", int_time_back);
	txt_time_back->setString(str->getCString());
	this->schedule(schedule_selector(GameFightingLayer::updateTimeBack), 1.0f);

	///设置扑克张数_
	int_pukers_left = pStatusPlay->cbHandCardCount[chair_left];
	int_pukers_right = pStatusPlay->cbHandCardCount[chair_right];
	int_curPuker_cnts = pStatusPlay->cbHandCardCount[chair_me];

	///显示出牌信息_
	if (sum != 54)
	{
		int_card_cnts = pStatusPlay->cbTurnCardCount;
		for (int i = 0; i < int_card_cnts; i++)
		{
			out_point[i] = convertTheMyPukerValue((pStatusPlay->cbTurnCardData)[i]);
		}

		///展示上家出牌_
		displayOutCards(pStatusPlay->wTurnWiner);

		///判断当前出牌类型_
		int land_point[20];
		for (int i = 0; i < int_card_cnts; i++)
		{
			land_point[i] = convertLandPoint(out_point[i]);
		}
		int_cur_logic = logic_land->getLandLogic(land_point, int_card_cnts);
		int_pre_logicPoint = logic_land->getLogicPoint(int_cur_logic, land_point, int_card_cnts);
		tip_landPoint = int_pre_logicPoint;
		btn_pass->setEnabled(true);
		btn_pass->setBright(true);
		btn_tip_cards->setEnabled(true);
		btn_tip_cards->setBright(true);
		img_tip->loadTexture("GameMainScene/btn/gui-l-text-tips.png");
		img_pass->loadTexture("GameMainScene/btn/gui-l-text-no-out.png");
		
	}
	else
	{
		int_cur_logic = CT_ANY_IS_OK;
		int_pre_logicPoint = 0;
		int_card_cnts = 0;
		tip_landPoint = 0;
		btn_pass->setEnabled(false);
		btn_pass->setBright(false);
		btn_tip_cards->setEnabled(false);
		btn_tip_cards->setBright(false);
		img_tip->loadTexture("GameMainScene/btn/gui-l-text-tips-hui.png");
		img_pass->loadTexture("GameMainScene/btn/gui-l-text-no-out-hui.png");
		
	}

	///设置闹钟位置_
	if (int_cur_player == chair_left)
	{
		img_clock->setPosition(Position_left);
	}
	else if (int_cur_player == chair_right)
	{
		img_clock->setPosition(Position_right);
	}
	else if (int_cur_player == chair_me)
	{
		img_clock->setPosition(Position_me);
		
		///弹出出牌按钮盒子_
		Panel_out_cards->setVisible(true);
		btn_out_cards->setEnabled(false);
		btn_out_cards->setBright(false);
		img_out->loadTexture("GameMainScene/btn/gui-l-text-out-card-hui.png");
	}

	///显示我的扑克_
	for (int i = 0; i < int_curPuker_cnts; i++)
	{
		puke[i] = convertTheMyPukerValue((pStatusPlay->cbHandCardData)[i]);
	}

	///展示扑克_
	resetThePukers(int_curPuker_cnts);

	///开启单点触摸_
	setTouchListener(true);
}

///设置庄家信息_
void GameFightingLayer::setFightBankerInfo(void* data, int dataSize, FrameLayer_doudizhu* frame_layer)
{
	f_layer = frame_layer;
	///庄家信息音效_
	SoundManager::shared()->playSound("BANKER_INFO_LAND");

	///转换结构体类型_
	CMD_S_BankerInfo* pStatusStart = (CMD_S_BankerInfo*)data;
	int_banker_player = pStatusStart->wBankerUser;
	int_cur_player = pStatusStart->wCurrentUser;

	for (int i = 0; i < 3; i++)
	{
		banker_puker[i] = convertTheMyPukerValue((pStatusStart->cbBankerCard)[i]);
	}

	///开启闹钟计时_
	img_clock->setVisible(true);
	int_time_back = the_sec_headOut;
	String* str = String::createWithFormat("%d", the_sec_headOut);
	txt_time_back->setString(str->getCString());

	///开启翻牌动画_
	int_time_turn = Ox5G;
	this->schedule(schedule_selector(GameFightingLayer::update_turnPukers), 0.1f);
	b_turnOver = true;

	if (int_cur_player == chair_left)
	{
		img_clock->setPosition(Position_left);
	}
	else if (int_cur_player == chair_right)
	{
		img_clock->setPosition(Position_right);
	}
	else if (int_cur_player == chair_me)
	{
		img_clock->setPosition(Position_me);
	}
	
}

///设置用户出牌_
void GameFightingLayer::setFightOutcards(void* data, int dataSize)
{
	
	///播放出牌音效_
	SoundManager::shared()->playSound("KA_3_LAND");

	///清理上次过牌动画_
	node_fighting->removeAllChildrenWithCleanup(true);
	LogicLand::setB_Loop();

	///解析数据_
	CMD_S_OutCard* pStatusStart = (CMD_S_OutCard*)data;
	int_cur_player = pStatusStart->wCurrentUser;
	int_card_cnts = pStatusStart->cbCardCount;
	int int_pre_player = pStatusStart->wOutCardUser;
	int int_outCard_player = pStatusStart->wOutCardUser;

	if (int_outCard_player == chair_left)
	{
		int_pukers_left = int_pukers_left - int_card_cnts;
	}
	else if (int_outCard_player == chair_right)
	{
		int_pukers_right = int_pukers_right - int_card_cnts;
	}

	///关闭上家倒计时闹钟_
	img_clock->setVisible(false);
	this->unschedule(schedule_selector(GameFightingLayer::updateTimeBack));
	tip_cnts = 0;

	///上家用户出牌信息_
	for (int i = 0; i < int_card_cnts; i++)
	{
		out_point[i] = convertTheMyPukerValue((pStatusStart->cbCardData)[i]);
	}

	///判断是否出了大小王_
	if (out_point[0] > 51 && out_point[1] > 51)
	{
		b_kingsD = true;///锁_
		b_twoKings = true;
		b_turnOver = true;
	}
	else
		b_twoKings = false;

	///判断当前出牌类型_
	int land_point[20];
	for (int i = 0; i < int_card_cnts; i++)
	{
		land_point[i] = convertLandPoint(out_point[i]);
	}
	int_cur_logic = logic_land->getLandLogic(land_point, int_card_cnts);
	int_pre_logicPoint = logic_land->getLogicPoint(int_cur_logic, land_point, int_card_cnts);
	tip_landPoint = int_pre_logicPoint;

	///清除所有对话框以及底牌_
	if (b_turnOver == true)
	{
		cleanAllTalkBox();
		cleanAllPukers();

		///出牌音效_
		if (int_pre_player == chair_me)
		{
			setSoundWlakPuker(getTalkWhichByLogic(int_cur_logic), sex_is_man_me);
		}
		else if (int_pre_player == chair_left)
		{
			setSoundWlakPuker(getTalkWhichByLogic(int_cur_logic), sex_is_man_left);
		}
		else if (int_pre_player == chair_right)
		{
			setSoundWlakPuker(getTalkWhichByLogic(int_cur_logic), sex_is_man_right);
		}
		
// 		if (puke_move0 && puke_move1 && puke_move2)
// 		{
// 			puke_move0->removeFromParentAndCleanup(true);
// 			puke_move0 = nullptr;
// 			puke_move1->removeFromParentAndCleanup(true);
// 			puke_move1 = nullptr;
// 			puke_move2->removeFromParentAndCleanup(true);
// 			puke_move2 = nullptr;
// 		}

		b_turnOver = false;
	}
	else if ( int_cur_logic != CT_BOMB_CARD   && int_cur_logic != CT_MISSILE_CARD && ///炸弹火箭_
			  int_cur_logic != CT_DOUBLE_LINE && int_cur_logic != CT_SINGLE_LINE  && ///单双顺_
			  int_cur_logic != CT_SINGLE_FLY  && int_cur_logic != CT_DOUBLE_FLY)     ///飞机_
	{
		///出牌音效_
		srand(time(0));
		int r = rand() % 2;
		
		int talk[2] = {talk_guan, talk_da};
		if (int_pre_player == chair_me)
		{
			setSoundWlakPuker(talk[r], sex_is_man_me);
		}
		else if (int_pre_player == chair_left)
		{
			setSoundWlakPuker(talk[r], sex_is_man_left);
		}
		else if (int_pre_player == chair_right)
		{
			setSoundWlakPuker(talk[r], sex_is_man_right);
		}

	}
	else
	{
		///出牌音效_
		if (int_pre_player == chair_me)
		{
			setSoundWlakPuker(getTalkWhichByLogic(int_cur_logic), sex_is_man_me);
		}
		else if (int_pre_player == chair_left)
		{
			setSoundWlakPuker(getTalkWhichByLogic(int_cur_logic), sex_is_man_left);
		}
		else if (int_pre_player == chair_right)
		{
			setSoundWlakPuker(getTalkWhichByLogic(int_cur_logic), sex_is_man_right);
		}
	}

	///展示上家出牌_
	Panel_out_cards->setVisible(false);
	displayOutCards(int_pre_player);

	///调整闹钟位置以及清除自身的对话和扑克_
	if (int_cur_player == chair_left)
	{
		img_clock->setPosition(Position_left);

		if (b_twoKings && b_kingsD)
		{
			int_time_back = the_sec_outCard;
			this->schedule(schedule_selector(GameFightingLayer::updateTimeBack), 1.0f);
			b_kingsD = false;
			setTalkBox(talk_pass, chair_me);
			setTalkBox(talk_pass, chair_right);
			cleanAllPukers(chair_me);
			cleanAllPukers(chair_right);
			return;
		}

		cleanAllTalkBox(chair_left);
		cleanAllPukers(chair_left);
		
	}
	else if (int_cur_player == chair_right)
	{
		img_clock->setPosition(Position_right);

		if (b_twoKings && b_kingsD)
		{
			int_time_back = the_sec_outCard;
			this->schedule(schedule_selector(GameFightingLayer::updateTimeBack), 1.0f);
			b_kingsD = false;
			setTalkBox(talk_pass, chair_me);
			setTalkBox(talk_pass, chair_left);
			cleanAllPukers(chair_me);
			cleanAllPukers(chair_left);
			return;
		}

		cleanAllTalkBox(chair_right);
		cleanAllPukers(chair_right);

	}
	else if (int_cur_player == chair_me)
	{
		img_clock->setPosition(Position_me);

		///火箭_
		if (b_twoKings && b_kingsD)
		{
			int_time_back = the_sec_outCard;
			this->schedule(schedule_selector(GameFightingLayer::updateTimeBack), 1.0f);
			b_kingsD = false;
			setTalkBox(talk_pass, chair_left);
			setTalkBox(talk_pass, chair_right);
			cleanAllPukers(chair_left);
			cleanAllPukers(chair_right);
			return;
		}

		///弹出出牌按钮盒子_
		Panel_out_cards->setVisible(true);
		btn_pass->setEnabled(true);
		btn_pass->setBright(true);
		btn_tip_cards->setEnabled(true);
		btn_tip_cards->setBright(true);
		btn_out_cards->setEnabled(false);
		btn_out_cards->setBright(false);

		img_pass->loadTexture("GameMainScene/btn/gui-l-text-no-out.png");
		img_tip->loadTexture("GameMainScene/btn/gui-l-text-tips.png");
		img_out->loadTexture("GameMainScene/btn/gui-l-text-out-card-hui.png");

		///判断选择牌型是否为可出_,设置出牌按钮可用_
		if (logic_land->myChoicePukerIsOk(int_cur_logic, int_card_cnts, int_pre_logicPoint, my_puke, int_curPuker_cnts))
		{
			btn_out_cards->setEnabled(true);
			btn_out_cards->setBright(true);
			img_out->loadTexture("GameMainScene/btn/gui-l-text-out-card.png");
		}

		///出牌前清理_
		cleanAllTalkBox(chair_me);
		cleanAllPukers(chair_me);
	}

	///开启闹钟计时_
	img_clock->setVisible(true);
	int_time_back = the_sec_outCard;
	String* str = String::createWithFormat("%d", the_sec_outCard);
	txt_time_back->setString(str->getCString());
	this->schedule(schedule_selector(GameFightingLayer::updateTimeBack), 1.0f);

	if (int_autos >= 2 && int_cur_player == chair_me)///自动托管状态_
	{
		btnTipCardsCallBack((Ref*)btn_tip_cards, ui::TouchEventType::TOUCH_EVENT_ENDED);
		if (out_enable)
		{
			
			btnOutCardsCallBack((Ref*)btn_out_cards, ui::TouchEventType::TOUCH_EVENT_ENDED);
		}
		else
		{
			btnPassCallBack((Ref*)btn_pass, ui::TouchEventType::TOUCH_EVENT_ENDED);
		}
		
	}

}

///设置用户放弃出牌_
void GameFightingLayer::setFightPassCards(void* data, int dataSize)
{
	///解析数据_
	CMD_S_PassCard* pStatusStart = (CMD_S_PassCard*)data;
	int_cur_player = pStatusStart->wCurrentUser;
	int int_pass_player = pStatusStart->wPassCardUser;
	b_turnOver = pStatusStart->cbTurnOver;

	///弹出谈话框_
	if (int_pass_player == chair_me)
	{
		setTalkBox(talk_pass, chair_me);
	}
	else if (int_pass_player == chair_left)
	{
		setTalkBox(talk_pass, chair_left);
	}
	else if (int_pass_player == chair_right)
	{
		setTalkBox(talk_pass, chair_right);
	}

	///关闭上家倒计时闹钟_
	img_clock->setVisible(false);
	this->unschedule(schedule_selector(GameFightingLayer::updateTimeBack));		

	if (int_cur_player == chair_left)
	{
		img_clock->setPosition(Position_left);

		///出牌前清理_
		cleanAllTalkBox(chair_left);
		cleanAllPukers(chair_left);
	}
	else if (int_cur_player == chair_right)
	{
		img_clock->setPosition(Position_right);

		///出牌前清理_
		cleanAllTalkBox(chair_right);
		cleanAllPukers(chair_right);
	}
	else if (int_cur_player == chair_me)
	{
		img_clock->setPosition(Position_me);

		///弹出出牌按钮盒子_
		Panel_out_cards->setVisible(true);
		btn_pass->setEnabled(true);
		btn_pass->setBright(true);
		btn_tip_cards->setEnabled(true);
		btn_tip_cards->setBright(true);
		btn_out_cards->setEnabled(false);
		btn_out_cards->setBright(false);

		img_pass->loadTexture("GameMainScene/btn/gui-l-text-no-out.png");
		img_tip->loadTexture("GameMainScene/btn/gui-l-text-tips.png");
		img_out->loadTexture("GameMainScene/btn/gui-l-text-out-card-hui.png");

		///判断当前出牌类型_
		if (b_turnOver)
		{
			int_cur_logic = CT_ANY_IS_OK;
			int_pre_logicPoint = 0;
			int_card_cnts = 0;
			tip_landPoint = 0;
			btn_pass->setEnabled(false);
			btn_pass->setBright(false);
			btn_tip_cards->setEnabled(false);
			btn_tip_cards->setBright(false);

			img_pass->loadTexture("GameMainScene/btn/gui-l-text-no-out-hui.png");
			img_tip->loadTexture("GameMainScene/btn/gui-l-text-tips-hui.png");
			
		}
		else
		{
			int land_point[20];
			for (int i = 0; i < int_card_cnts; i++)
			{
				land_point[i] = convertLandPoint(out_point[i]);
			}
			int_cur_logic = logic_land->getLandLogic(land_point, int_card_cnts);
			int_pre_logicPoint = logic_land->getLogicPoint(int_cur_logic, land_point, int_card_cnts);
			tip_landPoint = int_pre_logicPoint;

		}

		///判断选择牌型是否为可出_,设置出牌按钮可用_
		if (logic_land->myChoicePukerIsOk(int_cur_logic, int_card_cnts, int_pre_logicPoint, my_puke, int_curPuker_cnts))
		{
			btn_out_cards->setEnabled(true);
			btn_out_cards->setBright(true);
			img_out->loadTexture("GameMainScene/btn/gui-l-text-out-card.png");
		}

		///出牌前清理_
		cleanAllTalkBox(chair_me);
		cleanAllPukers(chair_me);
	}

	///开启闹钟计时_
	img_clock->setVisible(true);
	int_time_back = the_sec_outCard;
	String* str = String::createWithFormat("%d", the_sec_outCard);
	txt_time_back->setString(str->getCString());
	this->schedule(schedule_selector(GameFightingLayer::updateTimeBack), 1.0f);

	if (int_autos >= 2 && int_cur_player == chair_me)///自动托管状态_
	{
		if (!b_turnOver)
		{
			btnTipCardsCallBack((Ref*)btn_tip_cards, ui::TouchEventType::TOUCH_EVENT_ENDED);
			if (out_enable)
			{
				btnOutCardsCallBack((Ref*)btn_out_cards, ui::TouchEventType::TOUCH_EVENT_ENDED);
			}
			else
			{
				btnPassCallBack((Ref*)btn_pass, ui::TouchEventType::TOUCH_EVENT_ENDED);
			}
		}
		else
		{
			///整理所有扑克_
			for (int i = 0; i < int_curPuker_cnts; i++)
			{
				if (my_puke[i]->isUp)
				{
					my_puke[i]->runAction(MoveBy::create(0, Vec2(0, -50)));
					my_puke[i]->isUp = false;
				}
			}

			IClientKernel * kernel = IClientKernel::get();

			///把最小一张扑克发送出去_
			my_puke[int_curPuker_cnts - 1]->isUp = true;

			CMD_C_OutCard outCard;
			logic_land->chooseMyPukers(my_puke, &outCard, int_curPuker_cnts, puke);
			kernel->SendSocketData(SUB_C_OUT_CARD, &outCard, sizeof(outCard)-sizeof(outCard.cbCardData) + outCard.cbCardCount*sizeof(byte));

			///处理剩余扑克_
			int_curPuker_cnts = int_curPuker_cnts - outCard.cbCardCount;
			resetThePukers(int_curPuker_cnts);
			tip_landPoint = 0;
			tip_cnts = 0;
		}
		
	}

}



///设置游戏结束_
void GameFightingLayer::setFightOver(void* data, int dataSize,  FrameLayer_doudizhu* frame_layer)
{
	///庄家胜利还是失败_
	bool b_win = false;

	this->unschedule(schedule_selector(GameFightingLayer::updateTimeBack));

	
	f_layer = frame_layer;

	///解析数据_
	CMD_S_GameConclude* pStatus = (CMD_S_GameConclude*)data;

	

	b_flee = pStatus->bFlee;///逃跑_
	int int_num_spring = 0;///春天_
	if (pStatus->bChunTian || pStatus->bFanChunTian)
	{
		int_num_spring = 1;
	}
	int int_num_bomb = pStatus->cbBombCount;///炸弹_
	int int_callScore = pStatus->cbBankerint64;///叫分数目_
	int int_maxScore = pStatus->lRestrictint64;///每局封顶_
	int int_singleScore = pStatus->lCellint64;///单元积分_
	int int_ScorePlayer[3];///游戏积分_
	int int_pukeNumPlayer[3];///扑克数目_

	

	///结束状态倒计时_
	txt_time_back->setString("30");
	img_clock->setVisible(true);
	int_time_back = 30;
	
	img_clock->setPosition(Vec2(Position_me));
	this->schedule(schedule_selector(GameFightingLayer::updateTimeBack), 1.0f);

	///展示游戏结束界面_
	Panel_end->setVisible(true);
	setTouchListener(false);

	///移除托管界面_
	setAutoPuker(false);

	///隐藏托管按钮_
	btn_autoPuker->setVisible(false);
	f_layer->img_top_score->setVisible(false);

	img_talk_left->setVisible(false);
	img_talk_me->setVisible(false);
	img_talk_right->setVisible(false);
	///获取剩余扑克信息_
	for (int i = 0; i < 3; i++)
	{
		int_ScorePlayer[i] = (pStatus->lGameint64)[i];
		int_pukeNumPlayer[i] = (pStatus->cbCardCount)[i];
		if (!b_flee)
		{
			if (int_pukeNumPlayer[int_banker_player] == 0)
			{
				b_win = true;
			}
			else
			{
				b_win = false;
			}
		}
	}

	///调用游戏结束人物表情_
	/*if (b_win)
	{
	if (int_banker_player == chair_me)
	{
	frame_layer->onAction(4, frame_layer->int_faceId_me, frame_layer->spt_me);
	frame_layer->onAction(3, frame_layer->int_faceId_left, frame_layer->spt_left);
	frame_layer->onAction(3, frame_layer->int_faceId_right, frame_layer->spt_right);
	}
	else if (int_banker_player == chair_left)
	{
	frame_layer->onAction(3, frame_layer->int_faceId_me, frame_layer->spt_me);
	frame_layer->onAction(4, frame_layer->int_faceId_left, frame_layer->spt_left);
	frame_layer->onAction(3, frame_layer->int_faceId_right, frame_layer->spt_right);
	}
	else
	{
	frame_layer->onAction(3, frame_layer->int_faceId_me, frame_layer->spt_me);
	frame_layer->onAction(3, frame_layer->int_faceId_left, frame_layer->spt_left);
	frame_layer->onAction(4, frame_layer->int_faceId_right, frame_layer->spt_right);
	}
	}
	else
	{
	if (int_banker_player == chair_me)
	{
	frame_layer->onAction(3, frame_layer->int_faceId_me, frame_layer->spt_me);
	frame_layer->onAction(4, frame_layer->int_faceId_left, frame_layer->spt_left);
	frame_layer->onAction(4, frame_layer->int_faceId_right, frame_layer->spt_right);
	}
	else if (int_banker_player == chair_left)
	{
	frame_layer->onAction(4, frame_layer->int_faceId_me, frame_layer->spt_me);
	frame_layer->onAction(3, frame_layer->int_faceId_left, frame_layer->spt_left);
	frame_layer->onAction(4, frame_layer->int_faceId_right, frame_layer->spt_right);
	}
	else
	{
	frame_layer->onAction(4, frame_layer->int_faceId_me, frame_layer->spt_me);
	frame_layer->onAction(4, frame_layer->int_faceId_left, frame_layer->spt_left);
	frame_layer->onAction(3, frame_layer->int_faceId_right, frame_layer->spt_right);
	}
	}
	*/



	///展示玩家剩余扑克_
	int x = 0;
	for (int i = 0; i < 3; i++)
	{
		if (i == chair_me)
		{
			x = x + int_pukeNumPlayer[i];
			continue;
		}
		
		else 
		{
		
			
			for (int j = 0; j < int_pukeNumPlayer[i]; j++, x++)//
			{
				out_point[j] = convertTheMyPukerValue(pStatus->cbHandCardData[x]);
			}
			int_card_cnts = int_pukeNumPlayer[i];

					displayOutCards(i, true);
			}
		
	}

	String* str_holder;
	if (!b_flee)///正常游戏结束_
	{
		///春天_
		str_holder = String::createWithFormat("%d", int_num_spring);
		txt_num_spring->setString(str_holder->getCString());

		///炸弹_
		str_holder = String::createWithFormat("%d", int_num_bomb);
		txt_num_bomb->setString(str_holder->getCString());

		///胜败_
		ImageView* img;
		if (b_win)
		{
			if (int_banker_player == chair_me)
			{
				img = (ImageView*)img_win_land->clone();
				///胜利音效_
				SoundManager::shared()->stopMusic();
				SoundManager::shared()->playMusic("OVER_WIN_LAND", false);
				setSoundWlakPuker(talk_win, sex_is_man_me);
			}
			else
			{
				img = (ImageView*)img_fail_farmer->clone();
				///失败音效_
				SoundManager::shared()->stopMusic();
				SoundManager::shared()->playMusic("OVER_LOSE_LAND", false);
			}
		}
		else
		{
			if (int_banker_player == chair_me)
			{
				img = (ImageView*)img_fail_land->clone();
				///失败音效_
				SoundManager::shared()->stopMusic();
				SoundManager::shared()->playMusic("OVER_LOSE_LAND", false);
			}
			else
			{
				img = (ImageView*)img_win_farmer->clone();
				///胜利音效_
				SoundManager::shared()->stopMusic();
				SoundManager::shared()->playMusic("OVER_WIN_LAND", false);
				setSoundWlakPuker(talk_win, sex_is_man_me);
			}
		}
		img->setPosition(Vec2(710, 570));
		Panel_end->addChild(img);

		///计算得分_
		TextAtlas* txt_score_left;
		TextAtlas* txt_score_right;
		TextAtlas* txt_score_me;

		///左边玩家_
		if (int_ScorePlayer[chair_left] > 0)
		{
			txt_score_left = (TextAtlas*)aTxt_win->clone();
			str_holder = String::createWithFormat("+%d", int_ScorePlayer[chair_left]);
		}
		else
		{
			txt_score_left = (TextAtlas*)aTxt_fail->clone();
			str_holder = String::createWithFormat("%d", int_ScorePlayer[chair_left]);
		}
		txt_score_left->setPosition(Vec2(377, 440));
		txt_score_left->setString(str_holder->getCString());
		Panel_end->addChild(txt_score_left);

		///右边玩家_
		if (int_ScorePlayer[chair_right] > 0)
		{
			txt_score_right = (TextAtlas*)aTxt_win->clone();
			str_holder = String::createWithFormat("+%d", int_ScorePlayer[chair_right]);
		}
		else
		{
			txt_score_right = (TextAtlas*)aTxt_fail->clone();
			str_holder = String::createWithFormat("%d", int_ScorePlayer[chair_right]);
		}
		txt_score_right->setPosition(Vec2(1002, 440));
		txt_score_right->setString(str_holder->getCString());
		Panel_end->addChild(txt_score_right);

		///自己玩家_
		if (int_ScorePlayer[chair_me] > 0)
		{
			txt_score_me = (TextAtlas*)aTxt_win->clone();
			str_holder = String::createWithFormat("+%d", int_ScorePlayer[chair_me]);
		}
		else
		{
			txt_score_me = (TextAtlas*)aTxt_fail->clone();
			str_holder = String::createWithFormat("%d", int_ScorePlayer[chair_me]);
		}
		txt_score_me->setPosition(Vec2(710, 340));
		txt_score_me->setString(str_holder->getCString());
		Panel_end->addChild(txt_score_me);
	}
	else///游戏逃跑结束_
	{
		///清理桌面_
		cleanAllTalkBox();
		cleanAllPukers();

		if (puke_move0 && puke_move1 && puke_move2)
		{
			puke_move0->removeFromParentAndCleanup(true);
			puke_move0 = nullptr;
			puke_move1->removeFromParentAndCleanup(true);
			puke_move1 = nullptr;
			puke_move2->removeFromParentAndCleanup(true);
			puke_move2 = nullptr;
		}

//		Panel_out_cards->setVisible(false);
		
		///计算得分_
		TextAtlas* txt_score_left;
		TextAtlas* txt_score_right;
		TextAtlas* txt_score_me;

		///左边玩家_
		if (int_ScorePlayer[chair_left] > 0)
		{
			txt_score_left = (TextAtlas*)aTxt_win->clone();
			str_holder = String::createWithFormat("+%d", int_ScorePlayer[chair_left]);
		}
		else
		{
			txt_score_left = (TextAtlas*)aTxt_fail->clone();
			str_holder = String::createWithFormat("%d", int_ScorePlayer[chair_left]);
		}
		txt_score_left->setPosition(Vec2(377, 440));
		txt_score_left->setString(str_holder->getCString());
		Panel_end->addChild(txt_score_left);

		///右边玩家_
		if (int_ScorePlayer[chair_right] > 0)
		{
			txt_score_right = (TextAtlas*)aTxt_win->clone();
			str_holder = String::createWithFormat("+%d", int_ScorePlayer[chair_right]);
		}
		else
		{
			txt_score_right = (TextAtlas*)aTxt_fail->clone();
			str_holder = String::createWithFormat("%d", int_ScorePlayer[chair_right]);
		}
		txt_score_right->setPosition(Vec2(1002, 440));
		txt_score_right->setString(str_holder->getCString());
		Panel_end->addChild(txt_score_right);

		///自己玩家_
		if (int_ScorePlayer[chair_me] > 0)
		{
			txt_score_me = (TextAtlas*)aTxt_win->clone();
			str_holder = String::createWithFormat("+%d", int_ScorePlayer[chair_me]);
		}
		else
		{
			txt_score_me = (TextAtlas*)aTxt_fail->clone();
			str_holder = String::createWithFormat("%d", int_ScorePlayer[chair_me]);
		}
		txt_score_me->setPosition(Vec2(710, 340));
		txt_score_me->setString(str_holder->getCString());
		Panel_end->addChild(txt_score_me);
	}

	IClientKernel * kernel = IClientKernel::get();

	///显示昵称_
	std::string my_nick = kernel->GetMeUserItem()->GetNickName();
	txt_mynick->setString(my_nick);

	///显示金币数量_
	my_gold = kernel->GetMeUserItem()->GetUserScore() + int_ScorePlayer[chair_me];
	str_holder = String::createWithFormat("%lld", my_gold);

	alt_mygold->setString(str_holder->getCString());


	//设置fighting的倍数和main中的钱数
	alt_beishu_num->setString(frame_layer->alt_beishu_num->getString());
	frame_layer->alt_mygold->setString(alt_mygold->getString());
	
}

///将服务器端的扑克面值转换成本地想要的值_
byte GameFightingLayer::convertTheMyPukerValue(byte puke)
{
	if (puke < 14)
	{
		return puke - 1;
	}
	else if (puke < 30)
	{
		return puke - 4;
	}
	else if (puke < 46)
	{
		return puke - 7;
	}
	else if (puke < 62)
	{
		return puke - 10;
	}
	else if (puke == 78)
	{
		return Ox4E;
	}
	else if (puke == 79)
	{
		return Ox4F;
	}
}