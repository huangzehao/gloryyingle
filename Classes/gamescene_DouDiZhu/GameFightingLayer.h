﻿#ifndef GAMEFIGHTINGLAYER_LAND_H
#define GAMEFIGHTINGLAYER_LAND_H

#include "cocos2d.h"

#include "SimpleAudioEngine.h"

USING_NS_CC;
using namespace std;


enum Talk_witch
{
	talk_none = 255,
	talk_pass = 0,
	talk_one = 1,
	talk_two = 2,
	talk_three = 3,
	talk_boom, talk_da, talk_double, talk_four_two, talk_guan, talk_line_double, talk_line_single, talk_line_three,
	talk_plane, talk_rocket, talk_single, talk_three_one, talk_three_two, talk_win
};

class LogicLand;
class FrameLayer_doudizhu;

class GameFightingLayer : public Layer
{
	GameFightingLayer();
	~GameFightingLayer();
public:
	static GameFightingLayer* create(int headCard, int outCard)
	{
		GameFightingLayer *pRet = new GameFightingLayer();
		if (pRet && pRet->init(headCard, outCard))
		{
			pRet->autorelease();
			return pRet;
		}
		else
		{
			delete pRet;
			pRet = NULL;
			return NULL;
		}
	}
	bool init(int headCard, int outCard);

public:
	///创建战斗界面_
	void createFightingPanel();
	///设置游戏战斗开始_
	void setGameFightingStart(void* data, int dataSize);
	///设置游戏进行状态_
	void setGameFightingPlay(void* data, int dataSize);
	///发牌动画_
	void dealPukersAction();
	///设置庄家信息_
	void setFightBankerInfo(void* data, int dataSize, FrameLayer_doudizhu* frame_layer);
	///设置用户出牌_
	void setFightOutcards(void* data, int dataSize);
	///设置用户放弃出牌_
	void setFightPassCards(void* data, int dataSize);
	///设置游戏结束_
	void setFightOver(void* data, int dataSize,  FrameLayer_doudizhu* frame_layer);

public:
	///将服务器端的扑克面值转换成本地想要的值_
	byte convertTheMyPukerValue(byte puke);
	///转换成地主点数_
	int convertLandPoint(int point);
	///调用谈话框_
	void setTalkBox(int talk_witch, int chair_id);
	///调用过牌声音_
	void setSoundWlakPuker(int talk_witch, bool cur_sex_is_man);
	///获取逻辑对应的声音选项_
	int getTalkWhichByLogic(int cur_logic);
	///全清谈话框_
	void cleanAllTalkBox(int order = 100);
	///全清桌面扑克_
	void cleanAllPukers(int order = 100);

	///设置成我的本地位置_
	void setMyChairPosition();
	///设置所有玩家性别_
	void setAllPlayerSex();
	///调整扑克顺序_
	void resetThePukers(int cnts);
	///设置监听事件_
	void setTouchListener(bool turnOn);
	///玩家出牌显示_
	void displayOutCards(int pre_player, bool is_over = false);
	///设置托管扑克_
	void setAutoPuker(bool isOn);

public:
	///倒计时_
	void updateTimeBack(float dt);
	///延时处理发牌动画_
	void update_dealPukers(float dt);
	///翻牌动画_
	void update_turnPukers(float dt);
	///延时隐藏过牌动画_
	void update_hideAction(float dt);
	///炸弹动画_
	void update_boomAction(float dt);

public:
	///调用单连动画_
	void callLineSingleAction();
	///调用对连动画_
	void callLineDoubleAction();
	///调用三顺动画_
	void callLineThreeAction();
	///调用飞机动画_
	void callPlaneAction();
	///调用火箭动画_
	void callRocketAction();
	///调用炸弹动画_
	void callBoomAction();

	ui::TextAtlas* alt_mygold;///玩家本人金币数量label_
	
private:
	bool onTouchBegan(Touch* touch, Event* event);
	void onTouchMoved(Touch* touch, Event* event);
	void onTouchEnded(Touch* touch, Event* event);
	void btnPassCallBack(cocos2d::Ref * ref, ui::TouchEventType eType);
	void btnTipCardsCallBack(cocos2d::Ref * ref, ui::TouchEventType eType);
	void btnOutCardsCallBack(cocos2d::Ref * ref, ui::TouchEventType eType);
	void btnContinueCallBack(cocos2d::Ref * ref, ui::TouchEventType eType);
	void btnExitCallBack(cocos2d::Ref * ref, ui::TouchEventType eType);
	void btnDropAutoPukerCallBack(cocos2d::Ref * ref, ui::TouchEventType eType);
	void btnAutoPukerCallBack(cocos2d::Ref * ref, ui::TouchEventType eType);
	void showScore(cocos2d::Node *target, void* data);


private:
	ui::Widget* AutoPukers;///托管界面_
	ui::Widget* Panel_Fighting;///josn_文件子根节点
	ui::ImageView* img_talk_left;///左边谈话框_
	ui::ImageView* img_talk_right;///右边谈话框_
	ui::ImageView* img_talk_me;///本人谈话框_

	ui::Widget* img_clock;///闹钟_
	ui::TextAtlas* txt_time_back;///倒计时_
	ui::Widget* Panel_out_cards;///出牌按钮盒子_
	ui::Button* btn_pass;///不出_
	ui::Button* btn_tip_cards;///提示_
	ui::Button* btn_out_cards;///出牌_
	ui::ImageView* img_pass;//不出的图片
	ui::ImageView* img_tip;//提示的图片
	ui::ImageView* img_out;//出牌的图片
	ui::TextAtlas* alt_beishu_num;///玩家本人金币数量label_

	ui::Widget* Panel_end;///游戏结束层_
	ui::Button* btn_continue;///继续下一局按钮_
	ui::Button* btn_exit;///退出游戏按钮_
	ui::Text* txt_num_bomb;///炸弹个数_
	ui::Text* txt_num_spring;///春天_
	ui::ImageView* img_win_land;///地主胜利_
	ui::ImageView* img_win_farmer;///农民胜利_
	ui::ImageView* img_fail_land;///地主失败_
	ui::ImageView* img_fail_farmer;///农民失败_
	ui::TextAtlas* aTxt_fail;///失败分数字体_
	ui::TextAtlas* aTxt_win;///胜利分数字体_
	ui::ImageView* img_mydata; ///游戏场景底部玩家信息_
	ui::Text* txt_mynick;///玩家本人昵称label_
	
	

	ui::Button* btn_drop_autoPuker;///取消托管_
	ui::Button* btn_autoPuker;///托管按钮_
	ui::Widget* Panel_action;///动画集成盒子_
	ui::ImageView* img_plane;///飞机_
	ui::ImageView* img_rocket;///火箭_
	ui::ImageView* img_line_boat;///小船_
	ui::ImageView* img_line_reboat;///反小船_
	ui::ImageView* img_line_water;///水_
	ui::ImageView* img_boom;///炸弹_
	ui::ImageView* img_boom_0;///炸弹效果0_
	ui::ImageView* img_boom_1;///炸弹效果1_
	ui::ImageView* img_boom_2;///炸弹效果2_

	int the_sec_headOut;///首出时间_
	int the_sec_outCard;///出牌时间_
	bool b_turnOver;///是否一轮结束_

	Puker* puke_back;///扑克背面_
	Puker* puke_move0;///扑克背面_
	Puker* puke_move1;///扑克背面_
	Puker* puke_move2;///扑克背面_
	int int_banker_player;///庄家玩家_
	int banker_puker[3];///庄家扑克点数_
	int dt_dealpuker;///发牌使用的时间参数_
	int int_time_back;///倒计时专用时间参数_
	int int_time_turn;///翻牌动画时间参数_

	int puke[20];///获取到的扑克点数_me
	int out_point[20];///出牌扑克点数_
	Puker* my_puke[20];///我的扑克盒子_
	Puker* out_puke_me[20];///出牌扑克盒子_me
	Puker* out_puke_left[20];///出牌扑克盒子_left
	Puker* out_puke_right[20];///出牌扑克盒子_right
	int my_puke_pointX;///第一张牌的位置_
	int cur_outPuke[20];///正在出牌的盒子_

	int chair_me;///我的位置_
	int chair_left;///左边玩家_
	int chair_right;///右边玩家_

	///玩家性别_
	bool sex_is_man_me;
	bool sex_is_man_left;
	bool sex_is_man_right;

	int int_cur_player;///当前玩家_
	int int_curPuker_cnts;///当前扑克剩余数量_me
	int int_pukers_left;
	int int_pukers_right;
	bool my_puke_touchMoved[20];///记录我的扑克是否被滑动_
	Sprite* spt_hui[20];///透明灰_
	int int_hui;///透明灰计数器_
	int move_puke[20];///move选中扑克列表_
	Point sLocalPos;///触摸的开始坐标_
	int int_move_s;///滑动起点_
	int int_move_left;///滑动左端_
	int int_move_right;///滑动右端_
	bool m_isMoved;///是否为滑动选牌_

	int int_cur_logic;///当前出牌的逻辑类型_
	int int_card_cnts;///当前出牌张数_
	int int_pre_logicPoint;///上家扑克的逻辑点数_
	bool b_twoKings;///是否大小王_
	bool b_kingsD;
	bool b_flee;///逃跑标识_
	int tip_landPoint;///提示最大点数_
	int tip_cnts;///已提示的次数_
	int int_autos;///自动出牌次数_
	int out_enable;///托管时出牌标识_

	bool is_fighting_fierce;///是否进入战斗高潮_
	Node* node_fighting;///战斗界面节点_
	int ac_time;///过牌动画结束时间_
	int boom_cnts;///炸弹帧数_

	EventListenerTouchOneByOne* touchListener;///触摸监听事件_
	LogicLand * logic_land;///斗地主逻辑_
	FrameLayer_doudizhu* f_layer;

	long long my_gold;//我的金币
	
};

#endif