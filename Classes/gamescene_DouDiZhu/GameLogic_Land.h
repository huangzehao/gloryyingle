#ifndef _GameLogic_Land_H_
#define _GameLogic_Land_H_

#include "cocos2d.h"
#include "Puker.h"
#include "cmd_doudizhu.h"

USING_NS_CC;
using namespace doudizhu;

class LogicLand
{
private:
	static LogicLand* p_logic;
	static bool b_loop;///判断提示出牌的时候是否为此逻辑要不起_
public:
	static LogicLand* getInstance();
	static void setB_Loop();///循环标识_
public:
	///将我选择的扑克写入结构体_
	void chooseMyPukers(Puker* puke_box[20], CMD_C_OutCard* outCard, int cur_pukers, int puke[20]);
	///判断我选择的扑克是否符合逻辑_
	bool myChoicePukerIsOk(int require_logic, int requir_cnts, int pre_maxPoint, Puker* puke_box[20], int myPuker_cnts);
	///调整扑克顺序_
	void resetThePoint(int land_point[20], int cnts);
	///获取斗地主牌型逻辑_
	int getLandLogic(int land_point[20], int cnts);
	///转换成服务端扑克点数_
	byte convertServerPoint(int my_point);
	///转换成地主点数_
	int convertLandPoint(int point);
	///获取逻辑的点数_
	int getLogicPoint(int require_logic, int land_point[20], int cnts);
	///只能筛选顺子或双顺牌型扑克_
	void autoSeclectPukers(Puker* puke_box[20], int cnts);
	///提示出牌按钮处理_
	int tipOutCards(Puker* puke_box[20], int cnts, int land_logic, int requir_cnts, int int_cur_point);


public:
	///智能筛选双顺_
	bool isExistLineDouble(Puker* puke_box[20], int cnts);
	///智能筛选单顺_
	bool isExistLineSingle(Puker* puke_box[20], int cnts);

	///判断是否是三带一_
	int getThree_One(int land_point[20]);
	///判断是否为单连_
	bool getSingleLine(int land_point[20], int cnts);
	///判断是否为对连_
	bool getDoubleLine(int land_point[20], int cnts);
	///判断是否为三连_
	bool getThreeLine(int land_point[20], int cnts);
	///判断是否为飞机带单牌_
	bool getSingleFly(int land_point[20], int cnts);
	///判断是否为飞机带对子_
	bool getDoubleFly(int land_point[20], int cnts);

	///判断五张牌_
	int getFivePuker(int land_point[20]);
	///判断六张牌_
	int getSixPuker(int land_point[20]);
	///判断八张牌_
	int getEightPuker(int land_point[20]);

};

#endif