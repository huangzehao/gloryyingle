#include "FrameLayer_doudizhu.h"
#include "Tools/tools/StringData.h"
#include "ClientKereneSink_doudizhu.h"
#include "Kernel/kernel/game/IClientKernel.h"
#include "Tools/Manager/SpriteHelper.h"
#include "GameScene_doudizhu.h"
#include "Kernel/kernel/server/IServerItemSink.h"
#include "Tools/Manager/SoundManager.h"
#include "common/GameSetLayer.h"
#include "Tools/tools/SimpleTools.h"
#include "Tools/Dialog/NewDialog.h"
#include "Tools/tools/Convert.h"
#include "Tools/ViewHeader.h"
#include "ui/UIWidget.h"

#include "cmd_doudizhu.h"
#include "GameFightingLayer.h"
#include "Puker.h"

using namespace doudizhu;
USING_NS_CC_EXT;
using namespace ui;

FrameLayer_doudizhu::FrameLayer_doudizhu()
{
	the_bar_poped = false;
	exit_is_tips = false;
	the_sec_start = 0;
	the_sec_call = 0;
	time_cnts = 0;

	animation = nullptr;
	beishu = 0;

}

FrameLayer_doudizhu::~FrameLayer_doudizhu()
{
	
}

bool FrameLayer_doudizhu::init()
{
	if (!Layer::init())
	{
		return false;
	}

	///创建游戏主框架_
	createMainPanel();
	///设置游戏主界面布局_
	placeTheGameSceneLayout();

	///弹出游戏场景底部和顶部信息盒子_
	the_bar_poped = false;


	return true;
}

///创建游戏主框架_
void FrameLayer_doudizhu::createMainPanel()
{
	Widget* root = cocostudio::GUIReader::getInstance()->widgetFromJsonFile("doudizhuGameScene/GameMainScene.json");
	addChild(root);

	Panel_Main = dynamic_cast<Widget *>(root->getChildByName("Panel_Main"));

	img_game_table = dynamic_cast<ImageView*>(Panel_Main->getChildByName("Image_143"));
	img_game_table->setVisible(true);
	///游戏退出按钮_
	Button * btn_close = dynamic_cast<Button *>(Panel_Main->getChildByName("btn_close"));
	btn_close->addTouchEventListener(this, SEL_TouchEvent(&FrameLayer_doudizhu::btnCloseGameCallBack));

	///开始按钮_
	btn_start = dynamic_cast<Button*>(Panel_Main->getChildByName("btn_start"));
	btn_start->addTouchEventListener(this, SEL_TouchEvent(&FrameLayer_doudizhu::btnStartGameCallBack));

	
	///游戏底部信息条_
	auto img_mydata2 = dynamic_cast<Widget*>(Panel_Main->getChildByName("img_mydata"));
	img_mydata = img_mydata2->clone();
	this->addChild(img_mydata, 1002);
	img_mydata2->removeFromParentAndCleanup(true);

	///玩家本人昵称label_
	txt_mynick = dynamic_cast<Text*>(img_mydata->getChildByName("panl_name")->getChildByName("txt_mynick"));
	///玩家本人金币数量label_
	alt_mygold = dynamic_cast<TextAtlas*>(img_mydata->getChildByName("panl_money")->getChildByName("alt_mygold"));
	///显示倍数_
	alt_beishu_num = dynamic_cast<TextAtlas*>(img_mydata->getChildByName("img_beishubg")->getChildByName("alt_beishu_num"));

		
	///玩家1剩余扑克数量_
	txt_nums_player1 = dynamic_cast<TextAtlas*>(Panel_Main->getChildByName("img_cards_count_box1")->getChildByName("txt_nums_player1"));
	///玩家2剩余扑克数量_
	txt_nums_player2 = dynamic_cast<TextAtlas*>(Panel_Main->getChildByName("img_cards_count_box2")->getChildByName("txt_nums_player2"));
	
	
	///玩家1剩余扑克数量盒子_
	img_cards_count_box1 = dynamic_cast<ImageView*>(Panel_Main->getChildByName("img_cards_count_box1"));
	///玩家2剩余扑克数量盒子_
	img_cards_count_box2 = dynamic_cast<ImageView*>(Panel_Main->getChildByName("img_cards_count_box2"));
	///闹钟_
	img_clock = dynamic_cast<ImageView*>(Panel_Main->getChildByName("img_clock"));
	///空闲状态时间倒计时_
	txt_time_back = dynamic_cast<TextAtlas*>(img_clock->getChildByName("txt_time_back"));


	auto img_top_score2 = dynamic_cast<ImageView*>(Panel_Main->getChildByName("img_top_score"));
	img_top_score = img_top_score2->clone();
	this->addChild(img_top_score,3);
	img_top_score2->removeAllChildrenWithCleanup(true);
//	img_top_score->setVisible(true);

	///本人头像背景框_
	auto img_head_bg_me2 = dynamic_cast<Widget*>(Panel_Main->getChildByName("img_head_bg_me"));
	/// 本人头像
	img_head_bg_me = img_head_bg_me2->clone();
	img_head_me = dynamic_cast<ImageView*>(img_head_bg_me->getChildByName("img_person"));
	this->addChild(img_head_bg_me, 1001);
	img_head_me->setPosition(Vec2(-92,-180));
	img_head_bg_me2->removeAllChildrenWithCleanup(true);

	///玩家1头像背景框_
	img_head_bg_player1 = dynamic_cast<ImageView*>(Panel_Main->getChildByName("img_head_bg_player1"));
	/// 玩家1头像
	img_head_player1 = dynamic_cast<ImageView*>(img_head_bg_player1->getChildByName("img_person"));
	///玩家1头像背景框_
	img_head_bg_player2 = dynamic_cast<ImageView*>(Panel_Main->getChildByName("img_head_bg_player2"));
	/// 玩家2头像
	img_head_player2 = dynamic_cast<ImageView*>(img_head_bg_player2->getChildByName("img_person"));

	///叫分按钮盒子_
	Panel_call = dynamic_cast<Widget*>(Panel_Main->getChildByName("Panel_call"));
	Panel_call->setPosition(Vec2(-145, -50));
	///按钮_不叫_
	btn_call_none = dynamic_cast<Button*>(Panel_Main->getChildByName("Panel_call")->getChildByName("btn_call_none"));
	btn_call_none->addTouchEventListener(this, SEL_TouchEvent(&FrameLayer_doudizhu::btnCallNoneCallBack));
	img_sc_none = dynamic_cast<ImageView*>(btn_call_none->getChildByName("img_sc_none"));

	///按钮_1分_
	btn_call_one = dynamic_cast<Button*>(Panel_Main->getChildByName("Panel_call")->getChildByName("btn_call_one"));
	btn_call_one->addTouchEventListener(this, SEL_TouchEvent(&FrameLayer_doudizhu::btnCallOneCallBack));
	img_sc_one = dynamic_cast<ImageView*>(btn_call_one->getChildByName("img_sc_one"));

	///按钮_2 分_
	btn_call_two = dynamic_cast<Button*>(Panel_Main->getChildByName("Panel_call")->getChildByName("btn_call_two"));
	btn_call_two->addTouchEventListener(this, SEL_TouchEvent(&FrameLayer_doudizhu::btnCallTwoCallBack));
	img_sc_two = dynamic_cast<ImageView*>(btn_call_two->getChildByName("img_sc_two"));

	///按钮_3 分_
	btn_call_three = dynamic_cast<Button*>(Panel_Main->getChildByName("Panel_call")->getChildByName("btn_call_three"));
	btn_call_three->addTouchEventListener(this, SEL_TouchEvent(&FrameLayer_doudizhu::btnCallThreeCallBack));
	img_sc_three = dynamic_cast<ImageView*>(btn_call_three->getChildByName("img_sc_three"));

	///准备图标_ me
	img_prepare_me = dynamic_cast<ImageView *>(Panel_Main->getChildByName("img_prepare_me"));
	///准备图标_ player1
	img_prepare_player1 = dynamic_cast<ImageView *>(Panel_Main->getChildByName("img_prepare_player1"));
	///准备图标_ player2
	img_prepare_player2 = dynamic_cast<ImageView *>(Panel_Main->getChildByName("img_prepare_player2"));

	///左边玩家姓名_
	txt_name_left =  dynamic_cast<Text*>(Panel_Main->getChildByName("img_head_bg_player1")->getChildByName("txt_name_left"));
	///右边玩家姓名_
	txt_name_right = dynamic_cast<Text*>(Panel_Main->getChildByName("img_head_bg_player2")->getChildByName("txt_name_right"));

	rightIsHave = false;
	leftIsHave = false;


	Director::getInstance()->getTextureCache()->addImage("doudizhuGameScene/GameMainScene/layout/game_role01.png");

	Director::getInstance()->getTextureCache()->addImage("doudizhuGameScene/GameMainScene/layout/game_role11.png");

	Director::getInstance()->getTextureCache()->addImage("doudizhuGameScene/GameMainScene/layout/game_role21.png");
}



///清理其他玩家离开痕迹_
void FrameLayer_doudizhu::cleanOtherUserPrepare(int chair_id)
{
	if (chair_id == chair_left)
	{
		img_prepare_player1->setVisible(false);
		img_cards_count_box1->setVisible(false);
		img_head_player1->loadTexture("GameMainScene/layout/game_role22.png");
		img_head_bg_player1->setVisible(false);
// 		spt_left->removeFromParentAndCleanup(true);
// 		spt_left = nullptr;

		txt_name_left->setString("");
	}
	else if (chair_id == chair_right)
	{
		img_prepare_player2->setVisible(false);
		img_cards_count_box2->setVisible(false);
		img_head_player2->loadTexture("GameMainScene/layout/game_role12.png");
		img_head_bg_player2->setVisible(false);
// 		spt_right->removeFromParentAndCleanup(true);
// 		spt_right = nullptr;
		txt_name_right->setString("");
	}

}

///设置玩家准备状态_
void FrameLayer_doudizhu::setOtherUserPrepare(IClientUserItem *pIClientUserItem, byte user_state)
{
	int faceId = SimpleTools::switchFaceID(pIClientUserItem->GetFaceID() % 8);
	int chair_id = pIClientUserItem->GetChairID();

	if (chair_id == chair_left)
	{
		int_faceId_left = faceId;
		
		img_cards_count_box1->setVisible(false);
		

		
		///昵称_
		txt_name_left->setString(pIClientUserItem->GetNickName());
		txt_name_left->setVisible(true);
		img_head_bg_player1->setVisible(true);
		if (user_state == US_READY){
			img_prepare_player1->setVisible(true);
			
		}
			
		else
			img_prepare_player1->setVisible(false);
	}
	else if (chair_id == chair_right)
	{
		int_faceId_right = faceId;
		img_cards_count_box2->setVisible(false);
		///昵称_
		txt_name_right->setString(pIClientUserItem->GetNickName());
		txt_name_right->setVisible(true);
		img_head_bg_player2->setVisible(true);
		if (user_state == US_READY){
			
	
			img_prepare_player2->setVisible(true);
		}
		else
			img_prepare_player2->setVisible(false);
	}
	else if (chair_id == chair_me)
	{
		int_faceId_me = faceId;
		if (user_state == US_READY){
			img_prepare_me->setVisible(true);
			
			
		}
		else
			img_prepare_me->setVisible(false);

	}
}

///设置游戏主界面布局_
void FrameLayer_doudizhu::placeTheGameSceneLayout()
{
	///隐藏叫分按钮盒子_
	Panel_call->setVisible(false);
	
	///设置成我的本地位置_
	setMyChairPosition();

	IClientKernel * kernel = IClientKernel::get();
	if (!kernel) return;
	int faceId = SimpleTools::switchFaceID(kernel->GetMeUserItem()->GetFaceID() % 8);
	int chair_id = kernel->GetMeUserItem()->GetChairID();

	
}

///返回游戏大厅按钮接收事件_
void FrameLayer_doudizhu::btnCloseGameCallBack(cocos2d::Ref * ref, ui::TouchEventType eType)
{
	if (eType == ui::TouchEventType::TOUCH_EVENT_BEGAN)
	{
		//SoundManager::shared()->playSound("ADD_SCORE");
	}
	else if (eType == ui::TouchEventType::TOUCH_EVENT_ENDED)
	{
		ui::Button * btn_now = Button::create();
		btn_now->setTag(DLG_MB_OK);
		IClientKernel * kernel = IClientKernel::get();

		if (!exit_is_tips)
		{
			(mTarget->*mCallback)(btn_now);
		}
		else
		{
			NewDialog::create(SSTRING("back_to_room_content"), NewDialog::AFFIRMANDCANCEL, [=]()
			{
				if (IClientKernel::get())
					IClientKernel::get()->Intermit(GameExitCode_Normal);
			});
		}

	}
}

///游戏开始按钮事件返回_
void FrameLayer_doudizhu::btnStartGameCallBack(cocos2d::Ref * ref, ui::TouchEventType eType)
{
	if (eType == ui::TouchEventType::TOUCH_EVENT_BEGAN)
	{
		///SoundManager::shared()->playSound("GAME_START_LAND");
	}
	else if (eType == ui::TouchEventType::TOUCH_EVENT_ENDED)
	{
		///关闭空闲状态倒计时_
		this->unschedule(schedule_selector(FrameLayer_doudizhu::updateFreeTimeBack));
		
		///隐藏开始按钮_
		btn_start->setVisible(false);

		///隐藏闹钟_
		img_clock->setVisible(false); 



		///向服务器请求准备开始游戏_
		IClientKernel * kernel = IClientKernel::get();
		kernel->SendUserReady(NULL, 0);

// 		img_head_player1->loadTexture("GameMainScene/layout/game_role22");
// 		img_head_player2->loadTexture("GameMainScene/layout/game_role12");
// 		img_head_me->loadTexture("GameMainScene/layout/game_role02");
	}
}

///叫分_不叫按钮事件返回_
void FrameLayer_doudizhu::btnCallNoneCallBack(cocos2d::Ref * ref, ui::TouchEventType eType)
{
	if (eType == ui::TouchEventType::TOUCH_EVENT_ENDED)
	{
		IClientKernel * kernel = IClientKernel::get();

		///关闭定时器_
		this->unschedule(schedule_selector(FrameLayer_doudizhu::updateCallTimeBack));
		///放弃叫分_
		CMD_C_Callint64 CallScore;
		CallScore.cbCallint64 = 0;
		kernel->SendSocketData(SUB_C_CALL_int64, &CallScore, sizeof(CallScore));

		///隐藏闹钟和叫分按钮_
		img_clock->setVisible(false);
		Panel_call->setVisible(false);
	}
}

///叫分_1分按钮事件返回_
void FrameLayer_doudizhu::btnCallOneCallBack(cocos2d::Ref * ref, ui::TouchEventType eType)
{
	if (eType == ui::TouchEventType::TOUCH_EVENT_ENDED)
	{
		IClientKernel * kernel = IClientKernel::get();

		///关闭定时器_
		this->unschedule(schedule_selector(FrameLayer_doudizhu::updateCallTimeBack));
		
		///叫1分_
		CMD_C_Callint64 CallScore;
		CallScore.cbCallint64 = 1;
		kernel->SendSocketData(SUB_C_CALL_int64, &CallScore, sizeof(CallScore));

		///隐藏闹钟和叫分按钮_
		img_clock->setVisible(false);
		Panel_call->setVisible(false);

	}
}

///叫分_2分按钮事件返回_
void FrameLayer_doudizhu::btnCallTwoCallBack(cocos2d::Ref * ref, ui::TouchEventType eType)
{
	if (eType == ui::TouchEventType::TOUCH_EVENT_ENDED)
	{
		IClientKernel * kernel = IClientKernel::get();

		///关闭定时器_
		this->unschedule(schedule_selector(FrameLayer_doudizhu::updateCallTimeBack));
		
		///叫2分_
		CMD_C_Callint64 CallScore;
		CallScore.cbCallint64 = 2;
		kernel->SendSocketData(SUB_C_CALL_int64, &CallScore, sizeof(CallScore));

		///隐藏闹钟和叫分按钮_
		img_clock->setVisible(false);
		Panel_call->setVisible(false);
	}
}

///叫分_3分按钮事件返回_
void FrameLayer_doudizhu::btnCallThreeCallBack(cocos2d::Ref * ref, ui::TouchEventType eType)
{
	if (eType == ui::TouchEventType::TOUCH_EVENT_ENDED)
	{

		IClientKernel * kernel = IClientKernel::get();

		///关闭定时器_
		this->unschedule(schedule_selector(FrameLayer_doudizhu::updateCallTimeBack));

		///叫3分_
		CMD_C_Callint64 CallScore;
		CallScore.cbCallint64 = 3;
		kernel->SendSocketData(SUB_C_CALL_int64, &CallScore, sizeof(CallScore));

		///隐藏闹钟和叫分按钮_
		img_clock->setVisible(false);
		Panel_call->setVisible(false);

	}
}


///游戏设置按钮事件接收_
void FrameLayer_doudizhu::btnSetGameCallBack(cocos2d::Ref * ref, ui::TouchEventType eType)
{
	if (eType == ui::TouchEventType::TOUCH_EVENT_BEGAN)
	{
		//SoundManager::shared()->playSound("ADD_SCORE");
	}
	else if (eType == ui::TouchEventType::TOUCH_EVENT_ENDED)
	{

	}
}

///框架函数_
void FrameLayer_doudizhu::setCloseDialogInfo(cocos2d::Ref* target, cocos2d::SEL_CallFuncN callfun, const std::string& title, const std::string& content)
{
	mTarget = target;
	mCallback = callfun;
	mTitle = title;
	mContent = content;
}

///从游戏主场景返回游戏大厅通道_
void FrameLayer_doudizhu::func_affirm_exit_call_back(cocos2d::Ref *pSender)
{
	Node* node = dynamic_cast<Node*>(pSender);
	auto kernel = IClientKernel::get();
	int clock_id = kernel->GetClockID();
	if (clock_id != 50)
	{
		NewDialog::create(SSTRING("back_to_room_content"), NewDialog::AFFIRMANDCANCEL, [=]()
		{
			(mTarget->*mCallback)(node);
		});
	}
	else
	{
		if (mCallback)
		{
			(mTarget->*mCallback)(node);
		}
	}

}

///设置成我的本地位置_
void FrameLayer_doudizhu::setMyChairPosition()
{
	IClientKernel * kernel = IClientKernel::get();
	if (!kernel) return;
	int int_holder = kernel->GetMeUserItem()->GetChairID();

	if (int_holder == 1)
	{
		chair_me = 1;
		chair_left = 0;
		chair_right = 2;
	}
	else if (int_holder == 0)
	{
		chair_me = 0;
		chair_left = 2;
		chair_right = 1;
	}
	else if (int_holder == 2)
	{
		chair_me = 2;
		chair_left = 1;
		chair_right = 0;
	}

}

///设置所有玩家性别_
void FrameLayer_doudizhu::setAllPlayerSex()
{
	IClientKernel * kernel = IClientKernel::get();

// 	sex_is_man_me = kernel->GetTableUserItem(chair_me)->GetGender() == GENDER_MANKIND;
// 	sex_is_man_left = kernel->GetTableUserItem(chair_left)->GetGender() == GENDER_MANKIND;
// 	sex_is_man_right = kernel->GetTableUserItem(chair_right)->GetGender() == GENDER_MANKIND;
	sex_is_man_me = 1;
	sex_is_man_left = 1;
	sex_is_man_right = 1;

}

///设置游戏空闲状态信息显示_
void FrameLayer_doudizhu::setGameSceneFree(void * data, int dataSize)
{
	IClientKernel * kernel = IClientKernel::get();
	String* str_holder = nullptr;

	///转换结构体类型_
	CMD_S_StatusFree* pStatusFree = (CMD_S_StatusFree*)data;

	///从结构体中取出数据_
	the_sec_start = pStatusFree->cbTimeStartGame;///游戏开始时间_
	the_sec_call = pStatusFree->cbTimeCallint64;///叫分时间_
	the_sec_headOut = pStatusFree->cbTimeHeadOutCard;///首出时间_
	the_sec_outCard = pStatusFree->cbTimeOutCard;///出牌时间_

	///开始按钮_
	btn_start->setVisible(true);
	btn_start->setOpacity(0);
	btn_start->runAction(FadeIn::create(0.3));

	///显示闹钟_
	img_clock->setVisible(true);
//	img_clock->setOpacity(0);
//	img_clock->runAction(FadeIn::create(0.3));

	///显示昵称_
	std::string my_nick = kernel->GetMeUserItem()->GetNickName();
	txt_mynick->setString(my_nick);

	///显示金币数量_
	long long my_gold = kernel->GetMeUserItem()->GetUserScore();
	str_holder = String::createWithFormat("%lld", my_gold);
	alt_mygold->setString(str_holder->getCString());
	//al_mScore->setString(StringUtils::format("%d", str_holder))

	///显示空闲状态倒计时_
	time_cnts = the_sec_start;
	str_holder = String::createWithFormat("%d", pStatusFree->cbTimeStartGame);
	txt_time_back->setString(str_holder->getCString());

	///设置游戏倍数_
	alt_beishu_num->setString("0");
	beishu =0;
	///隐藏玩家剩余扑克数量盒子_
	img_cards_count_box1->setVisible(false);
	img_cards_count_box2->setVisible(false);
	
	///开启空闲状态到计时_
	this->schedule(schedule_selector(FrameLayer_doudizhu::updateFreeTimeBack), 1.0f);

}

///设置游戏进行状态_
void FrameLayer_doudizhu::setGameScenePlay(void * data, int dataSize)
{
	IClientKernel * kernel = IClientKernel::get();
	String* str_holder = nullptr;

	///转换结构体类型_
	CMD_S_StatusPlay* pStatusPlay = (CMD_S_StatusPlay*)data;

	///从结构体中取出数据_
	the_sec_start = pStatusPlay->cbTimeStartGame;///游戏开始时间_
	the_sec_call = pStatusPlay->cbTimeCallint64;///叫分时间_
	the_sec_headOut = pStatusPlay->cbTimeHeadOutCard;///首出时间_
	the_sec_outCard = pStatusPlay->cbTimeOutCard;///出牌时间_

	///开始按钮_
	btn_start->setVisible(false);

	///显示闹钟_
	img_clock->setVisible(false); 

	///显示昵称_
	std::string my_nick = kernel->GetMeUserItem()->GetNickName();
	txt_mynick->setString(my_nick);

	///显示金币数量_
	long long my_gold = kernel->GetMeUserItem()->GetUserScore();
	str_holder = String::createWithFormat("%lld", my_gold);
	alt_mygold->setString(str_holder->getCString());


	///设置游戏倍数_
	str_holder = String::createWithFormat("%d", pStatusPlay->cbBankerint64);
	alt_beishu_num->setString(str_holder->getCString());
	beishu = pStatusPlay->cbBankerint64;

	///显示玩家剩余扑克数量盒子_
	img_cards_count_box1->setVisible(true);
	img_cards_count_box2->setVisible(true);

	///设置玩家性别_
	setAllPlayerSex();

	///退出游戏需要提示_
	exit_is_tips = true;

	///进入游戏战斗界面_
	fighting_layer = GameFightingLayer::create(the_sec_headOut, the_sec_outCard);
	addChild(fighting_layer);

	img_prepare_me->setVisible(false);
	img_prepare_player1->setVisible(false);
	img_prepare_player2->setVisible(false);

	///设置剩余扑克张数_
	int_pukers_left = pStatusPlay->cbHandCardCount[chair_left];
	int_pukers_right = pStatusPlay->cbHandCardCount[chair_right];
	str_holder = String::createWithFormat("%d", pStatusPlay->cbHandCardCount[chair_left]);
	txt_nums_player1->setString(str_holder->getCString());
	str_holder = String::createWithFormat("%d", pStatusPlay->cbHandCardCount[chair_right]);
	txt_nums_player2->setString(str_holder->getCString());

	if (pStatusPlay->cbHandCardCount[chair_left] <= 3)
		txt_nums_player1->setColor(Color3B(255, 0, 0));

	if (pStatusPlay->cbHandCardCount[chair_right] <= 3)
		txt_nums_player2->setColor(Color3B(255, 0, 0));

	///进入战斗游戏界面_
	fighting_layer->setGameFightingPlay(data, dataSize);

	///设置地主农民标识_
	int int_banker_player = pStatusPlay->wBankerUser;
	if (int_banker_player == chair_left)
	{
		str_holder = String::createWithFormat("%d", int_pukers_left);
		txt_nums_player1->setString(str_holder->getCString());

		
		img_head_player1->loadTexture("doudizhuGameScene/GameMainScene/layout/game_role21.png");

	
		img_head_player2->loadTexture("GameMainScene/layout/game_role12.png");

		
		img_head_me->loadTexture("GameMainScene/layout/game_role02.png");
		img_head_me->setScale(1.3);
	}
	else if (int_banker_player == chair_right)
	{
		str_holder = String::createWithFormat("%d", int_pukers_right);
		txt_nums_player2->setString(str_holder->getCString());

		
		img_head_player1->loadTexture("GameMainScene/layout/game_role22.png");

		img_head_player2->loadTexture("doudizhuGameScene/GameMainScene/layout/game_role11.png");

		img_head_me->loadTexture("GameMainScene/layout/game_role02.png");
		img_head_me->setScale(1.3);
	}
	else if (int_banker_player == chair_me)
	{
		
		img_head_player1->loadTexture("GameMainScene/layout/game_role22.png");

		img_head_player2->loadTexture("GameMainScene/layout/game_role12.png");

		img_head_me->loadTexture("doudizhuGameScene/GameMainScene/layout/game_role01.png");
		img_head_me->setScale(1.15);
	}
}

///设置游戏开始_
void FrameLayer_doudizhu::setGameSceneStart(void * data, int dataSize)
{
	IClientKernel * kernel = IClientKernel::get();
	
	///设置玩家性别_
	setAllPlayerSex();
	
	img_head_me->loadTexture("GameMainScene/layout/game_role02.png");
	img_head_me->setScale(1.3);
	img_head_player1->loadTexture("GameMainScene/layout/game_role22.png");
	img_head_player2->loadTexture("GameMainScene/layout/game_role12.png");
	///设置游戏倍数_
	alt_beishu_num->setString("0");
	beishu = 0;

	///恢复玩家剩余扑克数量字体颜色_
	txt_nums_player1->setColor(Color3B(255,255,255));
	txt_nums_player2->setColor(Color3B(255, 255, 255));

	

	///转换结构体类型_
	CMD_S_GameStart* pStatusStart = (CMD_S_GameStart*)data;

	///当前叫分玩家_
	int_start_user = pStatusStart->wStartUser;

	///退出游戏需要提示_
	exit_is_tips = true;

	///启动剩余扑克计数累加_
	time_cnts = 0;
	img_cards_count_box1->setVisible(true);
	img_cards_count_box2->setVisible(true);
	this->schedule(schedule_selector(FrameLayer_doudizhu::updateStartCardsAdd), 0.1f);

	///进入游戏战斗界面_
	fighting_layer = GameFightingLayer::create(the_sec_headOut, the_sec_outCard);
	addChild(fighting_layer);

	img_prepare_me->setVisible(false);
	img_prepare_player1->setVisible(false);
	img_prepare_player2->setVisible(false);

	fighting_layer->setGameFightingStart(data, dataSize);
}

///设置游戏叫分_
void FrameLayer_doudizhu::setGameSceneCall(void * data, int dataSize)
{
	///转换结构体类型_
	CMD_S_Callint64* pStatusStart = (CMD_S_Callint64*)data;

	///获取当前玩家_
	int_start_user = pStatusStart->wCurrentUser;

	img_clock->setVisible(false);
	this->unschedule(schedule_selector(FrameLayer_doudizhu::updateTimeBack));

	///获取上次叫分_
	int int_pre_call = pStatusStart->cbUserCallint64;

	///进行完一圈叫分，重置战斗界面_
	if (pStatusStart->wCurrentUser > 244)
	{
		if (int_pre_call == 255 && pStatusStart->cbCurrentint64 == 0)
		{
			fighting_layer->removeFromParentAndCleanup(true);
			fighting_layer = GameFightingLayer::create(the_sec_headOut, the_sec_outCard);
			addChild(fighting_layer);
		}
		else
		{
			///设置谈话框_
			fighting_layer->setTalkBox(pStatusStart->cbUserCallint64, pStatusStart->wCallint64User);
		}
		return;
	}
	
	///设置谈话框_
	fighting_layer->setTalkBox(pStatusStart->cbUserCallint64, pStatusStart->wCallint64User);

	if (int_pre_call == 3)
	{
		///叫分结束_
		return;
	}
	else if (int_pre_call == 2)
	{
		btn_call_one->setBright(false);
		btn_call_one->setEnabled(false);
		btn_call_two->setBright(false);
		btn_call_two->setEnabled(false);
		img_sc_one->loadTexture("GameMainScene/btn/gui-l-text-1-hui.png");
		img_sc_two->loadTexture("GameMainScene/btn/gui-l-text-2-hui.png");
	}
	else if (int_pre_call == 1)
	{
		btn_call_one->setBright(false);
		btn_call_one->setEnabled(false);
		img_sc_one->loadTexture("GameMainScene/btn/gui-l-text-1-hui.png");
	}

	///开始叫分_
	if (int_start_user == chair_me)
	{
		setGameSceneCall();
	}
	else if (int_start_user == chair_left)
	{
		img_clock->setPosition(Vec2(400, 600));
		img_clock->setVisible(true);

		time_cnts = the_sec_call;
		txt_time_back->setString(String::createWithFormat("%d", the_sec_call)->getCString());
		this->schedule(schedule_selector(FrameLayer_doudizhu::updateTimeBack), 1.0f);
	}
	else if (int_start_user == chair_right)
	{
		img_clock->setPosition(Vec2(1030, 600));
		img_clock->setVisible(true);
		
		time_cnts = the_sec_call;
		txt_time_back->setString(String::createWithFormat("%d", the_sec_call)->getCString());
		this->schedule(schedule_selector(FrameLayer_doudizhu::updateTimeBack), 1.0f);
	}
	
}

///设置游戏庄家信息_
void FrameLayer_doudizhu::setGameSceneBanker(void * data, int dataSize)
{
	///转换结构体类型_
	CMD_S_BankerInfo* pStatusStart = (CMD_S_BankerInfo*)data;
	int int_times_num = pStatusStart->cbBankerint64;
	int int_banker_player = pStatusStart->wBankerUser;

	String* str_holder;
	this->unschedule(schedule_selector(FrameLayer_doudizhu::updateTimeBack));
	if (int_banker_player == chair_left)
	{
		int_pukers_left = 20;
		int_pukers_right = 17;
		str_holder = String::createWithFormat("%d", int_pukers_left);
		txt_nums_player1->setString(str_holder->getCString());

		
		img_head_player1->loadTexture("doudizhuGameScene/GameMainScene/layout/game_role21.png");

		img_head_player2->loadTexture("GameMainScene/layout/game_role12.png");

		img_head_me->loadTexture("GameMainScene/layout/game_role02.png");
	}
	else if (int_banker_player == chair_right)
	{
		int_pukers_left = 17;
		int_pukers_right = 20;
		str_holder = String::createWithFormat("%d", int_pukers_right);
		txt_nums_player2->setString(str_holder->getCString());

		img_head_player1->loadTexture("GameMainScene/layout/game_role22.png");

		img_head_player2->loadTexture("doudizhuGameScene/GameMainScene/layout/game_role11.png");

		img_head_me->loadTexture("GameMainScene/layout/game_role02.png");
	}
	else if (int_banker_player == chair_me)
	{
		int_pukers_left = 17;
		int_pukers_right = 17;
		img_head_player1->loadTexture("GameMainScene/layout/game_role22.png");

		img_head_player2->loadTexture("GameMainScene/layout/game_role12.png");

		img_head_me->loadTexture("doudizhuGameScene/GameMainScene/layout/game_role01.png");
	}

	///设置当局倍数_
	str_holder = String::createWithFormat("%d", int_times_num);
	alt_beishu_num->setString(str_holder->getCString());
	beishu = int_times_num;
	fighting_layer->setFightBankerInfo(data, dataSize,this);

	///恢复上次叫2分的按钮_
	btn_call_one->setBright(true);
	btn_call_one->setEnabled(true);
	btn_call_two->setBright(true);
	btn_call_two->setEnabled(true);

	img_sc_one->loadTexture("GameMainScene/btn/gui-l-text-1.png");
	img_sc_two->loadTexture("GameMainScene/btn/gui-l-text-2.png");
}

///设置用户出牌信息_
void FrameLayer_doudizhu::setGameSceneOutCards(void* data, int dataSize)
{
	CMD_S_OutCard* pStatus = (CMD_S_OutCard*)data;
	int int_outCard_player = pStatus->wOutCardUser;
	int int_card_cnts = pStatus->cbCardCount;

	String* str_holder;
	if (int_outCard_player == chair_left)
	{
		int_pukers_left = int_pukers_left - int_card_cnts;
		str_holder = String::createWithFormat("%d", int_pukers_left);
		txt_nums_player1->setString(str_holder->getCString());

// 		///拉警报_
// 		if (int_pukers_left > 0 && int_pukers_left < 4)
// 		{
// 			SoundManager::shared()->playSound("WARNING_LAND");
// 			txt_nums_player1->setColor(Color3B(255, 0, 0));
// 
// 			if (leftIsHave == false){
// 				img_light_left = Sprite::create("GameMainScene/layout/jingbaodeng_0.png");
// 				this->addChild(img_light_left);
// 				img_light_left->setPosition(Vec2(200, 380));
// 
// 
// 				left_liang = Sprite::create("GameMainScene/layout/jingbaodeng_1.png");
// 				left_liang->setPosition(Vec2(35, 48));
// 				img_light_left->addChild(left_liang);
// 				
// 				
// 				this->schedule(schedule_selector(FrameLayer_doudizhu::LightingLeft), 0.5f);
// 
// 			}
// 		}

	}
	else if (int_outCard_player == chair_right)
	{
		int_pukers_right = int_pukers_right - int_card_cnts;
		str_holder = String::createWithFormat("%d", int_pukers_right);
		txt_nums_player2->setString(str_holder->getCString());

// 		///拉警报_
// 		if (int_pukers_right > 0 && int_pukers_right < 4)
// 		{
// 			SoundManager::shared()->playSound("WARNING_LAND");
// 			txt_nums_player2->setColor(Color3B(255, 0, 0));
// 
// 			//右边的报警器
// 			if (rightIsHave == false){
// 				img_light_right = Sprite::create("GameMainScene/layout/jingbaodeng_0.png");
// 				this->addChild(img_light_right);
// 				img_light_right->setPosition(Vec2(1210, 380));
// 
// 
// 				right_liang = Sprite::create("GameMainScene/layout/jingbaodeng_1.png");
// 				right_liang->setPosition(Vec2(35, 48));
// 				img_light_right->addChild(right_liang);
// 
// 
// 				this->schedule(schedule_selector(FrameLayer_doudizhu::LightingRight), 0.5f);
// 			}
// 		}

	}

	fighting_layer->setFightOutcards(data, dataSize);
}
void FrameLayer_doudizhu::LightingLeft(float dt)
{
	Blink * blink = Blink::create(0.5f, 1);
	Hide * hide = Hide::create();
	Sequence * seq = Sequence::create(blink, hide, NULL);
	left_liang->runAction(seq);
	leftIsHave = true;
}
void FrameLayer_doudizhu::LightingRight(float dt)
{
	Blink * blink = Blink::create(0.5f, 1);
	Hide * hide = Hide::create();
	Sequence * seq = Sequence::create(blink, hide, NULL);
	right_liang->runAction(seq);
	rightIsHave = true;
}

///设置放弃出牌信息_
void FrameLayer_doudizhu::setGameScenePassCards(void* data, int dataSize)
{
	fighting_layer->setFightPassCards(data, dataSize);
}

///设置游戏结束_
void FrameLayer_doudizhu::setGameSceneOver(void* data, int dataSize)
{
	if (img_light_left&&leftIsHave == true){

		this->unschedule(schedule_selector(FrameLayer_doudizhu::LightingLeft));
		img_light_left->setVisible(false);
		leftIsHave = false;
	}
	if (img_light_right&&rightIsHave == true){

		this->unschedule(schedule_selector(FrameLayer_doudizhu::LightingRight));
		img_light_right->setVisible(false);
		rightIsHave = false;
	}

	int int_ScorePlayer[3];///游戏积分_

	alt_mygold->setString(fighting_layer->alt_mygold->getString());


	exit_is_tips = false;
	fighting_layer->setFightOver(data, dataSize,this);

	
}

///调用叫分_
void FrameLayer_doudizhu::setGameSceneCall()
{
	img_clock->setPosition(Vec2(710, 380));
	img_clock->setVisible(true);
	Panel_call->setVisible(true);

	///叫分倒计时_
	time_cnts = the_sec_call;
	txt_time_back->setString(String::createWithFormat("%d", the_sec_call)->getCString());
	this->schedule(schedule_selector(FrameLayer_doudizhu::updateCallTimeBack), 1.0f);
}

///倒计时_
void FrameLayer_doudizhu::updateTimeBack(float dt)
{
	if (time_cnts > -1)
	{
		String* str_holder = String::createWithFormat("%d", time_cnts);
		txt_time_back->setString(str_holder->getCString());
		time_cnts--;
	}
	else
	{
		img_clock->setVisible(false);

		this->unschedule(schedule_selector(FrameLayer_doudizhu::updateTimeBack));
	}
}

///叫分倒计时_
void FrameLayer_doudizhu::updateCallTimeBack(float dt)
{
	if (time_cnts > -1)
	{
		if (time_cnts < 5)
		{
			SoundManager::shared()->playSound("TIME_BACK_LAND");
		}

		String* str_holder = String::createWithFormat("%d", time_cnts);
		txt_time_back->setString(str_holder->getCString());
		time_cnts--;
	}
	else
	{
		this->unschedule(schedule_selector(FrameLayer_doudizhu::updateCallTimeBack));

		IClientKernel * kernel = IClientKernel::get();
		///放弃叫分_
		CMD_C_Callint64 CallScore;
		CallScore.cbCallint64 = 0;
		kernel->SendSocketData(SUB_C_CALL_int64, &CallScore, sizeof(CallScore));

		img_clock->setVisible(false);

		//this->schedule(schedule_selector(FrameLayer_doudizhu::updateTimeBack), 1.0f);

		Panel_call->setVisible(false);
	}

}

///游戏开始剩余扑克计数器累加_
void FrameLayer_doudizhu::updateStartCardsAdd(float dt)
{
	if (time_cnts < 17)
	{
		String* str_holder = String::createWithFormat("%d", time_cnts + 1);
		txt_nums_player1->setString(str_holder->getCString());
		txt_nums_player2->setString(str_holder->getCString());
		time_cnts++;
	}
	else
	{
		if (int_start_user == chair_me)
		{
			///叫分设置界面_
			setGameSceneCall();
		}
		else if (int_start_user == chair_left)
		{
			img_clock->setPosition(Vec2(400, 600));
			img_clock->setVisible(true);
			txt_time_back->setString(String::createWithFormat("%d", time_cnts)->getCString());

			this->schedule(schedule_selector(FrameLayer_doudizhu::updateTimeBack), 1.0f);
		}
		else if (int_start_user == chair_right)
		{
			img_clock->setPosition(Vec2(1030, 600));
			img_clock->setVisible(true);
			txt_time_back->setString(String::createWithFormat("%d", time_cnts)->getCString());

			this->schedule(schedule_selector(FrameLayer_doudizhu::updateTimeBack), 1.0f);
		}
		this->unschedule(schedule_selector(FrameLayer_doudizhu::updateStartCardsAdd));
	}
}



///空闲状态倒计时_
void FrameLayer_doudizhu::updateFreeTimeBack(float dt)
{

	if (time_cnts < 1)
	{
		txt_time_back->setString("");
		this->unschedule(schedule_selector(FrameLayer_doudizhu::updateFreeTimeBack));
		///退出本游戏场景_
		ui::Button * btn_now = Button::create();
		btn_now->setTag(DLG_MB_OK);
		IClientKernel * kernel = IClientKernel::get();
		(mTarget->*mCallback)(btn_now);
	}
	else
	{
		time_cnts--;
		///显示空闲状态倒计时_
		String* str_holder = String::createWithFormat("%d", time_cnts);
		txt_time_back->setString(str_holder->getCString());
	}

}

