#include "Tools/tools/MTNotification.h"
#include "Tools/Manager/SoundManager.h"
#include "ClientKereneSink_doudizhu.h"
#include "Kernel/kernel/server/IServerItemSink.h"
#include "Tools/tools/StringData.h"
#include "Tools/Dialog/Timer.h"
#include "Kernel/kernel/server/IServerItem.h"
#include "Tools/Dialog/NewDialog.h"

#include "GameScene_doudizhu.h"
#include "cmd_doudizhu.h"

USING_NS_CC;
using namespace doudizhu;

ClientKereneSink_doudizhu::ClientKereneSink_doudizhu()
{
	
}

ClientKereneSink_doudizhu::~ClientKereneSink_doudizhu()
{
	m_pGameScene = nullptr;
}

///启动游戏场景_
bool ClientKereneSink_doudizhu::SetupGameClient()
{
	Director * director = Director::getInstance();
	Scene * pScene = director->getRunningScene();
	if (pScene->getTag() == KIND_ID && pScene->getName() == "GameScene")
	{
		///表示是自己_
		m_pGameScene = dynamic_cast<GameScene_doudizhu *>(pScene);
	}
	else
	{
		if (!m_pGameScene)
			m_pGameScene = GameScene_doudizhu::create();
	}

	if (pScene != m_pGameScene)
	{
		director->pushScene(CCTransitionFade::create(0.5f, m_pGameScene));
		SoundManager::shared()->stopMusic();
		SoundManager::shared()->playMusic("BACK_MUSIC_LAND");
		SoundManager::shared()->playSound("GAME_ENTER_LAND");
	}
	else
	{
		if (IClientKernel::get())
		{
			IClientKernel::get()->SendGameOption();
		}
	}

	return false;
}

///重置游戏_
void ClientKereneSink_doudizhu::ResetGameClient()
{
	log("调试信息_: 重置游戏_");
}

///关闭游戏_
void ClientKereneSink_doudizhu::CloseGameClient(int exit_tag /*= 0*/)
{
	m_pGameScene = 0;

	if (!GameScene_doudizhu::is_Reconnect_on_loss())
		G_NOTIFY_D("MODE_SELECTED", MTData::create(1));
}

///系统滚动消息_
bool ClientKereneSink_doudizhu::OnGFTableMessage(const char* szMessage)
{

	return true;
}

///全局消息_
bool ClientKereneSink_doudizhu::OnGFGlobalMessage(const char* szMessage)
{

	return true;
}

///等待提示_
bool ClientKereneSink_doudizhu::OnGFWaitTips(bool bWait)
{

	return true;
}

///比赛信息_
bool ClientKereneSink_doudizhu::OnGFMatchInfo(tagMatchInfo* pMatchInfo)
{

	return true;
}

///比赛等待提示_
bool ClientKereneSink_doudizhu::OnGFMatchWaitTips(tagMatchWaitTip* pMatchWaitTip)
{
	return true;
}

///比赛结果_
bool ClientKereneSink_doudizhu::OnGFMatchResult(tagMatchResult* pMatchResult)
{
	return true;
}
//////////////////////////////////////////////////////////////////////////
///游戏事件_

///旁观消息_
bool ClientKereneSink_doudizhu::OnEventLookonMode(void* data, int dataSize)
{
	return true;
}

///场景消息_
bool ClientKereneSink_doudizhu::OnEventSceneMessage(byte cbGameStatus, bool bLookonUser, void* data, int dataSize)
{
	IClientKernel* kernel = IClientKernel::get();
	kernel->SetGameStatus(cbGameStatus);

 	switch (cbGameStatus)
 	{
 		case GAME_SCENE_FREE:		//空闲状态_
 		{
 			return onEventSceneFree(data, dataSize);
 		}
		case GAME_SCENE_CALL:		//叫分状态_
		{
			return true;
		}
		case GAME_SCENE_PLAY:		//进行状态_
 		{
			return onEventSceneGameplaying(data, dataSize);
 		}
 	}

	return false;
}

///空闲状态消息处理_
bool ClientKereneSink_doudizhu::onEventSceneFree(void * data, int dataSize)
{
	IClientKernel * kernel = IClientKernel::get();
	if (!kernel) return false;

	//效验数据_
	ASSERT(dataSize == sizeof(CMD_S_StatusFree));
	if (dataSize != sizeof(CMD_S_StatusFree)) return false;

	///传递参数给游戏场景，设置游戏信息_
	m_pGameScene->transmitSetGameSceneFree(data, dataSize);

	return true;
}

///游戏进行状态_ CMD_S_StatusPlay
bool ClientKereneSink_doudizhu::onEventSceneGameplaying(void* data, int dataSize)
{
	IClientKernel * kernel = IClientKernel::get();
	if (!kernel) return false;

	//效验数据_

	ASSERT(dataSize == sizeof(CMD_S_StatusPlay));
	if (dataSize != sizeof(CMD_S_StatusPlay)) return false;

	///传递参数给游戏场景，设置游戏信息_
	m_pGameScene->transmitSetGameScenePlay(data, dataSize);

	return true;
}

///游戏开始消息处理_  
bool ClientKereneSink_doudizhu::onEventGameStart(void * data, int dataSize)
{
	IClientKernel * kernel = IClientKernel::get();
	if (!kernel) return false;

	//效验数据_
	ASSERT(dataSize == sizeof(CMD_S_GameStart));
	if (dataSize != sizeof(CMD_S_GameStart)) return false;

	///传递参数给游戏场景，设置游戏信息_
	m_pGameScene->transmitSetGameSceneStart(data, dataSize);

	return true;
}

/// 用户叫分消息处理_
bool ClientKereneSink_doudizhu::onEventGameCall(void* data, int dataSize)
{
	IClientKernel * kernel = IClientKernel::get();
	if (!kernel) return false;

	//效验数据_
	ASSERT(dataSize == sizeof(CMD_S_Callint64));
	if (dataSize != sizeof(CMD_S_Callint64)) return false;

	///传递参数给游戏场景，设置游戏信息_
	m_pGameScene->transmitSetGameSceneCall(data, dataSize);


	return true;
}

///庄家信息消息处理_
bool ClientKereneSink_doudizhu::onEventGameBanker(void* data, int dataSize)
{
	IClientKernel * kernel = IClientKernel::get();
	if (!kernel) return false;

	//效验数据_
	ASSERT(dataSize == sizeof(CMD_S_BankerInfo));
	if (dataSize != sizeof(CMD_S_BankerInfo)) return false;

	///传递参数给游戏场景，设置游戏信息_
	m_pGameScene->transmitSetGameSceneBanker(data, dataSize);

	return true;
}

///用户出牌消息处理_
bool ClientKereneSink_doudizhu::OnEventGameOutCards(void* data, int dataSize)
{
	IClientKernel * kernel = IClientKernel::get();
	if (!kernel) return false;

	//效验数据_
	CMD_S_OutCard * pOutCard = (CMD_S_OutCard *)data;
	word wHeadSize = sizeof(CMD_S_OutCard)-sizeof(pOutCard->cbCardData);

	//效验数据_
	ASSERT((dataSize >= wHeadSize) && (dataSize == (wHeadSize + pOutCard->cbCardCount*sizeof(byte))));
	if ((dataSize < wHeadSize) || (dataSize != (wHeadSize + pOutCard->cbCardCount*sizeof(byte)))) return false;

	///传递参数给游戏场景，设置游戏信息_
	m_pGameScene->transmitSetGameSceneOutCards(data, dataSize);

	return true;
}

///用户放弃消息处理_
bool ClientKereneSink_doudizhu::OnEventGamePassCards(void* data, int dataSize)
{
	IClientKernel * kernel = IClientKernel::get();
	if (!kernel) return false;

	//效验数据_
	ASSERT(dataSize == sizeof(CMD_S_PassCard));
	if (dataSize != sizeof(CMD_S_PassCard)) return false;

	///传递参数给游戏场景，设置游戏信息_
	m_pGameScene->transmitSetGameScenePassCards(data, dataSize);

	return true;
}

///游戏结束消息处理_
bool ClientKereneSink_doudizhu::OnEventGameOver(void* data, int dataSize)
{
	IClientKernel * kernel = IClientKernel::get();
	if (!kernel) return false;

	//效验数据_
	int x = sizeof(CMD_S_GameConclude);
	ASSERT(dataSize == sizeof(CMD_S_GameConclude));
	if (dataSize != sizeof(CMD_S_GameConclude)) return false;

	///传递参数给游戏场景，设置游戏信息_
	m_pGameScene->transmitSetGameSceneOver(data, dataSize);

	return true;
}

//游戏消息_
bool ClientKereneSink_doudizhu::OnEventGameMessage(int wSubCmdID, void* pData, int wDataSize)
{

	switch (wSubCmdID)
	{
		case SUB_S_GAME_START:		//游戏开始_
		{
										return	onEventGameStart(pData, wDataSize);
		}
		case SUB_S_CALL_int64:		//用户叫分_
		{
										return	onEventGameCall(pData, wDataSize);
		}
		case SUB_S_BANKER_INFO:		//庄家信息_
		{
										return onEventGameBanker(pData, wDataSize);
		}
		case SUB_S_OUT_CARD:		//用户出牌_
		{
										return OnEventGameOutCards(pData, wDataSize);
		}
		case SUB_S_PASS_CARD:		//用户放弃_
		{
										return OnEventGamePassCards(pData, wDataSize);
		}
		case SUB_S_GAME_CONCLUDE:	//游戏结束_
		{
										return OnEventGameOver(pData, wDataSize);
		}
		case SUB_S_CHEAT_CARD:		//作弊数据_
		{
										return true;
		}
	}

	return true;
}

//////////////////////////////////////////////////////////////////////////
//时钟事件_
//用户时钟_
void ClientKereneSink_doudizhu::OnEventUserClock(word wChairID, word wUserClock)
{
	log("   ");
}

//时钟删除_
bool ClientKereneSink_doudizhu::OnEventGameClockKill(word wChairID)
{
	return true;
}

//时钟信息_
bool ClientKereneSink_doudizhu::OnEventGameClockInfo(word wChairID, uint nElapse, word wClockID)
{
	IClientKernel * kernel = IClientKernel::get();

	return false;
}

//用户进入_
void ClientKereneSink_doudizhu::OnEventUserEnter(IClientUserItem * pIClientUserItem, bool bLookonUser)
{
	Director * director = Director::getInstance();
	Scene * pScene = director->getRunningScene();
	if (pScene->getTag() == KIND_ID && pScene->getName() == "GameScene")
	{
		///< 表示是自己_!!
		m_pGameScene = dynamic_cast<GameScene_doudizhu *>(pScene);
	}
	else if (!m_pGameScene)
	{
		m_pGameScene = GameScene_doudizhu::create();
	}

	///设置其他玩家准备状态_
	if (!bLookonUser && m_pGameScene)
	{
		m_pGameScene->showUserInfo(pIClientUserItem, pIClientUserItem->GetUserInfo()->cbUserStatus);
	}

}

//用户离开_
void ClientKereneSink_doudizhu::OnEventUserLeave(IClientUserItem * pIClientUserItem, bool bLookonUser)
{
	///设置其他玩家离开状态_
	if (!bLookonUser)
	{
		m_pGameScene->cleanUserInfo(pIClientUserItem);
	}
}

//用户积分_
void ClientKereneSink_doudizhu::OnEventUserScore(IClientUserItem * pIClientUserItem, bool bLookonUser)
{
	log("   ");
}

//用户状态_
void ClientKereneSink_doudizhu::OnEventUserStatus(IClientUserItem * pIClientUserItem, bool bLookonUser)
{
	if (!bLookonUser)
		G_NOTIFY_D("EVENT_USER_STATUS", MTData::create((unsigned int)pIClientUserItem));

	IClientKernel* kernel = IClientKernel::get();
	if (m_pGameScene)
	{
		//US_READY_
		m_pGameScene->net_user_state_update(pIClientUserItem, pIClientUserItem->GetUserStatus());
	}

}

//用户属性_
void ClientKereneSink_doudizhu::OnEventUserAttrib(IClientUserItem * pIClientUserItem, bool bLookonUser)
{
	log("   ");
}

//用户头像_
void ClientKereneSink_doudizhu::OnEventCustomFace(IClientUserItem * pIClientUserItem, bool bLookonUser)
{
	log("   ");
}

void ClientKereneSink_doudizhu::CloseGameDelayClient()
{
	m_pGameScene->ExitGameTiming();
}
