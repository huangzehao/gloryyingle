#include "GameLogic_Land.h"
#include "cmd_doudizhu.h"

LogicLand* LogicLand::p_logic = nullptr;
bool LogicLand::b_loop = false;

LogicLand* LogicLand::getInstance()
{
	if (!p_logic)
	{
		p_logic = new LogicLand();
	}

	return p_logic;
}

void LogicLand::setB_Loop()
{
	b_loop = false;
}

///将我选择的扑克写入结构体_
void LogicLand::chooseMyPukers(Puker* puke_box[20], CMD_C_OutCard* outCard, int cur_pukers, int puke[20])
{
	int cnts = 0;

	for (int i = 0; i < cur_pukers; i++)
	{
		if (puke_box[i]->isUp)
		{
			outCard->cbCardData[cnts] = convertServerPoint(puke_box[i]->int_puker_point);
			puke[i] = -1;
			cnts++;
		}
	}

	resetThePoint(puke, 20);

	outCard->cbCardCount = cnts;

}

///判断我选择的扑克是否符合逻辑_
bool LogicLand::myChoicePukerIsOk(int require_logic, int requir_cnts, int pre_maxPoint, Puker* puke_box[20], int myPuker_cnts)
{
	int puke[20];
	int cnts = 0;

	for (int i = 0; i < myPuker_cnts; i++)
	{
		if (puke_box[i]->isUp)
		{
			puke[cnts] = convertLandPoint(puke_box[i]->int_puker_point);
			cnts++;
		}
	}

	///没有选中扑克_
	if (cnts == 0)
		return false;

	if (require_logic == CT_ANY_IS_OK)
	{
		if (getLandLogic(puke, cnts) == CT_ERROR)
		{
			return false;
		}
	}
	else
	{
		///判断当前相同逻辑的牌是否大过上家的牌_
		if (getLandLogic(puke, cnts) == require_logic)
		{
			if (getLogicPoint(require_logic, puke, cnts) <= pre_maxPoint || cnts != requir_cnts)
				return false;

		}
		else
		{
			///判断是否为炸弹_
			if (getLandLogic(puke, cnts) < 14)
			{
				return false;
			}
			else
			{
				if (getLandLogic(puke, cnts) < require_logic)
					return false;
			}

		}

	}

	return true;
}

///获取逻辑的点数_
int LogicLand::getLogicPoint(int require_logic, int land_point[20], int cnts)
{
	resetThePoint(land_point, cnts);

	bool b = require_logic == CT_THREE_TAKE_ONE || require_logic == CT_THREE_TAKE_TWO || require_logic == CT_FOUR_TAKE_ONE || 
		require_logic == CT_FOUR_TAKE_TWO || require_logic == CT_SINGLE_FLY || require_logic == CT_DOUBLE_FLY;

	if (b)
	{
		while (land_point[0] != land_point[2])
		{
			///把盒子往前推_
			int temp = land_point[0];
			for (int i = 0; i < cnts - 1; i++)
			{
				land_point[i] = land_point[i + 1];

			}
			land_point[cnts - 1] = temp;
		}

		return land_point[0];

	}
	else
	{
		return land_point[0];
	}

}

///只能筛选顺子或双顺牌型扑克_
void LogicLand::autoSeclectPukers(Puker* puke_box[20], int cnts)
{
	///智能筛选双顺_
	if (isExistLineDouble(puke_box, cnts))
		return;

	///智能筛选单顺_
	if (isExistLineSingle(puke_box, cnts))
		return;

}

///提示出牌按钮处理_
int LogicLand::tipOutCards(Puker* puke_box[20], int cnts, int land_logic, int requir_cnts, int int_cur_point)///int_cur_point是上家牌的逻辑点数_
{
	///全清选中扑克_
	for (int i = cnts - 1; i > -1; i--)
	{
		if (puke_box[i]->isUp)
		{
			puke_box[i]->runAction(MoveBy::create(0.1, Vec2(0, -50)));
			puke_box[i]->isUp = !(puke_box[i]->isUp);
		}
	}

	///判断单牌逻辑_
	if (land_logic == CT_SINGLE)
	{
		for (int i = cnts - 1; i > -1; i--)
		{
			if (convertLandPoint(puke_box[i]->int_puker_point) > int_cur_point)
			{
				if (i == 0)
				{
					puke_box[i]->runAction(MoveBy::create(0.1, Vec2(0, 50)));
					puke_box[i]->isUp = !(puke_box[i]->isUp);
					b_loop = true;
					return convertLandPoint(puke_box[i]->int_puker_point);
				}

				if (convertLandPoint(puke_box[i]->int_puker_point) != convertLandPoint(puke_box[i - 1]->int_puker_point))
				{
					puke_box[i]->runAction(MoveBy::create(0.1, Vec2(0, 50)));
					puke_box[i]->isUp = !(puke_box[i]->isUp);
					b_loop = true;
					return convertLandPoint(puke_box[i]->int_puker_point);
				}

				while (convertLandPoint(puke_box[i]->int_puker_point) == convertLandPoint(puke_box[i - 1]->int_puker_point))
				{
					i--;
					if (i == 0)///一次循环结束_
					{
						if (b_loop)
							return -1;
						else
							break;
					}
				}

			}

		}

	}

	///判断对子逻辑_
	if (land_logic == CT_DOUBLE)
	{
		for (int i = cnts - 1; i > -1; i--)
		{
			if (i == 0)
				break;
			if (convertLandPoint(puke_box[i]->int_puker_point) > int_cur_point)
			{
				if (i == 1)
				{
					if (convertLandPoint(puke_box[i]->int_puker_point) == convertLandPoint(puke_box[i - 1]->int_puker_point))
					{
						puke_box[i]->runAction(MoveBy::create(0.1, Vec2(0, 50)));
						puke_box[i]->isUp = !(puke_box[i]->isUp);
						puke_box[i - 1]->runAction(MoveBy::create(0.1, Vec2(0, 50)));
						puke_box[i - 1]->isUp = !(puke_box[i - 1]->isUp);

						b_loop = true;
						return convertLandPoint(puke_box[i]->int_puker_point);
					}
					else
					{
						break;
					}

				}

				if (convertLandPoint(puke_box[i]->int_puker_point) == convertLandPoint(puke_box[i - 1]->int_puker_point) &&
					convertLandPoint(puke_box[i]->int_puker_point) != convertLandPoint(puke_box[i - 2]->int_puker_point))
				{
					puke_box[i]->runAction(MoveBy::create(0.1, Vec2(0, 50)));
					puke_box[i]->isUp = !(puke_box[i]->isUp);
					puke_box[i - 1]->runAction(MoveBy::create(0.1, Vec2(0, 50)));
					puke_box[i - 1]->isUp = !(puke_box[i - 1]->isUp);
					b_loop = true;
					return convertLandPoint(puke_box[i]->int_puker_point);
				}
				while (convertLandPoint(puke_box[i]->int_puker_point) == convertLandPoint(puke_box[i - 2]->int_puker_point))
				{
					i -= 2;
					if (i < 2)///一次循环结束_
					{
						if (b_loop)
							return -1;
						else
							break;
					}

				}

			}

		}

	}

	///判断三条逻辑_
	if (land_logic == CT_THREE)
	{
		for (int i = cnts - 1; i > -1; i--)
		{
			if (i == 0 || i == 1)
				break;
			if (convertLandPoint(puke_box[i]->int_puker_point) > int_cur_point)
			{
				if (i == 2)
				{
					if (convertLandPoint(puke_box[i]->int_puker_point) == convertLandPoint(puke_box[i - 2]->int_puker_point))
					{
						puke_box[i]->runAction(MoveBy::create(0.1, Vec2(0, 50)));
						puke_box[i]->isUp = !(puke_box[i]->isUp);
						puke_box[i - 1]->runAction(MoveBy::create(0.1, Vec2(0, 50)));
						puke_box[i - 1]->isUp = !(puke_box[i - 1]->isUp);
						puke_box[i - 2]->runAction(MoveBy::create(0.1, Vec2(0, 50)));
						puke_box[i - 2]->isUp = !(puke_box[i - 2]->isUp);

						b_loop = true;
						return convertLandPoint(puke_box[i]->int_puker_point);
					}
					else
					{
						break;
					}
				}

				if (convertLandPoint(puke_box[i]->int_puker_point) == convertLandPoint(puke_box[i - 2]->int_puker_point) &&
					convertLandPoint(puke_box[i]->int_puker_point) != convertLandPoint(puke_box[i - 3]->int_puker_point))
				{
					puke_box[i]->runAction(MoveBy::create(0.1, Vec2(0, 50)));
					puke_box[i]->isUp = !(puke_box[i]->isUp);
					puke_box[i - 1]->runAction(MoveBy::create(0.1, Vec2(0, 50)));
					puke_box[i - 1]->isUp = !(puke_box[i - 1]->isUp);
					puke_box[i - 2]->runAction(MoveBy::create(0.1, Vec2(0, 50)));
					puke_box[i - 2]->isUp = !(puke_box[i - 2]->isUp);

					b_loop = true;
					return convertLandPoint(puke_box[i]->int_puker_point);
				}

				while (convertLandPoint(puke_box[i]->int_puker_point) == convertLandPoint(puke_box[i - 3]->int_puker_point))
				{
					i -= 3;
					if (i < 3)///一次循环结束_
					{
						if (b_loop)
							return -1;
						else
							break;
					}

				}

			}

		}

	}

	///判断单连的逻辑_
	if (land_logic == CT_SINGLE_LINE)
	{
		for (int i = cnts - 1; i > -1; i--)
		{
			if (convertLandPoint(puke_box[i]->int_puker_point) > 14 || cnts < requir_cnts)///点数在A以上或者手上扑克数量少于连牌数量_
				break;

			if (convertLandPoint(puke_box[i]->int_puker_point) > int_cur_point)
			{
				if (i + requir_cnts > cnts)
				{
					continue;;///尾部的扑克数量少于连牌数量_
				}
				else
				{
					int dex[12];///存储符合条件的数组下标_
					int _cur_point = convertLandPoint(puke_box[i]->int_puker_point) - 1;///第二个节点的地主点数_
					int x = 0;///dex数组的下标_
					dex[x] = i;
					x++;
					for (int j = i + 1; j < cnts; j++)
					{
						if (convertLandPoint(puke_box[j]->int_puker_point) == _cur_point)
						{
							_cur_point--;
							dex[x] = j;
							x++;
							if (x == requir_cnts)
								break;
						}
					}

					if (x == requir_cnts)///匹配成功_
					{
						for (int k = 0; k < requir_cnts; k++)
						{
							puke_box[dex[k]]->runAction(MoveBy::create(0.1, Vec2(0, 50)));
							puke_box[dex[k]]->isUp = !(puke_box[dex[k]]->isUp);
						}

						return convertLandPoint(puke_box[i]->int_puker_point);
					}

				}

			}

		}

	}

	///判断对连的逻辑_
	if (land_logic == CT_DOUBLE_LINE)
	{
		for (int i = cnts - 1; i > -1; i--)
		{
			if (convertLandPoint(puke_box[i]->int_puker_point) > 14 || cnts < requir_cnts)///点数在A以上或者手上扑克数量少于连牌数量_
				break;

			if (convertLandPoint(puke_box[i]->int_puker_point) > int_cur_point)
			{
				if (i == 0)///当前扑克前面还有一张牌_
				{
					break; ///或者当前扑克为第一张_
				}

				if (i + requir_cnts - 1 > cnts)
				{
					continue;;///尾部的扑克数量少于连牌数量_ 
				}
				else
				{
					if (convertLandPoint(puke_box[i]->int_puker_point) == convertLandPoint(puke_box[i - 1]->int_puker_point))
					{
						int dex[20];///存储符合条件的数组下标_
						int _cur_point = convertLandPoint(puke_box[i]->int_puker_point) - 1;///第二个节点的地主点数_
						int x = 0;///dex数组的下标_
						dex[x] = i - 1;
						dex[x + 1] = i;
						x += 2;

						for (int j = i + 2; j < cnts - 1; j += 2)
						{
							if (convertLandPoint(puke_box[j]->int_puker_point) == _cur_point && convertLandPoint(puke_box[j + 1]->int_puker_point) == _cur_point)
							{
								_cur_point--;
								dex[x] = j;
								dex[x + 1] = j + 1;
								x += 2;
								if (x == requir_cnts)///匹配成功结束_
									break;
								if (convertLandPoint(puke_box[j + 2]->int_puker_point) == _cur_point + 1)
									j++;
								else if (convertLandPoint(puke_box[j + 3]->int_puker_point) == _cur_point + 1)
								{
									j += 2;
								}
							}
						}

						if (x == requir_cnts)///匹配成功_
						{
							for (int k = 0; k < requir_cnts; k++)
							{
								puke_box[dex[k]]->runAction(MoveBy::create(0.1, Vec2(0, 50)));
								puke_box[dex[k]]->isUp = !(puke_box[dex[k]]->isUp);
							}

							return convertLandPoint(puke_box[i]->int_puker_point);
						}

					}

				}

			}


		}

	}

	///判断三连的逻辑_
	if (land_logic == CT_THREE_LINE)
	{
		for (int i = cnts - 1; i > -1; i--)
		{
			if (convertLandPoint(puke_box[i]->int_puker_point) > 14 || cnts < requir_cnts)///点数在A以上或者手上扑克数量少于连牌数量_
				break;

			if (convertLandPoint(puke_box[i]->int_puker_point) > int_cur_point)
			{
				if (i == 0 || i == 1)///当前扑克前面还有两张牌_
					break; ///当前扑克为前两张_

				if (i + requir_cnts - 2 > cnts)
				{
					continue;;///尾部的扑克数量少于连牌数量_
				}
				else
				{
					if (convertLandPoint(puke_box[i]->int_puker_point) == convertLandPoint(puke_box[i - 2]->int_puker_point))
					{
						int dex[20];///存储符合条件的数组下标_
						int _cur_point = convertLandPoint(puke_box[i]->int_puker_point) - 1;///第二个节点的地主点数_
						int x = 0;///dex数组的下标_
						dex[x] = i - 2;
						dex[x + 1] = i - 1;
						dex[x + 2] = i;
						x += 3;

						for (int j = i + 3; j < cnts - 2; j += 3)///后面需要留三张牌_
						{
							if (convertLandPoint(puke_box[j]->int_puker_point) == _cur_point && convertLandPoint(puke_box[j + 2]->int_puker_point) == _cur_point)
							{
								_cur_point--;
								dex[x] = j;
								dex[x + 1] = j + 1;
								dex[x + 2] = j + 2;
								x += 3;
								if (x == requir_cnts)///匹配成功结束_
									break;
								if (convertLandPoint(puke_box[j + 3]->int_puker_point) == _cur_point + 1)
									j++;

							}
						}

						if (x == requir_cnts)///匹配成功_
						{
							for (int k = 0; k < requir_cnts; k++)
							{
								puke_box[dex[k]]->runAction(MoveBy::create(0.1, Vec2(0, 50)));
								puke_box[dex[k]]->isUp = !(puke_box[dex[k]]->isUp);
							}

							return convertLandPoint(puke_box[i]->int_puker_point);
						}

					}
				}
			}
		}
	}

	///判断三带一单逻辑_
	while(land_logic == CT_THREE_TAKE_ONE)
	{
		int re_point = 0;///需要返回的点数_
		int dex[4];///三张相同扑克的下标容器_
		for (int i = cnts - 1; i > -1; i--)///检测三张相同扑克_
		{
			if (cnts < requir_cnts)///手上扑克数量少于三带一单数量_
				break;
			if (convertLandPoint(puke_box[i]->int_puker_point) > int_cur_point)
			{
				if (i == 0 || i == 1)
					break; ///当前扑克为前两张_

				if (convertLandPoint(puke_box[i]->int_puker_point) == convertLandPoint(puke_box[i - 2]->int_puker_point))
				{
					///三张相同牌在边上_
					if (i - 3 < 0)
					{
						dex[0] = i - 2;
						dex[1] = i - 1;
						dex[2] = i;
						re_point = convertLandPoint(puke_box[i]->int_puker_point);
						break;
					}

					if (convertLandPoint(puke_box[i]->int_puker_point) == convertLandPoint(puke_box[i - 3]->int_puker_point))
					{
						i -= 3;
						continue;
					}

					dex[0] = i - 2;
					dex[1] = i - 1;
					dex[2] = i;
					re_point = convertLandPoint(puke_box[i]->int_puker_point);
					break;
				}

			}

		}

		if (re_point == 0)
			break;

		int n = 0;///0表示有单牌存在_
		for (int i = cnts - 1; i > -2; i--)///寻找三带一单的单牌_
		{
			if (i <= 0 && n == 0)
			{
				if (i != dex[0] && i != dex[1] && i != dex[2] && i >= 0)
				{
					if (convertLandPoint(puke_box[i]->int_puker_point != convertLandPoint(puke_box[i + 1]->int_puker_point)))
					{
						dex[3] = i;
						break;
					}
					n++;
					i = cnts - 1;
					continue;
				}
				else///检测单牌逻辑循环一圈_
				{
					n++;
					i = cnts;
					continue;
				}

			}

			if (n == 1)
			{
				if (i != dex[0] && i != dex[1] && i != dex[2])
				{
					dex[3] = i;
					break;
				}
				continue;
			}

			if (convertLandPoint(puke_box[i]->int_puker_point) != convertLandPoint(puke_box[i - 1]->int_puker_point))
			{
				if (convertLandPoint(puke_box[i]->int_puker_point) == convertLandPoint(puke_box[dex[0]]->int_puker_point))
				{
					continue;
				}

				if (i == cnts - 1)
				{
					dex[3] = i;
					break;
				}
				else if (convertLandPoint(puke_box[i]->int_puker_point) != convertLandPoint(puke_box[i + 1]->int_puker_point))
				{
					dex[3] = i;
					break;
				}

			}
			else
			{
				i--;
			}
		}

		///匹配成功_
		for (int k = 0; k < requir_cnts; k++)
		{
			puke_box[dex[k]]->runAction(MoveBy::create(0.1, Vec2(0, 50)));
			puke_box[dex[k]]->isUp = !(puke_box[dex[k]]->isUp);
		}

		return re_point;
	}

	///判断三带一对的逻辑_
	while(land_logic == CT_THREE_TAKE_TWO)
	{
		int re_point = 0;///需要返回的点数_
		int dex[5];///三张相同扑克的下标容器_
		for (int i = cnts - 1; i > -1; i--)///检测三张相同扑克_
		{
			if (cnts < requir_cnts)///手上扑克数量少于三带一对数量_
				break;
			if (convertLandPoint(puke_box[i]->int_puker_point) > int_cur_point)
			{
				if (i == 0 || i == 1)
					break; ///当前扑克为前两张_

				if (convertLandPoint(puke_box[i]->int_puker_point) == convertLandPoint(puke_box[i - 2]->int_puker_point))
				{
					///三张相同牌在边上_
					if (i - 3 < 0)
					{
						dex[0] = i - 2;
						dex[1] = i - 1;
						dex[2] = i;
						re_point = convertLandPoint(puke_box[i]->int_puker_point);
						break;
					}

					if (convertLandPoint(puke_box[i]->int_puker_point) == convertLandPoint(puke_box[i - 3]->int_puker_point))
					{
						i -= 3;
						continue;;
					}

					dex[0] = i - 2;
					dex[1] = i - 1;
					dex[2] = i;
					re_point = convertLandPoint(puke_box[i]->int_puker_point);
					break;
				}

			}

		}

		///没有三张相同的扑克跳出_
		if (re_point == 0)
			break;

		///检测对子_
		int n = 0;///进入循环的次数_
		for (int i = cnts - 1; i > -2; i--)
		{
			if (n == 1)
			{
				if (i <= 0)
				{
					n++;
					break;
				}

				if (convertLandPoint(puke_box[i]->int_puker_point) == convertLandPoint(puke_box[i - 1]->int_puker_point) &&
					convertLandPoint(puke_box[i]->int_puker_point) != convertLandPoint(puke_box[dex[0]]->int_puker_point))
				{
					dex[3] = i;
					dex[4] = i - 1;
					break;
				}

			}

			if (i <= 0)
			{
				n++;
				i = cnts;
				continue;;
			}
				

			if (convertLandPoint(puke_box[i]->int_puker_point) == convertLandPoint(puke_box[i - 1]->int_puker_point))
			{
				if (i == 1)
				{
					dex[3] = i;
					dex[4] = i - 1;
					break;
				}

				if (convertLandPoint(puke_box[i]->int_puker_point) == convertLandPoint(puke_box[i - 2]->int_puker_point))
				{
					i -= 2;
					continue;
				}

				dex[3] = i;
				dex[4] = i - 1;
				break;
			}
		}

		///没有找到三带对的对子_
		if (n == 2)
			break;

		///匹配成功_
		for (int k = 0; k < requir_cnts; k++)
		{
			puke_box[dex[k]]->runAction(MoveBy::create(0.1, Vec2(0, 50)));
			puke_box[dex[k]]->isUp = !(puke_box[dex[k]]->isUp);
		}

		return re_point;
	}

	///判断飞机的逻辑_
	if (land_logic == CT_SINGLE_FLY || land_logic == CT_DOUBLE_FLY)
	{
		int _re_cnts = 0;

		if (land_logic == CT_SINGLE_FLY)
		{
			_re_cnts = (requir_cnts / 4) * 3;
		}
		else if (land_logic == CT_DOUBLE_FLY)
		{
			_re_cnts = (requir_cnts / 5) * 3;
		}

		for (int i = cnts - 1; i > -1; i--)
		{
			if (convertLandPoint(puke_box[i]->int_puker_point) > 14 || cnts < requir_cnts)///点数在A以上或者手上扑克数量少于连牌数量_
				break;

			if (convertLandPoint(puke_box[i]->int_puker_point) > int_cur_point)
			{
				if (i == 0 || i == 1)///当前扑克前面还有两张牌_
					break; ///当前扑克为前两张_

				if (i + _re_cnts - 2 > cnts)
				{
					continue;;///尾部的扑克数量少于连牌数量_
				}
				else
				{
					if (convertLandPoint(puke_box[i]->int_puker_point) == convertLandPoint(puke_box[i - 2]->int_puker_point))
					{
						int dex[20];///存储符合条件的数组下标_
						int _cur_point = convertLandPoint(puke_box[i]->int_puker_point) - 1;///第二个节点的地主点数_
						int x = 0;///dex数组的下标_
						dex[x] = i - 2;
						dex[x + 1] = i - 1;
						dex[x + 2] = i;
						x += 3;

						for (int j = i + 1; j < cnts - 2; j += 3)///后面需要留三张牌_
						{
							if (convertLandPoint(puke_box[j]->int_puker_point) == _cur_point && convertLandPoint(puke_box[j + 2]->int_puker_point) == _cur_point)
							{
								_cur_point--;
								dex[x] = j;
								dex[x + 1] = j + 1;
								dex[x + 2] = j + 2;
								x += 3;
								if (x == _re_cnts)///匹配成功结束_
									break;
								if (convertLandPoint(puke_box[j + 3]->int_puker_point) == _cur_point + 1)
									j++;

							}
						}

						if (x == _re_cnts)///匹配成功_
						{
							for (int k = 0; k < _re_cnts; k++)
							{
								puke_box[dex[k]]->runAction(MoveBy::create(0.1, Vec2(0, 50)));
								puke_box[dex[k]]->isUp = !(puke_box[dex[k]]->isUp);
							}

							return convertLandPoint(puke_box[i]->int_puker_point);
						}

					}
				}
			}
		}
	}

	///判断炸弹的逻辑_
	if (land_logic == CT_BOMB_CARD)
	{
		for (int i = cnts - 1; i > -1; i--)
		{
			if (i - 3 < 0 || int_cur_point >= 50)
				break;///扑克数量不够一个炸弹_
			else if (convertLandPoint(puke_box[i]->int_puker_point) > int_cur_point)
			{
				if (convertLandPoint(puke_box[i]->int_puker_point) == convertLandPoint(puke_box[i - 3]->int_puker_point))
				{
					for (int j = i; j > i - 4; j--)
					{
						puke_box[j]->runAction(MoveBy::create(0.1, Vec2(0, 50)));
						puke_box[j]->isUp = !(puke_box[j]->isUp);
					}

					return convertLandPoint(puke_box[i]->int_puker_point);
				}
			}
		}
	}

	///检测炸弹_
	for (int i = cnts - 1; i > -1; i--)
	{
		///如果本身就是炸弹类型就直接跳过_
		if (land_logic == CT_BOMB_CARD)
			break;

		if (i - 3 < 0 ||  int_cur_point >= 50)
			break;///扑克数量不够一个炸弹_
		else if (convertLandPoint(puke_box[i]->int_puker_point) == convertLandPoint(puke_box[i - 3]->int_puker_point))
		{
			for (int j = i; j > i - 4; j--)
			{
				puke_box[j]->runAction(MoveBy::create(0.1, Vec2(0, 50)));
				puke_box[j]->isUp = !(puke_box[j]->isUp);
			}
			b_loop = true;
			return 50 + convertLandPoint(puke_box[i]->int_puker_point);
		}
	}

	///检测火箭_
	if (cnts >= 2 && int_cur_point != 100)
	{
		if (puke_box[0]->int_puker_point > 51 && puke_box[1]->int_puker_point > 51)
		{
			puke_box[0]->runAction(MoveBy::create(0.1, Vec2(0, 50)));
			puke_box[0]->isUp = !(puke_box[0]->isUp);
			puke_box[1]->runAction(MoveBy::create(0.1, Vec2(0, 50)));
			puke_box[1]->isUp = !(puke_box[1]->isUp);
			b_loop = true;
			return 100;
		}
	}

	if (land_logic == CT_SINGLE)///继续判断有无大于上家的单牌_
	{
		for (int i = cnts - 1; i > - 1; i--)
		{
			if (convertLandPoint(puke_box[i]->int_puker_point) > int_cur_point)
			{
				puke_box[i]->runAction(MoveBy::create(0.1, Vec2(0, 50)));
				puke_box[i]->isUp = !(puke_box[i]->isUp);
				return int_cur_point;
			}
		}
	}

	if (land_logic == CT_DOUBLE)///继续判断有无大于上家的对牌_
	{
		for (int i = cnts - 1; i > -1; i--)
		{
			if (i == 0)
				break;
			if (convertLandPoint(puke_box[i]->int_puker_point) > int_cur_point)
			{
				if (convertLandPoint(puke_box[i]->int_puker_point) == convertLandPoint(puke_box[i - 1]->int_puker_point))
				{
					puke_box[i]->runAction(MoveBy::create(0.1, Vec2(0, 50)));
					puke_box[i]->isUp = !(puke_box[i]->isUp);
					puke_box[i - 1]->runAction(MoveBy::create(0.1, Vec2(0, 50)));
					puke_box[i - 1]->isUp = !(puke_box[i - 1]->isUp);

					return int_cur_point;
				}
				
			}
		}

	}

	return -1;
}

///调整扑克顺序_
void LogicLand::resetThePoint(int* land_point, int cnts)
{
	for (int i = 0; i < cnts; i++)
	{
		int max = land_point[i];
		for (int j = i + 1; j < cnts; j++)
		{
			if (max < land_point[j])
			{
				int x = max;
				max = land_point[j];
				land_point[j] = x;
			}
		}
		land_point[i] = max;

	}
}

///转换成服务端扑克点数_
byte LogicLand::convertServerPoint(int my_point)
{
	if (my_point < Ox11)
		return 0x01 + my_point % 13;
	else if (my_point < Ox21)
		return 0x11 + my_point % 13;
	else if (my_point < Ox31)
		return 0x21 + my_point % 13;
	else if (my_point < Ox4E)
		return 0x31 + my_point % 13;
	else if (my_point == Ox4E)
		return 0x4E;
	else if (my_point == Ox4F)
		return 0x4F;

}

///转换成地主点数_
int LogicLand::convertLandPoint(int point)
{
	if (point < 52)
	{
		if (point % 13 == 0)
		{
			return 13 + 1;///A
		}
		else if (point % 13 == 1)
		{
			return 14 + 1;///2
		}
		else
		{
			return point % 13 + 1;
		}
	}
	else
	{
		if (point == Ox4E)
		{
			return 15 + 1;
		}
		else if (point == Ox4F)
		{
			return 16 + 1;
		}
	}

}

///获取斗地主牌型逻辑_
int LogicLand::getLandLogic(int land_point[20], int cnts)
{
	switch (cnts)
	{
	case 0:
								return CT_ERROR;
	case 1:
								return CT_SINGLE;///单牌_
	case 2:
		if (land_point[0] == land_point[1])
								return CT_DOUBLE;///对子_
		else if (land_point[0] > 15 && land_point[1] > 15)
								return CT_MISSILE_CARD;///火箭_
		else
								return CT_ERROR;
	case 3:
		if (land_point[2] == land_point[0] && land_point[1] == land_point[0])
								return CT_THREE;///三条_
		else
								return CT_ERROR;
	case 4:
		if (land_point[3] == land_point[0] && land_point[2] == land_point[0] && land_point[1] == land_point[0])
								return CT_BOMB_CARD;///炸弹_
		else
								return getThree_One(land_point);///三带一或错误类型_
	case 5:
								return getFivePuker(land_point);
	case 6:
								return getSixPuker(land_point);
	case 8:
								return getEightPuker(land_point);
	default:
			if (getSingleLine(land_point, cnts))
								return CT_SINGLE_LINE;///单连_
			if (getDoubleLine(land_point, cnts))
								return CT_DOUBLE_LINE;///对连_
			if (getThreeLine(land_point, cnts))
								return CT_THREE_LINE;///三连_
			if (getSingleFly(land_point, cnts))
								return CT_SINGLE_FLY;///飞机带单牌_
			if (getDoubleFly(land_point, cnts))
								return CT_DOUBLE_FLY;///飞机带对子_
			
								return CT_ERROR;
	}
}

///是否存在双顺_
bool LogicLand::isExistLineDouble(Puker* puke_box[20], int cnts)
{
	Vector<Puker*> puke_point;///保存选中扑克_
	std::vector<int> vec;///保存要被降下的扑克序号_

	for (int i = 0; i < cnts; i++)
	{
		if (puke_box[i]->isUp)
		{
			puke_point.pushBack(puke_box[i]);
		}
	}

	if (puke_point.size() < 6)
		return false;

	for (int i = 0; i < puke_point.size() - 1; i++)
	{
		if (convertLandPoint(puke_point.at(i)->int_puker_point) > 14)
		{
			vec.push_back(i);
			continue;
		}

		while (convertLandPoint(puke_point.at(i)->int_puker_point) == convertLandPoint(puke_point.at(i + 1)->int_puker_point))
		{
			vec.push_back(i);
			i++;
			if (i == puke_point.size() - 1)
				break;
		}

		if (i == puke_point.size() - 1)
			break;

		if (convertLandPoint(puke_point.at(i)->int_puker_point) - 1 != convertLandPoint(puke_point.at(i + 1)->int_puker_point))
			return false;
	}

	if (puke_point.size() - vec.size() < 5)
		return false;

	for (int i = 0; i < vec.size(); i++)
	{
		puke_point.at(vec[i])->runAction(MoveBy::create(0, Vec2(0, -50)));
		puke_point.at(vec[i])->isUp = false;
	}

	return true;
}

///是否存在单顺_
bool LogicLand::isExistLineSingle(Puker* puke_box[20], int cnts)
{
	Vector<Puker*> puke_point;///保存选中扑克_
	std::vector<int> vec;///保存要被降下的扑克序号_

	for (int i = 0; i < cnts; i++)
	{
		if (puke_box[i]->isUp)
		{
			puke_point.pushBack(puke_box[i]);
		}
	}

	if (puke_point.size() < 5)
		return false;

	for (int i = 0; i < puke_point.size() - 1; i++)
	{
		if (convertLandPoint(puke_point.at(i)->int_puker_point) > 14)
		{
			vec.push_back(i);
			continue;
		}

		while(convertLandPoint(puke_point.at(i)->int_puker_point) == convertLandPoint(puke_point.at(i + 1)->int_puker_point))
		{
			vec.push_back(i);
			i++;
			if (i == puke_point.size() - 1)
				break;
		}

		if (i == puke_point.size() - 1)
			break;

		if (convertLandPoint(puke_point.at(i)->int_puker_point) - 1 != convertLandPoint(puke_point.at(i + 1)->int_puker_point))
			return false;
	}

	if (puke_point.size() - vec.size() < 5)
		return false;

	for (int i = 0; i < vec.size(); i++)
	{
		puke_point.at(vec[i])->runAction(MoveBy::create(0, Vec2(0, -50)));
		puke_point.at(vec[i])->isUp = false;
	}
	return true;
}

///判断是否是三带一_
int LogicLand::getThree_One(int land_point[20])
{
	resetThePoint(land_point, 4);

	bool b1 = land_point[0] == land_point[1] && land_point[2] == land_point[1];
	bool b2 = land_point[2] == land_point[1] && land_point[3] == land_point[1];

	if (b1 || b2)
								return CT_THREE_TAKE_ONE;
	else
								return CT_ERROR;

}

///判断是否为单连_
bool LogicLand::getSingleLine(int land_point[20], int cnts)
{
	resetThePoint(land_point, cnts);

	if (land_point[0] < 15)
	{
		for (int i = 0; i < cnts - 1; i++)
		{
			if (land_point[i] - 1 != land_point[i + 1])
				return false;
		}
	}
	else
	{
		return false;
	}

	return true;

}

///判断是否为对连_
bool LogicLand::getDoubleLine(int land_point[20], int cnts)
{
	if (cnts % 2)
		return false;
	resetThePoint(land_point, cnts);

	if (land_point[0] < 15)
	{
		for (int i = 0; i < cnts - 1; i += 2)
		{
			if (land_point[i] != land_point[i + 1])
				return false;

			if (i < cnts - 2)
			{
				if (land_point[i] - 1 != land_point[i + 2])
					return false;
				
			}
		}
	}
	else
	{
		return false;
	}
	
	return true;
}

///判断是否为三连_
bool LogicLand::getThreeLine(int land_point[20], int cnts)
{
	if (cnts % 3)
		return false;
	resetThePoint(land_point, cnts);

	if (land_point[0] < 15)
	{
		for (int i = 0; i < cnts - 2; i += 3)
		{
			if (land_point[i] != land_point[i + 1] || land_point[i] != land_point[i + 2])
				return false;

			if (i < cnts - 3)
			{
				if (land_point[i] - 1 != land_point[i + 3])
					return false;

			}
		}
	}
	else
	{
		return false;
	}

	return true;
}

///判断是否为飞机带单牌_
bool LogicLand::getSingleFly(int land_point[20], int cnts)
{
	int n = 0;
	int z = cnts / 4;

	if (cnts % 4)
		return false;

	while (land_point[0] != land_point[2])
	{
		n++;
		if (n > z)
			return false;

		///把盒子往前推_
		int temp = land_point[0];
		for (int i = 0; i < cnts - 1; i++)
		{
			land_point[i] = land_point[i + 1];

		}
		land_point[cnts - 1] = temp;
	}

	///判断扑克前面是不是三连_
	if (!getThreeLine(land_point, z * 3))
	{
		return false;
	}

	return true;
}

///判断是否为飞机带对子_
bool LogicLand::getDoubleFly(int land_point[20], int cnts)
{
	int n = 0;
	int z = cnts / 5;

	if (cnts % 5)
		return false;

	while (land_point[0] != land_point[2])
	{
		if (land_point[0] != land_point[1])
			return false;

		n++;
		if (n > z)
			return false;

		///把盒子往前推2格_
		int temp  = land_point[0];
		int temp_ = land_point[1];

		for (int i = 0; i < cnts - 2; i++)
		{
			land_point[i] = land_point[i + 2];

		}
		land_point[cnts - 1] = temp;
		land_point[cnts - 2] = temp_;
	}

	///判断扑克前面是不是三连_
	if (!getThreeLine(land_point, z * 3))
	{
		return false;
	}

	///判断扑克尾部是不是对子_
	for (int i = cnts - z * 3; i < cnts - 3; i += 2)
	{
		if (land_point[i] != land_point[i + 1])
			return false;
	}

	return true;
}

///判断五张牌_
int LogicLand::getFivePuker(int land_point[20])
{
	resetThePoint(land_point, 5);

	if (getSingleLine(land_point, 5))
		return CT_SINGLE_LINE;///单连_

	if (land_point[0] < 16)
	{
		bool b1 = land_point[0] == land_point[2];
		bool b2 = land_point[1] == land_point[2];
		bool b3 = land_point[3] == land_point[2];
		bool b4 = land_point[4] == land_point[2];
		bool b5 = land_point[0] == land_point[1];
		bool b6 = land_point[4] == land_point[3];

		bool b7 = b1 && b2 && b6;
		bool b8 = b3 && b4 && b5;
		if (b7 || b8)
		{
			return CT_THREE_TAKE_TWO;///三带二_
		}

	}
	return CT_ERROR;
}

///判断六张牌_
int LogicLand::getSixPuker(int land_point[20])
{
	resetThePoint(land_point, 6);

	if (getSingleLine(land_point, 6))
		return CT_SINGLE_LINE;///单连_

	if (getDoubleLine(land_point, 6))
		return CT_DOUBLE_LINE;///对连_

	if (getThreeLine(land_point, 6))
		return CT_THREE_LINE;///三连_

	if (land_point[1] < 16)
	{
		bool b1 = land_point[0] == land_point[3];
		bool b2 = land_point[1] == land_point[3];
		bool b3 = land_point[2] == land_point[3];
		bool b4 = land_point[4] == land_point[3];
		bool b5 = land_point[5] == land_point[3];

		bool b6 = b1 && b2 && b3;
		bool b7 = b2 && b3 && b4;
		bool b8 = b3 && b4 && b5;

		if (b6 || b7 || b8)
		{
			return CT_FOUR_TAKE_ONE;///四带两单_
		}

	}

	return CT_ERROR;
}

///判断八张牌_
int LogicLand::getEightPuker(int land_point[20])
{
	resetThePoint(land_point, 8);

	if (getSingleLine(land_point, 8))
						return CT_SINGLE_LINE;///单连_

	if (getDoubleLine(land_point, 8))
						return CT_DOUBLE_LINE;///对连_

	if (getSingleFly(land_point, 8))
						return CT_SINGLE_FLY;///飞机带单牌_

	if (land_point[0] < 15)
	{
		bool b1 = land_point[1] == land_point[0];
		bool b2 = land_point[2] == land_point[0];
		bool b3 = land_point[3] == land_point[0];

		bool b4 = land_point[2] == land_point[5];
		bool b5 = land_point[3] == land_point[5];
		bool b6 = land_point[4] == land_point[5];

		bool b7 = land_point[4] == land_point[7];
		bool b8 = land_point[5] == land_point[7];
		bool b9 = land_point[6] == land_point[7];

		bool b10 = land_point[2] = land_point[3];
		bool b11 = b1 && b2 && b3 && b6 && b9;
		bool b12 = b4 && b5 && b6 && b1 && b9;
		bool b13 = b7 && b8 && b9 && b1 && b10;

		if (b11 || b12 || b13)
		{
			return CT_FOUR_TAKE_TWO;///四带两对_
		}

	}

	return CT_ERROR;
}