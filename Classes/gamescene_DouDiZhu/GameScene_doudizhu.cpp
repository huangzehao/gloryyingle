#include "Tools/tools/MTNotification.h"
#include "common/KeybackLayer.h"
#include "Tools/tools/SimpleTools.h"
#include "Tools/ViewHeader.h"
#include "Tools/Manager/SoundManager.h"
#include "Tools/tools/gPlatform.h"
#include "Tools/tools/StringData.h"
#include "Kernel/kernel/game/IClientKernel.h"
#include "Tools/tools/StaticData.h"
#include "Kernel/kernel/server/IServerItemSink.h"
#include "Tools/Dialog/NewDialog.h"
#include "Platform/PFView/ServerScene/ServerScene.h"
#include "Platform/PFView/ModeScene/ModeScene.h"

#include "ClientKereneSink_doudizhu.h"
#include "cmd_doudizhu.h"
#include "GameScene_doudizhu.h"
#include "FrameLayer_doudizhu.h"

using namespace doudizhu;

static GameScene_doudizhu* __gGameScene_Land = 0;
bool isLandReconnectOnLoss = false;

GameScene_doudizhu::GameScene_doudizhu()
{
	__gGameScene_Land = this;
}

GameScene_doudizhu::~GameScene_doudizhu()
{
	__gGameScene_Land = nullptr;
	SoundManager::shared()->stopAllSound();
	PLAY_PLATFORM_BG_MUSIC
}

bool GameScene_doudizhu::is_Reconnect_on_loss()
{
	return isLandReconnectOnLoss;
}

void GameScene_doudizhu::func_Reconnect_on_loss(cocos2d::Ref * obj)
{
	IClientKernel* kernel = IClientKernel::get();
	if (kernel == nullptr)
	{
		G_NOTIFY_D("MODE_SELECTED", MTData::create(1));
		return;
	}

	isLandReconnectOnLoss = true;
	///断线重连_
	this->schedule(SEL_SCHEDULE(&GameScene_doudizhu::reconnect_on_loss), 1.5f);

}

void GameScene_doudizhu::reconnect_on_loss(float dt)
{
	static float total_time = 0;
	total_time += dt;

	bool isHaveNet = false;

#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID || CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
	isHaveNet = SimpleTools::obtainNetWorkState();
#endif

#if (CC_TARGET_PLATFORM == CC_PLATFORM_WIN32)
	if (total_time > 12)
		isHaveNet = true;
#endif
	if (total_time > 30)
	{
		this->unschedule(SEL_SCHEDULE(&GameScene_doudizhu::reconnect_on_loss));
		if (!isHaveNet)
		{
			///没有网络了_
			NewDialog::create(SSTRING("System_Tips_26"), NewDialog::AFFIRM, [=]()
			{
				if (IClientKernel::get())
					IClientKernel::get()->Intermit(GameExitCode_Normal);
			});
			isLandReconnectOnLoss = false;
			total_time = 0.0f;
			return;
		}
	}

	if (isHaveNet)
	{
		if (total_time <= 30.0f)
			this->unschedule(SEL_SCHEDULE(&GameScene_doudizhu::reconnect_on_loss));
		NewDialog::create(SSTRING("System_Tips_28"), NewDialog::NONEBUTTON, nullptr, nullptr, [=]()
		{
			ServerScene * tServer = dynamic_cast<ServerScene *>(ModeScene::create()->getChildByName("ServerScene"));
			if (tServer)
			{
				tServer->connectServer();
			}
			isLandReconnectOnLoss = false;
		});
		total_time = 0.0f;
		isLandReconnectOnLoss = true;
	}
	else
	{
		NewDialog::create(SSTRING("System_Tips_29"), NewDialog::NONEBUTTON);
	}

}

bool GameScene_doudizhu::init()
{
	frame_layer = FrameLayer_doudizhu::create();
	this->addChild(frame_layer);
	frame_layer->setCloseDialogInfo(this, callfuncN_selector(GameScene_doudizhu::closeCallback), SSTRING("back_to_room"), SSTRING("back_to_room_content"));

	mMessageLayer = MessageLayer::create();
	mMessageLayer->setName("mMessageLayer");
	addChild(mMessageLayer,100);

	auto kernel = IClientKernel::get();
	if (kernel)
	{
		kernel->SetChatSink(mMessageLayer);
		kernel->SetStringMessageSink(mMessageLayer);
	}

	G_NOTIFY_REG("EVENT_GAME_EXIT", GameScene_doudizhu::closeGameNotify);
	G_NOTIFY_REG("RECONNECT_ON_LOSS", GameScene_doudizhu::func_Reconnect_on_loss);

	this->setName("GameScene");
	this->setTag(KIND_ID);
	return true;
}

void GameScene_doudizhu::closeCallback(cocos2d::Node* obj)
{
	switch (obj->getTag())
	{
	case DLG_MB_OK:
	{
					  if (IClientKernel::get())
						  IClientKernel::get()->Intermit(GameExitCode_Normal);

	}
		break;
	}
}

void GameScene_doudizhu::closeGameNotify(cocos2d::Ref * ref)
{
	if (IClientKernel::get())
		IClientKernel::get()->Intermit(GameExitCode_Normal);
}

///初始化游戏基础数据_
void GameScene_doudizhu::initGameBaseData()
{
	DF::shared()->init(KIND_ID, GAME_PLAYER, VERSION_CLIENT, STATIC_DATA_STRING("appname"), platformGetPlatform());
}

//场景动画完成_
void GameScene_doudizhu::onEnterTransitionDidFinish()
{
 	Scene::onEnterTransitionDidFinish();
	auto ik = IClientKernel::get();
	if (ik)
	{
		ik->SendGameOption();
	}

}

//场景动画开始_
void GameScene_doudizhu::onExitTransitionDidStart()
{
	Scene::onExitTransitionDidStart();
}

void GameScene_doudizhu::onKeybackClicked()
{
	popup(SSTRING("system_tips_title"), SSTRING("back_to_chair_select"), DLG_MB_OK | DLG_MB_CANCEL, 0, this, callfuncN_selector(GameScene_doudizhu::onBackToRoom));
}

void GameScene_doudizhu::onBackToRoom(Node* pNode)
{
	switch (pNode->getTag())
	{
		case DLG_MB_OK:
		{
						  if (IClientKernel::get())
							  IClientKernel::get()->Intermit(GameExitCode_Normal);
						  return;
		}
		break;
	}
}

bool GameScene_doudizhu::onTouchBegan(Touch *pTouch, Event *pEvent)
{
	IClientKernel* kernel = IClientKernel::get();

	if (kernel == 0)
		return false;
}

void GameScene_doudizhu::onTouchMoved(Touch *pTouch, Event *pEvent)
{
	IClientKernel* kernel = IClientKernel::get();

	if (kernel == 0)
		return;
}

void GameScene_doudizhu::onTouchEnded(Touch *pTouch, Event *pEvent)
{

}

///框架函数_
void GameScene_doudizhu::net_user_state_update(IClientUserItem *pIClientUserItem, byte user_state)
{
	///设置玩家准备状态_
	frame_layer->setOtherUserPrepare(pIClientUserItem, user_state);
}

///框架函数_
void GameScene_doudizhu::showUserInfo(IClientUserItem *pIClientUserItem, byte cbUserStatus)
{
	///设置玩家准备状态_
	frame_layer->setOtherUserPrepare(pIClientUserItem, cbUserStatus);
	

}

void GameScene_doudizhu::cleanUserInfo(IClientUserItem *pIClientUserItem)
{
	///清理其他玩家离开痕迹_
	frame_layer->cleanOtherUserPrepare(pIClientUserItem->GetChairID());
}

///传递接收到的服务端信息到游戏场景_free
void GameScene_doudizhu::transmitSetGameSceneFree(void * data, int dataSize)
{
	frame_layer->setGameSceneFree(data, dataSize);
}

///传递接收到的服务端信息到游戏场景_ play
void GameScene_doudizhu::transmitSetGameScenePlay(void* data, int dataSize)
{
	frame_layer->setGameScenePlay(data, dataSize);
}

///传递接收到的服务端信息到游戏场景_start
void GameScene_doudizhu::transmitSetGameSceneStart(void * data, int dataSize)
{
	frame_layer->setGameSceneStart(data, dataSize);
}

///传递接收到的服务端信息到游戏场景_ call
void GameScene_doudizhu::transmitSetGameSceneCall(void * data, int dataSize)
{
	frame_layer->setGameSceneCall(data, dataSize);
}

///传递接收到的服务端信息到游戏场景_ banker
void GameScene_doudizhu::transmitSetGameSceneBanker(void * data, int dataSize)
{
	frame_layer->setGameSceneBanker(data, dataSize);
}

///传递接收到的服务端信息到游戏场景_ outCards
void GameScene_doudizhu::transmitSetGameSceneOutCards(void* data, int dataSize)
{
	frame_layer->setGameSceneOutCards(data, dataSize);
}

///传递接收到的服务端信息到游戏场景_ PassCards
void GameScene_doudizhu::transmitSetGameScenePassCards(void* data, int dataSize)
{
	frame_layer->setGameScenePassCards(data, dataSize);
}

///传递接收到的服务端信息到游戏场景_ gameOver
void GameScene_doudizhu::transmitSetGameSceneOver(void* data, int dataSize)
{
	frame_layer->setGameSceneOver(data, dataSize);
}
void GameScene_doudizhu::ExitGameTiming()
{
	NewDialog::create(SSTRING("System_Tips_36"), NewDialog::NONEBUTTON);
	this->scheduleOnce(SEL_SCHEDULE(&GameScene_doudizhu::CloseKernelExitGame), 2.5f);
}

void GameScene_doudizhu::CloseKernelExitGame(float dt)
{
	IClientKernel::destory();
}

