#ifndef PUKER_H_
#define PUKER_H_

#include "cocos2d.h"
#include "Common/TouchSprite.h"
USING_NS_CC;

enum Puker_point
{
	Ox01, Ox02, Ox03, Ox04, Ox05, Ox06, Ox07, Ox08, Ox09, Ox00, Ox0J, Ox0Q, Ox0K,	//方块_ A - K
	Ox11, Ox12, Ox13, Ox14, Ox15, Ox16, Ox17, Ox18, Ox19, Ox10, Ox1J, Ox1Q, Ox1K,	//梅花_ A - K
	Ox21, Ox22, Ox23, Ox24, Ox25, Ox26, Ox27, Ox28, Ox29, Ox20, Ox2J, Ox2Q, Ox2K,	//红桃_ A - K
	Ox31, Ox32, Ox33, Ox34, Ox35, Ox36, Ox37, Ox38, Ox39, Ox30, Ox3J, Ox3Q, Ox3K,	//黑桃_ A - K
	Ox4E, Ox4F,																	    //大小王_
	Ox5G, Ox5H, Ox5I															    //扑克背面_
};

class Puker : public Sprite
{
public:
	static Puker* create(int point)
	{
		Puker *pRet = new Puker();
		if (pRet && pRet->init(point))
		{
			pRet->autorelease();
			return pRet;
		}
		else
		{
			delete pRet;
			pRet = NULL;
			return NULL;
		}
	}
	bool init(int point);

private:
	///创建扑克_
	void createPuker(int point);

	///拼扑克_fd1 - 花色_， fd2 - 是否10以上_
	void shapeThePuker(int point, int up);

private:
	Sprite* spt_bg;///扑克背景_
	Sprite* spt_point;///扑克点数_
	Sprite* spt_lab;///扑克花色标识上_
	Sprite* spt_mid;///扑克花色标识中_
	Sprite* spt_big;///大王_
	Sprite* spt_small;///小王_
	Sprite* spt_back;///背面_
	Sprite* spt_turn_1;///背面翻转1
	Sprite* spt_turn_2;///背面翻转2

public:
	bool isUp;///扑克是否处于弹出状态_
	int int_puker_point;///扑克点数_
	///获取盒子范围_
	Rect getMyBoundingBox();
};








#endif