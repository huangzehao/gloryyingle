#ifndef __VIEWS_H__
#define __VIEWS_H__

#include "cocos2d.h"
#include "Tools/Dialog/Dialog.h"
#include "Platform/PlatformHeader.h"
#include "ViewHeader.h"
USING_NS_CC;

void popup(const char* title, const char* content, int iButtonType, float delayShow, cocos2d::Ref* pTarget, cocos2d::SEL_CallFuncN func, cocos2d::Node* parent)
{
	using namespace cocos2d;

	Size winSize = Director::getInstance()->getWinSize();

	CCSpriteFrameCache::sharedSpriteFrameCache()->addSpriteFramesWithFile("LobbyResources/GameRest/View.plist");

	SpriteFrameCache* cache = CCSpriteFrameCache::getInstance();
	SpriteFrame* frame = cache->spriteFrameByName("img_bg.png");
	Size size = frame->getRect().size;
	Rect insetRect = Rect(25, 25, size.width - 25 * 2, size.height - 25 - 25);

	// 定义一个弹出层，传入一张背景图
	Dialog* dlg = Dialog::create(frame, insetRect);
	dlg->setContentSize(cocos2d::Size(500, 300));
	//dlg->setTitle(title);
	dlg->setTitle("");
	dlg->setContentText(content, 20, 20, 20);
	dlg->setCallbackFunc(pTarget, func);
	dlg->setPosition(cocos2d::Vec2(winSize.width / 2, winSize.height / 2));


	if ( iButtonType == 0 )
	{
		Sprite* noraml = Sprite::createWithSpriteFrame(cache->spriteFrameByName("dlg_queding_1.png"));
		Sprite* selected = Sprite::createWithSpriteFrame(cache->spriteFrameByName("dlg_queding_2.png"));
		dlg->addButton(noraml, selected, "", DLG_MB_OK);
	}

	if (iButtonType&DLG_MB_OK)
	{
		Sprite* noraml = Sprite::createWithSpriteFrame(cache->spriteFrameByName("dlg_queding_1.png"));
		Sprite* selected = Sprite::createWithSpriteFrame(cache->spriteFrameByName("dlg_queding_2.png"));
		noraml->setPositionX(noraml->getPositionX() - 35);
		selected->setPositionX(selected->getPositionX() - 35);
		dlg->addButton(noraml, selected, "", DLG_MB_OK);
	}

	if (iButtonType&DLG_MB_YES)
	{
		Sprite* noraml = Sprite::createWithSpriteFrame(cache->spriteFrameByName("dlg_queding_1.png"));
		Sprite* selected = Sprite::createWithSpriteFrame(cache->spriteFrameByName("dlg_queding_2.png"));
		dlg->addButton(noraml, selected, "", DLG_MB_YES);
	}

	if (iButtonType&DLG_MB_NO)
	{
		Sprite* noraml = Sprite::createWithSpriteFrame(cache->spriteFrameByName("dlg_quxiao_1.png"));
		Sprite* selected = Sprite::createWithSpriteFrame(cache->spriteFrameByName("dlg_quxiao_2.png"));
		dlg->addButton(noraml, selected, "", DLG_MB_NO);
	}

	if (iButtonType&DLG_MB_CANCEL)
	{
		Sprite* noraml = Sprite::createWithSpriteFrame(cache->spriteFrameByName("dlg_quxiao_1.png"));
		Sprite* selected = Sprite::createWithSpriteFrame(cache->spriteFrameByName("dlg_quxiao_2.png"));
		noraml->setPositionX(noraml->getPositionX() + 35);
		selected->setPositionX(selected->getPositionX() + 35);
		dlg->addButton(noraml, selected, "", DLG_MB_CANCEL);
	}


	dlg->doModal(parent);

	// 弹出效果
	dlg->setScale(0, 0);
	dlg->setVisible(false);
	Action* act = Sequence::create(
		DelayTime::create(delayShow),
		Show::create(),
		ScaleTo::create(0.06f, 0.9f),
		ScaleTo::create(0.08f, 1.2f),
		ScaleTo::create(0.08f, 1.5f), 0);
	dlg->runAction(act);
}


void open_url(const char* url)
{
#if (CC_TARGET_PLATFORM==CC_PLATFORM_WIN32)
	ShellExecuteA(NULL, NULL, url, NULL, NULL, SW_SHOWNORMAL);
#elif (CC_TARGET_PLATFORM==CC_PLATFORM_IOS)
	//NSString* ns_str = [NSString stringWithUTF8String:url];
	//[[NSWorkspace sharedWorkspace] openURL: [NSURL URLWithString:ns_str]]
#elif (CC_TARGET_PLATFORM==CC_PLATFORM_ANDROID)

#else
#endif
}

// 格式化货币
std::string FormatMoneyFrom(const std::string& sIn)
{
	if (sIn.empty())
		return "";

	std::string sRet;
	int iflag = sIn.find_last_of('.');

	std::string s1 = iflag == -1 ? sIn : sIn.substr(0, iflag);
	std::string s2 = iflag == -1 ? "" : sIn.substr(iflag);
	int len = s1.size();

	int iFlag = 0;
	if (s1[0] == '-')
		iFlag = 1;

	int c = 0;

	for (int i = len - 1; i >= iFlag; --i)
	{
		sRet.push_back(s1[i]);
		++c;

		if (c >= 3 && i != iFlag)
		{
			sRet.push_back(',');
			c = 0;
		}
	}

	if (sRet.empty() && !s2.empty())
		sRet.push_back('0');

	if (iFlag == 1)
		sRet.push_back('-');

	//反转
	std::reverse(sRet.begin(), sRet.end());

	//只有一个点
	if (s2.size() == 1)
		s2.clear();

	return sRet + s2;
}


const char* FormatTime(int time)
{
	static std::string str;

	int iDay = time / 86400;
	time -= iDay * 86400;
	int iHour = time / 3600;
	time -= iHour * 3600;
	int iMin = time / 60;
	time -= iMin * 60;

	str.resize(128, '\0');
	sprintf(&str[0], "%02d:%02d:%02d", iHour, iMin, time);
	return str.c_str();
}


#endif /* __APPMACROS_H__ */
