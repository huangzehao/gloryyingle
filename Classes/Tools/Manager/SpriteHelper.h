#ifndef _SpriteHelper_H_
#define _SpriteHelper_H_
#include "cocos2d.h"


class SpriteHelper
{
public:
	static bool cacheAnimations(std::string rootName);
	static bool cacheAnimation(const char* plist);
	static void removeAnimation(const char* plist);
	static cocos2d::CCAnimate* createAnimate(const char* name);
	static cocos2d::CCAnimate* createEffectAnimate(const char* name, float time);
	static cocos2d::CCAnimate* createBirdAnimate(int type);
	static cocos2d::CCAnimate* createBirdDeadAnimate(int type);
	static cocos2d::CCAnimate* createJdbyBirdDeadAnimate(int type);
	static cocos2d::CCAnimate* createAnimateManager(const char *frameName, int beginNum, int endNum, float delay, bool adverse = false, int count = 1);
	static cocos2d::CCAnimate* createHeadAnimate(const char *frameName, int beginNum, int endNum, float delay);

}; 
#endif 