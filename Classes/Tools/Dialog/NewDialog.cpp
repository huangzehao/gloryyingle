#include "NewDialog.h"

using namespace ui;


NewDialog::NewDialog() : affirmCallBack(nullptr)
	, cancelCallBack(nullptr)
	, noneCallBack(nullptr)
	, mPTarget(nullptr)
	, mClickNode(nullptr)
{
}


NewDialog::~NewDialog()
{
	mClickNode = nullptr;
}

bool NewDialog::create(std::string txtString, DIALOGTYPE dialogType, std::function<void()> affirmCallBack /*= nullptr*/, std::function<void()> cancelCallBack /*= nullptr*/, std::function<void()> noneCallBack)
{
	NewDialog* dlg = new NewDialog();
	if (dlg && dlg->initDialog(txtString, dialogType, affirmCallBack, cancelCallBack, noneCallBack))
	{
		dlg->autorelease();
		
		Scene * now_Scene = Director::getInstance()->getRunningScene();
		if (now_Scene != nullptr) now_Scene->addChild(dlg, 0XFFFF);
		else
			return false;
		return true;
	}
	delete dlg;
	return false;
}

bool NewDialog::createOnce(std::string txtString, DIALOGTYPE dialogType, std::function<void()> affirmCallBack /*= nullptr*/, std::function<void()> cancelCallBack /*= nullptr*/, std::function<void()> noneCallBack)
{
	NewDialog* dlg = new NewDialog();
	if (dlg && dlg->initDialog(txtString, dialogType, affirmCallBack, cancelCallBack, noneCallBack))
	{
		dlg->autorelease();

		Scene * now_Scene = Director::getInstance()->getRunningScene();
		if (now_Scene != nullptr && now_Scene->getChildByTag(1888) == NULL)
			now_Scene->addChild(dlg, 0XFFFF, 1888);
		else
			return false;
		return true;
	}
	delete dlg;
	return false;
}

bool NewDialog::create(std::string titleString, std::string contentString, DIALOGTYPE dialogType, std::function<void()> affirmCallBack /*= nullptr*/, std::function<void()> cancelCallBack /*= nullptr*/, std::function<void()> noneCallBack /*= nullptr*/)
{
	NewDialog* dlg = new NewDialog();
	if (dlg && dlg->initDialog(titleString,contentString, dialogType, affirmCallBack, cancelCallBack, noneCallBack))
	{
		dlg->autorelease();

		Scene * now_Scene = Director::getInstance()->getRunningScene();
		if (now_Scene != nullptr) now_Scene->addChild(dlg, 0XFFFF);
		else
			return false;
		return true;
	}
	delete dlg;
	return false;
}

bool NewDialog::create(std::string titleString, std::string contentString, DIALOGTYPE dialogType, cocos2d::Ref* pTarget, cocos2d::SEL_CallFuncN func, cocos2d::Node * node)
{
	NewDialog* dlg = new NewDialog();
	if (dlg && dlg->initDialog(titleString, contentString, dialogType, pTarget, func, node))
	{
		dlg->autorelease();

		Scene * now_Scene = Director::getInstance()->getRunningScene();
		if (now_Scene != nullptr) now_Scene->addChild(dlg, 0XFFFF);
		else
			return false;
		return true;
	}
	delete dlg;
	return false;
}

NewDialog * NewDialog::createNoLoadScene(std::string txtString, DIALOGTYPE dialogType, bool loadScene, std::function<void()> affirmCallBack /*= nullptr*/, std::function<void()> cancelCallBack /*= nullptr*/, std::function<void()> noneCallBack /*= nullptr*/)
{
	NewDialog* dlg = new NewDialog();
	if (dlg && dlg->initDialog(txtString, dialogType, affirmCallBack, cancelCallBack, noneCallBack))
	{
		dlg->autorelease();

		if (loadScene)
		{
			Scene * now_Scene = Director::getInstance()->getRunningScene();
			if (now_Scene != nullptr) now_Scene->addChild(dlg);
		}
		return dlg;
	}
	delete dlg;
	return nullptr;
}

NewDialog * NewDialog::createNoLoadScene(std::string titleString, std::string txtString, DIALOGTYPE dialogType, bool loadScene, cocos2d::Ref* pTarget, cocos2d::SEL_CallFuncN func, cocos2d::Node * node)
{
	NewDialog* dlg = new NewDialog();
	if (dlg && dlg->initDialog(titleString, txtString, dialogType, pTarget, func, node))
	{
		dlg->autorelease();

		if (loadScene)
		{
			Scene * now_Scene = Director::getInstance()->getRunningScene();
			if (now_Scene != nullptr) now_Scene->addChild(dlg);
		}
		return dlg;
	}
	delete dlg;
	return nullptr;
}

bool NewDialog::initDialog(std::string txtString, DIALOGTYPE dialogType, std::function<void()> affirmCallBack_ /*= nullptr*/, std::function<void()> cancelCallBack_ /*= nullptr*/, std::function<void()> noneCallBack_)
{
	return initDialog("", txtString, dialogType, affirmCallBack_, cancelCallBack_, noneCallBack_);
}

bool NewDialog::initDialog(std::string titleString, std::string txtString, DIALOGTYPE dialogType, std::function<void()> affirmCallBack_ /*= nullptr*/, std::function<void()> cancelCallBack_ /*= nullptr*/, std::function<void()> noneCallBack_ /*= nullptr*/)
{
	do
	{
		CC_BREAK_IF(!BaseLayer::initWithJsonFile("LobbyResources/TipsLayer.json"));

		ImageView * Image_bg = dynamic_cast<ImageView *>(m_root_widget->getChildByName("Image_bg"));

		Text * lab_titile = dynamic_cast<Text *>(Image_bg->getChildByName("lab_titile"));
		CCASSERT(lab_titile, "");
		lab_titile->setString(titleString);

		Text * lab_txt = dynamic_cast<Text *>(Image_bg->getChildByName("lab_txt"));
		CCASSERT(lab_txt, "");
		lab_txt->setString(txtString);

		Button * btn_cancel = dynamic_cast<Button *>(Image_bg->getChildByName("btn_cancel"));
		btn_cancel->addTouchEventListener(this, SEL_TouchEvent(&NewDialog::cancelButtonTouch));
		CCASSERT(btn_cancel, "");
		btn_cancel->setVisible(false);
		Button * btn_affirm = dynamic_cast<Button *>(Image_bg->getChildByName("btn_affirm"));
		btn_affirm->addTouchEventListener(this, SEL_TouchEvent(&NewDialog::affirmButtonTouch));
		CCASSERT(btn_affirm, "");
		btn_affirm->setVisible(false);
		switch (dialogType)
		{
		case NewDialog::AFFIRMANDCANCEL:
			btn_cancel->setVisible(true);
		case NewDialog::AFFIRM:
			btn_affirm->setVisible(true);
			break;
		case NewDialog::NONEBUTTON:
		{
									  CCActionInterval * move = CCFadeOut::create(1.5f);
									  CCCallFunc * funcall = CCCallFunc::create(this, callfunc_selector(NewDialog::noneCallBackFunc));
									  CCFiniteTimeAction * seq = CCSequence::create(move, funcall, NULL);
									  m_root_widget->runAction(seq);
									  break;
		}

		default:
			break;

		}

		affirmCallBack = affirmCallBack_;
		cancelCallBack = cancelCallBack_;
		noneCallBack = noneCallBack_;
		return true;
	} while (0);
	return false;
}

bool NewDialog::initDialog(std::string titleString, std::string txtString, DIALOGTYPE dialogType, cocos2d::Ref* pTarget, cocos2d::SEL_CallFuncN func, cocos2d::Node * node)
{
	do
	{
		ImageView * Image_bg = dynamic_cast<ImageView *>(m_root_widget->getChildByName("Image_bg"));

		Text * lab_titile = dynamic_cast<Text *>(Image_bg->getChildByName("lab_titile"));
		CCASSERT(lab_titile, "");
		lab_titile->setString(titleString);

		Text * lab_txt = dynamic_cast<Text *>(Image_bg->getChildByName("lab_txt"));
		CCASSERT(lab_txt, "");
		lab_txt->setString(txtString);

		Button * btn_cancel = dynamic_cast<Button *>(Image_bg->getChildByName("btn_cancel"));
		btn_cancel->addTouchEventListener(this, SEL_TouchEvent(&NewDialog::cancelButtonTouch));
		CCASSERT(btn_cancel, "");
		btn_cancel->setVisible(false);
		Button * btn_affirm = dynamic_cast<Button *>(Image_bg->getChildByName("btn_affirm"));
		btn_affirm->addTouchEventListener(this, SEL_TouchEvent(&NewDialog::affirmButtonTouch));
		CCASSERT(btn_affirm, "");
		btn_affirm->setVisible(false);
		switch (dialogType)
		{
		case NewDialog::AFFIRMANDCANCEL:
			btn_cancel->setVisible(true);
		case NewDialog::AFFIRM:
			btn_affirm->setVisible(true);
			break;
		case NewDialog::NONEBUTTON:
		{
									  CCActionInterval * move = CCFadeOut::create(1.5f);
									  CCCallFunc * funcall = CCCallFunc::create(this, callfunc_selector(NewDialog::noneCallBackFunc));
									  CCFiniteTimeAction * seq = CCSequence::create(move, funcall, NULL);
									  m_root_widget->runAction(seq);
									  break;
		}

		default:
			break;

		}

		mFunc = func;
		mPTarget = pTarget;
		mClickNode = node;
		return true;
	} while (0);
	return false;
}

void NewDialog::affirmButtonTouch(cocos2d::Ref * ref, ui::TouchEventType eventType_)
{
	if (eventType_ == TouchEventType::TOUCH_EVENT_ENDED)
	{
		if (affirmCallBack != nullptr) affirmCallBack();
		if (mPTarget != nullptr) (mPTarget->*mFunc)(mClickNode);
		if (this != nullptr) removeFromParent();
	}
}

void NewDialog::cancelButtonTouch(cocos2d::Ref * ref, ui::TouchEventType eventType_)
{
	if (eventType_ == TouchEventType::TOUCH_EVENT_ENDED)
	{
		if (cancelCallBack != nullptr) cancelCallBack();
		if (this != nullptr) removeFromParent();
	}
}

void NewDialog::noneCallBackFunc()
{
	if (noneCallBack != nullptr)
	{
		noneCallBack();
	}

	if (mPTarget != nullptr) (mPTarget->*mFunc)(mClickNode);

	if (this != nullptr)
	{
		this->removeFromParent();
	}
}
