
#ifndef _Dialog_h_
#define _Dialog_h_

#include "cocos2d.h"
#include "cocos-ext.h"

class Dialog: public cocos2d::Node
{
public:
	static Dialog* create(const char* file, cocos2d::Rect rect, cocos2d::Rect capInsets);
	static Dialog* create(const char* spriteFrameName, cocos2d::Rect capInsets);
	static Dialog* create(cocos2d::SpriteFrame* spriteFrame, cocos2d::Rect capInsets);
private:
	Dialog();
	~Dialog();
	bool initWithFile(const char* file, cocos2d::Rect rect, cocos2d::Rect capInsets);
	bool initWithSpriteFrameName(const char* spriteFrameName, cocos2d::Rect capInsets);
	bool initWithSpriteFrame(cocos2d::SpriteFrame* spriteFrame, cocos2d::Rect capInsets);

public:
	virtual void onEnter();
//	virtual void onExit();

	void setTitle(const char *title, int fontsize=20, const char* font="");
	void setContentText(const char* text, int padding = 50, int paddintTop = 100, int fontsize = 20, const char* font="");
	void setCallbackFunc(cocos2d::Ref* target, cocos2d::SEL_CallFuncN callfun);

	bool addButton(const char* normalImage, const char* selectedImage, const char* title, int tag = 0);    
	bool addButton(cocos2d::Node *normal, cocos2d::Node *selected, const char *title, int tag = 0);

	void show(cocos2d::Node* parent=0);
	void doModal(cocos2d::Node* parent=0);
private:
	void buttonCallback(cocos2d::Ref* pSender);

private:
	int mContentPadding;
	int mContentPaddingTop;

	cocos2d::Ref*		mTarget;
	cocos2d::SEL_CallFuncN	mCallback;

	cocos2d::ui::Scale9Sprite*	mBackground;
	cocos2d::CCMenu*			mMenu;
	cocos2d::Label *		mLbTitle;
	cocos2d::Label *		mLbContentText;

	class __ShieldLayer*			mShieldLayer;
};

#endif