#include "cocos2d.h"

#include "Timer.h"

#if (CC_TARGET_PLATFORM==CC_PLATFORM_WIN32)
#include <Windows.h>
#else

#endif	

USING_NS_CC;
#if (CC_TARGET_PLATFORM!=CC_PLATFORM_WIN32)

//#include <sys/time.h>
#include <unistd.h>
#include <sys/times.h>

unsigned long GetTickCount()
{
	static unsigned long s_mode = 0;
	static unsigned long s_tt = 0;
	if (s_mode == 0)
	{
		unsigned long tps = (unsigned long)sysconf(_SC_CLK_TCK);
		if (1000 % tps == 0)
		{
			s_tt = 1000 / tps;
			s_mode = 1;
		}
		else
		{
			s_tt = tps;
			s_mode = 2;
		}
	}
	struct tms t;
	const unsigned long dw = (unsigned long)times(&t);
	return (s_mode == 1 ? (dw * s_tt) : (unsigned long)(dw * 1000LL / s_tt));
}
#endif	
//////////////////////////////////////////////////////////////////////////
unsigned long CoTimer::getCurrentTime()
{
#if (CC_TARGET_PLATFORM==CC_PLATFORM_WIN32)
	return GetTickCount();
#else
	return GetTickCount();
#endif
}

//////////////////////////////////////////////////////////////////////////
CoTimer::CoTimer(int delay)
{
	init(delay);
}

void CoTimer::init(int delay)
{
	mStart = CoTimer::getCurrentTime();
	mDelay = delay;
}

bool CoTimer::isTimeUp() const
{
	return CoTimer::getCurrentTime() - mStart >= mDelay;
}


unsigned long CoTimer::getElapsed() const
{
	return CoTimer::getCurrentTime() - mStart;
}