#ifndef NEWDIALOG_H_
#define NEWDIALOG_H_

#include "cocos2d.h"
#include "ui/CocosGUI.h"
#include "Platform/PFView/BaseLayer.h"

USING_NS_CC;

class NewDialog : public BaseLayer
{
public:
	///< 面板类型,
	enum DIALOGTYPE
	{
		// 包含确认取消
		AFFIRMANDCANCEL,
		// 确认
		AFFIRM,
		// 无
		NONEBUTTON
	};
	NewDialog();
	~NewDialog();

	static bool  create(std::string txtString, DIALOGTYPE dialogType, std::function<void()> affirmCallBack = nullptr, std::function<void()> cancelCallBack = nullptr, std::function<void()> noneCallBack = nullptr);

	static bool  createOnce(std::string txtString, DIALOGTYPE dialogType, std::function<void()> affirmCallBack = nullptr, std::function<void()> cancelCallBack = nullptr, std::function<void()> noneCallBack = nullptr);
	
	static bool  create(std::string titleString, std::string contentString, DIALOGTYPE dialogType, std::function<void()> affirmCallBack = nullptr, std::function<void()> cancelCallBack = nullptr, std::function<void()> noneCallBack = nullptr);

	static bool  create(std::string titleString, std::string contentString, DIALOGTYPE dialogType, cocos2d::Ref* pTarget, cocos2d::SEL_CallFuncN func, cocos2d::Node * node);

	///< 输入的字符串,面板类型,是否载入场景, 3个回调
	static NewDialog * createNoLoadScene(std::string txtString, DIALOGTYPE dialogType, bool loadScene,std::function<void()> affirmCallBack = nullptr, std::function<void()> cancelCallBack = nullptr, std::function<void()> noneCallBack = nullptr);

	static NewDialog * createNoLoadScene(std::string titleString, std::string txtString, DIALOGTYPE dialogType, bool loadScene, cocos2d::Ref* pTarget, cocos2d::SEL_CallFuncN func, cocos2d::Node * node);


private:
	///< 按钮回调
	void affirmButtonTouch(cocos2d::Ref * ref, ui::TouchEventType eventType_);
	///< 关闭回调
	void cancelButtonTouch(cocos2d::Ref * ref, ui::TouchEventType eventType_);
	///, 无显示的回调
	void noneCallBackFunc();
private:
	bool initDialog(std::string txtString, DIALOGTYPE dialogType, std::function<void()> affirmCallBack = nullptr, std::function<void()> cancelCallBack = nullptr, std::function<void()> noneCallBack = nullptr);
	
	bool initDialog(std::string titleString, std::string txtString, DIALOGTYPE dialogType, std::function<void()> affirmCallBack = nullptr, std::function<void()> cancelCallBack = nullptr, std::function<void()> noneCallBack = nullptr);

	bool initDialog(std::string titleString, std::string txtString, DIALOGTYPE dialogType, cocos2d::Ref* pTarget, cocos2d::SEL_CallFuncN func, cocos2d::Node * node);



	// 确认回调
	std::function<void()> affirmCallBack;
	// 取消回调
	std::function<void()> cancelCallBack;
		// 没有按钮的回调
	std::function<void()> noneCallBack;
	// 函数地址
	cocos2d::SEL_CallFuncN mFunc;
	// 调用父亲
	cocos2d::Ref * mPTarget;
	// 点击的节点
	cocos2d::Node * mClickNode;

};


#endif
