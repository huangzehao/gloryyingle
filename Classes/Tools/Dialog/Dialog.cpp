#include "Dialog.h"

USING_NS_CC;
USING_NS_CC_EXT;

//////////////////////////////////////////////////////////////////////////
//屏蔽层
class __ShieldLayer: public CCLayer
{
public:
	CREATE_FUNC(__ShieldLayer);
private:
	__ShieldLayer(){}

	~__ShieldLayer()
	{
		setTouchEnabled(false);
	}

	bool init()
	{
		do 
		{
			CC_BREAK_IF(!CCLayer::init());
			setTouchEnabled(true);
			setSwallowsTouches(true);
			return true;
		} while (0);
		return false;
	}


public:
	bool onTouchBegan(cocos2d::CCTouch *pTouch, cocos2d::CCEvent *pEvent)
	{
		return true;
	}

// 	virtual void registerWithTouchDispatcher(void)
// 	{
// 		// 这里的触摸优先级设置为 -128 这保证了，屏蔽下方的触摸
// 		CCDirector::getInstance()->getTouchDispatcher()->addTargetedDelegate(this, -500, true);
// 	}
};

//////////////////////////////////////////////////////////////////////////
Dialog* Dialog::create(const char* file, cocos2d::Rect rect, cocos2d::Rect capInsets)
{
	Dialog* dlg = new Dialog();
	if (dlg && dlg->initWithFile(file, rect, capInsets))
	{
		dlg->autorelease();
		return dlg;
	}
	delete dlg;
	return 0;
}

Dialog* Dialog::create(const char* spriteFrameName, cocos2d::Rect capInsets)
{
	Dialog* dlg = new Dialog();
	if (dlg && dlg->initWithSpriteFrameName(spriteFrameName, capInsets))
	{
		dlg->autorelease();
		return dlg;
	}
	delete dlg;
	return 0;
}

Dialog* Dialog::create(cocos2d::SpriteFrame* spriteFrame, cocos2d::Rect capInsets)
{
	Dialog* dlg = new Dialog();
	if (dlg && dlg->initWithSpriteFrame(spriteFrame, capInsets))
	{
		dlg->autorelease();
		return dlg;
	}
	delete dlg;
	return 0;
}
//////////////////////////////////////////////////////////////////////////
Dialog::Dialog()
	: mMenu(0)
	, mContentPadding(0)
	, mContentPaddingTop(0)
	, mTarget(0)
	, mCallback(0)
	, mBackground(0)
	, mLbContentText(0)
	, mLbTitle(0)
	, mShieldLayer(0)
{}

Dialog::~Dialog()
{
	CC_SAFE_RELEASE_NULL(mLbTitle);
	CC_SAFE_RELEASE_NULL(mLbContentText);
}

bool Dialog::initWithFile(const char* file, cocos2d::Rect rect, cocos2d::Rect capInsets)
{
	do
	{
		CC_BREAK_IF(!CCNode::init());
		setContentSize(Size::ZERO);

		mBackground = Scale9Sprite::create(file, rect, capInsets);
		mBackground->setContentSize(rect.size);
		addChild(mBackground, 0, 0);
		
		mMenu = CCMenu::create();
		mMenu->setPosition(Vec2::ZERO);
		//mMenu->setTouchPriority(-501);
		mMenu->setSwallowsTouches(true);
		addChild(mMenu, 0, 0);
		return true;
	}while(0);
	return false;
}

bool Dialog::initWithSpriteFrameName(const char* spriteFrameName, cocos2d::Rect capInsets)
{
	CCSpriteFrameCache* cache = CCSpriteFrameCache::sharedSpriteFrameCache();
	return initWithSpriteFrame(cache->spriteFrameByName(spriteFrameName), capInsets);	
}

bool Dialog::initWithSpriteFrame(cocos2d::SpriteFrame* spriteFrame, cocos2d::Rect capInsets)
{
	do
	{
		CC_BREAK_IF(!CCNode::init());
		setContentSize(Size::ZERO);

		mBackground = Scale9Sprite::createWithSpriteFrame(spriteFrame, capInsets);
		mBackground->setContentSize(spriteFrame->getRect().size);
		addChild(mBackground, 0, 0);

		mMenu = CCMenu::create();
		mMenu->setPosition(Vec2::ZERO);
		mMenu->setSwallowsTouches(true);
		//mMenu->setTouchPriority(-501);
		addChild(mMenu, 0, 0);
		return true;
	}while(0);
	return false;
}

void Dialog::onEnter()
{
	CCNode::onEnter();

	CCSize contentSize;

	if (getContentSize().equals(Size::ZERO)) {
		contentSize = mBackground->getContentSize();
	} else {
		contentSize = getContentSize();
	}

	mBackground->setContentSize(contentSize);
	
	
	cocos2d::Vector<cocos2d::Node *> array = mMenu->getChildren();
	
	float btnWidth = 0;
	float btnHeight = 0;
	float btnInterval = 30;

	for (unsigned int i = 0; i < array.size(); ++i)
	{
		CCNode* node = dynamic_cast<CCNode*>(array.at(i));
		btnWidth += node->getContentSize().width+btnInterval;

		if (btnHeight < node->getContentSize().height)
			btnHeight = node->getContentSize().height;
	}
	
	float btnX = -btnWidth/2;
	for (unsigned int i = 0; i < array.size(); ++i)
	{
		CCNode* node = dynamic_cast<CCNode*>(array.at(i));
		node->setPosition(cocos2d::Vec2( btnX + (node->getContentSize().width+btnInterval)/2,  -contentSize.height/2 + btnHeight));
		btnX += node->getContentSize().width+btnInterval;
	}

	// 显示对话框标题
	if (mLbTitle)
	{
		mLbTitle->setPosition(cocos2d::Vec2(0, contentSize.height/2 - mContentPaddingTop / 2));
		addChild(mLbTitle);
	}

	// 显示文本内容
	if (mLbContentText)
	{
		mLbContentText->setPosition(cocos2d::Vec2(0, btnHeight / 2 ));
		mLbContentText->setDimensions(contentSize.width - mContentPadding * 2, contentSize.height - mContentPaddingTop - btnHeight);
		mLbContentText->setHorizontalAlignment(kCCTextAlignmentCenter);
		mLbContentText->setVerticalAlignment(kCCVerticalTextAlignmentCenter);
		mLbContentText->setSystemFontSize(40);
		addChild(mLbContentText);
	}
}

// void Dialog::onExit()
// {
// 	CCNode::onExit();
// }

void Dialog::setTitle(const char *title, int fontsize, const char* font)
{
	CC_SAFE_RELEASE_NULL(mLbTitle);

	if (title == 0 || strlen(title) == 0)
		return;

	mLbTitle = Label::create(title, font, fontsize);
	mLbTitle->retain();
}

void Dialog::setContentText(const char *text, int padding, int paddingTop, int fontsize, const char* font)
{
	CC_SAFE_RELEASE_NULL(mLbContentText);

	if (text == 0 || strlen(text) == 0)
		return;

	mContentPadding		= padding;
	mContentPaddingTop	= paddingTop;
	mLbContentText		= Label::create(text, font, fontsize);
	mLbContentText->retain();
}

void Dialog::setCallbackFunc(cocos2d::Ref *target, SEL_CallFuncN callfun)
{
	mTarget		= target;
	mCallback	= callfun;    
}


bool Dialog::addButton(const char *normalImage, const char *selectedImage, const char *title, int tag)
{
	// 创建图片菜单按钮
	CCMenuItemImage* menuImage = CCMenuItemImage::create(normalImage, selectedImage, this, menu_selector(Dialog::buttonCallback));
	menuImage->setTag(tag);
	menuImage->setPosition(cocos2d::Vec2(0, 0));

	if (title != 0 && strlen(title) != 0)
	{
		CCSize imenu = menuImage->getContentSize();
		Label* ttf =Label::create(title, "", 20);
		ttf->setColor(ccc3(0, 0, 0));
		ttf->setPosition(cocos2d::Vec2(imenu.width / 2, imenu.height / 2));
		menuImage->addChild(ttf);
	}

	mMenu->addChild(menuImage);
	return true;
}

bool Dialog::addButton(cocos2d::Node *normal, cocos2d::Node *selected, const char *title, int tag)
{
	// 创建精灵菜单按钮
	CCMenuItemSprite* menuItem = CCMenuItemSprite::create(normal, selected , 0, this, menu_selector(Dialog::buttonCallback));
	menuItem->setTag(tag);
	menuItem->setPosition(cocos2d::Vec2(0, 0));
	if (title != 0 && strlen(title) != 0)
	{
		CCSize imenu = menuItem->getContentSize();
		Label* ttf =Label::create(title, "", 20);
		ttf->setColor(ccc3(0, 0, 0));
		ttf->setPosition(cocos2d::Vec2(imenu.width / 2, imenu.height / 2));
		menuItem->addChild(ttf);
	}

	mMenu->addChild(menuItem);
	return true;
}

void Dialog::buttonCallback(cocos2d::Ref *pSender)
{
	Node* node = dynamic_cast<Node*>(pSender);
	if (mCallback && mTarget)
	{
		(mTarget->*mCallback)(node);
	}

	if (mShieldLayer)
	{
		mShieldLayer->removeFromParentAndCleanup(true);
	}

	removeFromParentAndCleanup(true);
}

void Dialog::show(cocos2d::Node* parent)
{
	if (parent == 0)
		parent = CCDirector::getInstance()->getRunningScene();

	if (parent)
	{
		parent->addChild(this);
	}
}

void Dialog::doModal(cocos2d::Node* parent)
{
	if (parent == 0)
		parent = CCDirector::getInstance()->getRunningScene();

	if (parent)
	{
		mShieldLayer = __ShieldLayer::create();
		parent->addChild(mShieldLayer);
		parent->addChild(this);
	}
}