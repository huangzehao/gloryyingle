#ifndef SIMPLETOOLS_H_
#define SIMPLETOOLS_H_

#include "cocos2d.h"
#include "ui/CocosGUI.h"
#include "DntgGame/DntgDefine/Game/DntgxPoint.h"

USING_NS_CC;
using namespace ui;

///< 闪电类型
enum LINETYPE
{
	PINK = 0,
	BLUE,
	YELLOW,
	ALLTYPE
};
///< 简单的工具类
class SimpleTools
{
public:
	SimpleTools();
	~SimpleTools();

	//通过头像ID创建图片
	static const char * getGameHeadImageByFaceID(int wFaceID);
	///< 鱼的类型
	static int switchFaceID(int wFaceID);
	///< 判断游戏是什么类型的游戏
	static int isGameKind(int wKindID);

	static const char * getNN2GameHeadImageByScore(int gameScore, bool isMan);

	static float calcRotate(int char_id, const Dntg::xPoint& pt_offset);
	///< 获得圆的坐标
	static cocos2d::CCActionInterval * getCircularAction(const Vec2 & start_pos, float radius);

	///< 闪电的动作
	static cocos2d::Sprite * getLightAction(float duration, const Vec2 & base_pos, std::vector<Vec2> other_pos, LINETYPE sType = LINETYPE::PINK);
	///< 大闹天宫
	///< 鱼的类型
	static bool isDntgSpecialBird(int birdType);
	///< 是圆盘鱼
	static bool isDntgSpecialRoundBird(int birdType);
	///< 是需要直走的鱼
	static bool isDntgNeedGoStraightBird(int birdType);
	///< 是否反向,值为1或者-1
	static bool isDntgReverseAtGoStraightBird(const Vec2 & start_p, const Vec2 & end_p);
	///< 是龙王
	static bool isDntgDragonKing(int birdType);
	///< 经典捕鱼
	///< 鱼的类型
	static bool isJdbySpecialBird(int birdType);
	///< 是圆盘鱼
	static bool isJdbySpecialRoundBird(int birdType);
	///< 是需要直走的鱼
	static bool isJdbyNeedGoStraightBird(int birdType);
	///< 是否反向,值为1或者-1
	static bool isJdbyReverseAtGoStraightBird(const Vec2 & start_p, const Vec2 & end_p);
	///< 是龙王
	static bool isJdbyDragonKing(int birdType);
	///< 获取一个范围内的随机地点
	static cocos2d::Vec2 getConfineToRandomVec2(cocos2d::Vec2 & oprign, int radius);

#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID || CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
	static bool obtainNetWorkState();
	///<获取手机号码
	static std::string getPhoneNum();
#endif
	///< 是否进入过游戏
	static bool isEnterGame;
private:
	

};

// ///< 缩放由数字构成的文本
// #define DIGIT_SCALE(target, cond) if((cond) > (2 << 22)) target->setScale(0.7f); \
// 			else target->setScale(0.8f);

#endif
