#ifndef _MTNotification_H_
#define _MTNotification_H_

#include "cocos2d.h"
#include "pthread.h"

USING_NS_CC;

class MTData : public cocos2d::Ref
{
public:
	static MTData* create(unsigned int data1 = 0, unsigned int data2 = 0, unsigned int data3 = 0, 
		const std::string& str1 = "", const std::string& str2 = "", const std::string& str3 = "", void * pdata = nullptr)
	{
		MTData* dat = new MTData();
		if (dat && dat->init(data1, data2, data3, str1, str2, str3, pdata))
		{
			dat->autorelease();
			return dat;
		}
		delete dat;
		return 0;
	}

	unsigned int mData1;
	unsigned int mData2;
	unsigned int mData3;
	std::string  mStr1;
	std::string  mStr2;
	std::string  mStr3;
	void * mPData;
private:
	MTData(){}
	~MTData(){}

	bool init(unsigned int data1, unsigned int data2, unsigned int data3, 
		const std::string& str1, const std::string& str2, const std::string& str3, void * pdata)
	{
		mData1 = data1;
		mData2 = data2;
		mData3 = data3;
		mStr1  = str1;
		mStr2  = str2;
		mStr3  = str3;
		mPData = pdata;
		return true;
	}
};

//////////////////////////////////////////////////////////////////////////
class MTNotificationImp;
// 
// class MyNotification
// 	: public cocos2d::Ref
// {
// public:
// 	static MyNotification*	msInstance;
// public:
// 	static MyNotification* shared();
// 	static void purge();
// 
// public:
// 	MyNotification();
// 	~MyNotification();
// 	void postNotify(const std::string& name, cocos2d::Ref* obj);
// 	void post(float dt);
// private:
// 	struct MTNotifyData
// 	{
// 		std::string			name;
// 		cocos2d::Ref*	obj;
// 	}; // MTNotifyData
// 
// private:
// 	MTNotificationImp*	mImp;
// 
// 	pthread_t			mThreadPid;
// 	pthread_mutex_t		mMutex;
// 	std::list<MTNotifyData> mDataQueues;
// };


// #define G_NOTIFY(name, obj)				CCNotificationCenter::sharedNotificationCenter()->postNotification(name, obj)
// #define G_NOTIFY_REG(name, callfuncO)	CCNotificationCenter::sharedNotificationCenter()->addObserver(this, callfuncO_selector(callfuncO), name, 0)
// #define G_NOTIFY_UNREG(name)			CCNotificationCenter::sharedNotificationCenter()->removeObserver(this, name)


class MyNotification : public Ref
{
public:
	MyNotification();



	virtual ~MyNotification();
	
		
	

	static MyNotification * getInstance();

public:
	bool registerEvent(const std::string &name_, const std::function<void(EventCustom*)>& callback);
	bool unRegisterEventListener(const std::string& customEventName);

	void dispatchEvent(const std::string &name, void* data = nullptr);

	void post(float dt);
	// 创建MTNotificationImp
	void initMTNotificationImp();
private:
	static MyNotification* g_instance;
	///< 注册时间发送器的处理函数
	MTNotificationImp*	mImp; 

	
};











#define  G_NOTIFY_REG(name, callfunc)		 MyNotification::getInstance()->registerEvent( name, CC_CALLBACK_1( callfunc, this ) );
#define  G_NOTIFY_D(name, data )			 MyNotification::getInstance()->dispatchEvent( name, data );
#define  G_NOTIFY(name)						 MyNotification::getInstance()->dispatchEvent( name );
#define  G_NOTIFY_UNREG(customEventName)	 MyNotification::getInstance()->unRegisterEventListener(customEventName)


#define G_NOTIFY_SAFE(name, obj)		MTNotification::shared()->postNotify(name, obj)

#endif // _MTNotification_H_
