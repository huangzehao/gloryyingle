#include "SimpleTools.h"
//#include "ShlwGame/ShlwHeader.h"
#include "Platform/PlatformHeader.h"
#include "Tools/Manager/SpriteHelper.h"

#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)

#include <Jni.h>
#include "platform/android/jni/JniHelper.h"

bool SimpleTools::obtainNetWorkState()
{
	JniMethodInfo minfo;//定义Jni函数信息结构体
	int isHaveNet = 0;
	//getStaticMethodInfo 次函数返回一个bool值表示是否找到此函数
	const char * jniClass = "org.cocos2dx.cpp.AppActivity";//这里写你所要调用的java代码的类名
	bool isHave = JniHelper::getStaticMethodInfo(minfo,jniClass,"haveNetWork","()I");//本人在此传递的参数是字符串
	if (!isHave) {
		//CCLog("jni->%s/callJni:此函数不存在", jniClass);
	}
	else{
		//CCLog("jni->%s/callJni:此函数存在", jniClass);
		//jstring jdata = minfo.env->NewStringUTF(data);
		//调用此函数
		isHaveNet = minfo.env->CallStaticIntMethod(minfo.classID, minfo.methodID);
	}
	//CCLog("jni-java函数执行完毕");

	return isHaveNet;
}

std::string SimpleTools::getPhoneNum()
{
	JniMethodInfo minfo;//定义Jni函数信息结构体
	jstring phoneNum;
	//getStaticMethodInfo 次函数返回一个bool值表示是否找到此函数
	const char * jniClass = "org.cocos2dx.cpp.AppActivity";//这里写你所要调用的java代码的类名
	bool isHave = JniHelper::getStaticMethodInfo(minfo,jniClass,"getPhoneNum","()Ljava/lang/String;");//本人在此传递的参数是字符串
	if (!isHave) {
		CCLog("jni->%s/callJni:此函数不存在", jniClass);
	}
	else{
		CCLog("jni->%s/callJni:此函数存在", jniClass);
		//jstring jdata = minfo.env->NewStringUTF(data);
		//调用此函数
		phoneNum = (jstring)minfo.env->CallStaticObjectMethod(minfo.classID, minfo.methodID);
	}
	//	CCLog("jni-java函数执行完毕");

	std::string str = JniHelper::jstring2string(phoneNum);


	CCLog("jni-java函数执行完毕%s", str.c_str());
	return str;
}

#endif


#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
#include "../proj.ios_mac/ios/SystemInfo.h"

bool SimpleTools::obtainNetWorkState()
{
	bool haveWifi = isWIFIEnabled();
	bool have3G = is3GEnabled();

	return haveWifi || have3G;
}
#endif

#define IMG_HEAD_ANIMATION_0 "0-0-0.png"
#define IMG_HEAD_ANIMATION_1 "1-0-0.png"
#define IMG_HEAD_ANIMATION_2 "2-0-0.png"
#define IMG_HEAD_ANIMATION_3 "3-0-0.png"
#define IMG_HEAD_ANIMATION_4 "4-0-0.png"
#define IMG_HEAD_ANIMATION_5 "5-0-0.png"
#define IMG_HEAD_ANIMATION_6 "6-0-0.png"
#define IMG_HEAD_ANIMATION_7 "7-0-0.png"

///< 服务闪电线Action
const char * getNameByLineType(LINETYPE sType)
{
	
	int random_value =rand();
	switch (sType)
	{
	case PINK:
		return "PinkLine";
		break;
	case BLUE:
		return "BlueLine";
		break;
	case YELLOW:
		return "YellowLine";
		break;
	case ALLTYPE:
		random_value = random_value % 3;
		return getNameByLineType(LINETYPE(random_value));
	default:
		break;
	}
	return "PinkLine";
}

SimpleTools::SimpleTools()
{
}

SimpleTools::~SimpleTools()
{
}

int SimpleTools::switchFaceID(int wFaceID)
{
	switch (wFaceID)
	{
	case 0: return 3;
		break;
	case 1: return 7;
		break;
	case 2: return 2;
		break;
	case 3: return 5;
		break;
	case 4: return 6;
		break;
	case 5: return 0;
		break;
	case 6: return 1;
		break;
	case 7: return 4;
		break;
	default:
		return 3;
		break;
	}
}

int SimpleTools::isGameKind(int wKindID)
{
	switch (wKindID)
	{
		//捕鱼类
	case DANAOTIANGO_KINDID:
		return 1;
		//街机类
	case BAIJIALE_KINDID:
		return 2;
		//棋牌类
	case ZHAJINHUA_KINDID:
	case SIRENNIUNIU_KINDID:
	case DOUDIZHU_KINDID:
		return 3;
		break;
	default:
		return 0;
	}
	return 0;
}

const char * SimpleTools::getGameHeadImageByFaceID(int wFaceID)
{
	switch (wFaceID)
	{
		case 0: return IMG_HEAD_ANIMATION_0;
			break;
		case 1: return IMG_HEAD_ANIMATION_1;
			break;
		case 2: return IMG_HEAD_ANIMATION_2;
			break;
		case 3: return IMG_HEAD_ANIMATION_3;
			break;
		case 4: return IMG_HEAD_ANIMATION_4;
			break;
		case 5: return IMG_HEAD_ANIMATION_5;
			break;
		case 6: return IMG_HEAD_ANIMATION_6;
			break;
		case 7: return IMG_HEAD_ANIMATION_7;
			break;
		default:
				 return IMG_HEAD_ANIMATION_0;
			break;
	}
}


const char * SimpleTools::getNN2GameHeadImageByScore(int gameScore, bool isMan)
{
	if (isMan)
	{
		if (gameScore >= 100000000)
		{
			return ("Dating_head24.png");
		}
		else if (gameScore >= 50000000)
		{
			return ("Dating_head24.png");
		}
		else if (gameScore >= 20000000)
		{
			return ("Dating_head24.png");
		}
		else if (gameScore >= 10000000)
		{
			return ("Dating_head23.png");
		}
		else if (gameScore >= 5000000)
		{
			return ("Dating_head22.png");
		}
		else if (gameScore >= 1600000)
		{
			return ("Dating_head21.png");
		}
		else if (gameScore >= 800000)
		{
			return ("Dating_head20.png");
		}
		else if (gameScore >= 400000)
		{
			return ("Dating_head19.png");
		}
		else if (gameScore >= 100000)
		{
			return ("Dating_head18.png");
		}
		else if (gameScore >= 10000)
		{
			return ("Dating_head17.png");
		}
		else if (gameScore >= 1000)
		{
			return ("Dating_head16.png");
		}
		else if (gameScore >= 100)
		{
			return ("Dating_head15.png");
		}
		else
		{
			return ("Dating_head14.png");
		}
	}
	else
	{
		if (gameScore >= 100000000)
		{
			return ("Dating_head13.png");
		}
		else if (gameScore >= 50000000)
		{
			return ("Dating_head12.png");
		}
		else if (gameScore >= 20000000)
		{
			return ("Dating_head11.png");
		}
		else if (gameScore >= 10000000)
		{
			return ("Dating_head10.png");
		}
		else if (gameScore >= 5000000)
		{
			return ("Dating_head9.png");
		}
		else if (gameScore >= 1600000)
		{
			return ("Dating_head8.png");
		}
		else if (gameScore >= 800000)
		{
			return ("Dating_head7.png");
		}
		else if (gameScore >= 400000)
		{
			return ("Dating_head6.png");
		}
		else if (gameScore >= 100000)
		{
			return ("Dating_head5.png");
		}
		else if (gameScore >= 10000)
		{
			return ("Dating_head4.png");
		}
		else if (gameScore >= 1000)
		{
			return ("Dating_head3.png");
		}
		else if (gameScore >= 100)
		{
			return ("Dating_head2.png");
		}
		else
		{
			return ("Dating_head1.png");
		}
	}
}

cocos2d::CCActionInterval * SimpleTools::getCircularAction(const Vec2 & start_pos, float radius)
{
	///< 创建样条插值运动
	CCPointArray * array = CCPointArray::create(20);
	Vec2 base_pos = start_pos;
	/*array->addControlPoint(base_pos + Vec2(0, 0));*/
	array->addControlPoint(base_pos + Vec2(radius, 0));
	float x_ = CC_DEGREES_TO_RADIANS(45);
	array->addControlPoint(base_pos + Vec2(radius * cos(x_), radius * sin(x_)));
	array->addControlPoint(base_pos + Vec2(0, radius));
	x_ = CC_DEGREES_TO_RADIANS(135);
	array->addControlPoint(base_pos + Vec2(radius * cos(x_), radius * sin(x_)));
	array->addControlPoint(base_pos + Vec2(-radius, 0));
	x_ = CC_DEGREES_TO_RADIANS(225);
	array->addControlPoint(base_pos + Vec2(radius * cos(x_), radius * sin(x_)));
	array->addControlPoint(base_pos + Vec2(0, -radius));
	x_ = CC_DEGREES_TO_RADIANS(315);
	array->addControlPoint(base_pos + Vec2(radius * cos(x_), radius * sin(x_)));
	array->addControlPoint(base_pos + Vec2(radius, 0));
	CCActionInterval  * CardinalSplineTo = CCCardinalSplineTo::create(2, array, 0);
	CCFiniteTimeAction*seq = CCSequence::create(CardinalSplineTo, NULL);
	CCActionInterval * repeatForever = CCRepeatForever::create((CCActionInterval*)seq);

	return repeatForever;
}

cocos2d::Sprite * SimpleTools::getLightAction(float duration, const Vec2 & base_pos, std::vector<Vec2> other_pos, LINETYPE sType)
{
	
	Sprite * fatherSprite = Sprite::create();
	for (int i = 0; i < (int)other_pos.size(); i++)
	{
		const char * name = getNameByLineType(sType);
		Sprite * show_text = Sprite::create();
		show_text->setAnchorPoint(Vec2(0, 0.5f));
		show_text->setPosition(base_pos);
		Animate * animate = SpriteHelper::createAnimate(name);
		show_text->runAction(RepeatForever::create(animate));
		show_text->setScale(0.01f);
		Vec2 tOther = other_pos[i];
		Vec2 now_p = ccpSub(tOther, base_pos);
		float angle = ccpToAngle(now_p);
		float degrees = 360 - CC_RADIANS_TO_DEGREES(angle);
		show_text->setRotation(degrees);
		Rect tsize = animate->getAnimation()->getFrames().at(0)->getSpriteFrame()->getRect();

		float distance = now_p.getLength();
		///< 计算X轴缩放率
		float xScale = distance / tsize.size.width;
		///< 计算Y轴缩放率
		float yScale = /*0.1 + distance / 100 / 10*/1;
	//	log("yScale is %f", yScale);
		ScaleTo * s_to = ScaleTo::create(duration, xScale, yScale);
		show_text->runAction(CCEaseSineOut::create(s_to));
		fatherSprite->addChild(show_text);
	}
// 	Sprite * show_text = Sprite::create();
// 	show_text->setAnchorPoint(Vec2(0, 0.5f));
// 	show_text->setPosition(base_pos);
// 	show_text->runAction(RepeatForever::create(SpriteHelper::createAnimate("PinkLine")));
// 	show_text->setScale(0.1f);
// 	ScaleTo * s_to = ScaleTo::create(2, 2, 1);
// 	show_text->runAction(s_to);
// 	show_text->setPosition(Vec2(400, 400));

	return fatherSprite;
}

cocos2d::Vec2 SimpleTools::getConfineToRandomVec2(cocos2d::Vec2 & oprign, int radius)
{
	int randomValue_x = 0;
	int randomValue_y = 0;
	Rect oprignValue(oprign.x, oprign.y, radius, radius);
	Vec2 newPoint;
	int divisor = radius * 2;

	randomValue_x = rand() % divisor - radius;
	randomValue_y = rand() % divisor - radius;
	newPoint.x = oprign.x + randomValue_x;
	newPoint.y = oprign.y + randomValue_y;

	return newPoint;
}

bool SimpleTools::isEnterGame = false;



