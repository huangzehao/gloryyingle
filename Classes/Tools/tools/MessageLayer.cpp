#include "Platform/PlatformHeader.h"
#include "MessageLayer.h"
#include "Tools/tools/MTNotification.h"

USING_NS_CC;
USING_NS_CC_EXT;
using namespace std;
using namespace Message;
//////////////////////////////////////////////////////////////////////////
MessageLayer::MessageLayer() : isMove(false)
{
}

MessageLayer::~MessageLayer()
{
	this->unschedule(SEL_SCHEDULE(&MessageLayer::MessageUpdate));
}

//初始化方法
bool MessageLayer::init()
{
	do 
	{
		CC_BREAK_IF(!CCLayer::init());
		
		m_BgRoll = ui::Layout::create();
		m_BgRoll->setAnchorPoint(Vec2(0.5f, 0.5f));
		m_BgRoll->setContentSize(Size(1110, 45));
		m_BgRoll->setSize(Size(1110, 45));
		m_BgRoll->setBackGroundImage(SYSIMGPATH);

		Size winSize = Director::getInstance()->getWinSize();
		m_BgRoll->setPosition(Vec2(winSize.width / 2, winSize.height - 50));
		m_BgRoll->setClippingEnabled(true);
		m_BgRoll->setVisible(false);
		this->addChild(m_BgRoll);

		mBgLayout = ui::Layout::create();
		mBgLayout->setAnchorPoint(Vec2(0.5f, 0.5f));
		mBgLayout->setContentSize(Size(1035, 45));
		mBgLayout->setSize(Size(1035, 45));
		mBgLayout->setPosition(Vec2(1035 / 2 + 72, 45 / 2 -5));
		mBgLayout->setClippingEnabled(true);
		m_BgRoll->addChild(mBgLayout);
		
		mLabMessage = Label::create();
		mLabMessage->setSystemFontSize(36);
		mLabMessage->setTextColor(Color4B::YELLOW);
		mLabMessage->setAnchorPoint(Vec2(0.0f, 0.0f));
		mBgLayout->addChild(mLabMessage);

		
		this->schedule(SEL_SCHEDULE(&MessageLayer::MessageUpdate), 1);
		return true;
	} while (0);

	return false;
}

//////////////////////////////////////////////////////////////////////////
// IChatSink

//////////////////////////////////////////////////////////////////////////
//表情消息

//用户表情
bool MessageLayer::InsertExpression(const char* pszSendUser, unsigned int index)
{
	//PLAZZ_PRINTF(t8("%s 发送表情:%d\n"), pszSendUser, index);

	////变量定义
	//CExpressionManager * pExpressionManager=CExpressionManager::GetInstance();
	//CExpression * pExpressionItem=pExpressionManager->GetExpressionItem(pUserExpression->wItemIndex);

	////插入表情
	//if (pExpressionItem!=0)
	//{
	//	//获取路径
	//	char szImagePath[MAX_PATH]=TEXT("");
	//	pExpressionItem->GetExpressionPath(szImagePath,CountArray(szImagePath));

	//	//插入消息
	//	const char* pszSendUser=pISendUserItem->GetNickName();
	//	m_ChatMessage.InsertExpression(pszSendUser,szImagePath);
	//}
	return true;
}

//用户表情
bool MessageLayer::InsertExpression(const char* pszSendUser, unsigned int index, bool bMyselfString)
{
	//PLAZZ_PRINTF(t8("%s 发送表情:%d\n"), pszSendUser, index);
	return true;
}

//用户表情
bool MessageLayer::InsertExpression(const char* pszSendUser, const char* pszRecvUser, unsigned int index)
{
	//PLAZZ_PRINTF(t8("%s 对 %s 发送表情:%d\n"), pszSendUser, pszRecvUser, index);
	return true;
}

//////////////////////////////////////////////////////////////////////////
//聊天消息

//用户聊天
bool MessageLayer::InsertUserChat(const char* pszSendUser, const char* pszString, unsigned int crColor)
{
	//PLAZZ_PRINTF(t8("%s 说:%s\n"), pszSendUser, pszString);
	return true;
}
//用户聊天
bool MessageLayer::InsertUserChat(const char* pszSendUser, const char* pszRecvUser, const char* pszString, unsigned int crColor)
{
	//PLAZZ_PRINTF(t8("%s 对 %s 说:%s\n"), pszSendUser, pszRecvUser, pszString);
	return true;
}
//用户私聊
bool MessageLayer::InsertWhisperChat(const char* pszSendUser, const char* pszString, unsigned int crColor, bool bMyselfString)
{
	//PLAZZ_PRINTF(t8("%s 对你说:%s\n"), pszSendUser, pszString);
	return true;
}
//用户喇叭
bool MessageLayer::InsertUserTrumpet(const char* pszSendUser,const char* pszString,unsigned int crColor)
{
	//PLAZZ_PRINTF(t8("%s 发送Trumpet喇叭:%s\n"), pszSendUser, pszString);
	return true;
}
//用户喇叭
bool MessageLayer::InsertUserTyphon(const char* pszSendUser,const char* pszString,unsigned int crColor)
{
	//PLAZZ_PRINTF(t8("%s 发送Typhon喇叭:%s\n"), pszSendUser, pszString);
	return true;
}

//////////////////////////////////////////////////////////////////////////
//系统消息

//系统消息
bool MessageLayer::InsertSystemChat(const char* pszString)
{
	PLAZZ_PRINTF("MessageLayer::InsertSystemChat");
	//PLAZZ_PRINTF(t8("系统消息:%s \n"), pszString);
	return true;
}

//////////////////////////////////////////////////////////////////////////
// IStringMessageSink

//////////////////////////////////////////////////////////////////////////
//事件消息

//进入事件
bool MessageLayer::InsertUserEnter(const char* pszUserName)
{
	PLAZZ_PRINTF("MessageLayer::InsertUserEnter");
	//PLAZZ_PRINTF(t8("%s 进入房间\n"), pszUserName);
	return true;
}

//离开事件
bool MessageLayer::InsertUserLeave(const char* pszUserName)
{
	PLAZZ_PRINTF("MessageLayer::InsertUserLeave");
	//PLAZZ_PRINTF(t8("%s 离开房间\n"), pszUserName);
	return true;
}

//断线事件
bool MessageLayer::InsertUserOffLine(const char* pszUserName)
{
	PLAZZ_PRINTF("MessageLayer::InsertUserOffLine");
	//PLAZZ_PRINTF(t8("%s 断线\n"), pszUserName);
	return true;
}

//////////////////////////////////////////////////////////////////////////
//字符消息

//普通消息(窗口输出)
bool MessageLayer::InsertNormalString(const char* pszString)
{
	PLAZZ_PRINTF("MessageLayer::InsertNormalString");
	//PLAZZ_PRINTF(t8("普通消息:%s\n"), pszString);
	return true;
}
//系统消息(窗口输出)
bool MessageLayer::InsertSystemString(const char* pszString)
{
	PLAZZ_PRINTF("MessageLayer::InsertSystemString");
	log("message is %s ", pszString);

	mMessageQueue.push(pszString);
	//PLAZZ_PRINTF(t8("系统消息:%s\n"), pszString);
//	G_NOTIFY_D("GLAD_MSG", MTData::create(0, 0, 0, pszString, "", ""));
	return true;
}
//提示消息(对话框方式??)
int MessageLayer::InsertPromptString(const char* pszString, int iButtonType)
{
	PLAZZ_PRINTF("MessageLayer::InsertPromptString");
	//PLAZZ_PRINTF(t8("提示消息:%s[%s]\n"), pszString, iButtonType == 0 ? t8("确认"):t8("确认,取消"));
	return 0;
}
//公告消息(窗口输出)
bool MessageLayer::InsertAfficheString(const char* pszString)
{
	PLAZZ_PRINTF("MessageLayer::InsertAfficheString");
	//PLAZZ_PRINTF(t8("公告消息:%s\n"), pszString);
	return true;
}


//喜报消息
bool MessageLayer::InsertGladString(const char* pszContent, const char* pszNickName, const char* pszNum, dword colText, dword colName, dword colNum)
{
	G_NOTIFY_D("GLAD_MSG", MTData::create(colText, colName, colNum, pszContent, pszNickName, pszNum));
	return true;
}

void MessageLayer::moveEndCall()
{
	log("come in!!");
	isMove = false;
	if (mMessageQueue.size() < 1)
		m_BgRoll->setVisible(false);
}

void MessageLayer::MessageUpdate(float delta)
{
	///< 更新消息
	if (isMove) return;
	if (mMessageQueue.size() < 1) return;
	m_BgRoll->setVisible(true);
	std::string text = mMessageQueue.front();
	mMessageQueue.pop();
	mLabMessage->setString(text);

	isMove = true;
	Size label_pos = mBgLayout->getSize();
	mLabMessage->setPosition(Vec2(label_pos.width, 5));
	float move_distance = text.size() * 21;// +label_pos.width;
	if (move_distance < label_pos.width * 1.5f)
		move_distance += label_pos.width;

	float time = move_distance / 100 < 25 ? 25 : move_distance / 100;
	auto move = MoveBy::create(move_distance / 180, Vec2(-move_distance, 0));
	auto sequence = Sequence::create(move, CallFunc::create(this, callfunc_selector(MessageLayer::moveEndCall)), NULL);
	mLabMessage->runAction(sequence);

}

MessageLayer * MessageLayer::create()
{
	MessageLayer * layer = new MessageLayer;
	if (layer && layer->init())
	{
		layer->autorelease();
		return layer;
	}
	delete layer;
	layer = nullptr;
	return nullptr;
}
