#include "YRCommon.h"

#define IMG_HEAD_PHOTO_0 "LobbyResources/headIcon/women_1.png"
#define IMG_HEAD_PHOTO_1 "LobbyResources/headIcon/women_2.png"
#define IMG_HEAD_PHOTO_2 "LobbyResources/headIcon/women_3.png"
#define IMG_HEAD_PHOTO_3 "LobbyResources/headIcon/women_4.png"
#define IMG_HEAD_PHOTO_4 "LobbyResources/headIcon/men_1.png"
#define IMG_HEAD_PHOTO_5 "LobbyResources/headIcon/men_2.png"
#define IMG_HEAD_PHOTO_6 "LobbyResources/headIcon/men_3.png"
#define IMG_HEAD_PHOTO_7 "LobbyResources/headIcon/men_4.png"

const char * YRComGetHeadImageById(int head_id)
{
	switch (head_id % 8)
	{
	case 0:
		return IMG_HEAD_PHOTO_0;
	case 1:
		return IMG_HEAD_PHOTO_1;
	case 2:
		return IMG_HEAD_PHOTO_2;
	case 3:
		return IMG_HEAD_PHOTO_3;
	case 4:
		return IMG_HEAD_PHOTO_4;
	case 5:
		return IMG_HEAD_PHOTO_5;
	case 6:
		return IMG_HEAD_PHOTO_6;
	case 7:
		return IMG_HEAD_PHOTO_7;
	default:
		return IMG_HEAD_PHOTO_0;
	}
}

bool YRComStringIsNumber(const std::string strString)
{
	if (strString.empty()) return false;

	for (int i = 0; i < strString.size(); i++)
	{
		auto ch = strString[i];
		if (ch >= '0' && ch <= '9')
			continue;
		return false;
	}
	return true;
}