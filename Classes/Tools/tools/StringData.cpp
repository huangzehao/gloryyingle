#include "StringData.h"

USING_NS_CC;

using namespace std;


//定义StringData对象
static StringData* _sharedStringData = 0;

//得到StringData单例对象
StringData* StringData::shared()
{
	if(_sharedStringData == 0){
		_sharedStringData = new StringData();
		_sharedStringData->init();
	}
	return _sharedStringData;
}

//当内存不足时调用
void StringData::purge()
{
	CC_SAFE_RELEASE_NULL(_sharedStringData);
}


StringData::StringData()
{
}

StringData::~StringData()
{
	CC_SAFE_RELEASE_NULL(_dictionary);
}

///<

bool StringData::init()
{
	//相对路径
	const char* plistpath = "strings.plist";
	//通过CCFileUtils的fullPathFromFile方法获取绝对路径
	FileUtils* fileutils = FileUtils::getInstance();
	std::string pfullpath = fileutils->fullPathForFilename(plistpath);
	const char* cfullpath = pfullpath.c_str();

	_dictionary = CCDictionary::createWithContentsOfFile(cfullpath);

	CC_SAFE_RETAIN(_dictionary);
	int count = _dictionary->getReferenceCount();
	cocos2d::log("count is %d", count);
	return true;
}

//根据键值得到String类型数据
const char* StringData::stringFromKey(const std::string& key)
{
	return _dictionary->valueForKey(key)->getCString();
}
