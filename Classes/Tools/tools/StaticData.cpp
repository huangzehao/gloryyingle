#include "StaticData.h"

USING_NS_CC;

using namespace std;

//file path
#define  STATIC_DATA_PATH "static_data.plist"

//定义StaticData对象
static StaticData* _sharedStaticData = 0;

//得到StaticData单例对象
StaticData* StaticData::shared()
{
	if(_sharedStaticData == 0){
		_sharedStaticData = new StaticData();
		_sharedStaticData->init();
	}
	return _sharedStaticData;
}


void StaticData::purge()
{
	CC_SAFE_RELEASE_NULL(_sharedStaticData);
}


StaticData::StaticData()
{
}

StaticData::~StaticData()
{
	CC_SAFE_RELEASE_NULL(_dictionary);
}

bool StaticData::init()
{
	_dictionary = CCDictionary::createWithContentsOfFile(STATIC_DATA_PATH);
	CC_SAFE_RETAIN(_dictionary);
	return true;
}
//根据键值得到String类型数据
const char* StaticData::stringFromKey(const std::string& key)
{
	return _dictionary->valueForKey(key)->getCString();
}

//根据键值得到int类型数据
int StaticData::intFromKey(const std::string& key)
{
	return _dictionary->valueForKey(key)->intValue();
}


unsigned int StaticData::colorFromKey(const std::string& key)
{
	const char* str = _dictionary->valueForKey(key)->getCString();
	unsigned int val=0;
	sscanf(str, "%x", &val);
	return val;
}

//根据键值得到float类型数据
float StaticData::floatFromKey(const std::string& key)
{
	return _dictionary->valueForKey(key)->floatValue();
}

//根据键值得到bool类型数据
bool StaticData::booleanFromKey(const std::string& key)
{
	return _dictionary->valueForKey(key)->boolValue();
}

//根据键值得到point类型数据
cocos2d::Vec2 StaticData::pointFromKey(const std::string& key)
{
	return PointFromString(_dictionary->valueForKey(key)->getCString());
}

//根据键值得到rect类型数据
cocos2d::Rect StaticData::rectFromKey(const std::string& key)
{
	return RectFromString(_dictionary->valueForKey(key)->getCString());
}

//根据键值得到size类型数据
cocos2d::Size StaticData::sizeFromKey(const std::string& key)
{
	return SizeFromString(_dictionary->valueForKey(key)->getCString());
}

//根据键值得到size类型数据
cocos2d::CCArray* StaticData::arrayFromKey(const std::string& key)
{
	return (CCArray*)_dictionary->objectForKey(key);
}

