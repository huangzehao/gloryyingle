#ifndef _gPlatform_H_
#define _gPlatform_H_
//////////////////////////////////////////////////////////////////////////
// 游戏平台
#define GAME_PLATFORM_DEFAULT	0
#define GAME_PLATFORM_360		360

int  platformGetPlatform();
std::string platformGetPlatformName();
std::string platformGetPlatformVersion();
void platformOpenLoginView();
void platformOpenExitView();
void platformOpenPayView(int item, const char* account, const char* username, const char* userid);

//打开竣付通webview
void platformOpenJunFuTongWeChetPay(const char* accounts, int numble);

class QSJniFun
{
public:
	static void loginWX(const char* APP_ID, const char* AppSecret);
	static void shareImageWX(const char* ImgPath, int nType);
	static void shareTextWX(const char* kText, int nType);
	static void shareUrlWX(const char* kUrl, const char* kTitle, const char* kDesc, int nType);

	static void showWebView(const char* url);
	static void versionUpdate(const char* url, const char* desc, const int filesize, const int isUpdate);

	static void startSoundRecord();
	static const char* stopSoundRecord();

	static double getLatitude();
	static double getLongitude();

	static std::string getVersionName();
	static void copyToClipboard(const char* kText);
};

#endif // _gPlatform_H_