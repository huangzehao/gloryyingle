#include "Platform/PFView/LoginScene/LoginScene.h"
#include "Platform/PFView/ModeScene/ModeScene.h"
#include "Platform/PFView/ServerListScene/ServerListScene.h"
#include "Platform/PFView/ServerScene/ServerScene.h"
#include "Platform/PFDefine/data/ServerListData.h"
#include "Tools/tools/StringData.h"
#include "Tools/tools/MTNotification.h"
#include "Platform/PFDefine/data/GlobalUserInfo.h"
#include "Tools/tools/gPlatform.h"
#include "Tools/Dialog/NewDialog.h"
#include "Tools/ViewHeader.h"
#include "Kernel/socket/Socket.h"
USING_NS_CC;

extern void split(const std::string &s, char delim, std::vector<std::string> &elems);

//////////////////////////////////////////////////////////////////////////
class MTNotificationImp : public cocos2d::Node
{
private:
	int mDefaultModeSelected;

public:
	MTNotificationImp()
	{
		static bool isRes = false;
		if (isRes) return;
		mDefaultModeSelected = 0;
		G_NOTIFY_REG("LOADING_COMPLETE", MTNotificationImp::loadCompleteNotify);	
		G_NOTIFY_REG("LOGON_COMPLETE", MTNotificationImp::logonCompleteNotify);	
		G_NOTIFY_REG("MODE_SELECTED", MTNotificationImp::modeSelectedNotify);	
		G_NOTIFY_REG("BACK_TO_SERVER_LIST", MTNotificationImp::backToServerListNofity);	
		G_NOTIFY_REG("CONNECT_SERVER", MTNotificationImp::connectServerNotify);	
		G_NOTIFY_REG("BACK_TO_SERVER", MTNotificationImp::backToServerNotify);	
		G_NOTIFY_REG("GAME_EXIT_TIMEOUT", MTNotificationImp::gameExitTimeout);

		// payment
		G_NOTIFY_REG("PAYMENT_DISABLE", MTNotificationImp::paymentDisable);
		G_NOTIFY_REG("PAYMENT_PRODUCT_NONE", MTNotificationImp::paymentProductNone);
		G_NOTIFY_REG("PAYMENT_PAY", MTNotificationImp::paymentPay);
		G_NOTIFY_REG("PAYMENT_PAYING", MTNotificationImp::paymentPaying);
		G_NOTIFY_REG("PAYMENT_PAY_FINISH", MTNotificationImp::paymentPayFinish);
		G_NOTIFY_REG("PAYMENT_PAY_FAILED", MTNotificationImp::paymentPayFailed);
		isRes = true;
	}
	~MTNotificationImp()
	{
		G_NOTIFY_UNREG("LOADING_COMPLETE");
		G_NOTIFY_UNREG("LOGON_COMPLETE");
		G_NOTIFY_UNREG("MODE_SELECTED");	
		G_NOTIFY_UNREG("BACK_TO_SERVER_LIST");	
		G_NOTIFY_UNREG("CONNECT_SERVER");	
		G_NOTIFY_UNREG("BACK_TO_SERVER");	
		G_NOTIFY_UNREG("GAME_EXIT_TIMEOUT");

		// payment
		G_NOTIFY_UNREG("PAYMENT_DISABLE");
		G_NOTIFY_UNREG("PAYMENT_PRODUCT_NONE");
		G_NOTIFY_UNREG("PAYMENT_PAY");
		G_NOTIFY_UNREG("PAYMENT_PAYING");
		G_NOTIFY_UNREG("PAYMENT_PAY_FINISH");
		G_NOTIFY_UNREG("PAYMENT_PAY_FAILED");
	}

public:
	void loadCompleteNotify(cocos2d::Ref* obj)
	{
		CCDirector::getInstance()->replaceScene(CCTransitionFade::create(0.5f,LoginScene::create()));  
	}

	void logonCompleteNotify(cocos2d::Ref* obj)
	{
		//CCDirector::getInstance()->popScene();
	}

	void modeSelectedNotify(cocos2d::Ref* obj)
	{
		EventCustom *event_ = (EventCustom*)obj;
		if (event_ == 0){
			return;
		}

		MTData* data = (MTData*)event_->getUserData();
		if (data == 0)
			return;

		CCAssert(data, "modeSelectedNotify");
		mDefaultModeSelected = data->mData1;
						
		switch (mDefaultModeSelected)
		{
		case 0://快速模式
			{
				CGameServerItemMap::iterator it = CServerListData::shared()->GetServerItemMapBegin();
				CGameServerItem* pGameServerItem = 0;
				std::vector<CGameServerItem*> servers;

				while ((pGameServerItem = CServerListData::shared()->EmunGameServerItem(it)))
				{
					std::vector<std::string> elems;
					split(pGameServerItem->m_GameServer.szServerName, '|', elems);
					if (elems.size() <= 3)
					{
						continue;
					}

					int iServerType = 0;
					int iType		= 0;
					int iRate		= 0;
					sscanf(elems[1].c_str(), "%d", &iServerType);
					sscanf(elems[2].c_str(), "%d", &iType);
					sscanf(elems[3].c_str(), "%d", &iRate);

					if (iRate == 0) iRate = 1;
	
					if ((iType == 1 && iRate == 1) || (iType == 0))
						servers.push_back(pGameServerItem);
				}

				if (servers.empty())
				{
					popup(SSTRING("system_tips_title"), SSTRING("none_quick_mode_room"));
					return;
				}
	
				int iRandIndex = rand() % servers.size();

				G_NOTIFY_D("CONNECT_SERVER", MTData::create(0,0,0,"","","",servers[iRandIndex]));
			}
			break;
		case 1:
			{
//				CCDirector::getInstance()->replaceScene(CCTransitionFade::create(0.5f,ServerListScene::create(mDefaultModeSelected)));  
				//这个实际销毁的是游戏场景  Scene * now_Scene = Director::getInstance()->getRunningScene();
				//< 弹出游戏场景aa
				    
				  cocos2d::Director * director = CCDirector::getInstance();

				  cocos2d::Scene * now_Scene = director->getRunningScene();
					
				
				if (now_Scene != nullptr)
				{
					director->popScene();
				}
				else
				{
					director->popScene();
					//director->runWithScene(CCTransitionFade::create(0.5f, ModeScene::create()));
				}
			
				G_NOTIFY("EXIT_SERVERSCENE");
		}
			break;
		case 2:
			{
				popup(SSTRING("system_tips_title"), SSTRING("none_match_mode_room"));
				return;
				CCDirector::getInstance()->popScene();
				G_NOTIFY("EXIT_SERVERSCENE");
				//CCDirector::getInstance()->replaceScene(CCTransitionFade::create(0.5f, ModeScene::create()));
				return;
				CGameServerItemMap::iterator it = CServerListData::shared()->GetServerItemMapBegin();
				CGameServerItem* pGameServerItem = 0;
				std::vector<CGameServerItem*> servers;

				while ((pGameServerItem = CServerListData::shared()->EmunGameServerItem(it)))
				{
					std::vector<std::string> elems;
					split(pGameServerItem->m_GameServer.szServerName, '|', elems);
					if (elems.size() <= 3)
					{
						continue;
					}

					int iServerType = 0;
					int iType		= 0;
					int iRate		= 0;
					sscanf(elems[1].c_str(), "%d", &iServerType);
					sscanf(elems[2].c_str(), "%d", &iType);
					sscanf(elems[3].c_str(), "%d", &iRate);

					if (iRate == 0) iRate = 1;
	
					if (iType == 2)
						servers.push_back(pGameServerItem);
				}

				if (servers.empty())
				{
					popup(SSTRING("system_tips_title"), SSTRING("none_match_mode_room"));
					return;
				}
	
				int iRandIndex = rand() % servers.size();

				G_NOTIFY_D("CONNECT_SERVER", MTData::create(0,0,0,"","","",servers[iRandIndex]));
			}
			break;
		}
	}

	void backToServerListNofity(cocos2d::Ref* obj)
	{
		switch (mDefaultModeSelected)
		{
		case 0://快速模式
			{
				   G_NOTIFY_D("LOGON_COMPLETE", 0);
			}
			return;
		}

		//ServerListScene* scene = ServerListScene::create(mDefaultModeSelected);
		Director * director = CCDirector::getInstance();
		ModeScene * scene = ModeScene::create();
		if (director->getRunningScene() == scene)
			return;
		else
		{
			CCDirector::getInstance()->popScene();
			G_NOTIFY("EXIT_SERVERSCENE");
		}
		//CCDirector::getInstance()->replaceScene(CCTransitionFade::create(0.5f,scene));  

		EventCustom *event_ = (EventCustom*)obj;
		if (event_ == 0){
			return;
		}

		MTData* data = (MTData*)event_->getUserData();
		if (data == 0)
			return;
		
		popup(data->mStr1.c_str(), data->mStr2.c_str(), 0, 0.6f, 0, 0, scene);
		
	}

	void connectServerNotify(cocos2d::Ref* obj)
	{
		EventCustom *event_ = (EventCustom*)obj;
		if (event_ == 0){
			return;
		}

		MTData* data = (MTData*)event_->getUserData();
		if (data == 0)
			return;

		CCAssert(data, "connectServerNotify");

		log("out out out---");
		
		Director * director = CCDirector::getInstance();
		ModeScene * scene = ModeScene::create();
		if (director->getRunningScene() == scene)
			return;
		else
		{
			CCDirector::getInstance()->popScene();
			G_NOTIFY("EXIT_SERVERSCENE");
		}
		
	}

	void backToServerNotify(cocos2d::Ref* obj)
	{
		CCDirector::getInstance()->popScene();
		G_NOTIFY("EXIT_SERVERSCENE");
	}
	
	void gameExitTimeout(cocos2d::Ref* obj)
	{
		log("out out out");

		CCDirector::getInstance()->getScheduler()->unscheduleSelector(schedule_selector(MTNotificationImp::onGameExitTimeout), this);
		CCDirector::getInstance()->getScheduler()->scheduleSelector(schedule_selector(MTNotificationImp::onGameExitTimeout), this, 0.0f, 1, 1.5f, false);
	}

	void onGameExitTimeout(float dt)
	{
		CCDirector::getInstance()->getScheduler()->unscheduleSelector(schedule_selector(MTNotificationImp::onGameExitTimeout), this);
		NewDialog::create(SSTRING("Auto_game_exit"), NewDialog::NONEBUTTON);
	}

	// payment
	void paymentDisable(cocos2d::Ref* obj)
	{
		// 关闭waiting
		//CCScene* scene = CCDirector::getInstance()->getRunningScene();
		//scene->removeChildByTag(TAG_WAITING, true);
		popup(SSTRING("system_tips_title"), SSTRING("payment_disable"));
	} // paymentDisable

	void paymentProductNone(cocos2d::Ref* obj)
	{
		//CCScene* scene = CCDirector::getInstance()->getRunningScene();
		//scene->removeChildByTag(TAG_WAITING, true);
		popup(SSTRING("system_tips_title"), SSTRING("payment_product_none"));
	} // paymentProductNone

	void paymentProductsFailed(cocos2d::Ref* obj)
	{
		//CCScene* scene = CCDirector::getInstance()->getRunningScene();
		//scene->removeChildByTag(TAG_WAITING, true);
		popup(SSTRING("system_tips_title"), SSTRING("payment_products_failed"));
	} // paymentProductsFailed

	void paymentPay(cocos2d::Ref* obj)
	{
		EventCustom *event_ = (EventCustom*)obj;
		if (event_ == 0){
			return;
		}

		MTData* data = (MTData*)event_->getUserData();
		if (data == 0)
			return;

		//if (getChildByTag(TAG_WAITING) != 0)
		//	return;

		//ShieldLayer* waiting = ShieldLayer::create(-140);
		//waiting->setTag(TAG_WAITING);

		//CCScene* scene = CCDirector::getInstance()->getRunningScene();
		//scene->addChild(waiting);

		//变量定义
		CGlobalUserInfo * pGlobalUserInfo=CGlobalUserInfo::GetInstance();
		tagGlobalUserData * pGlobalUserData=pGlobalUserInfo->GetGlobalUserData();
		
		char szUserId[32]={0};
		sprintf(szUserId,"%d", pGlobalUserData->dwUserID);
		platformOpenPayView(data->mData1, pGlobalUserData->szAccounts, pGlobalUserData->szNickName, szUserId);
	} // paymentPay

	void paymentPaying(cocos2d::Ref* obj)
	{
		popup(SSTRING("system_tips_title"), SSTRING("payment_paying"));
	} // paymentPaying

	void paymentPayFinish(cocos2d::Ref* obj)
	{
		//CCScene* scene = CCDirector::getInstance()->getRunningScene();
		//scene->removeChildByTag(TAG_WAITING, true);
		MTData* data = dynamic_cast<MTData*>(obj);

		if (!data)
			return;

		popup(SSTRING("system_tips_title"), SSTRING("payment_pay_finish"));
		G_NOTIFY_D("INSURE_QUERY", MTData::create(1));
	} // paymentPayFinish

	void paymentPayFailed(cocos2d::Ref* obj)
	{
		//CCScene* scene = CCDirector::getInstance()->getRunningScene();
		//scene->removeChildByTag(TAG_WAITING, true);

		MTData* data = dynamic_cast<MTData*>(obj);

		if (!data)
			return;
		popup(SSTRING("system_tips_title"), data->mStr1.c_str());
	} // paymentPayFailed
}; // MTNotificationImp



//////////////////////////////////////////////////////////////////////////
// 多线程共享消息队列

//////////////////////////////////////////////////////////////////////////
// 队列锁
class MTQueueLocker  
{  
	pthread_mutex_t* mutex;  

public:  
	MTQueueLocker(pthread_mutex_t* aMutex) : mutex(aMutex)  
	{  
		pthread_mutex_lock(mutex);  
	}  
	~MTQueueLocker()
	{  
		pthread_mutex_unlock(mutex);  
	}  
}; // MTQueueLocker

#define MTQueueLock(mutex) MTQueueLocker __locker__(mutex) 

//////////////////////////////////////////////////////////////////////////

// MyNotification* MyNotification::msInstance = 0;
// 
// MyNotification* MyNotification::getInstance()
// {
// 	if (msInstance==0)
// 		msInstance = new MyNotification();
// 	return msInstance;
// }
// 
// void MyNotification::purge()
// {
// 	CSocket::purge();
// 	if (msInstance)
// 		delete msInstance;
// 	msInstance = 0;
// }
// 
// //////////////////////////////////////////////////////////////////////////
// 
// MyNotification::MyNotification()
// {
// 	mImp = new MTNotificationImp();
// 	pthread_mutex_init(&mMutex, 0);  
// }
// 
// MyNotification::~MyNotification()
// {
// 	delete mImp;
// 
// 	{
// 		MTQueueLock(&mMutex);
// 
// 		while (!mDataQueues.empty())
// 		{
// 			MTNotifyData& mtData = mDataQueues.front();
// 			CC_SAFE_RELEASE(mtData.obj);
// 			mDataQueues.pop_front();
// 		}
// 	}
// 
// 	pthread_mutex_destroy(&mMutex);
// }
// 
// void MyNotification::postNotify(const std::string& name, cocos2d::Ref* obj)
// {
// 	MTNotifyData mtData;
// 	mtData.name = name;
// 	mtData.obj  = obj;
// 	CC_SAFE_RETAIN(obj);
// 	MTQueueLock(&mMutex);
// 	mDataQueues.push_back(mtData);
// }
// 
// void MyNotification::post(float dt)
// {
// 	MTQueueLock(&mMutex);
// 	while (!mDataQueues.empty())
// 	{
// 		MTNotifyData& mtData = mDataQueues.front();
// 		G_NOTIFY(mtData.name.c_str(), mtData.obj);
// 		CC_SAFE_RELEASE(mtData.obj);
// 		mDataQueues.pop_front();
// 	}
// 
// 	gTimestampManager.tick(dt);
// 	CSocket::post();
// }

MyNotification * MyNotification::g_instance;

MyNotification::MyNotification()
{
	
}

MyNotification::~MyNotification()
{
	if (mImp != nullptr)
		delete mImp;
	if (g_instance != nullptr)
		delete g_instance;
}

MyNotification * MyNotification::getInstance(){
	if (g_instance){
		return g_instance;
	}

	g_instance = new MyNotification();
	g_instance->initMTNotificationImp();
	return g_instance;
}

void MyNotification::initMTNotificationImp()
{
	mImp = new MTNotificationImp();
}

bool MyNotification::registerEvent(const std::string &name_, const std::function<void(EventCustom*)>& callback)
{
	Director::getInstance()->getEventDispatcher()->addCustomEventListener(name_, callback);

	return true;
}

bool MyNotification::unRegisterEventListener(const std::string& customEventName)
{
	Director::getInstance()->getEventDispatcher()->removeCustomEventListeners(customEventName);

	return true;
}

void MyNotification::dispatchEvent(const std::string &name, void* data)
{
	Director::getInstance()->getEventDispatcher()->dispatchCustomEvent(name, data);

	return;
}

void MyNotification::post(float dt)
{
	CSocket::post();
}
