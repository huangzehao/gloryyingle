#include "SimpleTools.h"
#include "Platform/PlatformHeader.h"
#include "Tools/Manager/SpriteHelper.h"
#include "DntgGame/DntgDefine/Game/DntgBirdDefine.h"
#include "DntgGame/DntgDefine/Game/DntgCMD_CatchBird.h"

bool SimpleTools::isDntgSpecialBird(int birdType)
{
	switch (birdType)
	{
	case BIRD_TYPE_CHAIN:
	case BIRD_TYPE_RED:
	case BIRD_TYPE_ONE:
	case BIRD_TYPE_TWO:
	case BIRD_TYPE_THREE:
	case BIRD_TYPE_FOUR:
	case BIRD_TYPE_FIVE:
	case BIRD_TYPE_INGOT:
		return true;
		break;
	default:
		return false;
		break;
	}
}

bool SimpleTools::isDntgSpecialRoundBird(int birdType)
{
	switch (birdType)
	{
	case BIRD_TYPE_ONE:
	case BIRD_TYPE_TWO:
	case BIRD_TYPE_THREE:
	case BIRD_TYPE_FOUR:
	case BIRD_TYPE_FIVE:
		return true;
		break;
	default:
		return false;
		break;
	}
}

bool SimpleTools::isDntgNeedGoStraightBird(int birdType)
{
	switch (birdType)
	{
	case BIRD_TYPE_23:
	case BIRD_TYPE_24:
	case BIRD_TYPE_26:
	case BIRD_TYPE_27:
	case BIRD_TYPE_28:
	case BIRD_TYPE_29:
		return true;
		break;
	default:
		return false;
		break;
	}
}

bool SimpleTools::isDntgReverseAtGoStraightBird(const Vec2 & start_p, const Vec2 & end_p)
{
	Vec2 result_p = end_p - start_p;
	if (result_p.x >= 0.0f)
	{
		return false;
	}
	else if (result_p.x < 0.0f)
	{
		return true;
	}

	return true;
}

bool SimpleTools::isDntgDragonKing(int birdType)
{
	switch (birdType)
	{
		//case BIRD_TYPE_24:
		//case BIRD_TYPE_25:
		//case BIRD_TYPE_26:
		//case BIRD_TYPE_27:
		return false;
		break;
	default:
		return false;
		break;
	}
}

float SimpleTools::calcRotate(int char_id, const Dntg::xPoint& pt_offset)
{
	float rorate;
	Dntg::xPoint pt_bow = Dntg::xPoint(Dntg::CANNON_POSITION[char_id][0], Dntg::CANNON_POSITION[char_id][1]);
	Dntg::xPoint pt_rorate;

	if (char_id == 0 || char_id == 1 || char_id == 2)
	{
		pt_rorate = pt_offset - pt_bow;

		if (pt_rorate.y_ >= 0)
		{
			rorate = std::atan2(pt_rorate.y_, pt_rorate.x_) + M_PI_2;
		}
		else
		{
			if (pt_rorate.x_ >= 0)
				rorate = (float)M_PI_2;
			else
				rorate = (float)(M_PI_2 + M_PI);
		}
	}
	else if (char_id == 3 || char_id == 4 || char_id == 5)
	{
		pt_rorate = pt_offset - pt_bow;

		if (pt_rorate.y_ <= 0)
		{
			rorate = std::atan2(pt_rorate.y_, pt_rorate.x_) + M_PI_2;
		}
		else
		{
			if (pt_rorate.x_ >= 0)
				rorate = (float)M_PI_2;
			else
				rorate = (float)(M_PI_2 + M_PI);
		}
	}

	return rorate;
}
