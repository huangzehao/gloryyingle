#include "PlayerNode_Baccarat.h"
#include "Tools/tools/YRCommon.h"

PlayerNode_Baccarat::PlayerNode_Baccarat()
{
}

PlayerNode_Baccarat::~PlayerNode_Baccarat()
{
}

bool PlayerNode_Baccarat::init(int chair_id, Widget *mRoot)
{
	if (!Node::init())
	{
		return false;
	}

	if (chair_id == 0)
	{
		img_PlayerNode = dynamic_cast<ImageView *>(mRoot->getChildByName("Panel_Main")->getChildByName("img_down_layer"));
		img_head = dynamic_cast<ImageView *>(img_PlayerNode->getChildByName("img_head"));
	}
	else
		img_PlayerNode = dynamic_cast<ImageView *>(mRoot->getChildByName("Panel_Main")->getChildByName("img_top_layer"));

	lab_name = dynamic_cast<Text *>(img_PlayerNode->getChildByName("lab_name"));
	al_score = dynamic_cast<TextAtlas *>(img_PlayerNode->getChildByName("al_score"));

	return true;
}

PlayerNode_Baccarat * PlayerNode_Baccarat::create(int chard_id, Widget *mRoot)
{
	PlayerNode_Baccarat * node = new PlayerNode_Baccarat;
	if (node && node->init(chard_id, mRoot))
	{
		node->autorelease();
		return node;
	}
	delete node;
	return nullptr;
}

void PlayerNode_Baccarat::setNickName(const char * name)
{
	lab_name->setString(name);
}

void PlayerNode_Baccarat::setMoney(long long money)
{
	al_score->setString(StringUtils::format("%lld", money));
}

void PlayerNode_Baccarat::clearUser(bool isLevel)
{
	if (isLevel)
	{
		lab_name->setString("");
		al_score->setString("");
	}
}

Text * PlayerNode_Baccarat::getNickName()
{
	return lab_name;
}

TextAtlas * PlayerNode_Baccarat::getMoney()
{
	return al_score;
}

void PlayerNode_Baccarat::setFaceID(int wFaceID)
{
	img_head->loadTexture(YRComGetHeadImageById(wFaceID));
}

