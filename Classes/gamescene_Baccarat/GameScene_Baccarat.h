#ifndef _GameScene_BACCARAT_H_
#define _GameScene_BACCARAT_H_

#include "cocos2d.h"
#include "cocos-ext.h"
#include "Gamelogic_Baccarat.h"
#include "Kernel/kernel/user/IClientUserItem.h"
#include "CMD_Baccarat.h"
#include "common/IKeybackListener.h"
#include "common/TouchLayer.h"
#include "common/SpriteCard.h"
#include "Tools/tools/MessageLayer.h"
#define GAME_PLAYER_Ox 5

USING_NS_CC;
USING_NS_CC_EXT;
class CGameClientView;
class PlayerLayer_Baccarat;
class FrameLayer_Baccarat;
class tagApplyUser_Baccarat;

using namespace Message;

class GameScene_Baccarat 
	: public Scene
	, public ITouchSink
	,public IKeybackListener
{
public:
	//创建方法
	CREATE_FUNC(GameScene_Baccarat);

	//构造函数
	GameScene_Baccarat();

	~GameScene_Baccarat();

	//初始化方法
	virtual bool init();

	static GameScene_Baccarat* shared();

	CGameLogic_Baccarat*	m_pGameLogic;
	///< 初始化游戏基础数据
	static void initGameBaseData();
	 GLView *_openGLView;

	void onEnter();
	void onExit();
	virtual void onKeybackClicked();
	void onBackToRoom(Node* node);

	CCPoint			m_UserPoint[GAME_PLAYER_Ox];			//玩家位置
	CCPoint			m_UserScorePoint[GAME_PLAYER_Ox];			//玩家位置

	CCPoint			m_IdleScorePoint[GAME_PLAYER_Ox];		//闲家注码位置
	CCPoint         m_UserAddedPoint[GAME_PLAYER_Ox];      //用户已加注筹码		
	CCPoint			m_BankerPoint[GAME_PLAYER_Ox];			//庄家位置

	CCPoint			m_OpenFontPoint[GAME_PLAYER_Ox];			//tanpai
	CCPoint			m_OxPoint[GAME_PLAYER_Ox];

	Sprite			*m_pBackground;
	Sprite*			Background;

public:

	//场景动画完成
	virtual void onEnterTransitionDidFinish();

	//场景动画开始
	virtual void onExitTransitionDidStart();

	//震动屏幕
	void OnShakePreen(float m_ptOffest_x,float m_ptOffest_y);

	// 添加消息列表
	void add_msg(const std::string& msg);

	//消息显示
	void func_msg_show();

	//读取下一条消息
	void func_msg_start_next();

	//显示下一条消息
	void msg_start_next(bool bRemoveFront);

	void setMeUserClock(int time);



 	void ShowAddButton(int64	Score,bool IsCanShand,bool ShowHand);
 	void FistScore(int64	Score,int userIndex,bool Tybe);
 	void showAddScoreTips(int64	Score,int userIndex,bool Tybe);
	void ccButtonUserReday(Ref* obj,Control::EventType e);

 	void ccButtonAdd0(Ref* obj,Control::EventType e);
 	void ccButtonAdd1(Ref* obj,Control::EventType e);
 	void ccButtonAdd2(Ref* obj,Control::EventType e);
 	void ccButtonAdd3(Ref* obj,Control::EventType e);
 	void ccButtonAdd4(Node* sender);
 	void ccButtonAdd5(Node* sender);
	void ShowTime(int LTime);

	void showUserInfo(IClientUserItem *pIClientUserItem, byte cbUserStatus);
	void showGameEndScore(int userName1,int64 lScore1, int type,int conunt);


	///< 准备状态(空闲状态)
	void createButtonUserReday();
	///< 准备状态(空闲状态)
	void freeState();
	///< 叫庄状态
	void callBankerState(int chair_id);
	///< 下注状态
	void addGoldState(int chair_id, long long maxValue);
	///< 游戏状态
	void playingState(int addGoldChair_id, long long addGoldScore);
	void playingState(bool me_alery_showhard);

	///<开启开牌舞台
	void ShowCardStage(byte	m_cbTableCardArray[2][3], int XianNum, int ZhuangNum, int mTimeLeave, int state);
	///< 发送第一张卡,显示先发谁
	void sendCardFirst(byte bcCard);
	///< 发送卡信息
	void sendCardStart(byte	m_cbTableCardArray[2][3], int XianNum, int ZhuangNum, int state);
	void ShowMeCard(Node* sender);
	void showCallBankerButton();
	void ccButtonUserCall(Ref* obj,Control::EventType e);
	void ccButtonUserNoCall(Ref* obj,Control::EventType e);
	void showBankerWithIndex(int Index);
	void showTanpaiButton();
	void ccButtonUser(Ref* obj,Control::EventType e);
	void ccButtonUserNo(Ref* obj,Control::EventType e);
private:
	void startUpdate();
	void stopUpdate();
	void frameUpdate(float dt);
	void closeCallback(cocos2d::Node* obj);

private:
	//定时器
	void func_send_time_check(float dt);
	Point locationFromTouch(Touch* touch);

	void onBtnClick(Ref* obj,Control::EventType e);

	///< 断线重连
	void func_Reconnect_on_loss(cocos2d::Ref * obj);
	void reconnect_on_loss(float dt);
public:
	///< 是断线重连
	static bool is_Reconnect_on_loss();
public:
	virtual bool onTouchBegan(Touch *pTouch, Event *pEvent);
	virtual void onTouchMoved(Touch *pTouch, Event *pEvent);
	virtual void onTouchEnded(Touch *pTouch, Event *pEvent);

private:
	CGlobalUserInfo *	pGlobalUserInfo;	//用户信息
	tagUserInsureInfo*	pUserInsureInfo;	//用户账户信息
	tagGlobalUserData*  pGlobalUserData;	//获取奖牌数
	Sprite*				spMessage_bg;		//系统消息
	Label*				mMsgContents[2];	//消息内容
	int					mMsgContentIndex;	//消息内容索引
	bool				is_touch_fire_;		//是否开炮		

	Label*				m_NickName;			//玩家昵称
	Label*				coinRate ;			//渔币兑换金币比率
	Label*				mAwardNum;			//奖牌数量

	byte cbPlayerPoint[3];  //闲家点数数组
	byte cbBankerPoint[3];	 //庄家点数数组
	byte m_CardArray[2][3];	//桌面扑克
	
	int cardCount;		//当前发第几张牌
	int cardSum;        //庄家和闲家手牌的总数
	int playerPoint;	//当前闲家点数
	int bankerPoint;	//当前庄家点数
	int gameWinner;		//游戏赢家结果
	byte cbKingWinner;	//是否天王
	bool bPlayerTwoPair;//是否闲对子
	bool bBankerTwoPair;//是否庄对子
	int  m_state;
	
	TouchLayer*		    touch_layer_;		//触摸层	
	//按钮
 	ControlButton*	    close_bt;			// 关闭按钮

// 	ControlButton*		addCannonMulti;		//加炮
// 	ControlButton*		subCannonMulti;		//减炮
// 	//ControlButton*		GMControl_bt;		//GM控制按钮
public:
	typedef std::list<std::string>		MSG_LIST;			//消息列表
	typedef MSG_LIST::iterator			MSG_LIST_ITER;	
	MSG_LIST							mMessageList;		//消息列表对象

	CGameClientView*					mGameView;
	PlayerLayer_Baccarat*				mPlayerLayer;	///< 玩家层
	FrameLayer_Baccarat*				mFrameLayer;	///< 框架层
	MessageLayer*						mMessageLayer;	///< 消息层
	CSpriteCard    m_Card;
	std::vector<CSpriteCard *>			mCardList; ///< 所有卡的容器

	std::vector<Sprite *>				mChipList;
public:
	void net_user_state_update(int chair_id, byte user_state);
	///< 用户离开
	void net_user_level(int chair_id);
public:
	void clearPlayerPrepare();
	///< 显示庄家标志和框
	void showBankerIconAtPlayerNode(int chairId);
	///< 清理其他用户
	void clearOtherUser(int chair_id);
	///< 清理牌数据
	void clearCardData(int chair_id);
	///< 退出游戏
	void closeGameNotify(cocos2d::Ref * ref);
	///< 显示结算框
	void showSettlementFrame(int chair_id, bool isShow);

	///< 显示库存
	void showStorage(long long money);
	///< 清理筹码
	void clearUserJetton();
	///< 清理扑克
	void clearCard();
	///< 清理对应的扑克
	void clearCardByArea(byte onArea);
	///< 设置为等待界面状态
	void setWindowsIsWaitState();
	///< 设置时间logo状态
	//void setTimeState(bool freeTime,bool betTime,bool openTime);
	void setTimeState(int timeLableType);
	///< 设置庄家信息
	void SetBankerInfo(dword dwBankerUserID, int64 lBankerScore);
	///< 设置为下注界面状态
	void setWindowsIsBetState();
	///< 区域增加下注
	void onAreaAddJetton(word wChairID, byte cbJettonArea, int64 lJettonScore);
	///< 区域增加全部下注额
	void onAreaTotalJetton(byte cbJettonArea, int64 lJettonScore);
	///< 区域增加个人下注
	void SetMePlaceJetton(short cbViewIndex, long long lJettonCount);
	///< 增加用户到我要坐庄面板
	void addUserToBankerPanel(tagApplyUser_Baccarat tAuser);
	///< 减少用户到我要坐庄面板
	void deleteUserToBankerPanel(tagApplyUser_Baccarat tAuser);
	///< 插入用户到坐庄面板
	void insertUserToBankerPanel(tagApplyUser_Baccarat tAuser);
	//设置扑克
	void SetCardInfo(byte cbTableCardArray[5][5]);
	//庄家成绩
	void SetBankerScore(word wBankerTime, int64 lWinScore);
	///结算界面显示
	void SetCurGameScore(int64 lPlayScore[8], int64 lPlayAllScore);
	///< 设置路子面板数据
	void SetGameHistory(byte cbKingWinner, bool bPlayerTwoPair, bool bBankerTwoPair, byte cbPlayerCount, byte cbBankerCount);
	///< 加注区域,加注
	//设置筹码
	void PlaceUserJetton(byte cbViewIndex, int64 lScoreCount);
	///< 设置状态为游戏结束状态
	void setWindowsIsGameEndState();
	///< 检测玩家金币等级
	void checkUserScore();
	///< 检测用户是第几级别
	int checkUserMoneyGrade(int64 pUserMoney);
	///< 倒计时退出游戏
	void  ExitGameTiming();
	///< 注销内核退出游戏
	void  CloseKernelExitGame(float dt);
	//庄家信息
protected:
	word							m_wBankerUser;						//当前庄家
	word							m_wBankerTime;						//做庄次数
	int64							m_lBankerScore;						//庄家积分
	int64							m_lBankerWinScore;					//庄家成绩	
	int64							m_lTmpBankerWinScore;				//庄家成绩	
	bool							m_bEnableSysBanker;					//系统做庄

	float mPassTime;

};
#endif // _GameScene_SH2_H_