#ifndef PLAYERNODE_BACCARAT_H
#define PLAYERNODE_BACCARAT_H

#include "cocos2d.h"
#include "cocostudio/CocoStudio.h"
#include "ui/CocosGUI.h"

USING_NS_CC;
using namespace cocos2d::ui;
using namespace cocostudio;

class PlayerNode_Baccarat : public Node
{
public:
	PlayerNode_Baccarat();
	~PlayerNode_Baccarat();

	bool init(int chair_id, Widget *mRoot);
	static PlayerNode_Baccarat * create(int chard_id, Widget *mRoot);

	//设置头像
	void setFaceID(int wFaceID);
	void setNickName(const char * name);
	void setMoney(long long money);
	Text * getNickName();
	TextAtlas * getMoney();

	void clearUser(bool isLevel);
protected:
	///< 玩家名字
	Text * lab_name;
	///< 玩家分数
	TextAtlas * al_score;
	///< 玩家头像
	ImageView * img_head;
	///< 玩家节点
	ImageView * img_PlayerNode;
};

#endif

