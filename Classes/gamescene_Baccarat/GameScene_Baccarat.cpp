#include "ClientKernelSink_Baccarat.h"
#include "Tools/tools/MTNotification.h"
#include "GameScene_Baccarat.h"
#include "common/KeybackLayer.h"
#include "PlayerLayer_Baccarat.h"
#include "FrameLayer_Baccarat.h"
#include "Tools/tools/SimpleTools.h"
#include "Tools/ViewHeader.h"
#include "Tools/Manager/SoundManager.h"
#include "Tools/tools/gPlatform.h"
#include "ApplyUserList_Baccarat.h"
#include "Tools/tools/StringData.h"
#include "Kernel/kernel/game/IClientKernel.h"
#include "Tools/tools/StaticData.h"
#include "Kernel/kernel/server/IServerItemSink.h"
#include "Tools/Dialog/NewDialog.h"
#include "Platform/PFView/ServerScene/ServerScene.h"
#include "Platform/PFView/ModeScene/ModeScene.h"
#define  P_WIDTH      1420
#define  P_WIDTH_2    P_WIDTH/2

#define  P_HEIGHT     800
#define  P_HEIGHT_2   P_HEIGHT/2

#define TAG_BT_CLOSE			1
#define TAG_BT_ADD_CANNON_MULTI	2
#define TAG_BT_SUB_CANNON_MULTI	3
#define TAG_BT_ADD_CANNON_SCORE	4
#define TAG_BT_SUB_CANNON_SCORE	5
#define TAG_BT_GMCTR			6

bool isBaccaratReconnectOnLoss = false;
//////////////////////////////////////////////////////////////////////////
static GameScene_Baccarat* __gGameScene_Baccarat = 0;

GameScene_Baccarat* GameScene_Baccarat::shared()
{
	return __gGameScene_Baccarat;
}
//////////////////////////////////////////////////////////////////////////
GameScene_Baccarat::GameScene_Baccarat()
{
	__gGameScene_Baccarat = this;
	mMsgContentIndex = 0;
	mPassTime = 0;

	char sSound[32] = { 0 };
	sprintf(sSound, "BGM_NORMAL_1");
	SoundManager::shared()->playMusic(sSound);
	touch_layer_ = nullptr;
	m_wBankerUser = INVALID_CHAIR;
}

GameScene_Baccarat::~GameScene_Baccarat()
{
	__gGameScene_Baccarat = 0;
	if(touch_layer_)
	{
	touch_layer_->setTouchEnabled(false);
	touch_layer_->setSink(0);
	touch_layer_->release();
	}

	char sSound[32] = { 0 };
	sprintf(sSound, "BGM_NORMAL_1");
	SoundManager::shared()->stopMusic();

	G_NOTIFY_UNREG("GAME_CLOSE");
	G_NOTIFY_UNREG("EVENT_GAME_EXIT");

	mFrameLayer = nullptr;
	mPlayerLayer = nullptr;

	SoundManager::shared()->stopMusic();
	PLAY_PLATFORM_BG_MUSIC
}

//初始化方法
bool GameScene_Baccarat::init()
{
	do 
	{	
		CC_BREAK_IF(!Scene::init());
		
		addChild(KeybackLayer::create());

		is_touch_fire_ = false;

		int iZorder = -500;

		float baseDistance = 140;
		m_OxPoint[0] = ccp(620, 450);
		for (int i = 1; i < 5; i++)
		{
			m_OxPoint[i] = ccp(baseDistance + 260 * (i - 1), 100);
		}

		//1.解析主界面json
		Widget * mRoot = cocostudio::GUIReader::getInstance()->widgetFromJsonFile("baccaratGameScene/Baccarat_Main.json");

		//2.主界面层
		mFrameLayer = FrameLayer_Baccarat::create(mRoot);
		//< 有5张牌.每张牌的z轴都不同.
		this->addChild(mFrameLayer, 5);
		mFrameLayer->setCloseDialogInfo(this, callfuncN_selector(GameScene_Baccarat::closeCallback), SSTRING("back_to_room"), SSTRING("back_to_room_content"));

		//3.玩家层
		mPlayerLayer = PlayerLayer_Baccarat::create(mRoot);
		this->addChild(mPlayerLayer,10);
	
		mMessageLayer = MessageLayer::create();
		mMessageLayer->setName("mMessageLayer");
		addChild(mMessageLayer,100);

		auto kernel = IClientKernel::get();
		if (kernel)
		{
			kernel->SetChatSink(mMessageLayer);
			kernel->SetStringMessageSink(mMessageLayer);
		}

		CSpriteCard::setAutoScaleSize(1);
 		m_Card.setCardArrayWithRow("baccaratGameScene/Baccarat_Main/game_card-hd_baccarat.png", CSpriteCard::g_cbCardData, 55);

		SoundManager::shared()->playMusic("BACCARAT_BACK_GROUD");
		SoundManager::shared()->setMusicVolume(40);
		SoundManager::shared()->setSoundVolume(80);

		///< 关闭游戏
		G_NOTIFY_REG("EVENT_GAME_EXIT", GameScene_Baccarat::closeGameNotify);
		///< 断线重连
		G_NOTIFY_REG("RECONNECT_ON_LOSS", GameScene_Baccarat::func_Reconnect_on_loss);

		this->setName("GameScene");
		this->setTag(KIND_ID);

		return true;
	} while (0);

	return false;
}
void GameScene_Baccarat::createButtonUserReday()
{
// 	ControlButton  *btnStart = createButton2("SH2/game_bt_ready-hd.png", 0, 0, this, cccontrol_selector(GameScene_Baccarat::ccButtonUserReday));
// 	btnStart->setPosition(Vec2(P_WIDTH / 2, P_HEIGHT / 2));
// 	addChild(btnStart,1000);

	PlayerNode_Baccarat * node = mPlayerLayer->getPlayerNodeById(1);
	node->clearUser(false);

	node = mPlayerLayer->getPlayerNodeById(0);
	node->clearUser(false);

	mFrameLayer->showMenuItem(FrameLayer_Baccarat::ITEM_TAG_START, true);
	mFrameLayer->showMenuItem(FrameLayer_Baccarat::ITEM_TAG_REQUIRE_BANKSER, false);
	mFrameLayer->showMenuItem(FrameLayer_Baccarat::ITEM_TAG_SHOW_HARD, false);
	mFrameLayer->showMenuItem(FrameLayer_Baccarat::ITEM_TAG_ABORT, false);

}

void GameScene_Baccarat::freeState()
{
	// 	ControlButton  *btnStart = createButton2("SH2/game_bt_ready-hd.png", 0, 0, this, cccontrol_selector(GameScene_Baccarat::ccButtonUserReday));
	// 	btnStart->setPosition(Vec2(P_WIDTH / 2, P_HEIGHT / 2));
	// 	addChild(btnStart,1000);
	mFrameLayer->showMenuItem(FrameLayer_Baccarat::ITEM_TAG_START, true);
	mFrameLayer->showMenuItem(FrameLayer_Baccarat::ITEM_TAG_REQUIRE_BANKSER, false);
	mFrameLayer->showMenuItem(FrameLayer_Baccarat::ITEM_TAG_SHOW_HARD, false);
	mFrameLayer->showMenuItem(FrameLayer_Baccarat::ITEM_TAG_ABORT, false);
}

void GameScene_Baccarat::callBankerState(int chair_id)
{
	IClientKernel * kernel = IClientKernel::get();

	if (kernel->GetMeChairID() == chair_id)
	{
		mFrameLayer->showMenuItem(FrameLayer_Baccarat::ITEM_TAG_START, false);
		mFrameLayer->showMenuItem(FrameLayer_Baccarat::ITEM_TAG_REQUIRE_BANKSER, true);
		mFrameLayer->showMenuItem(FrameLayer_Baccarat::ITEM_TAG_SHOW_HARD, false);
		mFrameLayer->showMenuItem(FrameLayer_Baccarat::ITEM_TAG_ABORT, true);
	}
}

void GameScene_Baccarat::addGoldState(int chair_id, long long maxValue)
{
	IClientKernel * kernel = IClientKernel::get();

	if (kernel->GetMeChairID() == chair_id)
	{
	mFrameLayer->showMenuItem(FrameLayer_Baccarat::ITEM_TAG_START, false);
	mFrameLayer->showMenuItem(FrameLayer_Baccarat::ITEM_TAG_REQUIRE_BANKSER, false);
	mFrameLayer->showMenuItem(FrameLayer_Baccarat::ITEM_TAG_SHOW_HARD, false);
	mFrameLayer->showMenuItem(FrameLayer_Baccarat::ITEM_TAG_ABORT, false);
	mFrameLayer->setAddScoreMaxValue(maxValue);
	}
}

void GameScene_Baccarat::playingState(int addGoldChair_id, long long addGoldScore)
{
	IClientKernel * kernel = IClientKernel::get();

	if (kernel)
	{
		mFrameLayer->showMenuItem(FrameLayer_Baccarat::ITEM_TAG_START, false);
		mFrameLayer->showMenuItem(FrameLayer_Baccarat::ITEM_TAG_REQUIRE_BANKSER, false);
		mFrameLayer->showMenuItem(FrameLayer_Baccarat::ITEM_TAG_SHOW_HARD, false);
		mFrameLayer->showMenuItem(FrameLayer_Baccarat::ITEM_TAG_SHOW_HARD, true);
		mFrameLayer->showMenuItem(FrameLayer_Baccarat::ITEM_TAG_ABORT, false);

		PlayerNode_Baccarat * node = nullptr;
		if (kernel->GetMeChairID() == addGoldChair_id)
		{
			node = mPlayerLayer->getPlayerNodeById(0);

		}
		else
		{
			node = mPlayerLayer->getPlayerNodeById(1);
		}

	}
}

void GameScene_Baccarat::playingState(bool me_alery_showhard)
{
	mFrameLayer->showMenuItem(FrameLayer_Baccarat::ITEM_TAG_SHOW_HARD, !me_alery_showhard);
}

void GameScene_Baccarat::ccButtonUserReday(Ref* obj,Control::EventType e)
{
// 	Control* ctr = (Control*)obj;
// 	ctr->setVisible(false);
// 
// 	IClientKernel* kernel = IClientKernel::get();
// 	kernel->SendUserReady(NULL, 0);
// 
// 	gClientKernelSink_Ox.OnStartremove(50);///删除开始计时器
// 	removeChildByTag(1212); //删除结算图
// 	for (int index=0;index<GAME_PLAYER_Ox;index++)
// 	{
// 		removeChildByTag(600+index);
// 		removeChildByTag(193+index);//删除强退
// 		removeChildByTag(210+index);
// 	}
// 	for (byte j=0;j<100;j++)
// 	{
// 		removeChildByTag(8520+j);
// 	}
// 	removeChildByTag(201);
// 	removeChildByTag(202);
// 	removeChildByTag(203);
}
void GameScene_Baccarat::ShowTime(int LTime)
{
	removeChildByTag(48);
	char strDescribe[256];
	sprintf(strDescribe, "%d",LTime); 
	Label *pLableUserScore = Label::createWithSystemFont(strDescribe, "Arial", 30);
	pLableUserScore->setTag(48);
	pLableUserScore->setPosition(ccp(P_WIDTH_2, P_HEIGHT_2));
	addChild(pLableUserScore);
}
void GameScene_Baccarat::showUserInfo(IClientUserItem *pIClientUserItem, byte cbUserStatus)
{
	IClientKernel* kernel = IClientKernel::get();
// 	IClientUserItem* pClientUserItem= kernel->GetTableUserItem(ChairID);

	PlayerNode_Baccarat * node = nullptr;
	if (pIClientUserItem->GetChairID() == kernel->GetMeChairID())
		node = mPlayerLayer->getPlayerNodeById(0);
	else
	{
		return;
	}
	//	node = mPlayerLayer->getPlayerNodeById(1);

	node->setNickName(pIClientUserItem->GetNickName());
	node->setMoney(pIClientUserItem->GetUserScore());

	if (pIClientUserItem->GetUserStatus() == US_READY)
	{

		showSettlementFrame(pIClientUserItem->GetChairID(), false);
	}	
	
	//设置玩家信息
	node->setFaceID(pIClientUserItem->GetFaceID());
	///< 清理牌信息
	this->clearCardData(pIClientUserItem->GetChairID());

// 	Label *pLableNickName = Label::createWithSystemFont(pClientUserItem->GetNickName(), "Arial", 35);
// 	pLableNickName->setPosition(m_UserPoint[kernel->SwitchViewChairID(ChairID)]);
// 	addChild(pLableNickName);
// 
// 	char strDescribe[256];
// 	sprintf(strDescribe, "%lld",pClientUserItem->GetUserScore()); 
// 	Label *pLableUserScore = Label::createWithSystemFont(strDescribe, "Arial", 35);
// 	pLableUserScore->setPosition(m_UserScorePoint[kernel->SwitchViewChairID(ChairID)]);
// 	addChild(pLableUserScore);
// 
// 	Sprite* Reary = Sprite::create("SH2/READY.png");
// 	if (cbUserStatus==3)
// 	{
// 
// 		Reary->setPosition(Vec2((m_UserPoint[kernel->SwitchViewChairID(ChairID)].x)-100, m_UserPoint[kernel->SwitchViewChairID(ChairID)].y));
// 		addChild(Reary);
// 	}
// 	if (kernel->SwitchViewChairID(ChairID) != 0)	//不是自己
// 	{
// 		pLableNickName->setTag(100+kernel->SwitchViewChairID(ChairID));
// 		pLableUserScore->setTag(110+kernel->SwitchViewChairID(ChairID));
// 		Reary->setTag(120+kernel->SwitchViewChairID(ChairID));
// 	}

}
void GameScene_Baccarat::FistScore(longlong	Score,int userIndex,bool Tybe)
{
	if(Tybe==false)
	{
		removeChildByTag(200+userIndex);
		return;
	}
// 	CSpriteLabel *pLableUserScore1 = CSpriteLabel::spriteLabelWithString("", FONT_ARIAL_HOLD, 35);
// 	pLableUserScore1->setAnchorPoint(ccp(0.0f, 0.5f));
// 	pLableUserScore1->setTag(200+userIndex);
// 	pLableUserScore1->setLabelNumber(Score);
// 	pLableUserScore1->setPosition(m_UserAddedPoint[userIndex]);
// 	addChild(pLableUserScore1);
}
void GameScene_Baccarat::showBankerWithIndex(int Index)
{
	Label *pLableUserScore1 = Label::createWithSystemFont("庄家", "Arial", 40);
	pLableUserScore1->setTag(203);
	pLableUserScore1->setAnchorPoint(ccp(0.0f, 0.5f));
	pLableUserScore1->setPosition(m_BankerPoint[Index]+Vec2(500,0));
	addChild(pLableUserScore1);

}
void GameScene_Baccarat::ShowAddButton(longlong	Score,bool IsCanShand,bool ShowHand)
{
	mFrameLayer->setAddScoreMaxValue(Score);
// 	for(int i=0;i<4;i++)
// 	{
// 		ui::Scale9Sprite* sptNormal	= ui::Scale9Sprite::create("SH2/game_bt_addscorebg-hd.png");
// 
// 		ControlButton* btn = ControlButton::create();
// 		btn->setBackgroundSpriteForState(sptNormal, Control::State::NORMAL);
// 		btn->setPreferredSize(sptNormal->getPreferredSize());
// 
// 		int	pty=P_HEIGHT_2-10;
// 		int	ptx=100+i*120;
// 		switch (i)
// 		{
// 		case 0:
// 			{
// 				char strDescribe[256];
// 				sprintf(strDescribe, "%lld",Score/8); 
// 				btn->setTitleForState(strDescribe, Control::State::NORMAL);
// 				btn->setTag(150);
// 				btn->addTargetWithActionForControlEvents(this, cccontrol_selector(GameScene_Baccarat::ccButtonAdd0), Control::EventType::TOUCH_UP_INSIDE);
// 				break;
// 			}
// 		case 1:
// 			{
// 				char strDescribe[256];
// 				sprintf(strDescribe, "%lld",Score*4); 
// 				btn->setTitleForState(strDescribe, Control::State::NORMAL);
// 				btn->setTag(151);
// 				btn->addTargetWithActionForControlEvents(this, cccontrol_selector(GameScene_Baccarat::ccButtonAdd1), Control::EventType::TOUCH_UP_INSIDE);
// 				break;
// 			}
// 		case 2:
// 			{
// 				char strDescribe[256];
// 				sprintf(strDescribe, "%lld",Score*2); 
// 				btn->setTitleForState(strDescribe, Control::State::NORMAL);
// 				btn->setTag(152);
// 				btn->addTargetWithActionForControlEvents(this, cccontrol_selector(GameScene_Baccarat::ccButtonAdd2), Control::EventType::TOUCH_UP_INSIDE);
// 				break;
// 			}
// 		case 3:
// 			{
// 				char strDescribe[256];
// 				sprintf(strDescribe, "%lld",Score); 
// 				btn->setTitleForState(strDescribe, Control::State::NORMAL);
// 				btn->setTag(153);
// 				btn->addTargetWithActionForControlEvents(this, cccontrol_selector(GameScene_Baccarat::ccButtonAdd3), Control::EventType::TOUCH_UP_INSIDE);
// 				break;
// 			}
// 		default:
// 			break;
// 		}
// 
// 		btn->setPosition(ccp(ptx, pty));
// 		addChild(btn);
// 
// 	}
}
void GameScene_Baccarat::showAddScoreTips(longlong	Score,int userIndex,bool Tybe)
{
// 	if(Tybe==false)
// 	{
// 		removeChildByTag(210+userIndex);
// 		return;
// 	}
// 	char strDescribe[256];
// 	sprintf(strDescribe, "%lld",Score); 
// 	Label *pLableUserScore1 = Label::createWithSystemFont(strDescribe, "Arial", 35);
// 	pLableUserScore1->setAnchorPoint(ccp(0.0f, 0.5f));
// 	pLableUserScore1->setTag(210+userIndex);
// 	pLableUserScore1->setPosition(m_IdleScorePoint[userIndex]);
// 	addChild(pLableUserScore1);
	IClientKernel* kernel = IClientKernel::get();
	PlayerNode_Baccarat* node = nullptr;
	if (kernel->GetMeChairID() == userIndex)
	{
		node = mPlayerLayer->getPlayerNodeById(0);
	}
	else
	{
		node = mPlayerLayer->getPlayerNodeById(1);
	}
}

void GameScene_Baccarat::showGameEndScore(int userName1,longlong lScore1, int type,int conunt)
{
	IClientKernel* kernel = IClientKernel::get();
	IClientUserItem* pClientUserItem1= kernel->GetTableUserItem(userName1);
	PlayerNode_Baccarat * node = nullptr;
	//if (pClientUserItem1)
	//{
	if (userName1 == kernel->GetMeChairID())
	{
		node = mPlayerLayer->getPlayerNodeById(0);
	}
	else
	{
		node = mPlayerLayer->getPlayerNodeById(1);
	}
//	node->addMoney(lScore1);

	if (userName1 == kernel->GetMeChairID())
	{
		if (lScore1 > 0)
			SoundManager::shared()->playSound("GAME_WIN");
		else
			SoundManager::shared()->playSound("GAME_LOST");
	}
	

	//}
// 	if(m_xxx==1)
// 	{
// 
// 		Background = Sprite::create("Ox/game_score_ret-hd4.png");
// 		Background->setPosition(ccp(P_WIDTH/2-30, P_HEIGHT/2-15));
// 		Background->setTag(1212);
// 		addChild(Background);
// 		m_xxx=2;
// 		if(pClientUserItem1!=NULL)
// 		{
// 			//玩家1名称
// 			Label *pLableUserScore1 = Label::createWithSystemFont(pClientUserItem1->GetNickName(), "Arial", 25);
// 			pLableUserScore1->setPosition(ccp(310,210));
// 			Background->addChild(pLableUserScore1);
// 
// 			int mark = lScore1>0?1:-1;
// 			//下注分数
// 			lScore1=llabs(lScore1);
// 
// 			char strDescribe[256];
// 			sprintf(strDescribe, "%lld",lScore1*mark); 
// 			Label *scoreNum1 = Label::createWithSystemFont(strDescribe, "Arial", 25);
// 			scoreNum1->setPosition(ccp(410,210));
// 			Background->addChild(scoreNum1);
// 
// 
// 			Sprite* scoretype1 = NULL;
// 			if (mark > 0)
// 			{
// 				scoretype1 =  Sprite::create("game_win-hd.png");
// 			}
// 			else
// 			{
// 				scoretype1 =  Sprite::create("game_lose-hd.png");
// 			}
// 			scoretype1->setPosition(ccp(490, 210));
// 			Background->addChild(scoretype1);
// 		}
// 	}
// 	else
// 	{
// 		if(pClientUserItem1!=NULL)
// 		{
// 
// 			//玩家1名称
// 			Label *pLableUserScore1 = Label::createWithSystemFont(pClientUserItem1->GetNickName(), "Arial", 25);
// 			pLableUserScore1->setPosition(ccp(310,210+conunt*30));
// 			Background->addChild(pLableUserScore1);
// 
// 			int mark = lScore1>0?1:-1;
// 			//下注分数
// 			lScore1=llabs(lScore1);
// 
// 			char strDescribe[256];
// 			sprintf(strDescribe, "%lld",lScore1*mark); 
// 			Label *scoreNum1 = Label::createWithSystemFont(strDescribe, "Arial", 25);
// 			scoreNum1->setPosition(ccp(410,210+conunt*30));
// 			Background->addChild(scoreNum1);
// 
// 
// 			Sprite* scoretype1 = NULL;
// 			if (mark > 0)
// 			{
// 				scoretype1 =  Sprite::create("game_win-hd.png");
// 			}
// 			else
// 			{
// 				scoretype1 =  Sprite::create("game_lose-hd.png");
// 			}
// 			scoretype1->setPosition(ccp(490, 210+conunt*30));
// 			Background->addChild(scoretype1);
// 		}
// 
// 	}


}
void GameScene_Baccarat::ccButtonAdd0(Ref* obj,Control::EventType e)
{
	//gClientKernelSink_Baccarat.OnMeAddGold(8);
}
void GameScene_Baccarat::ccButtonAdd1(Ref* obj,Control::EventType e)
{
	//gClientKernelSink_Baccarat.OnMeAddGold(4);
}
void GameScene_Baccarat::ccButtonAdd2(Ref* obj,Control::EventType e)
{
	//gClientKernelSink_Baccarat.OnMeAddGold(2);
}
void GameScene_Baccarat::ccButtonAdd3(Ref* obj,Control::EventType e)
{
	//gClientKernelSink_Baccarat.OnMeAddGold(1);
}
void GameScene_Baccarat::ccButtonAdd4(Node* sender)
{
	//gClientKernelSink_Baccarat.OnMeAddGold(4);
}
void GameScene_Baccarat::ccButtonAdd5(Node* sender)
{
	//gClientKernelSink_Baccarat.OnMeGive(0);
}
void GameScene_Baccarat::sendCardStart(byte	m_cbTableCardArray[GAME_PLAYER_Baccarat][MAX_COUNT], int XianNum, int ZhuangNum, int state)
{
	for (int i = 0; i < GAME_PLAYER_Baccarat; i++)
	{
		memset(m_CardArray[i], 0, sizeof(byte)*MAX_COUNT);
		memcpy(m_CardArray[i], m_cbTableCardArray[i], sizeof(byte)*MAX_COUNT);
	}
	float step = 0;
	cardCount = 1;
	m_state = state;
	cardSum = XianNum + ZhuangNum;
	for (byte j = 0; j < MAX_COUNT; j++)
	{
		cbPlayerPoint[j] = NULL;
		cbBankerPoint[j] = NULL;
		for (byte i = INDEX_PLAYER; i < GAME_PLAYER_Baccarat; i++)
		{
			//判定是否需要补牌
			if ((i == INDEX_PLAYER && j == XianNum) || (i == INDEX_BANKER && j == ZhuangNum))
			{
				continue;
			}

			//庄闲扑克点数计算
			if (i == INDEX_PLAYER)
			{
				cbPlayerPoint[j] = m_pGameLogic->GetCardListPip(m_cbTableCardArray[INDEX_PLAYER], j + 1);
			} 
			else
			{
				cbBankerPoint[j] = m_pGameLogic->GetCardListPip(m_cbTableCardArray[INDEX_BANKER], j + 1);
			}

			byte value = ClientKernelSink_Baccarat::getInstance().m_cbTableCardArray[i][j];

			CSpriteCard * xxx = m_Card.getCardWithBack();
			/*xxx->setScale(1.2);*/
			xxx->setMark(value);
			Vec2 now_p = ccp(P_WIDTH_2, P_HEIGHT + 200);
			xxx->setPosition(now_p);
			xxx->setTag(10000 + i * 5 + j);
			mFrameLayer->addChild(xxx, j);
			if (m_state == 1)
			{
				if (j < MAX_COUNT - 1)
				{
					xxx->runAction(CCSequence::create(DelayTime::create(step += 0.6), MoveTo::create(0.2f, Vec2(P_WIDTH_2, 320)), MoveTo::create(0.1f, Vec2(ClientKernelSink_Baccarat::getInstance().m_CardPoint[i]) + Vec2(40 * j, 0)), CCCallFuncN::create(this, callfuncN_selector(GameScene_Baccarat::ShowMeCard)), NULL));
				} 
				else
				{
					xxx->runAction(CCSequence::create(DelayTime::create(step += 1), MoveTo::create(0.2f, Vec2(P_WIDTH_2, 320)), MoveTo::create(0.1f, Vec2(ClientKernelSink_Baccarat::getInstance().m_CardPoint[i]) + Vec2(40 * j, 0)), CCCallFuncN::create(this, callfuncN_selector(GameScene_Baccarat::ShowMeCard)), NULL));
				}
			} 
			else
			{
				xxx->runAction(CCSequence::create(MoveTo::create(0, Vec2(ClientKernelSink_Baccarat::getInstance().m_CardPoint[i]) + Vec2(40 * j, 0)), CCCallFuncN::create(this, callfuncN_selector(GameScene_Baccarat::ShowMeCard)), NULL));
			}
			
			mCardList.push_back(xxx);
		}
	}

	//设置标识
	ClientKernelSink_Baccarat::getInstance().m_bNeedSetGameRecord = true;
}

void GameScene_Baccarat::ShowMeCard(CCNode* sender)
{
	SoundManager::shared()->playSound("BACCARAT_OUT_CARD");

	CSpriteCard* xxx2 = dynamic_cast<CSpriteCard*>(sender);
	byte CardMark=xxx2->getMark();
	//xxx2->setScale(1.2);
	xxx2->setVisible(false);
	if(CardMark!=0)
	{
		CSpriteCard* xxx1 = m_Card.getCardCopyWithData(CardMark);
		//xxx1->setScale(1.2);
		xxx1->setPosition(xxx2->getPosition());
		mFrameLayer->addChild(xxx1, xxx2->getZOrder());
		xxx1->setTag(xxx2->getTag());
		mCardList.push_back(xxx1);
	}

	//更新界面点数
	if (cardCount == 1 )
	{
		playerPoint = cbPlayerPoint[0];
		bankerPoint = -1;
	}
	else if (cardCount == 2)
	{
		bankerPoint = cbBankerPoint[0];
	}
	else if (cardCount == 3)
	{
		playerPoint = cbPlayerPoint[1];
	}
	else if (cardCount == 4)
	{
		bankerPoint = cbBankerPoint[1];
	}
	else if (cardCount == 5)
	{
		if (cbPlayerPoint[2] == NULL)
		{
			bankerPoint = cbBankerPoint[2];
		}
		else
		{
			playerPoint = cbPlayerPoint[2];
		}
	}
	else
	{
		playerPoint = cbPlayerPoint[2];
		bankerPoint = cbBankerPoint[2];
	}
	mFrameLayer->ChangePointSprite(playerPoint, bankerPoint);
	if (cardCount == cardSum)
	{
		//判断结果
		if (playerPoint > 7 || bankerPoint > 7)
			cbKingWinner = 1;
		else
			cbKingWinner = 0;

		bPlayerTwoPair = false;
		bBankerTwoPair = false;

		if ((m_CardArray[0][0] - m_CardArray[0][1]) % 16 == 0)
		{
			bPlayerTwoPair = true;
		}
		if ((m_CardArray[1][0] - m_CardArray[1][1]) % 16 == 0)
		{
			bBankerTwoPair = true;
		}

		SoundManager::shared()->playSound("BACCARAT_GAME_PING");
		//区域胜利结果
		mFrameLayer->ShowWinerResult(gameWinner);
		//展示胜利区域
		mFrameLayer->flickerWiner(gameWinner);
		if (playerPoint > 7)
		{
			//闲天王
			mFrameLayer->flickerWiner(3);
		}
		if (bankerPoint > 7)
		{
			//庄天王
			mFrameLayer->flickerWiner(4);
		}
		if (bPlayerTwoPair)
		{
			//闲对子
			mFrameLayer->flickerWiner(6);
		}
		if (bBankerTwoPair)
		{
			//庄对子
			mFrameLayer->flickerWiner(7);
		}
		//添加游戏记录
		if (m_state)
			mFrameLayer->addTrendHistory(cbKingWinner, bPlayerTwoPair, bBankerTwoPair,playerPoint, bankerPoint);

	}
	cardCount++;
}

void GameScene_Baccarat::showTanpaiButton()
{
	mFrameLayer->showMenuItem(FrameLayer_Baccarat::ITEM_TAG_SHOW_HARD, true);
	mFrameLayer->showMenuItem(FrameLayer_Baccarat::ITEM_TAG_ABORT, false);
	mFrameLayer->showMenuItem(FrameLayer_Baccarat::ITEM_TAG_START, false);
	mFrameLayer->showMenuItem(FrameLayer_Baccarat::ITEM_TAG_REQUIRE_BANKSER, false);
// 	ControlButton* Call = ControlButton::create();
// 	Call->setTitleForState("抛牌", Control::State::NORMAL);
// 	Call->setPosition(ccp(P_WIDTH_2-200, 200));
// 	Call->setTag(1313);
// 	Call->addTargetWithActionForControlEvents(this, cccontrol_selector(GameScene_Baccarat::ccButtonUser), Control::EventType::TOUCH_UP_INSIDE);
// 	addChild(Call);
// 
// 	ControlButton* Call1 = ControlButton::create();
// 	Call1->setTitleForState("提示", Control::State::NORMAL);
// 	Call1->setPosition(ccp(P_WIDTH_2-80, 200));
// 	Call1->setTag(1314);
// 	Call1->addTargetWithActionForControlEvents(this, cccontrol_selector(GameScene_Baccarat::ccButtonUserNo), Control::EventType::TOUCH_UP_INSIDE);
// 	addChild(Call1);
}
void GameScene_Baccarat::ccButtonUser(Ref* obj,Control::EventType e)
{
	ClientKernelSink_Baccarat::getInstance().OnMeGive(0);
	removeChildByTag(1313);
	removeChildByTag(1314);
}
void GameScene_Baccarat::ccButtonUserNo(Ref* obj,Control::EventType e)
{
	ClientKernelSink_Baccarat::getInstance().OnMeGive(1);
}

void GameScene_Baccarat::showCallBankerButton()
{

// 	ControlButton* Call = ControlButton::create();
// 	Call->setTitleForState("叫庄", Control::State::NORMAL);
// 	Call->setTag(201);
// 	Call->setPosition(ccp(P_WIDTH_2+100, 250));
// 	Call->addTargetWithActionForControlEvents(this, cccontrol_selector(GameScene_Baccarat::ccButtonUserCall), Control::EventType::TOUCH_UP_INSIDE);
// 	addChild(Call);
// 
// 
// 	ControlButton* Call1 = ControlButton::create();
// 	Call1->setTitleForState("不叫", Control::State::NORMAL);
// 	Call1->setPosition(ccp(P_WIDTH_2, 250));
// 	Call1->setTag(202);
// 	Call1->addTargetWithActionForControlEvents(this, cccontrol_selector(GameScene_Baccarat::ccButtonUserNoCall), Control::EventType::TOUCH_UP_INSIDE);
// 	addChild(Call1);

	mFrameLayer->showMenuItem(FrameLayer_Baccarat::ITEM_TAG_START, false);
	mFrameLayer->showMenuItem(FrameLayer_Baccarat::ITEM_TAG_REQUIRE_BANKSER, true);
	mFrameLayer->showMenuItem(FrameLayer_Baccarat::ITEM_TAG_ABORT, true);

}
void GameScene_Baccarat::ccButtonUserCall(Ref* obj,Control::EventType e)
{
	//gClientKernelSink_Baccarat.ButtonWithCallBanker();
	removeChildByTag(201);
	removeChildByTag(202);
}
void GameScene_Baccarat::ccButtonUserNoCall(Ref* obj,Control::EventType e)
{
	ClientKernelSink_Baccarat::getInstance().ButtonWithGrabBanker();
}

void GameScene_Baccarat::onEnter()
{
	Scene::onEnter();
	if(KeybackLayer::getInstance())
	{
		KeybackLayer::getInstance()->addKeybackListener(this);
	}
}
void GameScene_Baccarat::onExit()
{
	if(KeybackLayer::getInstance())
	{
		KeybackLayer::getInstance()->removeKeybackListener(this);
	}
	Scene::onExit();
}

void GameScene_Baccarat::onKeybackClicked()
{	
	popup(SSTRING("system_tips_title"),SSTRING("back_to_chair_select"),DLG_MB_OK|DLG_MB_CANCEL, 0, this, callfuncN_selector(GameScene_Baccarat::onBackToRoom));
}


void GameScene_Baccarat::onBackToRoom(Node* pNode)
{
	switch (pNode->getTag())
	{
	case DLG_MB_OK:
		{
			if (IClientKernel::get())
				IClientKernel::get()->Intermit(GameExitCode_Normal);
			return ;
		}
		break;
	}
}

void GameScene_Baccarat::onBtnClick(Ref* obj,Control::EventType e)
{
	Control* ctr = (Control*)obj;
	switch (ctr->getTag())
	{
	case TAG_BT_CLOSE:
		{
			if (IClientKernel::get())
				IClientKernel::get()->Intermit(GameExitCode_Normal);
			return ;
		}
		break;
	default:
		break;
	}
}

void GameScene_Baccarat::onEnterTransitionDidFinish()
{
	Scene::onEnterTransitionDidFinish();
	auto ik = IClientKernel::get();
	if (ik)
	{
		ik->SendGameOption();
		startUpdate();
	}
	
	touch_layer_=0;

	EventListenerTouchOneByOne* mListener = EventListenerTouchOneByOne::create();
	mListener->setSwallowTouches(true);
	mListener->onTouchBegan = CC_CALLBACK_2(GameScene_Baccarat::onTouchBegan, this);
	mListener->onTouchMoved = CC_CALLBACK_2(GameScene_Baccarat::onTouchMoved, this);
	mListener->onTouchEnded = CC_CALLBACK_2(GameScene_Baccarat::onTouchEnded, this);
	_eventDispatcher->addEventListenerWithSceneGraphPriority(mListener, this);

}

void GameScene_Baccarat::onExitTransitionDidStart()
{	
	stopUpdate();
	Scene::onExitTransitionDidStart();
}

void GameScene_Baccarat::startUpdate()
{
	stopUpdate();
	schedule(schedule_selector(GameScene_Baccarat::frameUpdate));
}

void GameScene_Baccarat::stopUpdate()
{
	unschedule(schedule_selector(GameScene_Baccarat::frameUpdate));
}

void GameScene_Baccarat::frameUpdate(float dt)
{
	ClientKernelSink_Baccarat::getInstance().OnUpdate(dt);

}

void GameScene_Baccarat::closeCallback(cocos2d::Node* obj)
{
// 	if (IClientKernel::get())
// 	{
// 		stopUpdate();
// 
// 		std::string sInfo = "";
// 		if (obj != 0)
// 		{
// 			MTData* data = (MTData*)obj;
// 			sInfo = data->mStr1;
// 		}
// 
// 		IClientKernel::get()->Intermit(GameExitCode_Normal);
// 	}
	switch (obj->getTag())
	{
	case DLG_MB_OK:
	{
					  if (IClientKernel::get())
						  IClientKernel::get()->Intermit(GameExitCode_Normal);

					  //			if (IClientKernel::get())
					  //				IClientKernel::get()->IntermitConnect(ServerExitCode_Normal);
	}
		break;
	}
}

/////////////////////////////////////////////////////////////////////////////////

void GameScene_Baccarat::func_msg_show()
{
	msg_start_next(false);
}

void GameScene_Baccarat::func_msg_start_next()
{
	msg_start_next(true);
}
// 添加消息
void GameScene_Baccarat::add_msg(const std::string& msg)
{
	mMessageList.push_back(msg);

	if (mMessageList.size() == 1)
	{
		spMessage_bg->setVisible(true);
		spMessage_bg->setOpacity(0);
		spMessage_bg->stopAllActions();
		spMessage_bg->runAction(Sequence::create(
			FadeIn::create(0.3f),
			CallFunc::create(CC_CALLBACK_0(GameScene_Baccarat::func_msg_show,this)),
			//CCCallFuncND::create(this, callfuncND_selector(GameScene_Baccarat::func_msg_show), 0),
			0));
	}
}


void GameScene_Baccarat::msg_start_next(bool bRemoveFront)
{
	if (bRemoveFront && !mMessageList.empty())
		mMessageList.pop_front();

	if (mMessageList.empty())
	{
		spMessage_bg->stopAllActions();
		spMessage_bg->runAction(Sequence::create(
			FadeOut::create(0.3f),
			Hide::create(),
			0));
		return;
	}

	int index = mMsgContentIndex;
	mMsgContentIndex = (mMsgContentIndex+1)%2;
	Label* label = mMsgContents[index];

	label->setString(mMessageList.front().c_str());


	Size cs = spMessage_bg->getContentSize();
	Size csMsg = label->getContentSize();
	if (cs.width >= csMsg.width)
	{
		label->setPosition(Point(cs.width/2, cs.height/2+20));

		label->setVisible(true);
		//label->setOpacity(0);
		label->runAction(Sequence::create(
			Spawn::create(FadeIn::create(0.2f), MoveTo::create(0.2f, Point(cs.width/2, cs.height/2)), 0),
			DelayTime::create(3),
			CallFunc::create(CC_CALLBACK_0(GameScene_Baccarat::func_msg_start_next,this)),
			//CCCallFuncND::create(this, callfuncND_selector(GameScene_Baccarat::func_msg_start_next), 0),
			Spawn::create(CCFadeOut::create(0.2f), MoveTo::create(0.2f, Point(cs.width/2, cs.height/2-20)), 0),
			Hide::create(), 
			0));
	}
	else
	{
		float lenMoveSpeed = 0.01f;
		float len = (csMsg.width - cs.width) / 2;
		label->setPosition(Point(cs.width/2 + len, cs.height/2+20));

		label->setVisible(true);
		label->runAction(Sequence::create(
			Spawn::create(FadeIn::create(0.2f), MoveTo::create(0.2f, Point(cs.width/2 + len, cs.height/2)), 0),
			DelayTime::create(2),
			MoveTo::create(lenMoveSpeed * len * 2, Point(cs.width/2 - len, cs.height/2)),
			DelayTime::create(2),
			CallFunc::create(CC_CALLBACK_0(GameScene_Baccarat::func_msg_start_next,this)),
			//CCCallFuncND::create(this, callfuncND_selector(GameScene_Baccarat::func_msg_start_next), 0),
			Spawn::create(CCFadeOut::create(0.2f), MoveTo::create(0.2f, Point(cs.width/2 - len, cs.height/2-20)), 0),
			Hide::create(), 
			0));
	}
}
//////////////////////////////////////////////////////////////////////////
bool GameScene_Baccarat::onTouchBegan(Touch *pTouch, Event *pEvent)
{
	IClientKernel* kernel = IClientKernel::get();

	if (kernel==0)
		return false;

// 	Point pt = locationFromTouch(pTouch);
// 
// 	xPoint mouse_pos;
// 	mouse_pos.x_ = pt.x;
// 	mouse_pos.y_ = kRevolutionHeight - pt.y;
	/*
	int me_chair_id = kernel->GetMeChairID();

	SPoint cannon_pos = cannon_layer_->GetCannonPos(me_chair_id);

	float angle = CMathAide::CalcAngle(mouse_pos.x_, mouse_pos.y_, cannon_pos.x_, cannon_pos.y_);

	angle = angle*M_PI/180;

	cannon_layer_->SetCurrentAngle(me_chair_id,angle);

	cannon_layer_->update(0);

	is_touch_fire_ = true;

	gClientKernelSink.Fire(is_touch_fire_);
	*/
	return true;

	//cannon_layer_->Fire(me_chair_id);

}

void GameScene_Baccarat::onTouchMoved(Touch *pTouch, Event *pEvent)
{
	IClientKernel* kernel = IClientKernel::get();

	if (kernel==0)
		return;

// 	int me_chair_id = kernel->GetMeChairID();
// 
// 	Point pt = locationFromTouch(pTouch);
// 
// 	SPoint mouse_pos;
// 	mouse_pos.x_ = pt.x;
// 	mouse_pos.y_ = kResolutionHeight - pt.y;
// 
// 	SPoint cannon_pos = cannon_layer_->GetCannonPos(me_chair_id);
// 
// 	float angle = CMathAide::CalcAngle(mouse_pos.x_, mouse_pos.y_, cannon_pos.x_, cannon_pos.y_);
// 
// 	angle = angle*M_PI/180;
// 
// 	cannon_layer_->SetCurrentAngle(me_chair_id,angle);
// 
// 	cannon_layer_->update(0);

}

void GameScene_Baccarat::onTouchEnded(Touch *pTouch, Event *pEvent)
{
	is_touch_fire_ = false;

}


//定时器
void GameScene_Baccarat::func_send_time_check(float dt)
{

}

Point GameScene_Baccarat::locationFromTouch(Touch* touch)
{
	return Director::getInstance()->convertToGL(touch->getLocationInView());
}


void GameScene_Baccarat::OnShakePreen(float m_ptOffest_x,float m_ptOffest_y)
{
// 	scene_layer_->setPosition(Point(m_ptOffest_x,m_ptOffest_y));
// 
// 	fish_layer_->setPosition(Point(m_ptOffest_x,m_ptOffest_y));
// 
// 	cannon_layer_->setPosition(Point(m_ptOffest_x,m_ptOffest_y));
// 
// 	bullet_layer_ ->setPosition(Point(m_ptOffest_x,m_ptOffest_y));

}

void GameScene_Baccarat::setMeUserClock(int time)
{
	if (mFrameLayer)
	{
		mFrameLayer->showTimeOut(time);
	}
}

void GameScene_Baccarat::net_user_state_update(int chair_id, byte user_state)
{
	auto ik = IClientKernel::get();
	PlayerNode_Baccarat * node = nullptr;
	if (ik->GetMeChairID() == chair_id)
	{
		node = mPlayerLayer->getPlayerNodeById(0);

	}
	else
	{
		node = mPlayerLayer->getPlayerNodeById(1);
	}

	if (user_state == US_NULL)
	{
	}
	else if (user_state == US_READY)
	{
		///< 清理牌
		clearCardData(chair_id);
	}
}

void GameScene_Baccarat::net_user_level(int chair_id)
{
	if (chair_id == ClientKernelSink_Baccarat::getInstance().m_wCurrentBanker)
	{
		PlayerNode_Baccarat * node = mPlayerLayer->getPlayerNodeById(1);

		node->clearUser(true);
	}
	
}

void GameScene_Baccarat::clearPlayerPrepare()
{

}

void GameScene_Baccarat::showBankerIconAtPlayerNode(int chairId)
{
	SoundManager::shared()->playSound("GAME_START");
	PlayerNode_Baccarat * node = nullptr;
	if (chairId == IClientKernel::get()->GetMeChairID())
	{
		node = mPlayerLayer->getPlayerNodeById(0);
	}
	else
	{
		node = mPlayerLayer->getPlayerNodeById(1);
	}


}

void GameScene_Baccarat::clearOtherUser(int chair_id)
{
	auto node = mPlayerLayer->getPlayerNodeById(1);
	node->clearUser(false);
}

void GameScene_Baccarat::clearCardData(int chair_id)
{
	IClientKernel * kernel = IClientKernel::get();

	int index = kernel->GetMeChairID() == chair_id ? 0 : 1;
	for (int j = 0; j < 5; j++)
	{
		this->removeChildByTag(8520 + index * 5 + j);
	}

	removeChildByTag(600 + index);
	
}

void GameScene_Baccarat::closeGameNotify(cocos2d::Ref * ref)
{
	if (IClientKernel::get())
		IClientKernel::get()->Intermit(GameExitCode_Normal);
}

void GameScene_Baccarat::showSettlementFrame(int chair_id, bool isShow)
{
	PlayerNode_Baccarat * node = nullptr;
	if (chair_id == IClientKernel::get()->GetMeChairID())
	{
		node = mPlayerLayer->getPlayerNodeById(0);
	}
	else
	{
		node = mPlayerLayer->getPlayerNodeById(1);
	}

}

void GameScene_Baccarat::initGameBaseData()
{
	DF::shared()->init(KIND_ID, GAME_PLAYER, VERSION_CLIENT, STATIC_DATA_STRING("appname"), platformGetPlatform());
}

void GameScene_Baccarat::showStorage(long long money)
{

}

void GameScene_Baccarat::clearUserJetton()
{
	mFrameLayer->clearJetton();
}

void GameScene_Baccarat::SetMePlaceJetton(short cbViewIndex, long long lJettonCount)
{
	IClientKernel * kernel = IClientKernel::get();
	mFrameLayer->onAreaAddJetton(kernel->GetMeChairID(), cbViewIndex, lJettonCount);
}

void GameScene_Baccarat::clearCard()
{
	
 	for (int i = 0; !mCardList.empty() && i < mCardList.size(); i++)
	{
		CSpriteCard * now_sprite = mCardList[i];
		now_sprite->removeFromParent();
	}

	mCardList.clear();
}

void GameScene_Baccarat::clearCardByArea(byte onArea)
{

}

void GameScene_Baccarat::SetBankerInfo(dword dwBankerUserID, int64 lBankerScore)
{
	//庄家椅子号
	word wBankerUser = INVALID_CHAIR;
	IClientKernel * kernel = IClientKernel::get();
	//查找椅子号
	if (0 != dwBankerUserID)
	{
		for (word wChairID = 0; wChairID < MAX_CHAIR; ++wChairID)
		{
			IClientUserItem *pUserItem = kernel->GetTableUserItem(wChairID);
			if (pUserItem == NULL) continue;
			tagUserInfo  *pUserData = pUserItem->GetUserInfo();
			if (NULL != pUserData && dwBankerUserID == pUserData->dwUserID)
			{
				wBankerUser = wChairID;
				break;
			}
		}
	}

	//切换判断
	if (m_wBankerUser != wBankerUser)
	{
		m_wBankerUser = wBankerUser;
		m_wBankerTime = 0L;
		m_lBankerWinScore = 0L;
		m_lTmpBankerWinScore = 0L;
	}
	m_lBankerScore = lBankerScore;

	///< 设置庄家了.更新UI
	PlayerNode_Baccarat * node = mPlayerLayer->getPlayerNodeById(1);
	Text * nickName = node->getNickName();
	TextAtlas * lab_money = node->getMoney();
	
	if (m_wBankerUser == INVALID_CHAIR)
	{
		///< 系统坐庄
		nickName->setString(SSTRING("ox100_game_tips_5"));
		lab_money->setString("1000000000");
	}
	else
	{
		IClientUserItem *pUserItem = kernel->GetTableUserItem(wBankerUser);
		if (pUserItem != NULL)
		{
			tagUserInfo  *pUserData = pUserItem->GetUserInfo();
			nickName->setString(pUserData->szNickName);
			std::string user_money = StringUtils::format("%lld", m_lBankerScore);
			lab_money->setString(user_money);
		}
		else
		{
			nickName->setString(SSTRING("ox100_game_tips_5"));
			lab_money->setString("1000000000");
		}
	}
}

void GameScene_Baccarat::setWindowsIsBetState()
{
	IClientKernel * kernel = IClientKernel::get();

	word nowBanker = ClientKernelSink_Baccarat::getInstance().m_wCurrentBanker;
	if (kernel->GetMeChairID() != nowBanker)
	{
		mFrameLayer->setBtnJettionTouch(true);
		this->checkUserScore();
		mFrameLayer->checkUpdateCenterPanelStatus();
	}
	else
	{
		mFrameLayer->setBtnJettionTouch(false);
	}

	
	///< 检测更新界面按钮
	mFrameLayer->checkUpdateBankerPanel();
	
}

void GameScene_Baccarat::onAreaAddJetton(word wChairID, byte cbJettonArea, int64 lJettonScore)
{
	mFrameLayer->onAreaAddJetton(wChairID, cbJettonArea, lJettonScore);
	
	if (wChairID != ClientKernelSink_Baccarat::getInstance().m_wCurrentBanker)
	{
		mFrameLayer->checkUpdateCenterPanelStatus();
		///< 检测按钮状态是否可用.
		IClientKernel * kernel = IClientKernel::get();
		if (kernel->GetMeChairID() == wChairID)
		{
			checkUserScore();
		}
	}
}

void GameScene_Baccarat::addUserToBankerPanel(tagApplyUser_Baccarat tAuser)
{
	mFrameLayer->addUserToBnakerList(tAuser);
}

void GameScene_Baccarat::deleteUserToBankerPanel(tagApplyUser_Baccarat tAuser)
{
	mFrameLayer->deleteUserToBankerList(tAuser);
}


void GameScene_Baccarat::insertUserToBankerPanel(tagApplyUser_Baccarat tAuser)
{
/*	mFrameLayer->addUserToBnakerList(tAuser);*/
}


void GameScene_Baccarat::SetBankerScore(word wBankerTime, int64 lWinScore)
{
	m_wBankerTime = wBankerTime;
	m_lTmpBankerWinScore = lWinScore;
}

void GameScene_Baccarat::SetCurGameScore(int64 lPlayScore[8], int64 lPlayAllScore)
{
	mFrameLayer->updateSettleAccountsPanel(lPlayScore, lPlayAllScore);
}

void GameScene_Baccarat::SetGameHistory(byte cbKingWinner, bool bPlayerTwoPair, bool bBankerTwoPair, byte cbPlayerCount, byte cbBankerCount)
{
	mFrameLayer->addTrendHistory(cbKingWinner, bPlayerTwoPair, bBankerTwoPair, cbPlayerCount, cbBankerCount);
}

void GameScene_Baccarat::PlaceUserJetton(byte cbViewIndex, int64 lScoreCount)
{
// 	效验参数
// 		ASSERT(cbViewIndex <= ID_HUANG_MEN);
// 		if (cbViewIndex > ID_HUANG_MEN) return;
// 	
// 		//变量定义
// 		bool bPlaceJetton = false;
// 		int64 lScoreIndex[8] = { 100L, 500L, 1000L, 10000L, 100000L, 500000L, 1000000L, 5000000L };
// 	
// 		switch (cbViewIndex)
// 		{
// 		case ID_TIAN_MEN:
// 		{
// 							 gCKS_Bacc.m_lAllJettonScore[ID_TIAN_MEN] += lScoreCount;
// 							 break;
// 		}
// 		case ID_DI_MEN:
// 		{
// 						  gCKS_Bacc.m_lAllJettonScore[ID_DI_MEN] += lScoreCount;
// 						  break;
// 		}
// 		case ID_XUAN_MEN:
// 		{
// 							gCKS_Bacc.m_lAllJettonScore[ID_XUAN_MEN] += lScoreCount;
// 							break;
// 		}
// 		case ID_HUANG_MEN:
// 		{
// 							 gCKS_Bacc.m_lAllJettonScore[ID_HUANG_MEN] += lScoreCount;
// 							 break;
// 		}
// 		default:
// 			break;
// 		}

	///< 加入筹码到指定区域内.


}

void GameScene_Baccarat::setWindowsIsWaitState()
{
	///< 下注按钮为空
	mFrameLayer->setBtnJettionTouch(false);
	///< 清理筹码
	mFrameLayer->clearJetton();
	///< 清理胜利区域
	mFrameLayer->clearWineer();
	///< 清理卡牌
	clearCard();
	///< 检测更新界面按钮
	mFrameLayer->checkUpdateBankerPanel();
}

void GameScene_Baccarat::setWindowsIsGameEndState()
{
	mFrameLayer->setBtnJettionTouch(false);
	///< 清理屏幕上的Tips
	mFrameLayer->clearTipsStatus();
	///< 清理按钮上面的泡泡
	mFrameLayer->clearButtonOnPaoPao();

	///< 检测更新界面按钮
	mFrameLayer->checkUpdateBankerPanel();
}

void GameScene_Baccarat::SetCardInfo(byte cbTableCardArray[5][5])
{
	for (byte j = 0; j < 5; j++)
	{
		for (byte i = 0; i < GAME_PLAYER_Baccarat; i++)
		{
		
			byte value = cbTableCardArray[i][j];
				CSpriteCard* xxx = m_Card.getCardWithData(value);
				Vec2 now_p = ccp(ClientKernelSink_Baccarat::getInstance().m_CardPoint[i].x + j * 23, ClientKernelSink_Baccarat::getInstance().m_CardPoint[i].y);
				xxx->setPosition(now_p);

				xxx->setTag(10000 + i * 5 + j);
				addChild(xxx, j);
				mCardList.push_back(xxx);
			
		}
	}
}

void GameScene_Baccarat::onAreaTotalJetton(byte cbJettonArea, int64 lJettonScore)
{
	mFrameLayer->onAreaAddJetton(-1, cbJettonArea, lJettonScore);
}
///< 检测玩家金币等级
void GameScene_Baccarat::checkUserScore()
{
	IClientKernel * kernel = IClientKernel::get();

	int tGrade = 0;
	///< 检测自身
	IClientUserItem * pUserItem = kernel->GetTableUserItem(kernel->GetMeChairID());
	if (pUserItem == NULL)  CCASSERT(pUserItem, "User ERROR");
	tagUserInfo * pUserData = pUserItem->GetUserInfo();
	if (NULL != pUserData)
	{
		int64 pScore = pUserData->lScore - ClientKernelSink_Baccarat::getInstance().getMyJettonTotal();
		tGrade = checkUserMoneyGrade(pScore);
	}

	mFrameLayer->checkUserMoneyEnableJettonBtn(tGrade);
}

int GameScene_Baccarat::checkUserMoneyGrade(int64 pUserMoney)
{
	int grade = 0;
	if (pUserMoney < 1000)
	{
		grade = 0;
	}
	else if (pUserMoney < 10000 && pUserMoney >= 1000)
	{
		grade = 1;
	}
	else if (pUserMoney < 100000)
	{
		grade = 2;
	}
	else if (pUserMoney < 1000000)
	{
		grade = 3;
	}
	else if (pUserMoney < 5000000)
	{
		grade = 4;
	}
	else if (pUserMoney < 10000000)
	{
		grade = 5;
	}
	else if (pUserMoney >= 10000000)
	{
		grade = 6;
	}

	return grade;
}

void GameScene_Baccarat::setTimeState(int timeLableType)
{
	mFrameLayer->setTimeState(timeLableType);
}


void GameScene_Baccarat::ShowCardStage(byte	m_cbTableCardArray[2][3], int XianNum, int ZhuangNum, int mTimeLeave, int state)
{
	//判断赢家
	byte cbPlayerPoint = m_pGameLogic->GetCardListPip(m_cbTableCardArray[INDEX_PLAYER], XianNum);
	byte cbBankerPoint = m_pGameLogic->GetCardListPip(m_cbTableCardArray[INDEX_BANKER], ZhuangNum);
	
	if (cbPlayerPoint == cbBankerPoint)
	{
		//赢家为平
		gameWinner = AREA_PING;
		if (XianNum == ZhuangNum)
		{
			//同点平判断
			int wCardIndex = 0;
			for (; wCardIndex < XianNum; wCardIndex++)
			{
				byte cbPlayerValue = m_pGameLogic->GetCardPip(m_cbTableCardArray[INDEX_PLAYER][wCardIndex]);
				byte cbBankerValue = m_pGameLogic->GetCardPip(m_cbTableCardArray[INDEX_BANKER][wCardIndex]);
				if (cbBankerValue != cbPlayerValue) break;
			}
			if (wCardIndex == XianNum) gameWinner = AREA_TONG_DUI;
		}
	}
	else if (cbPlayerPoint > cbBankerPoint)
	{
		gameWinner = AREA_XIAN;
	}
	else
	{
		gameWinner = AREA_ZHUANG;
	}
	if (state == 1)
	{
		//正常进入
		//开始舞台展示动画
		mFrameLayer->ShowCardStage(cbPlayerPoint, cbBankerPoint);
	} 
	else
	{
		//断线重连
		mFrameLayer->breakLineStage(mTimeLeave);
		mFrameLayer->setBtnJettionTouch(false);
	}

	//发牌
	sendCardStart(m_cbTableCardArray, XianNum, ZhuangNum, state);
}

void GameScene_Baccarat::func_Reconnect_on_loss(cocos2d::Ref * obj)
{
	IClientKernel* kernel = IClientKernel::get();
	if (kernel == nullptr)
	{
		G_NOTIFY_D("MODE_SELECTED", MTData::create(1));
		return;
	}

	isBaccaratReconnectOnLoss = true;
	///< 断线重连
	this->schedule(SEL_SCHEDULE(&GameScene_Baccarat::reconnect_on_loss), 1.5f);

}

void GameScene_Baccarat::reconnect_on_loss(float dt)
{
	static float total_time = 0;
	total_time += dt;

	bool isHaveNet = false;

#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID || CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
	isHaveNet = SimpleTools::obtainNetWorkState();
#endif

#if (CC_TARGET_PLATFORM == CC_PLATFORM_WIN32)
	if (total_time > 10)
		isHaveNet = true;
#endif
	if (total_time > 15)
	{
		this->unschedule(SEL_SCHEDULE(&GameScene_Baccarat::reconnect_on_loss));
		if (!isHaveNet)
		{
			log("mei you wang luo l!");
			///< 没有网络了..
			NewDialog::create(SSTRING("System_Tips_26"), NewDialog::AFFIRM, [=]()
			{
				if (IClientKernel::get())
					IClientKernel::get()->Intermit(GameExitCode_Normal);
			});
			isBaccaratReconnectOnLoss = false;
			total_time = 0.0f;
			return;
		}
	}

	if (isHaveNet)
	{
		if (total_time <= 15.0f)
			this->unschedule(SEL_SCHEDULE(&GameScene_Baccarat::reconnect_on_loss));
		NewDialog::create(SSTRING("System_Tips_28"), NewDialog::NONEBUTTON, nullptr, nullptr, [=]()
		{
			//auto items = IClientKernel::get()->getGameServerItem();
			//G_NOTIFY_D("CONNECT_SERVER", MTData::create(0, 0, 0, "", "", "", items));
			ServerScene * tServer = dynamic_cast<ServerScene *>(ModeScene::create()->getChildByName("ServerScene"));
			if (tServer)
			{
				tServer->connectServer();
			}
			isBaccaratReconnectOnLoss = false;
		});
		total_time = 0.0f;
		isBaccaratReconnectOnLoss = true;
	}
	else
	{
		NewDialog::create(SSTRING("System_Tips_29"), NewDialog::NONEBUTTON);
	}

}
bool GameScene_Baccarat::is_Reconnect_on_loss()
{
	return isBaccaratReconnectOnLoss;
}
void GameScene_Baccarat::ExitGameTiming()
{
	NewDialog::create(SSTRING("System_Tips_36"), NewDialog::NONEBUTTON);
	this->scheduleOnce(SEL_SCHEDULE(&GameScene_Baccarat::CloseKernelExitGame), 2.5f);
}

void GameScene_Baccarat::CloseKernelExitGame(float dt)
{
	IClientKernel::destory();
}


