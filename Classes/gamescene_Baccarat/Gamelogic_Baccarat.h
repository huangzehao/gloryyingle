#ifndef GAME_LOGIC_BACCARAT_HEAD_FILE
#define GAME_LOGIC_BACCARAT_HEAD_FILE

#pragma once

//////////////////////////////////////////////////////////////////////////

//数值掩码
#define	LOGIC_MASK_COLOR			0xF0								//花色掩码
#define	LOGIC_MASK_VALUE			0x0F								//数值掩码

#include "Platform/PFDefine/df/types.h"

//////////////////////////////////////////////////////////////////////////

//游戏逻辑
class CGameLogic_Baccarat
{
	//变量定义
private:
	static const byte				g_cbCardData_Baccarat[52*8];				//扑克定义

	//函数定义
public:
	//构造函数
	CGameLogic_Baccarat();
	//析构函数
	virtual ~CGameLogic_Baccarat();

	//类型函数
public:
	//获取数值
	byte GetCardValue(byte cbCardData) { return cbCardData&LOGIC_MASK_VALUE; }
	//获取花色
	byte GetCardColor(byte cbCardData) { return cbCardData&LOGIC_MASK_COLOR; }

	//控制函数
public:
	//混乱扑克
	void RandCardList(byte cbCardBuffer[], byte cbBufferCount);

	//逻辑函数
public:
	//获取牌点
	byte GetCardPip(byte cbCardData);
	//获取牌点
	byte GetCardListPip(const byte cbCardData[], byte cbCardCount);
};

//////////////////////////////////////////////////////////////////////////

#endif
