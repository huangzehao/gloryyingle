#ifndef PLAYERLAYER_BACCARAT_
#define PLAYERLAYER_BACCARAT_

#include "cocos2d.h"
#include "PlayerNode_Baccarat.h"


USING_NS_CC;
using namespace cocos2d::ui;
using namespace cocostudio;


class PlayerLayer_Baccarat : public Layer
{
public:
	PlayerLayer_Baccarat();
	~PlayerLayer_Baccarat();

//	CREATE_FUNC(PlayerLayer_Baccarat);
	static PlayerLayer_Baccarat * create(Widget *mRoot);

	bool init(Widget *mRoot);

	PlayerNode_Baccarat * getPlayerNodeById(int chair_id);

	void updateUserScore(cocos2d::Ref * ref);
protected:
	std::vector<PlayerNode_Baccarat *> mPlayerList;


};

#endif

