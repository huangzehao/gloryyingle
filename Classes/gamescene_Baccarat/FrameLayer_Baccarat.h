#ifndef FrameLayer_BACCARAT_H
#define FrameLayer_BACCARAT_H

#include "cocos2d.h"
#include "GameScene_Baccarat.h"
#include "cocos-ext.h"
#include "ui/UITextAtlas.h"
#include "cocostudio/CocoStudio.h"
#include "ui/CocosGUI.h"
#include "ui/UITextAtlas.h"
#include "Platform/PFDefine/df/types.h"
#include "ui/UIButton.h"

using namespace cocos2d::ui;
using namespace cocostudio;
USING_NS_CC;

class tagTrendInfo_Baccarat;
class tagApplyUser_Baccarat;

class FrameLayer_Baccarat : public cocos2d::Layer
{
public:
	FrameLayer_Baccarat();
	~FrameLayer_Baccarat();

//	CREATE_FUNC(FrameLayer_Baccarat);
	static FrameLayer_Baccarat * create(Widget *mRoot);

	enum MenuItemTag
	{
		ITEM_TAG_START, ///< 开始按钮
		ITEM_TAG_REQUIRE_BANKSER, ///< 请求庄家
		ITEM_TAG_SHOW_HARD, ///< 摊牌
		ITEM_TAG_ABORT, ///< 放弃
	};

	enum ScoreTypeTag
	{
		SCORE_VALUE_1000 = 100,
		SCORE_VALUE_1W,
		SCORE_VALUE_10W,
		SCORE_VALUE_100W,
		SCORE_VALUE_500W,
		SCORE_VALUE_1000W,
	};

	enum CenterTypeTag
	{
		CENTER_TAG_XIAN = 200,
		CENTER_TAG_PING,
		CENTER_TAG_ZHUANG,
		CENTER_TAG_XIANTIANWANG,
		CENTER_TAG_ZHUANGTIANWANG,
		CENTER_TAG_TONGDIANPING,
		CENTER_TAG_XIANDUIZI,
		CENTER_TAG_ZHUANGDUIZI,
	};
	virtual bool init(Widget *mRoot);

	void setCloseDialogInfo(cocos2d::Ref* target, cocos2d::SEL_CallFuncN callfun, const std::string& title, const std::string& content);
	ui::Widget * getBankerPanel();
	ui::Widget * getSettleAccountsPanel();
	ui::Widget * getTrendPanel();
	ui::Widget * getMainPanel();
protected:
	cocos2d::Ref*		mTarget;
	cocos2d::SEL_CallFuncN	mCallback;
	std::string				mTitle;
	std::string				mContent;

	///<空闲时间logo
	ui::ImageView * img_free_time;
	///<下注时间logo
	ui::ImageView * img_bet_time;
	///<开牌时间logo
	ui::ImageView * img_open_time;
	///< 上庄界面
	ui::Widget * mBankerPanel;
	///< 结算界面
	ui::Widget * mSettleAccountsPanel;
	///< 走势界面
	ui::Widget * mTrendPanel;
	///< 主面板
	ui::Widget * mPanelMain;
	///< 主面板的底部层
	ui::ImageView * mImg_down_layer;
	///<游戏押注界面
	ui::Widget * Panel_game_bet;
	///<底部层
	ui::ImageView * img_down_layer;
	///<顶部层
	ui::ImageView * img_top_layer;
	///<玩家信息层
	ui::Widget * PlayerNode_player;
	///<庄家信息层
	ui::Widget * PlayerNode_zhuang;
	///<倒计时间层
	ui::ImageView * img_time_bg;
	///<倒计时间的类型
	ui::ImageView * img_time_type;
	///<倒计时间的数字
	cocos2d::ui::TextAtlas * al_time_nums;
	///<游戏比牌界面
	ui::Widget * Panel_game_stage;

// 	///< 黄色圈
// 	Sprite * img_ox100_yellow[4];
// 	///< 红色圈
// 	Sprite * img_ox100_red[4];
	///< 气泡
	ImageView * img_qipao;
	///< 闲家点数图片
	ui::ImageView * xianPoint;
	///< 庄家点数图片
	ui::ImageView * zhuangPoint;
	///< 闲家获胜图片
	ui::ImageView * xianWinImage;
	///< 庄家获胜图片
	ui::ImageView * zhuangWinImage;
	///< 庄闲战平图片
	ui::ImageView * pingWinImage;
	///< 舞台背景
	Sprite * stageSprite;
	///< 闲家下注总数
	cocos2d::ui::TextAtlas * al_xian_total;
	///< 闲家自身下注
	cocos2d::ui::TextAtlas * al_xian_my;
	///< 平家下注总数
	cocos2d::ui::TextAtlas * al_ping_total;
	///< 平家自身下注
	cocos2d::ui::TextAtlas * al_ping_my;
	///< 庄家下注总额
	cocos2d::ui::TextAtlas * al_zhuang_total;
	///< 庄家自身下注
	cocos2d::ui::TextAtlas * al_zhuang_my;
	///< 闲天王下注总额
	cocos2d::ui::TextAtlas * al_xiantianwang_total;
	///< 闲天王自身下注
	cocos2d::ui::TextAtlas * al_xiantianwang_my;
	///< 庄天王下注总数
	cocos2d::ui::TextAtlas * al_zhuangtianwang_total;
	///< 庄天王自身下注
	cocos2d::ui::TextAtlas * al_zhuangtianwang_my;
	///< 同点平下注总数
	cocos2d::ui::TextAtlas * al_tongdianping_total;
	///< 同点平自身下注
	cocos2d::ui::TextAtlas * al_tongdianping_my;
	///< 闲对子下注总额
	cocos2d::ui::TextAtlas * al_xianduizi_total;
	///< 闲对子自身下注
	cocos2d::ui::TextAtlas * al_xianduizi_my;
	///< 庄对子下注总额
	cocos2d::ui::TextAtlas * al_zhuangduizi_total;
	///< 庄对子自身下注
	cocos2d::ui::TextAtlas * al_zhuangduizi_my;
	///< 我要上庄按钮
	cocos2d::ui::Button * btn_Baccarat_banker_askfor;
private:
	void onBtnClick(cocos2d::Ref* obj, cocos2d::extension::Control::EventType e);

	void func_affirm_exit_call_back(cocos2d::Ref *pSender);
	void func_set_call_back(cocos2d::Ref *pSender);
	void func_start_call_back(cocos2d::Ref *pSender);
	void func_require_banker_call_back(cocos2d::Ref *pSender);
	void func_tanpai_call_back(cocos2d::Ref *pSender);
	void func_abort_call_back(cocos2d::Ref *pSender);
	void func_bottom_pour_back(cocos2d::Ref *pSender);

private:
	int total_time;		 ///<总时间
	long long max_score; ///< 最大投注额
	bool isSettle;		 ///<是否打开结算面板
public:
	///< 显示按钮
	void showMenuItem(MenuItemTag item_tag, bool isShow = true);
	///<开启开牌舞台
	void ShowCardStage(int playerPoint,int bankerPoint);
	///<开牌舞台完成
	void ShowCardStageEnd(cocos2d::Node *target, void* data);
	///<结算面板展开完成
	void ShowSettlePanelEnd(cocos2d::Node *target, void* data);
	///<隐藏结算面板
	void HideSettlePanel(cocos2d::Node *target, void* data);
	///<清除结算面板动画
	void ClearSettlePanelEnd(cocos2d::Node *target, void* data);
	///<隐藏舞台
	void HideCardStageEnd(cocos2d::Node *target, void* data);
	///<清除舞台卡片
	void ClearStageCard(cocos2d::Node *target, void* data);
	///<更新点数精灵
	void ChangePointSprite(int XianPoint,int ZhuangPoint);
	///<断线重连舞台
	void breakLineStage(int mTimeLeave);
	///<断线清除舞台卡片
	void breakClearStageCard(cocos2d::Node *target, void* data);
	///<显示赢家结果
	void ShowWinerResult(int gameWinner);
	///< 显示时间结束
	void showTimeOut(int time);
	///< 设置分数
	void setAddScoreMaxValue(long long score);
	///< 设置上庄条件
	void setScoreApplyBanker(int64 lApplyBankerScore);
	///< 玩家分数是否符合申请条件
	void playerScoreIsApplyBanker(int64 PlayerScore);
	///< 胜利区域闪烁
	void flickerWiner(int tag);
	///< 轮换庄家展示
	void huanZhuang(int tag);
	///<清除轮换庄家展示
	void huanZhuangClear(cocos2d::Node *target, void* data);
	/////////////////////////////////////////////////////////////////////////
	//创建主面板
	void createMainPanel(Widget *mRoot);
private:
	void sche_time_out(float dt);
	//加分设置
	void btnAddScoreCallBack(cocos2d::Ref * ref, ui::TouchEventType eType);
	//申请上庄
	void btnAskForBankerCallBack(cocos2d::Ref * ref, ui::TouchEventType eType);
	//查看路单 
	void btnQueryTrendCallBack(cocos2d::Ref * ref, ui::TouchEventType eType);
	//庄平闲点击
	void btnCenterClickCallBack(cocos2d::Ref * ref, ui::TouchEventType eType);
	//关闭游戏按钮
	void btnCloseGameCallBack(cocos2d::Ref * ref, ui::TouchEventType eType);
	///< 获取每个按钮的Tips
	void getBtnTips(cocos2d::Node * tNode, CenterTypeTag tag);

///////////////////////////////////////////////////////////////////////////////////////////
	///< 上庄面板
public:
	///< 创建上庄面板
	void createBankerPanel();
	///< 上庄关闭
	void btnBankerCloseCallBack(cocos2d::Ref * ref, ui::TouchEventType eType);
	///< 申请上庄
	void btnBankerAskForCallBack(cocos2d::Ref * ref, ui::TouchEventType eType);
	///< 抢庄
	void btnBankerGrabCallBack(cocos2d::Ref * ref, ui::TouchEventType eType);

protected:
	ui::ListView * lv_banker_user; ////< 列表容器

	///< 保险箱面板
public:
	///< 创建保险箱面板
	void createSafeBoxPanel();
	///< 刷新
	void btnSafeBoxFlushCallBack(cocos2d::Ref * ref, ui::TouchEventType eType);
	///< 取出
	void btnSafeBoxTakeOutCallBack(cocos2d::Ref * ref, ui::TouchEventType eType);
	///< 关闭
	void btnSafeBoxCloseCallBack(cocos2d::Ref * ref, ui::TouchEventType eType);
protected:
	///保险箱数目
	ui::Text * lab_safebox_money;
	///欢乐豆数目
	ui::TextAtlas * tf_safebox_takeout;
	///输入密码
	ui::TextAtlas * tf_safebox_password;
	///选择全部
	ui::CheckBox * cb_safebox_all;

///////////////////////////////////////////////////////////////////////////////////////////
	//结算面板
public:
	///< 创建结算面板
	void createSettleAccountsPanel();
protected:
	// 闲对子得分
	ui::Text * lab_xianduizi;
	// 庄对子得分
	ui::Text * lab_zhuangduizi;
	// 闲得分
	ui::Text * lab_xian;
	// 平得分
	ui::Text * lab_ping;
	// 庄得分
	ui::Text * lab_zhuang;
	// 闲天王得分
	ui::Text * lab_xiantianwang;
	// 同点平得分
	ui::Text * lab_tongdianping;
	// 庄天王得分
	ui::Text * lab_zhuangtianwang;
	// 总得分
	ui::Text * lab_total_points;
	///< 闲家点数图片
	ui::ImageView * img_xian_dian;
	///< 庄家点数图片
	ui::ImageView * img_zhuang_dian;

	std::string lab_xian_string;
	std::string lab_ping_string;
	std::string lab_zhuang_string;
	std::string lab_xianduizi_string;
	std::string lab_xiantianwang_string;
	std::string lab_zhuangduizi_string;
	std::string lab_zhuangtianwang_string;
	std::string lab_tongdianping_string;
	std::string lab_total_points_string;

/////////////////////////////////////////////////////////////////////////////////////////////
public:
	///< 创建路子面板
	void createTrendPanel();
	virtual bool onTouchBegan(Touch *pTouch, Event *pEvent);
protected:
	///< 路子面板
	ImageView * img_bg;
	///< 珠路面板
	ListView * lv_zhulu;
	///< 大路面板
	ListView * lv_dalu;
	///< 珠路竖列
	ListView * lv_zhu_shu;
	///< 珠路单元
	Layout * panel_zhu_cell;
	///< 大路竖列
	ListView * lv_da_shu;
	///< 大路单元
	Layout * panel_da_cell;

	Text * txt_xianduizi;
	Text * txt_zhuangduizi;
	Text * txt_xiantianwang;
	Text * txt_zhuangtianwang;
	Text * txt_xianshu;
	Text * txt_xianbai;
	Text * txt_pingshu;
	Text * txt_pingbai;
	Text * txt_zhuangshu;
	Text * txt_zhuangbai;

	int num_zong;
	int num_xian;
	int num_ping;
	int num_zhuang;
	int num_xianduizi;
	int num_zhuangduizi;
	int num_xiantianwang;
	int num_zhuangtianwang;
	int num_shu_copy;
	int num_da_copy;
	int type_now;
	int type_old;

	float bai_xian;
	float bai_ping;
	float bai_zhuang;
	int64 lApplyBankerCondition;  //申请上庄条件
///////////////////////////////////////////////////////////////////////////////////////////////
public:
	///< 游戏需要的计算. 用户区域上下分
	void onAreaAddJetton(word wChairID, byte cbJettonArea, int64 lJettonScore);
// 	///< 给自己的区域上分
// 	void onMyAreaAddJetton(byte cbJettonArea, int64 lJettonScore);
	///< 清理按钮上的标示
	void clearTipsStatus();
	///< 清理按钮上面的泡泡
	void clearButtonOnPaoPao();
	///< 清理筹码
	void clearJetton();
	///< 清理胜利区域
	void clearWineer();
	///< 增加路子记录
	void addTrendHistory(byte cbKingWinner, bool bPlayerTwoPair, bool bBankerTwoPair, byte cbPlayerCount, byte cbBankerCount);
	///< 设置筹码按钮是否能点击
	void setBtnJettionTouch(bool isCan);
	///< 更新结算面板
	void updateSettleAccountsPanel(int64 lPlayScore[8], int64 lPlayAllScore);
	///< 增加用户到上庄列表
	void addUserToBnakerList(tagApplyUser_Baccarat & tAuser);
	///< 删除用户从上庄列表
	void deleteUserToBankerList(tagApplyUser_Baccarat & tAuser);
	///< 检查更新用户上庄面板
	void checkUpdateBankerPanel();
	///< 检测用户金币是否可以使下注按钮可用.
	void checkUserMoneyEnableJettonBtn(int maxAllowableValue);
	///< 检测下注是否小于区域最大值
	int64 checkJettonLessThanArea(byte pArea, int64 pJettonScore);
	///< 通过ScoreTypeTag的值来获取分数
	int64 getScoreByScoreTypeTag(ScoreTypeTag tag);
	///< 设置时间logo状态
	//void setTimeState(bool freeTime, bool betTime, bool openTime);
	void setTimeState(int timeLableType);
	///< 检测更新按钮状态
	void checkUpdateCenterPanelStatus();
	///< 检查最大下注额
	int64 GetMaxPlayerScore(byte cbBetArea, word wChairID);
protected:
	///< 筹码的容器
	std::vector<Sprite *> mJettonList;
	///< 走势的数据容器
	std::vector<tagTrendInfo_Baccarat> mTrendsInfoList;
	///< 点走势的数据容器
	std::vector<tagTrendInfo_Baccarat> mDianTrendsInfoList;
	///< 现在的筹码倍数
	int mNowClickJetton;
	///< 提示性Tips的容器
	std::map<int, ui::ImageView *> mTipsList;
	///< 自己下注的分
	int64 mMyTotalScore;
	///< 总下注的分
	int64 mTotalScore;
	//std::vector<Widget *> mCallBankerList;

};

#endif

