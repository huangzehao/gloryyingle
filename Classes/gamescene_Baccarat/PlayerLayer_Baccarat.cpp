#include "PlayerLayer_Baccarat.h"
#include "Tools/tools/MTNotification.h"
#include "Kernel/kernel/game/IClientKernel.h"
#include "ClientKernelSink_Baccarat.h"
#include "CMD_Baccarat.h"
PlayerLayer_Baccarat::PlayerLayer_Baccarat()
{
	G_NOTIFY_REG("EVENT_USER_SCORE", PlayerLayer_Baccarat::updateUserScore);
}


PlayerLayer_Baccarat::~PlayerLayer_Baccarat()
{
	G_NOTIFY_UNREG("EVENT_USER_SCORE");
}

PlayerLayer_Baccarat * PlayerLayer_Baccarat::create(Widget *mRoot)
{
	PlayerLayer_Baccarat * node = new PlayerLayer_Baccarat;
	if (node && node->init(mRoot))
	{
		node->autorelease();
		return node;
	}
	delete node;
	return nullptr;
}

bool PlayerLayer_Baccarat::init(Widget *mRoot)
{
	if (!Layer::init())
	{
		return false;
	}

	for (int i = 0; i < 2; i++)
	{
		PlayerNode_Baccarat * node = PlayerNode_Baccarat::create(i,mRoot);
		this->addChild(node);
		mPlayerList.push_back(node);
	}

	return true;
}

PlayerNode_Baccarat * PlayerLayer_Baccarat::getPlayerNodeById(int chair_id)
{
	if (chair_id < mPlayerList.size())
	{
		return mPlayerList[chair_id];
	}

	return nullptr;
	
}

void PlayerLayer_Baccarat::updateUserScore(cocos2d::Ref * ref)
{
	EventCustom *event_ = (EventCustom*)ref;
	if (event_ == 0){
		return;
	}

	MTData* data = (MTData*)event_->getUserData();
	if (data == 0)
		return;

	IClientUserItem * item = (IClientUserItem *)data->mPData;

	IClientKernel * kernel = IClientKernel::get();
	PlayerNode_Baccarat * node = nullptr;
	if (item)
	{
		int chair_id = item->GetChairID();
		if (chair_id == kernel->GetMeChairID())
		{
			node = getPlayerNodeById(0);
		}
		else if (chair_id == ClientKernelSink_Baccarat::getInstance().m_wCurrentBanker)
		{
			node = getPlayerNodeById(1);
		}

		if (node)
		{
			node->setMoney(item->GetUserScore());
		}
	}

}
