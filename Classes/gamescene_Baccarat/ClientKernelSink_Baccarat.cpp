#include "Tools/tools/MTNotification.h"
#include "Tools/Manager/SoundManager.h"
#include "GameScene_Baccarat.h"
#include "FrameLayer_Baccarat.h"
#include "ClientKernelSink_Baccarat.h"
#include "Tools/Dialog/NewDialog.h"
#include "ApplyUserList_Baccarat.h"
#include "Kernel/kernel/game/IClientKernel.h"
#include "Kernel/kernel/server/IServerItemSink.h"
#include "Tools/tools/StringData.h"
#include "Tools/Dialog/Timer.h"
#include "Kernel/kernel/server/IServerItem.h"

#define  P_WIDTH      kRevolutionWidth
#define  P_WIDTH_2    P_WIDTH/2

#define  P_HEIGHT    kRevolutionHeight
#define  P_HEIGHT_2    P_HEIGHT/2

//时间标识
//时间标识
#define IDI_FREE					99									//空闲时间
#define IDI_PLACE_JETTON			100									//下注时间
#define IDI_DISPATCH_CARD			301									//发牌时间

//游戏时间
#define TIME_START_GAME				90									//开始定时器

#define	SHAKE_FPS					30
#define SHAKE_FRAMES				20
#define KICK_OUT_TIME				60000
#define FIRE_INTERVAL				200
#define HELP_SHOW_TIME				5000

#define MAX_PATHS			260
#define MODE_MEDAL							1					//元宝模式(既平台的奖牌)
USING_NS_CC;
using namespace Baccarat;

ClientKernelSink_Baccarat* ClientKernelSink_Baccarat::s_Instance = NULL;

//ClientKernelSink_Baccarat gClientKernelSink_Baccarat;

//构造函数
ClientKernelSink_Baccarat::ClientKernelSink_Baccarat()
{
	szGlobalMessage = "";
	char path[MAX_PATHS]={0};	

/*	mGameAttribute.wChairCount=GAME_PLAYER_SHOWHANDTWO;*/

	//下注倍数
	m_nMultiples[0] = 1;
	m_nMultiples[1] = 2;
	m_nMultiples[2] = 4;
	m_nMultiples[3] = 0;


	m_GameIsStart=false;

	m_cbLeftCardCount = 0;
	m_SendCard=false;                         //发牌标志
	m_wSendIndex=0;							//发牌索引
	m_nSendChair=0;							//发送座位	
	m_lQiangScore = 0;
	float baseDistance = 135;
	m_bMeApplyBanker = false;
	m_bCallBanker = false;

	m_CardPoint[0] = ccp(560, 320);
	m_CardPoint[1] = ccp(810, 320);

	for (int i = 0; i<GAME_PLAYER_Baccarat; i++)
	{
		m_lUserCardType[i] = 0;
	}

	return;
}

//析构函数
ClientKernelSink_Baccarat::~ClientKernelSink_Baccarat()
{
	m_pGameScene = nullptr;
}

ClientKernelSink_Baccarat* ClientKernelSink_Baccarat::GetInstance()
{
	if(s_Instance == NULL)
		s_Instance = new ClientKernelSink_Baccarat();

	return s_Instance;
}

void ClientKernelSink_Baccarat::Release()
{

}

//控制接口
//启动游戏场景
bool ClientKernelSink_Baccarat::SetupGameClient()
{
	PLAZZ_PRINTF(("flow->ClientKernelSink::SetupGameClient\n"));
	mIsNetworkPrepared = false;

	Director * director = Director::getInstance();
	Scene * pScene = director->getRunningScene();
	if (pScene->getTag() == KIND_ID && pScene->getName() == "GameScene")
	{
		///< 表示是自己!!
		m_pGameScene = dynamic_cast<GameScene_Baccarat *>(pScene);
	}
	else
	{
		if (!m_pGameScene)
			m_pGameScene = GameScene_Baccarat::create();
	}

	const tagUserAttribute &userattr_ = IServerItem::get()->GetUserAttribute();

	PLAZZ_PRINTF(("flow->ClientKernelSink::SetupGameClient1\n"));
	//CCDirector::getInstance()->replaceScene(CCTransitionFade::create(0.5f,mGameScene));

	if (pScene != m_pGameScene)
	{
		director->pushScene(CCTransitionFade::create(0.5f, m_pGameScene));
	}
	else
	{
		if (IClientKernel::get())
		{
			IClientKernel::get()->SendGameOption();
		}
	}

	return false;
}

//重置游戏
void ClientKernelSink_Baccarat::ResetGameClient()
{
	PLAZZ_PRINTF(("flow->ClientKernelSink_Ox100::ResetGameClient\n"));
	IClientKernel * kernel = IClientKernel::get();

	Director::getInstance()->getScheduler()->unschedule(schedule_selector(ClientKernelSink_Baccarat::OnGameTimer), this);

	kernel->KillGameClock(IDI_FREE);

	mIsNetworkPrepared = false;

}

//关闭游戏
void ClientKernelSink_Baccarat::CloseGameClient(int exit_tag /*= 0*/)
{
	mIsNetworkPrepared = false;
	m_pGameScene = 0;
	//删除定时器
	IClientKernel* kernel = IClientKernel::get();
	kernel->KillGameClock(IDI_FREE);

 	Director::getInstance()->getScheduler()->unschedule(
		schedule_selector(ClientKernelSink_Baccarat::OnGameTimer), this);


	if (!GameScene_Baccarat::is_Reconnect_on_loss())
		G_NOTIFY_D("MODE_SELECTED", MTData::create(1));

}


//////////////////////////////////////////////////////////////////////////
//框架事件

//系统滚动消息
bool ClientKernelSink_Baccarat::OnGFTableMessage(const char* szMessage)
{
	return true;
}


//全局消息
bool ClientKernelSink_Baccarat::OnGFGlobalMessage(const char* szMessage)
{
	szGlobalMessage = szMessage;
	m_pGameScene->add_msg(szMessage);
	return true;
}


//获取系统消息
std::string ClientKernelSink_Baccarat::GetGlobalMessage()
{
	return szGlobalMessage;
}


//等待提示
bool ClientKernelSink_Baccarat::OnGFWaitTips(bool bWait)
{
	return true;
}

//比赛信息
bool ClientKernelSink_Baccarat::OnGFMatchInfo(tagMatchInfo* pMatchInfo)
{
	return true;
}

//比赛等待提示
bool ClientKernelSink_Baccarat::OnGFMatchWaitTips(tagMatchWaitTip* pMatchWaitTip)
{
	return true;
}

//比赛结果
bool ClientKernelSink_Baccarat::OnGFMatchResult(tagMatchResult* pMatchResult)
{
	return true;
}
//////////////////////////////////////////////////////////////////////////
//游戏事件

//旁观消息
bool ClientKernelSink_Baccarat::OnEventLookonMode(void* data, int dataSize)
{
	return true;
}

//场景消息
bool ClientKernelSink_Baccarat::OnEventSceneMessage(byte cbGameStatus, bool bLookonUser, void* data, int dataSize)
{
	IClientKernel* kernel = IClientKernel::get();
	kernel->SetGameStatus(GAME_SCENE_FREE);

	switch (cbGameStatus)
	{
	case GAME_SCENE_FREE:		//空闲状态
	{
		return onEventSceneFree(data, dataSize);
	}
	case GAME_SCENE_PLAY:		//游戏状态
	case GAME_SCENE_END:		//结束状态
	{
		return onEventSceneGameingAndEnd(data, dataSize);
	}
	}

	return false;
}

//游戏消息
bool ClientKernelSink_Baccarat::OnEventGameMessage(int wSubCmdID, void* pData, int wDataSize)
{
 	switch (wSubCmdID)
	{
	case SUB_S_GAME_FREE:		//游戏空闲
	{
									return OnSubGameFree(pData, wDataSize);
	}
	case SUB_S_GAME_START:		//游戏开始
	{
									return OnSubGameStart(pData, wDataSize);
	}
	case SUB_S_PLACE_JETTON:	//用户加注
	{
									return OnSubPlaceJetton(pData, wDataSize);
	}
	case SUB_S_APPLY_BANKER:	//申请做庄
	{
									return OnSubUserApplyBanker(pData, wDataSize);
	}
	case SUB_S_CANCEL_BANKER:	//取消做庄
	{
									return OnSubUserCancelBanker(pData, wDataSize);
	}
	case SUB_S_CHANGE_BANKER:	//切换庄家
	{
									return OnSubChangeBanker(pData, wDataSize);
	}
	case SUB_S_GAME_END:		//游戏结束
	{
									return OnSubGameEnd(pData, wDataSize);
	}
	case SUB_S_SEND_RECORD:		//游戏记录
	{
									return OnSubGameRecord(pData, wDataSize);
	}
	case SUB_S_PLACE_JETTON_FAIL:	//下注失败
	{
									return OnSubPlaceJettonFail(pData, wDataSize);
	}
	case SUB_S_AMDIN_COMMAND:	//管理员命令
	{
									return OnSubReqResult(pData, wDataSize);
	}
	case SUB_S_UPDATE_STORAGE:	//库存通知
	{
									return OnSubUpdateStorage(pData, wDataSize);
	}
	}

	//错误断言
	ASSERT(FALSE);

	return true;
}

// bool ClientKernelSink_Ox100::OnSubMoveCard(const void * pBuffer, word wDataSize)
// {
// 	return true;
// }

void ClientKernelSink_Baccarat::Showback(Node* pNode)
{
	CSpriteCard* pCard = dynamic_cast<CSpriteCard*>(pNode);
	pCard->setPosition(ccp(pCard->getPosition().x,pCard->getPosition().y-30));
}


void ClientKernelSink_Baccarat::ButtonWithCallBanker(bool isCallBanker)
{
	//发送叫庄
	IClientKernel * kernel = IClientKernel::get();
	if (isCallBanker)
	{
		kernel->SendSocketData(SUB_C_APPLY_BANKER, NULL, 0);
		m_bMeApplyBanker = true;
		//叫庄标识
		m_bCallBanker = true;
	}
	else
	{
		///< 如果是下注时间和发牌时间则不允许下庄
		if (kernel->GetGameStatus() == GAME_SCENE_BET || kernel->GetGameStatus() == GAME_SCENE_END)
		{
			return;
		}
		kernel->SendSocketData(SUB_C_CANCEL_BANKER, NULL, 0);
		m_bMeApplyBanker = false;
		//叫庄标识
		m_bCallBanker = false;
	}
	
	
	//[self PlayEffect:@"Click.wav"];

}
void ClientKernelSink_Baccarat::ButtonWithGrabBanker()
{
	//发送叫庄
// 	CMD_C_CallBanker_Ox banker;
// 	memset(&banker,0,sizeof(CMD_C_CallBanker_Ox));
// 	banker.bBanker=0;
// 
// 	IClientKernel* kernel = IClientKernel::get();
// 
// // 	m_pGameScene->removeChildByTag(201);
// // 	m_pGameScene->removeChildByTag(202);
// 
// 	kernel->SendSocketData(SUB_C_CALL_BANKER, &banker, sizeof(CMD_C_CallBanker_Ox));
// 	//叫庄标识
// 	m_bCallBanker=false;
// 	OnStartremove(51);
// 	IClientKernel * kernel = IClientKernel::get();
// 
// 	kernel->SendSocketData(SUB_C_QIANG_BANKER, NULL, 0);
	

}
//游戏开始
bool ClientKernelSink_Baccarat::OnSubGameStart(const void * pBuffer, word wDataSize)
{


	//效验数据
	ASSERT(wDataSize == sizeof(CMD_S_GameStart));
	if (wDataSize != sizeof(CMD_S_GameStart)) return false;

	//消息处理
	CMD_S_GameStart * pGameStart = (CMD_S_GameStart *)pBuffer;
	IClientKernel * kernel = IClientKernel::get();
	//庄家信息
	SetBankerInfo(pGameStart->wBankerUser, pGameStart->lBankerScore);

	//玩家信息
	m_lPlayBetScore = pGameStart->lPlayBetScore;
	m_lPlayFreeSocre = pGameStart->lPlayFreeSocre;

	kernel->SetGameClock(kernel->GetMeChairID(), IDI_PLACE_JETTON, pGameStart->cbTimeLeave);
	//设置状态
	kernel->SetGameStatus(GAME_SCENE_BET);

	//m_pGameScene->setTimeState(false, true, false);
	m_pGameScene->setTimeState(2);

	//更新控制
	m_pGameScene->setWindowsIsBetState();

	//播放声音
	SoundManager::shared()->playSound("BACCARAT_GAME_BEGIN");
	
	return true;
}
//庄家信息
void ClientKernelSink_Baccarat::SetBankerInfo(word wBanker, int64 lScore)
{
	IClientKernel * kernel = IClientKernel::get();
	m_wCurrentBanker = wBanker;
	m_lBankerScore = lScore;

	IClientUserItem * pUserItem = nullptr;
	if (m_wCurrentBanker != INVALID_CHAIR)
	{
		pUserItem = kernel->GetTableUserItem(m_wCurrentBanker);
	}
	tagUserInfo * pUserData = nullptr;
	if (pUserItem != nullptr)
		pUserData = m_wCurrentUser == INVALID_CHAIR ? nullptr : pUserItem->GetUserInfo();

	dword dwBankerUserID = (nullptr == pUserData) ? 0 : pUserData->dwUserID;

	m_pGameScene->SetBankerInfo(dwBankerUserID, m_lBankerScore);

}

bool ClientKernelSink_Baccarat::onSubRecordConnectBanker(const void * pBuffer, int wDataSize)
{
	//效验数据
// 	if (wDataSize != sizeof(CMD_S_StatusCall_Ox)) return false;
// 	CMD_S_StatusCall_Ox * callBanker = (CMD_S_StatusCall_Ox *)pBuffer;
// 
// 	m_wCurrentUser = callBanker->wCallBanker;
// 	m_Istan = false;
// 
// 
// 	IClientKernel* kernel = IClientKernel::get();
// 
// 
// 	// 	for (int i=0;i<GAME_PLAYER_Ox;i++)
// 	// 	{
// 	// 		m_pGameScene->removeChildByTag(120+i);//删除准备
// 	// 
// 	// 	}
// 	///< 隐藏准备
// 	m_pGameScene->clearPlayerPrepare();
// 
// 	//当前叫庄
// 	
// 	m_pGameScene->callBankerState(m_wCurrentUser);
// 	
// 
// 	//显示空闲倒计时 
// 	OnStartremove(50);
// 	OnStartremove(51);
// 
// 	kernel->SetGameClock(kernel->GetMeChairID(), 51, 20);

	return true;
}

bool ClientKernelSink_Baccarat::onSubRecordConnectAddGold(const void * pBuffer, int wDataSize)
{
	//效验数据
// 	if (wDataSize != sizeof(CMD_S_StatusScore_Ox)) return false;
// 	CMD_S_StatusScore_Ox * pGameStart = (CMD_S_StatusScore_Ox *)pBuffer;
// 
// 	IClientKernel* kernel = IClientKernel::get();
// 
// 	//游戏变量
// 	memset(m_cbHandCardData, 0, sizeof(byte)*MAX_COUNT);
// 	m_wCurrentUser = pGameStart->wBankerUser;
// 	m_lTurnMaxGold = pGameStart->lTurnMaxScore;
// 
// 	//m_pGameScene->removeChildByTag(200);			//删除等待叫庄
// 	m_pGameScene->clearCardData(0);
// 	m_pGameScene->clearCardData(1);
// 	///< 金币框隐藏
// 	m_pGameScene->showSettlementFrame(0, false);
// 	m_pGameScene->showSettlementFrame(1, false);
// 
// 	m_pGameScene->m_Card.setCardArrayWithRow("game_card-hd.png", CSpriteCard::g_cbCardData, 55);
// 	//庄家标志
// 	m_pGameScene->showBankerIconAtPlayerNode(kernel->SwitchViewChairID(m_wCurrentUser));
// 
// 	for (int i = 0; i < GAME_PLAYER_Ox; i++)
// 	{
// 		m_cbUserOxCard[i] = 0;
// 	}
// 
// 	m_cbSendCardCount = 0;
// 	m_cbSendCard = 0;
// 	memset(m_lTableScore, 0, sizeof(m_lTableScore));
// 	m_bCanShowHand = false;
// 	m_bShowHand = false;
// 	m_GameIsStart = true;
// 
// 	//闲家加注, 在里面在进行判断
// 	m_pGameScene->addGoldState(!m_wCurrentUser, m_lTurnMaxGold);
// 
// 	
// 	OnStartremove(51);
// 
// 	kernel->SetGameClock(kernel->GetMeChairID(), 52, 20);
// 
// 	//闲家请下注
// 	m_pGameScene->showTipsWithType(1);

	return true;
}

bool ClientKernelSink_Baccarat::onSubRecordConnectPlaying(const void * pBuffer, int wDataSize)
{
// 	if (wDataSize != sizeof(CMD_S_StatusPlay_Ox)) return false;
// 	CMD_S_StatusPlay_Ox * sendCard = (CMD_S_StatusPlay_Ox *)pBuffer;
// 
// 	m_wCurrentUser = sendCard->wBankerUser;
// 	m_lTurnMaxGold = sendCard->lTurnMaxScore;
// 
// 	m_pGameScene->m_Card.setCardArrayWithRow("game_card-hd.png", CSpriteCard::g_cbCardData, 55);
// 	
// 
// 	IClientKernel* kernel = IClientKernel::get();
// 	///< 显示庄家
// 	//庄家标志
// 	m_pGameScene->showBankerIconAtPlayerNode(kernel->SwitchViewChairID(m_wCurrentUser));
// 	///< 显示下注信息
// 	int64 addScore = 0;
// 	int chair_id = 0;
// 	if (sendCard->lTableScore[0] != 0)
// 	{
// 		addScore = sendCard->lTableScore[0];
// 		chair_id = 0;
// 	}
// 	else
// 	{
// 		addScore = sendCard->lTableScore[1];
// 		chair_id = 1;
// 	}
// 	m_pGameScene->playingState(chair_id, addScore);
// 
// 
// 	//拷贝手牌
// 	for (int i = 0; i < GAME_PLAYER_Ox; i++)
// 	{
// 		memset(m_cbHandCardData[i], 0, sizeof(byte)*MAX_COUNT);
// 		memcpy(m_cbHandCardData[i], sendCard->cbHandCardData[i], sizeof(byte)*MAX_COUNT);
// 	}
// 
// 	///< 首先发送牌
// 	m_pGameScene->sendCardToPoint();
// 
// 	bool alery_show_hard = false;
// 	///< 然后检查是否有用户摊牌
// 	if (sendCard->bOxCard[0] != 0xFF)
// 	{
// 		///< 椅子0摊牌
// 		if (kernel->GetMeChairID() == 0)
// 		{
// 			OnMeGive(1);
// 			m_pGameScene->playingState(true);
// 			alery_show_hard = true;
// 		}
// 		else
// 		{
// 			///<对面摊牌
// 			otherUserShowHard(0);
// 		}
// 	}
// 
// 	if (sendCard->bOxCard[1] != 0xFF)
// 	{
// 		///< 椅子1摊牌
// 		if (kernel->GetMeChairID() == 1)
// 		{
// 			OnMeGive(1);
// 			m_pGameScene->playingState(true);
// 			alery_show_hard = true;
// 		}
// 		else
// 		{
// 			///<对面摊牌
// 			otherUserShowHard(1);
// 		}
// 	}
// 
// 	OnStartremove(51);
// 	OnStartremove(52);
// 
// 
// 	if (!alery_show_hard)
// 		kernel->SetGameClock(kernel->GetMeChairID(), 53, 20);
// 
 	return true;
}



void ClientKernelSink_Baccarat::ShowMeCard(Node* sender)
{
// 	CSpriteCard* xxx2 = dynamic_cast<CSpriteCard*>(sender);
// 	byte CardMark=xxx2->getMark();
// 	if(CardMark!=0)
// 	{
// 		CSpriteCard* xxx1 =m_Card.getCardWithData(CardMark);
// 		xxx1->setPosition(xxx2->getPosition());
// 		m_pGameScene->addChild(xxx1, 1);
// 		xxx1->setSelectorEnded(this, callfuncN_selector(ClientKernelSink_Ox100::ccCardTouched));
// 		xxx1->setTag(xxx2->getTag());
// 		xxx2->removeFromParent();
// 
// 	}
// 	else
// 	{
// 		xxx2->setSelectorEnded(this, callfuncN_selector(ClientKernelSink_Ox100::ccCardTouched));
// 	}
}
void ClientKernelSink_Baccarat::ccCardTouched(Node* pNode)
{
// 	CSpriteCard* pCard = dynamic_cast<CSpriteCard*>(pNode);
// 	if(pCard->getTag()==995)
// 	{
// 		CSpriteCard* xxx2 = m_Card.getCardWithData(m_MyHandCardData1[m_wChairID][0]);
// 		xxx2->setPosition(pCard->getPosition());
// 		pCard->setVisible(false);
// 		m_pGameScene->addChild(xxx2);	
// 		xxx2->runAction(CCSequence::create(CCMoveTo::create(0.5f, ccp(pCard->getPosition().x,pCard->getPosition().y+30)), CCCallFuncN::create(this, callfuncN_selector(ClientKernelSink_Ox100::ShowMeback)), NULL));
// 	}

}
void ClientKernelSink_Baccarat::ShowMeback(Node* pNode)
{
// 	CSpriteCard* pCard = dynamic_cast<CSpriteCard*>(pNode);
// 	pCard->removeFromParent();
// 	CSpriteCard* xxx2 = (CSpriteCard *)m_pGameScene->getChildByTag(995);
// 	xxx2->setVisible(true);
}
void ClientKernelSink_Baccarat::OnMeAddGold(byte onArea, int64 addScore_1)
{
	IClientKernel* kernel = IClientKernel::get();

	CMD_C_PlaceBet addScore;
	memset(&addScore, 0, sizeof(addScore));

	addScore.cbBetArea = onArea;
	addScore.lBetScore = addScore_1;

	kernel->SendSocketData(SUB_C_PLACE_JETTON, &addScore, sizeof(CMD_C_PlaceBet));
}

void ClientKernelSink_Baccarat::OnMeGive(int index)
{
}
//发牌消息
bool ClientKernelSink_Baccarat::OnSubSendCard(const void * pBuffer, word wDataSize)
{
// 	if (wDataSize != sizeof(CMD_S_SendCard_Ox)) return false;
// 	CMD_S_SendCard_Ox * sendCard = (CMD_S_SendCard_Ox *)pBuffer;
// 
// 	m_pGameScene->m_Card.setCardArrayWithRow("game_card-hd.png", CSpriteCard::g_cbCardData, 55);
// 
// 	//拷贝手牌
// 	for (int i = 0; i < GAME_PLAYER_Ox; i++)
// 	{
// 		memset(m_cbHandCardData[i], 0, sizeof(byte)*MAX_COUNT);
// 		memcpy(m_cbHandCardData[i], sendCard->cbCardData[i], sizeof(byte)*MAX_COUNT);
// 	}
// 
// 
// 	m_pGameScene->sendCardStart();
// 
// 	OnStartremove(51);
// 	OnStartremove(52);
// 
// 	IClientKernel* kernel = IClientKernel::get();
// 	kernel->SetGameClock(kernel->GetMeChairID(), 53, 20);

	return true;
}

//游戏结束
bool ClientKernelSink_Baccarat::OnSubGameEnd(const void * pBuffer, word wDataSize)
{
	//效验数据
	ASSERT(wDataSize == sizeof(CMD_S_GameEnd));
	if (wDataSize != sizeof(CMD_S_GameEnd)) return false;

	//消息处理
	CMD_S_GameEnd * pGameEnd = (CMD_S_GameEnd *)pBuffer;
	IClientKernel * kernel = IClientKernel::get();

	kernel->SetGameClock(kernel->GetMeChairID(), IDI_DISPATCH_CARD, pGameEnd->cbTimeLeave);

	for (int i = 0; i < GAME_PLAYER_Baccarat; i++)
	{
		memset(m_cbTableCardArray[i], 0, sizeof(byte)*MAX_COUNT);
		memcpy(m_cbTableCardArray[i], pGameEnd->cbTableCardArray[i], sizeof(byte)*MAX_COUNT);
	}
	memcpy(m_cbCardCount, pGameEnd->cbCardCount, sizeof(m_cbCardCount));
	memcpy(m_cbPlayScore, pGameEnd->lPlayScore, sizeof(m_cbPlayScore));

	// 设置状态
	kernel->SetGameStatus(GAME_SCENE_END);

	//m_pGameScene->setTimeState(false, false, true);
	m_pGameScene->setTimeState(3);

	///<开启开牌舞台
	m_pGameScene->ShowCardStage(m_cbTableCardArray, m_cbCardCount[INDEX_PLAYER], m_cbCardCount[INDEX_BANKER], pGameEnd->cbTimeLeave, 1);

	//<庄家信息
	m_pGameScene->SetBankerScore(pGameEnd->nBankerTime, pGameEnd->lBankerTotallScore);

	m_pGameScene->SetCurGameScore(m_cbPlayScore, pGameEnd->lPlayAllScore);

	///< 清理筹码消息
	for (int i = 0; i < AREA_COUNT + 1; i++)
	{
		m_lUserJettonScore[i] = 0;	//个人总注
		m_lAllJettonScore[i] = 0;	//全体总注
	}
	
	m_pGameScene->setWindowsIsGameEndState();

	//播放声音
	SoundManager::shared()->playSound("BACCARAT_GAME_END");

	return true;
}

//根据游戏模式获取金币类型
longlong ClientKernelSink_Baccarat::GetUserScoreEx(IClientUserItem* pGameUserItem)
{
	if (pGameUserItem == NULL)
	{
		return 0L;
	}

// 	if (GameConfig::GetInstance().nGameMode == MODE_MEDAL)
// 	{
// 	//	return (longlong)pGameUserItem->GetUserMedal();
// 	}

	return pGameUserItem->GetUserScore();
}

bool ClientKernelSink_Baccarat::OnUpdate(float fDt)
{
	IClientKernel* kernel = IClientKernel::get();

	if(kernel == 0 || m_pGameScene == 0)
		return false;
	dword timeNow = CoTimer::getCurrentTime();//timeGetTime();
	word wMeChairID = kernel->GetMeChairID();

	//AllowFire(!m_pGameScene_SH2->IsSwitchingScene());

	//	m_FishMgr.Cleanup();
	//	m_BulletMgr.Cleanup();

	//m_BlackWater->OnUpdate(fDt);
	if (wMeChairID < GAME_PLAYER)
	{
/*		xPoint mouse_pos;*/
// 		dword fid = m_pLockFishMgr->GetLockFishID(wMeChairID);
// 		CFishNode* pFish = m_FishMgr.GetEntity(fid);
// 		if(pFish == NULL || !pFish->InsideScreen())
// 		{
// 			UnLockFish(wMeChairID);
// 			if(pFish != NULL) 
// 				DoLockFish();
// 		}
// 		else
// 		{
// 			mouse_pos = m_pLockFishMgr->LockPos(wMeChairID);
// 		}

	}

//	UpdateShakeScreen(fDt);

// 	//震动屏幕
// 	m_pGameScene->OnShakePreen(m_ptOffest.x_, m_ptOffest.y_);

	return false;
}

void ClientKernelSink_Baccarat::OnDlgBlackWhite(int iUserID, int iMode, unsigned int data)
{
// 	IClientKernel* kernel = IClientKernel::get();
// 	CMD_C_UserFilter userFilter;
// 	zeromemory(&userFilter, sizeof(userFilter));
// 	userFilter.user_id = iUserID;
// 	userFilter.operate_code = iMode;
// 
// 	kernel->SendSocketData(SUB_C_USER_FILTER, &userFilter, sizeof(userFilter));
}

void ClientKernelSink_Baccarat::OnDlgStorage(int iMode, longlong lValue, unsigned int data)
{
	IClientKernel* kernel = IClientKernel::get();
// 
// 	CMD_C_Storage stroage;
// 	zeromemory(&stroage, sizeof(stroage));
// 	stroage.iMode = iMode;
// 	stroage.lValue = lValue;
// 
// 	kernel->SendSocketData(SUB_C_STORAGE, &stroage, sizeof(stroage));
}
void ClientKernelSink_Baccarat::OnStartremove(int tga)
{
	IClientKernel* kernel = IClientKernel::get();
	kernel->KillGameClock(tga);

	///m_pGameScene->removeChildByTag(48);


}
//////////////////////////////////////////////////////////////////////////
//时钟事件
//用户时钟
void ClientKernelSink_Baccarat::OnEventUserClock(word wChairID, word wUserClock)
{
	IClientKernel * kernel = IClientKernel::get();
	if (kernel && m_pGameScene && wChairID == kernel->GetMeChairID())
	{
		m_pGameScene->setMeUserClock(wUserClock);
	}
}

//时钟删除
bool ClientKernelSink_Baccarat::OnEventGameClockKill(word wChairID)
{
	return true;
}

//时钟信息
bool ClientKernelSink_Baccarat::OnEventGameClockInfo(word wChairID, uint nElapse, word wClockID)
{
	IClientKernel * kernel = IClientKernel::get();

	switch (wClockID)
	{
	case IDI_FREE:
	{

					 return true;
	}
	case IDI_PLACE_JETTON:
	{
							 if (nElapse == 0)
							 {
							 }
							 if (nElapse <= 5)
								 SoundManager::shared()->playSound("BACCARAT_TIME_WARIMG");
							 return true;
	}
	case IDI_DISPATCH_CARD:
	{
							  return true;
	}
	}
	return false;
}

//////////////////////////////////////////////////////////////////////////
//用户事件

//用户进入
void ClientKernelSink_Baccarat::OnEventUserEnter(IClientUserItem * pIClientUserItem, bool bLookonUser)
{
	Director * director = Director::getInstance();
	Scene * pScene = director->getRunningScene();
	if (pScene->getTag() == KIND_ID && pScene->getName() == "GameScene")
	{
		///< 表示是自己!!
		m_pGameScene = dynamic_cast<GameScene_Baccarat *>(pScene);
	}
	else if (!m_pGameScene)
	{
		m_pGameScene = GameScene_Baccarat::create();
	}

	if(!bLookonUser && m_pGameScene)
	{ 
			m_pGameScene->showUserInfo(pIClientUserItem,pIClientUserItem->GetUserInfo()->cbUserStatus);
	}
}
//用户离开
void ClientKernelSink_Baccarat::OnEventUserLeave(IClientUserItem * pIClientUserItem, bool bLookonUser)
{

	if(!bLookonUser)
	{
		///< 是不是观看用户
		word wChairID = pIClientUserItem->GetChairID();
		IClientKernel* kernel = IClientKernel::get();
		if (kernel->GetMeChairID() != INVALID_CHAIR && kernel->GetMeChairID() != wChairID)
		{
			///< 其他用户离开
			m_pGameScene->net_user_level(wChairID);
		}
		else if(m_pGameScene)
		{
// 			if (IClientKernel::get())
// 				IClientKernel::get()->Intermit(GameExitCode_Normal);

			G_NOTIFY_D("EVENT_GAME_EXIT", MTData::create()); 
		}
	}
	
}

//用户积分
void ClientKernelSink_Baccarat::OnEventUserScore(IClientUserItem * pIClientUserItem, bool bLookonUser)
{
	if (!bLookonUser && m_pGameScene)
		G_NOTIFY_D("EVENT_USER_SCORE", MTData::create(0,0,0,"","","",pIClientUserItem));
}

//用户状态
void ClientKernelSink_Baccarat::OnEventUserStatus(IClientUserItem * pIClientUserItem, bool bLookonUser)
{
	if (!bLookonUser)
		G_NOTIFY_D("EVENT_USER_STATUS", MTData::create((unsigned int)pIClientUserItem));

	IClientKernel* kernel = IClientKernel::get();
	if (m_pGameScene)
	{
		//US_READY
		m_pGameScene->net_user_state_update(pIClientUserItem->GetChairID(), pIClientUserItem->GetUserStatus());
	}


}

//用户属性
void ClientKernelSink_Baccarat::OnEventUserAttrib(IClientUserItem * pIClientUserItem, bool bLookonUser)
{
}

//用户头像
void ClientKernelSink_Baccarat::OnEventCustomFace(IClientUserItem * pIClientUserItem, bool bLookonUser)
{
}


//开始消息
int ClientKernelSink_Baccarat::OnMessageStart(uint wParam, uint lParam)
{
	IClientKernel * kernel = IClientKernel::get();

	//游戏变量
	m_wCurrentUser = INVALID_CHAIR;

	//删除时间
	//kernel->KillGameClock(IDI_START_GAME);

	return 0;
}

void ClientKernelSink_Baccarat::OnGameTimer(float dt)
{
	//OnTimer(IDI_GAME_TIME);
}


//时间消息
void ClientKernelSink_Baccarat::OnTimer(uint nIDEvent)
{

	IClientKernel * kernel = IClientKernel::get();
	if (kernel == 0)
		return;

	//游戏时间
	if ((nIDEvent == IDI_PLACE_JETTON) && (m_wCurrentUser != INVALID_CHAIR))
	{
		//变量定义
		bool bWarnning = false;
		bool bDeductTime = false;

	
		//设置界面
		word wViewChairID = m_wCurrentUser;

		//发送超时
		if (m_wCurrentUser == kernel->GetMeChairID())
		{
			//kernel->SendSocketData(MDM_GF_GAME, SUB_C_CONCLUDE_REQUEST, &ConcludeRequest, sizeof(ConcludeRequest));
		}

		return;
	}
}

//游戏空闲
bool ClientKernelSink_Baccarat::OnSubGameFree(const void * pBuffer, word wDataSize)
{
	//效验数据
	ASSERT(wDataSize == sizeof(CMD_S_GameFree));
	if (wDataSize != sizeof(CMD_S_GameFree)) return false;

	//消息处理
	CMD_S_GameFree * pGameFree = (CMD_S_GameFree *)pBuffer;

	IClientKernel * kernel = IClientKernel::get();
	//设置时间
	kernel->SetGameClock(kernel->GetMeChairID(), IDI_FREE, pGameFree->cbTimeLeave);
	//
	kernel->SetGameStatus(GAME_SCENE_FREE);

	//清理用户筹码
	m_pGameScene->clearUserJetton();
	//隐藏特效框
	//m_pGameScene->setWinnerSide(false, false, false, false, false);

	for (int nAreaIndex = AREA_XIAN; nAreaIndex <= AREA_ZHUANG_DUI; ++nAreaIndex)
		SetMePlaceJetton(nAreaIndex, 0);

	///< 等待状态
	m_pGameScene->setWindowsIsWaitState();

	m_pGameScene->setTimeState(1);

	kernel->KillGameClock(IDI_DISPATCH_CARD);

	//判断是否可以坐庄
	m_lPlayFreeSocre = kernel->GetMeUserItem()->GetUserScore();
	m_pGameScene->mFrameLayer->playerScoreIsApplyBanker(m_lPlayFreeSocre);

	return true;
}

bool ClientKernelSink_Baccarat::OnSubPlaceJetton(const void * pBuffer, word wDataSize)
{
	//效验数据
	ASSERT(wDataSize == sizeof(CMD_S_PlaceBet));
	if (wDataSize != sizeof(CMD_S_PlaceBet)) return false;

	//srand(GetTickCount());

	//消息处理
	CMD_S_PlaceBet * pPlaceJetton = (CMD_S_PlaceBet *)pBuffer;
	IClientKernel * kernel = IClientKernel::get();

	if (kernel->GetMeChairID() == pPlaceJetton->wChairID)
	{
		m_lUserJettonScore[pPlaceJetton->cbBetArea] += pPlaceJetton->lBetScore;
	}

	m_lAllJettonScore[pPlaceJetton->cbBetArea] += pPlaceJetton->lBetScore;

	m_pGameScene->onAreaAddJetton(pPlaceJetton->wChairID, pPlaceJetton->cbBetArea, pPlaceJetton->lBetScore);

	if (pPlaceJetton->wChairID == kernel->GetMeChairID())
		SoundManager::shared()->playSound("BACCARAT_ADD_MYJETTON");
	else
		SoundManager::shared()->playSound("BACCARAT_ADD_JETTON");

	return true;
}
//申请坐庄
bool ClientKernelSink_Baccarat::OnSubUserApplyBanker(const void * pBuffer, word wDataSize)
{
	//效验数据
	ASSERT(wDataSize == sizeof(CMD_S_ApplyBanker));
	if (wDataSize != sizeof(CMD_S_ApplyBanker)) return false;

	//消息处理
	CMD_S_ApplyBanker * pApplyBanker = (CMD_S_ApplyBanker *)pBuffer;
	IClientKernel * kernel = IClientKernel::get();

	IClientUserItem * pUserItem = kernel->GetTableUserItem(pApplyBanker->wApplyUser);
	tagUserInfo * pUserData = nullptr;

	if (pUserItem == NULL)
		return TRUE;

	//if (pUserItem != nullptr)
	//	pUserData = pUserItem->GetUserInfo();
	
	//插入玩家
	if (m_wCurrentBanker != pApplyBanker->wApplyUser)
	{
		tagApplyUser_Baccarat ApplyUser;
		ApplyUser.wChairID = pApplyBanker->wApplyUser;
		ApplyUser.bQiang = false;
		ApplyUser.strUserName = pUserItem->GetNickName();
		ApplyUser.lUserScore = pUserItem->GetUserScore();

		m_pGameScene->addUserToBankerPanel(ApplyUser);
	}

	//自己判断
	if (kernel->GetMeChairID()==pApplyBanker->wApplyUser) 	m_bMeApplyBanker = true;

	return true;
}
//取消申请
bool ClientKernelSink_Baccarat::OnSubUserCancelBanker(const void * pBuffer, word wDataSize)
{
	//效验数据
	ASSERT(wDataSize == sizeof(CMD_S_CancelBanker));
	if (wDataSize != sizeof(CMD_S_CancelBanker)) return false;

	//消息处理
	CMD_S_CancelBanker * pCancelBanker = (CMD_S_CancelBanker *)pBuffer;
	IClientKernel * kernel = IClientKernel::get();

	tagApplyUser_Baccarat ApplyUser;
	ApplyUser.wChairID = pCancelBanker->wCancelUser;

	if (kernel->GetMeChairID() == pCancelBanker->wCancelUser)
	{
		m_bMeApplyBanker = false;
		//m_bMeQiangBanker = false;
	}

	m_pGameScene->deleteUserToBankerPanel(ApplyUser);

	return true;
}

bool ClientKernelSink_Baccarat::OnSubChangeBanker(const void * pBuffer, word wDataSize)
{
	//效验数据
	ASSERT(wDataSize == sizeof(CMD_S_ChangeBanker));
	if (wDataSize != sizeof(CMD_S_ChangeBanker)) return false;

	//消息处理
	CMD_S_ChangeBanker * pChangeBanker = (CMD_S_ChangeBanker *)pBuffer;
	IClientKernel * kernel = IClientKernel::get();

	if (m_wCurrentBanker == kernel->GetMeChairID() && pChangeBanker->wBankerUser != kernel->GetMeChairID())
	{
		///< 本人下庄
		m_bMeApplyBanker = false;
		//m_bMeQiangBanker = false;
	}
	else if (pChangeBanker->wBankerUser == kernel->GetMeChairID())
	{
		///< 本人上庄
		m_bMeApplyBanker = true;
	}

	//轮换庄家字幕
	int lunhuan = 0;
	if (pChangeBanker->wBankerUser == kernel->GetMeChairID())
	{
		lunhuan = 1;
	} 
	else if (pChangeBanker->wBankerUser == INVALID_CHAIR)
	{
		lunhuan = 2;
	}

	m_pGameScene->mFrameLayer->huanZhuang(lunhuan);

	SetBankerInfo(pChangeBanker->wBankerUser, pChangeBanker->lBankerScore);

	//m_pGameScene->SetBankerInfo(pChangeBanker->wBankerUser, pChangeBanker->lBankerScore);
	m_lBankerWinScore = 0;
	m_wBankerTime = 0;

	//删除玩家
	if (m_wCurrentBanker != INVALID_CHAIR)
	{
		IClientUserItem  *pBankerUserData = kernel->GetTableUserItem(m_wCurrentBanker);
		if (pBankerUserData != NULL)
		{
			tagApplyUser_Baccarat ApplyUser;
			ApplyUser.strUserName = pBankerUserData->GetNickName();
			ApplyUser.wChairID = pChangeBanker->wBankerUser;
			m_pGameScene->deleteUserToBankerPanel(ApplyUser);
		}
	}

	//判断是否可以坐庄
	m_lPlayFreeSocre = kernel->GetMeUserItem()->GetUserScore();
	m_pGameScene->mFrameLayer->playerScoreIsApplyBanker(m_lPlayFreeSocre);

	SoundManager::shared()->playSound("BACCARAT_HUAN_ZHUANG");

	return true;
}

bool ClientKernelSink_Baccarat::OnSubGameRecord(const void * pBuffer, word wDataSize)
{
	//效验参数
	ASSERT(wDataSize%sizeof(tagServerGameRecord) == 0);
	if (wDataSize%sizeof(tagServerGameRecord) != 0) return false;
	//设置记录
	word wRecordCount = wDataSize / sizeof(tagServerGameRecord);
 	for (word wIndex = 0; wIndex < wRecordCount; wIndex++)
 	{
 		tagServerGameRecord * pServerGameRecord = (((tagServerGameRecord *)pBuffer) + wIndex);

		m_pGameScene->SetGameHistory(pServerGameRecord->cbKingWinner, pServerGameRecord->bPlayerTwoPair, pServerGameRecord->bBankerTwoPair, pServerGameRecord->cbPlayerCount, pServerGameRecord->cbBankerCount);
	}
	return true;
}

bool ClientKernelSink_Baccarat::OnSubPlaceJettonFail(const void * pBuffer, word wDataSize)
{
	//效验数据
	ASSERT(wDataSize == sizeof(CMD_S_PlaceBetFail));
	if (wDataSize != sizeof(CMD_S_PlaceBetFail)) return false;

	IClientKernel * kernel = IClientKernel::get();
	//消息处理
	CMD_S_PlaceBetFail * pPlaceJettonFail = (CMD_S_PlaceBetFail *)pBuffer;

	byte cbViewIndex = pPlaceJettonFail->lBetArea;
	ASSERT(cbViewIndex <= AREA_ZHUANG_DUI && cbViewIndex >= AREA_XIAN);
	if (!(cbViewIndex <= AREA_ZHUANG_DUI && cbViewIndex >= AREA_XIAN)) return false;

// 	m_pGameScene->PlaceUserJetton(pPlaceJettonFail->lJettonArea, -pPlaceJettonFail->lPlaceScore);
// 
 	if (kernel->GetMeChairID() == pPlaceJettonFail->wPlaceUser)
 	{
//		m_lUserJettonScore[cbViewIndex] -= pPlaceJettonFail->lPlaceScore;
//		m_lAllJettonScore[cbViewIndex] -= pPlaceJettonFail->lPlaceScore;
// 		int64 lJettonCount = pPlaceJettonFail->lPlaceScore;
// 		//合法校验
// 		ASSERT(m_lUserJettonScore[cbViewIndex] >= lJettonCount);
// 		if (lJettonCount > m_lUserJettonScore[cbViewIndex]) return false;
// 
// 		//设置下注
// 		m_lUserJettonScore[cbViewIndex] -= lJettonCount;
// 		m_pGameScene->SetMePlaceJetton(cbViewIndex, m_lUserJettonScore[cbViewIndex]);
 	}

	return true;
}

bool ClientKernelSink_Baccarat::OnSubReqResult(const void * pBuffer, word wDataSize)
{

	ASSERT(wDataSize == sizeof(CMD_S_CommandResult));
	if (wDataSize != sizeof(CMD_S_CommandResult)) return false;

	return true;
}

bool ClientKernelSink_Baccarat::OnSubUpdateStorage(const void * pBuffer, word wDataSize)
{
// 	ASSERT(wDataSize == sizeof(CMD_S_UpdateStorage));
// 	if (wDataSize != sizeof(CMD_S_UpdateStorage)) return false;

// 	if (NULL != m_GameClientView.m_pClientControlDlg && NULL != m_GameClientView.m_pClientControlDlg->GetSafeHwnd())
// 	{
// 		m_GameClientView.m_pClientControlDlg->UpdateStorage(pBuffer);
// 	}
	return true;
}

bool ClientKernelSink_Baccarat::OnSubQiangBanker(void * pBuffer, word wDataSize)
{
	//效验数据
	ASSERT(wDataSize == sizeof(CMD_S_ApplyBanker));
	if (wDataSize != sizeof(CMD_S_ApplyBanker)) return false;

	//消息处理
	CMD_S_ApplyBanker * pApplyBanker = (CMD_S_ApplyBanker *)pBuffer;
	IClientKernel * kernel = IClientKernel::get();

	//获取玩家
	IClientUserItem * pUserData = kernel->GetTableUserItem(pApplyBanker->wApplyUser);

	if (kernel->GetMeChairID() == pApplyBanker->wApplyUser) m_bMeApplyBanker = true;

	//插入玩家
	if (m_wCurrentBanker != pApplyBanker->wApplyUser)
	{
		tagApplyUser_Baccarat ApplyUser;
		ApplyUser.bQiang = true;
		ApplyUser.strUserName = pUserData->GetNickName();
		ApplyUser.lUserScore = pUserData->GetUserScore();
		m_pGameScene->insertUserToBankerPanel(ApplyUser);
		///< 更新坐庄面板
	}



	return true;
}

void ClientKernelSink_Baccarat::SetMePlaceJetton(byte cbViewIndex, int64 lJettonCount)
{
	//合法判断
/*	ASSERT(cbViewIndex < AREA_XIAN && cbViewIndex > AREA_ZHUANG_DUI);*/
	if (!(cbViewIndex >= AREA_XIAN && cbViewIndex <= AREA_ZHUANG_DUI)) return;

	//设置变量
	m_lUserJettonScore[cbViewIndex] = lJettonCount;

	//设置界面
	m_pGameScene->SetMePlaceJetton(cbViewIndex, lJettonCount);
}

bool ClientKernelSink_Baccarat::onEventSceneFree(void * pData, int wDataSize)
{
	IClientKernel * kernel = IClientKernel::get();
	if (!kernel) return false;
	//效验数据
	ASSERT(wDataSize == sizeof(CMD_S_StatusFree_Baccarat));
	if (wDataSize != sizeof(CMD_S_StatusFree_Baccarat)) return false;

	//消息处理
	CMD_S_StatusFree_Baccarat * pStatusFree = (CMD_S_StatusFree_Baccarat *)pData;

	//设置时间
	kernel->SetGameClock(kernel->GetMeChairID(), IDI_FREE, pStatusFree->cbTimeLeave);

	m_pGameScene->setTimeState(1);

	//玩家信息(最大分限制)
	m_lPlayFreeSocre = pStatusFree->lPlayFreeSocre;

	const tagUserInfo *pMeUserData = kernel->GetMeUserItem()->GetUserInfo();

	//庄家信息
	SetBankerInfo(pStatusFree->wBankerUser, pStatusFree->lBankerScore);
	m_pGameScene->SetBankerScore(pStatusFree->wBankerTime, pStatusFree->lBankerWinScore);
	m_bEnableSysBanker = pStatusFree->bEnableSysBanker;

	//控制信息
	m_lApplyBankerCondition = pStatusFree->lApplyBankerCondition;
	m_lAreaLimitScore = pStatusFree->lAreaLimitScore;

	if (kernel->GetMeChairID() == m_wCurrentBanker)
	{
		m_bMeApplyBanker = true;
	}

	//空闲状态设置申请当庄条件
	m_pGameScene->mFrameLayer->setScoreApplyBanker(pStatusFree->lApplyBankerCondition);

	//判断是否可以坐庄
	m_lPlayFreeSocre = kernel->GetMeUserItem()->GetUserScore();
	m_pGameScene->mFrameLayer->playerScoreIsApplyBanker(m_lPlayFreeSocre);

	//设置等待状态处理
	m_pGameScene->setWindowsIsWaitState();

	return true;
}

bool ClientKernelSink_Baccarat::onEventSceneGameingAndEnd(void * pData, int wDataSize)
{
	ASSERT(wDataSize == sizeof(CMD_S_StatusPlay));
	if (wDataSize != sizeof(CMD_S_StatusPlay)) return false;

	IClientKernel * kernel = IClientKernel::get();
	//消息处理
	CMD_S_StatusPlay * pStatusPlay = (CMD_S_StatusPlay *)pData;

	//下注信息
	for (int nAreaIndex = 1; nAreaIndex <= AREA_COUNT; ++nAreaIndex)
	{
		m_pGameScene->onAreaTotalJetton(nAreaIndex, pStatusPlay->lAllBet[nAreaIndex]);
		SetMePlaceJetton(nAreaIndex, pStatusPlay->lPlayBet[nAreaIndex]);
	}

	//玩家信息
	m_lPlayBetScore = pStatusPlay->lPlayBetScore;
	m_lPlayFreeSocre = pStatusPlay->lPlayFreeSocre;

	const tagUserInfo *pMeUserData = kernel->GetMeUserItem()->GetUserInfo();

	//控制信息
	m_lApplyBankerCondition = pStatusPlay->lApplyBankerCondition;
	m_lAreaLimitScore = pStatusPlay->lAreaLimitScore;

	//空闲状态设置申请当庄条件
	m_pGameScene->mFrameLayer->setScoreApplyBanker(pStatusPlay->lApplyBankerCondition);

	//押注或者比牌中进入游戏,不能上庄
	m_pGameScene->mFrameLayer->playerScoreIsApplyBanker(-1);

	//庄家信息
	SetBankerInfo(pStatusPlay->wBankerUser, pStatusPlay->lBankerScore);
	m_pGameScene->SetBankerScore(pStatusPlay->wBankerTime, pStatusPlay->lBankerScore);

	if (kernel->GetMeChairID() == m_wCurrentBanker)
	{
		m_bMeApplyBanker = true;
	}

	//设置状态
	kernel->SetGameStatus(pStatusPlay->cbGameStatus);

	if (pStatusPlay->cbGameStatus == GAME_SCENE_END)
	{
		//m_pGameScene->setTimeState(false, false, true);
		m_pGameScene->setTimeState(3);
		
		kernel->SetGameClock(kernel->GetMeChairID(), IDI_DISPATCH_CARD, pStatusPlay->cbTimeLeave);

		for (int i = 0; i < GAME_PLAYER_Baccarat; i++)
		{
			memset(m_cbTableCardArray[i], 0, sizeof(byte)*MAX_COUNT);
			memcpy(m_cbTableCardArray[i], pStatusPlay->cbTableCardArray[i], sizeof(byte)*MAX_COUNT);
		}
		memcpy(m_cbCardCount, pStatusPlay->cbCardCount, sizeof(m_cbCardCount));
		memcpy(m_cbPlayScore, pStatusPlay->lPlayScore, sizeof(m_cbPlayScore));
		m_pGameScene->SetCurGameScore(m_cbPlayScore, pStatusPlay->lPlayAllScore);
		///<开启开牌舞台
		m_pGameScene->ShowCardStage(m_cbTableCardArray, m_cbCardCount[INDEX_PLAYER], m_cbCardCount[INDEX_BANKER], pStatusPlay->cbTimeLeave, 0);
	}
	else
	{
		//m_pGameScene->setTimeState(false, true, false);
		m_pGameScene->setTimeState(2);

		kernel->SetGameClock(kernel->GetMeChairID(), IDI_PLACE_JETTON, pStatusPlay->cbTimeLeave);
		m_pGameScene->setWindowsIsBetState();
	}
	
	return true;
}

int64 ClientKernelSink_Baccarat::getCurJettonTotal()
{
	int64 pTotalScore = 0;
	for (int i = 0; i < AREA_COUNT + 1; i++)
	{
		pTotalScore += m_lAllJettonScore[i] * (getCurrentAreaMult(i) - 1);	//全体总注
	}

	return pTotalScore;
}

int64 ClientKernelSink_Baccarat::getMyJettonTotal()
{
	int64 pTotalScore = 0;
	for (int i = 0; i < AREA_COUNT + 1; i++)
	{
		pTotalScore += m_lUserJettonScore[i];
	}

	return pTotalScore;
}

int ClientKernelSink_Baccarat::getCurrentAreaMult(int area)
{
	switch (area)
	{
	case AREA_XIAN:
		return MULTIPLE_XIAN;
	case AREA_PING:
		return MULTIPLE_PING;
	case AREA_ZHUANG:
		return MULTIPLE_ZHUANG;
	case AREA_XIAN_TIAN:
		return MULTIPLE_XIAN_TIAN;
	case AREA_ZHUANG_TIAN:
		return MULTIPLE_ZHUANG_TIAN;
	case AREA_TONG_DUI:
		return MULTIPLE_TONG_DIAN;
	case AREA_XIAN_DUI:
		return MULTIPLE_XIAN_PING;
	case AREA_ZHUANG_DUI:
		return MULTIPLE_ZHUANG_PING;
	default:
		break;
	}
}

void ClientKernelSink_Baccarat::CloseGameDelayClient()
{
	m_pGameScene->ExitGameTiming();
}
