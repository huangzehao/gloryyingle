#include "FrameLayer_Baccarat.h"
#include "Tools/tools/StringData.h"
#include "ClientKernelSink_Baccarat.h"
#include "Kernel/kernel/game/IClientKernel.h"
#include "Tools/Manager/SpriteHelper.h"
#include "GameScene_Baccarat.h"
#include "Tools/Manager/SoundManager.h"
#include "ApplyUserList_Baccarat.h"
#include "common/GameSetLayer.h"
#include "Tools/tools/SimpleTools.h"
#include "Tools/Dialog/NewDialog.h"
#include "Tools/tools/Convert.h"
#include "Tools/ViewHeader.h"
#include "ui/UIWidget.h"
///< 不允许的位置
#define InvalidPostion Vec2(-500, -500)

USING_NS_CC_EXT;
using namespace ui;
FrameLayer_Baccarat::FrameLayer_Baccarat()
{
	mBankerPanel = nullptr;
	mSettleAccountsPanel = nullptr;
	mTrendPanel = nullptr;
	mNowClickJetton = 0;
	mMyTotalScore = 0;
	mTotalScore = 0;
	num_zong = 0;
	num_xian = 0;
	num_ping = 0;
	num_zhuang = 0;
	num_xianduizi = 0;
	num_zhuangduizi = 0;
	num_xiantianwang = 0;
	num_zhuangtianwang = 0;
	num_shu_copy = 0;
	num_da_copy = 0;
	bai_xian = 0;
	bai_ping = 0;
	bai_zhuang = 0;
	type_now = 0;
	type_old = 5;
	lApplyBankerCondition = 0;
}

FrameLayer_Baccarat::~FrameLayer_Baccarat()
{
	bool isHave = this->isScheduled(SEL_SCHEDULE(&FrameLayer_Baccarat::sche_time_out));
	if (isHave)
	{
		this->unschedule(SEL_SCHEDULE(&FrameLayer_Baccarat::sche_time_out));
	}
}

FrameLayer_Baccarat * FrameLayer_Baccarat::create(Widget *mRoot)
{
	FrameLayer_Baccarat * node = new FrameLayer_Baccarat;
	if (node && node->init(mRoot))
	{
		node->autorelease();
		return node;
	}
	delete node;
	return nullptr;
}

bool FrameLayer_Baccarat::init(Widget *mRoot)
{
	if (!Layer::init())
	{
		return false;
	}

	createMainPanel(mRoot);
	createBankerPanel();
	createSettleAccountsPanel();
	createTrendPanel();

	return true;
}


void FrameLayer_Baccarat::createMainPanel(Widget *mRoot)
{
	Size winSize = Director::getInstance()->getWinSize();
	SpriteFrameCache* cache = SpriteFrameCache::getInstance();

	//1.解析cocostudio json文件 
//	Widget * mRoot = cocostudio::GUIReader::getInstance()->widgetFromJsonFile("baccaratGameScene/Baccarat_Main.json");
	Widget * Panel_Main = dynamic_cast<Widget *>(mRoot->getChildByName("Panel_Main"));
	mPanelMain = Panel_Main;
	this->addChild(mRoot);

	//2.将几大块功能层对应解析命名

	///退出游戏
	Button * btn_close_game = dynamic_cast<Button *>(Panel_Main->getChildByName("btn_close_game"));
	btn_close_game->addTouchEventListener(this, SEL_TouchEvent(&FrameLayer_Baccarat::btnCloseGameCallBack));

	//1.压注层 --------------------------------------------------
	Panel_game_bet = dynamic_cast<Widget *>(Panel_Main->getChildByName("Panel_game_bet"));
	Panel_game_stage = dynamic_cast<Widget *>(Panel_Main->getChildByName("Panel_game_stage"));
	//1.1 时间
	img_time_bg = dynamic_cast<ImageView *>(Panel_game_bet->getChildByName("img_time_bg"));
	img_time_bg->setZOrder(2);
	//1.1.1类型
	img_time_type = dynamic_cast<ImageView *>(img_time_bg->getChildByName("img_time_type"));
	//1.1.2 num：
	al_time_nums = dynamic_cast<TextAtlas *>(img_time_bg->getChildByName("al_time_nums"));

	//1.2 按钮
	Button * btn_Baccact_xian = dynamic_cast<Button *>(Panel_game_bet->getChildByName("btn_Baccact_xian"));
	btn_Baccact_xian->setTag(CenterTypeTag::CENTER_TAG_XIAN);
	btn_Baccact_xian->addTouchEventListener(this, SEL_TouchEvent(&FrameLayer_Baccarat::btnCenterClickCallBack));
	getBtnTips(btn_Baccact_xian, CenterTypeTag::CENTER_TAG_XIAN);

	Button * btn_Baccact_ping = dynamic_cast<Button *>(Panel_game_bet->getChildByName("btn_Baccact_ping"));
	btn_Baccact_ping->setTag(CenterTypeTag::CENTER_TAG_PING);
	btn_Baccact_ping->addTouchEventListener(this, SEL_TouchEvent(&FrameLayer_Baccarat::btnCenterClickCallBack));
	getBtnTips(btn_Baccact_ping, CenterTypeTag::CENTER_TAG_PING);

	Button * btn_Baccact_zhuang = dynamic_cast<Button*>(Panel_game_bet->getChildByName("btn_Baccact_zhuang"));
	btn_Baccact_zhuang->setTag(CenterTypeTag::CENTER_TAG_ZHUANG);
	btn_Baccact_zhuang->addTouchEventListener(this, SEL_TouchEvent(&FrameLayer_Baccarat::btnCenterClickCallBack));
	getBtnTips(btn_Baccact_zhuang, CenterTypeTag::CENTER_TAG_ZHUANG);

	Button * btn_Baccact_xianduizi = dynamic_cast<Button*>(Panel_game_bet->getChildByName("btn_Baccact_xianduizi"));
	btn_Baccact_xianduizi->setTag(CenterTypeTag::CENTER_TAG_XIANDUIZI);
	btn_Baccact_xianduizi->addTouchEventListener(this, SEL_TouchEvent(&FrameLayer_Baccarat::btnCenterClickCallBack));
	getBtnTips(btn_Baccact_xianduizi, CenterTypeTag::CENTER_TAG_XIANDUIZI);

	Button * btn_Baccact_zhuangduizi = dynamic_cast<Button*>(Panel_game_bet->getChildByName("btn_Baccact_zhuangduizi"));
	btn_Baccact_zhuangduizi->setTag(CenterTypeTag::CENTER_TAG_ZHUANGDUIZI);
	btn_Baccact_zhuangduizi->addTouchEventListener(this, SEL_TouchEvent(&FrameLayer_Baccarat::btnCenterClickCallBack));
	getBtnTips(btn_Baccact_zhuangduizi, CenterTypeTag::CENTER_TAG_ZHUANGDUIZI);

	Button * btn_Baccact_xiantianwang = dynamic_cast<Button*>(Panel_game_bet->getChildByName("btn_Baccact_xiantianwang"));
	btn_Baccact_xiantianwang->setTag(CenterTypeTag::CENTER_TAG_XIANTIANWANG);
	btn_Baccact_xiantianwang->addTouchEventListener(this, SEL_TouchEvent(&FrameLayer_Baccarat::btnCenterClickCallBack));
	getBtnTips(btn_Baccact_xiantianwang, CenterTypeTag::CENTER_TAG_XIANTIANWANG);

	Button * btn_Baccact_tongdianping = dynamic_cast<Button*>(Panel_game_bet->getChildByName("btn_Baccact_tongdianping"));
	btn_Baccact_tongdianping->setTag(CenterTypeTag::CENTER_TAG_TONGDIANPING);
	btn_Baccact_tongdianping->addTouchEventListener(this, SEL_TouchEvent(&FrameLayer_Baccarat::btnCenterClickCallBack));
	getBtnTips(btn_Baccact_tongdianping, CenterTypeTag::CENTER_TAG_TONGDIANPING);


	Button * btn_Baccact_zhuangtianwang = dynamic_cast<Button*>(Panel_game_bet->getChildByName("btn_Baccact_zhuangtianwang"));
	btn_Baccact_zhuangtianwang->setTag(CenterTypeTag::CENTER_TAG_ZHUANGTIANWANG);
	btn_Baccact_zhuangtianwang->addTouchEventListener(this, SEL_TouchEvent(&FrameLayer_Baccarat::btnCenterClickCallBack));
	getBtnTips(btn_Baccact_zhuangtianwang, CenterTypeTag::CENTER_TAG_ZHUANGTIANWANG);

	//1.3 押注钱数：
	al_xian_total = dynamic_cast<TextAtlas *>(Panel_game_bet->getChildByName("al_xian_total"));
	al_xian_total->setZOrder(2);

	al_xian_my = dynamic_cast<TextAtlas *>(Panel_game_bet->getChildByName("al_xian_my"));
	al_xian_my->setZOrder(2);

	al_ping_total = dynamic_cast<TextAtlas *>(Panel_game_bet->getChildByName("al_ping_total"));
	al_ping_total->setZOrder(2);

	al_ping_my = dynamic_cast<TextAtlas *>(Panel_game_bet->getChildByName("al_ping_my"));
	al_ping_my->setZOrder(2);

	al_zhuang_total = dynamic_cast<TextAtlas *>(Panel_game_bet->getChildByName("al_zhuang_total"));
	al_zhuang_total->setZOrder(2);

	al_zhuang_my = dynamic_cast<TextAtlas *>(Panel_game_bet->getChildByName("al_zhuang_my"));
	al_zhuang_my->setZOrder(2);

	al_xiantianwang_total = dynamic_cast<TextAtlas *>(Panel_game_bet->getChildByName("al_xiantianwang_total"));
	al_xiantianwang_total->setZOrder(2);

	al_xiantianwang_my = dynamic_cast<TextAtlas *>(Panel_game_bet->getChildByName("al_xiantianwang_my"));
	al_xiantianwang_my->setZOrder(2);

	al_zhuangtianwang_total = dynamic_cast<TextAtlas *>(Panel_game_bet->getChildByName("al_zhuangtianwang_total"));
	al_zhuangtianwang_total->setZOrder(2);

	al_zhuangtianwang_my = dynamic_cast<TextAtlas *>(Panel_game_bet->getChildByName("al_zhuangtianwang_my"));
	al_zhuangtianwang_my->setZOrder(2);

	al_tongdianping_total = dynamic_cast<TextAtlas *>(Panel_game_bet->getChildByName("al_tongdianping_total"));
	al_tongdianping_total->setZOrder(2);

	al_tongdianping_my = dynamic_cast<TextAtlas *>(Panel_game_bet->getChildByName("al_tongdianping_my"));
	al_tongdianping_my->setZOrder(2);

	al_xianduizi_total = dynamic_cast<TextAtlas *>(Panel_game_bet->getChildByName("al_xianduizi_total"));
	al_xianduizi_total->setZOrder(2);

	al_xianduizi_my = dynamic_cast<TextAtlas *>(Panel_game_bet->getChildByName("al_xianduizi_my"));
	al_xianduizi_my->setZOrder(2);

	al_zhuangduizi_total = dynamic_cast<TextAtlas *>(Panel_game_bet->getChildByName("al_zhuangduizi_total"));
	al_zhuangduizi_total->setZOrder(2);

	al_zhuangduizi_my = dynamic_cast<TextAtlas *>(Panel_game_bet->getChildByName("al_zhuangduizi_my"));
	al_zhuangduizi_my->setZOrder(2);


	//2.底部层 --------------------------------------------------
	img_down_layer = dynamic_cast<ImageView *>(Panel_Main->getChildByName("img_down_layer"));

	///筹码按钮
	Button * btn_score_1000 = dynamic_cast<Button *>(img_down_layer->getChildByName("btn_score_1000"));
	btn_score_1000->setTag(ScoreTypeTag::SCORE_VALUE_1000);
	btn_score_1000->addTouchEventListener(this, SEL_TouchEvent(&FrameLayer_Baccarat::btnAddScoreCallBack));

	Button * btn_score_1w = dynamic_cast<Button *>(img_down_layer->getChildByName("btn_score_1w"));
	btn_score_1w->setTag(ScoreTypeTag::SCORE_VALUE_1W);
	btn_score_1w->addTouchEventListener(this, SEL_TouchEvent(&FrameLayer_Baccarat::btnAddScoreCallBack));

	Button * btn_score_10w = dynamic_cast<Button *>(img_down_layer->getChildByName("btn_score_10w"));
	btn_score_10w->setTag(ScoreTypeTag::SCORE_VALUE_10W);
	btn_score_10w->addTouchEventListener(this, SEL_TouchEvent(&FrameLayer_Baccarat::btnAddScoreCallBack));

	Button * btn_score_100w = dynamic_cast<Button *>(img_down_layer->getChildByName("btn_score_100w"));
	btn_score_100w->setTag(ScoreTypeTag::SCORE_VALUE_100W);
	btn_score_100w->addTouchEventListener(this, SEL_TouchEvent(&FrameLayer_Baccarat::btnAddScoreCallBack));

	Button * btn_score_500w = dynamic_cast<Button *>(img_down_layer->getChildByName("btn_score_500w"));
	btn_score_500w->setTag(ScoreTypeTag::SCORE_VALUE_500W);
	btn_score_500w->addTouchEventListener(this, SEL_TouchEvent(&FrameLayer_Baccarat::btnAddScoreCallBack));

	Button * btn_score_1000w = dynamic_cast<Button *>(img_down_layer->getChildByName("btn_score_1000w"));
	btn_score_1000w->setTag(ScoreTypeTag::SCORE_VALUE_1000W);
	btn_score_1000w->addTouchEventListener(this, SEL_TouchEvent(&FrameLayer_Baccarat::btnAddScoreCallBack));

	btn_score_1000->setTouchEnabled(false);
	btn_score_1000->setBright(false);
	btn_score_1w->setTouchEnabled(false);
	btn_score_1w->setBright(false);
	btn_score_10w->setTouchEnabled(false);
	btn_score_10w->setBright(false);
	btn_score_100w->setTouchEnabled(false);
	btn_score_100w->setBright(false);
	btn_score_500w->setTouchEnabled(false);
	btn_score_500w->setBright(false);
	btn_score_1000w->setTouchEnabled(false);
	btn_score_1000w->setBright(false);

	img_qipao = dynamic_cast<ImageView *>(img_down_layer->getChildByName("img_qipao"));
	img_qipao->setVisible(false);

	//3.顶部层 --------------------------------------------------
	img_top_layer = dynamic_cast<ImageView *>(Panel_Main->getChildByName("img_top_layer"));

	//申请上庄
	Button * btn_asdforbanker = dynamic_cast<Button *>(img_top_layer->getChildByName("btn_asdforbanker"));
	btn_asdforbanker->addTouchEventListener(this, SEL_TouchEvent(&FrameLayer_Baccarat::btnAskForBankerCallBack));

	//查看路单 
	Button * btn_trend = dynamic_cast<Button *>(img_top_layer->getChildByName("btn_trend"));
	btn_trend->addTouchEventListener(this, SEL_TouchEvent(&FrameLayer_Baccarat::btnQueryTrendCallBack));

	//4. 人物层 --------------------------------------------------
	PlayerNode_player = dynamic_cast<Widget *>(Panel_Main->getChildByName("PlayerNode_player"));
	PlayerNode_zhuang = dynamic_cast<Widget *>(Panel_Main->getChildByName("PlayerNode_zhuang"));

}


void FrameLayer_Baccarat::setCloseDialogInfo(cocos2d::Ref* target, cocos2d::SEL_CallFuncN callfun, const std::string& title, const std::string& content)
{
	mTarget = target;
	mCallback = callfun;
	mTitle = title;
	mContent = content;
}

void FrameLayer_Baccarat::onBtnClick(cocos2d::Ref* obj, cocos2d::extension::Control::EventType e)
{
	Control* ctr = (Control*)obj;


	popup(mTitle.c_str(), mContent.c_str(), DLG_MB_OK | DLG_MB_CANCEL, 0, mTarget, mCallback);

}

void FrameLayer_Baccarat::func_affirm_exit_call_back(cocos2d::Ref *pSender)
{
	Node* node = dynamic_cast<Node*>(pSender);
	// 	if (node->getTag() == DLG_MB_OK)
	// 	{
	// 		
	// 	}
	auto kernel = IClientKernel::get();
	int clock_id = kernel->GetClockID();
	if (clock_id != 50)
	{
		popup(mTitle.c_str(), mContent.c_str(), DLG_MB_OK | DLG_MB_CANCEL, 0, mTarget, mCallback);
	}
	else
	{
		if (mCallback)
		{
			(mTarget->*mCallback)(node);
		}
	}

}

void FrameLayer_Baccarat::func_set_call_back(cocos2d::Ref *pSender)
{
	Node* node = dynamic_cast<Node*>(pSender);
	GameSetLayer * layer = GameSetLayer::create();

	this->getParent()->addChild(layer, 0xFFFF);
}

void FrameLayer_Baccarat::func_start_call_back(cocos2d::Ref *pSender)
{
	Node* node = dynamic_cast<Node*>(pSender);

	IClientKernel* kernel = IClientKernel::get();
	kernel->SendUserReady(NULL, 0);

	ClientKernelSink_Baccarat::getInstance().OnStartremove(50);///删除开始计时器

	showMenuItem(MenuItemTag::ITEM_TAG_START, false);

	kernel->SetGameClock(kernel->GetMeChairID(), 50, 90);

	GameScene_Baccarat * pGame_scene = dynamic_cast<GameScene_Baccarat *>(this->getParent());

	pGame_scene->clearCardData(kernel->GetMeChairID());
	pGame_scene->showSettlementFrame(kernel->GetMeChairID(), false);
}

void FrameLayer_Baccarat::func_require_banker_call_back(cocos2d::Ref *pSender)
{
	Node* node = dynamic_cast<Node*>(pSender);


	showMenuItem(ITEM_TAG_REQUIRE_BANKSER, false);
	showMenuItem(ITEM_TAG_ABORT, false);
}

void FrameLayer_Baccarat::func_tanpai_call_back(cocos2d::Ref *pSender)
{
	showMenuItem(ITEM_TAG_SHOW_HARD, false);
	Node* node = dynamic_cast<Node*>(pSender);
	ClientKernelSink_Baccarat::getInstance().OnMeGive(0);
}

void FrameLayer_Baccarat::func_abort_call_back(cocos2d::Ref *pSender)
{
	Node* node = dynamic_cast<Node*>(pSender);

	ClientKernelSink_Baccarat::getInstance().ButtonWithGrabBanker();
	showMenuItem(ITEM_TAG_REQUIRE_BANKSER, false);
	showMenuItem(ITEM_TAG_ABORT, false);

}

void FrameLayer_Baccarat::func_bottom_pour_back(cocos2d::Ref *pSender)
{

}

void FrameLayer_Baccarat::showMenuItem(MenuItemTag item_tag, bool isShow)
{
	// 	switch (item_tag)
	// 	{
	// 	case FrameLayer_Baccarat::ITEM_TAG_START:
	// 		menuItem_2->setVisible(isShow);
	// 		break;
	// 	case FrameLayer_Baccarat::ITEM_TAG_REQUIRE_BANKSER:
	// 		menuItem_3->setVisible(isShow);
	// 		break;
	// 	case FrameLayer_Baccarat::ITEM_TAG_SHOW_HARD:
	// 		menuItem_4->setVisible(isShow);
	// 		break;
	// 	case FrameLayer_Baccarat::ITEM_TAG_ABORT:
	// 		menuItem_5->setVisible(isShow);
	// 		break;
	// 	default:
	// 		break;
	// 	}
}

void FrameLayer_Baccarat::sche_time_out(float dt)
{
	total_time = total_time - (int)dt;
	// 	if (total_time < 3)
	// 	{
	// 		SoundManager::shared()->playSound("GAME_WARN");
	// 	}
	if (total_time >= 0)
	{
		al_time_nums->setString(StringUtils::format("%d", total_time));
		return;
	}
	else
	{
		this->unschedule(SEL_SCHEDULE(&FrameLayer_Baccarat::sche_time_out));
		//func_affirm_exit_call_back(menuItem_0);
		auto kernel = IClientKernel::get();
		if (!kernel) return;
		int clock_id = kernel->GetClockID();
		if (clock_id == 50)
		{
			//func_affirm_exit_call_back(menuItem_0);
		}
	}
}

void FrameLayer_Baccarat::showTimeOut(int time)
{
	if (time <= 0) return;
	total_time = time;

	bool isHave = this->isScheduled(SEL_SCHEDULE(&FrameLayer_Baccarat::sche_time_out));
	if (total_time > 0 && !isHave)
	{
		this->schedule(SEL_SCHEDULE(&FrameLayer_Baccarat::sche_time_out), 1);
	}
}

void FrameLayer_Baccarat::setAddScoreMaxValue(long long score)
{
	if (score < 0) return;
	max_score = score;

	// 	btn_score_bg->setVisible(true);
	// 
	// 	std::string str = StringUtils::format("%lld", score / 1);
	// 	btn_score_bg_1->setTitleText(str);
	// 	str = StringUtils::format("%lld", score / 2);
	// 	btn_score_bg_2->setTitleText(str);
	// 	str = StringUtils::format("%lld", score / 4);
	// 	btn_score_bg_3->setTitleText(str);
	// 	str = StringUtils::format("%lld", score / 8);
	// 	btn_score_bg_4->setTitleText(str);
}

void FrameLayer_Baccarat::btnAddScoreCallBack(cocos2d::Ref * ref, ui::TouchEventType eType)
{
	if (eType == ui::TouchEventType::TOUCH_EVENT_BEGAN)
	{
		PLAY_BUTTON_CLICK_EFFECT;
	}
	if (eType == ui::TouchEventType::TOUCH_EVENT_ENDED)
	{
		ui::Button * btn_now = dynamic_cast<ui::Button *>(ref);
		int tag = btn_now->getTag();
		img_qipao->setPosition(btn_now->getPosition() + ccp(0, 5.35));
		img_qipao->setVisible(true);

		mNowClickJetton = tag;
		checkUpdateCenterPanelStatus();
	}
}
//申请上庄
void FrameLayer_Baccarat::btnAskForBankerCallBack(cocos2d::Ref * ref, ui::TouchEventType eType)
{
	if (eType == ui::TouchEventType::TOUCH_EVENT_BEGAN)
	{
		PLAY_BUTTON_CLICK_EFFECT;
	}
	if (eType == ui::TouchEventType::TOUCH_EVENT_ENDED)
	{
		mBankerPanel->setVisible(true);
	}
}
//查看路单 
void FrameLayer_Baccarat::btnQueryTrendCallBack(cocos2d::Ref * ref, ui::TouchEventType eType)
{
	if (eType == ui::TouchEventType::TOUCH_EVENT_BEGAN)
	{
		PLAY_BUTTON_CLICK_EFFECT;
	}
	if (eType == ui::TouchEventType::TOUCH_EVENT_ENDED)
	{
		mTrendPanel->setVisible(true);
	}
}

void FrameLayer_Baccarat::btnCenterClickCallBack(cocos2d::Ref * ref, ui::TouchEventType eType)
{
	ui::Button * btn_now = dynamic_cast<ui::Button *>(ref);

	if (eType == ui::TouchEventType::TOUCH_EVENT_BEGAN)
	{

	}
	if (eType == ui::TouchEventType::TOUCH_EVENT_ENDED)
	{
		int tag = btn_now->getTag();
		if (mNowClickJetton == 0) return;
		byte onArea = 1;
		switch (tag)
		{
		case FrameLayer_Baccarat::CENTER_TAG_XIAN:
			onArea = AREA_XIAN;
			break;
		case FrameLayer_Baccarat::CENTER_TAG_PING:
			onArea = AREA_PING;
			break;
		case FrameLayer_Baccarat::CENTER_TAG_ZHUANG:
			onArea = AREA_ZHUANG;
			break;
		case FrameLayer_Baccarat::CENTER_TAG_XIANTIANWANG:
			onArea = AREA_XIAN_TIAN;
			break;
		case FrameLayer_Baccarat::CENTER_TAG_ZHUANGTIANWANG:
			onArea = AREA_ZHUANG_TIAN;
			break;
		case FrameLayer_Baccarat::CENTER_TAG_TONGDIANPING:
			onArea = AREA_TONG_DUI;
			break;
		case FrameLayer_Baccarat::CENTER_TAG_XIANDUIZI:
			onArea = AREA_XIAN_DUI;
			break;
		case FrameLayer_Baccarat::CENTER_TAG_ZHUANGDUIZI:
			onArea = AREA_ZHUANG_DUI;
			break;
		default:
			break;
		}

		int64 mJetton = getScoreByScoreTypeTag((ScoreTypeTag)mNowClickJetton);
		mJetton = checkJettonLessThanArea(onArea, mJetton);

		if (mJetton == 0) return;

		ClientKernelSink_Baccarat::getInstance().OnMeAddGold(onArea, mJetton);
	}
}

void FrameLayer_Baccarat::btnCloseGameCallBack(cocos2d::Ref * ref, ui::TouchEventType eType)
{
	if (eType == ui::TouchEventType::TOUCH_EVENT_BEGAN)
	{
		PLAY_BUTTON_CLICK_EFFECT;
	}
	if (eType == ui::TouchEventType::TOUCH_EVENT_ENDED)
	{
		ui::Button * btn_now = dynamic_cast<ui::Button *>(ref);
		int tag = btn_now->getTag();
		btn_now->setTag(DLG_MB_OK);
		IClientKernel * kernel = IClientKernel::get();

		if (kernel->GetGameStatus() == GAME_SCENE_FREE)
		{
			(mTarget->*mCallback)(btn_now);
		}
		else
		{
			NewDialog::create(SSTRING("back_to_room_content"), NewDialog::AFFIRMANDCANCEL, [=]()
			{
				(mTarget->*mCallback)(btn_now);
			});
		}

	}
}


void FrameLayer_Baccarat::createBankerPanel()
{

	Widget * mRoot = cocostudio::GUIReader::getInstance()->widgetFromJsonFile("baccaratGameScene/Baccarat_Banker.json");

	mBankerPanel = mRoot;

	lv_banker_user = dynamic_cast<ListView *>(mBankerPanel->getChildByName("lv_banker_user"));

	Button * btn_Baccarat_banker_close = dynamic_cast<Button *>(mBankerPanel->getChildByName("btn_Baccarat_banker_close"));
	btn_Baccarat_banker_close->addTouchEventListener(this, SEL_TouchEvent(&FrameLayer_Baccarat::btnBankerCloseCallBack));

	btn_Baccarat_banker_askfor = dynamic_cast<Button *>(mBankerPanel->getChildByName("btn_Baccarat_banker_askfor"));
	btn_Baccarat_banker_askfor->addTouchEventListener(this, SEL_TouchEvent(&FrameLayer_Baccarat::btnBankerAskForCallBack));

	btn_Baccarat_banker_askfor->loadTextureNormal("baccaratGameScene/Baccarat_Banker/btn_apply_banker_0.png");
	btn_Baccarat_banker_askfor->loadTexturePressed("baccaratGameScene/Baccarat_Banker/btn_apply_banker_1.png");
	btn_Baccarat_banker_askfor->loadTextureDisabled("baccaratGameScene/Baccarat_Banker/btn_apply_banker_2.png");
	btn_Baccarat_banker_askfor->setTouchEnabled(false);
	btn_Baccarat_banker_askfor->setBright(false);

	mBankerPanel->setVisible(false);

	this->addChild(mBankerPanel, 5);
}


void FrameLayer_Baccarat::btnBankerCloseCallBack(cocos2d::Ref * ref, ui::TouchEventType eType)
{
	if (eType == ui::TouchEventType::TOUCH_EVENT_BEGAN)
	{
		PLAY_BUTTON_CLICK_EFFECT;
	}
	if (eType == ui::TouchEventType::TOUCH_EVENT_ENDED)
	{
		ui::Button * btn_now = dynamic_cast<ui::Button *>(ref);
		int tag = btn_now->getTag();

		mBankerPanel->setVisible(false);

	}
}

void FrameLayer_Baccarat::btnBankerAskForCallBack(cocos2d::Ref * ref, ui::TouchEventType eType)
{
	if (eType == ui::TouchEventType::TOUCH_EVENT_BEGAN)
	{
		PLAY_BUTTON_CLICK_EFFECT;
	}
	if (eType == ui::TouchEventType::TOUCH_EVENT_ENDED)
	{
		ui::Button * btn_now = dynamic_cast<ui::Button *>(ref);
		int tag = btn_now->getTag();
		IClientKernel * kernel = IClientKernel::get();
		if (!kernel) return;
		if (ClientKernelSink_Baccarat::getInstance().m_wCurrentBanker == kernel->GetMeChairID() || ClientKernelSink_Baccarat::getInstance().m_bMeApplyBanker)
		{
			ClientKernelSink_Baccarat::getInstance().ButtonWithCallBanker(false);
		}
		else
		{
			playerScoreIsApplyBanker(kernel->GetMeUserItem()->GetUserScore());
			//判断是否可以坐庄
			if (kernel->GetMeUserItem()->GetUserScore() >= lApplyBankerCondition);
				ClientKernelSink_Baccarat::getInstance().ButtonWithCallBanker(true);
		}
	}
}

void FrameLayer_Baccarat::btnBankerGrabCallBack(cocos2d::Ref * ref, ui::TouchEventType eType)
{
	if (eType == ui::TouchEventType::TOUCH_EVENT_BEGAN)
	{
		PLAY_BUTTON_CLICK_EFFECT;
	}
	if (eType == ui::TouchEventType::TOUCH_EVENT_ENDED)
	{
		ui::Button * btn_now = dynamic_cast<ui::Button *>(ref);
		int tag = btn_now->getTag();

		IClientKernel * kernel = IClientKernel::get();
		///< 排除自己的情况
		if (!kernel) return;
		if (ClientKernelSink_Baccarat::getInstance().m_wCurrentBanker == kernel->GetMeChairID())
		{
			return;
		}
		std::string str_info = StringUtils::format(SSTRING("ox100_game_tips_1"), ClientKernelSink_Baccarat::getInstance().m_lQiangScore);
		NewDialog::create(str_info, NewDialog::AFFIRMANDCANCEL,
			[=]()
		{
			ClientKernelSink_Baccarat::getInstance().ButtonWithGrabBanker();
		});

	}
}

void FrameLayer_Baccarat::createSettleAccountsPanel()
{
	Widget * mRoot = cocostudio::GUIReader::getInstance()->widgetFromJsonFile("baccaratGameScene/Baccarat_SettleAccounts.json");
	Widget * Panel_Main = dynamic_cast<Widget *>(mRoot->getChildByName("Panel_Main"));
	mSettleAccountsPanel = mRoot;

	lab_xianduizi = dynamic_cast<Text *>(Panel_Main->getChildByName("lab_xianduizi"));
	lab_zhuangduizi = dynamic_cast<Text *>(Panel_Main->getChildByName("lab_zhuangduizi"));
	lab_xian = dynamic_cast<Text *>(Panel_Main->getChildByName("lab_xian"));
	lab_ping = dynamic_cast<Text *>(Panel_Main->getChildByName("lab_ping"));
	lab_zhuang = dynamic_cast<Text *>(Panel_Main->getChildByName("lab_zhuang"));
	lab_xiantianwang = dynamic_cast<Text *>(Panel_Main->getChildByName("lab_xiantianwang"));
	lab_tongdianping = dynamic_cast<Text *>(Panel_Main->getChildByName("lab_tongdianping"));
	lab_zhuangtianwang = dynamic_cast<Text *>(Panel_Main->getChildByName("lab_zhuangtianwang"));
	lab_total_points = dynamic_cast<Text *>(Panel_Main->getChildByName("lab_total_points"));
	img_xian_dian = dynamic_cast<ImageView *>(Panel_Main->getChildByName("img_xian_dian"));
	img_zhuang_dian = dynamic_cast<ImageView *>(Panel_Main->getChildByName("img_zhuang_dian"));

	mSettleAccountsPanel->setVisible(false);
	this->addChild(mSettleAccountsPanel, 3);
}

////////////////////////////////////////////////////////////////////////////////////////
void FrameLayer_Baccarat::createTrendPanel()
{
	Widget * mRoot = cocostudio::GUIReader::getInstance()->widgetFromJsonFile("baccaratGameScene/Baccarat_Trend.json");
	mTrendPanel = mRoot;

	mTrendPanel->setVisible(false);
	this->addChild(mTrendPanel, 3);

	img_bg = dynamic_cast<ImageView *>(mTrendPanel->getChildByName("img_bg"));
	lv_zhulu = dynamic_cast<ListView *>(img_bg->getChildByName("lv_zhulu"));
	lv_dalu = dynamic_cast<ListView *>(img_bg->getChildByName("lv_dalu"));
	lv_zhu_shu = dynamic_cast<ListView *>(img_bg->getChildByName("lv_zhu_shu"));
	panel_zhu_cell = dynamic_cast<Layout *>(img_bg->getChildByName("panel_zhu_cell"));
	lv_da_shu = dynamic_cast<ListView *>(img_bg->getChildByName("lv_da_shu"));
	panel_da_cell = dynamic_cast<Layout *>(img_bg->getChildByName("panel_da_cell"));
	txt_xianduizi = dynamic_cast<Text *>(img_bg->getChildByName("lab_xianduizi"));
	txt_zhuangduizi = dynamic_cast<Text *>(img_bg->getChildByName("lab_zhuangduizi"));
	txt_xiantianwang = dynamic_cast<Text *>(img_bg->getChildByName("lab_xiantianwang"));
	txt_zhuangtianwang = dynamic_cast<Text *>(img_bg->getChildByName("lab_zhuangtianwang"));
	txt_xianshu = dynamic_cast<Text *>(img_bg->getChildByName("lab_xianshu"));
	txt_xianbai = dynamic_cast<Text *>(img_bg->getChildByName("lab_xianbai"));
	txt_pingshu = dynamic_cast<Text *>(img_bg->getChildByName("lab_pingshu"));
	txt_pingbai = dynamic_cast<Text *>(img_bg->getChildByName("lab_pingbai"));
	txt_zhuangshu = dynamic_cast<Text *>(img_bg->getChildByName("lab_zhuangshu"));
	txt_zhuangbai = dynamic_cast<Text *>(img_bg->getChildByName("lab_zhuangbai"));

	lv_zhulu->removeAllChildren();
	lv_dalu->removeAllChildren();
	lv_zhu_shu->removeAllChildren();
	lv_da_shu->removeAllChildren();

	//触摸响应注册_ 
	auto touchListener_0 = EventListenerTouchOneByOne::create();//创建单点触摸事件监听器_
	touchListener_0->setSwallowTouches(true);
	touchListener_0->onTouchBegan = CC_CALLBACK_2(FrameLayer_Baccarat::onTouchBegan, this);//触摸开始_
	_eventDispatcher->addEventListenerWithSceneGraphPriority(touchListener_0, mTrendPanel);//注册分发器_
}

void FrameLayer_Baccarat::onAreaAddJetton(word wChairID, byte cbJettonArea, int64 lJettonScore)
{
	IClientKernel * kernel = IClientKernel::get();
	bool isMe = kernel->GetMeChairID() == wChairID;

	Sprite * spriteJetton = nullptr;
	if (lJettonScore == 1000)
	{
		spriteJetton = Sprite::create("baccaratGameScene/Baccarat_Main/room_chip_2_0.png");
	}
	else if (lJettonScore == 10000)
	{
		spriteJetton = Sprite::create("baccaratGameScene/Baccarat_Main/room_chip_3_0.png");
	}
	else if (lJettonScore == 100000)
	{
		spriteJetton = Sprite::create("baccaratGameScene/Baccarat_Main/room_chip_4_0.png");
	}
	else if (lJettonScore == 1000000)
	{
		spriteJetton = Sprite::create("baccaratGameScene/Baccarat_Main/room_chip_5_0.png");
	}
	else if (lJettonScore == 5000000)
	{
		spriteJetton = Sprite::create("baccaratGameScene/Baccarat_Main/room_chip_6_0.png");
	}
	else if (lJettonScore == 10000000)
	{
		spriteJetton = Sprite::create("baccaratGameScene/Baccarat_Main/room_chip_7_0.png");
	}
	int64 areaTotalMoney = ClientKernelSink_Baccarat::getInstance().m_lAllJettonScore[cbJettonArea];;
	int64 areaMyMoney = ClientKernelSink_Baccarat::getInstance().m_lUserJettonScore[cbJettonArea];;

	std::string strTotalMoney = StringUtils::format("%lld", areaTotalMoney);
	std::string strMyMoney = StringUtils::format("%lld", areaMyMoney);

	if (spriteJetton)
	{
		Vec2 startPoint;
		if (rand() % 2 == 0)
			startPoint.x = -100;
		else
			startPoint.x = 1520;
		startPoint.y = 100 + rand() % 600;
		spriteJetton->setPosition(startPoint);

		if (cbJettonArea == AREA_XIAN)
		{
			al_xian_total->setVisible(true);
			if (isMe)
				al_xian_my->setVisible(true);
			al_xian_total->setString(strTotalMoney);
			al_xian_my->setString(strMyMoney);
			Node * node = Panel_game_bet->getChildByName("btn_Baccact_xian");
			Vec2 now_pos = node->getPosition();
			Vec2 new_pos = SimpleTools::getConfineToRandomVec2(now_pos, 50);
			spriteJetton->runAction(CCMoveTo::create(0.3f, new_pos));
		}
		else if (cbJettonArea == AREA_PING)
		{
			al_ping_total->setVisible(true);
			if (isMe)
				al_ping_my->setVisible(true);
			al_ping_total->setString(strTotalMoney);
			al_ping_my->setString(strMyMoney);
			Node * node = Panel_game_bet->getChildByName("btn_Baccact_ping");
			Vec2 now_pos = node->getPosition();
			Vec2 new_pos = SimpleTools::getConfineToRandomVec2(now_pos, 50);
			spriteJetton->runAction(CCMoveTo::create(0.3f, new_pos));
		}
		else if (cbJettonArea == AREA_ZHUANG)
		{
			al_zhuang_total->setVisible(true);
			if (isMe)
				al_zhuang_my->setVisible(true);
			al_zhuang_total->setString(strTotalMoney);
			al_zhuang_my->setString(strMyMoney);
			Node * node = Panel_game_bet->getChildByName("btn_Baccact_zhuang");
			Vec2 now_pos = node->getPosition();
			Vec2 new_pos = SimpleTools::getConfineToRandomVec2(now_pos, 50);
			spriteJetton->runAction(CCMoveTo::create(0.3f, new_pos));
		}
		else if (cbJettonArea == AREA_XIAN_TIAN)
		{
			al_xiantianwang_total->setVisible(true);
			if (isMe)
				al_xiantianwang_my->setVisible(true);
			al_xiantianwang_total->setString(strTotalMoney);
			al_xiantianwang_my->setString(strMyMoney);
			Node * node = Panel_game_bet->getChildByName("btn_Baccact_xiantianwang");
			Vec2 now_pos = node->getPosition();
			Vec2 new_pos = SimpleTools::getConfineToRandomVec2(now_pos, 25);
			spriteJetton->runAction(CCMoveTo::create(0.3f, new_pos));
		}
		if (cbJettonArea == AREA_ZHUANG_TIAN)
		{
			al_zhuangtianwang_total->setVisible(true);
			if (isMe)
				al_zhuangtianwang_my->setVisible(true);
			al_zhuangtianwang_total->setString(strTotalMoney);
			al_zhuangtianwang_my->setString(strMyMoney);
			Node * node = Panel_game_bet->getChildByName("btn_Baccact_zhuangtianwang");
			Vec2 now_pos = node->getPosition();
			Vec2 new_pos = SimpleTools::getConfineToRandomVec2(now_pos, 25);
			spriteJetton->runAction(CCMoveTo::create(0.3f, new_pos));
		}
		else if (cbJettonArea == AREA_TONG_DUI)
		{
			al_tongdianping_total->setVisible(true);
			if (isMe)
				al_tongdianping_my->setVisible(true);
			al_tongdianping_total->setString(strTotalMoney);
			al_tongdianping_my->setString(strMyMoney);
			Node * node = Panel_game_bet->getChildByName("btn_Baccact_tongdianping");
			Vec2 now_pos = node->getPosition();
			Vec2 new_pos = SimpleTools::getConfineToRandomVec2(now_pos, 25);
			spriteJetton->runAction(CCMoveTo::create(0.3f, new_pos));
		}
		else if (cbJettonArea == AREA_XIAN_DUI)
		{
			al_xianduizi_total->setVisible(true);
			if (isMe)
				al_xianduizi_my->setVisible(true);
			al_xianduizi_total->setString(strTotalMoney);
			al_xianduizi_my->setString(strMyMoney);
			Node * node = Panel_game_bet->getChildByName("btn_Baccact_xianduizi");
			Vec2 now_pos = node->getPosition();
			Vec2 new_pos = SimpleTools::getConfineToRandomVec2(now_pos, 25);
			spriteJetton->runAction(CCMoveTo::create(0.3f, new_pos));
		}
		else if (cbJettonArea == AREA_ZHUANG_DUI)
		{
			al_zhuangduizi_total->setVisible(true);
			if (isMe)
				al_zhuangduizi_my->setVisible(true);
			al_zhuangduizi_total->setString(strTotalMoney);
			al_zhuangduizi_my->setString(strMyMoney);
			Node * node = Panel_game_bet->getChildByName("btn_Baccact_zhuangduizi");
			Vec2 now_pos = node->getPosition();
			Vec2 new_pos = SimpleTools::getConfineToRandomVec2(now_pos, 25);
			spriteJetton->runAction(CCMoveTo::create(0.3f, new_pos));
		}
	}
	else if (lJettonScore == 0)
	{
		///< 清理分数显示
		if (cbJettonArea == AREA_XIAN)
		{
			al_xian_total->setVisible(false);
			al_xian_my->setVisible(false);
		}
		else if (cbJettonArea == AREA_PING)
		{
			al_ping_total->setVisible(false);
			al_ping_my->setVisible(false);
		}
		else if (cbJettonArea == AREA_ZHUANG)
		{
			al_zhuang_total->setVisible(false);
			al_zhuang_my->setVisible(false);
		}
		else if (cbJettonArea == AREA_XIAN_TIAN)
		{
			al_xiantianwang_total->setVisible(false);
			al_xiantianwang_my->setVisible(false);
		}
		if (cbJettonArea == AREA_ZHUANG_TIAN)
		{
			al_zhuangtianwang_total->setVisible(false);
			al_zhuangtianwang_my->setVisible(false);

		}
		else if (cbJettonArea == AREA_TONG_DUI)
		{
			al_tongdianping_total->setVisible(false);
			al_tongdianping_my->setVisible(false);
		}
		else if (cbJettonArea == AREA_XIAN_DUI)
		{
			al_xianduizi_total->setVisible(false);
			al_xianduizi_my->setVisible(false);
		}
		else if (cbJettonArea == AREA_ZHUANG_DUI)
		{
			al_zhuangduizi_total->setVisible(false);
			al_zhuangduizi_my->setVisible(false);
		}
	}

	if (spriteJetton)
	{
		spriteJetton->setScale(0.35f);
		Panel_game_bet->addChild(spriteJetton, 1);
		mJettonList.push_back(spriteJetton);

		if (isMe)
			mMyTotalScore += lJettonScore;
		mTotalScore += lJettonScore;
	}
}

void FrameLayer_Baccarat::clearTipsStatus()
{
	for (int i = 0; i < 8; i++)
	{
		ImageView * img = mTipsList[i];
		img->setVisible(false);
	}
}

void FrameLayer_Baccarat::clearButtonOnPaoPao()
{
	img_qipao->setVisible(false);
}

void FrameLayer_Baccarat::clearJetton()
{
	for (int i = 0; !mJettonList.empty() && i < mJettonList.size(); i++)
	{
		Sprite * now_sprite = mJettonList[i];
		now_sprite->removeFromParent();
	}

	mJettonList.clear();
	mNowClickJetton = 0;
	mMyTotalScore = 0;
	mTotalScore = 0;

}

void FrameLayer_Baccarat::addTrendHistory(byte cbKingWinner, bool bPlayerTwoPair, bool bBankerTwoPair, byte cbPlayerCount, byte cbBankerCount)
{
	//复制珠路单元
	Layout * now_panel = dynamic_cast<Layout *>(panel_zhu_cell->clone());
	ImageView *  img_cell = dynamic_cast<ImageView *>(now_panel->getChildByName("img_cell"));
	ImageView *  img_zhuangdui = dynamic_cast<ImageView *>(now_panel->getChildByName("img_zhuangdui"));
	ImageView *  img_xiandui = dynamic_cast<ImageView *>(now_panel->getChildByName("img_xiandui"));
	//复制大路单元
	Layout * now_da_cell = dynamic_cast<Layout *>(panel_da_cell->clone());
	ImageView *  now_img_cell = dynamic_cast<ImageView *>(now_da_cell->getChildByName("img_cell"));

	//1.庄先平数目
	num_zong++;
	if (cbPlayerCount > cbBankerCount)
	{
		num_xian++;
		img_cell->loadTexture("baccaratGameScene/Baccarat_Trend/game_ludan2_xian.png");
		now_img_cell->loadTexture("baccaratGameScene/Baccarat_Trend/game_ludan1_xian.png");
		type_now = 1;
	} 
	else if (cbPlayerCount < cbBankerCount)
	{
		num_zhuang++;
		img_cell->loadTexture("baccaratGameScene/Baccarat_Trend/game_ludan2_zhuang.png");
		now_img_cell->loadTexture("baccaratGameScene/Baccarat_Trend/game_ludan1_zhuang.png");
		type_now = 2;
	}
	else
	{
		num_ping++;
		img_cell->loadTexture("baccaratGameScene/Baccarat_Trend/game_ludan2_ping.png");
		now_img_cell->loadTexture("baccaratGameScene/Baccarat_Trend/game_ludan1_ping.png");
		type_now = 0;
	}
	//天王数目
	if (cbKingWinner)
	{
		if (cbPlayerCount > 7)
		{
			num_xiantianwang++;
		}
		if (cbBankerCount > 7)
		{
			num_zhuangtianwang++;
		}
	}
	//对子数目
	if (bPlayerTwoPair)
	{
		img_xiandui->setVisible(true);
		num_xianduizi++;
	}
	if (bBankerTwoPair)
	{
		img_zhuangdui->setVisible(true);
		num_zhuangduizi++;
	}

	bai_xian = (float)num_xian / (float)num_zong * 100;
	bai_ping = (float)num_ping / (float)num_zong * 100;
	bai_zhuang = (float)num_zhuang / (float)num_zong * 100;

	txt_xianshu->setString(StringUtils::format("%d", num_xian));
	txt_pingshu->setString(StringUtils::format("%d", num_ping));
	txt_zhuangshu->setString(StringUtils::format("%d", num_zhuang));
	txt_xianduizi->setString(StringUtils::format("%d", num_xianduizi));
	txt_zhuangduizi->setString(StringUtils::format("%d", num_zhuangduizi));
	txt_xiantianwang->setString(StringUtils::format("%d", num_xiantianwang));
	txt_zhuangtianwang->setString(StringUtils::format("%d", num_zhuangtianwang));
	txt_xianbai->setString(StringUtils::format("%0.2f", bai_xian) + "%");
	txt_pingbai->setString(StringUtils::format("%0.2f", bai_ping) + "%");
	txt_zhuangbai->setString(StringUtils::format("%0.2f", bai_zhuang) + "%");
	
	//绘制珠路图
	if (num_zong % 6 == 1)
	{
		num_shu_copy++;
		ListView * now_zhu_shu = dynamic_cast<ListView *>(lv_zhu_shu->clone());
		lv_zhulu->addChild(now_zhu_shu, 1, num_shu_copy);
	}
	lv_zhulu->getChildByTag(num_shu_copy)->addChild(now_panel);
	lv_zhulu->refreshView();
	lv_zhulu->jumpToPercentHorizontal(100.0f);

	//绘制大路图
	if (type_old == 5)
	{
		num_da_copy++;
		ListView * now_da_shu = dynamic_cast<ListView *>(lv_da_shu->clone());
		lv_dalu->addChild(now_da_shu, 1, num_da_copy);
		type_old = type_now;
	}
	else if (type_now != type_old && type_now != 0)
	{
		num_da_copy++;
		ListView * now_da_shu = dynamic_cast<ListView *>(lv_da_shu->clone());
		lv_dalu->addChild(now_da_shu, 1, num_da_copy);
		type_old = type_now;
	}

	lv_dalu->getChildByTag(num_da_copy)->addChild(now_da_cell);
	lv_dalu->refreshView();
	lv_dalu->jumpToPercentHorizontal(100.0f);
}

void FrameLayer_Baccarat::setBtnJettionTouch(bool isCan)
{
	mImg_down_layer = dynamic_cast<ImageView *>(mPanelMain->getChildByName("img_down_layer"));

	Button * btn_score_1000 = dynamic_cast<Button *>(mImg_down_layer->getChildByName("btn_score_1000"));

	Button * btn_score_1w = dynamic_cast<Button *>(mImg_down_layer->getChildByName("btn_score_1w"));

	Button * btn_score_10w = dynamic_cast<Button *>(mImg_down_layer->getChildByName("btn_score_10w"));

	Button * btn_score_100w = dynamic_cast<Button *>(mImg_down_layer->getChildByName("btn_score_100w"));

	Button * btn_score_500w = dynamic_cast<Button *>(mImg_down_layer->getChildByName("btn_score_500w"));

	Button * btn_score_1000w = dynamic_cast<Button *>(mImg_down_layer->getChildByName("btn_score_1000w"));


	btn_score_1000->setTouchEnabled(isCan);
	btn_score_1000->setBright(isCan);
	btn_score_1w->setTouchEnabled(isCan);
	btn_score_1w->setBright(isCan);
	btn_score_10w->setTouchEnabled(isCan);
	btn_score_10w->setBright(isCan);
	btn_score_100w->setTouchEnabled(isCan);
	btn_score_100w->setBright(isCan);
	btn_score_500w->setTouchEnabled(isCan);
	btn_score_500w->setBright(isCan);
	btn_score_1000w->setTouchEnabled(isCan);
	btn_score_1000w->setBright(isCan);

	Button * btn_Baccact_xianduizi = dynamic_cast<Button *>(Panel_game_bet->getChildByName("btn_Baccact_xianduizi"));

	Button * btn_Baccact_zhuangduizi = dynamic_cast<Button *>(Panel_game_bet->getChildByName("btn_Baccact_zhuangduizi"));

	Button * btn_Baccact_zhuang = dynamic_cast<Button *>(Panel_game_bet->getChildByName("btn_Baccact_zhuang"));

	Button * btn_Baccact_xian = dynamic_cast<Button *>(Panel_game_bet->getChildByName("btn_Baccact_xian"));

	Button * btn_Baccact_ping = dynamic_cast<Button *>(Panel_game_bet->getChildByName("btn_Baccact_ping"));

	Button * btn_Baccact_zhuangtianwang = dynamic_cast<Button *>(Panel_game_bet->getChildByName("btn_Baccact_zhuangtianwang"));

	Button * btn_Baccact_xiantianwang = dynamic_cast<Button *>(Panel_game_bet->getChildByName("btn_Baccact_xiantianwang"));

	Button * btn_Baccact_tongdianping = dynamic_cast<Button *>(Panel_game_bet->getChildByName("btn_Baccact_tongdianping"));

	btn_Baccact_xianduizi->setTouchEnabled(isCan);
	btn_Baccact_zhuangduizi->setTouchEnabled(isCan);
	btn_Baccact_zhuang->setTouchEnabled(isCan);
	btn_Baccact_xian->setTouchEnabled(isCan);
	btn_Baccact_ping->setTouchEnabled(isCan);
	btn_Baccact_zhuangtianwang->setTouchEnabled(isCan);
	btn_Baccact_xiantianwang->setTouchEnabled(isCan);
	btn_Baccact_tongdianping->setTouchEnabled(isCan);

	btn_Baccact_xianduizi->setBrightStyle(BrightStyle::NORMAL);
	btn_Baccact_zhuangduizi->setBrightStyle(BrightStyle::NORMAL);
	btn_Baccact_zhuang->setBrightStyle(BrightStyle::NORMAL);
	btn_Baccact_xian->setBrightStyle(BrightStyle::NORMAL);
	btn_Baccact_ping->setBrightStyle(BrightStyle::NORMAL);
	btn_Baccact_zhuangtianwang->setBrightStyle(BrightStyle::NORMAL);
	btn_Baccact_xiantianwang->setBrightStyle(BrightStyle::NORMAL);
	btn_Baccact_tongdianping->setBrightStyle(BrightStyle::NORMAL);
}

void FrameLayer_Baccarat::updateSettleAccountsPanel(int64 lPlayScore[8], int64 lPlayAllScore)
{
	//判断是否显示结账面板
	isSettle = true;

	int Index = 0;
	for (; Index < 8; Index++)
	{
		if (lPlayScore[Index] != 0) break;
	}
	if (Index == 8) isSettle = false;

	//结账面板参数初始化
	lab_xian_string = StringUtils::format("");
	lab_ping_string = StringUtils::format("");
	lab_zhuang_string = StringUtils::format("");
	lab_xianduizi_string = StringUtils::format("");
	lab_xiantianwang_string = StringUtils::format("");
	lab_zhuangduizi_string = StringUtils::format("");
	lab_zhuangtianwang_string = StringUtils::format("");
	lab_tongdianping_string = StringUtils::format("");
	lab_total_points_string = StringUtils::format("0");
	//闲家赋值
	if (lPlayScore[0] > 0)
	{
		lab_xian_string = StringUtils::format("+%lld", lPlayScore[0]);
	}
	else if (lPlayScore[0] < 0)
	{
		lab_xian_string = StringUtils::format("%lld", lPlayScore[0]);
	}
	//平家赋值
	if (lPlayScore[1] > 0)
	{
		lab_ping_string = StringUtils::format("+%lld", lPlayScore[1]);
	}
	else if (lPlayScore[1] < 0)
	{
		lab_ping_string = StringUtils::format("%lld", lPlayScore[1]);
	}
	//庄家赋值
	if (lPlayScore[2] > 0)
	{
		lab_zhuang_string = StringUtils::format("+%lld", lPlayScore[2]);
	}
	else if (lPlayScore[2] < 0)
	{
		lab_zhuang_string = StringUtils::format("%lld", lPlayScore[2]);
	}
	//闲天王赋值
	if (lPlayScore[3] > 0)
	{
		lab_xiantianwang_string = StringUtils::format("+%lld", lPlayScore[3]);
	}
	else if (lPlayScore[3] < 0)
	{
		lab_xiantianwang_string = StringUtils::format("%lld", lPlayScore[3]);
	}
	//庄天王赋值
	if (lPlayScore[4] > 0)
	{
		lab_zhuangtianwang_string = StringUtils::format("+%lld", lPlayScore[4]);
	}
	else if (lPlayScore[4] < 0)
	{
		lab_zhuangtianwang_string = StringUtils::format("%lld", lPlayScore[4]);
	}
	//同点平赋值
	if (lPlayScore[5] > 0)
	{
		lab_tongdianping_string = StringUtils::format("+%lld", lPlayScore[5]);
	}
	else if (lPlayScore[5] < 0)
	{
		lab_tongdianping_string = StringUtils::format("%lld", lPlayScore[5]);
	}
	//闲对子赋值
	if (lPlayScore[6] > 0)
	{
		lab_xianduizi_string = StringUtils::format("+%lld", lPlayScore[6]);
	}
	else if (lPlayScore[6] < 0)
	{
		lab_xianduizi_string = StringUtils::format("%lld", lPlayScore[6]);
	}
	//庄对子赋值
	if (lPlayScore[7] > 0)
	{
		lab_zhuangduizi_string = StringUtils::format("+%lld", lPlayScore[7]);
	}
	else if (lPlayScore[7] < 0)
	{
		lab_zhuangduizi_string = StringUtils::format("%lld", lPlayScore[7]);
	}
	//总得分赋值
	if (lPlayAllScore > 0)
	{
		lab_total_points_string = StringUtils::format("+%lld", lPlayAllScore);
	}
	else if (lPlayAllScore < 0)
	{
		lab_total_points_string = StringUtils::format("%lld", lPlayAllScore);
	}

	lab_xian->setString(lab_xian_string);
	lab_ping->setString(lab_ping_string);
	lab_zhuang->setString(lab_zhuang_string);
	lab_xianduizi->setString(lab_xianduizi_string);
	lab_zhuangduizi->setString(lab_zhuangduizi_string);
	lab_xiantianwang->setString(lab_xiantianwang_string);
	lab_tongdianping->setString(lab_tongdianping_string);
	lab_zhuangtianwang->setString(lab_zhuangtianwang_string);
	lab_total_points->setString(lab_total_points_string);

}

ui::Widget * FrameLayer_Baccarat::getBankerPanel()
{
	return mBankerPanel;
}

ui::Widget * FrameLayer_Baccarat::getSettleAccountsPanel()
{
	return mSettleAccountsPanel;
}

ui::Widget * FrameLayer_Baccarat::getTrendPanel()
{
	return mTrendPanel;
}

ui::Widget * FrameLayer_Baccarat::getMainPanel()
{
	return mPanelMain;
}

void FrameLayer_Baccarat::addUserToBnakerList(tagApplyUser_Baccarat & tAuser)
{
	int tag = 1000 + tAuser.wChairID;
	Node * now_panel = lv_banker_user->getChildByTag(tag);

	if (!now_panel)
	{
		Layout * childPanel = dynamic_cast<Layout *>(mBankerPanel->getChildByName("BankerChildPanel"));

		Widget * new_Panel = childPanel->clone();

		Text * lab_nickname = dynamic_cast<Text *>(new_Panel->getChildByName("lab_nickname"));

		Text * lab_user_money = dynamic_cast<Text *>(new_Panel->getChildByName("lab_user_money"));

		ImageView * img_qiang_icon = dynamic_cast<ImageView *>(new_Panel->getChildByName("img_qiang_icon"));

		lab_nickname->setString(tAuser.strUserName);

		std::string strValue = StringUtils::format("%lld", tAuser.lUserScore);
		lab_user_money->setString(strValue);

		if (tAuser.bQiang)
		{
			img_qiang_icon->setVisible(true);
		}

		new_Panel->setTag(tag);
		new_Panel->setVisible(true);
		//mCallBankerList.push_back(new_Panel);
		if (tAuser.bQiang)
		{
			lv_banker_user->insertCustomItem(new_Panel, 0);
		}
		else
		{
			lv_banker_user->pushBackCustomItem(new_Panel);
		}

	}
	else
	{
		Text * lab_nickname = dynamic_cast<Text *>(now_panel->getChildByName("lab_nickname"));

		Text * lab_user_money = dynamic_cast<Text *>(now_panel->getChildByName("lab_user_money"));

		ImageView * img_qiang_icon = dynamic_cast<ImageView *>(now_panel->getChildByName("img_qiang_icon"));

		if (lab_user_money && lab_user_money && img_qiang_icon)
		{
			lab_nickname->setString(tAuser.strUserName);

			std::string strValue = StringUtils::format("%lld", tAuser.lUserScore);
			lab_user_money->setString(strValue);

			if (tAuser.bQiang)
			{
				img_qiang_icon->setVisible(true);

				now_panel->removeFromParentAndCleanup(false);

				lv_banker_user->insertCustomItem((Widget*)now_panel, 0);
			}
		}
		else
		{
			now_panel->removeFromParent();
			addUserToBnakerList(tAuser);
		}
	}

	checkUpdateBankerPanel();

}

void FrameLayer_Baccarat::deleteUserToBankerList(tagApplyUser_Baccarat & tAuser)
{
	int tag = tAuser.wChairID + 1000;
	Node * now_panel = lv_banker_user->getChildByTag(tag);
	if (now_panel)
	{
		lv_banker_user->removeChild(now_panel);
	}
	checkUpdateBankerPanel();

}

void FrameLayer_Baccarat::checkUpdateBankerPanel()
{
	IClientKernel * kernel = IClientKernel::get();

	if (ClientKernelSink_Baccarat::getInstance().m_wCurrentBanker == kernel->GetMeChairID())
	{
		//取消坐庄
		btn_Baccarat_banker_askfor->loadTextureNormal("baccaratGameScene/Baccarat_Banker/btn_cancel_banker_0.png");
		btn_Baccarat_banker_askfor->loadTexturePressed("baccaratGameScene/Baccarat_Banker/btn_cancel_banker_1.png");
		btn_Baccarat_banker_askfor->loadTextureDisabled("baccaratGameScene/Baccarat_Banker/btn_cancel_banker_2.png");
	}
	else if (ClientKernelSink_Baccarat::getInstance().m_bMeApplyBanker)
	{
		//取消申请
		btn_Baccarat_banker_askfor->loadTextureNormal("baccaratGameScene/Baccarat_Banker/btn_cancel_apply_0.png");
		btn_Baccarat_banker_askfor->loadTexturePressed("baccaratGameScene/Baccarat_Banker/btn_cancel_apply_1.png");
		btn_Baccarat_banker_askfor->loadTextureDisabled("baccaratGameScene/Baccarat_Banker/btn_cancel_apply_2.png");
	}
	else
	{
		//申请上庄
		btn_Baccarat_banker_askfor->loadTextureNormal("baccaratGameScene/Baccarat_Banker/btn_apply_banker_0.png");
		btn_Baccarat_banker_askfor->loadTexturePressed("baccaratGameScene/Baccarat_Banker/btn_apply_banker_1.png");
		btn_Baccarat_banker_askfor->loadTextureDisabled("baccaratGameScene/Baccarat_Banker/btn_apply_banker_2.png");
	}
}

///< 设置上庄条件
void FrameLayer_Baccarat::setScoreApplyBanker(int64 lApplyBankerScore)
{
	lApplyBankerCondition = lApplyBankerScore;
}

///< 玩家分数是否符合申请条件
void FrameLayer_Baccarat::playerScoreIsApplyBanker(int64 PlayerScore)
{
	if (PlayerScore >= lApplyBankerCondition)
	{
		btn_Baccarat_banker_askfor->setTouchEnabled(true);
		btn_Baccarat_banker_askfor->setBright(true);
	}
	else
	{
		btn_Baccarat_banker_askfor->setTouchEnabled(false);
		btn_Baccarat_banker_askfor->setBright(false);
	}
}

void FrameLayer_Baccarat::checkUserMoneyEnableJettonBtn(int maxAllowableValue)
{
	Button * btn_score_1000 = dynamic_cast<Button *>(mImg_down_layer->getChildByName("btn_score_1000"));
	Button * btn_score_1w = dynamic_cast<Button *>(mImg_down_layer->getChildByName("btn_score_1w"));
	Button * btn_score_10w = dynamic_cast<Button *>(mImg_down_layer->getChildByName("btn_score_10w"));
	Button * btn_score_100w = dynamic_cast<Button *>(mImg_down_layer->getChildByName("btn_score_100w"));
	Button * btn_score_500w = dynamic_cast<Button *>(mImg_down_layer->getChildByName("btn_score_500w"));
	Button * btn_score_1000w = dynamic_cast<Button *>(mImg_down_layer->getChildByName("btn_score_1000w"));

	btn_score_1000->setTouchEnabled(false);
	btn_score_1000->setBright(false);
	btn_score_1w->setTouchEnabled(false);
	btn_score_1w->setBright(false);
	btn_score_10w->setTouchEnabled(false);
	btn_score_10w->setBright(false);
	btn_score_100w->setTouchEnabled(false);
	btn_score_100w->setBright(false);
	btn_score_500w->setTouchEnabled(false);
	btn_score_500w->setBright(false);
	btn_score_1000w->setTouchEnabled(false);
	btn_score_1000w->setBright(false);

	switch (maxAllowableValue)
	{
	case 6:
	{
			  btn_score_1000w->setTouchEnabled(true);
			  btn_score_1000w->setBright(true);

	}
	case 5:
	{
			  btn_score_500w->setTouchEnabled(true);
			  btn_score_500w->setBright(true);
	}
	case 4:
	{
			  btn_score_100w->setTouchEnabled(true);
			  btn_score_100w->setBright(true);
	}
	case 3:
	{
			  btn_score_10w->setTouchEnabled(true);
			  btn_score_10w->setBright(true);
	}
	case 2:
	{
			  btn_score_1w->setTouchEnabled(true);
			  btn_score_1w->setBright(true);
	}
	case 1:
	{
			  btn_score_1000->setTouchEnabled(true);
			  btn_score_1000->setBright(true);
	}
	case 0:
		break;
	default:
		break;
	}

	if (mNowClickJetton != 0 && mNowClickJetton > maxAllowableValue + 99)
	{
		mNowClickJetton = maxAllowableValue + 99;

		///< 气泡需要重新移位
		Node * node = mImg_down_layer->getChildByTag(mNowClickJetton);
		if (node)
		{
			img_qipao->setPosition(node->getPosition() + ccp(0, 5.35));
		}
		else
		{
			img_qipao->setVisible(false);
		}
	}
}

///< 胜利区域闪烁
void FrameLayer_Baccarat::flickerWiner(int tag)
{
	FadeOut * fadeout = FadeOut::create(0.2f);
	FadeIn * fadein = FadeIn::create(0.5f);
	DelayTime * delay = DelayTime::create(0.5f);
	Sequence * seq = Sequence::create(fadeout, fadein, delay, NULL);

	RepeatForever * forever = RepeatForever::create(seq);

	Panel_game_bet->getChildByTag(1000 + tag)->setVisible(true);
	Panel_game_bet->getChildByTag(1000 + tag)->runAction(forever);
}
///< 清理胜利区域
void FrameLayer_Baccarat::clearWineer()
{
	for (int i = 0; i < 8; i++)
	{
		Panel_game_bet->getChildByTag(1000 + i)->setVisible(false);
		Panel_game_bet->getChildByTag(1000 + i)->stopAllActions();
	}
}

int64 FrameLayer_Baccarat::checkJettonLessThanArea(byte pArea, int64 pJettonScore)
{
	int64 nowAreaScore = ClientKernelSink_Baccarat::getInstance().m_lAllJettonScore[pArea];
	if (nowAreaScore + pJettonScore < ClientKernelSink_Baccarat::getInstance().m_lAreaLimitScore)
	{
		///< 小于区域分数, 直接过
		return pJettonScore;
	}
	else
	{
		///< 大于区域分数限制
		mNowClickJetton--;
		while (mNowClickJetton >= 100)
		{
			nowAreaScore = getScoreByScoreTypeTag((ScoreTypeTag)mNowClickJetton);
			if (nowAreaScore + pJettonScore < ClientKernelSink_Baccarat::getInstance().m_lAreaLimitScore)
			{
				///< 小于区域分数,过
				break;
			}
			mNowClickJetton--;
		}
		if (mNowClickJetton < 100)
		{
			nowAreaScore = 0;
		}

		return nowAreaScore;
	}
}

int64 FrameLayer_Baccarat::getScoreByScoreTypeTag(ScoreTypeTag tag)
{
	int64 mJetton = 0;
	switch (tag)
	{
	case FrameLayer_Baccarat::SCORE_VALUE_1000:
		mJetton = 1000;
		break;
	case FrameLayer_Baccarat::SCORE_VALUE_1W:
		mJetton = 10000;
		break;
	case FrameLayer_Baccarat::SCORE_VALUE_10W:
		mJetton = 100000;
		break;
	case FrameLayer_Baccarat::SCORE_VALUE_100W:
		mJetton = 1000000;
		break;
	case FrameLayer_Baccarat::SCORE_VALUE_500W:
		mJetton = 5000000;
		break;
	case FrameLayer_Baccarat::SCORE_VALUE_1000W:
		mJetton = 10000000;
		break;
	default:
		mJetton = 0;
		break;
	}

	return mJetton;
}

// void FrameLayer_Baccarat::setTimeState(bool freeTime, bool betTime, bool openTime)
// {
// // 	img_free_time->setVisible(freeTime);
// // 	img_bet_time->setVisible(betTime);
// // 	img_open_time->setVisible(openTime);
// }

void FrameLayer_Baccarat::setTimeState(int timeLableType)
{
	char timeLableStr[80];
	sprintf(timeLableStr, "baccaratGameScene/Baccarat_Main/sp_tip_%d.png", timeLableType);
	img_time_type->loadTexture(timeLableStr);
}

void FrameLayer_Baccarat::ShowCardStage(int playerPoint, int bankerPoint)
{
	if (playerPoint > bankerPoint)
	{
		char XianPointStr[80];
		sprintf(XianPointStr, "baccaratGameScene/Baccarat_Main/Lable/Point_%d.png", playerPoint);
		img_xian_dian->loadTexture(XianPointStr);
		char ZhuangPointStr[80];
		sprintf(ZhuangPointStr, "baccaratGameScene/Baccarat_Main/Lable/Point_grey_%d.png", bankerPoint);
		img_zhuang_dian->loadTexture(ZhuangPointStr);
	}
	else if (playerPoint < bankerPoint)
	{
		char XianPointStr[80];
		sprintf(XianPointStr, "baccaratGameScene/Baccarat_Main/Lable/Point_grey_%d.png", playerPoint);
		img_xian_dian->loadTexture(XianPointStr);
		char ZhuangPointStr[80];
		sprintf(ZhuangPointStr, "baccaratGameScene/Baccarat_Main/Lable/Point_%d.png", bankerPoint);
		img_zhuang_dian->loadTexture(ZhuangPointStr);
	}
	else
	{
		char XianPointStr[80];
		sprintf(XianPointStr, "baccaratGameScene/Baccarat_Main/Lable/Point_%d.png", playerPoint);
		img_xian_dian->loadTexture(XianPointStr);
		char ZhuangPointStr[80];
		sprintf(ZhuangPointStr, "baccaratGameScene/Baccarat_Main/Lable/Point_%d.png", bankerPoint);
		img_zhuang_dian->loadTexture(ZhuangPointStr);
	}

	///<开启舞台展开动画
	CCSprite* spt = CCSprite::createWithSpriteFrameName("stage_1.png");
	mPanelMain->addChild(spt);
	spt->setPosition(Panel_game_stage->getPosition());
	spt->setScale(1.1);
	spt->runAction(CCSequence::create(
		DelayTime::create(0),
		SpriteHelper::createAnimateManager("stage_", 1, 10, 0.05),
		CCCallFuncND::create(this, callfuncND_selector(FrameLayer_Baccarat::ShowCardStageEnd), 0),
		0));
}

void FrameLayer_Baccarat::ShowCardStageEnd(cocos2d::Node *target, void* data)
{
	///<牌型点数img创建
	xianPoint = ImageView::create();
	zhuangPoint = ImageView::create();
	xianPoint->setPosition(Vec2(590, 515) / 1.07);
	zhuangPoint->setPosition(Vec2(840, 515) / 1.07);
	mPanelMain->addChild(xianPoint, 2);
	mPanelMain->addChild(zhuangPoint, 2);
	xianPoint->setScale(0.9);
	zhuangPoint->setScale(0.9);

	///<庄闲平胜负img创建
	xianWinImage = ImageView::create();
	xianWinImage->loadTexture("baccaratGameScene/Baccarat_Main/Lable/Lable_xian_grey.png");
	zhuangWinImage = ImageView::create();
	zhuangWinImage->loadTexture("baccaratGameScene/Baccarat_Main/Lable/Lable_zhuang_grey.png");
	pingWinImage = ImageView::create();
	xianWinImage->setPosition(Vec2(590, 445) / 1.07);
	zhuangWinImage->setPosition(Vec2(840, 445) / 1.07);
	pingWinImage->setPosition(Vec2(715, 445) / 1.07);
	mPanelMain->addChild(xianWinImage, 2);
	mPanelMain->addChild(zhuangWinImage, 2);
	mPanelMain->addChild(pingWinImage, 2);
	xianWinImage->setScale(0.9);
	zhuangWinImage->setScale(0.9);
	pingWinImage->setScale(0.9);

	//清除动画
	target->pauseSchedulerAndActions();
	target->removeFromParentAndCleanup(true);

	CCSprite* spt = CCSprite::createWithSpriteFrameName("stage_10.png");
	mPanelMain->addChild(spt);
	spt->setScale(1.1);
	spt->setPosition(Panel_game_stage->getPosition());
	spt->runAction(CCSequence::create(
		DelayTime::create(7.5),
		CCCallFuncND::create(this, callfuncND_selector(FrameLayer_Baccarat::ClearStageCard), 0),
		SpriteHelper::createAnimateManager("stage_", 10, 1, 0.05, true),
		CCCallFuncND::create(this, callfuncND_selector(FrameLayer_Baccarat::HideCardStageEnd), 0),
		0));
}

void FrameLayer_Baccarat::ClearStageCard(cocos2d::Node *target, void* data)
{
	GameScene_Baccarat* mGameScene = GameScene_Baccarat::shared();
	if (mGameScene)
	{
		mGameScene->clearCard();
	}
	xianPoint->removeFromParent();
	zhuangPoint->removeFromParent();
	xianWinImage->removeFromParent();
	zhuangWinImage->removeFromParent();
	pingWinImage->removeFromParent();
}

void FrameLayer_Baccarat::HideCardStageEnd(cocos2d::Node *target, void* data)
{
	target->pauseSchedulerAndActions();
	target->removeFromParentAndCleanup(true);

	if (!isSettle) return;

	///<开启结算面板展开动画
	CCSprite* spt = CCSprite::createWithSpriteFrameName("settlePanel_1.png"); //settlePanel_1.png
	mPanelMain->addChild(spt);
	spt->setScale(1.1);
	spt->setPosition(Panel_game_stage->getPosition());
	spt->runAction(CCSequence::create(
		DelayTime::create(0),
		SpriteHelper::createAnimateManager("settlePanel_", 1, 10, 0.05),
		CCCallFuncND::create(this, callfuncND_selector(FrameLayer_Baccarat::ShowSettlePanelEnd), 0),
		0));

}

void FrameLayer_Baccarat::ShowSettlePanelEnd(cocos2d::Node *target, void* data)
{
	//清除动画
	target->pauseSchedulerAndActions();
	target->removeFromParentAndCleanup(true);

	mSettleAccountsPanel->setVisible(true);

	if (atoll(lab_total_points->getString().c_str()) > 0)
	{
		///< 玩家赢看
		SoundManager::shared()->playSound("BACCARAT_GAME_WIN");
	}
	else if (atoll(lab_total_points->getString().c_str()) < 0)
	{
		///< 玩家输了
		SoundManager::shared()->playSound("BACCARAT_GAME_LOSE");
	}
	else
	{
		///< 没输没赢
		SoundManager::shared()->playSound("BACCARAT_GAME_PING");
	}

	CCSprite* spt = CCSprite::createWithSpriteFrameName("settlePanel_10.png");
	mPanelMain->addChild(spt);
	spt->setPosition(Panel_game_stage->getPosition());
	spt->setScale(1.1);
	spt->runAction(CCSequence::create(
		DelayTime::create(5),
		CCCallFuncND::create(this, callfuncND_selector(FrameLayer_Baccarat::HideSettlePanel), 0),
		SpriteHelper::createAnimateManager("settlePanel_", 10, 1, 0.05, true),
		CCCallFuncND::create(this, callfuncND_selector(FrameLayer_Baccarat::ClearSettlePanelEnd), 0),
		0));
}

void FrameLayer_Baccarat::HideSettlePanel(cocos2d::Node *target, void* data)
{
	mSettleAccountsPanel->setVisible(false);
}

void FrameLayer_Baccarat::ClearSettlePanelEnd(cocos2d::Node *target, void* data)
{
	target->pauseSchedulerAndActions();
	target->removeFromParentAndCleanup(true);
}

void FrameLayer_Baccarat::huanZhuang(int tag)
{
	//时间不足7秒,直接展示结果
	CCSprite* spt = CCSprite::create(StringUtils::format("baccaratGameScene/Baccarat_Main/luanhua_%d.png", tag));
	mPanelMain->addChild(spt, 10);
	spt->setScale(1.1);
	spt->setPosition(Panel_game_stage->getPosition());
	spt->runAction(CCSequence::create(
		DelayTime::create(2),
		CCCallFuncND::create(this, callfuncND_selector(FrameLayer_Baccarat::huanZhuangClear), 0),
		0));
}

void FrameLayer_Baccarat::huanZhuangClear(cocos2d::Node *target, void* data)
{
	target->removeFromParentAndCleanup(true);
}

void FrameLayer_Baccarat::ChangePointSprite(int XianPoint, int ZhuangPoint)
{
	if (ZhuangPoint == -1)
	{
		char XianPointStr[80];
		sprintf(XianPointStr, "baccaratGameScene/Baccarat_Main/Lable/Point_%d.png", XianPoint);
		xianPoint->loadTexture(XianPointStr);
	}
	else if (XianPoint > ZhuangPoint)
	{
		char XianPointStr[80];
		sprintf(XianPointStr, "baccaratGameScene/Baccarat_Main/Lable/Point_%d.png", XianPoint);
		xianPoint->loadTexture(XianPointStr);
		char ZhuangPointStr[80];
		sprintf(ZhuangPointStr, "baccaratGameScene/Baccarat_Main/Lable/Point_grey_%d.png", ZhuangPoint);
		zhuangPoint->loadTexture(ZhuangPointStr);
	}
	else if (XianPoint < ZhuangPoint)
	{
		char XianPointStr[80];
		sprintf(XianPointStr, "baccaratGameScene/Baccarat_Main/Lable/Point_grey_%d.png", XianPoint);
		xianPoint->loadTexture(XianPointStr);
		char ZhuangPointStr[80];
		sprintf(ZhuangPointStr, "baccaratGameScene/Baccarat_Main/Lable/Point_%d.png", ZhuangPoint);
		zhuangPoint->loadTexture(ZhuangPointStr);
	}
	else
	{
		char XianPointStr[80];
		sprintf(XianPointStr, "baccaratGameScene/Baccarat_Main/Lable/Point_%d.png", XianPoint);
		xianPoint->loadTexture(XianPointStr);
		char ZhuangPointStr[80];
		sprintf(ZhuangPointStr, "baccaratGameScene/Baccarat_Main/Lable/Point_%d.png", ZhuangPoint);
		zhuangPoint->loadTexture(ZhuangPointStr);
	}
}

void FrameLayer_Baccarat::ShowWinerResult(int gameWinner)
{
	if (gameWinner == AREA_XIAN)
	{
		xianWinImage->loadTexture("baccaratGameScene/Baccarat_Main/Lable/Lable_xian_win.png");
	}
	else if (gameWinner == AREA_ZHUANG)
	{
		zhuangWinImage->loadTexture("baccaratGameScene/Baccarat_Main/Lable/Lable_zhuang_win.png");
	}
	else if (gameWinner == AREA_PING)
	{
		pingWinImage->loadTexture("baccaratGameScene/Baccarat_Main/Lable/Lable_ping_win.png");
		xianWinImage->setVisible(false);
		zhuangWinImage->setVisible(false);
	}
	else if (gameWinner == AREA_TONG_DUI)
	{
		pingWinImage->loadTexture("baccaratGameScene/Baccarat_Main/Lable/Lable_ping_win.png");
		xianWinImage->setVisible(false);
		zhuangWinImage->setVisible(false);
	}
}
void FrameLayer_Baccarat::breakLineStage(int mTimeLeave)
{
	///<牌型点数img创建
	xianPoint = ImageView::create();
	zhuangPoint = ImageView::create();
	xianPoint->setPosition(Vec2(590, 515) / 1.07);
	zhuangPoint->setPosition(Vec2(840, 515) / 1.07);
	mPanelMain->addChild(xianPoint, 2);
	mPanelMain->addChild(zhuangPoint, 2);
	xianPoint->setScale(0.9);
	zhuangPoint->setScale(0.9);

	///<庄闲平胜负img创建
	xianWinImage = ImageView::create();
	xianWinImage->loadTexture("baccaratGameScene/Baccarat_Main/Lable/Lable_xian_grey.png");
	zhuangWinImage = ImageView::create();
	zhuangWinImage->loadTexture("baccaratGameScene/Baccarat_Main/Lable/Lable_zhuang_grey.png");
	pingWinImage = ImageView::create();
	xianWinImage->setPosition(Vec2(590, 445)/1.07);
	zhuangWinImage->setPosition(Vec2(840, 445)/1.07);
	pingWinImage->setPosition(Vec2(715, 445)/1.07);
	mPanelMain->addChild(xianWinImage, 2);
	mPanelMain->addChild(zhuangWinImage, 2);
	mPanelMain->addChild(pingWinImage, 2);
	xianWinImage->setScale(0.9);
	zhuangWinImage->setScale(0.9);
	pingWinImage->setScale(0.9);

	if (mTimeLeave > 7 && isSettle)
	{
		//时间超过7秒 展示牌,如果押注展示结果
		CCSprite* spt = CCSprite::createWithSpriteFrameName("stage_10.png");
		mPanelMain->addChild(spt);
		spt->setScale(1.1);
		spt->setPosition(Panel_game_stage->getPosition());
		spt->runAction(CCSequence::create(
			DelayTime::create(mTimeLeave - 7),
			CCCallFuncND::create(this, callfuncND_selector(FrameLayer_Baccarat::ClearStageCard), 0),
			SpriteHelper::createAnimateManager("stage_", 10, 1, 0.05, true),
			CCCallFuncND::create(this, callfuncND_selector(FrameLayer_Baccarat::HideCardStageEnd), 0),
			0));
	}
	else
	{
		//时间不足7秒,直接展示结果
		stageSprite = CCSprite::createWithSpriteFrameName("stage_10.png");
		mPanelMain->addChild(stageSprite);
		stageSprite->setScale(1.1);
		stageSprite->setPosition(Panel_game_stage->getPosition());
		stageSprite->runAction(CCSequence::create(
			DelayTime::create(mTimeLeave),
			CCCallFuncND::create(this, callfuncND_selector(FrameLayer_Baccarat::breakClearStageCard), 0),
			0));
	}
}
void FrameLayer_Baccarat::breakClearStageCard(cocos2d::Node *target, void* data)
{
	GameScene_Baccarat* mGameScene = GameScene_Baccarat::shared();
	if (mGameScene)
	{
		mGameScene->clearCard();
	}
	xianPoint->removeFromParent();
	zhuangPoint->removeFromParent();
	xianWinImage->removeFromParent();
	zhuangWinImage->removeFromParent();
	pingWinImage->removeFromParent();
	stageSprite->removeFromParent();
}

void FrameLayer_Baccarat::getBtnTips(cocos2d::Node * tNode, CenterTypeTag tag)
{
	ImageView * img_tips_status = dynamic_cast<ImageView *>(tNode->getChildByName("img_tips_status"));

	int index = tag - 200;
	if (img_tips_status)
	{
		mTipsList[index] = img_tips_status;
	}
}

void FrameLayer_Baccarat::checkUpdateCenterPanelStatus()
{
	if (!mNowClickJetton) return;

	int multScore[8] = { 0 };
	int nowScore = getScoreByScoreTypeTag((ScoreTypeTag)mNowClickJetton);
	multScore[0] = nowScore * MULTIPLE_XIAN - nowScore;
	multScore[1] = nowScore * MULTIPLE_PING - nowScore;
	multScore[2] = nowScore * MULTIPLE_ZHUANG - nowScore;
	multScore[3] = nowScore * MULTIPLE_XIAN_TIAN - nowScore;
	multScore[4] = nowScore * MULTIPLE_ZHUANG_TIAN - nowScore;
	multScore[5] = nowScore * MULTIPLE_TONG_DIAN - nowScore;
	multScore[6] = nowScore * MULTIPLE_XIAN_PING - nowScore;
	multScore[7] = nowScore * MULTIPLE_ZHUANG_PING - nowScore;

	bool isNoMeetIf = false; ///< 是不满足条件
	IClientKernel * kernel = IClientKernel::get();
	if (!kernel) return;
	int64 nowMyScore = kernel->GetMeUserItem()->GetUserScore();
	IClientUserItem * BankerItem = kernel->GetTableUserItem(ClientKernelSink_Baccarat::getInstance().m_wCurrentBanker);
	int64 bankerScore = 0;
	if (BankerItem)
	{
		bankerScore = BankerItem->GetUserScore();
	}
	else
	{
		bankerScore = ClientKernelSink_Baccarat::getInstance().m_lAreaLimitScore;
	}

	for (int i = 0; i < 8; i++)
	{
		///< 情况1, 小于自身拥有的钱
		///< 情况2, 小于庄家用户拥有的钱
		///< 1,2满足任意一种,即不在进行计算
		ImageView * nowTips = mTipsList[i];
		// 		if (nowMyScore < nowScore + mMyTotalScore)
		// 		{
		// 			///< 是不满足下注的
		// 			isNoMeetIf = true;
		// 		}
		// 
		// 		int64 totalJetton = multScore[i] + ClientKernelSink_Baccarat::getInstance().getCurJettonTotal();
		// 		CCLOG("totalJetton money %lld", totalJetton);
		// 		if (!isNoMeetIf && bankerScore < totalJetton)
		// 		{
		// 			isNoMeetIf = true;
		// 		}
		int64 maxScore = GetMaxPlayerScore(i, kernel->GetMeChairID());
		CCLOG("maxScore money %lld", maxScore);
		if (nowScore > maxScore)
		{
			isNoMeetIf = true;
		}

		if (isNoMeetIf)
		{
			///< 该区域不能下注
			nowTips->loadTexture("baccaratGameScene/Baccarat_Main/img_tips_no.png");
		}
		else
		{
			///< 该区域能下注
			nowTips->loadTexture("baccaratGameScene/Baccarat_Main/img_tips_yes.png");
		}
		nowTips->setVisible(true);
		isNoMeetIf = false;
	}
}

//最大下注
int64 FrameLayer_Baccarat::GetMaxPlayerScore(byte cbBetArea, word wChairID)
{
	//获取玩家
	IClientKernel * kernel = IClientKernel::get();
	IClientUserItem *pIMeServerUserItem = kernel->GetTableUserItem(wChairID);
	if (NULL == pIMeServerUserItem)
		return 0L;

	if (cbBetArea >= AREA_MAX)
		return 0L;

	//已下注额
	int64 lNowBet = 0l;
	for (int nAreaIndex = 0; nAreaIndex < AREA_MAX; ++nAreaIndex)
		lNowBet += ClientKernelSink_Baccarat::getInstance().m_lUserJettonScore[nAreaIndex];

	//庄家金币
	int64 lBankerScore = ClientKernelSink_Baccarat::getInstance().m_lBankerScore;

	//区域倍率
	byte cbMultiple[AREA_MAX] = { MULTIPLE_XIAN, MULTIPLE_PING, MULTIPLE_ZHUANG,
		MULTIPLE_XIAN_TIAN, MULTIPLE_ZHUANG_TIAN, MULTIPLE_TONG_DIAN,
		MULTIPLE_XIAN_PING, MULTIPLE_ZHUANG_PING };

	//区域输赢
	byte cbArae[4][4] = { { AREA_XIAN_DUI, 255, AREA_MAX, AREA_MAX },
	{ AREA_ZHUANG_DUI, 255, AREA_MAX, AREA_MAX },
	{ AREA_XIAN, AREA_PING, AREA_ZHUANG, AREA_MAX },
	{ AREA_XIAN_TIAN, AREA_TONG_DUI, AREA_ZHUANG_TIAN, 255 } };

	//筹码设定
	for (int nTopL = 0; nTopL < 4; ++nTopL)
	{
		if (cbArae[0][nTopL] == AREA_MAX)
			continue;

		for (int nTopR = 0; nTopR < 4; ++nTopR)
		{
			if (cbArae[1][nTopR] == AREA_MAX)
				continue;

			for (int nCentral = 0; nCentral < 4; ++nCentral)
			{
				if (cbArae[2][nCentral] == AREA_MAX)
					continue;

				for (int nBottom = 0; nBottom < 4; ++nBottom)
				{
					if (cbArae[3][nBottom] == AREA_MAX)
						continue;

					byte cbWinArea[AREA_MAX] = { FALSE };

					//指定获胜区域
					if (cbArae[0][nTopL] != 255)
						cbWinArea[cbArae[0][nTopL]] = TRUE;

					if (cbArae[1][nTopR] != 255)
						cbWinArea[cbArae[1][nTopR]] = TRUE;

					if (cbArae[2][nCentral] != 255)
						cbWinArea[cbArae[2][nCentral]] = TRUE;

					if (cbArae[3][nBottom] != 255)
						cbWinArea[cbArae[3][nBottom]] = TRUE;

					//选择区域为玩家胜利，同等级的其他的区域为玩家输。以得出最大下注值
					for (int i = 0; i < 4; i++)
					{
						for (int j = 0; j < 4; j++)
						{
							if (cbArae[i][j] == cbBetArea)
							{
								for (int n = 0; n < 4; ++n)
								{
									if (cbArae[i][n] != 255 && cbArae[i][n] != AREA_MAX)
									{
										cbWinArea[cbArae[i][n]] = FALSE;
									}
								}
								cbWinArea[cbArae[i][j]] = TRUE;
							}
						}
					}

					int64 lScore = ClientKernelSink_Baccarat::getInstance().m_lBankerScore;;
					for (int nAreaIndex = 0; nAreaIndex < AREA_MAX; ++nAreaIndex)
					{
						if (cbWinArea[nAreaIndex] == TRUE)
						{
							lScore -= ClientKernelSink_Baccarat::getInstance().m_lAllJettonScore[nAreaIndex] * (cbMultiple[nAreaIndex] - 1);
						}
						else if (cbWinArea[AREA_PING] == TRUE && (nAreaIndex == AREA_XIAN || nAreaIndex == AREA_ZHUANG))
						{

						}
						else
						{
							lScore += ClientKernelSink_Baccarat::getInstance().m_lAllJettonScore[nAreaIndex];
						}
					}
					if (lBankerScore == -1)
						lBankerScore = lScore;
					else
						lBankerScore = std::min(lBankerScore, lScore);
				}
			}
		}
	}

	//最大下注
	int64 lMaxBet = 0L;

	//最大下注
	lMaxBet = std::min<int64>(pIMeServerUserItem->GetUserScore() - lNowBet, ClientKernelSink_Baccarat::getInstance().m_lPlayFreeSocre - ClientKernelSink_Baccarat::getInstance().m_lUserJettonScore[cbBetArea]);

	lMaxBet = std::min<int64>(lMaxBet, ClientKernelSink_Baccarat::getInstance().m_lAreaLimitScore - ClientKernelSink_Baccarat::getInstance().m_lAllJettonScore[cbBetArea]);

	lMaxBet = std::min<int64>(lMaxBet, lBankerScore / (cbMultiple[cbBetArea] - 1));

	//非零限制
	lMaxBet = std::max<int64>(lMaxBet, 0);

	//庄闲对等
	if (cbBetArea == AREA_XIAN && (ClientKernelSink_Baccarat::getInstance().m_lAllJettonScore[AREA_ZHUANG] - ClientKernelSink_Baccarat::getInstance().m_lAllJettonScore[AREA_XIAN]) && lMaxBet < (ClientKernelSink_Baccarat::getInstance().m_lAllJettonScore[AREA_ZHUANG] - ClientKernelSink_Baccarat::getInstance().m_lAllJettonScore[AREA_XIAN]))
		lMaxBet = ClientKernelSink_Baccarat::getInstance().m_lAllJettonScore[AREA_ZHUANG] - ClientKernelSink_Baccarat::getInstance().m_lAllJettonScore[AREA_XIAN];
	else if (cbBetArea == AREA_ZHUANG && (ClientKernelSink_Baccarat::getInstance().m_lAllJettonScore[AREA_XIAN] - ClientKernelSink_Baccarat::getInstance().m_lAllJettonScore[AREA_ZHUANG]) && lMaxBet < (ClientKernelSink_Baccarat::getInstance().m_lAllJettonScore[AREA_XIAN] - ClientKernelSink_Baccarat::getInstance().m_lAllJettonScore[AREA_ZHUANG]))
		lMaxBet = ClientKernelSink_Baccarat::getInstance().m_lAllJettonScore[AREA_XIAN] - ClientKernelSink_Baccarat::getInstance().m_lAllJettonScore[AREA_ZHUANG];

	return lMaxBet;
}

bool FrameLayer_Baccarat::onTouchBegan(Touch *pTouch, Event *pEvent)
{
	if (mTrendPanel->isVisible())
		mTrendPanel->setVisible(false);
	return false;
}


