#ifndef _ClientKernelSink_BACCARAT_H_
#define _ClientKernelSink_BACCARAT_H_

//#pragma once

#include "cocos2d.h"
#include "Gamelogic_Baccarat.h"
#include "CMD_Baccarat.h"
#include "Kernel/kernel/game/IClientKernelSink.h"

USING_NS_CC;
// class SceneLayer;
// class CCannonLayer;
// class CoinLayer;
// class TaskManager;
// class CMessage;
// class Bingo;
class CSoundSetting;
// class LockFishLayer;
// class CWater;
// class CBlackWater;
// class EffectLayer;
class GameScene_Baccarat;
//////////////////////////////////////////////////////////////////////////
#define GAME_PLAYER_Baccarat			2
#define	MAX_COUNT				3
//索引定义
#define INDEX_PLAYER				0									//闲家索引
#define INDEX_BANKER				1									//庄家索引
//游戏引擎
class ClientKernelSink_Baccarat :public Ref, public IClientKernelSink
{
	//游戏变量
public:
	CGameLogic_Baccarat*			m_pGameLogic;
	word							m_wCurrentBanker;					//当前庄家
	word							m_wCurrentUser;						//当前用户

	char							m_szUserAccounts[2][LEN_ACCOUNTS];	//玩家帐号

	cocos2d::GLView *_openGLView;

	 bool							m_SendCard;                         //发牌标志
	 byte							m_wSendIndex;						//发牌索引
	 byte							m_cbSendCardCount;					//发牌张数

	 int								m_nSendChair;						//发送座位	
	 int								m_cbLeftCardCount;						//发牌计数		
	 CCPoint							m_CardPoint[GAME_PLAYER_Baccarat];			//手牌位置

	 longlong						m_lBasicGold;						//单元数目
	 longlong						m_lPlayBetScore;					//玩家最大下注
	 longlong						m_lPlayFreeSocre;					//玩家自由积分
	 longlong						m_lTurnBasicGold;					//基础下注
	 int64							m_lQiangCondition;
	 int64							m_lQiangScore;						//抢分
	 bool							m_bCanShowHand;						//是否能梭哈
	 bool							m_bShowHand;						//是否已梭哈
	 longlong						m_lTableScore[GAME_PLAYER_Baccarat];			//玩家总下注
	 bool							m_GameIsStart;
	 int								m_nMultiples[4];
	 int								m_cbSendCard;
	 byte							m_cbPlayStatus[GAME_PLAYER_Baccarat];			//玩家状态
	 bool							m_bCallBanker;

	 byte							m_bUserOxCard[GAME_PLAYER_Baccarat];				//用户牛牛数据
	 byte							m_lUserCardType[GAME_PLAYER_Baccarat];			//用户牌点数据

	 byte							m_cbCardCount[2];					//扑克数目
	 byte							m_cbTableCardArray[GAME_PLAYER_Baccarat][MAX_COUNT];	//桌面扑克
	 int64							m_cbPlayScore[AREA_MAX];			//玩家成绩
	 byte							m_cbTableSortCardArray[5][5];		//桌面扑克
	 bool							m_bShowGameResult;					//显示结果
	 byte							m_bcfirstShowCard;
	 bool	m_Istan;

	//函数定义
public:
	//构造函数
	ClientKernelSink_Baccarat();
	//析构函数
	virtual ~ClientKernelSink_Baccarat();

	static ClientKernelSink_Baccarat & getInstance()
	{
		static ClientKernelSink_Baccarat gClientKernelSink;
		return gClientKernelSink;
	}

	//控制接口
public:
	//启动游戏
	virtual bool SetupGameClient();
	//重置游戏
	virtual void ResetGameClient();
	//关闭游戏
	virtual void CloseGameClient(int exit_tag = 0);

	//框架事件
public:
	//系统滚动消息
	virtual bool OnGFTableMessage(const char* szMessage);
	//全局消息
	virtual bool OnGFGlobalMessage(const char* szMessage);
	//等待提示
	virtual bool OnGFWaitTips(bool bWait);
	//比赛信息
	virtual bool OnGFMatchInfo(tagMatchInfo* pMatchInfo);
	//比赛等待提示
	virtual bool OnGFMatchWaitTips(tagMatchWaitTip* pMatchWaitTip);
	//比赛结果
	virtual bool OnGFMatchResult(tagMatchResult* pMatchResult);

	//游戏事件
public:
	//旁观消息
	virtual bool OnEventLookonMode(void* data, int dataSize);
	//场景消息
	virtual bool OnEventSceneMessage(byte cbGameStatus, bool bLookonUser, void* data, int dataSize);
	//游戏消息
	virtual bool OnEventGameMessage(int sub, void* data, int dataSize);

	//时钟事件
public:
	//用户时钟
	virtual void OnEventUserClock(word wChairID, word wUserClock);
	//时钟删除
	virtual bool OnEventGameClockKill(word wChairID);
	//时钟信息
	virtual bool OnEventGameClockInfo(word wChairID, uint nElapse, word wClockID);

	//用户事件
public:
	//用户进入
	virtual void OnEventUserEnter(IClientUserItem * pIClientUserItem, bool bLookonUser);
	//用户离开
	virtual void OnEventUserLeave(IClientUserItem * pIClientUserItem, bool bLookonUser);
	//用户积分
	virtual void OnEventUserScore(IClientUserItem * pIClientUserItem, bool bLookonUser);
	//用户状态
	virtual void OnEventUserStatus(IClientUserItem * pIClientUserItem, bool bLookonUser);
	//用户属性
	virtual void OnEventUserAttrib(IClientUserItem * pIClientUserItem, bool bLookonUser);
	//用户头像
	virtual void OnEventCustomFace(IClientUserItem * pIClientUserItem, bool bLookonUser);

	//消息定义
public:
	//开始消息
	int OnMessageStart(uint wParam, uint lParam);

	//内部函数
protected:
	//更新按钮
	bool UpdateManualControl(word wViewStepCount);
	//时间消息
	void OnTimer(uint nIDEvent);
	void OnGameTimer(float dt);

	//LKPY函数
	////////////////////////////////////////////////////////////////////
public:
	static ClientKernelSink_Baccarat* GetInstance();
	static void Release();

	bool InitClientKernel(int szCMD);

	bool OnUpdate(float fDt);

	bool OnRender(float hscale, float vscale);

	void SetGameActive(bool active);

	//获取系统消息
	std::string GetGlobalMessage();

public:

	void OnDlgBlackWhite(int iUserID, int iMode, unsigned int data);

	void OnDlgStorage(int iMode, longlong lValue, unsigned int data);
protected:
	//根据游戏模式获取金币类型
	longlong GetUserScoreEx(IClientUserItem* pGameUserItem);

private:
	CSoundSetting*		m_pSoundSetting;
	bool				m_bShowHelp;
	word				m_wShowUserInfoCharID;
	bool				m_bGameReady;
	bool				m_bActive;
	CCPoint				m_Offest;
	float				m_fElapse;

	bool				m_bMirrorShow;
	std::string			szGlobalMessage;
	CCPoint				m_ptOffest;
protected:
	static ClientKernelSink_Baccarat*	s_Instance;

private:
	GameScene_Baccarat*			m_pGameScene;
	bool						mIsNetworkPrepared;

public:
		void OnStartremove(int tga);

		//游戏开始
		bool OnSubGameStart(const void * pBuffer, word wDataSize);
		//游戏空闲
		bool OnSubGameFree(const void * pBuffer, word wDataSize);
		//用户加注
		bool OnSubPlaceJetton(const void * pBuffer, word wDataSize);
		//游戏结束
		bool OnSubGameEnd(const void * pBuffer, word wDataSize);
		//申请做庄
		bool OnSubUserApplyBanker(const void * pBuffer, word wDataSize);
		//取消做庄
		bool OnSubUserCancelBanker(const void * pBuffer, word wDataSize);
		//切换庄家
		bool OnSubChangeBanker(const void * pBuffer, word wDataSize);
		//游戏记录
		bool OnSubGameRecord(const void * pBuffer, word wDataSize);
		//下注失败
		bool OnSubPlaceJettonFail(const void * pBuffer, word wDataSize);
		//扑克牌
		bool OnSubSendCard(const void * pBuffer, word wDataSize);
		//申请结果
		bool OnSubReqResult(const void * pBuffer, word wDataSize);
		//更新库存
		bool OnSubUpdateStorage(const void * pBuffer, word wDataSize);
		//抢庄
		bool OnSubQiangBanker(void * pBuffer, word wDataSize);

		//断线重连_叫庄
		bool onSubRecordConnectBanker(const void * pBuffer, int wDataSize);
		//断线重连_下注
		bool onSubRecordConnectAddGold(const void * pBuffer, int wDataSize);
		//断线重连_游戏
		bool onSubRecordConnectPlaying(const void * pBuffer, int wDataSize);

		void ShowMeCard(Node* sender);
		void ccCardTouched(Node* pNode);
		void ShowMeback(Node* sender);
		void OnMeAddGold(byte onArea, int64 addScore);
		void OnMeGive(int index);
		void Showback(Node* sender);

		void ButtonWithCallBanker(bool isCallBanker);
		void ButtonWithGrabBanker();

		//设置庄家
		void SetBankerInfo(word wBanker, int64 lScore);
		//< 空闲状态消息处理
		bool onEventSceneFree(void * data, int dataSize);
		//< 游戏状态和结束状态消息的处理
		bool onEventSceneGameingAndEnd(void * data, int dataSize);
		///< 获取当前下注的总钱数
		int64 getCurJettonTotal();
		///< 获取玩家下注的总钱数
		int64 getMyJettonTotal();
		///< 获取当前区域的倍数
		int getCurrentAreaMult(int area);
		//下注信息
public:
	int64						m_lUserJettonScore[AREA_COUNT + 1];	//个人总注
	int64						m_lAllJettonScore[AREA_COUNT + 1];	//全体总注
	int64						m_lAllPlayBet[GAME_PLAYER][AREA_COUNT + 1];//所有玩家下注
	bool							m_bNeedSetGameRecord;				//完成设置
	int64						m_lMeStatisticScore;				//游戏成绩
	int64						m_lMeCurGameScore;					//我的成绩

	int64						m_lBankerWinScore;					//庄家成绩	
	int64						m_lBankerScore;						//庄家积分
	int64						m_lTmpBankerWinScore;				//庄家成绩	
	word						m_wBankerTime;						//庄家局数
	bool						m_bEnableSysBanker;					//系统做庄

	//状态变量
public:
	bool						m_bMeApplyBanker;					//申请标识
	bool                        m_bMeQiangBanker;
	int64						m_lAreaLimitScore;					//区域限制
	int64						m_lApplyBankerCondition;			//申请条件
public:
	void SetMePlaceJetton(byte cbViewIndex, int64 lJettonCount);

	virtual void CloseGameDelayClient();

};

//extern ClientKernelSink_Baccarat gClientKernelSink_Baccarat;
//#define gCKS_Bacc gClientKernelSink_Baccarat
#endif // _ClientKernelSink_H_