#ifndef APPLY_USER_LIST_HEAD_FILE_BACCARAT
#define APPLY_USER_LIST_HEAD_FILE_BACCARAT
//
#include "Platform/PFDefine/df/types.h"

class tagApplyUser_Baccarat
{
public:
	//玩家信息
	word						wChairID;							//< 椅子ID
	std::string					strUserName;						//玩家帐号
	int64						lUserScore;							//玩家欢乐豆
	bool                        bQiang;
};

class tagTrendInfo_Baccarat
{
public:
	byte							cbPlayerCount;						//闲家点数
	byte							cbBankerCount;						//庄家点数
	byte							cbKingWinner;						//天王赢家
	bool							bPlayerTwoPair;						//对子标识
	bool							bBankerTwoPair;						//对子标识
};

#endif

