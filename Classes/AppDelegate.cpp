#include "AppDelegate.h"
#include "SimpleAudioEngine.h"
#include "DntgGame/DntgKernel/DntgClientKernelSink.h"
#include "DntgGame/DntgKernel/DntgObjectPool.h"
#include "DntgGame/DntgKernel/DntgPathManager.h"
#include "Platform/PFView/LoginScene/LoginScene.h"
#include "Tools/Manager/SoundManager.h"
#include "Tools/tools/StaticData.h"
#include "Tools/tools/StringData.h"
#include "Tools/tools/MTNotification.h"

#include "C2DXShareSDK/C2DXShareSDK.h"
#include <string>
#include <sstream>

const float kRevolutionWidth = 1420;
const float kRevolutionHeight = 800;

USING_NS_CC;
using namespace CocosDenshion;
using namespace Dntg;
using namespace std;

AppDelegate::AppDelegate() {

}

AppDelegate::~AppDelegate() 
{
	CCSpriteFrameCache::sharedSpriteFrameCache()->purgeSharedSpriteFrameCache();
	CCAnimationCache::sharedAnimationCache()->purgeSharedAnimationCache();

	SoundManager::purge();
	Path_Manager::purge();
	ObjectPool::purge();
	StaticData::purge();
	StringData::purge();
	CServerListData::purge();
	CParameterGlobal::purge();
}

//if you want a different context,just modify the value of glContextAttrs
//it will takes effect on all platforms
void AppDelegate::initGLContextAttrs()
{
    //set OpenGL context attributions,now can only set six attributions:
    //red,green,blue,alpha,depth,stencil
    GLContextAttrs glContextAttrs = {8, 8, 8, 8, 24, 8};

    GLView::setGLContextAttrs(glContextAttrs);
}




bool AppDelegate::applicationDidFinishLaunching() {

	//初始化ShareSDK
	//this->initShareSDKConfig();

	CCLOG("come in appdelegate");
	CCTexture2D::setDefaultAlphaPixelFormat(kCCTexture2DPixelFormat_RGBA8888);
	CCTexture2D::PVRImagesHavePremultipliedAlpha(true);
	ZipUtils::ccSetPvrEncryptionKeyPart(0, 0x11111111);
	ZipUtils::ccSetPvrEncryptionKeyPart(1, 0x22222222);
	ZipUtils::ccSetPvrEncryptionKeyPart(2, 0x33333333);
	ZipUtils::ccSetPvrEncryptionKeyPart(3, 0x44444444);
	srand((unsigned int)time(0));

    // initialize director
    auto director = Director::getInstance();
    auto glview = director->getOpenGLView();
    if(!glview) {
        glview = GLViewImpl::create("ArcadeProject");
        director->setOpenGLView(glview);
    }

#if(CC_TARGET_PLATFORM == CC_PLATFORM_WIN32)
	glview->setFrameSize(1136, 640);
#endif

	glview->setDesignResolutionSize(kRevolutionWidth, kRevolutionHeight, kResolutionExactFit);

	//glview->setViewName(STATIC_DATA_STRING(""));
	//eglView->setFrameSize(1920, 1080);

	//  去掉  系统  按钮
	//long l_WinStyle = GetWindowLong(glview->getWin32Window(), GWL_STYLE);
	//SetWindowLong(glview->getWin32Window(), GWL_STYLE, l_WinStyle & ~WS_SYSMENU /*& ~WS_CAPTION & ~WS_BORDER*/);
	//eglView->setWndProc(WINDOW_PROC);

    // turn on display FPS
    director->setDisplayStats(false);

    // set FPS. the default value is 1.0/60 if you don't call this
    director->setAnimationInterval(1.0 / 50);

	FileUtils::getInstance()->addSearchPath(FileUtils::getInstance()->getWritablePath());
    // create a scene. it's an autorelease object
	Scene *pScene = LoginScene::create();

	srand(time(0));
    // run
	director->runWithScene(pScene);

	CCDirector::getInstance()->getScheduler()->scheduleSelector(
		schedule_selector(MyNotification::post),
		MyNotification::getInstance(),
		(float)(1.0 / 60.0), false);


    return true;
}

// This function will be called when the app is inactive. When comes a phone call,it's be invoked too
void AppDelegate::applicationDidEnterBackground() {
	Director::getInstance()->stopAnimation();

	// if you use SimpleAudioEngine, it must be pause
	auto mananger = SimpleAudioEngine::getInstance();
	mananger->pauseBackgroundMusic();
	mananger->setBackgroundMusicVolume(0);
	//CCSpriteFrameCache::sharedSpriteFrameCache()->removeUnusedSpriteFrames();

	G_NOTIFY_D("GAME_PAUSE", MTData::create());
	
}

// this function will be called when the app is active again
void AppDelegate::applicationWillEnterForeground() {
	Director::getInstance()->startAnimation();

	// if you use SimpleAudioEngine, it must resume here
	auto mananger = SimpleAudioEngine::getInstance(); 
	mananger->resumeBackgroundMusic();
	mananger->setBackgroundMusicVolume(50);

	G_NOTIFY_D("GAME_RESUME", MTData::create());
}
/*
void AppDelegate::initShareSDKConfig()
{
	//设置平台配置
	//Platforms
	__Dictionary *totalDict = __Dictionary::create();

	//微信
	__Dictionary *wechatConf = __Dictionary::create();
	wechatConf->setObject(__String::create("wx7de6c5c43caa2176"), "AppId");
	//wechatConf->setObject(__String::create("wx7de6c5c43caa2176"), "app_id");
	//wechatConf->setObject(__String::create("a576080d3d17ee891d760e48bcea24e6"), "app_secret");
	//wechatConf->setObject(__String::create("3e82269051fa4341676e24a92a204edc"), "app_secret");
	wechatConf->setObject(__String::create("3e82269051fa4341676e24a92a204edc"), "AppSecret");
	wechatConf->setObject(__String::create("false"), "BypassApproval");
	wechatConf->setObject(__String::create("true"), "Enable");
	wechatConf->setObject(__String::create("4"), "Id");
	wechatConf->setObject(__String::create("1"), "SortID");
	stringstream wechat;
	wechat << cn::sharesdk::C2DXPlatTypeWeChat;
	totalDict->setObject(wechatConf, wechat.str());

	//QQ
	__Dictionary *qqConf = __Dictionary::create();
	qqConf->setObject(__String::create("12b8562d96420"), "app_id");
	qqConf->setObject(__String::create("7ddc6c7f2d12415c113641412d174ca7"), "app_key");
	stringstream qq;
	qq << cn::sharesdk::C2DXPlatTypeQQPlatform;
	totalDict->setObject(qqConf, qq.str());

	cn::sharesdk::C2DXShareSDK::registerAppAndSetPlatformConfig("1cc530f23ef38", totalDict);
}*/
