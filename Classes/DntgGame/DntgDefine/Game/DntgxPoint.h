#ifndef __Dntg_POINT_H__
#define __Dntg_POINT_H__
#pragma once
#pragma pack(1)
///////////////////////////////////////////////////////////////////////////////////////////////

namespace  Dntg
{
	///////////////////////////////////////////////////////////////////////////////////////////////
	class xPoint
	{
	public:
		xPoint() :x_(0), y_(0) {}
		xPoint(float x, float y) :x_(x), y_(y) {}
		xPoint(const xPoint &point) :x_(point.x_), y_(point.y_) {}
		~xPoint() {}

	public:
		void offset(float x, float y) { x_ += x; y_ += y; }

		void set_point(float x, float y) { x_ = x; y_ = y; }

		bool operator == (const xPoint &point) const { return (x_ == point.x_&&y_ == point.y_); }
		bool operator != (const xPoint &point) const { return (x_ != point.x_ || y_ != point.y_); }

		xPoint &operator = (const xPoint &point) { x_ = point.x_; y_ = point.y_; return *this; }

		void operator += (const xPoint &point) { x_ += point.x_; y_ += point.y_; }
		void operator -= (const xPoint &point) { x_ -= point.x_; y_ -= point.y_; }

		xPoint operator + (const xPoint &point) { return xPoint(x_ + point.x_, y_ + point.y_); }
		xPoint operator - (const xPoint &point) { return xPoint(x_ - point.x_, y_ - point.y_); }
		xPoint operator - () { return xPoint(-x_, -y_); }

		xPoint operator * (float multip) { return xPoint(x_*multip, y_*multip); }

	public:
		float x_;
		float y_;
	};


	inline xPoint operator+ (const xPoint &lhs, const xPoint &rhs)
	{
		return xPoint(lhs.x_ + rhs.x_, lhs.y_ + rhs.y_);
	}

	inline xPoint operator- (const xPoint &lhs, const xPoint &rhs)
	{
		return xPoint(lhs.x_ - rhs.x_, lhs.y_ - rhs.y_);
	}

#pragma pack()

	///////////////////////////////////////////////////////////////////////////////////////////////
}
#endif
