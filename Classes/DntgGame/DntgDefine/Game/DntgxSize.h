#ifndef __Dntg_SIZE_H__
#define __Dntg_SIZE_H__
#pragma once
/////////////////////////////////////////////////////////////////////////////////////////
#pragma pack(push)
#pragma pack(1)
namespace  Dntg
{

	/////////////////////////////////////////////////////////////////////////////////////////
	class xSize
	{
	public:
		xSize() :width_(0), height_(0)  { }
		xSize(float width, float height) :width_(width), height_(height) {}
		xSize(const xSize &size) : width_(size.width_), height_(size.height_) {}
		~xSize() {}

		bool operator == (const xSize &size) const { return (size.width_ == width_&&size.height_ == height_); }
		bool operator != (const xSize &size) const { return (size.width_ != width_ || size.height_ != height_); }

		xSize &operator = (const xSize &size) { width_ = size.width_; height_ = size.height_; return *this; }

		xSize& operator += (const xSize &size) { width_ += size.width_; height_ += size.height_; return *this; }
		xSize& operator -= (const xSize &size) { width_ -= size.width_; height_ -= size.height_; return *this; }

		void set_size(float width, float height) { width_ = width; height_ = height; }

		xSize operator + (const xSize &size) { return xSize(width_ + size.width_, height_ + size.height_); }
		xSize operator - (const xSize &size) { return xSize(width_ - size.width_, height_ - size.height_); }
		xSize operator - () { return xSize(-width_, -height_); }

	public:
		float width_;
		float height_;
	};

#pragma pack(pop)
	/////////////////////////////////////////////////////////////////////////////////////////
}
#endif
