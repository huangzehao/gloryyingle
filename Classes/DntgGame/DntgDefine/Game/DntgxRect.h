#ifndef __Dntg_RECT_H__
#define __Dntg_RECT_H__
#pragma once
#pragma pack(push)
#pragma pack(1)

///////////////////////////////////////////////////////////////////////////////////////////////
#include "DntgxPoint.h"
#include "DntgxSize.h"

///////////////////////////////////////////////////////////////////////////////////////////////
namespace  Dntg
{

	///////////////////////////////////////////////////////////////////////////////////////////////
	class  xRect
	{
	public:
		xRect();
		xRect(const xPoint &point, const xSize &size);
		xRect(float left, float top, float right, float bottom);
		xRect(const xRect& rect);

		xPoint get_position() const { return xPoint(left_, top_); }
		void set_position(const xPoint& pt);

		float get_width() const { return right_ - left_; }
		void set_width(float width) { right_ = left_ + width; }

		float get_height() const { return bottom_ - top_; }
		void set_height(float height) { bottom_ = top_ + height; }

		xSize get_size() const { return xSize(get_width(), get_height()); }
		void set_size(const xSize& size)	{ set_width(size.width_); set_height(size.height_); }

		void set_rect(float left, float top, float right, float bottom);
		void set_rect(const xPoint &point, const xSize &size);

		void offset_rect(float x, float y);
		void offset_rect(const xSize &size);

		bool pt_in_rect(const xPoint& point) const;
		bool is_empty() const { return (get_width() == 0.0f && get_height() == 0.0f); }

		xRect intersection_rect(const xRect& rect) const;
		//xRect union_rect(const xRect& rect) const ;
		//xRect subtract_rect(const xRect& rect) const ;

		xRect& operator = (const xRect& rhs)
		{
			left_ = rhs.left_;
			top_ = rhs.top_;
			right_ = rhs.right_;
			bottom_ = rhs.bottom_;

			return *this;
		}

		bool operator == (const xRect& rhs) const
		{
			return ((left_ == rhs.left_) && (right_ == rhs.right_) && (top_ == rhs.top_) && (bottom_ == rhs.bottom_));
		}
		bool operator != (const xRect& rhs) const { return !operator==(rhs); }

	public:
		float left_, top_, right_, bottom_;
	};

#pragma pack(pop)
	///////////////////////////////////////////////////////////////////////////////////////////////
}
#endif