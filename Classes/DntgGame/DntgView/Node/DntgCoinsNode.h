#ifndef _Dntg_CoinsNode_H_
#define _Dntg_CoinsNode_H_

#include "cocos2d.h"
namespace  Dntg
{
#define MAX_COIN		30

	class CoinsNode : public cocos2d::Node
	{
	public:
		static CoinsNode* create();

	private:
		CoinsNode();
		~CoinsNode();
		bool init();

	public:
		void show_coin(int count, int score);

	private:
		void func_show(cocos2d::Node* node);
	private:
		cocos2d::Label *label_coin_;
		cocos2d::Sprite *spr_coin_[MAX_COIN];

		cocos2d::SpriteBatchNode* mBatchNode;
	};
}
#endif // _CoinsNode_H_