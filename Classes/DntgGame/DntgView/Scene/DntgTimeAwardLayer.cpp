#include "DntgTimeAwardLayer.h"
#include "Tools/tools/MTNotification.h"
#include "Tools/tools/StaticData.h"
#include "DntgGame/DntgHeader.h"
#include "Tools/tools/Convert.h"
#include "Tools/tools/StringData.h"
#include "Tools/ViewHeader.h"
USING_NS_CC;
USING_NS_CC_EXT;

//////////////////////////////////////////////////////////////////////////
#define IMG_BT_AWARD_1		"bt_award_1.png"
#define IMG_BT_AWARD_2		"bt_award_2.png"

#define TAG_AWARD			1
//////////////////////////////////////////////////////////////////////////
using namespace Dntg;
static ControlButton* createButton(const char* normal, const char* selected, const char* disable, cocos2d::Ref* target, cocos2d::extension::Control::Handler selector)
{
	// 	ui::Button * btn_now = ui::Button::create(normal, selected, disable);
	// 	btn_now->addTouchEventListener(target, selector);

	ui::Scale9Sprite* sptNormal = ui::Scale9Sprite::createWithSpriteFrameName(normal);
	ui::Scale9Sprite* sptSelected = selected == 0 ? 0 : ui::Scale9Sprite::createWithSpriteFrameName(selected);
	ui::Scale9Sprite* sptDisable = disable == 0 ? 0 : ui::Scale9Sprite::createWithSpriteFrameName(disable);

	//关闭按钮
	ControlButton* bt = ControlButton::create();
	bt->setBackgroundSpriteForState(sptNormal, Control::State::NORMAL);
	if (sptSelected)
		bt->setBackgroundSpriteForState(sptSelected, Control::State::SELECTED);
	if (sptDisable)
		bt->setBackgroundSpriteForState(sptDisable, Control::State::DISABLED);
	bt->setPreferredSize(sptNormal->getPreferredSize());
	bt->addTargetWithActionForControlEvents(target, selector, Control::EventType::TOUCH_UP_INSIDE);

	return bt;
}

//////////////////////////////////////////////////////////////////////////
TimeAwardLayer::TimeAwardLayer()
: mTimeAwardMission(ADDRESS_URL, SERVER_PORT)
{
	mTimeAwardMission.setMissionSink(this);

	mCurGold = 0;
	mTimeForGold = 0;
	mGold = 0;
	mGoldLimit = 0;
}

TimeAwardLayer::~TimeAwardLayer()
{
	mTimeAwardMission.setMissionSink(0);
}


//初始化方法
bool TimeAwardLayer::init()
{
	do
	{
		CC_BREAK_IF(!CCLayer::init());

		ControlButton* btAward = createButton(IMG_BT_AWARD_1, IMG_BT_AWARD_2, 0, this, cccontrol_selector(TimeAwardLayer::onBtnClick));
		btAward->setTag(TAG_AWARD);
		btAward->setPosition(cocos2d::Vec2(btAward->getContentSize().width / 2 + 20, kRevolutionHeight - btAward->getContentSize().height / 2 - 10));
		addChild(btAward);
		Label* lbAwardInfo = Label::create(a_u8("点击领取\n0金币"), "Arial", 20, cocos2d::Size(100, 100), kCCTextAlignmentCenter, kCCVerticalTextAlignmentCenter);
		lbAwardInfo->setTag(1);
		lbAwardInfo->setColor(ccc3(255, 255, 0));
		lbAwardInfo->setPosition(cocos2d::Vec2(40, 0));
		btAward->addChild(lbAwardInfo);

		return true;
	} while (0);

	return false;
}

void TimeAwardLayer::onEnterTransitionDidFinish()
{
	CCLayer::onEnterTransitionDidFinish();
	mTimeAwardMission.checkTimeAward();
}

//按钮事件
void TimeAwardLayer::onBtnClick(cocos2d::Ref* obj, cocos2d::extension::Control::EventType e)
{
	Control* ctr = (Control*)obj;

	switch (ctr->getTag())
	{
	case TAG_AWARD:
	{
					  if (mCurGold > 0)
						  mTimeAwardMission.getTimeAward();
	}
		break;
	}

}

void TimeAwardLayer::recheckTimeAward(float delay)
{
	mTimeAwardMission.checkTimeAward();
}

void TimeAwardLayer::timeAwardTickFirst(float delay)
{
	mCurGold += mGold;
	if (mCurGold >= mGoldLimit)
		mCurGold = mGoldLimit;
	else // 未到上限
		this->schedule(schedule_selector(TimeAwardLayer::timeAwardTick), (int)mTimeForGold);

	updateGoldShow();
}


void TimeAwardLayer::timeAwardTick(float delay)
{
	mCurGold += mGold;
	if (mCurGold >= mGoldLimit)
	{
		mCurGold = mGoldLimit;
		this->unschedule(schedule_selector(TimeAwardLayer::timeAwardTick));
	}

	updateGoldShow();
}

void TimeAwardLayer::updateGoldShow()
{
	Control* ctr = (Control*)getChildByTag(TAG_AWARD);
	Label* ttf = 0;

	if (ctr != 0)
		ttf = (Label*)ctr->getChildByTag(1);

	if (ttf)
	{
		char szContent[128] = { 0 };
		sprintf(szContent, a_u8("点击领取\n" LLSTRING "金币"), mCurGold);
		ttf->setString(szContent);
	}
}

//////////////////////////////////////////////////////////////////////////
//IGPTimeAwardMissionSink
void TimeAwardLayer::onGPTimeAwardCheckResult(int64 lLastAwardTime, int64 lCurrentTime, int iTimeForGold, int iGold, int iGoldLimit)
{
	int64 lInterval = lCurrentTime - lLastAwardTime;

	mCurGold = 0;
	mTimeForGold = iTimeForGold;
	mGold = iGold;
	mGoldLimit = iGoldLimit;

	//计算可领取金币
	mCurGold = 0;

	if (mGold == 0)
	{
		mGold = 1;
	}


	if (mTimeForGold > 0 && lInterval > 0 && mGold > 0)
	{
		mCurGold = lInterval / mTimeForGold * mGold;

		if (mCurGold > mGoldLimit)
			mCurGold = mGoldLimit;
	}

	updateGoldShow();

	lInterval = lInterval - mCurGold * mTimeForGold / mGold;
	lInterval = mTimeForGold - lInterval;
	if (lInterval < 0)
		lInterval = 0;

	this->unschedule(schedule_selector(TimeAwardLayer::timeAwardTickFirst));
	this->unschedule(schedule_selector(TimeAwardLayer::timeAwardTick));

	if (lInterval > 0)
	{
		this->scheduleOnce(schedule_selector(TimeAwardLayer::timeAwardTickFirst), (int)lInterval);
	}
	else
	{
		this->schedule(schedule_selector(TimeAwardLayer::timeAwardTick), (int)mTimeForGold);
	}
}

void TimeAwardLayer::onGPTimeAwardGetResult(int32 iGold)
{
	if (iGold <= 0)
	{
		popup(SSTRING("system_tips_title"), a_u8("领取失败"));
	}
	else
	{
		char szContent[128] = { 0 };
		sprintf(szContent, a_u8("恭喜您获得%d金币"), iGold);
		popup(SSTRING("system_tips_title"), szContent);
		G_NOTIFY_D("INSURE_QUERY", 0);
	}


	this->unschedule(schedule_selector(TimeAwardLayer::recheckTimeAward));
	this->scheduleOnce(schedule_selector(TimeAwardLayer::recheckTimeAward), 0.5f);
}
