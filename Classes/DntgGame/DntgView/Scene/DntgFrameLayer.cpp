#include "DntgFrameLayer.h"
#include "cocos-ext.h"
#include "DntgGameScene.h"
#include "Tools/tools/MTNotification.h"
#include "Kernel/kernel/game/IClientKernel.h"
#include "DntgGame/DntgHeader.h"

USING_NS_CC;
USING_NS_CC_EXT;

using namespace ui;
using namespace Dntg;


//////////////////////////////////////////////////////////////////////////
FrameLayer::FrameLayer()
// m_admin_item(0)
: admin_ui_item_call_obj(0)
, admin_ui_item_callfun(0)
, isLock(false)
, menuvisible(true)
, isAutoFire(false)
{

}

FrameLayer::~FrameLayer()
{
	// 	if (this->isScheduled(SEL_SCHEDULE(&FrameLayer::fun_exit_balance_time)))
	// 	{
	// 		this->unschedule(SEL_SCHEDULE(&FrameLayer::fun_exit_balance_time));
	// 	}
	this->unscheduleAllCallbacks();
}

//初始化方法
bool FrameLayer::init()
{
	do
	{
		CC_BREAK_IF(!Layer::init());

		Size winSize = Director::getInstance()->getWinSize();

		SpriteFrameCache* cache = SpriteFrameCache::getInstance();

		auto lock = MenuItemSprite::create(Sprite::createWithSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("lock.png")),
			Sprite::createWithSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("lock_d.png")));
		auto unlock = MenuItemSprite::create(Sprite::createWithSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("unlock.png")),
			Sprite::createWithSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("unlock_d.png")));

		lockMenu = MenuItemToggle::createWithTarget(this, menu_selector(FrameLayer::lockCallback), lock, unlock, NULL);
		lockMenu->setPosition(cocos2d::Vec2(winSize.width / 13, winSize.height / 7 * 4.0f + 50));

		auto autofire = MenuItemSprite::create(Sprite::createWithSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("autofire_d.png")),
			Sprite::createWithSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("autofire.png")));
		auto unautofire = MenuItemSprite::create(Sprite::createWithSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("autofire.png")),
			Sprite::createWithSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("autofire_d.png")));

		autofireMenu = MenuItemToggle::createWithTarget(this, menu_selector(FrameLayer::autofireCallback), autofire, unautofire, NULL);
		autofireMenu->setPosition(cocos2d::Vec2(winSize.width / 13 * 12, winSize.height / 7 * 2.5f + 50));

		Sprite* addCannonItemNormal = Sprite::createWithSpriteFrame(cache->spriteFrameByName("addcannon.png"));
		Sprite* addCannonItemSelected = Sprite::createWithSpriteFrame(cache->spriteFrameByName("addcannon_d.png"));
		addCannonItem = MenuItemSprite::create(addCannonItemNormal, addCannonItemSelected, nullptr, this, menu_selector(FrameLayer::addCannonCallback));
		addCannonItem->setPosition(cocos2d::Vec2(winSize.width / 13, winSize.height / 7 * 2.5f + 50));

		Sprite* subCannonItemNormal = Sprite::createWithSpriteFrame(cache->spriteFrameByName("subcannon.png"));
		Sprite* subCannonItemSelected = Sprite::createWithSpriteFrame(cache->spriteFrameByName("subcannon_d.png"));
		subCannonItem = MenuItemSprite::create(subCannonItemNormal, subCannonItemSelected, nullptr, this, menu_selector(FrameLayer::subCannonCallback));
		subCannonItem->setPosition(cocos2d::Vec2(winSize.width / 13, winSize.height / 7 * 1 + 50));

		Sprite* sptCloseNormal = Sprite::createWithSpriteFrame(cache->spriteFrameByName("quit.png"));
		Sprite* sptCloseSelected = Sprite::createWithSpriteFrame(cache->spriteFrameByName("quit_d.png"));
		pCloseItem = MenuItemSprite::create(sptCloseNormal, sptCloseSelected, nullptr, this, menu_selector(FrameLayer::menuCloseCallback));
		pCloseItem->setPosition(cocos2d::Vec2(winSize.width / 13 * 12, winSize.height / 7 * 4.0f + 50));

		pMenu = Menu::create(addCannonItem, subCannonItem, pCloseItem, lockMenu, autofireMenu, 0);
		pMenu->setAnchorPoint(cocos2d::Vec2(0, 0));
		pMenu->setPosition(0, 0);
		this->addChild(pMenu);

		auto hide = MenuItemSprite::create(Sprite::createWithSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("hide.png")),
			Sprite::createWithSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("hide_d.png")));
		auto show = MenuItemSprite::create(Sprite::createWithSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("show.png")),
			Sprite::createWithSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("show_d.png")));

		hideMenu = MenuItemToggle::createWithTarget(this, menu_selector(FrameLayer::hideItemCallback), hide, show, NULL);
		hideMenu->setPosition(cocos2d::Vec2(winSize.width / 13 * 12, winSize.height / 7 * 1 + 50));

		Menu* pMenu_hideItem = Menu::create(hideMenu, 0);
		pMenu_hideItem->setAnchorPoint(cocos2d::Vec2(0, 0));
		pMenu_hideItem->setPosition(0, 0);
		this->addChild(pMenu_hideItem);

		createExitBalancePanel();

		return true;
	} while (0);

	return false;
}

void FrameLayer::addAdminButton()
{
	Size winSize = Director::getInstance()->getWinSize();

	SpriteFrameCache* cache = SpriteFrameCache::getInstance();

	Sprite* sptCloseNormal = Sprite::createWithSpriteFrame(cache->spriteFrameByName("bt_settings_1.png"));
	Sprite* sptCloseSelected = Sprite::createWithSpriteFrame(cache->spriteFrameByName("bt_settings_1.png"));
	MenuItemSprite *pAdminItem = MenuItemSprite::create(sptCloseNormal, sptCloseSelected, nullptr, this, menu_selector(FrameLayer::adminItemCallBack));
	pAdminItem->setAnchorPoint(cocos2d::Vec2(1, 1));
	pAdminItem->setPosition(cocos2d::Vec2(winSize.width - 50, winSize.height));

	//pAdminItem->setVisible(false);
	//m_admin_item = pAdminItem;

	Menu* pMenu = Menu::create(pAdminItem, 0);
	pMenu->setAnchorPoint(cocos2d::Vec2(0, 0));
	pMenu->setPosition(0, 0);
	this->addChild(pMenu);
}

void FrameLayer::setCloseDialogInfo(cocos2d::Ref* target, cocos2d::SEL_CallFuncN callfun, const std::string& title, const std::string& content)
{
	mTarget = target;
	mCallback = callfun;
	mTitle = title;
	mContent = content;
}

void FrameLayer::menuCloseCallback(cocos2d::Ref* pSender)
{
	//popup(mTitle.c_str(), mContent.c_str(), DLG_MB_OK|DLG_MB_CANCEL, 0, mTarget, mCallback);
	mBalancePanel->setVisible(true);
	pMenu->setVisible(false);
	flushExitBalancePanelData();
}
void FrameLayer::addCannonCallback(cocos2d::Ref* pSender)
{
	G_NOTIFY_D("CANNON_ADD", 0);
}
void FrameLayer::subCannonCallback(cocos2d::Ref* pSender)
{
	G_NOTIFY_D("CANNON_SUB", 0);
}
void FrameLayer::autofireCallback(cocos2d::Ref* pSender)
{
	// 	if (isLock)
	// 	{
	// 		isLock = false;
	// 	}
	GameScene * gameScene = dynamic_cast<GameScene *>(this->getParent());
	if (!isAutoFire){
		isAutoFire = true;
		gameScene->autoFire();
	}
	else
	{
		isAutoFire = false;
		gameScene->setIsFire(false);
	}
}
void FrameLayer::lockCallback(cocos2d::Ref* pSender)
{
	// 	if (isAutoFire)
	// 	{
	// 		isAutoFire = false;
	// 	}

	isLock = !isLock;
	if (!isLock)
	{
		IClientKernel* kernel = IClientKernel::get();
		int char_id = kernel->GetMeChairID();
		int value = 1 << char_id;
		G_NOTIFY_D("GAME_BIRD_LOCK_CANCEL", MTData::create(value, 1));
	}
}
void FrameLayer::hideItemCallback(cocos2d::Ref* pSender)
{
	if (menuvisible)
	{
		pMenu->setVisible(false);
		menuvisible = false;
	}
	else
	{
		pMenu->setVisible(true);
		menuvisible = true;
	}
}
void FrameLayer::hideMenuButton()
{
	pMenu->setVisible(false);
	menuvisible = false;
	hideMenu->setSelectedIndex(1);
}
void FrameLayer::setAdminItemCallBack(cocos2d::Ref* target_, cocos2d::SEL_CallFunc callfun_)
{
	admin_ui_item_call_obj = target_;
	admin_ui_item_callfun = callfun_;
}

void FrameLayer::adminItemCallBack(cocos2d::Ref* pSender)
{
	GameScene *gc_ = dynamic_cast<GameScene*>(admin_ui_item_call_obj);
	if (gc_ && admin_ui_item_callfun){
		(gc_->*admin_ui_item_callfun)();
	}
}

void FrameLayer::createExitBalancePanel()
{
	///< 检测更新!!
	Size winSize = Director::getInstance()->getWinSize();
	///< 1. 创建面板
	mBalancePanel = Sprite::createWithSpriteFrameName("img_count_exit_bg.png");
	mBalancePanel->setScale(1.1);
	mBalancePanel->setPosition(Vec2(winSize.width / 2, winSize.height / 2));
	this->addChild(mBalancePanel);

	int z = 0;
	for (int i = 4; i >= 0; i--)
	{
		for (int j = 0; j < 7; j++)
		{
			TextAtlas * lab_count = TextAtlas::create("0", "fonts/img_gray_number.png", 18, 21, "0");
			lab_count->setPosition(Vec2(150+ j * 130, 191 + i * 72));
			lab_count->setString("0");
			lab_count->setTag(z++);
			mBalancePanel->addChild(lab_count);
			mBirdCount.push_back(lab_count);
		}
	}

	lab_timeout = TextAtlas::create("20", "fonts/img_exit_window_continue_num.png", 33, 46, "0");
	lab_timeout->setPosition(Vec2(505, 60));
	mBalancePanel->addChild(lab_timeout);

	lab_bird_add_Money = TextAtlas::create("200000", "fonts/img_user_score.png", 13, 19, "0");
	lab_bird_add_Money->setPosition(Vec2(320, 140));
	lab_bird_add_Money->setAnchorPoint(Vec2(0, 1));
	mBalancePanel->addChild(lab_bird_add_Money);

	lab_total_money = TextAtlas::create("200000000000", "fonts/img_user_score.png", 13, 19, "0");
	lab_total_money->setPosition(Vec2(780, 140));
	lab_total_money->setAnchorPoint(Vec2(0, 1));
	mBalancePanel->addChild(lab_total_money);

	Sprite * btn_continue_game = Sprite::createWithSpriteFrameName("btn_continue_game.png");
	Sprite * btn_exit_game = Sprite::createWithSpriteFrameName("btn_exit_game.png");
	Sprite * btn_continue_game_2 = Sprite::createWithSpriteFrameName("btn_continue_game.png");
	Sprite * btn_exit_game_2 = Sprite::createWithSpriteFrameName("btn_exit_game.png");

	cocos2d::CCMenu* mMenu = CCMenu::create();
	mMenu->setPosition(Vec2(0, 0));

	CCMenuItemSprite* menuItem = CCMenuItemSprite::create(btn_continue_game, btn_continue_game_2, nullptr, this, menu_selector(FrameLayer::func_affirm_exit_call_back));
	menuItem->setTag(DLG_MB_CANCEL);
	menuItem->setPosition(Vec2(200, 50));
	mMenu->addChild(menuItem);

	CCMenuItemSprite* menuItem_2 = CCMenuItemSprite::create(btn_exit_game, btn_exit_game_2, nullptr, this, menu_selector(FrameLayer::func_affirm_exit_call_back));
	menuItem_2->setTag(DLG_MB_OK);
	menuItem_2->setPosition(Vec2(820, 50));
	mMenu->addChild(menuItem_2);

	mBalancePanel->addChild(mMenu);

	mBalancePanel->setVisible(false);

}

void FrameLayer::func_affirm_exit_call_back(cocos2d::Ref *pSender)
{
	Node* node = dynamic_cast<Node*>(pSender);
	if (node->getTag() == DLG_MB_OK)
	{
		if (mCallback)
		{
			(mTarget->*mCallback)(node);
		}
	}
	else if (node->getTag() == DLG_MB_CANCEL)
	{
		mBalancePanel->setVisible(false);
		pMenu->setVisible(true);
	}
}

void FrameLayer::flushExitBalancePanelData()
{
	GameScene * _gameScene = dynamic_cast<GameScene *>(this->getParent());
	BirdDealCountManager & bird_manage = _gameScene->getBirdDealCountnManager();
	std::string single_money = _gameScene->getSingleGameTotalGold();
	std::string total_money = _gameScene->getUserTotalMoney();
	total_time = 20;
	lab_bird_add_Money->setString(single_money);
	lab_total_money->setString(total_money);
	lab_timeout->setString(StringUtils::format("%d", total_time));


	for (auto it : bird_manage)
	{
		TextAtlas * now_text = nullptr;
		if (it.first == BIRD_TYPE_INGOT) continue;

		switch (it.first)
		{
		case BIRD_TYPE_29:
			now_text = mBirdCount[27];
			break;
		case BIRD_TYPE_CHAIN:
			now_text = mBirdCount[33];
			break;
		case BIRD_TYPE_RED:
			now_text = mBirdCount[34];
			break;
		case BIRD_TYPE_ONE:
			now_text = mBirdCount[28];
			break;
		case BIRD_TYPE_TWO:
			now_text = mBirdCount[29];
			break;
		case BIRD_TYPE_THREE:
			now_text = mBirdCount[30];
			break;
		case BIRD_TYPE_FOUR:
			now_text = mBirdCount[31];
			break;
		case BIRD_TYPE_FIVE:
			now_text = mBirdCount[32];
			break;
		default:
			now_text = mBirdCount[it.first];
			break;
		}

		now_text->setString(StringUtils::format("%d", it.second));
	}

	this->schedule(SEL_SCHEDULE(&FrameLayer::fun_exit_balance_time), 1);


}

void FrameLayer::fun_exit_balance_time(float dt)
{
	total_time--;
	if (total_time > 0)
	{
		lab_timeout->setString(StringUtils::format("%d", total_time));
	}
	else
	{
		this->unschedule(SEL_SCHEDULE(&FrameLayer::fun_exit_balance_time));

		mBalancePanel->setVisible(false);
		pMenu->setVisible(true);
	}
}

FrameLayer * FrameLayer::create()
{
	FrameLayer * layer = new FrameLayer;
	if (layer && layer->init())
	{
		layer->autorelease();
		return layer;
	}
	delete layer;
	layer = nullptr;
	return nullptr;
}

