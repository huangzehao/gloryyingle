#ifndef _Dntg_TimeAwardLayer_H_
#define _Dntg_TimeAwardLayer_H_

#include "cocos2d.h"
#include "cocos-ext.h"
#include "Platform/PFKernel/CGPTimeAwardMission.h"
#include "Platform/PlatformHeader.h"
namespace  Dntg
{
	class TimeAwardLayer
		: public cocos2d::Layer
		, public IGPTimeAwardMissionSink
	{
	public:
		CREATE_FUNC(TimeAwardLayer);

	public:
		TimeAwardLayer();
		~TimeAwardLayer();
		bool init();

		virtual void onEnterTransitionDidFinish();

	private:
		//按钮事件
		void onBtnClick(cocos2d::Ref* obj, cocos2d::extension::Control::EventType e);

		void recheckTimeAward(float delay);
		void timeAwardTickFirst(float delay);
		void timeAwardTick(float delay);
		void updateGoldShow();

		//////////////////////////////////////////////////////////////////////////
		//IGPTimeAwardMissionSink
	public:
		virtual void onGPTimeAwardCheckResult(int64 lLastAwardTime, int64 lCurrentTime, int iTimeForGold, int iGold, int iGoldLimit);
		virtual void onGPTimeAwardGetResult(int32 iGold);

	private:
		CGPTimeAwardMission		mTimeAwardMission;
		int64					mCurGold;				//当前可领取金币
		int						mTimeForGold;			//金币增长间隔时间
		int						mGold;					//金币增长量
		int						mGoldLimit;				//金币增长上限
	};
}
#endif // _TimeAwardLayer_H_