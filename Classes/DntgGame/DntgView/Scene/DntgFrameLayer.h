#ifndef _Dntg_FrameLayer_H_
#define _Dntg_FrameLayer_H_

#include "cocos2d.h"
#include "ui/CocosGUI.h"

USING_NS_CC;
namespace  Dntg
{
	class FrameLayer
		: public cocos2d::Layer
	{
	public:
		//CREATE_FUNC(FrameLayer);
		static FrameLayer * create();
	public:
		FrameLayer();
		~FrameLayer();
		bool init();

		void setCloseDialogInfo(cocos2d::Ref* target, cocos2d::SEL_CallFuncN callfun, const std::string& title, const std::string& content);

		void setAdminItemCallBack(cocos2d::Ref* target_, cocos2d::SEL_CallFunc callfun_);

		void addAdminButton();

		///< 返回是否锁定
		bool getLock() const { return isLock; }
		///< 返回是否自动开火
		bool getAutoFire() const { return isAutoFire; }
		//隐藏菜单按钮
		void hideMenuButton();
	private:
		void menuCloseCallback(cocos2d::Ref* pSender);
		void addCannonCallback(cocos2d::Ref* pSender);
		void subCannonCallback(cocos2d::Ref* pSender);
		void hideItemCallback(cocos2d::Ref* pSender);
		void autofireCallback(cocos2d::Ref* pSender);
		void lockCallback(cocos2d::Ref* pSender);

		void adminItemCallBack(cocos2d::Ref* pSender);

		///< 创建退出结算面板
		void createExitBalancePanel();
		///< 刷新结束面板数据
		void flushExitBalancePanelData();
		///< 退出结算面板计时
		void fun_exit_balance_time(float dt);
		///< 确认按钮点击回调
		void func_affirm_exit_call_back(cocos2d::Ref *pSender);
	private:
		cocos2d::Ref*		mTarget;
		cocos2d::SEL_CallFuncN	mCallback;
		std::string				mTitle;
		std::string				mContent;

		MenuItemSprite *pCloseItem;
		MenuItemSprite *addCannonItem;
		MenuItemSprite *subCannonItem;
		Menu* pMenu;
		MenuItemToggle* lockMenu;
		MenuItemToggle* autofireMenu;
		MenuItemToggle* hideMenu;
		bool menuvisible;
		bool isLock;
		///< 是自动开火
		bool isAutoFire;
		//CCMenuItem *m_admin_item;
		cocos2d::Ref* admin_ui_item_call_obj;
		cocos2d::SEL_CallFunc admin_ui_item_callfun;

		///< 结算面板,总面板
		cocos2d::Sprite * mBalancePanel;
		///< 各个数字,由小到大
		std::vector<cocos2d::ui::TextAtlas *> mBirdCount;
		///<  时间计数
		cocos2d::ui::TextAtlas * lab_timeout;
		///< 打死鱼加的钱
		cocos2d::ui::TextAtlas * lab_bird_add_Money;
		///<  总钱数
		cocos2d::ui::TextAtlas * lab_total_money;
		///< 时间间隔
		int total_time;
	};
}
#endif // _FrameLayer_H_