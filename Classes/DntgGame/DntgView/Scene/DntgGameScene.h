#ifndef _Dntg_GameScene_H_
#define _Dntg_GameScene_H_

#include "cocos2d.h"
#include "Tools/Dialog/Timer.h"
#include "DntgGame/DntgDefine/Game/DntgCMD_CatchBird.h"
#include "DntgGame/DntgDefine/Game/DntgBird.h"
#include "Tools/Manager/SoundManager.h"
USING_NS_CC;

namespace  Dntg
{
	class CSoundNode;
	class BirdLayer;
	class RoleLayer;
	class BulletLayer;
	class CoinLayer;
	class NetLayer;
	class TaskLayer;
	class SceneLayer;
	class MeInfoLayer;
	class FrameLayer;
	class CashShopLayer;
	class BirdNode;
	class ChatLayer;
	class UIAdmin;

	enum GameStatus {
		GAME_LOGIC,
		GAME_BUY_PREPARE,
		GAME_BUY_SEND,
		GAME_ACCOUNT,
		GAME_END
	};


	struct SendSavaStruct
	{
	public:
		std::map<int, Vec2> startList; ///< 保证的红鱼
		std::map<int, std::vector<Vec2>> otherList; ///< 其他的链接鱼
		std::map<int, int> type_value; ///< 表示使用哪种效果
		int total_price; ///< 总价值

		SendSavaStruct() : total_price(0) {}

	};

	class GameScene
		: public cocos2d::CCScene
	{
	public:
		//创建方法
		//CREATE_FUNC(GameScene);
		static GameScene * create();
		static GameScene* shared();
		static void purge();
		static void initGameBaseData();
	public:
		//构造函数
		GameScene();
		~GameScene();

		//初始化方法
		virtual bool init();
		void onMenuItemClick(cocos2d::Ref* obj);
		//场景动画完成
		virtual void onEnterTransitionDidFinish();
		//   添加管理员UI
		void addAdminUI();

		void showAdminUI();
		void removeAdminUI();

		//
		virtual void update(float delta);

		// 碰撞检测
		void collision_dection();
		//< 锁定检测
		bool collision_pos_bird(cocos2d::Vec2 touch_pos);
		bool collision_pos_bird();

		bool compute_collision(
			float bird_x, float bird_y, float bird_width, float bird_height, float bird_rotation,
			float bullet_x, float bullet_y, float bullet_radius);
		int bullet_index_factroy();
		void open_net(Bullet *bullet);

		void destoryBird(Bird* bird);

		RoleLayer* get_role_layer() { return role_layer_; }
		GameStatus get_status() const { return status_; }
		void set_status(GameStatus status)  { status_ = status; }
		//获取当前总财富
		int64 getTreasure();
		//change by xy
		void autoFire();
		void updateBowRotation();
		bool getIsFire();
		void setIsFire(bool isCanFire);
		//change end
		//////////////////////////////////////////////////////////////////////////
		// ITouchSink
	public:
		virtual bool onTouchBegan(cocos2d::CCTouch *pTouch, cocos2d::CCEvent *pEvent);
		virtual void onTouchMoved(cocos2d::CCTouch *pTouch, cocos2d::CCEvent *pEvent);
		virtual void onTouchEnded(cocos2d::CCTouch *pTouch, cocos2d::CCEvent *pEvent);

		//////////////////////////////////////////////////////////////////////////
		//网络消息
	public:
		void on_scene_message(byte cbGameStatus, bool bLookonUser, void* data, int dataSize);

		//用户进入
		void net_user_enter(int chair_id);
		//用户离开
		void net_user_leave(int chair_id);
		//用户分数
		void net_user_score(int chair_id);
		//用户状态
		void net_user_status(int chair_id);
		//用户属性
		void net_user_attrib(int chair_id);
		// 购买子弹成功
		void net_buy_bullet_success(int chair_id, uint64 count);
		//购买子弹失败
		void net_buy_bullet_failed(int chair_id);
		//购买失败
		void net_fire_failed(CMD_S_Fire_Failed *pFireFailed);
		//改变炮弹
		bool net_change_cannon(void* data, int dataSize);
		//子弹生成
		void net_send_bullet(CMD_S_Send_Bullet* send_bullet);
		//鱼生成
		void net_send_bird(CMD_S_Send_Bird* send_bird);
		//特殊鱼阵生成
		void net_send_bird_linear(CMD_S_Send_Bird_Linear* send_bird);
		void net_send_bird_round(CMD_S_Send_Bird_Round* send_bird);
		void net_send_bird_pause_linear(CMD_S_Send_Bird_Pause_Linear* send_bird);

		//生成鱼阵
		//圆阵
		void net_send_scene_round();
		//一圈一圈鼓出的鱼
		void net_send_scene_bloating();
		void round_linear(cocos2d::Node* node, void* data);

		//抓住鱼
		//void net_bird_dead(CMD_S_Catch_Bird* catch_bird);
		//鱼群中的抓住鱼
		void net_bird_dead(CMD_S_Special_Catch_Bird* catch_bird);
		//抓住鱼
		void net_bird_group_dead(CMD_S_Catch_Bird_Group* catch_group, int thread_id, bool isEndWave = false);
		///< 抓住闪电鱼
		void net_bird_group_dead(CMD_S_Catch_Bird_Chain* catch_group, int thread_id, bool isEndWave = false);
		//切换场景
		void net_change_scene(int scene);
		//上下分
		void net_account(CMD_S_Account* account);
		//鱼特效
		void net_bird_effect(CMD_S_Bird_Effect* bird_effect);
		//网络时间校准
		void net_time_check();

		//武器信息
		bool net_weapon_level(void* data, int dataSize);
		//技能
		bool net_skill(void* data, int dataSize);
		//强化结果
		bool net_strengthen_result(void* data, int dataSize);

		//任务
		void net_task(CMD_S_Task_Open* task);
		//任务数量
		void net_task_count(CMD_S_TaskCount* taskCount);
		//任务完成
		void net_task_complete(CMD_S_Task_Finish* taskComplete);

		//定
		void bird_ding(bool ding);
		//加速
		void bird_speed_up(bool speed_up);
		//设置是否可开火
		void set_fire_enable(bool isEanbled);

		//震屏
		void start_shake_screen(float shake_radius);
		void stop_shake_screen();
		void shake_screen_update();

		//购买子弹
		void op_buy_bullet(SCORE fishCoins);
		//////////////////////////////////////////////////////////////////////////
		// 辅助函数

	private:
		//定时器
		void func_send_time_check(float dt);
		//删除爆炸
		void func_bomb_effect_end(cocos2d::Node * node, void * data);
		float calcRotate(int chair_id, const xPoint& pt_offset);
		cocos2d::Vec2 locationFromTouch(cocos2d::CCTouch *touch);

		//游戏操作
		//下分
		void op_account();
		//最大分
		void op_buy_max();
		//增加炮类型
		void op_cannon_add();
		// 减少炮
		void op_cannon_sub();
		// 最大炮
		void op_cannon_max();
		// 发炮
		void op_fire();
		// 想服务器发送子弹
		void send_op_fire(Bullet * bullet);

		void closeCallback(cocos2d::Node *pNode);

		void func_strenthen_confirm(cocos2d::Ref* obj);
		void func_ui_strenthen_open(cocos2d::Ref* obj);

		//win32键盘消息
		void func_keyboard(cocos2d::Ref* obj);

		// 游戏暂停
		void func_game_pause(cocos2d::Ref* obj);
		// 游戏回复
		void func_game_resume(cocos2d::Ref* obj);

		void func_score_up(cocos2d::Ref* obj);
		void func_score_down(cocos2d::Ref* obj);
		void func_cannon_add(cocos2d::Ref* obj);
		void func_cannon_sub(cocos2d::Ref* obj);
		void func_cannon_max(cocos2d::Ref* obj);
		///< 断线重连
		void func_Reconnect_on_loss(cocos2d::Ref * obj);
		void reconnect_on_loss(float dt);
		///< 切换场景
		void changeSceneMusicPlay(float dt);
		///< 清除鱼
		void clearSceneBird();
		///< 清除子弹
		void clearSceneBullet();
	public:
		///< 是断线重连
		static bool is_Reconnect_on_loss();

		///< 获得鱼死亡统计管理器
		BirdDealCountManager & getBirdDealCountnManager() { return bird_deal_count_manager_; }
		///< 获得单局获得总金币
		std::string getSingleGameTotalGold();
		///< 获得对应总钱数
		std::string getUserTotalMoney();
	public:
		UIAdmin* getAdminLayer(){
			return admin_layer_;
		}

		int getBirdTypeByBirdId(int bird_id);

	private:
		struct round_bird_linear
		{
			uint8 bird_type;
			uint32 bird_count;
			uint32 start_id;
			xPoint pt_center;
		};

		BirdManager		bird_manager_;
		BulletManager	bullet_manager_;
		BirdDealCountManager bird_deal_count_manager_;
		///< 单局奖励的钱
		int64 SingleGameTotalGold;

		SceneLayer*		scene_layer_;
		BirdLayer*		bird_layer_;
		NetLayer*		net_layer_;
		BulletLayer*	bullet_layer_;
		RoleLayer*		role_layer_;
		CoinLayer*		coin_layer_;
		/*	TouchLayer*		touch_layer_;*/
		TaskLayer*		task_layer_;
		FrameLayer*		frame_layer_;
		MeInfoLayer*	me_info_layer_;
		ChatLayer*			mChatLayer;
		CashShopLayer*  mCashShopLayer_;

		UIAdmin*     admin_layer_;

		GameStatus status_;
		bool 			is_key_fire_;
		bool			is_touch_fire_;
		bool			is_game_pause_;
		int				netType;
		int exchange_count;				//单位兑换
		double cash_ratio;				//兑换比

		CoTimer			warning_timer_;

// 		CSoundNode* warning_snd_node_;
// 		CSoundNode* effect_snd_node_;
		//////////////////////////////////////////////////////////////////////////
		//震屏
		float shake_screen_angle_;
		float shake_screen_radius_;

		//< 监听器
		EventListenerTouchOneByOne * _listener;
		EventListenerMouse * _mouseListener;

		///< 发送存储结构容器, 用来处理线程安全
		std::map<int, SendSavaStruct> sendSavaList;
		///< 前一个线程的id
		int pre_thread_id;

		//	std::map<int, cocos2d::Action *> _stop_action;
	public:
		//< 取消鱼的锁定的回调
		void cancelBirdLock(cocos2d::Ref * obj);
		///< 取消全部鱼的锁定
		void cancelAllBirdLock();
		///< 设置鱼锁定
		void setBirdLock(int char_id, int bird_id);
		///< ֨քȫߖҤ
		bool isDing;
	};
}

#endif // _GameScene_H_
