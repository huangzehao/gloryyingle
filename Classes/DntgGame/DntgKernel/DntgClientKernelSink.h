#ifndef _Dntg_ClientKernelSink_H_
#define _Dntg_ClientKernelSink_H_

#include "cocos2d.h"
#include "DntgGame/DntgView/Scene/DntgGoDataProxy.h"
#include "Kernel/kernel/game/IClientKernel.h"

namespace Dntg
{
	class GameScene;
	struct CMD_S_Task_Open;

	class ClientKernelSink
		: public IClientKernelSink
	{
	public:
		ClientKernelSink(void);
		virtual ~ClientKernelSink(void);

		static ClientKernelSink & getInstance()
		{
			static ClientKernelSink gClientKernelSink;
			return gClientKernelSink;
		}
		//控制接口
	public:
		//启动游戏
		virtual bool SetupGameClient();
		//重置游戏
		virtual void ResetGameClient();
		//关闭游戏
		virtual void CloseGameClient(int exit_tag = 0);

		//框架事件
	public:
		//系统滚动消息
		virtual bool OnGFTableMessage(const char* szMessage);
		//等待提示
		virtual bool OnGFWaitTips(bool bWait);
		//比赛信息
		virtual bool OnGFMatchInfo(tagMatchInfo* pMatchInfo);
		//比赛等待提示
		virtual bool OnGFMatchWaitTips(tagMatchWaitTip* pMatchWaitTip);
		//比赛结果
		virtual bool OnGFMatchResult(tagMatchResult* pMatchResult);

		//游戏事件
	public:
		//旁观消息
		virtual bool OnEventLookonMode(void* data, int dataSize);
		//场景消息
		virtual bool OnEventSceneMessage(byte cbGameStatus, bool bLookonUser, void* data, int dataSize);
		//游戏消息
		virtual bool OnEventGameMessage(int sub, void* data, int dataSize);


		//用户事件
	public:
		//用户进入
		virtual void OnEventUserEnter(IClientUserItem * pIClientUserItem, bool bLookonUser);
		//用户离开
		virtual void OnEventUserLeave(IClientUserItem * pIClientUserItem, bool bLookonUser);
		//用户积分
		virtual void OnEventUserScore(IClientUserItem * pIClientUserItem, bool bLookonUser);
		//用户状态
		virtual void OnEventUserStatus(IClientUserItem * pIClientUserItem, bool bLookonUser);
		//用户属性
		virtual void OnEventUserAttrib(IClientUserItem * pIClientUserItem, bool bLookonUser);
		//用户头像
		virtual void OnEventCustomFace(IClientUserItem * pIClientUserItem, bool bLookonUser);


		//////////////////////////////////////////////////////////////////////////
		// 游戏子消息
		//购买子弹成功
		bool on_sub_buy_bullet_success(void* data, int dataSize);
		//购买子弹失败
		bool on_sub_buy_bullet_failed(void* data, int dataSize);
		//改变炮类型
		bool on_sub_change_cannon(void* data, int dataSize);
		//鱼生成
		bool on_sub_send_bird(void* data, int dataSize);
		//子弹生成
		bool on_sub_send_bullet(void* data, int dataSize);
		//开火失败
		bool on_sub_fire_failed(void* data, int dataSize);
		//抓住鱼, 系统回掉
		bool on_sub_catch_bird(void * data, int dataSize);
		//抓住鱼, 代码内调用, 写2个函数是为了不破坏整个代码的风格
		bool on_sub_catch_bird_class(void * data, int dataSize);
		//抓住鱼
		bool on_sub_catch_bird_group(void* data, int dataSize);
		///< 抓住闪电鱼
		bool on_sub_catch_bird_chain(void* data, int dataSize);
		//改变场景
		bool on_sub_scene_change(void* data, int dataSize);
		//上下分
		bool on_sub_game_account(void* data, int dataSize);
		//时间校准
		bool on_sub_time_check(void* data, int dataSize);
		//时间校准
		bool on_sub_order_time_check(void* data, int dataSize);
		//鱼特效
		bool on_sub_bird_effect(void* data, int dataSize);
		//鱼boss
		bool on_sub_bird_boss(void* data, int dataSize);

		//武器信息
		bool on_sub_weapon_info(void* data, int dataSize);
		//武器等级
		bool on_sub_weapon_level(void* data, int dataSize);
		//财富信息
		bool on_sub_treasure(void* data, int dataSize);
		//技能
		bool on_sub_skill(void* data, int dataSize);
		//强化结果
		bool on_sub_strengthen_result(void* data, int dataSize);

		//任务悬赏
		bool on_sub_task_reward(void* data, int dataSize);
		//任务
		bool on_sub_task(void* data, int dataSize);
		//任务数量
		bool on_sub_task_count(void* data, int dataSize);
		//任务完成
		bool on_sub_task_complete(void* data, int dataSize);
		//排名信息
		bool on_sub_rank_info(void* data, int dataSize);
		//排行榜
		bool on_sub_rank_list(void* data, int dataSize);
		//库存操作
		bool on_sub_storage(void* data, int dataSize);
		//黑白名单
		bool on_sub_black_white(void* data, int dataSize);
		//游戏状态 所有鱼
		bool on_sub_play_bird(void* data, int dataSize);
		//游戏状态 所有子弹
		bool on_sub_play_bullet(void* data, int dataSize);
		//特殊鱼阵
		bool on_sub_bird_linear(void* data, int dataSize);
		bool on_sub_bird_round(void* data, int dataSize);
		bool on_sub_bird_pause_linear(void* data, int dataSize);
		///< 任务开启
		bool on_sub_game_task_open(void* data, int dataSize);
		///< 任务结束
		bool on_sub_game_task_finish(void* data, int dataSize);
		/////////////////////////////////////////////////////////////////////////////////////

		//   前控 解析

		bool onSockOperateResult(void *data, int dataSize);

		bool onRBLControlResult(void*data, int dataSize);

		bool onDifficultOperateResult(void *data, int dataSize);

		//////////////////////////////////////////////////////////////////////////
		// 功能函数
	public:
		void send_time_check(bool is_game_status);
		///< 清理任务信息
		void clear_task_info();

		virtual void OnEventUserClock(word wChairID, word wUserClock);

		virtual bool OnEventGameClockKill(word wChairID);

		virtual bool OnEventGameClockInfo(word wChairID, uint nElapse, word wClockID);

		virtual void CloseGameDelayClient();

		//////////////////////////////////////////////////////////////////////////
		// 
	private:
		bool		mIsNetworkPrepared;
		GameScene*	mGameScene;
		GoDataProxy mDataProxy;
		///< 线程计数
		int thread_count;
		///< 2个任务
		CMD_S_Task_Open * mChairTask;
		CMD_S_Task_Open * mRoomTask;
	};
}


#endif // _ClientKernelSink_H_