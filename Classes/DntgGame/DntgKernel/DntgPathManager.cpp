#include "cocos2d.h"
#include "DntgPathManager.h"
#include "DntgGame/DntgDefine/Game/DntgCMD_CatchBird.h"
USING_NS_CC;
//////////////////////////////////////////////////////////////////////////
using namespace Dntg;
Path_Manager* Path_Manager::msInstance = 0;

Path_Manager* Path_Manager::shared()
{
	if (msInstance == 0)
	{
		msInstance = new Path_Manager();
		msInstance->initialise_paths("");
	}

	return msInstance;
}

void Path_Manager::purge()
{
	if (msInstance)
		delete msInstance;
	msInstance = 0;
}

///////////////////////////////////////////////////////////////////////////
Path_Manager::Path_Manager()
{
}

Path_Manager::~Path_Manager()
{
	small_paths_.clear();
	big_paths_.clear();
	huge_paths_.clear();
	special_paths_.clear();
	scene_paths_.clear();
}

size_t Dntg_pfwrite(void* data, int elemsize, int count, std::string &sz_path)
{
	unsigned char dataa[1024];
	int size_byte = elemsize*count;

	unsigned char* src = (unsigned char*)data;
	unsigned char* dest = dataa;

	for (int i = 0; i < size_byte; i++)
		*(dest++) = (*src++) ^ 0xDE;

	dataa[size_byte] = '\0';
	sz_path += (char*)dataa;

	return size_byte;
}

bool Dntg_get_string_line(std::string& path, const std::string& all, unsigned int& start_pos)
{
	path.clear();
	int end_pos = 0;
	unsigned int n = all.length();

	if ((start_pos >= 0) && (start_pos < n))
	{
		end_pos = all.find_first_of("\n ", start_pos);

		if ((end_pos < 0) && (end_pos >n))
			end_pos = n;

		path += all.substr(start_pos, end_pos - start_pos);
		start_pos = end_pos + 1;

	}
	return !path.empty();
}

bool Dntg_path_load(std::vector<Move_Points>& paths, int count, const char* format)
{
	paths.clear();
	std::string sData;
	std::string line;

	for (int i = 0; i < count; i++)
	{

		char filename[128] = { 0 };
		sprintf(filename, format, i);
		//获得文件在系统的绝对路径
		std::string filepath = CCFileUtils::sharedFileUtils()->fullPathForFilename(filename);
		//读取的字节数，读取失败则为0
		ssize_t nread = 0;
		//读取的内容
		unsigned char *rdata = CCFileUtils::sharedFileUtils()->getFileData(filepath.c_str(), "rb", &nread);
		if (nread <= 0 || rdata == 0)
			return false;

		//解密	
		sData.clear();
		sData.resize(nread, 0);
		for (int i = 0; i < nread; i++)
			sData[i] = rdata[i] ^ 0xDE;
		delete[] rdata;

		//新加路径
		paths.push_back(Move_Points());
		Move_Points &move_points = paths[paths.size() - 1];

		//每行读取
		unsigned int start_pos = 0;
		Dntg_get_string_line(line, sData, start_pos);

		while (Dntg_get_string_line(line, sData, start_pos))
		{
			int x, y, staff;
			float angle;
			//读取路径节点信息
			std::sscanf(line.c_str(), "(%d,%d,%f,%d)", &x, &y, &angle, &staff);
			//CCLOG("(%d,%d,%d,%d)", i, x, y, staff);
			move_points.push_back(Move_Point(xPoint(x, y), angle));
		}
	}

	return true;
}

bool Path_Manager::initialise_paths(const std::string &directory)
{
	if (!Dntg_path_load(small_paths_, MAX_SMALL_PATH, "path/small/%d.dat"))
		return false;
	if (!Dntg_path_load(big_paths_, MAX_BIG_PATH, "path/big/%d.dat"))
		return false;
	if (!Dntg_path_load(huge_paths_, MAX_HUGE_PATH, "path/huge/%d.dat"))
		return false;
	if (!Dntg_path_load(special_paths_, MAX_SPECIAL_PATH, "path/special/%d.dat"))
		return false;
	if (!Dntg_path_load(scene_paths_, MAX_SCENE_PATH, "path/scene/%d.dat"))
		return false;

	return true;
}

Move_Points &Path_Manager::get_paths(uint16_t path_id, uint8_t path_type)
{
	if (path_type == PATH_TYPE_SMALL)
	{
		if (path_id >= MAX_SMALL_PATH)
			throw("get_paths small path_id too big");

		return small_paths_[path_id];
	}
	else if (path_type == PATH_TYPE_BIG)
	{
		if (path_id >= MAX_BIG_PATH)
			throw("get_paths big path_id too big");

		return big_paths_[path_id];
	}
	else if (path_type == PATH_TYPE_HUGE)
	{
		if (path_id >= MAX_HUGE_PATH)
			throw("get_paths huge path_id too big");

		return huge_paths_[path_id];
	}
	else if (path_type == PATH_TYPE_SPECIAL)
	{
		if (path_id >= MAX_SPECIAL_PATH)
			throw("get_paths special path_id too big");

		return special_paths_[path_id];
	}
	else
	{
		if (path_id >= MAX_SCENE_PATH)
			throw("get_paths scene path_id too big");

		return scene_paths_[path_id];
	}
}


///////////////////////////////////////////////////////////////////////////
