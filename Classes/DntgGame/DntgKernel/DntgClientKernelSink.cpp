#include "DntgGame/DntgView/Scene/DntgGameScene.h"
#include "DntgGame/DntgKernel/DntgClientKernelSink.h"
#include "DntgGame/DntgView/Scene/DntgTimestampManager.h"
#include "Tools/tools/MTNotification.h"
#include "Platform/PFDefine/msg/CMD_GameServer.h"
#include "Kernel/kernel/server/IServerItem.h"
USING_NS_CC;
using namespace Dntg;

//////////////////////////////////////////////////////////////////////////
ClientKernelSink::ClientKernelSink(void)
: mGameScene(0), thread_count(0), mChairTask(nullptr), mRoomTask(nullptr)
{

}

ClientKernelSink::~ClientKernelSink(void)
{
	clear_task_info();
	mGameScene = nullptr;
}

void ClientKernelSink::clear_task_info()
{
	if (mRoomTask) delete mRoomTask;
	if (mChairTask) delete mChairTask;
	mRoomTask = nullptr;
	mChairTask = nullptr;
}

//控制接口
//启动游戏场景
bool ClientKernelSink::SetupGameClient()
{
	mDataProxy.reset();
	PLAZZ_PRINTF(("flow->ClientKernelSink::SetupGameClient\n"));
	mIsNetworkPrepared = false;

	Director * director = Director::getInstance();
	Scene * pScene = director->getRunningScene();
	if (pScene->getTag() == KIND_ID && pScene->getName() == "GameScene")
	{
		///< 表示是自己!!
		mGameScene = dynamic_cast<GameScene *>(pScene);
	}
	else
	{
		if (!mGameScene)
			mGameScene = GameScene::create();
	}

	const tagUserAttribute &userattr_ = IServerItem::get()->GetUserAttribute();
// 	if (CUserRight::IsGameCheatUser(userattr_.dwUserRight)){
// 		mGameScene->addAdminUI();
// 	}

	PLAZZ_PRINTF(("flow->ClientKernelSink::SetupGameClient1\n"));
	//CCDirector::getInstance()->replaceScene(CCTransitionFade::create(0.5f,mGameScene));

	if (pScene != mGameScene)
	{
		director->pushScene(CCTransitionFade::create(0.5f, mGameScene));
	}
	else
	{
		if (IClientKernel::get())
		{
			IClientKernel::get()->SendGameOption();
		}
	}

	//return true 发送进入场景消息
	//否则, 自己调用IClientKernel->SendGameOption();
	return false;
}

//重置游戏
void ClientKernelSink::ResetGameClient()
{
	mDataProxy.reset();
	PLAZZ_PRINTF(("flow->ClientKernelSink::ResetGameClient\n"));
	mIsNetworkPrepared = false;
}

//关闭游戏
void ClientKernelSink::CloseGameClient(int exit_tag)
{
	mIsNetworkPrepared = false;
	mGameScene = 0;
	PLAZZ_PRINTF(("flow->ClientKernelSink::CloseGameClient\n"));
	//CCDirector::getInstance()->replaceScene(CCTransitionFade::create(0.5f,ServerScene::create())); 

	//CCDirector::getInstance()->popScene();
	//G_NOTIFY("MODE_SELECTED", MTData::create(1));

	//	G_NOTIFY_D("BACK_TO_SERVER_LIST", 0);
	if (!GameScene::is_Reconnect_on_loss())
		G_NOTIFY_D("MODE_SELECTED", MTData::create(1));

}


//////////////////////////////////////////////////////////////////////////
//框架事件

//系统滚动消息
bool ClientKernelSink::OnGFTableMessage(const char* szMessage)
{
	PLAZZ_PRINTF(("flow->ClientKernelSink::OnGFTableMessage %s\n"), szMessage);
	return true;
}

//等待提示
bool ClientKernelSink::OnGFWaitTips(bool bWait)
{
	//PLAZZ_PRINTF(("flow->ClientKernelSink::OnGFWaitTips %s\n"), bWait ? a_u8("游戏等待...") : a_u8("等待结束"));
	return true;
}

//比赛信息
bool ClientKernelSink::OnGFMatchInfo(tagMatchInfo* pMatchInfo)
{
	PLAZZ_PRINTF(("flow->ClientKernelSink::OnGFMatchInfo\n"));
	return true;
}

//比赛等待提示
bool ClientKernelSink::OnGFMatchWaitTips(tagMatchWaitTip* pMatchWaitTip)
{
	PLAZZ_PRINTF(("flow->ClientKernelSink::OnGFMatchWaitTips\n"));
	return true;
}

//比赛结果
bool ClientKernelSink::OnGFMatchResult(tagMatchResult* pMatchResult)
{
	PLAZZ_PRINTF(("flow->ClientKernelSink::OnGFMatchResult\n"));
	return true;
}


//////////////////////////////////////////////////////////////////////////
//游戏事件

//旁观消息
bool ClientKernelSink::OnEventLookonMode(void* data, int dataSize)
{
	PLAZZ_PRINTF(("flow->ClientKernelSink::OnEventLookonMode\n"));
	return true;
}

//场景消息
bool ClientKernelSink::OnEventSceneMessage(byte cbGameStatus, bool bLookonUser, void* data, int dataSize)
{
	switch (cbGameStatus)
	{
	case GAME_STATUS_FREE:
	case GAME_STATUS_PLAY:
	{
							 //效验数据
							 int nSize1 = sizeof(Role_Net_Object);
							 nSize1 = sizeof(uint16);
							 nSize1 = sizeof(uint64);
							 nSize1 = sizeof(uint32);
							 nSize1 = sizeof(uint8);
							 nSize1 = sizeof(Bullet_Config);
							 nSize1 = sizeof(int);

							 // 							 struct CMD_S_StatusFree
							 // 							 {
							 // 								 uint8 scene_;
							 // 								 int ex_score_cnt_;
							 // 								 int ex_bullet_cnt_;
							 // 								 int cannon_type_max_;
							 // 								 int cannon_price_base_;
							 // 								 uint8 ding_;
							 // 								 Role_Net_Object role_objects_[GAME_PLAYER];
							 // 							 };

							 // PLAZZ_PRINTF("%x %x  %x %x   ");

							 int nSize = sizeof(CMD_S_StatusFree);
							 if (dataSize != sizeof(CMD_S_StatusFree)) return false;

							 CMD_S_StatusFree * status_free = (CMD_S_StatusFree *)data;


							 //PLAZZ_PRINTF("%d %x  %x %d  %x  %x %x   ", &status_free->scene_, &status_free->ex_score_cnt_, &status_free->ex_bullet_cnt_, &status_free->cannon_type_max_, &status_free->cannon_price_base_, &status_free->ding_, &status_free->role_objects_ );

							 gDntgTimestampManager.timeCheck(0, 0);

							 mGameScene->on_scene_message(cbGameStatus, bLookonUser, data, dataSize);
							 send_time_check(true);
							 return true;
	}
	}
	return true;
}

//游戏消息
bool ClientKernelSink::OnEventGameMessage(int sub, void* data, int dataSize)
{
	bool isSuc = false;
	switch (sub)
	{
		//购买子弹成功
	case SUB_S_BUY_BULLET_SUCCESS:	isSuc = on_sub_buy_bullet_success(data, dataSize); break;
		//购买子弹失败
	case SUB_S_BUY_BULLET_FAILED:	isSuc = on_sub_buy_bullet_failed(data, dataSize); break;
		//改变炮类型
	case SUB_S_CHANGE_CANNON:		isSuc = on_sub_change_cannon(data, dataSize); break;
		//鱼生成
	case SUB_S_SEND_BIRD:			isSuc = on_sub_send_bird(data, dataSize); break;
		//子弹生成
	case SUB_S_SEND_BULLET:			isSuc = on_sub_send_bullet(data, dataSize); break;
		//开火失败
	case SUB_S_FIRE_FAILED:			isSuc = on_sub_fire_failed(data, dataSize); break;
		//抓住鱼
	case SUB_S_CATCH_BIRD:			isSuc = on_sub_catch_bird(data, dataSize); break;
		//抓住鱼
	case SUB_S_CATCH_BIRD_GROUP:	isSuc = on_sub_catch_bird_group(data, dataSize); break;
		//< 闪电鱼
	case SUB_S_CATCH_BIRD_CHAIN:	isSuc = on_sub_catch_bird_chain(data, dataSize); break;
		//改变场景
	case SUB_S_CHANGE_SCENE:		isSuc = on_sub_scene_change(data, dataSize); break;
		//
	case SUB_S_ACCOUNT:				isSuc = on_sub_game_account(data, dataSize); break;
		//时间校准
	case SUB_S_TIME_CHECK:			isSuc = on_sub_time_check(data, dataSize); break;
		//时间校准
	case SUB_S_ORDER_TIME_CHECK:	isSuc = on_sub_order_time_check(data, dataSize); break;
		//鱼特效
	case SUB_S_BIRD_EFFECT:			isSuc = on_sub_bird_effect(data, dataSize); break;
		//鱼boss
	case SUB_S_BIRD_BOSS:			isSuc = on_sub_bird_boss(data, dataSize); break;

		//武器信息
	case SUB_S_WEAPON_INFO:			isSuc = on_sub_weapon_info(data, dataSize); break;
		//武器等级
	case SUB_S_WEAPON_LEVEL:		isSuc = on_sub_weapon_level(data, dataSize); break;
		//财富信息
	case SUB_S_TREASURE:			isSuc = on_sub_treasure(data, dataSize); break;
		//技能
	case SUB_S_SKILL:				isSuc = on_sub_skill(data, dataSize); break;
		//强化结果
	case SUB_S_STRENGTHEN_RESULT:	isSuc = on_sub_strengthen_result(data, dataSize); break;

		//任务悬赏
	case  SUB_S_TASK_REWARD:		isSuc = on_sub_task_reward(data, dataSize); break;
		//任务
	case SUB_S_TASK:				isSuc = on_sub_task(data, dataSize); break;
		//任务数量
	case SUB_S_TASK_COUNT:			isSuc = on_sub_task_count(data, dataSize); break;
		//任务完成
	case SUB_S_TASK_COMPLETE:		isSuc = on_sub_task_complete(data, dataSize); break;
		//排行榜自己信息
	case SUB_S_RANK_INFO:			isSuc = on_sub_rank_info(data, dataSize); break;
		//排行榜列表
	case SUB_S_RANK_LIST:			isSuc = on_sub_rank_list(data, dataSize); break;
		//游戏状态 所有鱼
	case SUB_S_PLAY_BIRD:			isSuc = on_sub_play_bird(data, dataSize); break;
		//游戏状态 所有子弹
	case SUB_S_PLAY_BULLET:			isSuc = on_sub_play_bullet(data, dataSize); break;

		//////////////////////////////////////////////////////////////////////////
		// 控制
		//////////////////////////////////////////////////////////////////////////
		//库存操作
	case SUB_S_STORAGE:				isSuc = on_sub_storage(data, dataSize); break;
		//黑白名单
	case SUB_S_BLACK_WHITE:			isSuc = on_sub_black_white(data, dataSize); break;
		//发送特殊鱼阵
	case SUB_S_SEND_BIRD_LINEAR:	isSuc = on_sub_bird_linear(data, dataSize); break;
	case SUB_S_SEND_BIRD_ROUND:		isSuc = on_sub_bird_round(data, dataSize); break;
	case SUB_S_SEND_BIRD_PAUSE_LINEAR: isSuc = on_sub_bird_pause_linear(data, dataSize); break;

		//新加前控
	case SUB_S_STOCK_OPERATE_RESULT: isSuc = onSockOperateResult(data, dataSize); break;
	case SUB_S_ADMIN_CONTROL: isSuc = onRBLControlResult(data, dataSize); break;
	case SUB_S_DIFFICULTY:  isSuc = onDifficultOperateResult(data, dataSize); break;
		///< 任务打开
	case SUB_S_TASK_OPEN:	isSuc = on_sub_game_task_open(data, dataSize); break;
		///< 任务完成
	case SUB_S_TASK_FINISH:	isSuc = on_sub_game_task_finish(data, dataSize); break;
	}

	if (isSuc)
	{
		return true;
	}
	else
	{
		return false;
	}

}

//////////////////////////////////////////////////////////////////////////
//用户事件

//用户进入
void ClientKernelSink::OnEventUserEnter(IClientUserItem * pIClientUserItem, bool bLookonUser)
{
	if (!bLookonUser && mGameScene)
		mGameScene->net_user_enter(pIClientUserItem->GetChairID());
}

//用户离开
void ClientKernelSink::OnEventUserLeave(IClientUserItem * pIClientUserItem, bool bLookonUser)
{
	if (!bLookonUser && mGameScene)
		mGameScene->net_user_leave(pIClientUserItem->GetChairID());
}

//用户积分
void ClientKernelSink::OnEventUserScore(IClientUserItem * pIClientUserItem, bool bLookonUser)
{
	if (!bLookonUser && mGameScene)
	{
		// 		CGlobalUserInfo * pGlobalUserInfo = CGlobalUserInfo::GetInstance();
		// 		tagGlobalUserData * pGlobalUserData = pGlobalUserInfo->GetGlobalUserData();
		// 
		// 		pGlobalUserData->lUserIngot = pIClientUserItem->GetUserInfo()->lIngot;
	}
	//G_NOTIFY_D("EVENT_USER_SCORE", MTData::create(0,0,0,"","","",pIClientUserItem));
}

//用户状态
void ClientKernelSink::OnEventUserStatus(IClientUserItem * pIClientUserItem, bool bLookonUser)
{
	if (!bLookonUser)
		G_NOTIFY_D("EVENT_USER_STATUS", MTData::create(0, 0, 0, "", "", "", pIClientUserItem));
}

//用户属性
void ClientKernelSink::OnEventUserAttrib(IClientUserItem * pIClientUserItem, bool bLookonUser)
{
	if (!bLookonUser && mGameScene)
		mGameScene->net_user_attrib(pIClientUserItem->GetChairID());
}

//用户头像
void ClientKernelSink::OnEventCustomFace(IClientUserItem * pIClientUserItem, bool bLookonUser)
{
	PLAZZ_PRINTF(("flow->ClientKernelSink::OnEventCustomFace\n"));

}


//////////////////////////////////////////////////////////////////////////
// 功能函数
void ClientKernelSink::send_time_check(bool is_game_status)
{
	IClientKernel * kernel = IClientKernel::get();

	if (kernel == 0)
		return;

	CMD_C_TimeCheck time_check;
	time_check.chair_id_ = kernel->GetMeChairID();
	time_check.time_ = gDntgTimestampManager.getTimeCheck();
	time_check.is_game_status_ = is_game_status;

	kernel->SendSocketData(MDM_GF_GAME, SUB_C_TIME_CHECK, &time_check, sizeof(CMD_C_TimeCheck));
}

void Dntg::ClientKernelSink::OnEventUserClock(word wChairID, word wUserClock)
{
	CCLOG("The method or operation is not implemented.");
}

bool Dntg::ClientKernelSink::OnEventGameClockKill(word wChairID)
{
	return true;
}

bool Dntg::ClientKernelSink::OnEventGameClockInfo(word wChairID, uint nElapse, word wClockID)
{
	return true;
}

void Dntg::ClientKernelSink::CloseGameDelayClient()
{
	IClientKernel::destory();
}
