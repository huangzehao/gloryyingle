#ifndef __Dntg_PATH_MANAGER_H__
#define __Dntg_PATH_MANAGER_H__

///////////////////////////////////////////////////////////////////////////////////////////
#include "DntgGame/DntgView/DntgActionCustom.h"

namespace Dntg
{

	///////////////////////////////////////////////////////////////////////////////////////////
	class Path_Manager
	{
	private:
		static Path_Manager* msInstance;
	public:
		static Path_Manager* shared();
		static void purge();

	public:
		Move_Points &get_paths(uint16_t path_id, uint8_t path_type);

	private:
		Path_Manager();
		~Path_Manager();

		bool initialise_paths(const std::string &directory);
	private:
		std::vector<Move_Points> small_paths_;
		std::vector<Move_Points> big_paths_;
		std::vector<Move_Points> huge_paths_;
		std::vector<Move_Points> special_paths_;
		std::vector<Move_Points> scene_paths_;
	};

}

///////////////////////////////////////////////////////////////////////////////////////////


///////////////////////////////////////////////////////////////////////////////////////////

#endif